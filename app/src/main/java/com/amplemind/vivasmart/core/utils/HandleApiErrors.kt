package com.amplemind.vivasmart.core.utils

import android.util.Log
import com.crashlytics.android.Crashlytics
import org.json.JSONObject
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
open class HandleApiErrors @Inject constructor() {

    private fun getGenericMessage() : String {
        return "Error de conexión"
    }

    fun handleLoginError(error: Throwable) : String {
        Log.e("HandleApiErrors", error.toString())
        return if (error is HttpException) {
            val responseBody = error.response().errorBody()?.string()
            try {
                val jsonObject = JSONObject(responseBody)
                jsonObject.getJSONArray("errors").getJSONObject(0).getString("detail")
            } catch (e: Exception) {
                e.toString()
            }
        } else {
            Crashlytics.logException(error)
            getGenericMessage()
        }
    }

}