package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class ActivityCodeModel(
        @Index
        @SerializedName("id")                   var id: Long = 0,
        @Index
        @SerializedName("activity_id")          var activityId: Int = 0,
        @Index
        @SerializedName("activity_uuid")        var activityUuid: String = "",
        @Index
        @SerializedName("code")                 var code: Int = 0,
        @Ignore
        @SerializedName("total")                var total: String? = "",
        @Index
        @SerializedName("stage_id")             var stageId: Int = 0,
        @Index
        @SerializedName("stage_uuid")           var stageUuid: String = "",

        @PrimaryKey
        @SerializedName("uuid")
        override var uuid: String = getUUID(),

        @Index
        @SerializedName("version")
        var version: Long = 0,


        var activity: ActivityModel? = null,
        var stage: StageModel? = null

) : RealmObject(), Identifiable {

        @Index  var occupiedSpaces: Int = 0
        @Index  var totalSpaces: Int = 0
        @Index  var isOpen: Boolean = true

}
