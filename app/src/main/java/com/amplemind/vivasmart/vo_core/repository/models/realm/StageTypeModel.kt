package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class StageTypeModel(
        @SerializedName("lot_id")                   var lotId: Int? = 0,
        @SerializedName("id_stage_type")            var idStageType: Int? = 0,
        @SerializedName("date")                     var date: String? = "",
        @SerializedName("active")                   var isClosed: Boolean? = false,
        @SerializedName("activity_log_count")       var activityLogCount: Int = 0
): RealmObject()