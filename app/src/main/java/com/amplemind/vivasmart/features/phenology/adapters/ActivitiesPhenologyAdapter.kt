package com.amplemind.vivasmart.features.phenology.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemActivitiesPayrollBinding
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModel
import androidx.databinding.library.baseAdapters.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_TPYE
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.TablesPHCEPerDay
import com.amplemind.vivasmart.vo_core.repository.models.realm.TablesPerDay
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValvesPerLotFertirriegoModel
import io.reactivex.subjects.PublishSubject
import io.realm.Realm
import java.util.*

class ActivitiesPhenologyAdapter : RecyclerView.Adapter<ActivitiesPhenologyAdapter.ActivitiesPayrollViewHolder>() {

    private var list = mutableListOf<ItemActivitiesPayrollViewModel>()

    private val clickSubject = PublishSubject.create<ItemActivitiesPayrollViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivitiesPayrollViewHolder {
        val binding = ItemActivitiesPayrollBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ActivitiesPayrollViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ActivitiesPayrollViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(activities: List<ItemActivitiesPayrollViewModel>) {
        list.addAll(activities)
        notifyDataSetChanged()
    }

    fun onClickItem() = clickSubject

    inner class ActivitiesPayrollViewHolder(private val binding: ItemActivitiesPayrollBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ItemActivitiesPayrollViewModel

        fun bind(item: ItemActivitiesPayrollViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item
            if (adapterPosition > -1) {
                binding.textView26.isEnabled = item.active
                binding.imageView10.isEnabled = item.active
                binding.root.isEnabled = item.active
                binding.root.setOnClickListener {
                    clickSubject.onNext(this.item)
                }
            }
        }

    }

}
