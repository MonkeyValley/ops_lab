package com.amplemind.vivasmart.vo_features.adapters.recyclerview

import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.util.LongSparseArray
import android.view.View
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import java.util.*
import kotlin.math.roundToInt

abstract class BaseAdapter<
        T: Any,
        VM: BaseAdapter.ViewModel<T>,
        VH: BaseAdapter.ViewHolder<VM>>
    : RecyclerView.Adapter<VH>() {

    companion object {
        private val TAG: String = BaseAdapter::class.java.simpleName
    }

    private var mArrayCapacity: Int = 0

    open var items = listOf<T>()
        set(value) {
            if (field !== value) {
                field = value
                onDataSetChanged()
            }
        }

    protected lateinit var mViewModels: ArrayList<VM>

    private val mViewHolders = LongSparseArray<ViewHolder<*>>()

    open operator fun get(position: Int): T = items[position]

    override fun onViewRecycled(holder: VH) {
        holder.cleanUp()
        super.onViewRecycled(holder)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(viewHolder: VH, position: Int) {
        viewHolder.viewModel = getViewModel(position)
        viewHolder.bind(viewHolder.viewModel)
        mViewHolders.put(viewHolder.id, viewHolder)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        cleanUp()
    }

    override fun getItemId(position: Int): Long = mViewModels[position].id

    private fun calculateCapacity(itemsSize: Int): Int =
            if (mArrayCapacity < itemsSize) {
                (itemsSize * 1.2).roundToInt()
            }
            else {
                mArrayCapacity
            }


    protected open fun buildViewModelAndBind(position: Int): VM {

        val viewModel = buildViewModel(get(position)!!)
        viewModel.bind(viewModel.item)
        viewModel.key = getItemKey(viewModel.item)

        if (position >= mViewModels.size) {
            mViewModels.add(viewModel)
        }
        else {
            mViewModels[position] = viewModel
        }

        return viewModel
    }

    open fun cleanUp() {
        for (i: Int in 0 until mViewHolders.size()) {
            mViewHolders.valueAt(i).cleanUp()
        }
        mViewHolders.clear()
    }

    fun getViewModel(position: Int): VM = getViewModel(position, true)!!

    protected open fun getViewModel(position: Int, createIfNotExists: Boolean): VM? {
        var viewModel: VM? = null

        if (position < mViewModels.size) {
            viewModel = mViewModels[position]
        }

        if (viewModel == null && createIfNotExists) {
            viewModel = buildViewModelAndBind(position)
        }

        return viewModel
    }

    open fun setItems(changes: Changes<T>?) {
        if (changes != null) {
            if (changes.state == Changes.State.INITIAL) {
                items = changes.data
                Log.i(TAG, "Items changed ArraySize: ${mViewModels.size}")
            } else if(changes.state == Changes.State.UPDATE) {
                changes.forEachUpdateRange { type, range ->
                    when (type) {
                        Changes.Type.INSERT -> onInsertedRange(range.position, range.length)
                        Changes.Type.UPDATE -> onUpdatedRange(range.position, range.length)
                        Changes.Type.DELETE -> onDeletedRange(range.position, range.length)
                    }
                }
            }
        }
    }

    private fun allowedChangeLength(position: Int, length: Int): Int {
        return if (position + length > mViewModels.size) {
            mViewModels.size - position
        } else {
            length
        }
    }

    open fun onDataSetChanged() {
        clearViewModels()
        notifyDataSetChanged()
    }

    open fun onInsertedRange(position: Int, length: Int) {

        val fixedLength = allowedChangeLength(position, length)

        if (fixedLength > 0) {
            val newViewModels = List(fixedLength) { index ->
                buildViewModelAndBind(position + index)
            }

            mViewModels.addAll(position, newViewModels)

            Log.i(TAG, "Items inserted from $position length $fixedLength: ArraySize: ${mViewModels.size}")

            notifyItemRangeInserted(position, length)

            onInsertedViewModels(newViewModels)
        }
    }

    open fun onUpdatedRange(position: Int, length: Int) {

        val fixedLength = allowedChangeLength(position, length)

        if (fixedLength > 0) {
            for (i: Int in position until position + fixedLength) {
                val viewModel = mViewModels[i]
                viewModel.bind(viewModel.item)
            }

            Log.i(TAG, "Items updated from $position length $fixedLength: ArraySize: ${mViewModels.size}")

            notifyItemRangeChanged(position, fixedLength)
        }
    }

    open fun onDeletedRange(position: Int, length: Int) {

        val fixedLength = allowedChangeLength(position, length)

        if (fixedLength > 0) {
            val deletedItems = mutableListOf<VM>()

            for (i: Int in position until position + fixedLength) {
                getViewModel(i, false)?.apply {
                    deletedItems.add(this)
                    mViewModels.removeAt(i)
                }
            }

            Log.i(TAG, "Items removed from $position length $fixedLength: ArraySize: ${mViewModels.size}")

            notifyItemRangeRemoved(position, length)

            onDeletedViewModels(deletedItems)
        }
    }

    open fun onInsertedViewModels(viewModels: List<VM>) {}
    open fun onDeletedViewModels(viewModels: List<VM>) {}

    protected fun <VMH> forEachViewModel(it: MutableIterator<VMH>, getViewModel: (VMH) -> VM?, func: (VM) -> Unit) {
        while (it.hasNext()) {
            val viewModel = getViewModel(it.next())
            if (viewModel != null) {
                func.invoke(viewModel)
            }
        }
    }

    protected fun <T, VMH> mapViewModels(
            it: MutableIterator<VMH>,
            viewModelsSize: Int,
            getViewModel: (VMH) -> VM?,
            func: (VM) -> T?
    ): List<T> {
        val results = ArrayList<T>(viewModelsSize)
        forEachViewModel (it, getViewModel) { viewModel ->
            val result = func.invoke(viewModel)
            if (result != null) {
                results.add(result)
            }
        }
        return results
    }

    open fun clearViewModels() {
        mViewModels = ArrayList(calculateCapacity(itemCount))
        notifyDataSetChanged()
        System.gc()
    }

    open fun forEachViewModel(func: (VM) -> Unit) {
        if (::mViewModels.isInitialized) {
            forEachViewModel(mViewModels.iterator(), { it }, func)
        }
    }

    open fun <T> mapViewModels(func: (VM) -> T?) =
        if (::mViewModels.isInitialized) {
            mapViewModels(mViewModels.iterator(), mViewModels.size, { it }, func)
        }
        else {
            listOf()
        }

    abstract fun buildViewModel(item: T): VM
    abstract fun getItemKey(item: T): String

    abstract class ViewHolder<VM: ViewModel<*>> ( viewItem: View ): RecyclerView.ViewHolder(viewItem) {

        companion object {
            private var sID: Long = 1L
        }

        val id = sID++

        lateinit var viewModel: VM
            internal set

        abstract fun bind(viewModel: VM)

        open fun cleanUp() {}
    }

    abstract class ViewModel<T: Any>(var item: T) {

        companion object {
            private var sID: Long = 1L
        }

        val id = sID++

        lateinit var key: String
            internal set

        abstract fun bind(item: T)

    }

}