package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.PrimaryKey
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.google.gson.annotations.SerializedName

data class CarryOrderModel(@SerializedName("box_no") val boxNo: Double,
                           @SerializedName("variety_box") val varietyBoxCounts: List<VarietyBoxCount>,
                           @SerializedName("second_quality_box") val secondQualityBox: Double,
                           @SerializedName("business_unit_id") val businessUnitId: Int,
                           @SerializedName("collaborator_id") val collaboratorId: Int,
                           @SerializedName("crop_id") val cropId: Long,
                           @SerializedName("folio") val folio: String,
                           @SerializedName("is_active") @ColumnInfo(name = "carry_order_is_active") val isActive: Boolean,
                           @SerializedName("id") @PrimaryKey val id: Int,
                           @SerializedName("lot_id") val lotId: Int,
                           @SerializedName("revision") val reception: Boolean,
                           @SerializedName("sign_collaborator") val signCollaborator: String,
                           @SerializedName("sign_super") val signSuper: String,
                           @SerializedName("status") val status: String,
                           @SerializedName("table") val table: List<Int>?,
                           @SerializedName("temperature") val temperature: Double,
                           @SerializedName("userId") val userId: Int,
                           @SerializedName("vehicle_unit_id") val vehicleUnitId: Int,
                           @SerializedName("created_at") val date: String,
                           @SerializedName("comment") val comment: String?,
                           @SerializedName("lot") @Embedded val lot: LotModel?,
                           @SerializedName("crop") @Embedded val crop: CropStage?,
                           @SerializedName("user") @Embedded val user: UserModel?,
                           @SerializedName("valid_box_no") val validBoxNo: Double?,
                           @SerializedName("average_weight") val averageWeight: Double?
)

