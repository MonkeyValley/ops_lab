package com.amplemind.vivasmart.vo_core.repository.dao

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel
import io.realm.Realm

class GroovesDao: RealmDao<GrooveModel>(GrooveModel::class.java) {

    override fun retrieveServerData(response: Any?): List<GrooveModel>? {
        return null
    }

    fun getAssigned(activityCode: ActivityCodeModel): List<GrooveModel> {
        val realm = Realm.getDefaultInstance()
        val results = realm.where(mModelClazz)
                .equalTo("activityLog.activityCodeUuid", activityCode.uuid)
                .findAll()

        val ret = realm.copyFromRealm(results)
        realm.close()

        return ret
    }

    fun save(activityLog: ActivityLogModel, grooves: List<GrooveModel>): List<GrooveModel>? {

        val realm = Realm.getDefaultInstance()

        val results = realm.where(mModelClazz)
                .equalTo("activityLogUuid", activityLog.uuid)
                .findAll()

        realm.beginTransaction()
        results.deleteAllFromRealm()
        realm.commitTransaction()

        realm.close()

        grooves.forEach {
            it.activityLog = activityLog
            it.activityLogId = activityLog.id
            it.activityLogUuid = activityLog.uuid
        }

        return upsert(grooves)
    }

}