package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.google.gson.annotations.SerializedName

data class IssuesResponse(
        @SerializedName("data") val data: List<IssueModel>
)