package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class VehicleModel(@SerializedName("id") val id: Int,
                   @SerializedName("name") val name: String)