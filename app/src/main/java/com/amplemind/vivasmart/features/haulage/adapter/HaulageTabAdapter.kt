package com.amplemind.vivasmart.features.haulage.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.amplemind.vivasmart.features.haulage.fragment.HaulageFragment
import com.amplemind.vivasmart.features.haulage.fragment.HaulageReportFragment

class HaulageTabAdapter  (fm: FragmentManager, private val stageId: Int) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                return HaulageFragment().newInstance(stageId)
            }
            else -> return HaulageReportFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Crear orden"
            else -> "Reporte de acarreos"
        }
    }
}