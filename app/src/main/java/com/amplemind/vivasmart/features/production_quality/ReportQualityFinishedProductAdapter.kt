package com.amplemind.vivasmart.features.production_quality

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemReportQualityFinishedProductBinding
import io.reactivex.subjects.BehaviorSubject

class ReportQualityFinishedProductAdapter constructor(val list: MutableList<ItemReportFinishedProductViewModel>) : RecyclerView.Adapter<ReportQualityFinishedProductAdapter.ReportQualityFinishedProductViewHolder>() {

    private val onClickSubject = BehaviorSubject.create<ItemReportFinishedProductViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportQualityFinishedProductViewHolder {
        val binding = ItemReportQualityFinishedProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReportQualityFinishedProductViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ReportQualityFinishedProductViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<ItemReportFinishedProductViewModel> {
        return onClickSubject
    }

    fun isEmpty() = list.isEmpty()


    inner class ReportQualityFinishedProductViewHolder(private val binding: ItemReportQualityFinishedProductBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemReportFinishedProductViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.cvUser.setOnClickListener {
                    onClickSubject.onNext(item)
                }
                binding.cvUser.setOnLongClickListener {
                    onClickSubject.onNext(item)
                    return@setOnLongClickListener true
                }
                if (item.percentInt!! >= 94) {
                    binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.backGreen))
                } else if (item.percentInt!! > 89 && item.percentInt!! <= 93.99 ) {
                    binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.whiteCard))
                }
                else{
                    binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.redAlert))
                }
            }
        }

    }

}