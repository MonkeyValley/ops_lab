package com.amplemind.vivasmart.vo_core.repository.remote.utils

data class Optional<T>(
        val value: T?
)