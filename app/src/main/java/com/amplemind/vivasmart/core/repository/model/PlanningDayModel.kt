package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class PlanningDayModel(@SerializedName("estimated_units") val units: Double,
                            @SerializedName("people_no") val people: Int,
                            @SerializedName("estimated_hours") val estimatedHours: String,
                            @SerializedName("time") val time: Int,
                            @SerializedName("id") val id: Int)