package com.amplemind.vivasmart

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.util.Log
import com.amplemind.vivasmart.core.dagger.component.DaggerAppComponent
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.core.utils.RetrofitHolder
import com.amplemind.vivasmart.vo_core.repository.models.migrations.DBMigrations
import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import com.amplemind.vivasmart.vo_features.services.SyncManager
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Inject


/**
 * Created by
 * amplemind on 6/29/18.
 */
class VivaSmart : DaggerApplication(), Application.ActivityLifecycleCallbacks {

    @Inject
    lateinit var mRetrofitHolder: RetrofitHolder

    @Inject
    lateinit var mPreferences: UserAppPreferences

    @Inject
    lateinit var mEventBus: EventBus

    @Inject
    lateinit var mSyncManager: SyncManager

    private var refWatcher: RefWatcher? = null

    private var mActivityReferences: Int = 0
    private var mActivityIsChangingConfig: Boolean = false

    override fun onCreate() {
        super.onCreate()

        Fabric.with(this, Crashlytics())

        //Set up some information about the build in Crashlytics
        Crashlytics.setString   (  "Build type",     BuildConfig.BUILD_TYPE)
        Crashlytics.setString   (  "Build flavor",   BuildConfig.FLAVOR)
        Crashlytics.setBool     (  "Is Debug",       BuildConfig.DEBUG)

        Realm.init(this)
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this)
                                .withLimit(100000)
                                .build())
                        .build())

        val config = RealmConfiguration.Builder()
                .schemaVersion(MigrationExecutable.CURRENT_VERSION)
                .migration(DBMigrations())
                .build()

        Realm.setDefaultConfiguration(config)

        Log.e("host", "Selected log url ${mPreferences.selectedServerUrl}")

        mRetrofitHolder.setHost(mPreferences.selectedServerUrl)

        mSyncManager.setUp()

        if (BuildConfig.DEBUG == true) {
            //RealmDao.deleteDatabase()
            //RealmDao.deleteUploads()
            //Uploader.activate = false
        }

        registerActivityLifecycleCallbacks(this)

//        initializeLeakDetection()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
                .builder()
                .application(this)
                .build()
    }

    private fun initializeLeakDetection() {
        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return
            }
            refWatcher = LeakCanary.install(this)
        }
    }



    override fun onActivityPaused(activity: Activity?) {
    }

    override fun onActivityResumed(activity: Activity?) {
    }

    override fun onActivityStarted(activity: Activity?) {
        mActivityReferences++

        if (mActivityReferences == 1 && !mActivityIsChangingConfig) {
            mEventBus.send(AppInBackgroundEvent(false))
        }
    }

    override fun onActivityDestroyed(activity: Activity?) {
        refWatcher?.watch(activity)
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    override fun onActivityStopped(activity: Activity?) {
        mActivityReferences --
        mActivityIsChangingConfig = activity?.isChangingConfigurations ?: false
        if (mActivityReferences == 0 && !mActivityIsChangingConfig) {
            mEventBus.send(AppInBackgroundEvent(true))
        }
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
    }

    data class AppInBackgroundEvent(
            val isInBackground: Boolean
    )

}