package com.amplemind.vivasmart.core.dagger.component

import android.app.Application
import com.amplemind.vivasmart.VivaSmart
import com.amplemind.vivasmart.core.dagger.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by
 *          amplemind on 6/29/18.
 */
@Singleton
@Component(modules = [
    (AndroidSupportInjectionModule::class),
    (AppModule::class),
    (NetworkModule::class),
    (DatabaseDaoModule::class),
    (ServiceBuilderModule::class),
    (ActivityBuilder::class),
    (VONetworkModule::class),
    (VODatabaseModule::class)
])
interface AppComponent : AndroidInjector<VivaSmart> {

    override fun inject(app: VivaSmart)

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application : Application) : Builder

        fun build() : AppComponent
    }

}