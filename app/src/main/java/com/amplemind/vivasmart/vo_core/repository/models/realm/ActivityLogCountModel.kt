package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.utils.getUUID
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class ActivityLogCountModel (
       @PrimaryKey  var uuid: String = getUUID(),
       @Index       var stageUuid: String? = null,
       @Index       var activityUuid: String? = null,
       var count: Int = 0
): RealmObject()