package com.amplemind.vivasmart.vo_core.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.google.gson.annotations.SerializedName

class ActivityLogsResponse (
        @SerializedName("data") val data: List<ActivityLogModel>,
        @SerializedName("total_count") val totalCount: Int)