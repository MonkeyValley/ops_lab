package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityCodesDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.remote.PayrollActivitiesApi
import io.reactivex.Single
import io.realm.RealmObject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ActivityDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mActivitiesApi: PayrollActivitiesApi,
        private val mPref: UserAppPreferences,
        private val mActivityCodesDao: ActivityCodesDao
) : RealmDownloader<RealmObject?, ActivityModel>(realmHolder, ActivityModel::class.java) {

    override fun buildServerRequest(parent: RealmObject?, lastVersion: Long): Single<List<ActivityModel>> =
            mActivitiesApi.getActivities(
                    mPref.authorizationToken,
                    businessUnitId = mPref.user.businessUnitId,
                    version = lastVersion
            ).map { response ->
                Log.e("Version: ", lastVersion.toString())
                Log.e("Data-Activity: ", response.data.toString())
                response.data
            }

    override fun beforeSaving(parent: RealmObject?, child: ActivityModel): Single<ActivityModel> =
            mActivityCodesDao.findAllForActivity(child.uuid)
                    .flatMap { activityCodes ->
                        executeListTransaction {
                            activityCodes.map { activityCode ->
                                activityCode.apply {
                                    totalSpaces = child.unitLimit
                                }
                            }
                        }
                    }
                    .flatMap { super.beforeSaving(parent, child) }

}