package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.model.CategoriesDetailModel
import com.amplemind.vivasmart.features.interfaces.ICategoriesDetailItemViewModel
import java.util.*
import javax.inject.Inject

class CategoriesDetailItemViewModel @Inject constructor(private val model: CategoriesDetailModel){

    val name: String = model.name
    var count: String = model.count.toString()

    fun getId() : Int {
        return model.id
    }

}