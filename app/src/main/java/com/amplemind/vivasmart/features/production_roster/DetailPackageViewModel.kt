package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.extensions.formatMoney
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.core.repository.PayrollRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ActivityCodeDetail
import com.amplemind.vivasmart.core.repository.model.DetailtRosterLinesResponse
import com.amplemind.vivasmart.core.repository.model.DetailtRosterResponse
import com.amplemind.vivasmart.core.repository.request.UpdateCollaboratorSign
import com.amplemind.vivasmart.core.repository.request.UpdateCollaboratorUnity
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.production_roster.adapters.package_roster.DetailRosterTitlePackageViewModel
import com.amplemind.vivasmart.features.production_roster.adapters.package_roster.RosterDetailPackageViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject

class DetailPackageViewModel @Inject constructor(private val repository: PayrollRepository, private val mediator: Mediator,
                                                 private val errorApi: HandleApiErrors, private val repositoryTimer: ActivityCodeRepository,
                                                 private val preferences: UserAppPreferences, private val repoImage: ProfileRepository) : BaseViewModel() {

    private var intent: Intent? = null
    private val showSign = BehaviorSubject.create<Boolean>()
    private val name = BehaviorSubject.create<String>()
    private var reports = mutableListOf<RosterDetailPackageViewModel>()
    private val reportsSubject = BehaviorSubject.create<MutableList<RosterDetailPackageViewModel>>()
    //private val updatePosition = BehaviorSubject.create<UpdateResponse>()
    private val totalSalarySubject = BehaviorSubject.create<String>()
    private val sendSignSuccessful = BehaviorSubject.create<Boolean>()
    private val refressRoster = BehaviorSubject.create<Boolean>()
    private val colapsepPosition = mutableListOf<Int>()

    //region send results to view

    fun getCollasePositions(): MutableList<Int> {
        return colapsepPosition
    }

    fun showSign(): BehaviorSubject<Boolean> {
        return showSign
    }

    fun getName(): BehaviorSubject<String> {
        return name
    }

    fun onRefressData(): BehaviorSubject<Boolean> {
        return refressRoster
    }

    fun getRosterDetail(): BehaviorSubject<MutableList<RosterDetailPackageViewModel>> {
        return reportsSubject
    }

    /*
    fun updateItem(): BehaviorSubject<UpdateResponse> {
        return updatePosition
    }

     */

    fun getTotalSalary(): BehaviorSubject<String> {
        return totalSalarySubject
    }

    fun sendSignSuccess(): BehaviorSubject<Boolean> {
        return sendSignSuccessful
    }
    //endregion

    //region Get data in Intent
    private fun getNameIntent(): String {
        return intent!!.getStringExtra("name")
    }

    fun isSyncFlow(): Boolean {
        return intent!!.getBooleanExtra("SyncFlow", false)
    }

    fun getIdCollaborator(): Int {
        return intent!!.getIntExtra("id_collaborator", 0)
    }

    fun isReview(): Boolean {
        return intent!!.getBooleanExtra("is_review", false)
    }

    fun intentData(intent: Intent?) {
        this.intent = intent
        showSign.onNext(intent!!.getBooleanExtra("READ", false))
        name.onNext(getNameIntent())
    }
    //endregion

    private fun mapModelViewModelDetailLines(result: PayrollRepository.DetailtRosterLinesActivitiesResponse): List<RosterDetailPackageViewModel> {
        val viewModels = mutableListOf<RosterDetailPackageViewModel>()

        val list = result.response.sortedBy { it.activity_id }

        val ids = list.groupBy { it.activity_id }

        for (item in ids) {
            val details = mutableListOf<ItemRosterDetailViewModel>()

            val name = item.value.first().activity?.name ?: ""

            val titleModel = DetailRosterTitlePackageViewModel(name, item.key, item.value)

            item.value.forEach { report ->
                val model = ItemRosterDetailViewModel(name, createDetailRosterPackage(report), mediator)
                details.add(model)
            }

            viewModels.add(RosterDetailPackageViewModel(titleModel, details))
        }

        return viewModels
    }

    private fun createDetailRosterPackage(line: DetailtRosterLinesResponse): DetailtRosterResponse {
        val activityCode = ActivityCodeDetail(line.activity, line.activity_id, 0, line.id, true, line.stage_id)

        activityCode.activity?.isHead = false

        val map = DetailtRosterResponse(
                activityCode, line.packingline_collaborator_id,
                line.activity_id, line.collaborator_id,
                "", if (line.id_detail == null) line.id else line.id_detail.toInt(), false, line.is_training, line.is_practice,
                line.quantity, line.sign, line.time?.toInt() ?: 0, line.extra_time?.toInt() ?: 0, line.extra_quantity, true, line.packinglineCollaborator?.isTime
                ?: false, line.packing?.cost ?: 0.0, activityLogIdLocalDB = line.id.toLong(),
                statusSync = line.status_sync, packing_name = line.packing?.name ?: "")

        return map
    }

    fun loadRosterDetail(): Disposable {
        return repository.getDetailRosterLinesActivities(getIdCollaborator(), mediator.bundleItemLinesPackage!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .doOnError { progressStatus.onNext(false) }
                .map(this::mapModelViewModelDetailLines)
                .subscribe({ report ->
                    reports = report.toMutableList()
                    reportsSubject.onNext(reports)
                    calculateSalary()
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(errorApi.handleLoginError(it))
                })
    }

    fun loadLocalRosterDetailActivitiesLines(sync: Boolean = false): Disposable {
        return repository.getDetailRosterOfflineLinesActivities(getIdCollaborator(), mediator.bundleItemLinesPackage, sync)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map(this::mapModelViewModelDetailLines)
                .subscribe({ report ->
                    reports.addAll(report)
                    reportsSubject.onNext(reports)
                    calculateSalary()
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(errorApi.handleLoginError(it))
                })
    }

    fun editUnitOnline(cant: Double, time: Int, id_log: Int, position: Int?, dialog: EditQuantityFragment): Disposable {
        val milisecound = time * 60L
        val request = UpdateCollaboratorUnity(id_log, milisecound, cant)
        return repositoryTimer.updateCollaboratorUnity(listOf(request), true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (!colapsepPosition.contains(position)){
                        colapsepPosition.add(position!!)
                    }
                    refressRoster.onNext(true)
                    dialog.dismiss()
                }, {
                    dialog.setSnackError()
                })
    }

    fun editUnit(cant: Double, time: Int, id_log: Int, position: Int?, dialog: EditQuantityFragment): Disposable {
        val milisecound = time * 60L
        return repositoryTimer.updateLocalCollaboratorUnityOffline(milisecound, cant, id_log)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    //updatePosition.onNext(UpdateResponse(cant, milisecound, position!!, id_log))
                    dialog.dismiss()
                }, {
                    dialog.setSnackError()
                })
    }

    fun updateIsTrainingSalary(checked: Boolean) {
        reports.forEach { report ->
            report.reports.map {
                it.updateIsTraining(checked)
            }
        }
    }

    fun calculateSalary() {
        var result = 0.0
        reports.forEach { report ->
            result += report.reports.sumByDouble {
                if (it.isTraining) {
                    it.moneyTraining + it.moneyHrsExtra
                } else if (it.isPractice) {
                    it.moneyPractice + it.moneyHrsExtra
                } else {
                    it.money + it.moneyHrsExtra
                }
            }
        }
        totalSalarySubject.onNext(formatMoney(result))
    }

    fun isTrainingEnabled(): Boolean {
        reports.forEach { report ->
            for (act in report.reports) {
                if (act.isTrainingEnabled()) {
                    return true
                }
            }
        }
        return false
    }

    fun uploadSign(image: File, id_activities: List<Int>, isTraining: Boolean): Disposable {
        return repoImage.uploadImage(image, "sign")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    finishActivitesSignRequest(id_activities, it.relative_url, isTraining)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(errorApi.handleLoginError(it))
                })
    }

    private fun finishActivitesSignRequest(id_activities: List<Int>, path: String, isTraining: Boolean): Disposable {
        val request = mutableListOf<UpdateCollaboratorSign>()

        id_activities.forEach {
            request.add(UpdateCollaboratorSign(it, path, isTraining))
        }

        return repositoryTimer.finishCollaboratorSign(request, true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    sendSignSuccessful.onNext(true)
                }, {
                    requestFail.onNext(errorApi.handleLoginError(it))
                })
    }


    fun signLocalCollaborator(image: File, id_activities: List<Int>, isTraining: Boolean): Disposable {
        return repositoryTimer.finishLocalCollaboratorProduction(id_activities, isTraining, image.absolutePath, false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    sendSignSuccessful.onNext(true)
                }, {
                    requestFail.onNext(errorApi.handleLoginError(it))
                })
    }

    private val isTrainingActivities = BehaviorSubject.create<Boolean>()

    fun isAllTraining(): Disposable {
        return Observable.fromCallable {
            var isTraining = false
            reports.forEach {
                isTraining = it.reports.any { report -> report.isTraining }
                if (isTraining) {
                    it.reports.map {
                        it.updateIsTraining(true)
                    }
                }
            }
            return@fromCallable isTraining
        }.subscribe({
            isTrainingActivities.onNext(it)
        }, {
            requestFail.onNext(errorApi.handleLoginError(it))
        })
    }

    fun isTraining(): BehaviorSubject<Boolean> {
        return isTrainingActivities
    }


}
