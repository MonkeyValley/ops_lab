package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.repository.response.ActivitiesResponse
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import io.reactivex.subjects.BehaviorSubject

class TimerUserSectionViewModel constructor(val name: String,var collaborator_left : Int, val users: MutableList<TimerUserViewModel>,val id_lineOffline : Int? = -1, val id_activity : Int? = -1) : ExpandableGroup<TimerUserViewModel>(name,users) {

    /*
    * Behavior Observables
    * they are used to notify the view about any change
    * */
    private val collapsibleAnimObservable = BehaviorSubject.create<Boolean>()
    private val notifyCountChange = BehaviorSubject.create<Boolean>()

    private var costs : ActivitiesResponse? = null

    fun getActivityInfo(): ActivitiesResponse? {
        return costs
    }

    fun getUsersSize(): Int {
        return users.size
    }

    fun getCollaboratorLeft(): Int {
        return collaborator_left
    }

    fun getActivityId() : Int? {
        return users.first().getActivityCode()
    }

    fun getPackingId() : Int? {
        return users.first().packing_id
    }

    fun getPackingLine() : Int? {
        return users.first().packing_line
    }

    fun groupCollapsed() {
        collapsibleAnimObservable.onNext(true)
    }

    fun changeCountTitle(){
        notifyCountChange.onNext(true)
    }

    fun groupExpanded() {
        collapsibleAnimObservable.onNext(false)
    }

    fun getNotifyChange(): BehaviorSubject<Boolean> {
        return notifyCountChange
    }

    //region BehaviorSubject
    fun getCollapsibleAnimObservable(): BehaviorSubject<Boolean> {
        return collapsibleAnimObservable
    }

    fun setActivity(activity_info : ActivitiesResponse?) {
        this.costs = activity_info
    }
}