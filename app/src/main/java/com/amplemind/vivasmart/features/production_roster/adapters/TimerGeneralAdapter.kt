package com.amplemind.vivasmart.features.production_roster.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserGeneralViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import io.reactivex.subjects.BehaviorSubject

class TimerGeneralAdapter : RecyclerView.Adapter<TimerUserGeneralViewModel>() {

    private var list = mutableListOf<TimerUserViewModel>()
    private val userClick: BehaviorSubject<Pair<TimerUserViewModel, Int>> = BehaviorSubject.create()
    private val timeClick: BehaviorSubject<Pair<TimerUserViewModel, Int>> = BehaviorSubject.create()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimerUserGeneralViewModel {
        return TimerUserGeneralViewModel(LayoutInflater.from(parent.context).inflate(R.layout.item_users_general_timer, parent, false))
    }

    fun addElements(activities: List<TimerUserViewModel>) {
        list = activities as MutableList<TimerUserViewModel>
        notifyDataSetChanged()
    }

    fun onUserClick(): BehaviorSubject<Pair<TimerUserViewModel, Int>> {
        return userClick
    }

    fun onTimeClick(): BehaviorSubject<Pair<TimerUserViewModel, Int>> {
        return timeClick
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TimerUserGeneralViewModel, position: Int) {
        holder.bind(list[position], position, userClick, timeClick)
    }

}