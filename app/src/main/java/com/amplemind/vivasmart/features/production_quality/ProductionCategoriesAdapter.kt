package com.amplemind.vivasmart.features.production_quality

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemProductionCategoriesBinding
import io.reactivex.subjects.BehaviorSubject

class ProductionCategoriesAdapter constructor(val context: Context, val horizontalSpan: Int, val readOnly: Boolean = false) : RecyclerView.Adapter<ProductionCategoriesAdapter.ProductionCategoriesViewHolder>() {

    private val TAG = ProductionCategoriesAdapter::class.java.simpleName

    var list = mutableListOf<ProductionCategoriesItemViewModel>()

    private val onClick = BehaviorSubject.create<ProductionCategoriesItemViewModel>()

    fun addElements(data: List<ProductionCategoriesItemViewModel>) {
        list = data as MutableList<ProductionCategoriesItemViewModel>
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductionCategoriesViewHolder {
        val binding = ItemProductionCategoriesBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        // Get the ViewGroup requestType's measured width
        val measuredWidth = parent.measuredWidth
        Log.d(TAG, "Production categories measured width: " + measuredWidth)

        // Calculate desired width for each element
        val desiredWidth = measuredWidth / horizontalSpan
        Log.d(TAG, "Calculated desired width: " + desiredWidth)

        binding.root.layoutParams.width = desiredWidth
        binding.root.findViewById<TextView>(R.id.name).isSelected = true

        return ProductionCategoriesViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ProductionCategoriesViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem(): BehaviorSubject<ProductionCategoriesItemViewModel> {
        return onClick
    }

    inner class ProductionCategoriesViewHolder(private val binding: ItemProductionCategoriesBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ProductionCategoriesItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.setVariable(BR.isComplete, readOnly)
            binding.executePendingBindings()
            if (!readOnly) {
                binding.root.setOnClickListener {
                    if (adapterPosition > -1) {
                        onClick.onNext(item)
                    }
                }
            }
        }

    }

}