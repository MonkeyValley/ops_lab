package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import java.util.ArrayList

open class ActivityModel(
        @Index
        @SerializedName("id")                       var id: Int = 0,
        @SerializedName("activity_area_id")         var activityAreaId: Int = 0,
        @SerializedName("activity_category_id")     var activityCategoryId: Int = 0,
        @SerializedName("activity_sub_category_id") var activitySubCategoryId: Int = 0,
        @SerializedName("activity_type_id")         var activityTypeId: Int = 0,
        @Index
        @SerializedName("business_unit_id")         var businessUnitId: Int = 0,
        @SerializedName("code")                     var code: Int = 0,
        @SerializedName("cost")                     var cost: Double? = 0.0,
        @SerializedName("crops")                    var crops: RealmList<CropModel>? = null,
        @SerializedName("cost_per_hour")            var costPerHour: Double = 0.0,
        @SerializedName("extra_time_cost")          var extraTimeCost: Double = 0.0,
        @SerializedName("practice_cost")            var practiceCost: Double = 0.0,
        @SerializedName("is_head")                  var isHead: Boolean = false,
        @SerializedName("sunday_rate")              var sundayRate: Boolean = false,
        @SerializedName("is_practice")              var isPractice: Boolean = false,
        @SerializedName("control_code")             var controlCode: Int? = 0,
        @SerializedName("control_cost")             var controlCost: Double = 0.0,
        @SerializedName("description")              var description: String = "",
        @SerializedName("icon")                     var icon: String = "",
        @Index
        @SerializedName("is_active")                var isActive: Boolean? = false,
        @SerializedName("is_grooves")               var isGrooves: Boolean? = false,
        @SerializedName("is_removable")             var isRemovable: Boolean? = false,
        @Index
        @SerializedName("name")                     var name: String = "",
        @SerializedName("performance")              var performance: Double = 0.0,
        @SerializedName("range_from")               var rangeFrom: Int = 0,
        @SerializedName("range_to")                 var rangeTo: Int = 0,
        @SerializedName("unit")                     var unit: CropUnitModel? = null,
        @SerializedName("unit_id")                  var unitId: Int = 0,
        @SerializedName("unit_limit")               var unitLimit: Int = 0,
        @SerializedName("stage_activities")         var stageActivities: Boolean = false,

        @PrimaryKey @SerializedName("uuid")         override var uuid: String = getUUID(),
        @Index @SerializedName("version")           var version: Long = 0,

        //Local fields
        var activityLogCounts: RealmList<ActivityLogCountModel>? = RealmList()

) : RealmObject(), Identifiable , Parcelable {

        constructor(parcel: Parcel) : this(
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Double::class.java.classLoader) as Double,
                cropsArrayToRealmList(parcel.createTypedArrayList(CropModel.CREATOR)),
                parcel.readValue(Double::class.java.classLoader) as Double,
                parcel.readValue(Double::class.java.classLoader) as Double,
                parcel.readValue(Double::class.java.classLoader) as Double,
                parcel.readValue(Boolean::class.java.classLoader) as Boolean,
                parcel.readValue(Boolean::class.java.classLoader) as Boolean,
                parcel.readValue(Boolean::class.java.classLoader) as Boolean,
                parcel.readValue(Int::class.java.classLoader) as? Int,
                parcel.readDouble(),
                parcel.readString()!!,
                parcel.readString()!!,
                parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
                parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
                parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
                parcel.readString()!!,
                parcel.readValue(Double::class.java.classLoader) as Double,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readParcelable(CropUnitModel::class.java.classLoader),
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readValue(Int::class.java.classLoader) as Int,
                parcel.readByte() != 0.toByte(),
                parcel.readString()!!,
                parcel.readLong())

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeValue(id)
                parcel.writeValue(activityAreaId)
                parcel.writeValue(activityCategoryId)
                parcel.writeValue(activitySubCategoryId)
                parcel.writeValue(activityTypeId)
                parcel.writeValue(businessUnitId)
                parcel.writeValue(code)
                parcel.writeValue(cost)
                parcel.writeTypedList(crops)
                parcel.writeValue(costPerHour)
                parcel.writeValue(extraTimeCost)
                parcel.writeValue(practiceCost)
                parcel.writeValue(isHead)
                parcel.writeValue(sundayRate)
                parcel.writeValue(isPractice)
                parcel.writeValue(controlCode)
                parcel.writeDouble(controlCost)
                parcel.writeString(description)
                parcel.writeString(icon)
                parcel.writeValue(isActive)
                parcel.writeValue(isGrooves)
                parcel.writeValue(isRemovable)
                parcel.writeString(name)
                parcel.writeValue(performance)
                parcel.writeValue(rangeFrom)
                parcel.writeValue(rangeTo)
                parcel.writeParcelable(unit, flags)
                parcel.writeValue(unitId)
                parcel.writeValue(unitLimit)
                parcel.writeByte(if (stageActivities) 1 else 0)
                parcel.writeString(uuid)
                parcel.writeLong(version)
        }

        override fun describeContents(): Int {
                return 0
        }

        fun getCost(): Double {
                return cost ?: unit?.cost!!
        }

        fun getUnitName(): String {
                return when (activityTypeId) {
                        ActivityModel.TYPE_HOURS -> "Hora"
                        else                     -> unit?.name ?: "Surco"
                }
        }

        companion object {

                const val TYPE_PIECE = 1
                const val TYPE_HOURS = 2

                private fun cropsArrayToRealmList(array: ArrayList<CropModel>?): RealmList<CropModel> {
                        val list = RealmList<CropModel>()
                        array?.forEach { list.add(it) }
                        return list
                }

                @JvmField
                val CREATOR = object: Parcelable.Creator<ActivityModel> {
                        override fun createFromParcel(parcel: Parcel): ActivityModel {
                                return ActivityModel(parcel)
                        }

                        override fun newArray(size: Int): Array<ActivityModel?> {
                                return arrayOfNulls(size)
                        }
                }
        }


}
