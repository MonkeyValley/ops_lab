package com.amplemind.vivasmart.features.MLBarcodeScanner.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemCollaboratorsScannerBinding
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.reactivex.subjects.PublishSubject

class CollaboratorSacanAdapter : (RecyclerView.Adapter<CollaboratorSacanAdapter.CollaboratorSacanViewHolder>)() {

    private var list = listOf<CollaboratorModel>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    fun addElements(data: List<CollaboratorModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollaboratorSacanViewHolder {
        val binding = ItemCollaboratorsScannerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CollaboratorSacanViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CollaboratorSacanViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class CollaboratorSacanViewHolder(private val binding: ItemCollaboratorsScannerBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: CollaboratorModel

        fun bind(item: CollaboratorModel) {
            binding.setVariable(com.amplemind.vivasmart.BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.tvName!!.text = item.name
            val imageProfile = binding.root.findViewById<ImageView>(R.id.image_profile)

            Glide.with(binding.root.context)
                    .load(Uri.parse("https://s3-us-west-1.amazonaws.com/viva-organica-profile-pictures/fotosvo/${item.employeeCode}.JPG"))
                    .apply(RequestOptions()
                            .placeholder(R.drawable.ic_user))
                    .into(imageProfile)
        }

    }


}
