package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivitiesDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityCodesDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.StagesDao
import javax.inject.Inject

class TimerActivityRepository @Inject constructor(
        private val stagesDao: StagesDao,
        private val activitiesDao: ActivitiesDao,
        private val activityCodesDao: ActivityCodesDao,
        private val activityLogsDao: ActivityLogsDao
) : Repository() {

    override fun cleanUp() {
        stagesDao.cleanUp()
        activitiesDao.cleanUp()
        activityCodesDao.cleanUp()
        activityLogsDao.cleanUp()
    }

    fun loadStage(stageUuid: String) =
            stagesDao.findByUuid(stageUuid)

    fun loadActivity(activityUuid: String) =
            activitiesDao.findByUuid(activityUuid)

    fun loadActivityCodes(stageUuid: String, activityUuid: String) =
            activityCodesDao.findAllForStageAndActivity(stageUuid, activityUuid)

    fun createActivityCode(stageUuid: String, activityUuid: String) =
            activityCodesDao.create(stageUuid, activityUuid)

    fun createActivityLogs(activityCodeUuid: String, collaboratorUuids: Array<String>) =
            activityLogsDao.create(activityCodeUuid, collaboratorUuids)

}
