package com.amplemind.vivasmart.features.reciba_quality.viewModel

import com.amplemind.vivasmart.features.reciba_quality.repository.response.ReportRecibaQualityResponse
import java.math.RoundingMode

class ItemReportRecibaQualityViewModel (qualityReciba : ReportRecibaQualityResponse) {
    val id = qualityReciba.id.toString()
    val folio: String = qualityReciba.folio
    val lot_name = qualityReciba.lot_name
    val table = qualityReciba.table
    val percent = "${(qualityReciba.percent ?: 0.00).toBigDecimal().setScale(2, RoundingMode.HALF_EVEN)}%"
    val percentInt = qualityReciba.percent
}
