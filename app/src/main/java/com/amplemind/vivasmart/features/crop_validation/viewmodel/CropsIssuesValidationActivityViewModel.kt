package com.amplemind.vivasmart.features.crop_validation.viewmodel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.CropValidationRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import javax.inject.Inject


class CropsIssuesValidationActivityViewModel @Inject constructor(private val repository: CropValidationRepository, private val apiErrors: HandleApiErrors) : BaseViewModel() {


}