package com.amplemind.vivasmart.vo_core.repository.livedata

import android.util.Log

data class Changes<T: Any> (
    var inserted: List<Range>? = null,
    var updated: List<Range>? = null,
    var deleted: List<Range>? = null,
    var data: List<T> = listOf(),
    var state: State = State.INITIAL
) {

    enum class State {
        INITIAL,
        UPDATE,
        ERROR
    }

    enum class Type {
        INSERT,
        UPDATE,
        DELETE
    }

    fun forEachUpdateRange(func: (Type, Range) -> Unit) {
        if (state == State.UPDATE) {
            deleted?.reversed()?.forEach { range ->
                func.invoke(Type.DELETE, range)
            }
            inserted?.forEach { range ->
                func.invoke(Type.INSERT, range)
            }
            updated?.forEach { range ->
                func.invoke(Type.UPDATE, range)
            }
        }
    }

    fun forEachUpdate(func: (Type, Int) -> Unit) =
            forEachUpdateRange { type, range ->
                Log.e("RANGE", "${range.position}, ${range.length}")
                range.forEach { position ->
                    func.invoke( type, position)
                }
            }

    operator fun get(position: Int): T = data[position]

    data class Range (
            val position: Int,
            val length: Int
    ) {
        fun forEach(func: (Int) -> Unit) {
            for (position: Int in position until position + length) {
                func.invoke(position)
            }
        }
    }

}
