package com.amplemind.vivasmart.features.collaborators

import android.annotation.SuppressLint
import android.os.Bundle
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayout.Tab
import androidx.appcompat.view.ActionMode
import android.util.Log
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.collaborators.callBack.ActionModeCallback
import com.amplemind.vivasmart.features.collaborators.viewModel.AssingGroovesViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_features.production_roster.adapters.GrooveListPagerAdapter
import com.amplemind.vivasmart.vo_features.production_roster.adapters.GroovesAdapter
import kotlinx.android.synthetic.main.content_assing_grooves_activity.*
import javax.inject.Inject

@SuppressLint("Registered")
class AssignGroovesActivity : BaseActivityWithFragment() {

    companion object {
        private val TAG = AssignGroovesActivity::class.java.simpleName

        val PARAM_ACTIVITY_LOG_UUID  = "$TAG.ActivityLogUuid"
        val PARAM_ACTIVITY_CODE_UUID = "$TAG.ActivityCodeUuid"

        private var mActionMode: ActionMode? = null
        private val mActionModeCallback = ActionModeCallback()

    }

    @Inject
    lateinit var mViewModel: AssingGroovesViewModel

    @Inject
    lateinit var mEventBus: EventBus

    private lateinit var mPagerAdapter: GrooveListPagerAdapter

    var mSelectionCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_assing_grooves_activity)

        readArguments()

        setUpUI()
        setUpUICallbacks()
        setUpCallbacks()
        loadData()
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewModel.cleanUp()
        mPagerAdapter.cleanUp()
    }

    private fun readArguments() {
        mViewModel.activityCodeUuid = requireNotNull(intent?.extras?.get(PARAM_ACTIVITY_CODE_UUID) as String? )
        mViewModel.activityLogUuid = requireNotNull(intent?.extras?.get(PARAM_ACTIVITY_LOG_UUID) as String? )
    }

    override fun onSupportActionModeFinished(mode: ActionMode) {
        super.onSupportActionModeFinished(mode)

        if (mode == mActionMode) {
            mActionMode = null
        }

        mPagerAdapter.clearSelection()
        mSelectionCount = 0
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                if (mActionMode != null) {
                    mActionMode?.finish()
                }
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadData() {
        mViewModel.loadActivityLog().subscribe(this::onActivityLogLoaded)?.addTo(subscriptions)
        mViewModel.loadGrooves().subscribe(this::onGroovesLoaded)?.addTo(subscriptions)
    }

    private fun setUpCallbacks() {

    }

    private fun setUpUI() {
        setUpToolbar()
        setUpViewPager()
    }

    private fun setUpUICallbacks() {
        mEventBus.observe<GroovesAdapter.OnGrooveSelectionChangedEvent>()
                .filter {event ->
                    event.parentItemId == mViewModel.activityLogUuid
                }
                .subscribe(this::onActionModeUpdate)
                .addTo(subscriptions)

        mActionModeCallback.onAddClick().subscribe(this::onAddClicked).addTo(subscriptions)
    }

    private fun onActivityLogLoaded(activityLog: ActivityLogModel) {
        supportActionBar?.title = activityLog.collaborator?.name
        startActionMode(activityLog.grooves?.size ?: 0)
    }

    private fun onGroovesLoaded(tableGrooves: List<AssingGroovesViewModel.TableGrooves>) {
        mPagerAdapter.tableGrooves = tableGrooves
    }

    private fun setUpToolbar() {
        val mToolbar = toolbar_assing_grooves
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setSubtitle(R.string.aga_subtitle)
    }

    private fun setUpViewPager() {
        mPagerAdapter = GrooveListPagerAdapter(mEventBus)

        viewPager_assing_grooves.adapter = mPagerAdapter
        viewPager_assing_grooves.offscreenPageLimit = 3

        tabs_grooves.setupWithViewPager(viewPager_assing_grooves)

        tabs_grooves.addOnTabSelectedListener(object: OnTabSelectedListener {
            override fun onTabReselected(p0: Tab?) {
            }

            override fun onTabUnselected(tab: Tab?) {
                tab?.apply {
                    mPagerAdapter.cancelIfLoading(position)
                }
            }

            override fun onTabSelected(tab: Tab?) {
                tab?.apply {
                    mPagerAdapter.setUpTab(position)
                }
            }

        })
    }

    private fun startActionMode(count: Int) {
        if (count > 0) {
            mSelectionCount = count
            if (mActionMode == null) {
                mActionMode = startSupportActionMode(mActionModeCallback)
                Log.e("COUNT", count.toString())
            }
            mActionMode?.title = mSelectionCount.toString()
            mActionMode?.invalidate()
        }
    }

    private fun onActionModeUpdate(event: GroovesAdapter.OnGrooveSelectionChangedEvent) {
        Log.e("SELECTED", "${event.tableNo}, ${event.grooveNo}")
        if (event.isSelected) {
            if (mActionMode == null) {
                mActionMode = startSupportActionMode(mActionModeCallback)
            }
            mSelectionCount ++
        }
        else {
            mSelectionCount--
        }

        if (mSelectionCount > 0) {
            mActionMode?.title = mSelectionCount.toString()
            mActionMode?.invalidate()
        }
        else {
            mActionMode?.finish()
            mActionMode = null
        }
    }

    private fun onAddClicked(add: Boolean) {
        mViewModel.assignGrooves(mPagerAdapter.getSelectedGrooves()).subscribe({
            mActionMode?.finish()
            finish()
        }, {
            Log.e(TAG, Log.getStackTraceString(it))
        }).addTo(subscriptions)
    }
}