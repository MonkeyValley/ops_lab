package com.amplemind.vivasmart.features.collaborators.fragment


import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.features.MLBarcodeScanner.adapter.CollaboratorSacanAdapter
import com.amplemind.vivasmart.features.collaborators.SearchCollaboratorActivity
import com.amplemind.vivasmart.features.scannerbarcode.source.BarCodeScanningProcessor
import com.amplemind.vivasmart.features.scannerbarcode.source.CameraSource
import com.amplemind.vivasmart.features.scannerbarcode.source.CameraSourcePreview
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.BarcodeScannerViewModel
import kotlinx.android.synthetic.main.fragment_barcode_scanner.*
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BarcodeScannerFragment : BaseFragment() {

    @Inject
    lateinit var mPermission: AndroidPermission

    @Inject
    lateinit var mViewModel: BarcodeScannerViewModel

    private var mCameraSource: CameraSource? = null

    private var mBarcodeProcessor = BarCodeScanningProcessor()

    var mAdapter = CollaboratorSacanAdapter()
    var collaboratorList = ArrayList<CollaboratorModel>()
    var rvCollaboratorsScaner : RecyclerView? = null
    var llListCcan: LinearLayout? = null
    var tvScannerCount: TextView? = null
    var tvScannerName: TextView? = null
    var firePreview: CameraSourcePreview? = null

    private var mView: View? = null

    private var validate = true

    companion object {

        val TAG: String = BarcodeScannerFragment::class.java.simpleName

        @JvmStatic
        fun newInstance() = BarcodeScannerFragment()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_barcode_scanner, container, false)
        setUpUi()
        initRecycler()
        setUpCamera()
        return mView
    }

    fun setUpUi(){
        firePreview = mView!!.findViewById(R.id.fire_Preview)
        rvCollaboratorsScaner = mView!!.findViewById(R.id.rv_collaborators_scaner)
        llListCcan = mView!!.findViewById(R.id.ll_list_scan)
        tvScannerCount = mView!!.findViewById(R.id.tv_scanner_count)
        tvScannerName = mView!!.findViewById(R.id.tv_scanner_name)
    }

    override fun onResume() {
        super.onResume()
        setUpCamera()
    }

    override fun onPause() {
        super.onPause()
        releaseCamera()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mViewModel.cleanUp()
    }

    override fun setUpUICallbacks() {
        try {
            if(validate) {
                mBarcodeProcessor.getBarcode().throttleFirst(2, TimeUnit.SECONDS)
                        .concatMapMaybe(mViewModel::findByCode)
                        .subscribe(this::sendScannedEvent, this::onScanError)
                        .addTo(subscriptions)
            }
        } catch (e: Exception){
            Log.e("mBarcodeProcessor", e.toString())
        }
    }

    private fun setUpCamera() {
        if (mPermission.cameraPermission(context!!)) {
            if (mCameraSource == null) {
                mCameraSource = CameraSource(activity!!)
                mCameraSource!!.setMachineLearningFrameProcessor(mBarcodeProcessor)
                startCamera()
            }
        } else {
            mPermission.verifyHavePermissions(activity!!)
        }
    }

    private fun startCamera() {
        try {
            if (mCameraSource != null) {
                firePreview!!.start(mCameraSource!!)
            }
        } catch (e: IOException) {
            showErrorDialog(e)
        }
    }

    private fun releaseCamera() {
        firePreview!!.stop()
        firePreview!!.release()
        mCameraSource = null
    }

    private fun sendScannedEvent(collaborator: CollaboratorModel) {
        try {
            if (collaborator.isWorking == true) {
                if(validate) onErrorShowAlert(getString(R.string.bcsf_already_working))
            } else {

                addElementsPlague(collaborator)
                mEventBus.send(OnCodeScannedEvent(
                        collaborator.uuid,
                        collaborator.employeeCode,
                        collaborator.name
                ))
            }
        } catch (e: Exception){
            Log.e("sendScannedEvent", e.toString())
        }
    }

    private fun onScanError(error: Throwable) {
        if(error.localizedMessage.equals("Object is no longer managed by Realm. Has it been deleted?"))
        { if(validate) onErrorShowAlert("No existe este colaborador en la base de datos del dispositivo o fallo el scaneo") }
        else {if(validate) onErrorShowAlert(error.localizedMessage) }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            mPermission.REQUEST_CODE_PERMISSIONS -> {
                startCamera()
            }
        }
    }

    data class OnCodeScannedEvent(
            val uuid: String,
            val code: String,
            val name: String
    )

    override fun onError(error: String) {
        AlertDialog.Builder(context!!)
                .setTitle(getString(R.string.app_name))
                .setMessage(error)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, i ->
                    dialogInterface.dismiss()
                    setUpUICallbacks()
                    //activity!!.finish()
                }.show()

    }

    private fun initRecycler() {
        rvCollaboratorsScaner!!.hasFixedSize()
        rvCollaboratorsScaner!!.layoutManager = LinearLayoutManager(activity)
        rvCollaboratorsScaner!!.itemAnimator = DefaultItemAnimator()
        rvCollaboratorsScaner!!.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        rvCollaboratorsScaner!!.adapter = mAdapter
    }

    fun addElementsPlague(data: CollaboratorModel) {
        llListCcan!!.visibility = View.VISIBLE
        tvScannerName!!.text = data.name

        collaboratorList.add(data)

        tvScannerCount!!.text = "Colaboradores escaneados - " + collaboratorList.distinctBy { it.id }.size

        mAdapter!!.addElements(collaboratorList.distinctBy { it.id })
    }


    open fun onErrorShowAlert(error: String) {
        validate = false
        AlertDialog.Builder(activity!!)
                .setTitle(getString(R.string.app_name))
                .setMessage(error)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, i ->
                    validate = true
                    dialogInterface.dismiss()
                }.show()

    }


}
