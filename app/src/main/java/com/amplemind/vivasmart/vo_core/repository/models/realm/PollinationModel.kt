package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class PollinationModel (
        @SerializedName("stage_id") var stageId: Int? = 0,
        @SerializedName("level_0") var lvl0: Int? = 0,
        @SerializedName("level_1") var lvl1: Int? = 0,
        @SerializedName("level_2") var lvl2: Int? = 0,
        @SerializedName("level_3") var lvl3: Int? = 0,
        @SerializedName("percent_level_0") var porcentLvl0: Double? = 0.0,
        @SerializedName("percent_level_1") var porcentLvl1: Double? = 0.0,
        @SerializedName("percent_level_2") var porcentLvl2: Double? = 0.0,
        @SerializedName("percent_level_3") var porcentLvl3: Double? = 0.0,
        @SerializedName("pollination_percent") var pollinationPorcent: Double? = 0.0,
        @SerializedName("table_no") var table: Int? = 0,
        @SerializedName("week_no") var week: Int? = 0,
        @SerializedName("flowers") var flowers: Int? = 0,
        @PrimaryKey @SerializedName("uuid") override var uuid: String = UUID.randomUUID().toString()
) : RealmObject(), Identifiable