package com.amplemind.vivasmart.features.fertiriego.fragment

import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_DATE
import com.amplemind.vivasmart.features.fertiriego.adapter.FertirriegoSoilReportAdapter
import com.amplemind.vivasmart.features.fertiriego.viewModel.FertirriegoSoilMonitoringViewModel
import com.amplemind.vivasmart.features.fertiriego.viewModel.SoilReportValveModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PHCEForTable
import com.jakewharton.rxbinding2.view.visibility
import javax.inject.Inject

class SoilReportFragment : BaseFragment(), View.OnClickListener {

    @Inject
    lateinit var viewModel: FertirriegoSoilMonitoringViewModel

    fun newInstance(date: String, lotId: Int, table: String, lotName: String): SoilReportFragment {
        val args = Bundle()
        args.putString("DATE", date)
        args.putInt("LOT_ID", lotId)
        args.putString("TABLE", table)
        args.putString("LOT_NAME", lotName)

        val fragment = SoilReportFragment()
        fragment.arguments = args
        return fragment
    }

    var soilReportList = ArrayList<SoilReportValveModel>()

    var mView: View? = null
    var etCeSend: TextView? = null
    var etPhSend: TextView? = null
    var etCeSqueezed: TextView? = null
    var etPhSqueezed: TextView? = null
    var rvSoilReport: RecyclerView? = null
    var btnSend: Button? = null

    var lotId = 0
    var date = ""
    var table = ""
    var lotName = ""

    private var soilReportAdapter = FertirriegoSoilReportAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_soil_report, container, false)

        argument()
        setUpIu()
        viewModel.setServiceContect(activity!!)
        viewModel.fragment = this
        setupRecycler()
        addSubscribers()
        loadDataReport()
        return mView
    }

    fun setUpIu(){
        etCeSend = mView!!.findViewById(R.id.et_ce_send)
        etPhSend = mView!!.findViewById(R.id.et_ph_send)
        etCeSqueezed = mView!!.findViewById(R.id.et_ce_squeezed)
        etPhSqueezed = mView!!.findViewById(R.id.et_ph_squeezed)
        rvSoilReport = mView!!.findViewById(R.id.rv_soil_report)
        btnSend = mView!!.findViewById(R.id.btn_send)
        btnSend!!.setOnClickListener(this)
    }

    fun argument(){
        lotId = arguments!!.getInt("LOT_ID", 0) ?: 0
        date = arguments!!.getString("DATE") ?: ""
        table = arguments!!.getString("TABLE") ?: ""
        lotName = arguments!!.getString("LOT_NAME") ?: ""
        viewModel.date = date
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccesPHCEDataPerDay().subscribe(this::setDataForPHCE).addTo(subscriptions)
        viewModel.onSuccesPulseForTable().subscribe(this::addElementsPulse).addTo(subscriptions)
    }

    fun loadDataReport(){
        date = FERTIRRIEGO_DATE
        viewModel.date = date
        setDataToZero()
        if(viewModel.validateDate())  btnSend!!.visibility = View.VISIBLE
        else btnSend!!.visibility = View.GONE
        viewModel.getPHCEDataForTable(lotId, date, "T"+table)
        viewModel.getValveForTable(lotId, date, "T"+table)
    }

    private fun setDataForPHCE(list: List<PHCEForTable>) {
        list.forEach {
            if(it.type == "envio") {
                etPhSend!!.text = if (it.ph != null) it.ph else "-"
                etCeSend!!.text = if (it.ce != null) it.ce else "-"
            }  else if(it.type == "chupatubo") {
                etPhSqueezed!!.text = if (it.ph != null) it.ph else "-"
                etCeSqueezed!!.text = if (it.ce != null) it.ce else "-"
            }
        }
    }

    fun setDataToZero(){
        btnSend!!.visibility = View.GONE
        etPhSend!!.text = "-"
        etCeSend!!.text = "-"
        etPhSqueezed!!.text = "-"
        etCeSqueezed!!.text = "-"
        soilReportList.clear()
        soilReportAdapter.addElements(soilReportList)
    }

    private fun setupRecycler() {
        rvSoilReport!!.hasFixedSize()
        rvSoilReport!!.layoutManager = LinearLayoutManager(activity)
        rvSoilReport!!.itemAnimator = DefaultItemAnimator()
        rvSoilReport!!.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        rvSoilReport!!.adapter = soilReportAdapter
    }

    private fun addElementsPulse(data: List<SoilReportValveModel>) {
        soilReportList.clear()
        soilReportList.addAll(data)
        soilReportAdapter.addElements(data)

        if(data.isEmpty()) btnSend!!.visibility = View.GONE
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btn_send -> {
                if(validateData()) {
                    val builder = android.app.AlertDialog.Builder(activity!!)
                    builder.setTitle("Reporte de suelo")
                    builder.setMessage("¿Desea enviar el registro de riego de "+ lotName + " - tabla " + table + "?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                        showProgressDialog(true)
                        soilReportList.forEach {
                            viewModel.saveData(it.valveUuId, it.pulsesNo, it.minutes, it.m3ha)
                        }
                        dialog.dismiss()
                        finishSoilReport()
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
                    builder.show()
                } else {
                    soilReportList.forEach {
                        if(!it.active) {
                            showSnackBar("La informacion ya ha sido enviada")
                            return
                        }
                    }
                    showSnackBar("Todos los datos son requeridos y deben tener un valor diferente a 0")
                }
            }
        }
    }

    private fun finishSoilReport(){
        showProgressDialog(false)
        val builder = android.app.AlertDialog.Builder(activity!!)
        builder.setTitle("Reporte de suelo")
        builder.setMessage("Información enviada")
        builder.setPositiveButton(android.R.string.ok) { dialog, _ ->
            dialog.dismiss()
            btnSend!!.visibility = View.GONE
            loadDataReport()
        }
        builder.show()
    }

    fun validateData(): Boolean {
        var validate = true

        soilReportList.forEach { item ->
            if(item.pulsesNo == 0) validate = false
            if(item.m3ha == 0) validate = false
        }

        return validate
    }

    fun showSnackBar(msj: String){
        val snackbar = Snackbar.make(mView!!, msj, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

}
