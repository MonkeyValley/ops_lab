package com.amplemind.vivasmart.features.production_roster.adapters

import android.annotation.SuppressLint
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import com.amplemind.vivasmart.vo_core.utils.*
import io.reactivex.subjects.BehaviorSubject
import java.text.SimpleDateFormat
import java.util.*


class ExtraHoursMessagesAdapter(val list: List<MessageModel>) : RecyclerView.Adapter<ExtraHoursMessagesAdapter.ExtraHoursMessagesAdapterViewHolder>() {
    private val clickSubject = BehaviorSubject.create<MessageModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExtraHoursMessagesAdapterViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_extra_hours_inbox, parent, false)
        return ExtraHoursMessagesAdapterViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ExtraHoursMessagesAdapterViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onclickItem(): BehaviorSubject<MessageModel> {
        return clickSubject
    }


    inner class ExtraHoursMessagesAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var solicitudTxt: TextView
        lateinit var dateTxt: TextView
        lateinit var hourTxt: TextView
        lateinit var cvExtra: CardView

        @SuppressLint("SimpleDateFormat")
        fun bind(item: MessageModel) {
            // TODO: Futura programacion del bind de este adapater
            solicitudTxt = itemView.findViewById(R.id.extrahours_inbox_txt_solicitud)
            dateTxt = itemView.findViewById(R.id.extrahours_inbox_txt_date)
            hourTxt = itemView.findViewById(R.id.extrahours_inbox_txt_hour)
            cvExtra = itemView.findViewById(R.id.cv_inbox_extra_hour)
            solicitudTxt.text = retrieveCaptionForType(item.type)
            setDates(item.createdAt, dateTxt, hourTxt)

            if(!item.status.equals("new")){
                // cvExtra.setCardBackgroundColor(ContextCompat.getColor(itemView.context, R.color.blue_background_message))
            }

            cvExtra.setOnClickListener {
                clickSubject.onNext(item)   // TODO
            }

        }


    }

    private fun retrieveCaptionForType(type: String): String {
        return when (type) {
            TYPE_COLLABORATORS -> "Colaboradores"
            TYPE_LOTS -> "Lotes"
            TYPE_ACTIVITIES -> "Actividades"
            TYPE_ACTIVITY_CODES -> "Códigos"
            TYPE_ACTIVITY_CODE_COLLABORATORS -> "Colaboradores en códigos de actividad"
            TYPE_ACTIVITY_LOGS -> "Bitácoras de actividades"
            TYPE_EXTRA_TIME -> "Solicitud de Horas Extra"
            else -> "Notificación nueva"
        }
    }

    fun getDate(date: String): String {
        val date = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH).parse(date)
        return SimpleDateFormat("dd/MM/yyyy HH:mm a", Locale.ENGLISH).format(date)
    }

    private fun setDates(createdAt: String, datetxt: TextView, hourtxt: TextView) {

        val date = getDate(createdAt)
        if(date != null){
            Log.d("fechita", date.toString())
            val datet =SimpleDateFormat("dd/MM/yyyy HH:mm a", Locale.ENGLISH).parse(date)
            datetxt.text =  SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(datet)
            hourtxt.text = SimpleDateFormat("HH:mm a", Locale.ENGLISH).format(datet)
        }

    }
}