package com.amplemind.vivasmart.core.repository.model

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel


data class AssingGroovesModel(val groove: GrooveModel, val status : Boolean, var state : Int, var animation_status : Int?) {

    val grooveNo: Int
        get() = groove.grooveNo

    val name: String
        get() = groove.grooveNo.toString()

    val table: Int
        get() = groove.tableNo

    val activityLog: ActivityLogModel?
        get() = groove.activityLog



}