package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ProductionCategoriesModel (@SerializedName("id") @PrimaryKey val id: Int,
                                      @SerializedName("name") val name: String,
                                      @SerializedName("count") val total: Int,
                                      var stageId: Int? = null)