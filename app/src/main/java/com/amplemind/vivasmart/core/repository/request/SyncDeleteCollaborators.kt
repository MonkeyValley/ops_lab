package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

data class SyncDeleteCollaborators(@SerializedName("bulk") val bulk: List<DeleteCollaborator>)

data class DeleteCollaborator(@SerializedName("activity_log_id") val activityLog: Int,
                              @SerializedName("activity_code_collaborator_id") val collaboratorId: Int)