package com.amplemind.vivasmart.domain.qualitycarryorder

import android.util.Log
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.platform.SyncNotification
import com.amplemind.vivasmart.core.platform.SynchronizeDataBaseService
import com.amplemind.vivasmart.core.repository.CleanReportTotalRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.ControlQualityDao
import com.amplemind.vivasmart.core.repository.model.SyncReportModel
import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderSyncRequest
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.PicturesHelper
import org.jetbrains.anko.doAsync
import java.io.File
import javax.inject.Inject

class UploadQualityCarryOrder @Inject constructor(private val database: ControlQualityDao,
                                                  private val picturesHelper: PicturesHelper,
                                                  private val uploadRepository: ProfileRepository,
                                                  private val repository: CleanReportTotalRepository) {

    private var delegate: SyncNotification? = null
    private val type = SynchronizeDataBaseService.SynchronizeRequest.QUALITY_CARRY_ORDER
    private var subscriptions: AndroidDisposable? = null

    val listPhotos = mutableListOf<QualityCarryOrderRequest>()

    fun syncCarryOrders(delegate: SyncNotification, subscriptions: AndroidDisposable) {
        this.delegate = delegate
        this.subscriptions = subscriptions

        doAsync({
            Log.e("Error", "syncCarryOrders ${it.message}")
            delegate.onTerminate(mutableListOf(), type, "syncCarryOrders ${it.message}")
        }, {
            uploadPhotosQuality(database.getQualityCarryOrderRequest().toMutableList())
        })
    }

    private fun uploadPhotosQuality(list: MutableList<QualityCarryOrderRequest>) {
        if (list.isEmpty()) {
            syncReports(listPhotos)
            return
        }
        val photos = mutableListOf<String>()

        val item = list[0]
        if (item.image_one != null) {
            photos.add(item.image_one!!)
            item.image_one = null
        }
        if (item.image_two != null) {
            photos.add(item.image_two!!)
            item.image_two = null
        }
        if (item.image_three != null) {
            photos.add(item.image_three!!)
            item.image_three = null
        }
        if (item.image_four != null) {
            photos.add(item.image_four!!)
            item.image_four = null
        }

        if (photos.isEmpty()) {
            listPhotos.add(item)
            list.removeAt(0)
            uploadPhotosQuality(list)
        } else {
            uploadImages(photos, list)
        }
    }

    private fun uploadImages(photos: MutableList<String>, list: MutableList<QualityCarryOrderRequest>) {
        if (photos.isEmpty()) {
            listPhotos.add(list[0])
            list.removeAt(0)
            uploadPhotosQuality(list)
            return
        }

        val photo = photos[0]
        val file = File(photo)
        picturesHelper.compressImage(file, file, 400f, 400f)
        uploadRepository.uploadImage(file, "user")
                .subscribe({
                    photos.removeAt(0)
                    val item = list[0]
                    if (item.image_one == null) {
                        item.image_one = it.relative_url
                        uploadImages(photos, list)
                        return@subscribe
                    }
                    if (item.image_two == null) {
                        item.image_two = it.relative_url
                        uploadImages(photos, list)
                        return@subscribe
                    }
                    if (item.image_three == null) {
                        item.image_three = it.relative_url
                        uploadImages(photos, list)
                        return@subscribe
                    }
                    if (item.image_four == null) {
                        item.image_four = it.relative_url
                        uploadImages(photos, list)
                        return@subscribe
                    }
                }, {
                    photos.removeAt(0)
                    uploadImages(photos, list)
                }).addTo(subscriptions!!)
    }


    private fun syncReports(list: List<QualityCarryOrderRequest>) {
        repository.syncQualityCarryOrder(QualityCarryOrderSyncRequest(list))
                .subscribe({ reportsSent ->
                    delegate?.onTerminate(reportsSent.data.map {
                        val total = 1
                        val complete = if (it.status) total else 0
                        SyncReportModel("Reporte ${it.id}", total, complete, it.carryOrderId)
                    }, type)
                    deleteCarryOrders()
                }, {
                    val reports = list.map {
                        SyncReportModel("Reporte ${it.id}", 1, 0, it.carryOrderId)
                    }
                    delegate?.onTerminate(reports, type)
                }).addTo(subscriptions!!)
    }

    private fun deleteCarryOrders() {
        doAsync({
            Log.e("Error", "deleteCarryOrders ${it.message}")
            delegate?.onTerminate(mutableListOf(), type, "deleteCarryOrders ${it.message}")
        }, {
            database.deleteAllCarryOrders()
        })
    }


}