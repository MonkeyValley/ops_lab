package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.ActivityCodesDao
import com.amplemind.vivasmart.vo_core.repository.dao.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.DailyReportModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.ActivityCodeUploader
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.ActivityLogUploader
import io.reactivex.Observable
import java.io.File
import javax.inject.Inject

class ActivityCodeRepository @Inject constructor(
        private val dao: ActivityCodesDao,
        private val logsDao: ActivityLogsDao,
        private val codesUploader: ActivityCodeUploader,
        private val logsUploader: ActivityLogUploader) {

    fun getCodes(activityId: Int, stageId: Int): Observable<List<ActivityCodeModel>> {
        return Observable.fromCallable{
            return@fromCallable dao.getCodes(activityId, stageId)
        }
    }

    fun createCode(activityId: Int, stageId: Int): Observable<ActivityCodeModel?> {
        return Observable.fromCallable{
            return@fromCallable dao.createCode(activityId, stageId)
        }.doOnNext {
            codesUploader.create(it!!)
        }
    }

    fun loadUndone(activityCode: ActivityCodeModel): Observable<Changes<ActivityLogModel>> {
        return logsDao.loadUndone(activityCode)
    }

    fun startTimer(activityLog: ActivityLogModel): Observable<ActivityLogModel?> {
        return startTimer(listOf(activityLog))
                .map {
                    return@map it.first()
                }
    }

    fun startTimer(activityLogs: List<ActivityLogModel>): Observable<List<ActivityLogModel>?> {
        return Observable.fromCallable{
            return@fromCallable logsDao.startTimer(activityLogs)
        }
                .doOnNext{
                    logsUploader.update(it!!)
                }
    }

    fun pauseTimer(activityLog: ActivityLogModel): Observable<ActivityLogModel?> {
        return pauseTimer(listOf(activityLog))
                .map{
                    return@map it.first()
                }
    }

    fun pauseTimer(activityLogs: List<ActivityLogModel>): Observable<List<ActivityLogModel>?> {
        return Observable.fromCallable{
            return@fromCallable logsDao.pauseTimer(activityLogs)
        }
                .doOnNext {
                    logsUploader.update(it!!)
                }
    }

    fun finish(activityLog: ActivityLogModel, sign: File?): Observable<ActivityLogModel?> {
        return Observable.fromCallable{
            return@fromCallable logsDao.finish(activityLog, sign)
        }
                .doOnNext {
                    logsUploader.update(it!!)
                }
    }

    fun finish(activityLogs: List<ActivityLogModel>): Observable<List<ActivityLogModel>?> {
        return Observable.fromCallable {
            return@fromCallable logsDao.finish(activityLogs)
        }
                .doOnNext {
                    logsUploader.update(it!!)
                }
    }

    fun deleteActivityLog(activityLog: ActivityLogModel): Observable<ActivityLogModel?> {
        return Observable.fromCallable {
            return@fromCallable logsDao.remove(activityLog)
        }
                .doOnNext{
                    logsUploader.delete(it!!)
                }
    }

    fun setUnits(activityLog: ActivityLogModel, quantity: Int): Observable<ActivityLogModel?> {
        return Observable.fromCallable {
            return@fromCallable logsDao.setUnits(activityLog, quantity)
        }
                .doOnNext {
                    logsUploader.update(it!!)
                }
    }

    fun setUnits(activityLog: ActivityLogModel, goodQuantity: Int, badQuantity: Int): Observable<ActivityLogModel?> {
        return Observable.fromCallable {
            return@fromCallable logsDao.setUnits(activityLog, goodQuantity, badQuantity)
        }
                .doOnNext {
                    logsUploader.update(it!!)
                }
    }

    fun calculateUnitTotals(activityCode: ActivityCodeModel): Observable<String> {
        return dao.calculateUnitTotals(activityCode)
    }

    fun retrieveDailyReportForDate(stage: StageModel, date: String): Observable<List<DailyReportModel>?> {
        return Observable.fromCallable{
            return@fromCallable logsDao.retrieveDailyReportForDate(stage, date)
        }
    }

    fun retrieveTodaysHarvestTotalBoxCount(activityLog: ActivityLogModel): Observable<Int> {
        return Observable.fromCallable {
            return@fromCallable logsDao.retrieveTodaysHarvestTotalBoxCount(activityLog)
        }
    }


}

