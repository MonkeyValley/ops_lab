package com.amplemind.vivasmart.core.repository.request

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class QualityCarryOrderRequest(@SerializedName("carry_order_id") val carryOrderId: Int,
                               @SerializedName("sample_size") val sampleSize: Int,
                               @SerializedName("unit_id") val unitId: Int,
                               @SerializedName("user_id") val userId: Int,
                               @SerializedName("issues") val issues: List<IssueRequest>,
                               @SerializedName("export") val export: Int,
                               @SerializedName("permanent_defect") val permanentDefect: Int,
                               @SerializedName("defect_condition") val defectCondition: Int,
                               @SerializedName("offline_id") @PrimaryKey(autoGenerate = true) val id: Int? = null,
                               @SerializedName("status") val status: Boolean = false,
                               @SerializedName("image1") var image_one : String? = null,
                               @SerializedName("image2") var image_two : String? = null,
                               @SerializedName("image3") var image_three : String? = null,
                               @SerializedName("image4") var image_four : String? = null
)

class QualityCarryOrderSyncRequest(@SerializedName("bulk") val bulk: List<QualityCarryOrderRequest>)
class QualityCarryOrderSyncResponse(@SerializedName("data") val data: List<QualityCarryOrderRequest>)
