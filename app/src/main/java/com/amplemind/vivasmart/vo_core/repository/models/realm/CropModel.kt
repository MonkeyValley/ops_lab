package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class CropModel(
        @Index
        @SerializedName("activity_id")  var activityId: Int? = 0,
        @Index
        @SerializedName("crop_id")      var cropId: Int? = 0,
        @PrimaryKey
        var uuid: String = getUUID()
) : RealmObject(), Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(activityId)
        parcel.writeValue(cropId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CropModel> {
        override fun createFromParcel(parcel: Parcel): CropModel {
            return CropModel(parcel)
        }

        override fun newArray(size: Int): Array<CropModel?> {
            return arrayOfNulls(size)
        }
    }
}
