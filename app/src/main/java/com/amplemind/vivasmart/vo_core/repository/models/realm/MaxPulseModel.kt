package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class MaxPulseModel (
        @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("max_valvule") var max: Int? = 28,
        @SerializedName("date") var date: String? = ""
) : RealmObject()