package com.amplemind.vivasmart.features.reciba_quality.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.setBackground
import com.amplemind.vivasmart.databinding.ItemReportQualityFinishedProductBinding
import com.amplemind.vivasmart.databinding.ItemReportRecibaQualityProductBinding
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ItemReportRecibaQualityViewModel
import io.reactivex.subjects.BehaviorSubject
import kotlin.math.roundToInt

class ReportRecibaQualityAdapter constructor(val list: MutableList<ItemReportRecibaQualityViewModel>) : RecyclerView.Adapter<ReportRecibaQualityAdapter.ReportRecibaQualityViewHolder>() {

    private val onClickSubject = BehaviorSubject.create<ItemReportRecibaQualityViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportRecibaQualityViewHolder {
        val binding = ItemReportRecibaQualityProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReportRecibaQualityViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ReportRecibaQualityViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<ItemReportRecibaQualityViewModel> {
        return onClickSubject
    }

    fun isEmpty() = list.isEmpty()


    inner class ReportRecibaQualityViewHolder(private val binding: ItemReportRecibaQualityProductBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemReportRecibaQualityViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.cvUser.setOnClickListener {
                    onClickSubject.onNext(item)
                }
                binding.cvUser.setOnLongClickListener {
                    onClickSubject.onNext(item)
                    return@setOnLongClickListener true
                }

                if (item.percentInt != null) {
                    binding.TVPercentIssue.text = ""+ item.percentInt.roundToInt() +"%"
                    if (item.percentInt >= 94) {
//                        binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.backGreen))
                        binding.TVPercentIssue.setBackground(binding.root.resources.getDrawable( R.drawable.corner_reciba_status_high))
                        binding.TVPercentIssue.setTextColor(binding.root.resources.getColor(R.color.whiteCard))
                    } else if (item.percentInt > 89 && item.percentInt <= 93.99) {
//                        binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.whiteCard))
                        binding.TVPercentIssue.setBackground(binding.root.resources.getDrawable( R.drawable.corner_reciba_status_normal))
                        binding.TVPercentIssue.setTextColor(binding.root.resources.getColor(R.color.textBlue))
                    } else {
//                        binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.redAlert))
                        binding.TVPercentIssue.setBackground(binding.root.resources.getDrawable( R.drawable.corner_reciba_status_low))
                        binding.TVPercentIssue.setTextColor(binding.root.resources.getColor(R.color.whiteCard))
                    }
                }else {
//                        binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.redAlert))
                    binding.TVPercentIssue.setBackground(binding.root.resources.getDrawable( R.drawable.corner_reciba_status_low))
                    binding.TVPercentIssue.setTextColor(binding.root.resources.getColor(R.color.whiteCard))
                }
            }
        }

    }

}