package com.amplemind.vivasmart.vo_core.repository.dao.realm

import android.util.Log
import android.util.SparseArray
import com.amplemind.vivasmart.core.utils.EDITED_LOG_IN_TIMER
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.core.utils.STAGE_TYPE_ID
import com.amplemind.vivasmart.core.utils.VALIDATE_NO_SIGNATURE
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.extensions.*
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.ActivityLogUploader
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.FileUploader
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.ActivityLogDailyPayUploader
import com.amplemind.vivasmart.vo_core.utils.currentDate
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmQuery
import io.realm.RealmResults
import io.realm.Sort
import java.io.File
import java.lang.Exception
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ActivityLogsDao @Inject constructor(
        realmHolder: RealmHolder,
        private val stagesDao: StagesDao,
        private val activitiesDao: ActivitiesDao,
        private val mActivityCodesDao: ActivityCodesDao,
        private val collaboratorsDao: CollaboratorsDao,
        private val mUploadObjectDao: UploadObjectDao,
        private val mGroovesDao: GroovesDao,
        private val mActivityLogUploader: ActivityLogUploader,
        private val mActivityLogUploaderDailyPay: ActivityLogDailyPayUploader,
        private val mFileUploader: FileUploader
) : RealmDao<ActivityLogModel>(realmHolder, ActivityLogModel::class.java) {

    private enum class LogCount(val multiplier: Int) {
        ADD(1),
        SUBTRACT(-1)
    }

    private fun countForStage(stageUuid: String): Observable<Int> =
            findAllLiveAsync { realm ->
                query(realm).equalTo("activityCode.stageUuid", stageUuid)
            }
                    .map { changes ->
                        changes.data.size
                    }

    private fun countForActivityInStage(activityUuid: String, stageUuid: String): Observable<Int> =
            findAllLiveAsync { realm ->
                query(realm)
                        .equalTo("activityCode.activityUuid", activityUuid)
                        .and()
                        .equalTo("activityCode.stageUuid", stageUuid)
            }
                    .map { changes ->
                        changes.data.size
                    }


    private fun groupStageLogs(stageUuid: String, stageTypeId: Int, conditions: (RealmQuery<ActivityLogModel>) -> RealmQuery<ActivityLogModel>) =
            executeListTransactionAsync { realm ->

                val pendingLogs = mutableListOf<PendingLogsModel>()

                val realmQuery =
                        query(realm).equalTo("activityCode.stageUuid", stageUuid)
                                .and()

                var activityLogs = ArrayList<ActivityLogModel>()

                val activityLogsResult = conditions.invoke(realmQuery)
                        .sort("collaboratorUuid", Sort.ASCENDING)
                        .findAll()

                activityLogsResult.forEach { activityLog ->
                    var activityResult = realm
                            .where(ActivityModel::class.java)
                            .equalTo("id", activityLog.activityCode!!.activityId)
                            .and()
                            .equalTo("activitySubCategoryId", stageTypeId)
                            .findFirst()

                    if (activityResult != null) {
                        activityLogs.add(activityLog)
                    }
                }

                //.createSnapshot()

                var currentCollaborator = ""
                var currentPendingLog: PendingLogsModel? = null
                for (activityLog in activityLogs) {
                    if (currentCollaborator != activityLog.collaboratorUuid) {

                        currentPendingLog = PendingLogsModel(
                                collaborator = realm.copyFromRealm(activityLog.collaborator!!),
                                stage = realm.copyFromRealm(activityLog.activityCode?.stage!!),
                                payment = 0.0
                        )

                        pendingLogs.add(currentPendingLog)

                        currentCollaborator = activityLog.collaboratorUuid
                    }

                    currentPendingLog?.apply {
                        payment += activityLog.getPayment()
                    }

                }

                pendingLogs.sortBy { pendingLog ->
                    pendingLog.collaborator.name
                }

                return@executeListTransactionAsync pendingLogs

            }

    fun filterLogsByMode(pendingLogs: List<PendingLogsModel>, mode: Int): List<PendingLogsModel> {
        Realm.getDefaultInstance().use { realm ->
            var result = ArrayList<PendingLogsModel>()

            val date = getTodayDate()

            pendingLogs.forEach { log ->

                var dailyPayCollaborator = realm
                        .where(DailyPayModel::class.java)
                        .equalTo("collaboratorId", log.collaborator.id)
                        .and()
                        .equalTo("date", date)
                        .findFirst()

                when (mode) {
                    1 -> if (dailyPayCollaborator == null) result.add(log)
                    2 -> if (dailyPayCollaborator != null) result.add(log)
                }
            }

            return result
        }
    }

    fun groupSignedLogs(stageUuid: String, stageTypeId: Int) =
            groupStageLogs(stageUuid, stageTypeId) { realmQuery ->
                realmQuery
                        .equalTo("isPending", true)
                        .and()
                        .equalTo("isDone", false)
                        .and()
                        .notEqualTo("sign", "No Sign")
            }

    fun groupUnsignedLogs(stageUuid: String, stageTypeId: Int) =
            groupStageLogs(stageUuid, stageTypeId) { realmQuery ->
                realmQuery
                        .equalTo("isPending", true)
                        .and()
                        .equalTo("isDone", false)
                        .and()
                        .equalTo("sign", "No Sign")
            }

    fun findCollaboratorUnsignedLogs(stageUuid: String, collaboratorUuid: String): Single<List<ActivityLogModel>> {
        Realm.getDefaultInstance().use { realm ->

            var result = ArrayList<ActivityLogModel>()

            var activitylogs = realm
                    .where(ActivityLogModel::class.java)
                    .equalTo("collaboratorUuid", collaboratorUuid)
                    .and()
                    .equalTo("activityCode.stageUuid", stageUuid)
                    .and()
                    .equalTo("isPending", true)
                    .and()
                    .equalTo("isDone", false)
                    .and()
                    .equalTo("sign", "No Sign")
                    .sort("date", Sort.DESCENDING)
                    .findAll()

            activitylogs.forEach { activityLog ->
                var activityResult = realm
                        .where(ActivityModel::class.java)
                        .equalTo("id", activityLog.activityCode!!.activityId)
                        .and()
                        .equalTo("activitySubCategoryId", STAGE_TYPE_ID)
                        .findFirst()

                if (activityResult != null) {
                    result.add(activityLog)
                }
            }

            return Single.create<List<ActivityLogModel>> { emitter ->
                emitter.onSuccess(result)
            }
        }
    }

    fun findCollaboratorSignedLogs(stageUuid: String, collaboratorUuid: String): Single<List<ActivityLogModel>> {
        Realm.getDefaultInstance().use { realm ->

            var result = ArrayList<ActivityLogModel>()

            var activitylogs = realm
                    .where(ActivityLogModel::class.java)
                    .equalTo("collaboratorUuid", collaboratorUuid)
                    .and()
                    .equalTo("activityCode.stageUuid", stageUuid)
                    .and()
                    .equalTo("isPending", true)
                    .and()
                    .equalTo("isDone", false)
                    .and()
                    .notEqualTo("sign", "No Sign")
                    .sort("date", Sort.DESCENDING)
                    .findAll()

            activitylogs.forEach { activityLog ->
                var activityResult = realm
                        .where(ActivityModel::class.java)
                        .equalTo("id", activityLog.activityCode!!.activityId)
                        .and()
                        .equalTo("activitySubCategoryId", STAGE_TYPE_ID)
                        .findFirst()

                if (activityResult != null) {
                    result.add(activityLog)
                }
            }

            return Single.create<List<ActivityLogModel>> { emitter ->
                emitter.onSuccess(result)
            }
        }
    }

    fun findActiveLogs(stageUuid: String, activityUuid: String, func: (ActivityLogModel) -> Unit) =
            executeListTransactionAsync { realm ->
                query(realm).equalTo("activityCode.activityUuid", activityUuid)
                        .and()
                        .equalTo("activityCode.stageUuid", stageUuid)
                        .findAll()
                        .map { activityLog ->
                            if (activityLog.isValid) {
                                func.invoke(activityLog)
                            }
                            return@map activityLog
                        }
            }

    fun findActiveLogs(activityCodeUuid: String): Observable<Changes<ActivityLogModel>> =
            findAllLiveAsync { realm ->
                Log.e("activityCodeUuid", activityCodeUuid)
                query(realm).equalTo("activityCodeUuid", activityCodeUuid)
                        .and()
                        .equalTo("isDone", false)
                        .and()
                        .equalTo("isPending", false)
                //.sort("collaborator.name")
            }

    fun findActiveLogsForTimer(activityCodeUuid: String): Flowable<Changes<ActivityLogModel>> =
            findAllLive { realm ->
                query(realm).equalTo("activityCodeUuid", activityCodeUuid)
                        .and()
                        .equalTo("isDone", false)
                        .and()
                        .equalTo("isPending", false)
                        .sort("collaborator.name")
            }


    fun create(activityCode: ActivityCodeModel, collaboratorUuids: Array<String>) =
            create(activityCode.uuid, collaboratorUuids)

    fun create(activityCodeUuid: String, collaboratorUuids: Array<String>) =
            create(collaboratorUuids.size, func = { realm, index, log ->

                val auxActivityCode = realm.where(ActivityCodeModel::class.java)
                        .equalTo("uuid", activityCodeUuid)
                        .findFirst()
                        ?: throw IllegalStateException("Activity code with uuid: $activityCodeUuid does not exists")

                Log.e("ColaboratorSize", collaboratorUuids.size.toString())
                Log.e("ColaboratorIndex", index.toString())

                val collaboratorUuid = collaboratorUuids[index]
                val collaborator = realm.where(CollaboratorModel::class.java)
                        .equalTo("uuid", collaboratorUuid)
                        .findFirst()
                        ?: throw IllegalStateException("Collaborator with uuid: $collaboratorUuid does not exists")

                log.activityCode = auxActivityCode
                log.activityCodeId = auxActivityCode.id
                log.activityCodeUuid = auxActivityCode.uuid
                log.collaboratorId = collaborator.id
                log.collaboratorUuid = collaborator.uuid
                log.collaborator = collaborator

                setCollaboratorIsWorking(realm, log, true)

            }, after = { realm, logs ->
                addActivityLogCount(realm, logs, LogCount.ADD)
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.create(logs))
            })

    fun setUnits(activityLog: ActivityLogModel, quantity: Int, badQuantity: Int = 0): Single<ActivityLogModel> {

        var diff = 0
        val activityCodeUuid = activityLog.activityCodeUuid

        return update(activityLog, func = { _, log ->
            diff = quantity - log.quantity
            log.quantity = quantity
            log.badQuantity = badQuantity
        }, after = { realm, logs ->
            mActivityCodesDao.addSpacesAux(realm, activityCodeUuid, diff)
            mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
        })
    }

    fun assignGrooves(activityLogUuid: String, grooves: SparseArray<List<Int>>): Single<ActivityLogModel> {

        var diff = 0
        var activityCodeUuid = ""

        return updateByUuid(activityLogUuid, func = { realm, log ->

            mGroovesDao.deleteAllForActivityLogAux(realm, log.uuid)

            Log.e(TAG, "Creating grooves!")

            val grooveObjects = mGroovesDao.createAsRealmListAux(realm, grooves) { groove ->
                groove.apply {
                    activityCodeId = log.activityCodeId
                    activityLogId = log.id
                    this.activityCodeUuid = log.activityCodeUuid
                    this.activityLogUuid = log.uuid
                    activityCode = log.activityCode
                    activityLog = log
                }
            }

            activityCodeUuid = log.activityCodeUuid
            diff = grooveObjects.size - log.quantity
            log.quantity = grooveObjects.size
            log.grooves = grooveObjects

        }, after = { realm, logs ->
            mActivityCodesDao.addSpacesAux(realm, activityCodeUuid, diff)
            mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs, true))
        })
    }

    fun setQuantityAndTime(activityLog: ActivityLogModel, quantity: Int, timeInMinutes: Int): Single<ActivityLogModel> {

        var diff = 0
        val activityCodeUuid = activityLog.activityCodeUuid

        return update(activityLog, func = { _, log: ActivityLogModel ->
            diff = quantity - log.quantity
            log.quantity = quantity
            log.setTimeInMinutes(timeInMinutes)
        }, after = { realm, logs ->
            mActivityCodesDao.addSpacesAux(realm, activityCodeUuid, diff)
            mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
        })
    }

    fun startTimer(activityLog: ActivityLogModel) =
            update(activityLog, { _, log ->
                setTimerStatus(log, true)
            }, { realm, log ->
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(log))
            })


    fun startTimer(activityLogs: List<ActivityLogModel>) =
            update(activityLogs, { _, _, log ->
                setTimerStatus(log, true)
            },
                    after = { realm, logs ->
                        mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
                    })


    fun pauseTimer(activityLog: ActivityLogModel) =
            update(activityLog, { _, log ->
                setTimerStatus(log, false)
            }, { realm, log ->
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(log))
            })

    fun pauseTimer(activityLogs: List<ActivityLogModel>) =
            update(activityLogs, func = { _, _, log ->
                setTimerStatus(log, false)
            }, after = { realm, logs ->
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
            })

    private fun setTimerStatus(activityLog: ActivityLogModel, start: Boolean) {
        activityLog.apply {
            if (start) {
                startTime = currentDate()
                isPaused = false
            } else {
                time = getActiveTime()
                isPaused = true
            }
        }
    }

    fun remove(activityLog: ActivityLogModel): Single<ActivityLogModel> {

        val activityCodeUuid = activityLog.activityCodeUuid
        val diff = -activityLog.quantity

        return delete(activityLog, func = { realm, log ->
            addActivityLogCount(realm, log, LogCount.SUBTRACT)
            log.apply {
                mGroovesDao.deleteAllForActivityLogAux(realm, uuid)
                collaborator = null
                activityCode = null
                grooves = null
            }
        }, after = { realm, log ->
            try {
                mActivityCodesDao.addSpacesAux(realm, activityCodeUuid, diff)
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.delete(log))
            } catch (e: Exception) {
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.delete(log))
            }
        })
    }

    fun removeRosterDetail(activityLog: ActivityLogModel): Single<ActivityLogModel> {

        val activityCodeUuid = activityLog.activityCodeUuid
        val diff = -activityLog.quantity

        return delete(activityLog, func = { realm, log ->
            log.apply {
                mGroovesDao.deleteAllForActivityLogAux(realm, uuid)
                collaborator = null
                activityCode = null
                grooves = null
            }
        }, after = { realm, log ->
            try {
                mActivityCodesDao.addSpacesAux(realm, activityCodeUuid, diff)
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.delete(log))
            } catch (e: Exception) {
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.delete(log))
            }
        })
    }

    fun finish(activityLog: ActivityLogModel, signature: File? = null) =
            update(activityLog, { realm, log ->
                log.sign = when {
                    signature != null -> {
                        "sign/" + signature.name + ""
                    }
                    VALIDATE_NO_SIGNATURE -> {
                        "NULL"
                    }
                    else -> {
                        "No Sign"
                    }
                }

                if(!EDITED_LOG_IN_TIMER)
                    log.time = log.getActiveTime()
                else log.time = log.getActiveOnlyTime()
                EDITED_LOG_IN_TIMER = true
                log.isPaused = true
                log.isPending = true
                setCollaboratorIsWorking(realm, log, false)
                VALIDATE_NO_SIGNATURE = false
            }, { realm, log ->
                addActivityLogCount(realm, log, LogCount.SUBTRACT)
                try {
                    if (signature != null) {
                        mUploadObjectDao.queueForUploadAux(realm, mFileUploader.uploadSignature(signature))
                    }
                    mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(log))

                } catch (e: Exception) {
                    mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(log))
                }
            })

    fun finish(activityLogs: List<ActivityLogModel>, signature: File? = null) =
            update(activityLogs, func = { realm, _, log ->
                log.sign = if (signature != null) {
                    "sign/" + signature.name + ""
                } else {
                    "No Sign"
                }
                log.time = log.getActiveTime()
                log.isPaused = true
                log.isPending = true
                setCollaboratorIsWorking(realm, log, false)
            }, after = { realm, logs ->
                addActivityLogCount(realm, logs, LogCount.SUBTRACT)
                try {
                    if (signature != null) {
                        mUploadObjectDao.queueForUploadAux(realm, mFileUploader.uploadSignature(signature))
                    }
                    mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
                } catch (e: Exception) {
                    mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
                }
            })


    fun updatePaymentMode(activityLogs: List<ActivityLogModel>, isTraining: Boolean = false, isPractice: Boolean = false, isControl: Boolean = false) =
            update(activityLogs, func = { _, _, log ->
                log.isTraining = isTraining
                log.isPractice = isPractice
                log.isControl = isControl
            }, after = { realm, logs ->
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
            })

    fun sign(activityLogs: List<ActivityLogModel>, signature: File?, isTraining: Boolean = false, isPractice: Boolean = false, isControl: Boolean = false) =
            update(activityLogs, func = { realm, _, log ->
                log.sign = when {
                    VALIDATE_NO_SIGNATURE -> {
                        "NULL"
                    }
                    signature != null -> {
                        "sign/" + signature.name + ""
                    }
                    else -> {
                        "No Sign"
                    }
                }
                log.isTraining = isTraining
                log.isPractice = isPractice
                log.isControl = isControl
                VALIDATE_NO_SIGNATURE = false
            }, after = { realm, logs ->
                try {
                    if (signature != null) {
                        mUploadObjectDao.queueForUploadAux(realm, mFileUploader.uploadSignature(signature))
                    }
                    mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
                } catch (e: Exception) {
                    mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
                }
            })

    fun setAsDone(activityLogs: List<ActivityLogModel>) =
            update(activityLogs, func = { realm, _, log ->
                log.isPaused = true
                log.isPending = false
                log.isDone = true
                setCollaboratorIsWorking(realm, log, false)
            }, after = { realm, logs ->
                mUploadObjectDao.queueForUploadAux(realm, mActivityLogUploader.update(logs))
            })

    fun setAsDone(stage: StageModel, stageTypeId: Int): Single<List<ActivityLogModel>> =
            setAsDone(stage.uuid, stageTypeId)

    fun setAsDone(stageUuid: String, stageTypeId: Int): Single<List<ActivityLogModel>> {
        val copy = mutableListOf<ActivityLogModel>()
        return executeListTransactionAsync { realmQuerry ->

            var activityLogs = ArrayList<ActivityLogModel>()

            var activityLogsResult = query()
                    .equalTo("activityCode.stageUuid", stageUuid)
                    .and()
                    .equalTo("isPending", true)
                    .findAll()

            activityLogsResult.forEach { activityLog ->
                var activityResult = realmQuerry
                        .where(ActivityModel::class.java)
                        .equalTo("id", activityLog.activityCode!!.activityId)
                        .and()
                        .equalTo("activitySubCategoryId", stageTypeId)
                        .findFirst()

                if (activityResult != null) {
                    activityLogs.add(activityLog)
                }
            }


            activityLogs.forEach { log ->
                log.isPaused = true
                log.isPending = false
                log.isDone = true

                log.collaborator?.apply {
                    collaboratorsDao.setIsWorkingAux(realmQuerry, this, false)
                }

                val count = query(realmQuerry)
                        .equalTo("activityCodeUuid", log.activityCodeUuid)
                        .and()
                        .equalTo("isDone", false)
                        .count()

                if (count == 0L) {
                    mActivityCodesDao.closeIfFullAux(realmQuerry, log.activityCodeUuid)
                }

                copy.add(realmQuerry.copyFromRealm(log))

            }

            mUploadObjectDao.queueForUploadAux(realmQuerry, mActivityLogUploader.update(copy))

            return@executeListTransactionAsync copy
        }
    }

    fun setAsDoneDailyPay(stageUuid: String, stageTypeId: Int): Single<List<ActivityLogModel>> {
        val copy = mutableListOf<ActivityLogModel>()
        return executeListTransactionAsync { realmQuerry ->
            var result = ArrayList<ActivityLogModel>()

            val date = getTodayDate()

            var activityLogs = query()
                    .equalTo("activityCode.stageUuid", stageUuid)
                    .and()
                    .equalTo("isPending", true)
                    .findAll()

            if (STAGE_TYPE_ID == 5) {
                activityLogs.forEach { activityLog ->

                    var activityResult = realmQuerry
                            .where(ActivityModel::class.java)
                            .equalTo("id", activityLog.activityCode!!.activityId)
                            .and()
                            .equalTo("activitySubCategoryId", STAGE_TYPE_ID)
                            .findFirst()

                    if (activityResult != null) {
                        var dailyPayCollaborator = realmQuerry
                                .where(DailyPayModel::class.java)
                                .equalTo("collaboratorId", activityLog.collaboratorId)
                                .and()
                                .equalTo("date", date)
                                .findFirst()

                        if (dailyPayCollaborator != null) result.add(activityLog)
                    }
                }
            } else {
                result.addAll(activityLogs)
            }

            result.forEach { log ->
                log.isPaused = true
                log.isPending = false
                log.isDone = true

                log.collaborator?.apply {
                    collaboratorsDao.setIsWorkingAux(realmQuerry, this, false)
                }

                val count = query(realmQuerry)
                        .equalTo("activityCodeUuid", log.activityCodeUuid)
                        .and()
                        .equalTo("isDone", false)
                        .count()

                if (count == 0L) {
                    mActivityCodesDao.closeIfFullAux(realmQuerry, log.activityCodeUuid)
                }

                copy.add(realmQuerry.copyFromRealm(log))

            }

            mUploadObjectDao.queueForUploadAux(realmQuerry, mActivityLogUploaderDailyPay.update(copy))

            return@executeListTransactionAsync copy
        }
    }


    fun hasPeopleWorking(stageUuid: String, stageTypeId: Int) =
            executeTransactionAsync { realm ->

                var activityLogs = ArrayList<ActivityLogModel>()

                val activityLogsResult = query(realm).equalTo("activityCode.stageUuid", stageUuid)
                        .and()
                        .equalTo("isDone", false)
                        .and()
                        .equalTo("isPending", false)
                        .findAll()

                activityLogsResult.forEach { activityLog ->
                    var activityResult = realm
                            .where(ActivityModel::class.java)
                            .equalTo("id", activityLog.activityCode!!.activityId)
                            .and()
                            .equalTo("activitySubCategoryId", stageTypeId)
                            .findFirst()

                    if (activityResult != null) {
                        activityLogs.add(activityLog)
                    }
                }

                return@executeTransactionAsync activityLogs.size > 0
            }

    fun hasPeopleWorkingDailyPay(stageUuid: String, stageTypeId: Int) =
            executeTransactionAsync { realm ->
                var result = ArrayList<ActivityLogModel>()

                if (STAGE_TYPE_ID == 5) {
                    val activityLogs = query(realm).equalTo("activityCode.stageUuid", stageUuid)
                            .and()
                            .equalTo("isDone", false)
                            .and()
                            .equalTo("isPending", false)
                            .findAll()

                    val date = getTodayDate()

                    activityLogs.forEach { activityLog ->
                        var activityResult = realm
                                .where(ActivityModel::class.java)
                                .equalTo("id", activityLog.activityCode!!.activityId)
                                .and()
                                .equalTo("activitySubCategoryId", stageTypeId)
                                .findFirst()

                        if (activityResult != null) {
                            var dailyPayCollaborator = realm
                                    .where(DailyPayModel::class.java)
                                    .equalTo("collaboratorId", activityLog.collaboratorId)
                                    .and()
                                    .equalTo("date", date)
                                    .findFirst()

                            if (dailyPayCollaborator != null) result.add(activityLog)
                        }
                    }
                } else {
                    return@executeTransactionAsync false
                }

                return@executeTransactionAsync result.size > 0
            }

    fun hasUnsignedLogs(stageUuid: String, stageTypeId: Int) =
            executeTransactionAsync { realm ->

                var result = ArrayList<ActivityLogModel>()

                val activityLogResult = realm.where(ActivityLogModel::class.java)
                        .equalTo("activityCode.stageUuid", stageUuid)
                        .and()
                        .equalTo("isPending", true)
                        .and()
                        .equalTo("isDone", false)
                        .and()
                        .equalTo("sign", "No Sign")
                        .findAll()

                activityLogResult.forEach { activityLog ->
                    var activityResult = realm
                            .where(ActivityModel::class.java)
                            .equalTo("id", activityLog.activityCode!!.activityId)
                            .and()
                            .equalTo("activitySubCategoryId", stageTypeId)
                            .findFirst()

                    if (activityResult != null) {
                        result.add(activityLog)
                    }
                }


                return@executeTransactionAsync result.size > 0
            }

    fun hasDailyPayCollaboratorsPending(stageUuid: String, stageTypeId: Int) =
            executeTransactionAsync { realm ->

                var result = ArrayList<ActivityLogModel>()

                if (STAGE_TYPE_ID == 5) {
                    val date = getTodayDate()

                    var activityLogs =
                            realm.where(ActivityLogModel::class.java)
                                    .equalTo("isPending", true)
                                    .and()
                                    .equalTo("isDone", false)
                                    .and()
                                    .notEqualTo("sign", "No Sign")
                                    .findAll()

                    activityLogs.forEach { log ->
                        var activityResult = realm
                                .where(ActivityModel::class.java)
                                .equalTo("id", log.activityCode!!.activityId)
                                .and()
                                .equalTo("activitySubCategoryId", stageTypeId)
                                .findFirst()

                        if (activityResult != null) {
                            var dailyPayCollaborator = realm
                                    .where(DailyPayModel::class.java)
                                    .equalTo("collaboratorId", log.collaborator!!.id)
                                    .and()
                                    .equalTo("date", date)
                                    .findFirst()

                            if (dailyPayCollaborator != null) result.add(log)
                        }
                    }
                } else {
                    return@executeTransactionAsync false
                }

                return@executeTransactionAsync result.size > 0
            }


    fun hasUnsignedLogsDailyPay(stageUuid: String, stageTypeId: Int) =
            executeTransactionAsync { realm ->

                var result = ArrayList<ActivityLogModel>()

                if (STAGE_TYPE_ID == 5) {
                    val activityLogs = realm.where(ActivityLogModel::class.java)
                            .equalTo("activityCode.stageUuid", stageUuid)
                            .and()
                            .equalTo("isPending", true)
                            .and()
                            .equalTo("isDone", false)
                            .and()
                            .equalTo("sign", "No Sign")
                            .findAll()

                    val date = getTodayDate()

                    activityLogs.forEach { activityLog ->
                        var activityResult = realm
                                .where(ActivityModel::class.java)
                                .equalTo("id", activityLog.activityCode!!.activityId)
                                .and()
                                .equalTo("activitySubCategoryId", stageTypeId)
                                .findFirst()

                        if (activityResult != null) {
                            var dailyPayCollaborator = realm
                                    .where(DailyPayModel::class.java)
                                    .equalTo("collaboratorId", activityLog.collaboratorId)
                                    .and()
                                    .equalTo("date", date)
                                    .findFirst()

                            if (dailyPayCollaborator != null) result.add(activityLog)
                        }
                    }

                    Log.e("hasUnsignedLogsDailyPay", result.size.toString())
                } else {
                    return@executeTransactionAsync false
                }

                return@executeTransactionAsync result.size > 0
            }


    fun retrieveDailyReportForDate(stageUuid: String, date: String, mode: Int): Single<List<DailyReportModel>> =
            executeListTransactionAsync { realm ->

                val activityLogs = query(realm)
                        .sort("collaborator.name")
                        .distinct("collaboratorUuid")
                        .equalTo("activityCode.stageUuid", stageUuid)
                        .and()
                        .beginsWith("date", date)
                        .and()
                        .equalTo("isDone", true)
                        .findAll()

                var resultActivityLogs = ArrayList<ActivityLogModel>()


                activityLogs.forEach { log ->
                    var activityResult = realm
                            .where(ActivityModel::class.java)
                            .equalTo("id", log.activityCode!!.activityId)
                            .and()
                            .equalTo("activitySubCategoryId", STAGE_TYPE_ID)
                            .findFirst()

                    if (activityResult != null) {
                        var dailyPayCollaborator = realm
                                .where(DailyPayModel::class.java)
                                .equalTo("collaboratorId", log.collaborator!!.id)
                                .and()
                                .equalTo("date", date)
                                .findFirst()

                        when (mode) {
                            1 -> if (dailyPayCollaborator == null) resultActivityLogs.add(log)
                            2 -> if (dailyPayCollaborator != null) resultActivityLogs.add(log)
                            else -> resultActivityLogs.add(log)
                        }
                    }
                }

                val results = resultActivityLogs.map { activityLog ->
                    val collabLogs = query(realm)
                            .equalTo("activityCode.stageUuid", stageUuid)
                            .and()
                            .equalTo("collaboratorUuid", activityLog.collaboratorUuid)
                            .and()
                            .beginsWith("date", date)
                            .and()
                            .equalTo("isDone", true)
                            .findAll()

                    val total = collabLogs.sumByDouble { collabLog ->
                        collabLog.getPayment()
                    }

                    val code = activityLog.collaborator?.employeeCode ?: "S/C"
                    val name = activityLog.collaborator?.name ?: "S/N"

                    return@map DailyReportModel(code, name, total)

                }

                return@executeListTransactionAsync results ?: listOf()
            }


    private fun addActivityLogCount(realm: Realm, activityLog: ActivityLogModel, logCount: LogCount) =
            addActivityLogCount(realm, listOf(activityLog), logCount)

    private fun addActivityLogCount(realm: Realm, activityLogs: List<ActivityLogModel>, logCount: LogCount) {
        if (activityLogs.isNotEmpty()) {
            val stage = requireNotNull(activityLogs.first().activityCode?.stage)
            val activity = requireNotNull(activityLogs.first().activityCode?.activity)

            val count = activityLogs.size * logCount.multiplier

            activitiesDao.addLogCountAux(realm, activity, stage, count)
            stagesDao.addLogCountAux(realm, stage, count, activity.activitySubCategoryId)
        }
    }

    private fun setCollaboratorIsWorking(realm: Realm, activityLog: ActivityLogModel, isWorking: Boolean) {
        activityLog.collaborator?.apply {
            collaboratorsDao.setIsWorkingAux(realm, this, isWorking)
        }
    }

    fun getTodayDate(): String {
        val calendar = Calendar.getInstance()
        val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        return ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
    }


}