package com.amplemind.vivasmart.vo_core.repository.remote

import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectSingleResponse
import io.reactivex.Single
import retrofit2.http.*

interface FertirriegoApi {

    //https://dev.vivasmart.com.mx/v2/fertigation/lots
    @GET("/v2/fertigation/lots")
    @Headers("Content-Type: application/json")
    fun getLotFertirriego(
            @Header("Authorization")    authentication: String
            ): Single<ObjectResponse<LotFertirriegoModel>>


    //https://dev.vivasmart.com.mx/v2/fertigation/valves/11
    @GET("/v2/fertigation/valves/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getValveLotFertirriego(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<ObjectSingleResponse<ValvesPerLotFertirriegoModel>>


    //https://dev.vivasmart.com.mx/v2/fertigation/monitoring/valve/lastweek/11
    @GET("/v2/fertigation/monitoring/valve/lastweek/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getValesPerWeekFertirriego(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<ObjectSingleResponse<ValvesPerWeekFertirriegoModel>>


    //https://dev.vivasmart.com.mx/v2/fertigation/monitoring/valve/lastweek/phce/11
    @GET("/v2/fertigation/monitoring/valve/lastweek/phce/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getPHCEPerWeekFertirriego(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<ObjectSingleResponse<PHCEPerWeekFertirriegoModel>>


    //https://dev.vivasmart.com.mx/v2/fertigation/monitoring/pulse/lastweek/11
    @GET("/v2/fertigation/monitoring/pulse/lastweek/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getPulsePerWeekFertirriego(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<ObjectSingleResponse<PulsesPerWeekFertirriegoModel>>


    //https://dev.vivasmart.com.mx/v2/message?id=5
    @GET("/v2/message")
    @Headers("Content-Type: application/json")
    fun getMessageByIdApi(
            @Header("Authorization")        authentication: String,
            @Query("id")                    messageId: Int = 0
    ): Single<ObjectResponse<MessageModel>>

}
