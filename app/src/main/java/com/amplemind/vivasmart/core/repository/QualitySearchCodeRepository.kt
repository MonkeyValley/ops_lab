package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.remote.QualitySearchCodeApi
import com.amplemind.vivasmart.core.repository.response.CarryOrdersResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class QualitySearchCodeRepository @Inject constructor(private val api : QualitySearchCodeApi,
                                                           private val preferences: UserAppPreferences) {

    @Inject
    lateinit var authorization: ApiAuthorization

    fun searchCarryOrder(folio: String): Observable<CarryOrdersResponse> {
        return api.searchCarryOrder(authorization.getAuthToken(preferences.token), folio)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

}
