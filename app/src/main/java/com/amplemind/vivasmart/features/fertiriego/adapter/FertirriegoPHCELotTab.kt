package com.amplemind.vivasmart.features.fertiriego.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.util.SparseArray
import com.amplemind.vivasmart.features.fertiriego.fragment.PHCEFragment
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService

class FertirriegoPHCELotTab (fm: FragmentManager, private val date: String, private val lotId: Int, private val mode: String) : FragmentPagerAdapter(fm) {

    private val registeredFragments = SparseArray<Fragment>()

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                val fragment = PHCEFragment().newInstance(date, lotId, "T1", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            1 -> {
                val fragment = PHCEFragment().newInstance(date, lotId, "T2", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            2 -> {
                val fragment = PHCEFragment().newInstance(date, lotId, "T3", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            3 -> {
                val fragment = PHCEFragment().newInstance(date, lotId, "T4", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            4 -> {
                val fragment = PHCEFragment().newInstance(date, lotId, "T5", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            else -> {
                val fragment = PHCEFragment().newInstance(date, lotId, "T6", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
        }
    }

    override fun getCount(): Int {
        return FertirriegoService().getTableCountPerLot(lotId, "Soil")
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Tabla 1"
            1 -> "Tabla 2"
            2 -> "Tabla 3"
            3 -> "Tabla 4"
            4 -> "Tabla 5"
            else -> "Tabla 6"
        }
    }

    fun getRegisteredFragment(position: Int): Fragment? {
        return registeredFragments[position]
    }

}
