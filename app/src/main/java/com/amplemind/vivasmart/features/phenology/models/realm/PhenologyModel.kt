package com.amplemind.vivasmart.features.phenology.models.realm

import com.amplemind.vivasmart.features.phenology.models.local.PhenologyClusterItem
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVarItem
import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class PhenologyModel(
        @PrimaryKey @SerializedName("revision_id") var revisionId: Int = 0,
        @SerializedName("plant_list") var plantList: RealmList<PhenologyPlantsModel>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class PhenologyPlantsModel(
        @SerializedName("revision_id") var revisionId: Int = 0,
        @SerializedName("plantName") var plantName: String = "",
        @SerializedName("valve") var valve: Int? = 0,
        @SerializedName("vars") var vars: RealmList<PhenologyVarModel>? = RealmList(),
        @SerializedName("cluster") var cluster: RealmList<PhenologyClusterModel>? = RealmList(),
        @PrimaryKey  @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class PhenologyVarModel(
        @SerializedName("phenology_var_id") var phenology_var_id: Int? = 0,
        @SerializedName("medition") var medition: Double? = 0.0,
        @SerializedName("phenological_state") var phenological_state: String? = null,
        @PrimaryKey override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class PhenologyClusterModel(
        @SerializedName("button_status") var button_status: Int = 0,
        @SerializedName("flowering_status") var flowering_status: Int = 0,
        @SerializedName("cuaje_status") var cuaje_status: Int = 0,
        @SerializedName("cluster_no") var cluster_no: Int = 0,
        @SerializedName("fruit_no") var fruit_no: Int = 0,
        @PrimaryKey override var uuid: String = getUUID()
) : RealmObject(), Identifiable


fun convetPhenologyVarModel(vars: List<PhenologyVarItem>): RealmList<PhenologyVarModel> {
    Realm.getDefaultInstance().use {
        val result = RealmList<PhenologyVarModel>()
        for (item in vars) {
            result.add(PhenologyVarModel(item.phenology_var_id, item.medition, item.phenological_state))
        }
        return result
    }
}

fun convetPhenologyClusterModel(cluster: List<PhenologyClusterItem>): RealmList<PhenologyClusterModel> {
    Realm.getDefaultInstance().use {
        val result = RealmList<PhenologyClusterModel>()
        for (item in cluster) {
            result.add(PhenologyClusterModel(item.button_status, item.flowering_status, item.cuaje_status, item.cluster_no, item.fruit_no))
        }
        return result
    }
}