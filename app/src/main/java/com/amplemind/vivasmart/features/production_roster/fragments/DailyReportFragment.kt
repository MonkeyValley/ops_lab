package com.amplemind.vivasmart.features.production_roster.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_roster.adapters.DailyReportAdapter
import com.amplemind.vivasmart.features.production_roster.viewModels.DailyReportViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.utils.currentDateInMillis
import kotlinx.android.synthetic.main.fragment_report_close_day.*
import javax.inject.Inject


class DailyReportFragment : BaseFragment() {

    companion object {

        val TAG = DailyReportFragment::class.java.simpleName

        val PARAM_STAGE_UUID = "$TAG.StageUuid"

        @JvmStatic
        fun newInstance(stageUuid: String) = DailyReportFragment().apply {
            arguments = Bundle().apply {
                putString(PARAM_STAGE_UUID, stageUuid)
            }
        }
    }

    init {
        argumentsNeeded = true
    }

    @Inject
    lateinit var mViewModel: DailyReportViewModel

    private lateinit var mAdapter: DailyReportAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.setDate()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_report_close_day, container, false)


        return view
    }

    override fun onStart() {
        super.onStart()
        loadData()
    }

    private fun displayHumanDate() {
        editText.text = mViewModel.humanDate
    }

    private fun showNoIncidents(show: Boolean) {
        no_incidents.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun readArguments(args: Bundle) {
        super.readArguments(args)

        mViewModel.stageUuid = requireNotNull(args.getString(PARAM_STAGE_UUID))
    }

    override fun setUpUI() {
        super.setUpUI()

        displayHumanDate()

        initRecyclerView()
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()

        editText.setOnClickListener {

            val mDatePicker = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { datepicker, selectedYear, selectedMonth, selectedDay ->
                mViewModel.setDate(selectedYear, selectedMonth, selectedDay)
                mAdapter.date = mViewModel.mDate
                displayHumanDate()
                loadData()
            }, mViewModel.year, mViewModel.month, mViewModel.day)

            mDatePicker.datePicker.maxDate = currentDateInMillis()
            mDatePicker.setTitle("Selecciona una fecha")
            mDatePicker.show()
        }
    }

    override fun loadData() {
        mViewModel.loadStage()
                .subscribe { stage ->
                    mEventBus.send(SetLotNameEvent(stage))
                }
                .addTo(subscriptions)

        allReport()
    }

    fun allReport(){
        mViewModel.loadReport(0)
                .subscribe({data ->
                    showNoIncidents(data?.isEmpty() == true)
                    mAdapter.setItems(data)
                }, {
                    onError(it.localizedMessage)
                })
                .addTo(subscriptions)
    }

    fun onlyReport(){
        mViewModel.loadReport(1)
                .subscribe({data ->
                    showNoIncidents(data?.isEmpty() == true)
                    mAdapter.setItems(data)
                }, {
                    onError(it.localizedMessage)
                })
                .addTo(subscriptions)
    }

    fun onlyDailyPay(){
        mViewModel.loadReport(2)
                .subscribe({data ->
                    showNoIncidents(data?.isEmpty() == true)
                    mAdapter.setItems(data)
                }, {
                    onError(it.localizedMessage)
                })
                .addTo(subscriptions)
    }

    private fun initRecyclerView() {
        mAdapter = DailyReportAdapter()
        close_day_rv_report.adapter = mAdapter
        close_day_rv_report.layoutManager = LinearLayoutManager(context!!)
    }

    data class SetLotNameEvent(
            val stage: StageModel
    )

}
