package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class RosterLineModel(
        @SerializedName("id") @ColumnInfo(name = "line_id") @PrimaryKey(autoGenerate = false) val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("packinghouse_id") val packinghouseId: Int,
        @SerializedName("actual_stage") var actual_stage : Int?,
        @SerializedName("crops") val crops : List<CropLine>
)