package com.amplemind.vivasmart.feature.packaging_quality

import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.nhaarman.mockito_kotlin.mock
import org.junit.Before
import org.junit.Test

class PackagingQualityRepositoryTest {

    lateinit var packagingRepository : PackagingQualityRepository

    @Before
    fun setup(){
        packagingRepository = PackagingQualityRepository(mock())
    }

    @Test
    fun `test data quality packages`(){
        packagingRepository.getDataQualityPackages()
                .test()
                .assertValue {
                    it.isNotEmpty()
                }
    }

}