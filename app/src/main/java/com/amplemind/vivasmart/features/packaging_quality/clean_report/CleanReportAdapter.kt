package com.amplemind.vivasmart.features.packaging_quality.clean_report

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemCardCleanReportBinding
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Created by
 *          amplemind on 7/10/18.
 */
class CleanReportAdapter(context: Context?) : ArrayAdapter<ItemCleanViewModel>(context!!, R.layout.item_card_clean_report) {

    private var list = mutableListOf<ItemCleanViewModel>()
    private val clickSubject = PublishSubject.create<String>()

    fun setData(data: List<ItemCleanViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val binding: ItemCardCleanReportBinding

        if (convertView == null) {
            val inflate = ItemCardCleanReportBinding.inflate(LayoutInflater.from(parent!!.context), parent, false)
            binding = inflate
            view = inflate.root
            view.tag = binding
        } else {
            binding = convertView.tag as ItemCardCleanReportBinding
            view = convertView
        }


        binding.setVariable(BR.viewModel, getItem(position))
        binding.executePendingBindings()

        return view
    }

    val clickEvent: Observable<String> = clickSubject

    override fun getItem(position: Int): ItemCleanViewModel {
        val mPosition = if (position < 0) list.size - 1 else position
        val item = list[mPosition]
        clickSubject.onNext(item.name)
        return item
    }


    override fun getCount(): Int {
        return list.size
    }


}