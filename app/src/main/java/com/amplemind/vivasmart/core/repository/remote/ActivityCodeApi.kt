package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.core.repository.model.ActivityCodeModel
import com.amplemind.vivasmart.core.repository.model.CollaboratorsReponseActivities
import com.amplemind.vivasmart.core.repository.model.GroovesAssingReponse
import com.amplemind.vivasmart.core.repository.model.GroovesResultReponse
import com.amplemind.vivasmart.core.repository.request.*
import io.reactivex.Observable
import retrofit2.http.*
import java.util.*

interface ActivityCodeApi {

    @GET("v2/activity_code_totals/{stage_id}/{activity_id}")
    @Headers("Content-Type: application/json")
    fun getCodes(
            @Header("Authorization") authentication: String,
            @Path("activity_id") activityId: Int,
            @Path("stage_id") stageId: Int
    ): Observable<ArrayList<ActivityCodeModel>>

    @POST("v2/activity_code")
    @Headers("Content-Type: application/json")
    fun createCode(
            @Header("Authorization") authentication: String,
            @Body body: ActivityCodeRepository.CreateActivityCodeRequest
    ): Observable<ActivityCodeModel>

    @GET("/v2/activity_code/collaborator")
    @Headers("Content-Type: application/json")
    fun getActivitiesByCode(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "collaborator.activity_logs",
            @Query("activity_code_id") activityCode: Int,
            @Query("is_done") isDone: Boolean = false,
            @Query("__logic") logic: String = "AND"
    ): Observable<CollaboratorsReponseActivities>

    @POST("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun finishCollaboratorGrooves(
            @Header("Authorization") authentication: String,
            @Body body: ActivityCodeRepository.FinishCollaboratorGrooveRequest
    ): Observable<GroovesResultReponse>

    @POST("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun finishCollaboratorUnit(
            @Header("Authorization") authentication: String,
            @Body body: ActivityCodeRepository.FinishCollaboratorUnitRequest
    ): Observable<GroovesResultReponse>

    @PUT("/v2/activity_code/collaborator")
    @Headers("Content-Type: application/json")
    fun finishCollaborator(
            @Header("Authorization") authentication: String,
            @Body body: FinishCollaboratorRequest
    ): Observable<Any>

    @PUT("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun updateCollaborator(
            @Header("Authorization") authentication: String,
            @Body body: UpdateCollaboratorRequest
    ): Observable<Any>


    @PUT("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun updateCollaboratorUnit(
            @Header("Authorization") authentication: String,
            @Body body: UpdateCollaboratorUnityRequest
    ): Observable<Any>

    @PUT("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun addMoreCollaboratorUnit(
            @Header("Authorization") authentication: String,
            @Body body: AddMoreCollaboratorUnityRequest
    ): Observable<Any>


    @GET("/v2/activity_code/groove")
    @Headers("Content-Type: application/json")
    fun getGroovesAssing(
            @Header("Authorization") authentication: String,
            @Query("activity_log.activity_code_id") activity_code_id: Int,
            @Query("embed") embed: String = "activity_log",
            @Query("limit") limit: Int = 1000
    ): Observable<GroovesAssingReponse>

    @PUT("/v2/activity_code/collaborator/restart")
    @Headers("Content-Type: application/json")
    fun restartTimerCollaborators(
            @Header("Authorization") authentication: String,
            @Body body: RestartTimerCollaboratorRequest
    ): Observable<Any>

    @PUT("/v2/activity_code/collaborator")
    @Headers("Content-Type: application/json")
    fun pauseTimerCollaborators(
            @Header("Authorization") authentication: String,
            @Body body: PauseTimerCollaboratorRequest
    ): Observable<Any>

    @PUT("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun finishCollaboratorSign(
            @Header("Authorization") authentication: String,
            @Body body: UpdateCollaboratorSignRequest
    ): Observable<Any>

    @PUT("/v2/packingline_paysheet/packinglinelog")
    @Headers("Content-Type: application/json")
    fun finishCollaboratorSignLines(
            @Header("Authorization") authentication: String,
            @Body body: UpdateCollaboratorSignRequest
    ): Observable<Any>

    @DELETE("/v2/activity_code/collaborator/{id}/{activityLogId}")
    @Headers("Content-Type: application/json")
    fun deleteCollaborator(
            @Header("Authorization") authentication: String,
            @Path("id") id: Int,
            @Path("activityLogId") activityLogId: Long
    ): Observable<Any>

    @PUT("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun updateGrooves(
            @Header("Authorization") authToken: String,
            @Body body: ActivityCodeRepository.GroovesUpdate
    ): Observable<Any>

    @POST("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun assignGrooves(
            @Header("Authorization") authToken: String,
            @Body body: ActivityCodeRepository.AssignGroovesRequest
    ): Observable<GroovesResultReponse>

    @PUT("/v2/packingline_paysheet/packinglinelog")
    @Headers("Content-Type: application/json")
    fun updateCollaboratorUnitLines(
            @Header("Authorization") authToken: String,
            @Body body: UpdateCollaboratorUnityRequest
    ): Observable<Any>



}