package com.amplemind.vivasmart.features.waste_control.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.WasteControlRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import javax.inject.Inject


class WasteControlTestViewModel @Inject constructor(
        private val repository: WasteControlRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

}