package com.amplemind.vivasmart.features.phenology

import android.app.ProgressDialog
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.custom_views.CustomViewPager
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.features.phenology.PhenologyVarietiesPlantsActivity.Companion.RevisionPost
import com.amplemind.vivasmart.features.phenology.adapters.VarietiesPlantsPhenologyPagerAdapter
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyVarietiesPlantsFragment
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyPlant
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyPost
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyPostRevisionResponse
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVars
import com.amplemind.vivasmart.features.phenology.viewmodel.PhenologyVarietiesPlantFragmentViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.PhenologyVarietiesPlantsActivityViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesTablesPhenologyItemViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.plugins.RxJavaPlugins.onError
import kotlinx.android.synthetic.main.activity_phenology_varieties_plants.*
import javax.inject.Inject

class PhenologyVarietiesPlantsActivity : BaseActivityWithFragment() {
    @Inject
    lateinit var viewModel: PhenologyVarietiesPlantsActivityViewModel
    @Inject
    lateinit var viewModelPlant: PhenologyVarietiesPlantFragmentViewModel
    lateinit var view: View

    var progressDialog: ProgressDialog? = null

    companion object {
        const val TAG = "PhenologyVarietiesPlantsActivity"
        var stage_id: Int = 0
        var lot_id: Int = 0
        var lot_name: String = ""
        var variety_id: Int = 0
        var revision_id: Int = 0
        var indexPrincipal: Int = 0
        var tabPosition: Int = 0
        var variety_name = ""
        var table_no: String = ""
        var groove_from_to: String = ""
        var has_cluster: Boolean = false
        var sampling: Int = 0
        var isNew: Boolean = true

        lateinit var table: VarietiesTablesPhenologyItemViewModel
        var checkList = ArrayList<Boolean>()
        var vars = ArrayList<PhenologyVars>()
        var listPhenologyPlant = ArrayList<PhenologyPlant>()

        lateinit var RevisionPost: PhenologyPost
        lateinit var phenologyRevisionPost: PhenologyPost
        lateinit var tabLayout: TabLayout
        lateinit var pageAdapter: VarietiesPlantsPhenologyPagerAdapter
        lateinit var pagerPlants: CustomViewPager

        fun setupTabsMinusOne() {
            if(indexPrincipal > checkList.size){
                indexPrincipal = sampling -1
            }
            //checkList[indexPrincipal] = true

            Log.e("Index + tap", "$indexPrincipal - $tabPosition")
            if(indexPrincipal == tabPosition){
                checkList[indexPrincipal] = true
                indexPrincipal++
            }
            setInicioTab()
        }

        fun setInicioTab() {
            tabLayout.getTabAt(indexPrincipal)?.select()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phenology_varieties_plants)
        view = this.findViewById<View>(android.R.id.content).rootView
        tabPosition = 0

        checkList.clear()
        stage_id = intent.getIntExtra("stage_id", 0)
        lot_id = intent.getIntExtra("lot_id", 0)
        lot_name = intent.getStringExtra("lot_name")
        variety_id = intent.getIntExtra("variety_id", 0)
        variety_name = intent.getStringExtra("variety_name")
        has_cluster = intent.getBooleanExtra("has_cluster", false)
        isNew = intent.getBooleanExtra("isNew", true)

        table = Gson().fromJson(intent.getStringExtra("table"), VarietiesTablesPhenologyItemViewModel::class.java)
        vars = Gson().fromJson(intent.getStringExtra("vars"), object : TypeToken<ArrayList<PhenologyVars>?>() {}.type)

        table_no = table.table_no
        groove_from_to = (table.groove_from_to)
        revision_id = table.revision_id!!
        Log.e("revision_id", revision_id.toString())

        phenologyRevisionPost = PhenologyPost(stage_id, variety_id, table_no.toInt(), "", table.groove_from!!, table.groove_to!!, table.sampling.toInt(), null)

        sampling = table.sampling.toInt()
        tabLayout = findViewById(R.id.tabLayoutPlants)
        pageAdapter = VarietiesPlantsPhenologyPagerAdapter(supportFragmentManager)
        pagerPlants = findViewById(R.id.pager_plants)
        setupToolbar()
        setupView()

        subcribers()
    }

    fun subcribers(){
        viewModelPlant.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModelPlant.onSuccessRequest_postRevision().subscribe(this::getPhenologyVersionResponse).addTo(subscriptions)
    }

    private fun setupView() {
        setupTabs()
        initProgressDialog()
        pagerPlants.adapter = pageAdapter
        pagerPlants.offscreenPageLimit = 5
        pagerPlants.swipeEnabled = false
        pagerPlants.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}
            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabSelected(tab: TabLayout.Tab?) {
                Log.e("tab!!.position", tab!!.position.toString())
                Log.e("indexPrincipal", indexPrincipal.toString())

//                pagerPlants.currentItem = tab.position
//                indexPrincipal = tab.position

                if(tab.position > indexPrincipal){
                    onSNACK(view, "Envíe la planta seleccionada para realizar las posteriores")
                    setInicioTab()
                }
                else {
                    pagerPlants.currentItem = tab.position
                    tabPosition = tab.position
                }
            }
        })

        callTabFragment()
    }

    fun callTabFragment() {
        pagerPlants.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                val fragment: Fragment = pageAdapter.getItem(position)
                (fragment as PhenologyVarietiesPlantsFragment).loadData(false)
            }
        })
    }

    fun onSNACK(view: View, message: String) {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT).setAction("Action", null).setActionTextColor(Color.BLUE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(Color.LTGRAY)
        val textView = snackbarView.findViewById(R.id.snackbar_text) as TextView
        textView.setTextColor(Color.BLUE)
        textView.textSize = 18f
        textView.gravity = Gravity.CENTER
        snackbar.show()
    }

    private fun setupTabs() {
        tabLayout.removeAllTabs()
        // New Revision
        if (table.revision_id == 0) {
            for (i in 0 until table.sampling.toInt()) {
                val index = i + 1

                checkList.add(false)

                tabLayout.addTab(tabLayout.newTab().setText("Planta $index"))
                pageAdapter.addFragment(PhenologyVarietiesPlantsFragment.newInstance(index, Gson().toJson(vars), Gson().toJson(phenologyRevisionPost), has_cluster, false))
                pageAdapter.notifyDataSetChanged()
            }
        } else {
            // continue with revision
            if (table.revised_plants  < table.sampling.toInt()) {
                for (i in table.revised_plants until table.sampling.toInt()) {
                    val index = i + 1

                    checkList.add(false)

                    tabLayout.addTab(tabLayout.newTab().setText("Planta $index"))
                    pageAdapter.addFragment(PhenologyVarietiesPlantsFragment.newInstance(index, Gson().toJson(vars), Gson().toJson(phenologyRevisionPost), has_cluster, false))
                    pageAdapter.notifyDataSetChanged()
                }

            }
            // terminar la revision con el comentario y firma
            else {
                tabLayout.addTab(tabLayout.newTab().setText("Finalizar revision"))
                pageAdapter.addFragment(PhenologyVarietiesPlantsFragment.newInstance(0, Gson().toJson(vars), Gson().toJson(phenologyRevisionPost), has_cluster, true))
                pageAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_phenology_varieties
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = variety_name
        action_bar_title.text = "$lot_name / Tabla: $table_no / Surco: $groove_from_to"
        action_bar_title.setOnLongClickListener {
            Toast.makeText(this, "Revision: " + table.revision_id, Toast.LENGTH_SHORT).show()
            true
        }
    }

    override fun onBackPressed() {
        indexPrincipal = 0
        tabPosition = 0
        Log.e("PlantActivityBackPress", "BackPress")
        showProgressDialog(true)
        finishActivity()
    }

    private fun finishActivity(){
        if(revision_id == 0){
            if (ifConected()) {
                try {
                    viewModelPlant.postPhenologyRevision(RevisionPost)
                } catch (e: Exception){
                    showProgressDialog(false)
                    finish()
                }
            }
        } else {
            if (viewModelPlant.postPlants(revision_id)) {
                showProgressDialog(false)
                finish()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                Log.e("PlantActivityToolBar", "BackPress")
                showProgressDialog(true)
                finishActivity()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    fun initProgressDialog() {
            progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(getString(R.string.app_name))
            progressDialog!!.setMessage(getString(R.string.server_loading))
            progressDialog!!.setCancelable(false)
    }

    fun showProgressDialog(show: Boolean) {
        if (show) progressDialog!!.show() else progressDialog!!.dismiss()
    }

    fun ifConected(): Boolean {
        return if (hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

    fun getPhenologyVersionResponse(respose: PhenologyPostRevisionResponse) {
        Log.d("getPhenologyVersionRes", Gson().toJson(respose))
        revision_id = respose.id
        listPhenologyPlant.forEach {
            viewModelPlant.addPhenologyPlant(it.plantName!!, revision_id, it.plant!!)
        }
        finishActivity()
    }
}