package com.amplemind.vivasmart.features.crop_validation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.features.crop_validation.adapter.CropValidationCarryOrderAdapter
import com.amplemind.vivasmart.features.crop_validation.viewmodel.CropValidationActivityViewModel
import com.amplemind.vivasmart.features.crop_validation.viewmodel.ItemCarryOrderHarvestViewModel
import com.github.badoualy.datepicker.DatePickerTimeline
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_crop_validation.*
import kotlinx.android.synthetic.main.activity_crop_validation.timeline
import kotlinx.android.synthetic.main.activity_report_quality_finished_product.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.*
import javax.inject.Inject

class CropValidationActivity : BaseActivity() {
    @Inject
    lateinit var viewModel: CropValidationActivityViewModel
    var fecha : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crop_validation)

        setupToolbar()
        addSubscribers()
        loadDateTime()
        loadReportList()
    }

    fun ifConected(): Boolean{
        return if(hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

    override fun onResume() {
        super.onResume()
        loadReportList()
    }

    private fun loadReportList() {
        viewModel.getCarryOrderHarvest(fecha!!)
    }

    private fun loadDateTime(){
        val date = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        var cal_begin = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        cal_begin.add(Calendar.DATE, -15)

        Log.e("date", "" + date.get(Calendar.DST_OFFSET))
        Log.e("cal_begin", "" + date.get(Calendar.DST_OFFSET))

        if(date.get(Calendar.DST_OFFSET) != 0){
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) +"-"+ cal_begin.get(Calendar.MONTH)+"-"+cal_begin.get(Calendar.DAY_OF_MONTH)+1)
            timeline.setLastVisibleDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH)+1)
            timeline.setSelectedDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH)+1)
        } else {
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH))
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) +"-"+ cal_begin.get(Calendar.MONTH)+"-"+cal_begin.get(Calendar.DAY_OF_MONTH))
            timeline.setLastVisibleDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH))
            timeline.setSelectedDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH))
        }

        val sti =  date.get(Calendar.YEAR).toString()
        fecha = ( ( date.get(Calendar.YEAR)).toString() + "-" +(date.get(Calendar.MONTH) + 1).toString() + "-" + (date.get(Calendar.DAY_OF_MONTH)).toString() )

        val stringYear = sti.subSequence(2, 4)
        timeline.setDateLabelAdapter {
            calendar,
            index -> Integer.toString(calendar[Calendar.MONTH] + 1) + "/" + stringYear
        }
        timeline.onDateSelectedListener = DatePickerTimeline.OnDateSelectedListener { year, month, day, index ->
            Log.d("dateSelected", "year:$year month:$month day:$day index: $index")
            fecha = ((year).toString() + "-" + (month + 1).toString() + "-" + (day).toString())
            if (ifConected()) {
                loadReportList()
            }
        }
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_crop_validation
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.crop_validation_title)
        lbl_track_title.text = "/ Calidad / Empaque / Validación de cosecha /"
        lbl_track_title.setTextColor(this.resources.getColor(R.color.white))
        icon_track_title.setColorFilter(this.resources.getColor(R.color.white))

    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest().subscribe(this::updateList).addTo(subscriptions)
    }

    private fun updateList(list: List<ItemCarryOrderHarvestViewModel>) {
        Log.d("listaCrop", Gson().toJson(list))

        val adapter = CropValidationCarryOrderAdapter(list.toMutableList())
        rv_crop_validation.hasFixedSize()
        rv_crop_validation.layoutManager = LinearLayoutManager(this)
        rv_crop_validation.itemAnimator = DefaultItemAnimator()
        rv_crop_validation.adapter = adapter

        adapter.onClick().subscribe(this::onClickItem).addTo(subscriptions)
    }

    private fun onClickItem(item: ItemCarryOrderHarvestViewModel) {
        if(ifConected()) {
            Log.e("Cero: ", Gson().toJson(item))
            intent = Intent(this, CropIssuesValidationActivity().newInstance(item)::class.java)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==200) {
            if (resultCode == Activity.RESULT_OK) {
                loadReportList()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val TAG = "CropValidationAct"
        const val CROP = "crop"
    }
}
