package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm
import io.realm.FieldAttribute

class Version_9 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        createActivityLogCount(realm)
        addActivityLogCountToStage(realm)
        addActivityLogCountToActivity(realm)
    }

    override fun drop(realm: DynamicRealm) {
        dropActivityLogCount(realm)
        dropActivityLogCountToStage(realm)
        dropActivityLogCountToActivity(realm)
    }

    private fun createActivityLogCount(realm: DynamicRealm) {
        realm.schema.create("ActivityLogCountModel")
                .addField("stageUuid", String::class.java, FieldAttribute.INDEXED)
                .addField("activityUuid", String::class.java, FieldAttribute.INDEXED)
                .addField("count", Int::class.java)
    }

    private fun addActivityLogCountToStage(realm: DynamicRealm) {
        realm.schema.get("StageModel")
                ?.addField("activityLogCount", Int::class.java)
    }

    private fun addActivityLogCountToActivity(realm: DynamicRealm) {
        realm.schema.get("ActivityModel")
                ?.addRealmListField("activityLogCounts", realm.schema.get("ActivityLogCountModel")!!)
    }

    private fun dropActivityLogCount(realm: DynamicRealm) {
        realm.schema.remove("ActivityLogCountModel")
    }

    private fun dropActivityLogCountToStage(realm: DynamicRealm) {
        realm.schema.get("StageModel")
                ?.removeField("activityLogCount")
    }

    private fun dropActivityLogCountToActivity(realm: DynamicRealm) {
        realm.schema.get("ActivityModel")
                ?.removeField("activityLogCounts")
    }

}