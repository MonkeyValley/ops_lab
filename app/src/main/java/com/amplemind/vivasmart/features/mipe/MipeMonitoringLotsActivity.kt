package com.amplemind.vivasmart.features.mipe


import android.os.Bundle
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.mipe.fragment.MipeLotFragment
import kotlinx.android.synthetic.main.activity_mipe_activities.*
import kotlinx.android.synthetic.main.progress_track_component.*

class MipeMonitoringLotsActivity : BaseActivityWithFragment() {

    companion object {
        val TAG: String =  MipeMonitoringLotsActivity::class.java.simpleName
        val PARAM_TITTLE = "$TAG.Tittle"
        val PARAM_TYPE = "$TAG.Type"
    }

    var tittle = ""
    var type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mipe_activities)
        readArguments()
        setupToolbar()
        callFragmentMenuActivities()
    }

    private fun readArguments() {
        tittle = requireNotNull(intent.getStringExtra(PARAM_TITTLE))
        type = requireNotNull(intent.getStringExtra(PARAM_TYPE))
    }

    private fun callFragmentMenuActivities() {
        val support = supportFragmentManager
        val commit = support.beginTransaction()
                .replace(R.id.container, MipeLotFragment().newInstance(tittle, type))
                .addToBackStack("PhenologyFragment")
                .commit()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_mipe
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = tittle

        when(type){
            "1"-> lbl_track_title.text = "/ MIPE / Monitoreo /"
            "2"-> lbl_track_title.text = "/ Operaciones / Rangos de producción /"
        }
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
