package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.model.CarryOrderCondensedModel

class CarryOrderCondensedViewModel(model: CarryOrderCondensedModel) {

    val id = model.id
    val folio = model.folio
    val lotName = model.lotName
    val lots = getLotsToString(model.tables)

    private fun getLotsToString(tables : List<Int>) : String {
        var tableFormat = ""
        tables.forEach { id ->
            tableFormat += if (tableFormat.isEmpty()) "$id" else ",$id"
        }
        return tableFormat
    }

}
