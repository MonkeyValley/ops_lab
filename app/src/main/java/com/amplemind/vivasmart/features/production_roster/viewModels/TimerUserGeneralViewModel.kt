package com.amplemind.vivasmart.features.production_roster.viewModels

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import io.reactivex.subjects.BehaviorSubject

class TimerUserGeneralViewModel (private val view: View) : ChildViewHolder(view) {

    var viewForeground: ConstraintLayout? = null
    var viewBackground: RelativeLayout? = null
    var viewFinish: RelativeLayout? = null

    fun bind(viewModel: TimerUserViewModel, position: Int, userClick: BehaviorSubject<Pair<TimerUserViewModel, Int>>,
             timeClick: BehaviorSubject<Pair<TimerUserViewModel, Int>>) {

        viewForeground = view.findViewById(R.id.viewForeground)
        viewBackground = view.findViewById(R.id.view_background)
        viewFinish = view.findViewById(R.id.view_finish)
        val userImage = view.findViewById<ImageView>(R.id.profileImage)
        val time = view.findViewById<TextView>(R.id.tv_time)

        Glide.with(view.context)
                .load(viewModel.getUserImage())
                .apply(RequestOptions()
                        .placeholder(R.drawable.ic_user))
                .into(userImage)

        userImage.setOnClickListener{ userClick.onNext(Pair(viewModel, position))}
        time.setOnClickListener { timeClick.onNext(Pair(viewModel, position)) }
        time.text = viewModel.getElapsedTime()

    }

}