package com.amplemind.vivasmart.core.navegation.flow

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.core.navegation.AppFlow

class StateC(val previousState: State, val appFlow: AppFlow) : State {

    override fun changeToPreviousState() {
        appFlow.state = previousState
    }

    override fun startActivity(context: Context) {
        // TODO
    }

    override fun getFragment() {
        // TODO
    }

    override fun doSomeAwesomeThing(param: String) {
        Log.d("StateC", "doing something awesome with $param")
        appFlow.state = StateA(appFlow)
    }

    override fun getSomeAwesomeThing(): String {
        return "getting something awesome from StateC"
    }


}