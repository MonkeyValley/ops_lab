package com.amplemind.vivasmart.core.utils

import android.util.Base64
import android.util.Base64.NO_WRAP
import java.io.UnsupportedEncodingException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class ApiAuthorization @Inject constructor() {

    fun getAuthToken(token: String): String {
        var data = ByteArray(0)
        try {
            val user = "USER_$USER_ID"
            data = "$user:${token}".toByteArray(charset("UTF-8"))
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP)
    }
}