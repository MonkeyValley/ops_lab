package com.amplemind.vivasmart.features.production_roster.fragments.interfaces

interface ControlTimerManager {
    fun onStartGeneralTimer()
    fun onPauseGeneralTimer()
    fun onNotifyItemChanged(position: Int)
    fun onRemoveItem(position: Int)
    fun onNotifyDataSetChange()
}
