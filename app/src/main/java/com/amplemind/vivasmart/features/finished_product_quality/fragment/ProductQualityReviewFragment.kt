package com.amplemind.vivasmart.features.finished_product_quality.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.repository.model.FinishedProductReportModel
import com.amplemind.vivasmart.core.utils.WEIGHT_PROM_LIST
import com.amplemind.vivasmart.features.finished_product_quality.FinishedProductQualityActivity.Companion.sharedPref
import com.amplemind.vivasmart.features.finished_product_quality.viewModel.ProductQualityReviewViewModel
import com.amplemind.vivasmart.features.packaging_quality.dialog.RecibaAverageWeightDialog
import com.amplemind.vivasmart.features.production_quality.ClientViewModel
import com.amplemind.vivasmart.features.production_quality.ItemCropUnitModelViewModel
import com.amplemind.vivasmart.features.production_quality.LotsViewModel
import com.amplemind.vivasmart.features.production_quality.UnitsViewModel
import com.amplemind.vivasmart.features.waste_control.viewModel.WasteControlTestFragmentViewModel
import kotlinx.android.synthetic.main.product_quality_review_fragment.*
import javax.inject.Inject

class ProductQualityReviewFragment : BaseFragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    @Inject
    lateinit var viewModel: ProductQualityReviewViewModel
    var spinnerCrops: Spinner? = null
    var spinnerPresentation: Spinner? = null
    var spinnerClient: Spinner? = null
    var spinnerLot: Spinner? = null
    var spinnerUnit: Spinner? = null
    var etPromKg: TextView? = null
    var etPromLb: TextView? = null

    val listCrop = ArrayList<ProductQualityReviewViewModel.CropStageModelItem>()
    var listaPacking = ArrayList<ProductQualityReviewViewModel.CropUnitModelItem>()
    val listClientes = ArrayList<ProductQualityReviewViewModel.ClientItem>()
    val listaLots = ArrayList<ProductQualityReviewViewModel.LotItem>()
    val listaUnits = ArrayList<ProductQualityReviewViewModel.UnitItem>()

    var crop_id : Int = 0
    var client_id : Int? = null
    var packing_id : Int? = null
    var stage_id : Int? = null
    var unit_id : Int? = null
    var prom_kg : String? = null
    var oneClick: Boolean = true

    fun newInstance(title: String): ProductQualityReviewFragment {
        val args = Bundle()
        args.putString("Title", title)

        val fragment = ProductQualityReviewFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val t = inflater.inflate(R.layout.product_quality_review_fragment, container, false)
        WEIGHT_PROM_LIST.clear()
        addSubscribers()
        spinnerCrops = t.findViewById(R.id.spinner_crop)
        spinnerPresentation = t.findViewById(R.id.spinner_presentation)
        spinnerClient = t.findViewById(R.id.spinner_client)
        spinnerLot = t.findViewById(R.id.spinner_lot)
        spinnerUnit = t.findViewById(R.id.spinner_unit)
        val btnClick = t.findViewById(R.id.btn_check) as Button

        spinnerPresentation!!.isEnable(false)
        spinnerClient!!.isEnable(true)
        spinnerLot!!.isEnable(true)

        spinnerCrops!!.onItemSelectedListener = this
        spinnerPresentation!!.onItemSelectedListener = this
        spinnerClient!!.onItemSelectedListener = this
        spinnerLot!!.onItemSelectedListener = this
        spinnerUnit!!.onItemSelectedListener = this

        etPromKg = t.findViewById(R.id.et_prom_kg)
        etPromKg!!.setOnClickListener(this)
        etPromLb = t.findViewById(R.id.et_prom_lb)

        btnClick.setOnClickListener{
            validardatos()
        }

        getCatalogos()
        initSpinner()

        return t
    }

    private fun validardatos(){
        var sample = et_sample?.text.toString()
        prom_kg = et_prom_kg?.text.toString()
        var brix: Double?
        if( et_brix?.text.toString().isNotEmpty()){
            brix = et_brix?.text.toString().toDouble()
        }
        else{
            brix = null
        }

        when {
            crop_id == 0 -> { Toast.makeText(context, "Seleccione un tipo de cultivo", Toast.LENGTH_SHORT).show() }
            packing_id == null -> { Toast.makeText(context, "Debe seleccionar una presentación", Toast.LENGTH_SHORT).show() }
            client_id == null -> { Toast.makeText(context, "Debe seleccionar un cliente", Toast.LENGTH_SHORT).show() }
            stage_id == null -> { Toast.makeText(context, "Debe seleccionar un lote", Toast.LENGTH_SHORT).show() }
            unit_id == null -> { Toast.makeText(context, "Debe seleccionar una unidad de medida", Toast.LENGTH_SHORT).show() }
            etPromKg!!.text.isEmpty() -> { Toast.makeText(context, "Debe intoducir el peso promedio", Toast.LENGTH_SHORT).show() }
            sample.isEmpty() -> { Toast.makeText(context, "Debe intoducir la cantidad de muestra", Toast.LENGTH_SHORT).show() }
            else -> {
                var objFinishedProd = FinishedProductReportModel(
                        null,
                        crop_id!!,
                        client_id!!,
                        packing_id!!,
                        stage_id!!,
                        prom_kg,
                        Integer.parseInt(sample),
                        unit_id!!,
                        null,
                        null,
                        brix,
                        null,
                        null)
                sharedPref.saveObjectToSharedPreference("finishedProductObj", objFinishedProd)
                if(ifConected()) {
                    WEIGHT_PROM_LIST.clear()
                    nextfrag()
                }
            }
        }
    }

    fun nextfrag(){
        val support = activity?.supportFragmentManager
        val commit = support?.beginTransaction()
                ?.replace(R.id.container_payroll, ProductQualityReviewDetailFragment().newInstance("ProductQualityReviewDetailFragment", true, "", "", ""))
                ?.addToBackStack("ProductQualityReviewDetailFragment")
                ?.commit()
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_unitCrop().subscribe(this::updateList_unitCrop).addTo(subscriptions)
        viewModel.onSuccessRequest_clients().subscribe(this::updateList_clients).addTo(subscriptions)
        viewModel.onSuccessRequest_lots().subscribe(this::updateList_lots).addTo(subscriptions)
        viewModel.onSuccessRequest_units().subscribe(this::updateList_units).addTo(subscriptions)
    }

    fun ifConected(): Boolean{
        return if(context!!.hasInternet())
            true
        else {
            Toast.makeText(context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

    fun getCatalogos() {
        listCrop.addAll(viewModel.getCropList())

        if(ifConected()) {
            //showProgressDialogInner(true)
            viewModel.getClients()
        }
    }

    fun getPresentaciones(id: Int) {
        if(ifConected()) {
            viewModel.getUnitCropList(id)
        }
    }

    private fun initSpinner() {
        spinnerCrops?.adapter = ArrayAdapter<ProductQualityReviewViewModel.CropStageModelItem>(activity?.applicationContext!!, R.layout.spinner_item_textview, listCrop)
    }

    override fun onItemSelected(adapter: AdapterView<*>, view: View, position: Int, id: Long) {
        when (adapter.id) {
            R.id.spinner_crop -> {
                if (position != 0) {
                    val nombre = adapter.getItemAtPosition(position).toString()
                    crop_id = listCrop.get(position).getId()

                    if (crop_id != 0) {
                        getPresentaciones(listCrop.get(position).getId())
                        spinnerPresentation!!.isEnable(true)
                    }
                }
                else{
                    crop_id = 0
                }

            }
            R.id.spinner_presentation -> {
                packing_id = listaPacking[position].getId()
            }
            R.id.spinner_client -> {
                client_id = listClientes[position].getId()
            }
            R.id.spinner_lot -> {
                stage_id = listaLots[position].getId()
            }
            R.id.spinner_unit -> {
                unit_id = listaUnits[position].getId()
            }
        }
    }

    private fun updateList_unitCrop(list: List<ItemCropUnitModelViewModel>) {
        listaPacking.clear()
        for (item in list) {
            var obj  =  ProductQualityReviewViewModel.CropUnitModelItem(item.id, item.crop_id, item.name)
            listaPacking.add(obj)
        }
        spinnerPresentation?.adapter = ArrayAdapter<ProductQualityReviewViewModel.CropUnitModelItem>(activity?.applicationContext!!, R.layout.spinner_item_textview, listaPacking )
    }

    private fun updateList_clients(list: List<ClientViewModel>) {
        listClientes.clear()
        for (item in list) {
            var obj  =  ProductQualityReviewViewModel.ClientItem(item.id, item.name)
            listClientes.add(obj)
        }
        spinnerClient?.adapter = ArrayAdapter<ProductQualityReviewViewModel.ClientItem>(activity?.applicationContext!!, R.layout.spinner_item_textview, listClientes )

    }

    private fun updateList_lots(list: List<LotsViewModel>) {
        listaLots.clear()
        for (item in list) {
            var obj  =  ProductQualityReviewViewModel.LotItem(item.id, item.crop_id!!, item.lot!!.name, item.business_unit_id)
            listaLots.add(obj)
        }
        spinnerLot?.adapter = ArrayAdapter<ProductQualityReviewViewModel.LotItem>(activity?.applicationContext!!, R.layout.spinner_item_textview, listaLots )
    }

    private fun updateList_units(list: List<UnitsViewModel>) {
        listaUnits.clear()

        var firtItem: ProductQualityReviewViewModel.UnitItem? = null

        for (item in list) {
            var obj  =  ProductQualityReviewViewModel.UnitItem(item.id, item.business_unit_id!!, item.name, item.business_unit_names)
            if(item.name == "Piezas") firtItem = obj
            listaUnits.add(obj)
        }

        if(firtItem != null) {
            listaUnits.remove(firtItem)
            listaUnits.add(0, firtItem)
        }

        spinner_unit?.adapter = ArrayAdapter<ProductQualityReviewViewModel.UnitItem>(activity?.applicationContext!!, R.layout.spinner_item_textview, listaUnits )
       // showProgressDialogInner(false)
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {
        println("notSelected")
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.et_prom_kg -> {
                if (oneClick) {
                    oneClick = false
                    val dialog = RecibaAverageWeightDialog.newInstance(2,0L)

                    dialog.setListener(object : RecibaAverageWeightDialog.Listener {
                        override fun onAcceptClicked(dialog: RecibaAverageWeightDialog) {
                            val averageWeight = dialog.getAverageWeight()
                            oneClick = true
                            if (averageWeight != 0.0) {
                                etPromKg!!.text = "%.2f".format(averageWeight)
                                etPromLb!!.text = "%.2f".format((averageWeight * 2.2))
                            } else {
                                etPromKg!!.text = ""
                                etPromLb!!.text = ""
                            }
                            dialog.dismiss()
                        }

                        override fun onOmitClicked(dialog: RecibaAverageWeightDialog) {
                            oneClick = true
                            dialog.dismiss()
                        }

                        override fun onDismissed(dialog: RecibaAverageWeightDialog) {
                            oneClick = true
                        }

                    })

                    dialog.show(activity?.supportFragmentManager!!, "avgWeight")
                }
            }
        }
    }


}

