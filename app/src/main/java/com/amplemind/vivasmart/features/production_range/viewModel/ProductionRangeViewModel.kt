package com.amplemind.vivasmart.features.production_range.viewModel

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.production_range.service.ProductionRangeService
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangeVariatiesModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.ArrayList
import javax.inject.Inject

class ProductionRangeViewModel @Inject constructor(
        private val service: ProductionRangeService
) : BaseViewModel() {

    fun setServiceContect(context: Context){
        service.context = context
        service.preferences  = UserAppPreferences(context, null, null)
    }

    private val requestSuccessProductionRangesVarieties = BehaviorSubject.create<List<ProductionRangeVariatiesModel>>()

    fun getVariety(lotId: String, soilId: String): Disposable =
            service.getActivities(lotId.toInt(), soilId.toInt())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { }
                    .doOnTerminate { }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        requestSuccessProductionRangesVarieties.onNext(it)
                    }, {
                        Log.e("ProductionRangesData", "error", it)
                    })

    fun onSuccesProductionRangesVarieties(): BehaviorSubject<List<ProductionRangeVariatiesModel>> = requestSuccessProductionRangesVarieties

    fun saveProductionRangeVarieties(list: ArrayList<ProductionRangeVariatiesModel>, lotId: String) : Boolean =
        service.saveProductionRangeVarieties(list, lotId)


    fun validateWeekData(lotId: String, week: Int): Boolean = service.validateWeekData(lotId, week)


}