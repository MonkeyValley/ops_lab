package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.extensions.dummyListCards
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.TinderCardModelTemp
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.AccountApi
import com.amplemind.vivasmart.core.repository.remote.CleaningApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.google.gson.Gson
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/10/18.
 */
open class CleaningRepository @Inject constructor(cleaningApi : CleaningApi, private val accountApi: AccountApi, private val sharedPreferences : UserAppPreferences, private val apiAuthorization: ApiAuthorization){

    fun getCards() : Observable<List<TinderCardModelTemp>>{
        return Observable.just(dummyListCards())
    }

    fun saveAgreedTutorial(): Observable<UserModel> {
        return accountApi.setAgreedTutorial(apiAuthorization.getAuthToken(sharedPreferences.token), AccountRepository.UpdateAgreedTutorial(false), getIdCollaborator())
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun isAgreedTutorial(): Observable<Boolean> {
        return Observable.just(Gson().fromJson(sharedPreferences.userInfo, UserModel::class.java).tutorial)
    }

    fun getIdCollaborator() : Int {
        return Gson().fromJson(sharedPreferences.userInfo, UserModel::class.java).id
    }

    fun saveUser(user : String) {
        sharedPreferences.userInfo = user
    }

}