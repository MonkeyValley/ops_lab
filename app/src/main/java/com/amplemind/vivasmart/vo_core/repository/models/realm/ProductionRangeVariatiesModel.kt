package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class ProductionRangeVariatiesModel(
        @SerializedName("id") var id: Int? = 0,
        @SerializedName("name") var name: String? = "",
        @SerializedName("options") var options: RealmList<ProductionRangesOption>? = RealmList(),
        @SerializedName("production_var_data_type_id") var productionVarDataTypeId: Int? = 0,
        @SerializedName("production_var_data_type") var productionVarDataType: ProductionVarDataType? = null,
        @SerializedName("production_var_type_id") var productionVarTypeId: Int? = 0,
        @SerializedName("production_var_type") var productionVarType: ProductionVarType? = null,
        @SerializedName("soil_type_id") var soilTypeId: Int? = 0,
        @SerializedName("soil_type") var soilType: SoilType? = null,
        @SerializedName("crops") var crops: RealmList<Crops>? = RealmList(),
        @SerializedName("business_unit_id") var businessUnitId: Int? = 0,
        @SerializedName("activity_sub_category_id") var activitySubCategoryId: Int? = 0,
        @SerializedName("activity_sub_category") var activitySubCategory: ActivitySubCategory? = null,
        var value: String? = "",
        var minVal: String? = "",
        var maxVal: String? = "",
        var productionVarId: String? = "",
        var productionVarOptionId: String? = "",
        var mode: String? = ""
) : RealmObject()

open class ProductionRangesOption(
        @SerializedName("id") var id: Int? = 0,
        @SerializedName("name") var name: String? = "",
        @SerializedName("production_var_id") var productionVarId: Int? = 0
) : RealmObject()

open class ProductionVarDataType(
        @SerializedName("id") var id: Int? = 0,
        @SerializedName("name") var name: String? = "",
        @SerializedName("production_var_type_id") var productionVarTypeId: Int? = 0
) : RealmObject()

open class ProductionVarType(
        @SerializedName("id") var id: Int? = 0,
        @SerializedName("name") var name: String? = ""
) : RealmObject()

open class SoilType(
        @SerializedName("id") var id: Int? = 0,
        @SerializedName("name") var name: String? = ""
) : RealmObject()

open class Crops(
        @SerializedName("crop_id") var cropId: Int? = 0,
        @SerializedName("production_var_id") var productionVarId: Int? = 0,
        @SerializedName("crop") var crop: Crop? = null
): RealmObject()

open class Crop(
        @SerializedName("id") var id: Int? = 0,
        @SerializedName( "crop_type_id") var cropTypeId: Int? = 0,
        @SerializedName("graft_name") var graftName: String? = "",
        @SerializedName("has_cluster") var hasCluster: Boolean? = false,
        @SerializedName("is_active") var isActive: Boolean? = false,
        @SerializedName("is_graft") var isGraft: Boolean? = false,
        @SerializedName("name") var name: String? = "",
        @SerializedName("presentation_names") var presentationNames: String? = "",
        @SerializedName("uuid") var uuid: String? = getUUID(),
        @SerializedName("variety") var variety: String? = ""
): RealmObject()

open class ActivitySubCategory(
        @SerializedName("activity_category_id") var activityCategoryId: Int? = 0,
        @SerializedName("id") var id: Int? = 0,
        @SerializedName("name") var name: String? = "",
        @SerializedName("quality_percentage") var qualityPercentage: Double? = 0.0
): RealmObject()