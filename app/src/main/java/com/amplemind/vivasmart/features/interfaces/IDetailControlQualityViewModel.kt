package com.amplemind.vivasmart.features.interfaces

import com.amplemind.vivasmart.features.quality_control.viewModel.DetailControlQualityItemViewModel

interface IDetailControlQualityViewModel {

    fun loadStaticData(): io.reactivex.Observable<List<DetailControlQualityItemViewModel>>
    fun getSignatureTitle(): String
    fun getSignatureMessage(): String

}