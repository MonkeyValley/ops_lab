package com.amplemind.vivasmart.features.splash.viewModel

import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.features.login.LoginActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val api: AccountRepository) {

    private val validToken: BehaviorSubject<Boolean> = BehaviorSubject.create()
    var token: String = ""

    fun validateToken(token: String): Disposable {
        this.token = token
        return api.validateDeepLinkToken(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    validToken.onNext(true)
                }, {
                    validToken.onNext(false)
                } )
    }

    fun onValidToken(): BehaviorSubject<Boolean> {
        return validToken
    }

}