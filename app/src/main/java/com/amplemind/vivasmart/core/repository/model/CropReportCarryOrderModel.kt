package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class CropReportCarryOrderModel(@SerializedName("box_no") val send: Int,
                                     @SerializedName("folio") val carryOrder: String,
                                     @SerializedName("id") val id: Int)