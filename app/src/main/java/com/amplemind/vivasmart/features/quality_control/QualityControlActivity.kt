package com.amplemind.vivasmart.features.quality_control

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.getSpanCount
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.quality_control.adapter.ControlQualityAdapter
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlQualityItemViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.QualityControlViewModel
import kotlinx.android.synthetic.main.content_quality_control_activity.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

/**
 * Created by
 *           amplemind on 7/17/18.
 */
@SuppressLint("Registered")
open class QualityControlActivity : BaseActivity() {

    private val TAG = QualityControlActivity::class.java.simpleName

    @Inject
    lateinit var viewModel: QualityControlViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_quality_control_activity)
        viewModel.flow = QualityControlViewModel.LOT_FLOW.QUALITY_CONTROL
        setupToolbar()
        initRecycler()
        getData()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getLots()?.addTo(subscriptions)
    }

    private fun initRecycler() {
        rv_control_quality.hasFixedSize()
        rv_control_quality.layoutManager = GridLayoutManager(this, getSpanCount(this))
        rv_control_quality.addItemDecoration(ItemOffsetDecoration(4.toPx()))
        rv_control_quality.adapter = ControlQualityAdapter()
        clickItemList()
    }

    private fun getData() {
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onGetLots().subscribe(this::onGetLots).addTo(subscriptions)
    }

    private fun onGetLots(lots: List<ControlQualityItemViewModel>) {
        (rv_control_quality.adapter as ControlQualityAdapter).addElements(lots)
    }


    private fun clickItemList() {
        (rv_control_quality.adapter as ControlQualityAdapter).onClickItem().subscribe {
            if (!it.isComplete()) {
                val activityIntent = Intent(this, ListControlActivity::class.java)
                activityIntent.putExtra("title", it.getLotName())
                activityIntent.putExtra("stage_id", it.stageId)
                startActivity(activityIntent)
            }
        }.addTo(subscriptions)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_quality_control
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.quality_control)
        lbl_track_title.text = "/ Calidad / Producción / Labores /"
//        lbl_track_title.setTextColor(this.resources.getColor(R.color.white))
//        icon_track_title.setColorFilter(this.resources.getColor(R.color.white))
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}