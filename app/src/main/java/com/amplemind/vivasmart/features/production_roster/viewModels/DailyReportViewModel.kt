package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.vo_core.repository.DailyReportRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.utils.formatHumanDate
import com.amplemind.vivasmart.vo_core.utils.formatISODate
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

class DailyReportViewModel @Inject constructor(val repository: DailyReportRepository) {

    lateinit var stageUuid: String

    lateinit var humanDate: String

    lateinit var mDate: String
    private val mCurrentDate: Calendar = Calendar.getInstance()

    var year: Int = mCurrentDate.get(Calendar.YEAR)
    var month: Int = mCurrentDate.get(Calendar.MONTH)
    var day: Int = mCurrentDate.get(Calendar.DAY_OF_MONTH)

    fun setDate() {
        setDate(year, month, day)
    }

    fun setDate(year: Int, month: Int, day: Int) {

        this.year = year
        this.month = month
        this.day = day

        mDate = formatISODate(year, month, day)
        humanDate = formatHumanDate(year, month, day)
    }

    fun loadStage(): Maybe<StageModel> =
            repository.loadStage(stageUuid)

    fun loadReport(mode: Int): Single<List<ItemDailyReportViewModel>?> {
        return repository.loadDailyReport(stageUuid, mDate, mode)
                .map{report ->
                    return@map report.map{item ->
                        return@map ItemDailyReportViewModel(item)
                    }
                }
    }

}