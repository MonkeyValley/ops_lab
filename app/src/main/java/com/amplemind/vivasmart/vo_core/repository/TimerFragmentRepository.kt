package com.amplemind.vivasmart.vo_core.repository

import android.util.SparseArray
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityCodesDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.CollaboratorsDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import java.io.File
import javax.inject.Inject

class TimerFragmentRepository @Inject constructor(
        private val activityCodesDao: ActivityCodesDao,
        private val activityLogsDao: ActivityLogsDao,
        private val collaboratorsDao: CollaboratorsDao
): Repository() {

    companion object {
        val TAG: String = TimerFragmentRepository::class.java.simpleName
    }

    override fun cleanUp() {
        activityCodesDao.cleanUp()
        activityLogsDao.cleanUp()
        collaboratorsDao.cleanUp()
    }

    fun loadActivityCode(uuid: String) =
            activityCodesDao.findByUuidLiveAsync(uuid)

    fun loadActiveLogs(activityCodeUuid: String) =
            activityLogsDao.findActiveLogs(activityCodeUuid)

    fun create(activityCode: ActivityCodeModel, collaboratorsUuids: Array<String>) =
            activityLogsDao.create(activityCode, collaboratorsUuids)

    fun setUnits(activityLog: ActivityLogModel, quantity: Int, badQuantity: Int) =
            activityLogsDao.setUnits(activityLog, quantity, badQuantity)

    fun assignGrooves(activityLogUuid: String, grooves: SparseArray<List<Int>>) =
            activityLogsDao.assignGrooves(activityLogUuid, grooves)

    fun setQuantityAndTime(activityLog: ActivityLogModel, quantity: Int, timeInMinutes: Int) =
            activityLogsDao.setQuantityAndTime(activityLog, quantity, timeInMinutes)

    fun startTimer(activityLog: ActivityLogModel) =
            activityLogsDao.startTimer(activityLog)

    fun startTimer(activityLogs: List<ActivityLogModel>) =
            activityLogsDao.startTimer(activityLogs)

    fun pauseTimer(activityLog: ActivityLogModel) =
            activityLogsDao.pauseTimer(activityLog)

    fun pauseTimer(activityLogs: List<ActivityLogModel>) =
            activityLogsDao.pauseTimer(activityLogs)

    fun remove(activityLog: ActivityLogModel) =
            activityLogsDao.remove(activityLog)

    fun removeRosterDetail(activityLog: ActivityLogModel) =
            activityLogsDao.removeRosterDetail(activityLog)

    fun finish(activityLog: ActivityLogModel, signature: File? = null) =
            activityLogsDao.finish(activityLog, signature)

    fun finish(activityLogs: List<ActivityLogModel>, signature: File? = null) =
            activityLogsDao.finish(activityLogs, signature)

    fun updatePaymentMode(activityLogs: List<ActivityLogModel>, isTraining: Boolean = false, isPractice: Boolean = false, isControl: Boolean = false) =
            activityLogsDao.updatePaymentMode(activityLogs, isTraining, isPractice, isControl)

    fun setAsDone(activityLogs: List<ActivityLogModel>) =
            activityLogsDao.setAsDone(activityLogs)

    fun setAsDone(stage: StageModel, stageTypeId: Int) =
            activityLogsDao.setAsDone(stage, stageTypeId)

}