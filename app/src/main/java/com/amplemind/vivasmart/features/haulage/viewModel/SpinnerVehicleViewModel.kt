package com.amplemind.vivasmart.features.haulage.viewModel

import com.amplemind.vivasmart.core.repository.model.VehicleModel

class SpinnerVehicleViewModel(private val model: VehicleModel) {

    val id = model.id

    override fun toString(): String {
        return model.name
    }

}