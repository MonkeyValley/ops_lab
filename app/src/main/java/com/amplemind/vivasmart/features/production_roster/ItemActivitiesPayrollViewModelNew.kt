package com.amplemind.vivasmart.features.production_roster

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import android.view.View
import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.core.repository.model.ActivitiesModel


class ItemActivitiesPayrollViewModelNew(val item: ActivitiesModel, val stageId: Int) {

    enum class PAYMENTTYPE (val type : Int) {
        PIECE(1),
        HRS(2);

        companion object {
            fun from (value : Int) = PAYMENTTYPE.values().first{ it.type == value }
        }
    }


    val name = item.name
    val id = item.id
    val drawable = 1
    val rangeFrom = item.rangeFrom
    val rangeTo = item.rangeTo
    val unitLimit = item.unitLimit ?: 1
    val cost = if(item.cost != null) item.cost else item.unit?.cost
    val activityId = item.id
    val activityName = item.name
    val isGrooves = item.isGrooves
    val name_unit = if (item.unit != null) item.unit.name else "Surcos"
    val icon = "${BuildConfig.SERVER_URL}/uploads/icons/${item.icon}"
    val costPerHour = item.costPerHour
    val payment = PAYMENTTYPE.from(item.activityTypeId ?: 1)
    val sunday_rate = item.sunday_rate ?: false
    val isPractice = item.isPractice ?: false
    val isHead = item.isHead ?: false
    val costPractice = item.practiceCost

    val state = item.stage_activities

    var isExtraHour = false

    fun isTrainingEnabled(): Boolean {
        if (item.code == null || item.code == 0) {
            return false
        }
        return true
    }


}
