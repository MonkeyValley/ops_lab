package com.amplemind.vivasmart.features.login

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.core.utils.Validations
import com.amplemind.vivasmart.features.login.viewModel.SetPasswordViewModel
import com.amplemind.vivasmart.features.main.MainActivity
import kotlinx.android.synthetic.main.content_new_password_activity.*
import javax.inject.Inject

@SuppressLint("Registered")
open class SetPasswordActivity : BaseActivity() {

    private val TAG = LoginActivity::class.java.simpleName
    private var isReestablishPassword = false
    private lateinit var token: String

    @Inject
    lateinit var viewModel : SetPasswordViewModel
    @Inject
    lateinit var apiErrors: HandleApiErrors

    private lateinit var dialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_new_password_activity)
        initializeView()
        viewModel.onProgressStatus().subscribe(this::onShowProgress).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onRequestFail).addTo(subscriptions)
        viewModel.onSuccessGetUser().subscribe(this::onGetUser).addTo(subscriptions)
        viewModel.getFormValidation().subscribe { validation ->

            when(validation) {
                Validations.PasswordValidation.PASSWORD_MUST_MATCH -> {
                    txtInputPassword.error = getString(R.string.password_must_match)
                }
                Validations.PasswordValidation.PASSWORD_ERROR_FORMAT -> {
                    txtInputPassword.error = getString(R.string.password_format_error)
                }
                else -> {
                    makeRequest()
                }
            }
        }
    }

    private fun makeRequest() {
        viewModel.setNewPassword(AccountRepository.SetPasswordRequest(token, ed_new_password.text.toString()))
                .doOnSubscribe {
                    dialog.show()
                }
                .doOnTerminate { dialog.dismiss() }
                .doOnDispose {
                }
                .subscribe({
                    viewModel.saveToken(it.token)
                    viewModel.getUser()
                }, {
                    txtInputPassword.error = apiErrors.handleLoginError(it)
                }).addTo(subscriptions)
    }

    private fun onRequestFail(error: String) {
        txtInputPassword.error = error
    }

    private fun onShowProgress(show: Boolean) {
        if (show) dialog.show() else dialog.dismiss()
    }

    private fun onGetUser(success: Boolean) {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun initializeView() {
        token = intent.getStringExtra("token")
        dialog = ProgressDialog(this)
        dialog.setMessage(getString(R.string.server_connection))
        dialog.setCancelable(false)
        btn_log_in_new_pass.setOnClickListener {
            viewModel.validatePassword(ed_new_password.text.toString(), ed_password_confirm.text.toString())
        }
    }
}
