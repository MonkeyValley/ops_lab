package com.amplemind.vivasmart.vo_core.repository.responses

import com.google.gson.annotations.SerializedName

data class ObjectResponse<T: Any> (
        @SerializedName("data")         val data: List<T>,
        @SerializedName("item_count")   val itemCount: Int
)

