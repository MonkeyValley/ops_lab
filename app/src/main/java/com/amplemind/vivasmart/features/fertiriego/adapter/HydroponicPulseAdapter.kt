package com.amplemind.vivasmart.features.fertiriego.adapter

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.HydroponicPulseItemBinding
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PulseForTable
import com.amplemind.vivasmart.vo_core.repository.models.realm.TablesPulsePerDay
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.PublishSubject
import io.reactivex.Observable
import io.realm.Realm

class HydroponicPulseAdapter : RecyclerView.Adapter<HydroponicPulseAdapter.HydroponicPulseViewHolder>() {
    private var list = mutableListOf<Int>()

    var listener: Listener? = null

    var date = ""
    var lotId = 0

    private val clickSubject = PublishSubject.create<Int>()

    private var mRecyclerView: RecyclerView? = null

    fun addElements(data: List<Int>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HydroponicPulseViewHolder {
        val binding = HydroponicPulseItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HydroponicPulseViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HydroponicPulseViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem() = clickSubject

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mRecyclerView = null
    }

    inner class HydroponicPulseViewHolder(private val binding: HydroponicPulseItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private var item: Int = 0

        fun bind(item: Int) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.btnPulse.text = item.toString()

            when (validateDayRvision(item, binding)) {
                1 -> binding.btnPulse.setBackgroundColor(Color.parseColor("#f2dc57"))
                2 -> binding.btnPulse.setBackgroundColor(Color.parseColor("#3e69df"))
                else -> binding.btnPulse.setBackgroundColor(Color.parseColor("#ffffff"))
            }

            binding.btnPulse.setOnClickListener {
                clickSubject.onNext(this.item)
            }

            binding.root.setOnClickListener {
                clickSubject.onNext(this.item)
            }
        }

    }

    interface Listener {
        fun onGetCounterValue(position: Int): Observable<Int>?
    }

    fun validateDayRvision(pulse: Int, binding: HydroponicPulseItemBinding): Int {
        var validate = 0
        Realm.getDefaultInstance().use {

            var pulseForTable = it
                    .where(PulseForTable::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .and()
                    .equalTo("pulse", pulse)
                    .findAll()

            pulseForTable.forEach { pulseTable ->
                if(pulseTable.isCalculated!!) binding.tvCalculated.visibility = View.VISIBLE
            }

            val result = it
                    .where(TablesPulsePerDay::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .findFirst()

            val lot = it
                    .where(LotModel::class.java)
                    .equalTo("id", lotId)
                    .findFirst()

            var count = 0
            var t1 = 0
            var t2 = 0
            var t3 = 0
            var t4 = 0
            var t5 = 0
            var t6 = 0
            if (lot != null) {
                if (lot.table1 != 0) {
                    count++
                    t1 = 1
                }
                if (lot.table2 != 0) {
                    count++
                    t2 = 1
                }
                if (lot.table3 != 0) {
                    count++
                    t3 = 1
                }
                if (lot.table4 != 0) {
                    count++
                    t4 = 1
                }
                if (lot.table5 != 0) {
                    count++
                    t5 = 1
                }
                if (lot.table6 != 0) {
                    count++
                    t6 = 1
                }
            }

            if (result != null) {
                if (result.T1!!.where().equalTo("pulse", pulse).findAll().size != 0
                        || result.T2!!.where().equalTo("pulse", pulse).findAll().size != 0
                        || result.T3!!.where().equalTo("pulse", pulse).findAll().size != 0
                        || result.T4!!.where().equalTo("pulse", pulse).findAll().size != 0
                        || result.T5!!.where().equalTo("pulse", pulse).findAll().size != 0
                        || result.T6!!.where().equalTo("pulse", pulse).findAll().size != 0) {
                    validate = when {
                        result.T1!!.where().equalTo("pulse", pulse).findAll().size < t1 -> 1
                        result.T2!!.where().equalTo("pulse", pulse).findAll().size < t2 -> 1
                        result.T3!!.where().equalTo("pulse", pulse).findAll().size < t3 -> 1
                        result.T4!!.where().equalTo("pulse", pulse).findAll().size < t4 -> 1
                        result.T5!!.where().equalTo("pulse", pulse).findAll().size < t5 -> 1
                        result.T6!!.where().equalTo("pulse", pulse).findAll().size < t6 -> 1
                        else -> 2
                    }
                } else validate = 0
            } else validate = 0

        }
        return validate
    }
}
