package com.amplemind.vivasmart.features.production_roster

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.collaborators.SearchCollaboratorActivity
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityCodesDao
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_features.production_roster.TimerFragment
import com.amplemind.vivasmart.vo_features.production_roster.adapters.TimerFragmentViewPagerAdapter
import com.amplemind.vivasmart.vo_features.production_roster.helpers.ActivityLogTimer
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.TimerActivityViewModel
import com.getbase.floatingactionbutton.FloatingActionsMenu
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_timer.*
import kotlinx.android.synthetic.main.activity_timer.view.*
import kotlinx.android.synthetic.main.content_haulage_activity.*
import javax.inject.Inject

class TimerActivity : BaseActivityWithFragment() {

    companion object {

        val TAG = TimerActivity::class.java.simpleName

        val PARAM_ACTIVITY_UUID     = "$TAG.Activity"
        val PARAM_STAGE_UUID        = "$TAG.Stage"

        private val PARAM_CURRENT_CODE_UUID = "$TAG.CurrentCodeUuid"

        private const val REQUEST_ADD_COLLABORATORS: Int = 10
    }

    @Inject
    lateinit var mViewModel: TimerActivityViewModel

    @Inject
    lateinit var preferences: UserAppPreferences

    @Inject
    lateinit var mEventBus: EventBus

    @Inject
    lateinit var mActivityLogTimer: ActivityLogTimer

    private lateinit var mViewPagerAdapter: TimerFragmentViewPagerAdapter

    private var mCurrentFragmentIndex: Int = -1
    private var mCurrentCodeUuid: String? = null

    private var mShowArrowDisposable: Disposable? = null

    private var activityId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer)

        Log.e("----------","TimerActivity")
        mViewModel.activityUuid = intent.getStringExtra(PARAM_ACTIVITY_UUID)
        mViewModel.stageUuid    = intent.getStringExtra(PARAM_STAGE_UUID)

        mCurrentCodeUuid = savedInstanceState?.getString(PARAM_CURRENT_CODE_UUID)

        mActivityLogTimer.setUp()

        setUpUI()
        setUpCallbacks()
        setUpUIListeners()
        loadData()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState?.putString(PARAM_CURRENT_CODE_UUID, mCurrentCodeUuid)
    }

    override fun onDestroy() {
        super.onDestroy()
        mActivityLogTimer.cleanUp()
        mViewModel.cleanUp()
    }

    private fun loadData() {
        mViewModel.loadStage().subscribe()?.addTo(subscriptions)
        mViewModel.loadActivity().subscribe(this::onActivityLoaded)?.addTo(subscriptions)
        mViewModel.loadActivityCodes().subscribe(this::onActivityCodesLoaded).addTo(subscriptions)
    }

    private fun setUpCallbacks() {

        mViewPagerAdapter.onFragmentCreated
                .filter{ index -> index == mCurrentFragmentIndex }
                .subscribe {
                    setUpFragmentEmptyCallback()
                }
                .addTo(subscriptions)
    }

    /*
    private fun onError(event: Repository.OnErrorData) {
        AlertDialog.Builder(this)
                .setMessage(event.error.localizedMessage)
                .show()
        Log.e(TAG, Log.getStackTraceString(event.error))
    }
    */

    private fun onActivityLoaded(activity: ActivityModel) {
        setupToolbar(activity)
    }

    private fun onActivityCodesLoaded(activityCodes: Changes<ActivityCodeModel>) {

        if (activityCodes.data.isEmpty()) {
            showArrow(true, R.string.add_activity_code)
        }
        else {
            showArrow(false, R.string.add_activity_code)
            mViewPagerAdapter.setItems(activityCodes)
        }
    }

    private fun setUpUI() {
        setUpTabs()
    }


    private fun setUpUIListeners() {

        fab_menu.setOnFloatingActionsMenuUpdateListener(object: FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
            override fun onMenuCollapsed() {
                overlay_activity_timer.visibility = View.GONE
            }

            override fun onMenuExpanded() {
                overlay_activity_timer.visibility = View.VISIBLE
            }

        })

        btn_add_users.setOnClickListener{
            if(mViewPagerAdapter.getCount() > 0) {
                val intent = Intent(this, SearchCollaboratorActivity::class.java).apply {
                    putExtra(SearchCollaboratorActivity.ACTIVITY_ID, activityId)
                }
                mCurrentCodeUuid = mViewPagerAdapter.currentItem.uuid
                startActivityForResult(intent, REQUEST_ADD_COLLABORATORS)
                fab_menu.collapse()
            } else {
                val snackbar = Snackbar.make(root_activity_timer, "Agregue por lo menos 1 Codigo", Snackbar.LENGTH_LONG)
                snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.redAlert))
                snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
                snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                snackbar.show()
            }
        }

        btn_crop.setOnClickListener {
            startActivity(Intent(this, CropReportActivity::class.java))
            fab_menu.collapse()
        }

        fab_add_activity_code.setOnClickListener {
            mViewModel
                    .createActivityCode()
                    .subscribe({}, { error ->
                        when (error) {
                            is ActivityCodesDao.OpenActivityCodesExceeded ->
                                AlertDialog.Builder(this)
                                        .setMessage(
                                                getString(R.string.activity_codes_exceeded)
                                                        .format(ActivityCodesDao.OPEN_LIMIT)
                                        )
                                        .show()
                            else -> throw error
                        }
                    }).addTo(subscriptions)
            fab_menu.collapse()
        }

        tabLayout.addOnTabSelectedListener(
                object: TabLayout.OnTabSelectedListener {
                    override fun onTabReselected(p0: TabLayout.Tab?) {
                    }

                    override fun onTabUnselected(tab: TabLayout.Tab?) {
                        mShowArrowDisposable?.dispose()
                    }

                    override fun onTabSelected(tab: TabLayout.Tab?) {
                        tab?.apply {
                            mCurrentFragmentIndex = position
                            setUpFragmentEmptyCallback()
                        }
                    }

                }
        )

    }

    private fun setupToolbar(activity: ActivityModel) {
        val mToolbar = toolbar_timer
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = activity.name
        activityId = activity.id
    }

    private fun setUpTabs() {

        mViewPagerAdapter = TimerFragmentViewPagerAdapter(supportFragmentManager)
        mViewPagerAdapter.setWithViewPager(pager)
        pager.swipeEnabled = false
        pager.offscreenPageLimit = 2
        tabLayout.setupWithViewPager(pager)

    }

    private fun setUpFragmentEmptyCallback() {
        if (mCurrentFragmentIndex > -1) {
            (mViewPagerAdapter.getFragment(mCurrentFragmentIndex))?.apply {
                mShowArrowDisposable = (this as TimerFragment).isEmpty
                        .subscribe{ show ->
                            showArrow(show, R.string.add_users)
                        }
            }
        }
    }

    private fun showArrow(show : Boolean, textResource: Int){
        empty_view.text.text = getString(textResource)
        empty_view.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_ADD_COLLABORATORS -> if (resultCode == Activity.RESULT_OK) {
                if (mCurrentCodeUuid != null) {

                    val collaboratorUuids =
                            data?.getStringArrayExtra(SearchCollaboratorActivity.EXTRA_SELECTED)

                    if (collaboratorUuids != null) {
                        mViewModel.createActivityLogs(mCurrentCodeUuid!!, collaboratorUuids)
                                .subscribe()
                                .addTo(subscriptions)
                    }

                }
            }
        }
    }

}

