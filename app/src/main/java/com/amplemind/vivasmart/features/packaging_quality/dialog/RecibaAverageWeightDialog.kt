package com.amplemind.vivasmart.features.packaging_quality.dialog

import android.graphics.Color
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.DialogFragment
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.features.packaging_quality.adapters.RecibaAverageWeightPagerAdapter
import com.amplemind.vivasmart.features.packaging_quality.viewModel.BoxAverageWeightViewModel
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class RecibaAverageWeightDialog : BaseDialogFragment() {

    companion object {
        @JvmStatic
        fun newInstance(mode : Int, cropId: Long)
                : RecibaAverageWeightDialog {
            val args = Bundle()
            args.putInt("MODE", mode)
            args.putLong("CROP_ID", cropId)
            val dialog = RecibaAverageWeightDialog()
            dialog.arguments = args
            dialog.isCancelable = false
            return dialog
        }
    }

    private lateinit var mTabs: TabLayout
    private lateinit var mAdapter: RecibaAverageWeightPagerAdapter

    private var mListener: Listener? = null

    var tvTittle: TextView? = null
    var barTittle: View? = null
    var mView: View? = null

    @Inject
    lateinit var mViewModel: BoxAverageWeightViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_reciba_average_weight_dialog, container)

        if(arguments?.getInt("MODE", 0) ?: 0 == 1)
            mAdapter = RecibaAverageWeightPagerAdapter(
                    childFragmentManager,
                    1,
                    arguments?.getLong("CROP_ID", 0L) ?: 0L)
        else {
            tvTittle = mView!!.findViewById(R.id.tv_tittle)
            tvTittle!!.visibility = View.GONE
            barTittle = mView!!.findViewById(R.id.view14)
            barTittle!!.visibility = View.GONE
            mAdapter = RecibaAverageWeightPagerAdapter(
                    childFragmentManager,
                    2,
                    0L)
        }

        mTabs = mView!!.findViewById(R.id.tabs)
        val pager = mView!!.findViewById<ViewPager>(R.id.pager)

        pager.adapter = mAdapter
        mTabs.setupWithViewPager(pager)

        mView!!.findViewById<Button>(R.id.btn_accept).setOnClickListener {

            if (mViewModel.valitateAcerageBox(
                            getAverageWeight(),
                            arguments?.getLong("CROP_ID", 0L) ?: 0L)) {
                showSnackBar("Peso promedio fuera de rango.")
            } else {
                mListener?.onAcceptClicked(this@RecibaAverageWeightDialog)
            }

        }

        mView!!.findViewById<Button>(R.id.btn_omit).setOnClickListener{
            mListener?.onOmitClicked(this@RecibaAverageWeightDialog)
        }

        dialog!!.setOnDismissListener{
            mListener?.onDismissed(this@RecibaAverageWeightDialog)
        }

        return mView!!
    }

    fun getAverageWeight(): Double {
        return mAdapter.getAverageWeight(mTabs.selectedTabPosition)
    }

    fun setListener(listener: Listener) {
        mListener = listener
    }

    interface Listener {
        fun onAcceptClicked(dialog: RecibaAverageWeightDialog)
        fun onOmitClicked(dialog: RecibaAverageWeightDialog)
        fun onDismissed(dialog: RecibaAverageWeightDialog)
    }

    fun showSnackBar(msj: String) {
        val snackbar = Snackbar.make(mView!!, msj, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

}
