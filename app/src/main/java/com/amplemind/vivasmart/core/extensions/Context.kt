package com.amplemind.vivasmart.core.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun Context.loadActivityIcon(activity: ActivityModel): Single<Drawable> =
        this.loadDrawableAsset("activity_icons/${activity.icon}")

fun Context.loadDrawableAsset(path: String): Single<Drawable> =
        Single.fromCallable {
                Drawable.createFromStream(assets.open(path), null)
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())


