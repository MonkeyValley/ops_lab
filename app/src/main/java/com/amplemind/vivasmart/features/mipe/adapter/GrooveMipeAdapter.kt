package com.amplemind.vivasmart.features.mipe.adapter

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.HydroponicPulseItemBinding
import com.amplemind.vivasmart.vo_core.repository.models.realm.MipeMonitoringModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.realm.Realm

class GrooveMipeAdapter : RecyclerView.Adapter<GrooveMipeAdapter.MipeGrooveViewHolder>() {
    private var list = mutableListOf<Int>()

    var listener: Listener? = null

    var date = ""
    var lotId = 0
    var table = ""
    var week = 0

    fun setData(lotId: Int, table: String, week: Int){
        this.lotId = lotId
        this.table = table
        this.week = week
    }

    private val clickSubject = PublishSubject.create<Int>()

    private var mRecyclerView: RecyclerView? = null

    fun addElements(data: List<Int>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MipeGrooveViewHolder {
        val binding = HydroponicPulseItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MipeGrooveViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MipeGrooveViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem() = clickSubject

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mRecyclerView = null
    }

    inner class MipeGrooveViewHolder(private val binding: HydroponicPulseItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private var item: Int = 0

        fun bind(item: Int) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.btnPulse.text = item.toString()

            binding.btnPulse.setOnClickListener {
                clickSubject.onNext(this.item)
            }

            binding.root.setOnClickListener {
                clickSubject.onNext(this.item)
            }


            Realm.getDefaultInstance().use {
                val result = it
                        .where(MipeMonitoringModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .equalTo("grooveNo", item)
                        .and()
                        .equalTo("week", week)
                        .endGroup()
                        .findFirst()

                if(result != null) {
                    when {
                        result.plants!!.size == 5 -> {
                            if(result.finished!!) binding.btnPulse.setBackgroundColor(Color.parseColor("#3e69df"))
                            else binding.btnPulse.setBackgroundColor(Color.parseColor("#f2dc57"))
                        }
                        result.plants!!.size > 0 -> binding.btnPulse.setBackgroundColor(Color.parseColor("#f2dc57"))
                        else -> binding.btnPulse.setBackgroundColor(Color.parseColor("#ffffff"))
                    }
                }
            }

        }

    }

    interface Listener {
        fun onGetCounterValue(position: Int): Observable<Int>?
    }

}