package com.amplemind.vivasmart.features.phenology.viewmodel

import android.os.Bundle
import com.amplemind.vivasmart.core.repository.PhenologyRepository
import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MenuPhenologyActivitiesViewModel @Inject constructor(private val repository: PhenologyRepository) {

    private var listMenu = mutableListOf<ItemActivitiesPayrollViewModel>()
    private var activitiesSubject = BehaviorSubject.create<List<ItemActivitiesPayrollViewModel>>()
    private var titleToolbar = BehaviorSubject.create<String>()

    fun loadMenuActivities(): Disposable {
        return repository.getPhenologyActivitiesMenu()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { mapModelToViewModels(it) }
                .doOnNext {
                    listMenu.addAll(it)
                }.doOnComplete {
                    activitiesSubject.onNext(listMenu)
                }.subscribe()
    }

    private fun mapModelToViewModels(models: List<PayrollActivitiesModel>): List<ItemActivitiesPayrollViewModel> {
        val list = arrayListOf<ItemActivitiesPayrollViewModel>()
        for (item in models) {
            list.add(ItemActivitiesPayrollViewModel(item))
        }
        return list
    }

    fun getActivities(): BehaviorSubject<List<ItemActivitiesPayrollViewModel>> {
        return activitiesSubject
    }

    fun setupData(arguments: Bundle) {
        titleToolbar.onNext(arguments.getString("Title", ""))
    }

    fun getToolbarTitle(): BehaviorSubject<String> {
        return titleToolbar
    }

}
