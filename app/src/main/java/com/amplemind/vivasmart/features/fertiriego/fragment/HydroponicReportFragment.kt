package com.amplemind.vivasmart.features.fertiriego.fragment

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_DATE
import com.amplemind.vivasmart.features.fertiriego.adapter.FertirriegoHydroponicReportAdapter
import com.amplemind.vivasmart.features.fertiriego.viewModel.FertirriegoHydroponicMonitoringViewModel
import com.amplemind.vivasmart.features.fertiriego.viewModel.PHCEViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PHCEForTable
import com.amplemind.vivasmart.vo_core.repository.models.realm.PulseForTable
import javax.inject.Inject

class HydroponicReportFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: FertirriegoHydroponicMonitoringViewModel

    fun newInstance(date: String, lotId: Int, table: String, mode: String): HydroponicReportFragment {
        val args = Bundle()
        args.putString("DATE", date)
        args.putInt("LOT_ID", lotId)
        args.putString("TABLE", table)
        args.putString("MODE", mode)

        val fragment = HydroponicReportFragment()
        fragment.arguments = args
        return fragment
    }

    var mView: View? = null
    var etPorcentDrain: TextView? = null
    var etCeSend: TextView? = null
    var etPhSend: TextView? = null
    var etCeSqueezed: TextView? = null
    var etPhSqueezed: TextView? = null
    var rvHydroponicReport: RecyclerView? = null

    var lotId = 0
    var date = ""
    var table = ""

    private var hydroponicReportAdapter = FertirriegoHydroponicReportAdapter()

    var hydroponicReportList = ArrayList<PulseForTable>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_hydroponic_report, container, false)

        setUpIu()
        argument()
        viewModel.fragment = this
        setupRecycler()
        addSubscribers()
        loadDataReport()
        return mView
    }

    fun setUpIu(){
        etPorcentDrain = mView!!.findViewById(R.id.et_porcent_drain)
        etCeSend = mView!!.findViewById(R.id.et_ce_send)
        etPhSend = mView!!.findViewById(R.id.et_ph_send)
        etCeSqueezed = mView!!.findViewById(R.id.et_ce_squeezed)
        etPhSqueezed = mView!!.findViewById(R.id.et_ph_squeezed)
        rvHydroponicReport = mView!!.findViewById(R.id.rv_hydroponic_report)
    }

    fun argument(){
        lotId = arguments!!.getInt("LOT_ID", 0) ?: 0
        date = arguments!!.getString("DATE") ?: ""
        table = arguments!!.getString("TABLE") ?: ""

    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccesPHCEDataPerDay().subscribe(this::setDataForPHCE).addTo(subscriptions)
        viewModel.onSuccesPulseForTable().subscribe(this::addElementsPulse).addTo(subscriptions)
    }

    fun loadDataReport(){
        date = FERTIRRIEGO_DATE
        setDataToZero()
        etPorcentDrain!!.text = (viewModel.getPocentTableDrain(lotId,  date, table.toInt()))
        viewModel.getPHCEDataForTable(lotId, date, "T"+table)
        viewModel.getPulseForTable(lotId, date, "T"+table)
    }

    private fun setDataForPHCE(list: List<PHCEForTable>) {
        list.forEach {
            if(it.type == "envio") {
                etPhSend!!.text = if (it.ph != null) it.ph else "-"
                etCeSend!!.text = if (it.ce != null) it.ce else "-"
            }  else if(it.type == "drenado") {
                etPhSqueezed!!.text = if (it.ph != null) it.ph else "-"
                etCeSqueezed!!.text = if (it.ce != null) it.ce else "-"
            }
        }
    }

    fun setDataToZero(){
        etPhSend!!.text = "-"
        etCeSend!!.text = "-"
        etPhSqueezed!!.text = "-"
        etCeSqueezed!!.text = "-"
        hydroponicReportList.clear()
        hydroponicReportAdapter.addElements(hydroponicReportList)
    }

    private fun setupRecycler() {
        rvHydroponicReport!!.hasFixedSize()
        rvHydroponicReport!!.layoutManager = LinearLayoutManager(activity)
        rvHydroponicReport!!.itemAnimator = DefaultItemAnimator()
        rvHydroponicReport!!.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        rvHydroponicReport!!.adapter = hydroponicReportAdapter
    }

    fun addElementsPulse(data: List<PulseForTable>) {
        hydroponicReportList.clear()
        hydroponicReportList.addAll(data)
        hydroponicReportAdapter.addElements(hydroponicReportList)
    }

}
