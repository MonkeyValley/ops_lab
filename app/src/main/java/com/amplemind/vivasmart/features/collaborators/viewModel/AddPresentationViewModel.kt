package com.amplemind.vivasmart.features.collaborators.viewModel

import com.amplemind.vivasmart.core.extensions.registerObserver
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.CollaboratorsRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.AssignCollaboratorModel
import com.amplemind.vivasmart.core.repository.model.PresentationResponse
import com.amplemind.vivasmart.core.repository.model.PresentationResultResponse
import com.amplemind.vivasmart.core.repository.request.UpdatePresentation
import com.amplemind.vivasmart.core.repository.request.UpdatePresentationRequest
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import com.amplemind.vivasmart.core.utils.FLOW_PRESENTATION
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class AddPresentationViewModel @Inject constructor(private val repository: CollaboratorsRepository, var mediator: Mediator,
                                                   private val preferences: UserAppPreferences, private val apiErrors: HandleApiErrors) {

    val presentation = mutableListOf<ItemAddActivitiesViewModel>()

    private val presentationSubject = BehaviorSubject.create<List<ItemAddActivitiesViewModel>>()

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val updateSuccess = BehaviorSubject.create<String>()
    private val requestFail = BehaviorSubject.create<String>()

    private val assingSuccess = BehaviorSubject.create<CollaboratorsInLinesResult>()
    private val searchLocalPresentation = BehaviorSubject.create<Boolean>()

    fun getPresentations(): Disposable {
        return repository.getPresentations(preferences.token, mediator.bundleItemLinesPackage.toString())
                .registerObserver(progressStatus)
                .map {
                    repository.savePresentation(it.presentations)
                    mapModelToViewModels(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { result ->
                    presentation.clear()
                    presentation.addAll(result)
                }
                .subscribe({
                    if(presentation.size == 1) {
                        presentation.first().selected(true)
                    }
                    presentationSubject.onNext(presentation)
                }, {
                    searchLocalPresentation.onNext(true)
                })
    }

    fun onSearchLocalPresentation(): BehaviorSubject<Boolean> {
        return searchLocalPresentation
    }

    fun getLocalPresentations(): Disposable {
        return repository.getPresentationsLocal(mediator.bundleItemLinesPackage!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    mapModelToViewModels(PresentationResponse(it))
                }
                .subscribe({ result ->
                    presentation.clear()
                    presentation.addAll(result)
                    if(presentation.size == 1) {
                        presentation.first().selected(true)
                    }
                    presentationSubject.onNext(presentation)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModels(result: PresentationResponse): List<ItemAddActivitiesViewModel> {
        val list = arrayListOf<ItemAddActivitiesViewModel>()

        for (item in result.presentations) {
            list.add(ItemAddActivitiesViewModel(presentation = item, flow = FLOW_PRESENTATION))
        }
        return list
    }

    fun getData(): BehaviorSubject<List<ItemAddActivitiesViewModel>> {
        return presentationSubject
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onAssingCollaboratorSuccess(): BehaviorSubject<CollaboratorsInLinesResult> {
        return assingSuccess
    }

    fun assignCollaborator(hasInternet: Boolean, idCollaborator: List<CollaboratorModel>, packing: PresentationResultResponse?, activity_id: Int, nameLine: String?, collaborator_left: Int?): Disposable? {
        val list = mutableListOf<AssignCollaboratorModel>()

        val idLine = mediator.bundleItemLinesPackage!!

        for (user in idCollaborator) {
            list.add(AssignCollaboratorModel(idLine, activity_id, user.id, packing?.id
                    ?: -1, false))
        }
        return if (hasInternet) {
            assignCollaboratorWithBoxOnline(list)
        } else {
            assignCollaboratorWithBoxOffline(list, idCollaborator, nameLine ?: "", collaborator_left
                    ?: 0, activity_id, idLine, packing)
        }
    }

    private fun assignCollaboratorWithBoxOffline(list: MutableList<AssignCollaboratorModel>, idCollaborator: List<CollaboratorModel>, nameLine: String,
                                                 collaborator_left: Int, activity_id: Int, idLine: Int, packing: PresentationResultResponse?): Disposable {

        return repository.saveCollaboratorsAndLineLocal(list, idCollaborator, nameLine, collaborator_left, activity_id, idLine, packing, mediator.getMapCountBoxRequest())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({ line ->
                    assingSuccess.onNext(line)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })

    }

    private fun assignCollaboratorWithBoxOnline(list: List<AssignCollaboratorModel>): Disposable {
        val assign = CollaboratorsRepository.AssingCollaborators(list, mediator.getMapCountBoxRequest())

        return repository.assingCollaborator(token = preferences.token, assing = assign)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    assingSuccess.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun searchPresentation(query: String) {
        if (query.isEmpty()) {
            presentationSubject.onNext(presentation)
        }
        presentationSubject.onNext(presentation.filter { it.name!!.toLowerCase().contains(query.toLowerCase()) })
    }

    fun updateCollaborator(hasInternet: Boolean, registerId: Int, presentation: PresentationResultResponse?): Disposable {
        val observer = if (hasInternet) {
            updateCollaboratorOnline(registerId, presentation?.id ?: 0)
        } else {
            updateCollaboratorOffline(registerId, presentation)
        }
        return observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    updateSuccess.onNext(presentation?.name ?: "")
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun updateCollaboratorOnline(registerId: Int, presentation: Int?): Observable<Any> {
        val request = UpdatePresentationRequest(listOf(UpdatePresentation(registerId, presentation, mediator.bundleItemLinesPackage!!)))
        return repository.updatePresentation(preferences.token, request)
    }

    private fun updateCollaboratorOffline(registerId: Int, presentation: PresentationResultResponse?): Observable<Any> {
        return repository.updatePresentationOffline(registerId, presentation!!, mediator.bundleItemLinesPackage!!)
    }

    fun onUpdateSuccess(): BehaviorSubject<String> {
        return updateSuccess
    }


}
