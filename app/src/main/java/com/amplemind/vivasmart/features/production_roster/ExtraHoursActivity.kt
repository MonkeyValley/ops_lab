package com.amplemind.vivasmart.features.production_roster

import android.net.Uri
import android.os.Bundle
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.production_roster.fragments.ExtraHoursAuthorizationFragment
import com.amplemind.vivasmart.features.production_roster.fragments.ExtraHoursCreateRequestFragment
import com.amplemind.vivasmart.features.production_roster.fragments.ExtraHoursRequestsFragment
import com.amplemind.vivasmart.features.production_roster.fragments.ExtraHoursTabHostFragment
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import kotlinx.android.synthetic.main.custom_toolbar.*


class ExtraHoursActivity : BaseActivityWithFragment(), ExtraHoursRequestsFragment.OnFragmentInteractionListener, ExtraHoursAuthorizationFragment.OnFragmentInteractionListener,
        ExtraHoursTabHostFragment.OnFragmentInteractionListener, ExtraHoursCreateRequestFragment.OnFragmentInteractionListener  {


    lateinit var mStage: StageModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extra_hours)

        //mStage = intent.getParcelableExtra("stage")
        initToolbar()
        addFragmentToActivity()


    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    public fun addFragmentToActivity() {
        val fragment = ExtraHoursTabHostFragment.newInstance(mStage)

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.extrahours_fragment_container, fragment, "Extra")
                .commit()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = mStage.lot?.name

    }
}
