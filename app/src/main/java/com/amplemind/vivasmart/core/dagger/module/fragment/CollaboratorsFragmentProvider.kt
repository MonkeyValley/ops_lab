package com.amplemind.vivasmart.core.dagger.module.fragment

import com.amplemind.vivasmart.core.base.dagger.scope.FragmentScope
import com.amplemind.vivasmart.features.collaborators.fragment.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CollaboratorsFragmentProvider {

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindCollaboratorsDataBaseFragment(): CollaboratorsDataBaseFragment


    @ContributesAndroidInjector//(modules = [(DatabaseDaoModule::class)])
    @FragmentScope
    internal abstract fun bindCollaboratorsRecentFragment(): CollaboratorsRecentFragment


    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindGroovesFragment(): GroovesFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindBarcodeScannerFragment(): BarcodeScannerFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindCollaboratorsFragment(): CollaboratorsFragment

}