package com.amplemind.vivasmart.vo_core.repository.remote

import com.amplemind.vivasmart.features.pollination.repository.responses.PollinationObjectResponse
import com.amplemind.vivasmart.features.pollination.repository.responses.Pollinationrevisions
import com.amplemind.vivasmart.vo_core.repository.models.realm.MipeMonitoringModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangesDataModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectSingleResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface StagesApi {

    //embed=crop,lot,lot.business_unit,varieties,varieties.variety,crop.plagues,crop.plagues.plague,crop.plagues.plague.images
    @GET("/v2/stage")
    @Headers("Content-Type: application/json")
    fun getStages(
            @Header("Authorization")        authentication: String,
            @Query("embed")                 embed: String = "crop,lot,lot.business_unit,varieties," +
                    "varieties.variety,crop.plagues,crop.plagues.plague,crop.plagues.plague.images",
            @Query("lot.business_unit_id")  businessUnitId: Long,
            @Query("lot.is_active")         isEnabled: Boolean = true,
            @Query("is_active")             stageIsEnabled: Boolean = true,
            @Query("stage_activities")      stageActivities: Boolean = true,
            @Query("order_by")              order: String = "lot:name.asc",
            @Query("__logic")               logic: String = "AND",
            @Query("version__gte")          version: Long? = null,
            @Query("limit")                 limit: Int = 100000
    ): Single<ObjectResponse<StageModel>>

    //embed=lot,lot.business_unit,varieties,varieties.variety
    @GET("/v2/stage")
    @Headers("Content-Type: application/json")
    fun getProductionRangeStage(
            @Header("Authorization")        authentication: String,
            @Query("embed")                 embed: String = "lot,lot.business_unit,varieties," +
                    "varieties.variety",
            @Query("lot.business_unit_id")  businessUnitId: Long,
            @Query("lot.is_active")         isEnabled: Boolean = true,
            @Query("is_active")             stageIsEnabled: Boolean = true,
            @Query("stage_activities")      stageActivities: Boolean = true,
            @Query("order_by")              order: String = "lot:name.asc",
            @Query("__logic")               logic: String = "AND",
            @Query("version__gte")          version: Long? = null,
            @Query("limit")                 limit: Int = 100000
    ): Single<ObjectResponse<StageModel>>


    ///v2/mipe/monitoring/week/{lot_id}
    @GET("/v2/mipe/monitoring/week/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getDataPerWeekMipe(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<ObjectResponse<MipeMonitoringModel>>

    //https://dev.vivasmart.com.mx/v2/production_var/lastweek/11
    @GET("/v2/production_var/lastweek/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getProductionRangeWeeksData(
            @Header("Authorization")    authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<ObjectSingleResponse<ProductionRangesDataModel>>

    //https://dev.vivasmart.com.mx/v2/pollination/lastweek/11
    @GET("/v2/pollination/lastweek/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getPollinationLastWeeks(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<PollinationObjectResponse<Pollinationrevisions>>

}