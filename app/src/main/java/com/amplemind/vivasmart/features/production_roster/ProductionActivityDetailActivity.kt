package com.amplemind.vivasmart.features.production_roster

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_roster.adapters.ProductionActivityDetailAdapter
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ProductionActivityDetailItemViewModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ProductionActivityDetailViewModel
import kotlinx.android.synthetic.main.activity_production_detail.*
import javax.inject.Inject

class ProductionActivityDetailActivity : BaseActivity() {

    companion object {

        val TAG = ProductionActivityDetailActivity::class.java

        val PARAM_ACTIVITY  = "$TAG.Activity"
        val PARAM_STAGE     = "$TAG.Stage"
    }

    @Inject
    lateinit var viewModel: ProductionActivityDetailViewModel

    private lateinit var progressDialog: ProgressDialog

    private lateinit var mActivity: ActivityModel
    private lateinit var mStage: StageModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_production_detail)

        //mActivity = intent.getParcelableExtra(PARAM_ACTIVITY)
        //mStage = intent.getParcelableExtra(PARAM_STAGE)

        setupToolbar()
        initRecycler()
        initProgress()
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.initialize(mActivity, mStage)
        //mViewModel.onProgressStatus().subscribe(this::showProgress).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::showError).addTo(subscriptions)
        viewModel.onRequestSuccess().subscribe(this::updateCodes).addTo(subscriptions)
        viewModel.onNewCodeCreated().subscribe(this::onNewCodeCreated).addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getActivityCodes(this).addTo(subscriptions)
    }

    private fun initProgress() {
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle(getString(R.string.app_name))
        progressDialog.setMessage(getString(R.string.server_connection))
        progressDialog.setCancelable(false)
    }

    private fun initRecycler() {
        rv_production_detail.hasFixedSize()
        rv_production_detail.layoutManager = LinearLayoutManager(this)
        rv_production_detail.adapter = ProductionActivityDetailAdapter(this)
        (rv_production_detail.adapter as ProductionActivityDetailAdapter).onItemClicked().subscribe(this::onItemClick).addTo(subscriptions)
    }

    private fun showProgress(show: Boolean) {
        if (show) progressDialog.show() else progressDialog.dismiss()
    }

    private fun showError(error: String) {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.app_name))
                .setMessage(error)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }.show()
    }

    private fun updateCodes(items: List<ProductionActivityDetailItemViewModel>) {
        (rv_production_detail.adapter as ProductionActivityDetailAdapter).addElements(items)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_production_detail.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //TODO: get real name
        supportActionBar!!.title = mActivity.name
    }

    private fun onNewCodeCreated(itemViewModel: ProductionActivityDetailItemViewModel) {
        //mActivityCodeUploader.create(this, itemViewModel.activityCode!!)
        onItemClick(itemViewModel)
    }

    private fun onItemClick(itemViewModel: ProductionActivityDetailItemViewModel) {
        if (itemViewModel.isNewCode()) {
            viewModel.createActivityCode().addTo(subscriptions)
        } else {
            val intent = Intent(this, TimerActivity::class.java)

            intent.putExtra(TimerActivity.PARAM_ACTIVITY_UUID, mActivity.uuid)
            intent.putExtra(TimerActivity.PARAM_STAGE_UUID, mStage.uuid)

            startActivityForResult(intent, 200)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            200 -> {
                finish()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
