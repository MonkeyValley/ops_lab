package com.amplemind.vivasmart.domain.rosterpackage

import android.util.Log
import android.util.Log.e
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.platform.SyncNotification
import com.amplemind.vivasmart.core.platform.SynchronizeDataBaseService
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.TimerLinesRepository
import com.amplemind.vivasmart.core.repository.local.CollaboratorsLineDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.PicturesHelper
import org.jetbrains.anko.doAsync
import java.io.File
import javax.inject.Inject

class UploadRosterPackage @Inject constructor(private var database: CollaboratorsLineDao,
                                              private val picturesHelper: PicturesHelper,
                                              private val uploadRepository: ProfileRepository,
                                              private val preferences: UserAppPreferences,
                                              private val collaboratorRepository: TimerLinesRepository) {

    private var delegate: SyncNotification? = null
    private val type = SynchronizeDataBaseService.SynchronizeRequest.ROSTER_PACKAGE
    private var subscriptions : AndroidDisposable? = null

    fun collaboratorsPackageReadyToSync(delegate: SyncNotification, subscriptions : AndroidDisposable) {
        this.delegate = delegate
        this.subscriptions = subscriptions

        val sync_collaborators = mutableListOf<SyncRosterPackageModel>()

        doAsync({
            e("Error","collaboratorsPackageReadyToSync ${it.message}")
            delegate.onTerminate(mutableListOf(),type, "collaboratorsPackageReadyToSync ${it.message}")
        },{
            val reports = database.getAllCollaboratorInPackage()

            reports.forEach { item ->
                val presentation = mutableListOf<SyncRosterPresentation>()

                val details = database.getReportDetail(item.id!!, item.packingline_id!!)

                var sign = if (item.sign != null) item.sign else "No Sign"

                if (details.isNotEmpty()){
                    sign = details.first().sign
                }

                val sync = SyncRosterPackageModel(item.collaborator?.employeeCode ?: "",
                        item.packingline_id, item.activity_id!!, item.is_time ?: false,
                        item.packing_id, sign, item.temp_time.toInt(),
                        item.isTraining ?: false, item.isPractice ?: false,item.is_paused,
                        item.is_done ?: false, true, item.id!!, presentation)

                details.forEach { box ->
                    sync.is_training = box.is_training
                    presentation.add(SyncRosterPresentation(box.packing_id!!, box.quantity ?: 0.0, box.id_lot, box.time?.toInt() ?: 0))
                }

                sync_collaborators.add(sync)
            }

            uploadImage(sync_collaborators, 0)
        })
    }

    private fun uploadImage(collaborators: List<SyncRosterPackageModel>, position: Int) {
        if (position < collaborators.size) {
            if (collaborators[position].sign == null || !File(collaborators[position].sign).exists()) {
                collaborators[position].sign = "No Sign"
                uploadImage(collaborators, position + 1)
                return
            }
            val file = File(collaborators[position].sign)
            picturesHelper.compressImage(file, file, 400f, 400f)

            uploadRepository.uploadImage(file, "user")
                    .subscribe({
                        collaborators[position].sign = it.relative_url
                        uploadImage(collaborators, position + 1)
                    }, {
                        Log.e("Fail upload","Error ${it.localizedMessage}")
                        collaborators[position].sign = "No Sign"
                        uploadImage(collaborators, position + 1)
                    }).addTo(subscriptions!!)
        } else {
            deleteCollaboratorsOnline(collaborators)
        }
    }

    /**
     *  Send request to remove collaborators that are assigned online
     *  but offline were eliminated
     */
    private fun deleteCollaboratorsOnline(collaborators: List<SyncRosterPackageModel>) {
        collaboratorRepository.syncRosterDeleteCollaborators(preferences.token)
                .subscribe({
                    syncCollaborators(collaborators)
                }, {
                    syncCollaborators(collaborators)
                    Log.e("Fail upload", "Error ${it.message}")
                }).addTo(subscriptions!!)
    }

    private fun syncCollaborators(collaborators: List<SyncRosterPackageModel>) {
        val request = SyncRosterPackageRequest(collaborators)

        collaboratorRepository.syncRosterPackageCollaborators(preferences.token, request)
                .subscribe({ syncCollaborators ->
                    val reports = mutableListOf<SyncReportModel>()
                    syncCollaborators.data.forEach { sync ->
                        if (reports.none { it.name == sync.employeeCode }) {

                            database.updateReportDetail(sync.offline_id, sync.packingline_id, sync.status)

                            val details = database.getReportDetail(sync.offline_id, sync.packingline_id)

                            val total = details.size
                            val complete = details.filter { it.status_sync  == true}.size

                            reports.add(SyncReportModel(sync.employeeCode, total, complete, sync.offline_id))
                        }
                    }
                    delegate?.onTerminate(reports, type)
                }, {
                    finishWithErrors(collaborators)
                }).addTo(subscriptions!!)
    }

    private fun finishWithErrors(collaborators: List<SyncRosterPackageModel>) {
        val reports = collaborators.map { sync ->
            val total = collaborators.filter { it.employeeCode == sync.employeeCode }.size

            database.updateReportDetail(sync.offline_id, sync.packingline_id, sync.status)

            SyncReportModel(sync.employeeCode, total, 0, sync.offline_id)
        }
        delegate?.onTerminate(reports, type)
    }
    //TODO: BoxCountResultResponse falta enviar el total

}
