package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class IncidentModel (
        @SerializedName("id")           val id: Int,
        @SerializedName("created_at")   val createdAt: String,
        @SerializedName("description")  val description: String,
        @SerializedName("groove_no")    val grooveNumber: Int,
        @SerializedName("table_no")     val tableNumber: Int,
        @SerializedName("issue")        val issue: IssueModel,
        @SerializedName("is_closed")    val isClosed: Boolean
)


