package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import com.amplemind.vivasmart.core.utils.CATEGORY_ID
import com.amplemind.vivasmart.vo_core.repository.CollaboratorsRepository
import javax.inject.Inject

class CollaboratorsViewModel @Inject constructor(
        private val collaboratorsRepository: CollaboratorsRepository
) {

    var selectedUuids = ArrayList<String>()

    fun cleanUp() {
        collaboratorsRepository.cleanUp()
    }

    fun hasCustomUuids() = selectedUuids.isNotEmpty()

    fun find(query: String = "") =
            if (hasCustomUuids()) {
                collaboratorsRepository.findByUuidAndCodeQuery(selectedUuids, query)
            }
            else {
                collaboratorsRepository.find(query)
            }

    fun findByActivity(query: String = "") =
            if (hasCustomUuids()) {
                CATEGORY_ID = 0
                collaboratorsRepository.findActivityByUuidAndCodeQuery(selectedUuids, query)
            }
            else {
                CATEGORY_ID = 0
                collaboratorsRepository.findByActivity(query)
            }

}