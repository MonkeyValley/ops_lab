package com.amplemind.vivasmart.features.packaging_quality

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.packaging_quality.viewModel.CarryOrderCondensedViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.PackagingQualityViewModel
import kotlinx.android.synthetic.main.content_packagin_activity.*
import javax.inject.Inject

@SuppressLint("Registered")
open class PackagingQualityActivity : BaseActivity() {

    private val DETAIL_ORDER_CODE = 1212

    @Inject
    lateinit var viewModel: PackagingQualityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_packagin_activity)

        setupToolbar()

        initRecycler()

        viewModel.getList().subscribe(this::setData).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.getDataQualityPackages().addTo(subscriptions)
    }

    /**
     *  call new activity detail orden
     */
    fun clickItemList(item: CarryOrderCondensedViewModel) {
        startActivityForResult(Intent(this, DetailOrdenActivity::class.java).putExtra("id", item.id), DETAIL_ORDER_CODE)
    }

    /**
     *  get data and asign click in mItems
     */
    fun setData(data: List<CarryOrderCondensedViewModel>) {
        (rv_quality.adapter as QualityAdapter).addElements(data)
        (rv_quality.adapter as QualityAdapter).getClickItem().subscribe(this::clickItemList).addTo(subscriptions)

        showImageNoData(data.isEmpty())
    }

    fun showImageNoData(isVisible: Boolean) {
        no_found_activities.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    /**
     *  create adapter for recyclerview
     */
    private fun initRecycler() {
        rv_quality.hasFixedSize()
        rv_quality.layoutManager = LinearLayoutManager(this)
        rv_quality.itemAnimator = DefaultItemAnimator()
        rv_quality.adapter = QualityAdapter()
        rv_quality.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
    }

    /**
     *  change title in toolbar
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_package
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Calidad en empaque"
    }


    /**
     *  menu search code
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_quality, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     *  call quality search code activity
     */
    fun showQualitySearchCode() {
        startActivity(Intent(this, QualitySearchCodeActivity::class.java))
    }

    /**
     *  finish this activity
     */
    fun finishThisActivity() {
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finishThisActivity()
                return true
            }
            R.id.search_package -> {
                showQualitySearchCode()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == DETAIL_ORDER_CODE && resultCode == Activity.RESULT_OK) {
            val carryOrderId = data!!.getIntExtra("carryOrderId", 0)
            viewModel.deleteCarryOrder(carryOrderId)
            (rv_quality.adapter as QualityAdapter).notifyDataSetChanged()
            showImageNoData((rv_quality.adapter as QualityAdapter).isEmpty())
        }
    }

}
