package com.amplemind.vivasmart.feature.ChangeLot

import com.amplemind.vivasmart.core.repository.ChangeLotRepository
import com.amplemind.vivasmart.features.production_roster.ChangeLotViewModel
import org.junit.Before
import org.junit.Test
import kotlin.test.assertNotNull

class ChangeLotViewModelTest {

    private var viewModel : ChangeLotViewModel? = null

    @Before
    fun setup(){
        viewModel = ChangeLotViewModel(ChangeLotRepository())
    }

    @Test
    fun `not null view model`(){
        assertNotNull(viewModel)
    }

    @Test
    fun `get batches behavior`(){
        viewModel!!.loadData()
    }

}