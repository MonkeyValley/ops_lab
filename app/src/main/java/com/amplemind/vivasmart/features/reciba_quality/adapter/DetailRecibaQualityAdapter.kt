package com.amplemind.vivasmart.features.reciba_quality.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemRecibaReviewDetailBinding
import com.amplemind.vivasmart.databinding.ItemReportRecibaQualityProductBinding
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ItemDetailRecibaQualityViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.BehaviorSubject
import java.util.*

class DetailRecibaQualityAdapter constructor(val list: MutableList<ItemDetailRecibaQualityViewModel>) : RecyclerView.Adapter<DetailRecibaQualityAdapter.DetailRecibaQualityViewHolder>() {

    private val onClickSubject = BehaviorSubject.create<ItemDetailRecibaQualityViewModel>()

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailRecibaQualityViewHolder {
        context = parent.context
        val binding = ItemRecibaReviewDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DetailRecibaQualityViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: DetailRecibaQualityViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<ItemDetailRecibaQualityViewModel> {
        return onClickSubject
    }

    fun isEmpty() = list.isEmpty()


    inner class DetailRecibaQualityViewHolder(private val binding: ItemRecibaReviewDetailBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemDetailRecibaQualityViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {

                binding.TVPercentExport.text = "%.2f".format((item.export.toDouble() / item.total.toDouble()) * 100)
                binding.TVPercentDamage.text = "%.2f".format((item.damaged.toDouble() / item.total.toDouble()) * 100)

                binding.btnDamagePermanent.setOnClickListener {
                    if (binding.tlDamagePermanent.visibility == View.GONE) binding.tlDamagePermanent.visibility = View.VISIBLE
                    else binding.tlDamagePermanent.visibility = View.GONE
                }

                binding.btnDamageCondition.setOnClickListener {
                    if (binding.tlDamageCondition.visibility == View.GONE) binding.tlDamageCondition.visibility = View.VISIBLE
                    else binding.tlDamageCondition.visibility = View.GONE
                }

                var count = 0
                if (item.issuesPermanent.size != 0) {
                    for (item in item.issuesPermanent) {
                        if(item.frequencyNo != 0) {
                            binding.tlDamagePermanentIssue.addView(createRow(item.name, Gravity.LEFT), 0)
                            binding.tlDamagePermanentQuantity.addView(createRow(item.frequencyNo.toString(), Gravity.CENTER), 0)
                            binding.tlDamagePermanentPercentage.addView(createRow(item.percentage.toString(), Gravity.CENTER), 0)
                            count++
                        }
                    }
                } else binding.btnDamagePermanent.visibility = View.GONE
                if(count == 0)  binding.btnDamagePermanent.visibility = View.GONE

                count = 0
                if (item.issuesCondition.size != 0) {
                    for (item in item.issuesCondition) {
                        if(item.frequencyNo != 0) {
                            binding.tlDamageConditionIssue.addView(createRow(item.name, Gravity.LEFT), 0)
                            binding.tlDamageConditionQuantity.addView(createRow(item.frequencyNo.toString(), Gravity.CENTER), 0)
                            binding.tlDamageConditionPercentage.addView(createRow(item.percentage.toString(), Gravity.CENTER), 0)
                            count++
                        }
                    }
                } else binding.btnDamageCondition.visibility = View.GONE
                if(count == 0) binding.btnDamageCondition.visibility = View.GONE

            }
        }

    }


    private fun createRow(data: String, gravity: Int): TableRow {
        val row = TableRow(context)
        row.gravity = gravity
        row.setBackgroundResource(R.drawable.row_border)
        val lp: TableRow.LayoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        row.layoutParams = lp
        val tv = TextView(context)
        tv.text = data
        row.addView(tv)
        return row
    }
}