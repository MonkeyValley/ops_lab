package com.amplemind.vivasmart.vo_features.services.sync

import android.content.res.Resources
import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.vo_core.repository.dao.realm.DownloadObjectDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.Downloader
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.DownloaderHolder
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.RequestType
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.Role
import com.amplemind.vivasmart.vo_features.services.ConnectivityManager
import io.reactivex.*
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SyncDownloadModule @Inject constructor(
        private val mDownloaderHolder: DownloaderHolder,
        private val mPref: UserAppPreferences,
        private val mConnectivityManager: ConnectivityManager,
        private val mDownloadObjectDao: DownloadObjectDao,
        private val mEventBus: EventBus
) : SyncModule() {

    companion object {

        private val TAG = SyncDownloadModule::class.java.simpleName

        private const val DAYS = -21
    }

    init {
        mDownloaderHolder.setDateRange(DAYS)
    }

    private var mFirstSyncCheckDisposable: Disposable? = null

    var onEvent = BehaviorSubject.createDefault(DownloadEvent(RequestType.UNKNOWN, EventType.NOT_READY))
    var retry: Flowable<Boolean>? = null

    fun setUp() {
        mFirstSyncCheckDisposable = checkIfReady()
                .filter {isReady -> isReady }
                .flatMapSingle {
                    mDownloadObjectDao.needsFirstSync()
                }
                .subscribeOn(subscriptionScheduler)
                .onMainThread()
                .subscribe { firstSyncNeeded ->
                    Log.e(TAG, "FirstSyncNeeded $firstSyncNeeded")
                    if (firstSyncNeeded && mPref.token.isNotEmpty()) {
                        mEventBus.send(DownloadEvent(RequestType.UNKNOWN, EventType.FIRST_SYNC_NEEDED))
                    }
                }
    }

    override fun cleanUp() {
        mFirstSyncCheckDisposable?.dispose()
        mDownloaderHolder.cleanUp()
    }

    fun downloadOnDemand(node: Role.Node): Completable =
            checkIfReady()
                    .filter { isReady -> isReady}
                    .switchMapCompletable {
                        buildSyncObservable(node, null, true)
                    }
                    .doOnError {error ->
                        Log.e(TAG, Log.getStackTraceString(error))
                    }
                    .subscribeOn(subscriptionScheduler)

    fun download(requestType: RequestType = RequestType.UNKNOWN, force: Boolean = false): Completable =
            checkIfReady()
                .filter { isReady -> isReady }
                .switchMapCompletable {
                    buildDownloadCompletable(requestType, force)
                }
                .doOnError {error ->
                    Log.e(TAG, Log.getStackTraceString(error))
                }
                .subscribeOn(subscriptionScheduler)
                .onMainThread()

    fun downloadForce(requestType: RequestType = RequestType.UNKNOWN, force: Boolean = false): Completable =
            checkIfReady()
                    .filter { isReady -> isReady }
                    .switchMapCompletable {
                        buildDownloadCompletable(requestType, force)
                    }
                    .doOnError {error ->
                        Log.e(TAG, Log.getStackTraceString(error))
                    }
                    .subscribeOn(subscriptionScheduler)
                    .onMainThread()

    private fun checkIfReady(): Observable<Boolean> =
            onReady.distinctUntilChanged()
                    .onMainThread()
                    .doOnNext { isReady ->
                        if (isReady) {
                            sendEvent(RequestType.UNKNOWN, EventType.READY)
                        }
                        else {
                            sendEvent(RequestType.UNKNOWN, EventType.NOT_READY)
                        }
                    }
                    .onCustomScheduler()

    private fun buildDownloadCompletable(requestType: RequestType, force: Boolean): Completable =
            getSyncTree(requestType)
                    .switchMapCompletable{tree ->
                        Log.e(TAG, "CREATING SYNC REGISTERS")
                        mDownloadObjectDao.createSyncRegistersIfNotExists( tree )
                                .andThen (
                                        buildSyncObservable(tree, null, force)
                                )
                                .andThen (
                                        mDownloadObjectDao.resetRequests(requestType)
                                )
                    }
                    .doOnComplete {
                        mDownloaderHolder.clearCache()
                    }
                    .onMainThread()
                    .doOnComplete {
                        sendEvent(RequestType.UNKNOWN, EventType.COMPLETE)
                    }
                    .onCustomScheduler()

    private fun getSyncTree(requestType: RequestType): Observable<Role.Node> =
            Observable.fromCallable {
                val tree = Role.getForRoleId( mPref.user.roleId )
                        .getFirstSyncTree()

                findRequestTypeInSyncTree(requestType, tree)
            }

    private fun findRequestTypeInSyncTree(requestType: RequestType, node: Role.Node): Role.Node =
            if (requestType == RequestType.UNKNOWN || node.requestType == requestType) {
                node
            } else {
                if (node.children.isEmpty()) {
                    throw Resources.NotFoundException("Type '${requestType.remoteName} not found in this sync tree'")
                } else {
                    var found: Role.Node = node
                    for (child: Role.Node in node.children) {
                        found = findRequestTypeInSyncTree(requestType, child)
                    }
                    found
                }
            }

    private fun buildSyncObservable(node: Role.Node, parent: Any?, force: Boolean): Completable =
            if (node.requestType == RequestType.UNKNOWN) {
                buildChildrenCompletable(node, parent, force)
            } else {
                mDownloadObjectDao.getForRequestType(node.requestType)
                        .doOnSuccess { obj ->
                            val remoteName = node.requestType.remoteName
                            when {
                                !obj.firstSyncDone ->
                                    Log.e(TAG, "Type '$remoteName' has never been synced")
                                obj.timesRequested > 0 ->
                                    Log.e(TAG, "Type '$remoteName has a sync pending'")
                                else ->
                                    Log.e(TAG, "Type '$remoteName' is synced'")
                            }
                        }
                        .flatMapSingle { obj ->
                            val firstSync = !obj.firstSyncDone
                            val makeServerRequest = force || firstSync || obj.timesRequested >= 0
                                buildRequestSingle(node, parent, makeServerRequest, firstSync)
                        }
                        .toObservable()
                        .concatMapIterable { it }
                        .concatMapCompletable { item ->
                            buildChildrenCompletable(node, item, force)
                        }

            }

    private fun buildChildrenCompletable(node: Role.Node, parent: Any?, force: Boolean): Completable =
            Observable.fromIterable(node.children)
                    .concatMapCompletable { child ->
                        buildSyncObservable(child, parent, force)
                    }


    private fun buildRequestSingle(node: Role.Node, parent: Any?, makeServerRequest: Boolean, firstSync: Boolean): Single<out List<Any?>> {

        val downloader =
                if (firstSync) mDownloaderHolder.getDownloaderForRequestType(node.requestType)
                else mDownloaderHolder.getDownloaderForRequestTypeForce(node.requestType)

        return if (makeServerRequest) {
            getLastVersion(downloader, firstSync)
                .onMainThread()
                .doOnSuccess{
                    sendEvent(node.requestType, EventType.DOWNLOADING)
                }
                .onCustomScheduler()
                .flatMap { lastVersion ->
                    downloader.buildServerRequest(parent, lastVersion)
                }
                .onMainThread()
                .doOnSuccess {
                    sendEvent(node.requestType, EventType.SAVING)
                }
                .onCustomScheduler()
                .flatMap{ children ->
                    downloader.saveServerResponse(parent, children)
                }
                .onMainThread()
                .doOnError { error ->
                    sendError(node.requestType, error)
                }
                .onCustomScheduler()
                .retryWhen { errors ->
                    errors.switchMap { error ->
                        when (error) {
                            is SocketTimeoutException -> {
                                buildCountdownTimer()
                                        .onMainThread()
                                        .doOnNext { remainingSeconds ->
                                            sendCountdownEvent(node.requestType, remainingSeconds)
                                        }
                                        .onCustomScheduler()
                                        .takeLast(1)
                            }
                            is UnknownHostException -> {
                                mConnectivityManager.notifyNoInternetDetected()
                                mConnectivityManager.onConnectionChanged
                                        .onCustomScheduler()
                                        .filter { hasInternet -> hasInternet }
                                        .toFlowable(BackpressureStrategy.DROP)
                            }
                            else -> {
                                retry ?: Flowable.error(error)
                            }
                        }
                    }
                }
        } else {
            downloader.findAll()
        }
    }

    private fun getLastVersion(downloader: Downloader<Any?, Any>, firstSync: Boolean): Single<Long> =
            if (firstSync) {
                Single.just(0L)
            }
            else {
                downloader.getLastVersion()
           }

    private fun buildCountdownTimer() =
            Flowable.intervalRange(0, 16, 2, 1, TimeUnit.SECONDS)
                    .map { value ->
                        15 - value.toInt()
                    }

    private fun sendEvent(requestType: RequestType, eventType: EventType) =
            onEvent.onNext(DownloadEvent(requestType, eventType))

    private fun sendCountdownEvent(requestType: RequestType, remainingSeconds: Int) =
            onEvent.onNext(RetryCountdownEvent(requestType, remainingSeconds))

    private fun sendError(requestType: RequestType, error: Throwable) =
            onEvent.onNext(DownloadErrorEvent(requestType, error))


    open class DownloadEvent(
            val requestType: RequestType,
            val eventType: EventType
    )

    class DownloadErrorEvent(
            requestType: RequestType,
            val error: Throwable
    ): DownloadEvent(requestType, EventType.ERROR)


    class RetryCountdownEvent(
            requestType: RequestType,
            val remainingSeconds: Int
    ): DownloadEvent(requestType, EventType.COUNTDOWN)

    enum class EventType {
        NOT_READY,
        READY,
        FIRST_SYNC_NEEDED,
        DOWNLOADING,
        SAVING,
        COUNTDOWN,
        ERROR,
        COMPLETE
    }

}