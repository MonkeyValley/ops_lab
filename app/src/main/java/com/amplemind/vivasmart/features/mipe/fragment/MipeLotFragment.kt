package com.amplemind.vivasmart.features.mipe.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.getSpanCount
import com.amplemind.vivasmart.core.extensions.toDp
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.mipe.MipeSelectTableActivity
import com.amplemind.vivasmart.features.mipe.adapter.LotsInMipeAdapter
import com.amplemind.vivasmart.features.mipe.viewModel.LotMipeItemViewModel
import com.amplemind.vivasmart.features.mipe.viewModel.MipeLotViewModel
import com.amplemind.vivasmart.features.pollination.PollinationActivity
import com.amplemind.vivasmart.features.production_range.ProductionRangeActivity
import dagger.android.DispatchingAndroidInjector
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_mipe_lot.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MipeLotFragment : BaseFragment(), LotsInMipeAdapter.Listener {

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    private var mListener: LotsInMipeAdapter.Listener? = null

    private var mAdapter = LotsInMipeAdapter()

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    @Inject
    lateinit var viewModel: MipeLotViewModel

    fun newInstance(title: String, type: String): MipeLotFragment {
        val args = Bundle()
        args.putString("TITTLE", title)
        args.putString("TYPE", type)

        val fragment = MipeLotFragment()
        fragment.arguments = args
        return fragment
    }

    var tittle = ""
    var type = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mipe_lot, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        readArguments()
        setupRecycler()

        mAdapter.type = type
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getLots()
        viewModel.onSuccessRequest_lots().subscribe(this::addElements).addTo(subscriptions)
    }

    private fun readArguments() {
        tittle = arguments!!.getString("TITTLE") ?: ""
        type = arguments!!.getString("TYPE") ?: ""
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun getData() {
        viewModel.getLots()
    }

    fun addElements(data: List<LotMipeItemViewModel>) {
        mAdapter.addElements(data)
    }

    private fun clickItemLot() {
        mAdapter.onClickItem()
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe(this::showLotDetail).addTo(subscriptions)
    }

    private fun showLotDetail(item: LotMipeItemViewModel) {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now

        when (type) {
            "1" -> {
                val intent = Intent(context, MipeSelectTableActivity::class.java).apply {
                    putExtra(MipeSelectTableActivity.TAG_NAME, tittle)
                    putExtra(MipeSelectTableActivity.LOT_ID, item.lot_id.toString())
                    putExtra(MipeSelectTableActivity.LOT_NAME, item.lot_name)
                }
                startActivity(intent)
            }
            "2" -> {
                val intent = Intent(context, ProductionRangeActivity::class.java).apply {
                    putExtra(ProductionRangeActivity.TAG_NAME, tittle)
                    putExtra(ProductionRangeActivity.LOT_ID, item.lot_id.toString())
                    putExtra(ProductionRangeActivity.SOIL_ID, item.soil_id.toString())
                    putExtra(ProductionRangeActivity.LOT_NAME, item.lot_name)
                }
                startActivity(intent)
            }
            "3"  -> {
                val intent = Intent(context, PollinationActivity::class.java).apply {
                    putExtra(PollinationActivity.TAG_NAME, tittle)
                    putExtra(PollinationActivity.LOT_ID, item.lot_id.toString())
                    putExtra(PollinationActivity.LOT_NAME, item.lot_name)
                }
                startActivity(intent)
            }
        }

    }

    private fun setupRecycler() {
        rv_selection_lots_mipe.hasFixedSize()
        rv_selection_lots_mipe.layoutManager = GridLayoutManager(context, getSpanCount(context!!))
        rv_selection_lots_mipe.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

        mAdapter.listener = this
        rv_selection_lots_mipe.adapter = mAdapter
        clickItemLot()
    }

    override fun onGetCounterValue(position: Int): Observable<Int>? {
        return mListener?.onGetCounterValue(position)
    }

}
