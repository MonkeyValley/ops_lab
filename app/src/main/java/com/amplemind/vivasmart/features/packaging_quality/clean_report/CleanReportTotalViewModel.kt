package com.amplemind.vivasmart.features.packaging_quality.clean_report

import android.util.Log.e
import com.amplemind.vivasmart.core.repository.CleanReportTotalRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.request.IssueRequest
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.core.utils.PicturesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject

class CleanReportTotalViewModel @Inject constructor(private val repository: CleanReportTotalRepository,
                                                    private val picturesHelper: PicturesHelper,
                                                    private val uploadRepository: ProfileRepository,
                                                    private val preferences: UserAppPreferences,
                                                    private val apiErrors: HandleApiErrors) {

    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()

    private val uploadPhotoSuccess = BehaviorSubject.create<Boolean>()
    private val uploadIssue = BehaviorSubject.create<Boolean>()

    private val listIssues = BehaviorSubject.create<List<ItemCleanTotalReportViewModel>>()
    private val issuesUpdated = BehaviorSubject.create<QualityCarryOrderResponse>()
    private val photos = mutableListOf<File>()

    private val photosUrl = mutableListOf<String>()

    private val issues = mutableListOf<ItemCleanTotalReportViewModel>()
    private var maxSize = 1

    fun setMaxSize(size: Int) {
        maxSize = size
    }

    fun getMaxSize(): Int {
        return maxSize
    }

    fun validateProblems(numberProblems: Int): Int {
        return if (numberProblems > maxSize) maxSize else numberProblems
    }

    fun getPercentFromProblem(numberProblems: Int): String {
        val problems = validateProblems(numberProblems)
        return "${(problems * 100) / maxSize} %"
    }

    fun getTotalPercent(export: String, damage: String, defectCondition: String): String {
        val exportation: Int = export.toIntOrNull() ?: 0
        val permanentDefects: Int = damage.toIntOrNull() ?: 0
        val defects: Int = defectCondition.toIntOrNull() ?: 0

        val total = exportation + permanentDefects + defects

        return "${(total * 100) / maxSize} %"
    }

    fun getProblemsByIssue(position: Int): Int {
        return if (issues[position].number.isEmpty()) 0 else issues[position].number.toInt()
    }

    fun addProblemByIssue(position: Int): Int {
        return getProblemsByIssue(position) + 1
    }

    fun lessProblemByIssue(position: Int): Int {
        return if (getProblemsByIssue(position) > 0) (getProblemsByIssue(position) - 1) else 0
    }

    fun addPhoto(photo: File) {
        photos.add(photo)
    }

    fun getNumPhotos(): String {
        return "${photos.size}"
    }

    fun isLimitPhotos(): Boolean {
        return photos.size >= 4
    }

    fun hasPhoto() : Boolean {
        return photos.isNotEmpty()
    }

    fun updateIssue(position: Int, problems: Int) {
        if (position >= issues.size) return
        issues[position].percentage = getPercentFromProblem(problems)
        issues[position].number = validateProblems(problems).toString()
        issues[position].showNumber()
    }

    fun getIssues(cropId: Int): Disposable {
        return repository.getIssues(cropId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { it ->
                    repository.saveLocalListOfIssues(cropId, it.data)
                    mapModelToViewModels(it.data)
                }
                .subscribe({
                    issues.clear()
                    issues.addAll(it)
                    listIssues.onNext(issues)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLocalIssues(cropId: Int): Disposable {
        return repository.getLocalListOfIssues(cropId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { it ->
                    val mergeList = mutableListOf<IssueModel>()
                    it.data.map { issue ->
                        val filtered = mergeList.filter { it.name == issue.name }
                        if (filtered.isEmpty()) {
                            mergeList.add(issue)
                        }
                    }
                    mapModelToViewModels(mergeList)
                }
                .subscribe({
                    issues.clear()
                    issues.addAll(it)
                    listIssues.onNext(issues)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }


    fun uploadIssues(unitId: Int, carryOrderId: Int, export: String, permanentDefect: String, defectCondition: String, isOnline: Boolean): Disposable {
        val listIssues = issues.map { IssueRequest(it.id, if (it.number.isEmpty()) 0 else it.number.toInt(), it.name, it.isPermanent) }
        val exportation: Int = export.toIntOrNull() ?: 0
        val permanentDefects: Int = permanentDefect.toIntOrNull() ?: 0
        val defects: Int = defectCondition.toIntOrNull() ?: 0

        return repository.createIssues(listIssues, carryOrderId, maxSize, unitId, exportation, permanentDefects, defects, isOnline, photosUrl, photos)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    issuesUpdated.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModels(models: List<IssueModel>): List<ItemCleanTotalReportViewModel> {
        val list = arrayListOf<ItemCleanTotalReportViewModel>()
        for (item in models) {
            list.add(ItemCleanTotalReportViewModel((item)))
        }
        return list
    }

    fun onGetIssues(): BehaviorSubject<List<ItemCleanTotalReportViewModel>> {
        return listIssues
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onIssuesUpdated(): BehaviorSubject<QualityCarryOrderResponse> {
        return issuesUpdated
    }

    fun getPercentageTotals(export: String, damage: String, defect: String): Int {
        val exportation: Int = export.toIntOrNull() ?: 0
        val permanentDefects: Int = damage.toIntOrNull() ?: 0
        val defects: Int = defect.toIntOrNull() ?: 0

        return exportation + permanentDefects + defects
    }

    fun getUploadPhotoSuccess(): BehaviorSubject<Boolean> {
        return uploadPhotoSuccess
    }
    fun getUploadIssueSuccess(): BehaviorSubject<Boolean> {
        return uploadIssue
    }

    private fun removePhoto(){
        if (photos.isNotEmpty()) {
            photos.removeAt(0)
        }
    }

    fun uploadPhotosIssues(): Disposable? {
        if (photos.isEmpty()) {
            uploadIssue.onNext(true)
            return null
        }
        val image = photos[0]
        picturesHelper.compressImage(image, image, 400f, 400f)
        return uploadRepository.uploadImage(image, "user")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    removePhoto()
                    photosUrl.add(it.relative_url)
                    uploadPhotoSuccess.onNext(true)
                }, {
                    e("Error","No se pudo subir la foto $it")
                    uploadPhotoSuccess.onNext(true)
                    removePhoto()
                })
    }


}
