package com.amplemind.vivasmart.core.dagger.module.fragment

import com.amplemind.vivasmart.core.base.dagger.scope.FragmentScope
import com.amplemind.vivasmart.features.packaging_quality.fragment.RecibaReportFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class RecibaFragmentProvider {

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindRecibaReportFragment(): RecibaReportFragment

}
