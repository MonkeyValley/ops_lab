package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import kotlinx.android.synthetic.main.activity_report_quality.*
import javax.inject.Inject

class ReportQualityActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ReportQualityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_quality)
        setupToolbar()
        addSubscribers()

        loadCarryOrder()
    }

    /**
     *  get data carry orders
     */
    private fun loadCarryOrder() {
        viewModel.getCarryOrder().addTo(subscriptions)
    }

    /**
     *  change title in toolbar
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_report_quality
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.clean_report_title)
    }

    /**
     *  Subscribers
     */
    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest().subscribe(this::updateList).addTo(subscriptions)
    }

    private fun updateList(list: List<ItemReportQualityViewModel>) {
        val adapter = ReportQualityAdapter(list.toMutableList())
        rv_quality.hasFixedSize()
        rv_quality.layoutManager = LinearLayoutManager(this)
        rv_quality.itemAnimator = DefaultItemAnimator()
        rv_quality.adapter = adapter

        adapter.onClick().subscribe(this::onClickItem).addTo(subscriptions)

        showImageNoData(list.isEmpty())
    }

    private fun showImageNoData(visible : Boolean){
        no_found_activities.visibility = if (visible) View.VISIBLE else View.GONE
    }

    private fun onClickItem(item: ItemReportQualityViewModel) {
        val intent = Intent(this, QualityOrderActivity::class.java)
        intent.putExtra("id_carry", item.id_carry)
        intent.putExtra("status", item.status)
        intent.putExtra("position", item.position)
        intent.putExtra("name_lot", item.lot)
        startActivityForResult(intent,200)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            (rv_quality.adapter as ReportQualityAdapter).removeReport(data?.getIntExtra("position",0) ?: 0)
            showImageNoData((rv_quality.adapter as ReportQualityAdapter).isEmpty())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
