package com.amplemind.vivasmart.core.repository.model

import android.os.Parcel
import android.os.Parcelable

data class HeaderModel(val title: String?, val packing_id: Int?, val packing_line: Int?, val activity_id: Int?, val collaborator_left: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeValue(packing_id)
        parcel.writeValue(packing_line)
        parcel.writeValue(activity_id)
        parcel.writeInt(collaborator_left)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeaderModel> {
        override fun createFromParcel(parcel: Parcel): HeaderModel {
            return HeaderModel(parcel)
        }

        override fun newArray(size: Int): Array<HeaderModel?> {
            return arrayOfNulls(size)
        }
    }
}