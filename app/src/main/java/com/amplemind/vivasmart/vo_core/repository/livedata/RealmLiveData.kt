package com.amplemind.vivasmart.vo_core.repository.livedata

import androidx.lifecycle.LiveData
import android.util.Log
import io.realm.OrderedRealmCollectionChangeListener
import io.realm.RealmObject
import io.realm.RealmResults

class RealmLiveData<T: RealmObject>(val results: RealmResults<T>): LiveData<Changes<T>>() {

    companion object {
        val TAG = RealmLiveData::class.simpleName
    }

    override fun onActive() {
        super.onActive()
        Log.i(TAG, "onActive")
        results.addChangeListener(mResultsListener)
        postValue(Changes(data = results))
    }

    override fun onInactive() {
        super.onInactive()
        Log.i(TAG, "onInactive")
        results.removeChangeListener(mResultsListener)
    }

    private val mResultsListener: OrderedRealmCollectionChangeListener<RealmResults<T>> = OrderedRealmCollectionChangeListener { t, changeSet ->

        val inserted  = changeSet.insertions
        val updated = changeSet.changes
        val deleted = changeSet.deletions

        //val changes = Changes(inserted, updated, deleted, results)

        //postValue(changes)
    }

}