package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ProductionQualityOfflineRequest(@SerializedName("sign_super") var signSuper: String,
                                           @SerializedName("sign_super_qa") var signCollaborator: String,
                                           @SerializedName("lot_id") val lotId: Int,
                                           @PrimaryKey val id: Int? = null)