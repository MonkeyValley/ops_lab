package com.amplemind.vivasmart.core.repository.remote


import com.amplemind.vivasmart.features.crop_validation.models.*
import io.reactivex.Observable
import retrofit2.http.*

interface CropValidationApi {
    @GET("/v2/carry_order_harvest")
    @Headers("Content-Type: application/json")
    fun getCarryOrderHarvest(
            @Header("Authorization") authentication: String,
            @Query("business_unit_id") business_unit_id: String,
            @Query("day") day: String
    ): Observable<List<CarryOrderHarvest>>

    @GET("/v2/harvest_quality/issues")
    @Headers("Content-Type: application/json")
    fun getIssues(
            @Header("Authorization") authentication: String,
            @Query("crop_id") crop_id: Int
    ): Observable<CropIssuesResponse>

    @GET("/v2/harvest_quality/{carry_order_id}")
    @Headers("Content-Type: application/json")
    fun getDetail(
            @Header("Authorization") authentication: String,
            @Path("carry_order_id") carry_order_id: Int
    ): Observable<CropValidationDetailResponse>

    @POST("/v2/harvest_quality")
    @Headers("Content-Type: application/json")
    fun sendDataCropValidation(
            @Header("Authorization") authentication: String,
            @Body body: CropValidationPost
    ): Observable<CropValidationPostResponse>



}