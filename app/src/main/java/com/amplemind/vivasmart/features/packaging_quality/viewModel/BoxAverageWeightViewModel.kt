package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.vo_core.repository.models.realm.WeightRestriction
import io.realm.Realm
import javax.inject.Inject

class BoxAverageWeightViewModel @Inject constructor(){

    fun valitateAcerageBox(mAverageValue: Double, cropId: Long): Boolean {
        Realm.getDefaultInstance().use {

            var result = it
                    .where(WeightRestriction::class.java)
                    .equalTo("cropId", cropId)
                    .findFirst()

            return if(result != null)
                mAverageValue < result.minWeight ?: 0.0 || mAverageValue > result.maxWeight ?: 0.0
            else false
        }
    }

}