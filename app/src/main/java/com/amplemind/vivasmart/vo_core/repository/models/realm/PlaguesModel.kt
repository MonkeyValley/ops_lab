package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import java.util.ArrayList

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
open class PlaguesModel(
        @SerializedName("crop_id") var cropId: Int? = 0,
        @SerializedName("plague_id") var plagueId: Int? = 0,
        @SerializedName("plague") var plagueDetail: PlagueDetail? = null
) : RealmObject(), Parcelable
{

    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readParcelable(PlagueDetail::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(cropId)
        parcel.writeValue(plagueId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlaguesModel> {
        override fun createFromParcel(parcel: Parcel): PlaguesModel {
            return PlaguesModel(parcel)
        }

        override fun newArray(size: Int): Array<PlaguesModel?> {
            return arrayOfNulls(size)
        }
    }

}

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
open class PlagueDetail(
        @SerializedName("dates") var date: String? = "",
        @PrimaryKey @SerializedName("id") var id: Int? = 0,
        @SerializedName("image") var image: String? = "",
        @SerializedName("is_disease") var isDesease: Boolean? = false,
        @SerializedName("is_qualitative") var isQualitative: Boolean? = false,
        @SerializedName("level1") var lv1: Int? = 0,
        @SerializedName("level2") var lv2: Int? = 0,
        @SerializedName("level3") var lv3: Int? = 0,
        @SerializedName("medition") var medition: String? = "",
        @SerializedName("name") var name: String? = "",
        @SerializedName("propagation") var propagation: String? = "",
        @SerializedName("images") var images: RealmList<PlagueImageModel>? = null,
        @SerializedName("version")   var version: Long? = 0
) : RealmObject(), Parcelable
{

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            plaguesImageArrayToRealmList(parcel.createTypedArrayList(PlagueImageModel))
            ){
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
        parcel.writeValue(id)
        parcel.writeString(image)
        parcel.writeValue(isDesease)
        parcel.writeValue(isQualitative)
        parcel.writeValue(lv1)
        parcel.writeValue(lv2)
        parcel.writeValue(lv3)
        parcel.writeString(medition)
        parcel.writeString(name)
        parcel.writeString(propagation)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlagueDetail> {
        override fun createFromParcel(parcel: Parcel): PlagueDetail {
            return PlagueDetail(parcel)
        }

        override fun newArray(size: Int): Array<PlagueDetail?> {
            return arrayOfNulls(size)
        }

        private fun plaguesImageArrayToRealmList(array: ArrayList<PlagueImageModel>?): RealmList<PlagueImageModel> {
            val list = RealmList<PlagueImageModel>()
            array?.forEach { list.add(it) }
            return list
        }

    }
}


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
open class PlagueImageModel(
        @PrimaryKey @SerializedName("id") var id: Int? = 0,
        @SerializedName("image") var image: String? = "",
        @SerializedName("level") var level: Int? = 0,
        @SerializedName("plague_id") var plagueId: Int? = 0
) : RealmObject(), Parcelable
{
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(String::class.java.classLoader) as? String,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int
           ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeValue(image)
        parcel.writeValue(level)
        parcel.writeValue(plagueId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlagueImageModel> {
        override fun createFromParcel(parcel: Parcel): PlagueImageModel {
            return PlagueImageModel(parcel)
        }

        override fun newArray(size: Int): Array<PlagueImageModel?> {
            return arrayOfNulls(size)
        }
    }

}

