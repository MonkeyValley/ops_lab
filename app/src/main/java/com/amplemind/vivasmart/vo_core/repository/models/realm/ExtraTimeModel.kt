package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class ExtraTimeModel(@SerializedName("id")                    var id: Int = 0,
                     @SerializedName("extra_hours")           var extra_hours: Int = 0,
                     @SerializedName("collaboratos_no")       var collaborators_no: Int = 0,
                     @SerializedName("activity_id")           var activity_id: Int = 0,
                     @SerializedName("activity_name")         var activity_name: String = "",
                     @SerializedName("activity_icon")    var activity_icon: String = "",
                     @SerializedName("stage_id")         var stage_id: Int = 0,
                     @SerializedName("stage_name")       var stage_name: String =""
) : RealmObject(), Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readString()!!) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(extra_hours)
        parcel.writeInt(collaborators_no)
        parcel.writeInt(activity_id)
        parcel.writeString(activity_name)
        parcel.writeString(activity_icon)
        parcel.writeInt(stage_id)
        parcel.writeString(stage_name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ExtraTimeModel> {
        override fun createFromParcel(parcel: Parcel): ExtraTimeModel {
            return ExtraTimeModel(parcel)
        }

        override fun newArray(size: Int): Array<ExtraTimeModel?> {
            return arrayOfNulls(size)
        }
    }
}