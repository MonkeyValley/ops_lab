package com.amplemind.vivasmart.core.extensions

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.graphics.Color
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.view.animation.TranslateAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.amplemind.vivasmart.R


/**
 * Created by
 *          amplemind on 7/12/18.
 */

fun ImageView.onStartAnimRight() {
    val animation = TranslateAnimation(0.0f, 50.0f,
            0.0f, 0.0f)          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
    animation.duration = 1000  // animation duration
    animation.repeatCount = Animation.INFINITE  // animation repeat count
    animation.repeatMode = 2   // repeat animation (left to right, right to left )
    this.startAnimation(animation)
}

fun ImageView.onStartAnimLeft() {
    val animation = TranslateAnimation(50.0f, 0.0f,
            0.0f, 0.0f)          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
    animation.duration = 1000  // animation duration
    animation.repeatCount = Animation.INFINITE  // animation repeat count
    animation.repeatMode = 2   // repeat animation (left to right, right to left )
    this.startAnimation(animation)
}

fun ImageView.onStarAnimtUp() {
    val animation = TranslateAnimation(0.0f, 0.0f,
            0.0f, 30.0f)          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
    animation.duration = 1000  // animation duration
    animation.repeatCount = Animation.INFINITE  // animation repeat count
    animation.repeatMode = 2   // repeat animation (left to right, right to left )
    this.startAnimation(animation)
}

fun ImageView.onShakeAnimation() {
    val rotate = RotateAnimation(0f, 20f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
    rotate.duration = 1100
    rotate.repeatCount = RotateAnimation.INFINITE
    rotate.interpolator = LinearInterpolator()
    this.startAnimation(rotate)
}

fun View.flipView(view_front: View, showFront: Boolean) {
    val leftIn = AnimatorInflater.loadAnimator(context, R.animator.card_flip_left_in) as AnimatorSet
    val rightOut = AnimatorInflater.loadAnimator(context, R.animator.card_flip_right_out) as AnimatorSet
    val leftOut = AnimatorInflater.loadAnimator(context, R.animator.card_flip_left_out) as AnimatorSet
    val rightIn = AnimatorInflater.loadAnimator(context, R.animator.card_flip_right_in) as AnimatorSet

    val showFrontAnim = AnimatorSet()
    val showBackAnim = AnimatorSet()

    leftIn.setTarget(this)
    rightOut.setTarget(view_front)
    showFrontAnim.playTogether(leftIn, rightOut)

    leftOut.setTarget(this)
    rightIn.setTarget(view_front)
    showBackAnim.playTogether(rightIn, leftOut)

    if (showFront) {
        showFrontAnim.start()
    } else {
        showBackAnim.start()
    }
}

fun View.snack(message: String, length: Int = Snackbar.LENGTH_SHORT) {
    val snackbar = Snackbar.make(this, message, length)
    snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
    snackbar.view.findViewById<TextView>(R.id.snackbar_text)
            .setTextColor(Color.WHITE)
    snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
    snackbar.show()
}

fun View.snackWithoutInternetMessage() {
    val snackbar = Snackbar.make(this, context.getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
    val snackText = snackbar.view.findViewById<TextView>(R.id.snackbar_text)
    snackbar.view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent))
    snackText.setTextColor(Color.WHITE)
    snackText.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_no_signal), null)
    snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
    snackbar.show()
}

fun View.snackWithInternetMessage() {
    val snackbar = Snackbar.make(this, context.getString(R.string.with_internet), Snackbar.LENGTH_INDEFINITE)
    val snackText = snackbar.view.findViewById<TextView>(R.id.snackbar_text)
    snackbar.view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
    snackText.setTextColor(Color.WHITE)
    snackText.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_wifi), null)
    snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
    snackbar.show()
}

fun View.showTrainingMesssage(isChecked: Boolean) {
    val message = if (isChecked) "Importe tipo Capacitación activado. Todas las actividades se pagarán en este modo."
    else "Importe tipo Capacitación desactivado. Todas las actividades se pagarán en este modo."
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    val snackText = snackbar.view.findViewById<TextView>(R.id.snackbar_text)
    snackbar.view.setBackgroundColor(ContextCompat.getColor(context, R.color.bgAlert))
    snackText.setTextColor(Color.WHITE)
    snackText.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_warning), null)
    snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
    snackbar.show()
}

fun View.backgroundTables(value: Boolean) {
    val drawable = if (value) {
        R.drawable.ic_button_blue
    } else {
        R.drawable.ic_button_gray
    }
    this.setBackgroundResource(drawable)
}

fun LinearLayout.paintTables(tables: List<Int>?) {
    tables?.forEachIndexed { index, id ->
        val tableView = this.getChildAt(index)
        tableView.visibility = View.VISIBLE
        (tableView as TextView).text = id.toString()
    }
}