package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.repository.PayrollRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ItemActivitiesPayrollViewModel
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class GeneralActivitiesViewModel @Inject constructor(private val repository: PayrollRepository, private val preferences: UserAppPreferences,
                                                     private val apiErrors: HandleApiErrors) {

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val requestFail = BehaviorSubject.create<String>()
    private val successRequest = BehaviorSubject.create<List<ItemActivitiesPayrollViewModel>>()


    private fun createDummyActivityModel(name: String, icon: String): ActivityModel {
        return ActivityModel()
        /*
        return ActivityModel(0, 0, 0, 0,
                0, 0, 0, 0.0, null, 0.0,0.0, 0.0, false, true, false,
                icon, "", true, null, null, name, 0.0, 0, 100, null,
                0, 0, true)
                */
    }

    fun getDummyActivities() {
        val activities = mutableListOf<ItemActivitiesPayrollViewModel>()
        /*
        activities.add(ItemActivitiesPayrollViewModel(createDummyActivityModel("Contable", "1.png")))
        activities.add(ItemActivitiesPayrollViewModel(createDummyActivityModel("Bajar planta", "6.png")))
        activities.add(ItemActivitiesPayrollViewModel(createDummyActivityModel("Capacitación", "9.png")))
        activities.add(ItemActivitiesPayrollViewModel(createDummyActivityModel("Recolección", "16.png")))
        activities.add(ItemActivitiesPayrollViewModel(createDummyActivityModel("Atar", "12.png")))
        */
        successRequest.onNext(activities)
    }

    fun onSuccessRequest(): BehaviorSubject<List<ItemActivitiesPayrollViewModel>> {
        return successRequest
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

}