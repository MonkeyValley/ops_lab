package com.amplemind.vivasmart.core.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.amplemind.vivasmart.core.repository.model.ControlQualityActivitiesListModel
import com.amplemind.vivasmart.core.repository.model.ControlQualityGroovesModel
import com.amplemind.vivasmart.core.repository.model.ControlQualityListModel
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.ReviewIssueRequest

@Dao
interface ControlQualityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertControlQuality(activity: List<ControlQualityListModel>)

    @Query("SELECT * FROM ControlQualityListModel WHERE stageId = :stageId")
    fun getListControlQuality(stageId: Int) : List<ControlQualityListModel>

    @Query("SELECT * FROM ControlQualityListModel WHERE collaboratorId = :collaboratorId")
    fun getCollaboratorControlQuality(collaboratorId: Int) : ControlQualityListModel

    /*
     * Control Quality Activities
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertControlQualityActivity(activity: ControlQualityActivitiesListModel): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertControlQualityGrooves(activity: List<ControlQualityGroovesModel>)

    @Query("SELECT * FROM ControlQualityActivitiesListModel WHERE collaborator_id = :collaboratorId")
    fun getControlQualityActivities(collaboratorId: Int) : List<ControlQualityActivitiesListModel>

    @Query("SELECT * FROM ControlQualityActivitiesListModel")
    fun getControlQualityActivities() : List<ControlQualityActivitiesListModel>

    @Query("UPDATE ControlQualityActivitiesListModel SET total = total + 1 WHERE activityLogId = :activityLogId")
    fun incrementTotalControlQualityActivity(activityLogId: Int)

    @Query("SELECT * FROM ControlQualityGroovesModel WHERE control_quality_id = :controlQualityId")
    fun getControlQualityGrooves(controlQualityId: Int) : List<ControlQualityGroovesModel>

    @Query("UPDATE ControlQualityGroovesModel SET qualityControl = :qualityControl WHERE groove_id = :grooveId")
    fun updateControlQualityGrooves(qualityControl: Boolean, grooveId: Int)

    @Query("UPDATE ControlQualityGroovesModel SET errorSync = :error WHERE groove_id = :grooveId")
    fun updateSyncStatusControlQualityGrooves(error: Boolean, grooveId: Int)

    /*
     * Issues List
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertIssues(issue: List<IssueModel>)

    @Query("SELECT * FROM IssueModel WHERE activityId = :activityId AND areaId = :areaId AND category = :category AND subCategory = :subCategory")
    fun getIssues(activityId: Int, areaId: Int, category: Int, subCategory: Int) : List<IssueModel>

    @Query("SELECT * FROM IssueModel WHERE cropId = :cropId")
    fun getIssuesByCrop(cropId: Int) : List<IssueModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReviewIssue(issue: ReviewIssueRequest)

    @Query("SELECT * FROM ReviewIssueRequest WHERE deleted = 0")
    fun getReviewIssue(): List<ReviewIssueRequest>

    @Query("UPDATE ReviewIssueRequest SET deleted = 1 WHERE id = :id")
    fun deleteReviewIssue(id: Long)

    /*
     * Quality Carry Order
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQualityCarryOrderRequest(issue: QualityCarryOrderRequest)

    @Query("SELECT * FROM QualityCarryOrderRequest")
    fun getQualityCarryOrderRequest(): List<QualityCarryOrderRequest>

    @Query("DELETE FROM QualityCarryOrderRequest")
    fun deleteAllCarryOrders()
}