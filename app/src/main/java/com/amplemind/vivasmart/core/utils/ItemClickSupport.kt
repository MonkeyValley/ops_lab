package com.amplemind.vivasmart.core.utils

import android.os.Handler
import android.os.Looper
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewConfiguration
import com.amplemind.vivasmart.R


/**
 * Created by Hugo Visser.
 * Please see http://www.littlerobots.nl/blog/Handle-Android-RecyclerView-Clicks/
 *
 * Modified by Filippo Beraldo:
 * - Added OnDoubleClickListener in place of the standard OnClickListener.
 *
 * http://www.littlerobots.nl/blog/Handle-Android-RecyclerView-Clicks/
 */

class ItemClickSupport private constructor(private val mRecyclerView: RecyclerView) {
    private var mOnItemClickListener: OnItemClickListener? = null

    private val mOnDoubleClickListener = object : OnDoubleClickListener() {
        override fun onDoubleClick(v: View) {
            if (mOnItemClickListener != null) {
                val holder = mRecyclerView.getChildViewHolder(v)
                mOnItemClickListener!!.onItemDoubleClicked(mRecyclerView, holder.adapterPosition, v)
            }
        }

        override fun onSingleClick(v: View) {
            if (mOnItemClickListener != null) {
                val holder = mRecyclerView.getChildViewHolder(v)
                //mOnItemClickListener!!.onItemClicked(mRecyclerView, holder.adapterPosition, v)
            }
        }

    }

    private val mAttachListener = object : RecyclerView.OnChildAttachStateChangeListener {
        override fun onChildViewDetachedFromWindow(p0: View) {}
        override fun onChildViewAttachedToWindow(view : View) {
            if (mOnItemClickListener != null) {
                view.setOnClickListener(mOnDoubleClickListener)
            }
            if (mOnItemClickListener != null){
                view.setOnLongClickListener {
                    val holder = mRecyclerView.getChildViewHolder(it)
                    mOnItemClickListener!!.onLongClicked(mRecyclerView, holder.adapterPosition, it)
                    return@setOnLongClickListener true
                }
            }
        }

    }

    init {
        mRecyclerView.setTag(R.id.item_click_support, this)
        mRecyclerView.addOnChildAttachStateChangeListener(mAttachListener)
    }

    fun setOnItemClickListener(listener: OnItemClickListener): ItemClickSupport {
        mOnItemClickListener = listener
        return this
    }

    private fun detach(view: RecyclerView) {
        view.removeOnChildAttachStateChangeListener(mAttachListener)
        view.setTag(R.id.item_click_support, null)
    }

    interface OnItemClickListener {
        //fun onItemClicked(recyclerView: RecyclerView, position: Int, v: View)
        fun onItemDoubleClicked(recyclerView: RecyclerView, position: Int, v: View)
        fun onLongClicked(recyclerView: RecyclerView, position: Int, v: View)
    }

    companion object {
        fun addTo(view: RecyclerView): ItemClickSupport {
            var support = view.getTag(R.id.item_click_support)
            if (support == null) {
                support = ItemClickSupport(view)
            }
            return support as ItemClickSupport
        }

        fun removeFrom(view: RecyclerView): ItemClickSupport? {
            val support = view.getTag(R.id.item_click_support) as ItemClickSupport
            support.detach(view)
            return support
        }
    }
}

abstract class OnDoubleClickListener : View.OnClickListener {
    private val doubleClickTimeout = ViewConfiguration.getDoubleTapTimeout()
    private val handler: Handler

    private var firstClickTime: Long = 0

    init {
        firstClickTime = 0L
        handler = Handler(Looper.getMainLooper())
    }

    override fun onClick(v: View) {
        val now = System.currentTimeMillis()

        if (now - firstClickTime < doubleClickTimeout) {
            handler.removeCallbacksAndMessages(null)
            firstClickTime = 0L
            onDoubleClick(v)
        } else {
            firstClickTime = now
            handler.postDelayed({
                //onSingleClick(v)
                firstClickTime = 0L
            }, doubleClickTimeout.toLong())
        }
    }

    abstract fun onDoubleClick(v: View)

    abstract fun onSingleClick(v: View)

    fun reset() {
        handler.removeCallbacksAndMessages(null)
    }
}