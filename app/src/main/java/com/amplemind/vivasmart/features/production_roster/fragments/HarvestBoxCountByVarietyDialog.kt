package com.amplemind.vivasmart.features.production_roster.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.SwitchCompat
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.showTrainingMesssage
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.amplemind.vivasmart.core.utils.LOT_ID
import com.amplemind.vivasmart.features.adapters.MarginItemDecoration
import com.amplemind.vivasmart.features.extensions.showErrorDialog
import com.amplemind.vivasmart.features.haulage.adapter.HaulageReportAdapter
import com.amplemind.vivasmart.features.haulage.viewModel.HaulageReportViewModel
import com.amplemind.vivasmart.features.haulage.viewModel.ItemHaulageReportViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.HarvestBoxCountByVarietyViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import com.jakewharton.rxbinding2.widget.RxTextView
import kotlinx.android.synthetic.main.content_haulage_activity.*
import kotlinx.android.synthetic.main.fragment_harvest_validation_carry_order_list.*
import org.jetbrains.anko.childrenSequence
import javax.inject.Inject


@Suppress("UNCHECKED_CAST")
class HarvestBoxCountByVarietyDialog : BaseDialogFragment(), CompoundButton.OnCheckedChangeListener {

    companion object {

        val TAG = HarvestBoxCountByVarietyViewModel::class.simpleName

        private val LOT_NAME = "$TAG.LotName"
        private val BOXES_COUNT = "$TAG.BoxesCount"
        private val PARAM_STAGE_UUID = "$TAG.StageUuid"
        private val PARAM_VARIETIES = "$TAG.Varieties"
        private val PARAM_BOX_COUNT = "$TAG.BoxCount"
        private val PARAM_SECOND_QUALITY = "$TAG.SecondQuality"
        private val TO_DATE = "$TAG.ToDate"
        private val FROM_DATE = "$TAG.FromDate"
        private val PARAM_READ_ONLY = "$TAG.ReadOnly"
        private val PARAM_READ_EDIT = "$TAG.ReadEdit"

        fun newInstance(stageUuid: String, boxCounts: List<VarietyBoxCount>? = null, secondQuality: Double = 0.0, readOnly: Boolean = false) =
                HarvestBoxCountByVarietyDialog().apply {
                    arguments = Bundle().apply {
                        putString(PARAM_STAGE_UUID, stageUuid)
                        putParcelableArray(PARAM_BOX_COUNT, boxCounts?.toTypedArray())
                        putDouble(PARAM_SECOND_QUALITY, secondQuality)
                        putBoolean(PARAM_READ_ONLY, readOnly)
                    }
                }

        fun newInstance(varieties: List<VarietyModel>, boxCounts: List<VarietyBoxCount>? = null, secondQuality: Double = 0.0, readOnly: Boolean = false, readEdit: Boolean) =
                HarvestBoxCountByVarietyDialog().apply {
                    arguments = Bundle().apply {
                        putParcelableArray(PARAM_VARIETIES, varieties.toTypedArray())
                        putParcelableArray(PARAM_BOX_COUNT, boxCounts?.toTypedArray())
                        putDouble(PARAM_SECOND_QUALITY, secondQuality)
                        putBoolean(PARAM_READ_ONLY, readOnly)
                        putBoolean(PARAM_READ_EDIT, readEdit)
                    }
                }

        fun newInstance(lotId: Int, lotName: String, boxes: String, varieties: List<VarietyModel>, boxCounts: List<VarietyBoxCount>? = null,
                        secondQuality: Double = 0.0, toDate: String, fromDate: String, readOnly: Boolean = false) =
                HarvestBoxCountByVarietyDialog().apply {
                    arguments = Bundle().apply {
                        LOT_ID = lotId
                        putString(LOT_NAME, lotName)
                        putString(BOXES_COUNT, boxes)
                        putParcelableArray(PARAM_VARIETIES, varieties.toTypedArray())
                        putParcelableArray(PARAM_BOX_COUNT, boxCounts?.toTypedArray())
                        putDouble(PARAM_SECOND_QUALITY, secondQuality)
                        putString(TO_DATE, toDate)
                        putString(FROM_DATE, fromDate)
                        putBoolean(PARAM_READ_ONLY, readOnly)
                    }
                }

    }

    @Inject
    lateinit var mViewModel: HarvestBoxCountByVarietyViewModel

    @Inject
    lateinit var haulageViewModel: HaulageReportViewModel

    private lateinit var mAdapter: HaulageReportAdapter

    private lateinit var mLayout: ViewGroup
    private lateinit var mVarieties: LinearLayout
    private lateinit var mSecondQuality: ViewGroup
    private lateinit var mBtnCancel: Button
    private lateinit var mBtnOk: Button
    private lateinit var mTvTotal: TextView
    private lateinit var scrollView: ScrollView
    private lateinit var swHaulage: SwitchCompat
    private lateinit var tvTitle: TextView
    private lateinit var lnBoxes: ConstraintLayout
    private lateinit var lnHaulagueReport: ConstraintLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var rvQualityFinished: RecyclerView

    private var lotName: String = "Captura de cajas"
    private var boxes: String = ""
    private var mReadOnly: Boolean = false
    private var mReadEdit: Boolean = false
    private var toDate: String = ""
    private var fromDate: String = ""

    private var hasVarieties = false

    private var boxCountResultCallback: ((List<VarietyBoxCount>, Double, Double) -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.apply {
            mViewModel.apply {
                lotName = getString(LOT_NAME) ?: "Captura de cajas"
                boxes = getString(BOXES_COUNT) ?: ""
                stageUuid = getString(PARAM_STAGE_UUID)
                varieties = getParcelableArray(PARAM_VARIETIES)?.toList() as? List<VarietyModel>
                boxCounts = getParcelableArray(PARAM_BOX_COUNT)?.toList() as? List<VarietyBoxCount>
                secondQuality = getDouble(PARAM_SECOND_QUALITY)
                toDate = getString(TO_DATE) ?: ""
                fromDate = getString(FROM_DATE) ?: ""
                mReadOnly = getBoolean(PARAM_READ_ONLY)
                mReadEdit = getBoolean(PARAM_READ_EDIT)
            }
        } ?: throw IllegalArgumentException("No arguments were passed!")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = AlertDialog.Builder(context!!)
                .create()

        setUpUI(dialog.layoutInflater)

        dialog.setView(mLayout)

        return dialog
    }

    override fun onStart() {
        super.onStart()
        setUpUI()
        setCallbacks()
        addSubscribers()
        setUpRecyclerView()
        showProgressBar(true)
        //revisar

        syncVarieties()
    }

    @SuppressLint("InflateParams")
    private fun setUpUI(layoutInflater: LayoutInflater) {
        mLayout = layoutInflater.inflate(R.layout.fragment_harvest_box_count_by_variety, null) as ViewGroup

        mVarieties = mLayout.findViewById(R.id.layout_varieties)

        mSecondQuality = mLayout.findViewById(R.id.layout_second_quality)

        scrollView = mLayout.findViewById(R.id.scr_varieties)

        lnBoxes = mLayout.findViewById(R.id.ln_boxes)
        lnHaulagueReport = mLayout.findViewById(R.id.ln_haulague_report)

        progressBar = mLayout.findViewById(R.id.progress_bar2)

        mBtnOk = mLayout.findViewById(R.id.btn_ok)
        mBtnCancel = mLayout.findViewById(R.id.btn_cancel)
        mTvTotal = mLayout.findViewById(R.id.tv_total)

        tvTitle = mLayout.findViewById(R.id.tv_title)
        swHaulage = mLayout.findViewById(R.id.sw_haulage)

        rvQualityFinished = mLayout.findViewById(R.id.rv_quality_finished)

        populateVarietyBoxCountLayout(mSecondQuality, "Segunda calidad")

        if (mReadOnly) {
            tvTitle.text = lotName
            mBtnCancel.visibility = View.GONE
            swHaulage.visibility = View.VISIBLE
            swHaulage.setOnCheckedChangeListener(this)
        } else if (mReadEdit) {
            swHaulage.visibility = View.VISIBLE
            swHaulage.text = "Editar: "
            swHaulage.isChecked = false
            swHaulage.setOnCheckedChangeListener(this)
        }

        RxView.clicks(mBtnCancel)
                .subscribe {
                    dismiss()
                }
                .addTo(subscriptions)


        RxView.clicks(mBtnOk)
                .subscribe {
                    if (!mReadOnly) {
                        if (hasVarieties) {
                            val boxCounts = mViewModel.buildVarietyBoxCounts()
                            val secondQuality = getBoxCountFromUI(mSecondQuality)
                            boxCountResultCallback?.invoke(boxCounts, secondQuality, mViewModel.boxTotal)
                            dismiss()
                        } else {
                            showErrorSnackBar()
                        }
                    } else {
                        dismiss()
                    }
                }
                .addTo(subscriptions)
    }

    private fun showErrorSnackBar() {
        val snackbar = Snackbar.make(mLayout.rootView, "Se deben de agregar variedades a la etapa, contactar con soporte tecnico", Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    override fun loadData() {
        mViewModel.loadVarieties()?.addTo(subscriptions)
    }

    private fun setCallbacks() {
        mViewModel.onVarietiesLoaded(this::onVarietiesLoaded).addTo(subscriptions)
        mViewModel.onTotalChanged(this::updateTotalDisplay).addTo(subscriptions)
    }

    private fun onVarietiesLoaded(varieties: List<VarietyModel>) {
        mVarieties.removeAllViewsInLayout()

        val layoutInflater = dialog!!.layoutInflater

        hasVarieties = varieties.isNotEmpty()

        varieties.forEach { variety ->
            scrollView.visibility = View.VISIBLE
            val layout = layoutInflater.inflate(R.layout.layout_variety_box_count, mVarieties, false)
            populateVarietyBoxCountLayout(layout, variety)
            mVarieties.addView(layout)
        }
    }

    private fun populateVarietyBoxCountLayout(view: View, variety: VarietyModel) {
        populateVarietyBoxCountLayout(view, variety.name, variety.id)
    }

    private fun populateVarietyBoxCountLayout(view: View, varietyName: String, varietyId: Long = -1) {
        view.apply {

            val etBoxCount = view.findViewById<EditText>(R.id.et_box_count)
            val swHalfBox = view.findViewById<SwitchCompat>(R.id.sw_half_box)


            Log.e("VARIETY", varietyName)
            view.findViewById<TextView>(R.id.tv_variety)?.text = varietyName

            if (mReadOnly) {
                etBoxCount.isEnabled = false
                etBoxCount.alpha = 1f
                etBoxCount.minEms = 5
                etBoxCount.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(10))
                etBoxCount.inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL

                etBoxCount.setText("%.1f".format(mViewModel.getFullCountFor(varietyId)))

                swHalfBox.visibility = View.GONE
            } else {
                etBoxCount.setText(mViewModel.getCountFor(varietyId).toString())
                swHalfBox.isChecked = mViewModel.hasHalfBox(varietyId)

                RxCompoundButton.checkedChanges(swHalfBox)
                        .subscribe { isChecked ->
                            swHalfBox.text = if (isChecked) ".5" else ".0"
                            setBoxCount(varietyId, etBoxCount, swHalfBox)
                        }
                        .addTo(subscriptions)

                etBoxCount.setTextIsSelectable(false)

                RxView.focusChanges(etBoxCount)
                        .subscribe { hasFocus ->
                            val text = etBoxCount.text.toString()
                            if (text == "0") {
                                if (hasFocus) etBoxCount.text.clear()
                            } else if (text.isEmpty() || text.isBlank()) {
                                if (!hasFocus) etBoxCount.setText("0")
                            } else {
                                if (hasFocus) etBoxCount.setSelection(text.length)
                            }
                        }
                        .addTo(subscriptions)

                RxView.clicks(etBoxCount)
                        .subscribe {
                            validateClick()
                        }
                        .addTo(subscriptions)


                RxTextView.textChanges(etBoxCount)
                        .subscribe {
                            setBoxCount(varietyId, etBoxCount, swHalfBox)
                        }
                        .addTo(subscriptions)


                tag = varietyId
            }
        }
    }

    fun validateClick() {
        if (!hasVarieties) showErrorSnackBar()
    }

    private fun setBoxCount(varietyId: Long, etBoxCount: EditText, swHalfBox: SwitchCompat) {
        mViewModel.setBoxCount(varietyId, getBoxCountFromUI(etBoxCount, swHalfBox))
    }

    private fun getBoxCountFromUI(etBoxCount: EditText, swHalfBox: SwitchCompat) =
            (etBoxCount.text.toString().toDoubleOrNull()
                    ?: 0.0) + if (swHalfBox.isChecked) 0.5 else 0.0

    private fun getBoxCountFromUI(view: View): Double {
        val etBoxCount = view.findViewById<EditText>(R.id.et_box_count)
        val swHalfBox = view.findViewById<SwitchCompat>(R.id.sw_half_box)
        return getBoxCountFromUI(etBoxCount, swHalfBox)
    }

    private fun updateTotalDisplay(total: Double) {
        if (mReadOnly) mTvTotal.text = boxes
        else mTvTotal.text = "%.1f".format(total)

    }

    fun onBoxCountResult(callback: (List<VarietyBoxCount>, secondQuality: Double, total: Double) -> Unit): HarvestBoxCountByVarietyDialog {
        boxCountResultCallback = callback
        return this
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if (mReadOnly) {
            if (isChecked) {
                haulageViewModel.toDate = toDate
                haulageViewModel.fromDate = fromDate
                lnBoxes.visibility = View.GONE
                lnHaulagueReport.visibility = View.VISIBLE
                mAdapter.clearData()
                loadDataHaulage()
            } else {
                lnBoxes.visibility = View.VISIBLE
                lnHaulagueReport.visibility = View.GONE
            }
        } else if (mReadEdit) {
            for (view: View in mVarieties.childrenSequence()) {
                varietyCountInputEnabled(view, isChecked)
            }
            varietyCountInputEnabled(mSecondQuality, isChecked)
        }
    }

    private fun varietyCountInputEnabled(layout: View, enabled: Boolean) {
        val etBoxCount = layout.findViewById<EditText>(R.id.et_box_count)
        val swHalfBox = layout.findViewById<SwitchCompat>(R.id.sw_half_box)

        etBoxCount?.isEnabled = enabled
        etBoxCount?.alpha = if (enabled) 1f else 0.6f
        swHalfBox?.isEnabled = enabled
    }

    private fun setUpRecyclerView() {
        mAdapter = HaulageReportAdapter()
        mAdapter.setData(emptyList())
        rvQualityFinished.apply {
            layoutManager = LinearLayoutManager(context!!)
            adapter = mAdapter
            addItemDecoration(MarginItemDecoration(8))
        }
    }

    private fun loadDataHaulage() {
        showNoRecordsLayout(false)
        haulageViewModel.loadCarryOrdersReport()
    }

    private fun onReportLoaded(data: List<ItemHaulageReportViewModel>) {
        showNoRecordsLayout(data.isEmpty())
        mAdapter.setData(data)
    }

    private fun showNoRecordsLayout(show: Boolean) {
        rvQualityFinished.visibility = if (show) View.GONE else View.VISIBLE
    }

    private fun showProgressBar(show: Boolean) {
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun addSubscribers() {
        haulageViewModel.onShowProgress(this::showProgressBar).addTo(subscriptions)
        haulageViewModel.onReportLoaded(this::onReportLoaded).addTo(subscriptions)
        haulageViewModel.onReportLoadingError(this::onReportLoadingError).addTo(subscriptions)
        haulageViewModel.onSuccessVaritiesRequest().subscribe(this::uploadStage).addTo(subscriptions)
    }

    private fun uploadStage(list: List<VarietyModel>) {
        showProgressBar(true)
        if (haulageViewModel.uploadVarieties(list)) {
            showProgressBar(false)
            loadData()
        }
    }

    private fun onReportLoadingError(error: Throwable) {
        showProgressBar(false)
    }

    override fun setUpUI() {
        super.setUpUI()
        if (mReadEdit) {
            for (view: View in mVarieties.childrenSequence()) {
                varietyCountInputEnabled(view, false)
            }
            varietyCountInputEnabled(mSecondQuality, false)
        }
        showProgressBar(false)
    }

    private fun syncVarieties() {
        haulageViewModel.getStage().forEach {
            haulageViewModel.getVarietiesStage(it.id)
        }
    }

}
