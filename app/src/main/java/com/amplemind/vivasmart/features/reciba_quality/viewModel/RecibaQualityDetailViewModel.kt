package com.amplemind.vivasmart.features.reciba_quality.viewModel

import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.reciba_quality.repository.RecibaQualityRepository
import com.amplemind.vivasmart.features.reciba_quality.repository.response.RewviewDetailRecibaQualityResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class RecibaQualityDetailViewModel  @Inject constructor(
        private val repository: RecibaQualityRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val list = mutableListOf<ItemDetailRecibaQualityViewModel>()
    private val requestSuccess = BehaviorSubject.create<List<ItemDetailRecibaQualityViewModel>>()

    var carryOrderId = "0"
    var lotName = ""
    var table = ""
    var tittle = ""

    fun getDetailList(): Disposable {
        return repository.getDetailList(carryOrderId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel)
                .subscribe({
                    list.clear()
                    list.addAll(it)
                    requestSuccess.onNext(list)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                    Log.e("getReportList", "error", it)
                })
    }

    fun mapModelToViewModel(result : List<RewviewDetailRecibaQualityResponse>) : List<ItemDetailRecibaQualityViewModel>{
        val viewModels = mutableListOf<ItemDetailRecibaQualityViewModel>()

        if (result.isNotEmpty()) tittle = result[0].cropName + " - "+ result[0].lotName

        result.forEach { obj ->
            viewModels.add(ItemDetailRecibaQualityViewModel(obj))
        }
        viewModels.sortBy { it.folio }
        viewModels.reverse()
        return viewModels
    }

    fun onSuccessRequest(): BehaviorSubject<List<ItemDetailRecibaQualityViewModel>> {
        return requestSuccess
    }

}