package com.amplemind.vivasmart.features.production_roster


import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.app.NotificationCompat
import androidx.core.app.RemoteInput
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.production_roster.fragments.ExtraHoursAuthRequest
import com.amplemind.vivasmart.features.production_roster.fragments.ExtraHoursInboxFragment
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import kotlinx.android.synthetic.main.custom_toolbar.*


class ExtraHoursMessagesActivity : BaseActivityWithFragment(),
        ExtraHoursInboxFragment.OnFragmentInteractionListener,
        ExtraHoursAuthRequest.OnFragmentInteractionListener{

    lateinit var mStage: StageModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extra_hours_messages)

        // mStage = intent.getParcelableExtra("stage")

        addFragment(ExtraHoursInboxFragment())
        initToolbar()

        val returnMessage = "Thank you"

        if (intent.getStringExtra("key_text_reply") != null) {
            val mBuilder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_accept)
                    .setContentText(returnMessage)

            val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            //update notification
            mNotificationManager.notify(1000, mBuilder.build())

            Toast.makeText(this, getMessageText(intent).toString(), Toast.LENGTH_SHORT).show()
        }


    }

    private fun getMessageText(intent: Intent): CharSequence? {
        val remoteInput = RemoteInput.getResultsFromIntent(intent)
        return remoteInput?.getCharSequence("key_text_reply")
    }


    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.ExtraHourMessage_container, fragment)
                .commit()
    }


    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        // supportActionBar!!.title = mStage.lot?.name

    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
