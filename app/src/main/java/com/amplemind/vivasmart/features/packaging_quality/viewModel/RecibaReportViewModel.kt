package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.amplemind.vivasmart.core.repository.response.CarryOrdersResponse
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel
import com.amplemind.vivasmart.vo_core.utils.formatHumanDate
import com.amplemind.vivasmart.vo_core.utils.formatISODate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

class RecibaReportViewModel @Inject constructor(val repository: PackagingQualityRepository) {

    private val mOnShowProgress = PublishSubject.create<Boolean>()
    private val mOnReportLoaded = PublishSubject.create<List<ItemRecibaReportViewModel>>()
    private val mOnReportLoadingError = PublishSubject.create<Throwable>()

    private var mCurrentDisposable: Disposable? = null

    private val mDefaultFromTime = "00:00:00"
    private val mDefaultToTime = "23:59:59"

    private val mDate = Calendar.getInstance()

    private val mFromDate = arrayOf(
            mDate.get(Calendar.YEAR),
            mDate.get(Calendar.MONTH),
            mDate.get(Calendar.DAY_OF_MONTH)
    )

    private val mToDate = arrayOf(
            mDate.get(Calendar.YEAR),
            mDate.get(Calendar.MONTH),
            mDate.get(Calendar.DAY_OF_MONTH))

    var fromDate = formatDateTime(mFromDate, mDefaultFromTime)
    var toDate = formatDateTime(mToDate, mDefaultToTime)

    var humanFromDate = setinitialDate(mFromDate) //mFromDate[2].toString()+"/"+ (mFromDate[1] +1) +"/"+mFromDate[0] //formatHumanDate(mFromDate)
    var humanToDate = mToDate[2].toString()+"/"+ (mToDate[1] +1) +"/"+mToDate[0] //formatHumanDate(mToDate)

    fun setinitialDate(date: Array<Int>): String{
        var day = ""+date[2]
        var month = ""+ (date[1] + 1)
        val year = ""+date[0]

        if(date[2] < 10 ){
            day = "0"+date[2]
        }

        if(date[1] < 10 ){
            month = "0"+date[1]
        }

        return day+"/"+month+"/"+year
    }

    val toYear: Int
        get() = mToDate[0]

    val toMonth: Int
        get() = mToDate[1]

    val toDay: Int
        get() = mToDate[2]

    val fromYear: Int
        get() = mFromDate[0]

    val fromMonth: Int
        get() = mFromDate[1]

    val fromDay: Int
        get() = mFromDate[2]


    fun setFromDate(year: Int, month: Int, day: Int) {
        setDateComponents(mFromDate, year, month, day)
        val arrayDate = arrayOf(year, month, day)
        humanFromDate = setinitialDate(arrayDate) //formatHumanDate(mFromDate)
        fromDate = formatDateTime(mFromDate, mDefaultFromTime)
    }

    fun setToDate(year: Int, month: Int, day: Int) {
        setDateComponents(mToDate, year, month, day)
        val arrayDate = arrayOf(year, month, day)
        humanToDate = setinitialDate(arrayDate) //formatHumanDate(mToDate)
        toDate = formatDateTime(mToDate, mDefaultToTime)
    }

    private fun formatDateTime(date: Array<Int>, time: String) =
            "${formatISODate(date)} $time"

    private fun setDateComponents(date: Array<Int>, year: Int, month: Int, day: Int) {
        date[0] = year
        date[1] = month
        date[2] = day
    }

    private fun formatHumanDate(date: Array<Int>) =
            formatHumanDate(date[0], date[1], date[2])

    private fun formatISODate(date: Array<Int>) =
            formatISODate(date[0], date[1], date[2])


    fun loadCarryOrders() =
            repository.loadCarryOrdersBetween(fromDate, toDate)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        mOnShowProgress.onNext(true)
                    }
                    .doOnComplete {
                        mOnShowProgress.onNext(false)
                        disposeCurrentRequest()
                    }
                    .doOnError {
                        mOnShowProgress.onNext(false)
                        it.printStackTrace()
                    }
                    .map(this::mapCarryOrders)
                    .subscribe({
                        mOnReportLoaded.onNext(it)
                    }, {
                        mOnReportLoadingError.onNext(it)
                    })

    fun disposeCurrentRequest() {
        mCurrentDisposable?.apply {
            if (isDisposed) {
                dispose()
            }
            mCurrentDisposable = null
        }
    }

    private fun mapCarryOrders(carryOrders: CarryOrdersResponse): List<ItemRecibaReportViewModel> {
        val data = carryOrders.data
        val size = data.size

        val checkedLots = mutableListOf<Int>()

        val viewModels = mutableListOf<ItemRecibaReportViewModel>()

        data.forEachIndexed{ i: Int, carryOrder1: CarryOrderModel ->

            if (!checkedLots.any{ it == carryOrder1.lotId }) {

                checkedLots.add(carryOrder1.lotId)

                val varieties = mutableListOf<VarietyModel>()
                val boxCounts = mutableListOf<VarietyBoxCount>()

                var secondQualityCount = 0.0

                var boxes = 0.0
                var weightSum = 0.0
                var carries = 0

                var validCarries = 0

                for (j in i until size) {
                    val carryOrder2 = data[j]
                    if (carryOrder1.lotId == carryOrder2.lotId) {

                        extractVarieties(carryOrder2, varieties, boxCounts)
                        secondQualityCount += carryOrder2.secondQualityBox

                        boxes += carryOrder2.validBoxNo ?: 0.0
                        if (carryOrder2.averageWeight != null && carryOrder2.averageWeight > 0) {
                            weightSum += carryOrder2.averageWeight
                            validCarries ++
                        }
                        carries ++
                    }
                }

                var avgWeight = weightSum / validCarries

                if (avgWeight.isNaN()) {
                    avgWeight = 0.0
                }

                viewModels.add(ItemRecibaReportViewModel(
                        carryOrder1.lot,
                        carries,
                        //boxes - secondQualityCount,
                        boxes,
                        avgWeight,
                        varieties,
                        boxCounts,
                        secondQualityCount
                ))

            }

        }

        return viewModels
    }

    private fun extractVarieties(carryOrder: CarryOrderModel, varieties: MutableList<VarietyModel>, boxCounts: MutableList<VarietyBoxCount>) {
        carryOrder.varietyBoxCounts.forEach {boxCount ->
            val varietyExists = varieties.any { variety -> variety.id == boxCount.varietyId}

            if (!varietyExists) varieties.add(boxCount.variety!!)

            val totalBoxCount = boxCounts.find {
                it.varietyId == boxCount.varietyId
            }

            if (totalBoxCount == null) {
                boxCounts.add(
                        VarietyBoxCount(boxCount.varietyId, boxCount.boxCount, boxCount.boxCountValid)
                )
            }
            else {
                totalBoxCount.boxCount += boxCount.boxCount
            }

        }
    }

    fun onShowProgress(callback: (Boolean) -> Unit) =
        mOnShowProgress.subscribe(callback)

    fun onReportLoaded(callback: (List<ItemRecibaReportViewModel>) -> Unit) =
        mOnReportLoaded.subscribe(callback)

    fun onReportLoadingError(callback: (Throwable) -> Unit) =
            mOnReportLoadingError.subscribe(callback)


}