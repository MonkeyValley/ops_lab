package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import androidx.databinding.ObservableField
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getPayment
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.utils.formatPayment
import com.amplemind.vivasmart.vo_core.utils.formatTime
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter

class RosterDetailActivityLogViewModel(
        item: ActivityLogModel
) : BaseAdapter.ViewModel<ActivityLogModel>(item) {

    val activityName: String
        get() = item.activityCode?.activity?.name ?: "Sin Nombre"

    val time: String
        get() = formatTime(item.time)

    val quantity: Int
        get() = item.quantity

    val payment = ObservableField<String>()

    val isGrooves: Boolean = item.activityCode?.activity?.isGrooves ?: false

    override fun bind(item: ActivityLogModel) {
        calculatePayment()
    }

    fun calculatePayment(
            isTraining: Boolean = item.isTraining,
            isPractice: Boolean = item.isPractice,
            isControl: Boolean = item.isControl
    ): Double {
        val payment = item.getPayment(isTraining = isTraining, isPractice = isPractice, isControl = isControl)
        this.payment.set(formatPayment(payment))
        return payment
    }


}