package com.amplemind.vivasmart.features.quality_control.viewModel

import com.amplemind.vivasmart.core.repository.QualityControlRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.request.ListIssueRequest
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class DetailControlQualityViewModel @Inject constructor(private val repository: QualityControlRepository,
                                                        private val preferences: UserAppPreferences,
                                                        private val apiErrors: HandleApiErrors) {

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val requestFail = BehaviorSubject.create<String>()
    private val getIssues = BehaviorSubject.create<List<IssueViewModel>>()
    private val list = mutableListOf<IssueViewModel>()
    private val createdIssue = BehaviorSubject.create<Boolean>()

    fun getSignatureTitle(): String {
        return "Verificar control de calidad"
    }

    fun getSignatureMessage(): String {
        return "¿Estas seguro de querer enviar este reporte? Por favor firme para confirmar:"
    }

    fun getControlListActivities(activityId: Int, areaId: Int, category: Int, subCategory: Int): Disposable {
        return repository.getListOfIssues(preferences.token, activityId, areaId, category, subCategory)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { it ->
                    mapModelToViewModels(it.data)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    getIssues.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLocalControlListActivities(activityId: Int, areaId: Int, category: Int, subCategory: Int): Disposable {
        return repository.getLocalListOfIssues(activityId, areaId, category, subCategory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { it -> mapModelToViewModels(it.data) }
                .subscribe({
                    getIssues.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun sendIssues(issues: List<ListIssueRequest>, comments: String, grooveId: Int, hasInternet: Boolean, activityLogId: Int, collaboratorId: Int): Observable<Any> {
        return repository.createIssue(preferences.token, issues, comments, grooveId, activityLogId, collaboratorId)
    }

    fun createIssues(comments: String, grooveId: Int, hasInternet: Boolean, activityLogId: Int, collaboratorId: Int): Disposable {
        val issues = mutableListOf<ListIssueRequest>()
        for (item in list) {
            if (!item.isNote) {
                issues.add(ListIssueRequest(item.id, item.switchEnabled))
            }
        }
        return sendIssues(issues, comments, grooveId, hasInternet, activityLogId, collaboratorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    createdIssue.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModels(models: List<IssueModel>): List<IssueViewModel> {
        list.clear()
        for (model in models) {
            list.add(IssueViewModel(model))
        }
        list.add(IssueViewModel(null, true))
        if (list.size == 1) {
            list.add(IssueViewModel(null, false, image_no_found = true))
        }
        return list
    }

    fun getComments(): String {
        val filter = list.filter { it.isNote }
        if (filter.isNotEmpty()) {
            return filter.first().comment
        }
        return ""
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onGetIssues(): BehaviorSubject<List<IssueViewModel>> {
        return getIssues
    }

    fun onCreatedIssue(): BehaviorSubject<Boolean> {
        return createdIssue
    }

}