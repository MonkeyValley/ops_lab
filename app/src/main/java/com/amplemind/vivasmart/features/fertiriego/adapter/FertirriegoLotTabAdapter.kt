package com.amplemind.vivasmart.features.fertiriego.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.amplemind.vivasmart.features.fertiriego.fragment.SoilFragment
import com.amplemind.vivasmart.features.fertiriego.fragment.hydroponicFragment

class FertirriegoLotTabAdapter (fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> SoilFragment()
            else -> return hydroponicFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Suelo"
            else -> "Sustrato"
        }
    }
}