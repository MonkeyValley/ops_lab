package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.CropReportCarryOrderModel
import com.google.gson.annotations.SerializedName

data class CropReportResponse(@SerializedName("difference_boxes") val differenceBoxes: Int,
                              @SerializedName("total_collected_boxes") val totalCollectedBoxes: Int,
                              @SerializedName("total_order_boxes") val totalOrderBoxes: Int,
                              @SerializedName("data") val data: List<CropReportCarryOrderModel>)