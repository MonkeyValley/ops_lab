package com.amplemind.vivasmart.features.reciba_quality.viewModel

import com.amplemind.vivasmart.features.reciba_quality.repository.response.VarietiesDetail

class VarietiesViewModel (variaties: VarietiesDetail) {
    var id = variaties.id
    var name = variaties.name
}