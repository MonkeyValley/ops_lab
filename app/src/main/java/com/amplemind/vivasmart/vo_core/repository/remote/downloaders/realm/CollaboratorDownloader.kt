package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_core.repository.remote.CollaboratorsApi
import io.reactivex.Single
import io.realm.RealmObject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CollaboratorDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mCollaboratorsApi: CollaboratorsApi,
        private val mPref: UserAppPreferences
): RealmDownloader<RealmObject?, CollaboratorModel>(realmHolder, CollaboratorModel::class.java) {

    override fun buildServerRequest(parent: RealmObject?, lastVersion: Long): Single<List<CollaboratorModel>> =
            mCollaboratorsApi.getCollaborators(
                    mPref.authorizationToken,
                    businessUnitId = mPref.user.businessUnitId,
                    version = lastVersion
            ).map { response ->
                Log.e("Version: ", lastVersion.toString())
                Log.e("Data-Collaborator: ", response.data.toString())
                response.data
            }

}

