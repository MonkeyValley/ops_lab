package com.amplemind.vivasmart.features.extensions

import androidx.fragment.app.Fragment
import androidx.appcompat.app.AlertDialog
import com.amplemind.vivasmart.R

fun Fragment.showErrorDialog(error: Throwable) {
    context?.apply {
        AlertDialog.Builder(this)
                .setTitle(R.string.error_dialog_title)
                .setMessage(error.localizedMessage)
                .setPositiveButton(R.string.accept) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }
}