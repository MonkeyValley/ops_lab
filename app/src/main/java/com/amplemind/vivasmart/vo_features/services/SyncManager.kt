package com.amplemind.vivasmart.vo_features.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import android.view.View
import android.widget.RemoteViews
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.VivaSmart
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.RequestType
import com.amplemind.vivasmart.vo_features.services.sync.SyncDownloadModule
import com.amplemind.vivasmart.vo_features.services.sync.SyncService
import com.amplemind.vivasmart.vo_features.services.sync.SyncUploadModule
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SyncManager @Inject constructor (
        private val mContext: Context,
        private val mDownloadModule: SyncDownloadModule,
        private val mUploadModule: SyncUploadModule,
        private val mEventBus: EventBus
) {

    companion object {

        private val TAG = SyncManager::class.java.simpleName

        private val CHANNEL_ID = "${SyncManager::class.java.name}.Notification"
        private const val CHANNEL_NAME = "Viva Smart Sync"
        private val NOTIF_ID = Random().nextInt(9999 - 1000) + 1000

        //Name of the parameter to tell the services that are called to run on the foreground
        internal val EXTRA_FOREGROUND = "${SyncManager::class.java.name}.Foreground"
    }

    private enum class Type {
        DOWNLOADS,
        UPLOADS
    }

    private enum class Status {
        ERROR,
        PROGRESS,
        OK
    }

    private val mNotificationUpdater = PublishSubject.create<Any>()

    private val mSubscriptions = AndroidDisposable()

    private var mIsSetUp: Boolean = false
    private var mCurrentStatus = HashMap<Type, Status>()

    private lateinit var mNotification: Notification
    private lateinit var mNotificationManager: NotificationManager

    private lateinit var mExpandedNotifView: RemoteViews

    fun setUp() {
        if (!mIsSetUp) {
            showNotification()
            setUpCallbacks()
            startServices()
            mIsSetUp = true
        }
    }

    fun cleanUp() {
        if (mIsSetUp) {
            mSubscriptions.dispose()
        }
    }

    fun pair(service: Service) {
        service.startForeground(NOTIF_ID, mNotification)
        showNoPending(Type.DOWNLOADS)
        showNoPending(Type.UPLOADS)
    }

    private fun setUpCallbacks() {
        mEventBus.observe<VivaSmart.AppInBackgroundEvent>{ event ->
            showOpenButton(event.isInBackground)
        }.addTo(mSubscriptions)

        setUpNotificationEvents()

        mDownloadModule.onEvent
                .subscribe(mNotificationUpdater)

        mUploadModule.onEvent
                .subscribe(mNotificationUpdater)
    }

    private fun setUpNotificationEvents() =
            mNotificationUpdater
                    .toFlowable(BackpressureStrategy.LATEST)
                    .concatMap { event ->
                        Flowable.timer(700, TimeUnit.MILLISECONDS)
                                .map {
                                    event
                                }
                    }
                    .subscribe{event ->
                        when (event) {
                            is SyncDownloadModule.DownloadEvent ->
                                onDownloadEvent(event)
                            is SyncUploadModule.UploadEvent ->
                                onUploadEvent(event)
                        }
                    }
                    .addTo(mSubscriptions)

    private fun onDownloadEvent(event: SyncDownloadModule.DownloadEvent) {
        when (event.eventType) {
            SyncDownloadModule.EventType.NOT_READY ->
                showWaitingService(Type.DOWNLOADS)
            SyncDownloadModule.EventType.READY ->
                showNoPending(Type.DOWNLOADS)
            SyncDownloadModule.EventType.COMPLETE ->
                showNoPending(Type.DOWNLOADS)
            SyncDownloadModule.EventType.DOWNLOADING ->
                showDownloading(event.requestType)
            SyncDownloadModule.EventType.SAVING ->
                showSaving(event.requestType)
            SyncDownloadModule.EventType.ERROR ->
                (event as SyncDownloadModule.DownloadErrorEvent).apply {
                    showDownloadError(error)
                }
            SyncDownloadModule.EventType.COUNTDOWN ->
                (event as SyncDownloadModule.RetryCountdownEvent).apply {
                    showDownloadsRetryCountdown(remainingSeconds)
                }
            SyncDownloadModule.EventType.FIRST_SYNC_NEEDED ->
                showFirstSyncNeeded()
        }
    }

    private fun onUploadEvent(event: SyncUploadModule.UploadEvent) {
        when (event.eventType) {
            SyncUploadModule.EventType.NOT_READY ->
                showWaitingService(Type.UPLOADS)
            SyncUploadModule.EventType.READY ->
                showNoPending(Type.UPLOADS)
            SyncUploadModule.EventType.COMPLETE ->
                showNoPending(Type.UPLOADS)
            SyncUploadModule.EventType.UPLOADING ->
                showUploading((event as SyncUploadModule.UploadingEvent).remaining)
            SyncUploadModule.EventType.ERROR ->
                (event as SyncUploadModule.UploadErrorEvent).apply {
                    showUploadError(error, remaining)
                }
            SyncUploadModule.EventType.COUNTDOWN ->
                (event as SyncUploadModule.RetryCountdownEvent).apply {
                    showUploadsRetryCountdown(remainingSeconds, remaining)
                }
        }
    }

    private fun startServices() {
        SyncService.start(mContext, true)
    }

    private fun showFirstSyncNeeded() {
        val messageView = retrieveMessageView(Type.DOWNLOADS)
        mExpandedNotifView.setTextViewText( messageView, mContext.getString( R.string.sync_notif_first_sync_required ))
        showStatus(Type.DOWNLOADS, Status.ERROR)
        updateNotification()
    }

    private fun showDownloading(requestType: RequestType) {
        val messageView = retrieveMessageView(Type.DOWNLOADS)
        mExpandedNotifView.setTextViewText(
                messageView,
                mContext.getString(
                        R.string.sync_notif_downloading
                ).format(requestType.getHumanName(mContext)))
        showStatus(Type.DOWNLOADS, Status.PROGRESS)
        updateNotification()
    }

    private fun showSaving(requestType: RequestType) {
        val messageView = retrieveMessageView(Type.DOWNLOADS)
        mExpandedNotifView.setTextViewText(
                messageView,
                mContext.getString(
                        R.string.sync_notif_saving
                ).format(requestType.getHumanName(mContext)))
        showStatus(Type.DOWNLOADS, Status.PROGRESS)
        updateNotification()
    }

    private fun showUploading(remaining: Int) {
        val messageView = retrieveMessageView(Type.UPLOADS)
        mExpandedNotifView.setTextViewText(messageView, mContext.getString(R.string.sync_notif_uploading).format(remaining))
        showStatus(Type.UPLOADS, Status.PROGRESS)
        updateNotification()
    }

    private fun showDownloadsRetryCountdown(remainingSeconds: Int) {
        val messageView = retrieveMessageView(Type.DOWNLOADS)
        mExpandedNotifView.setTextViewText(messageView, mContext.getString(R.string.sync_notif_download_timeout_countdown)
                .format(remainingSeconds))
        showStatus(Type.DOWNLOADS, Status.PROGRESS)
        updateNotification()
    }

    private fun showUploadsRetryCountdown(remainingSeconds: Int, remainingItems: Int) {
        val messageView = retrieveMessageView(Type.UPLOADS)
        mExpandedNotifView.setTextViewText(
                messageView,
                mContext.getString(R.string.sync_notif_download_timeout_countdown).format(remainingItems)
                .format(remainingSeconds))
        showStatus(Type.UPLOADS, Status.PROGRESS)
        updateNotification()
    }

    private fun showDownloadError(error: Throwable) {
        val messageView = retrieveMessageView(Type.DOWNLOADS)

        val textResource = when (error) {
            is UnknownHostException ->
                R.string.sync_notif_download_network_error
            is SocketTimeoutException ->
                R.string.sync_notif_download_timeout_error
            is HttpException ->
                R.string.sync_notif_download_server_error
            else ->
                R.string.sync_notif_download_error
        }

        mExpandedNotifView.setTextViewText(messageView, mContext.getString(textResource))
        showStatus(Type.DOWNLOADS, Status.ERROR)
        updateNotification()
    }

    private fun showUploadError(error: Throwable, remaining: Int) {
        val messageView = retrieveMessageView(Type.UPLOADS)

        val textResource = when (error) {
            is UnknownHostException ->
                R.string.sync_notif_upload_network_error
            is SocketTimeoutException ->
                R.string.sync_notif_upload_timeout_error
            is HttpException ->
                R.string.sync_notif_upload_server_error
            else ->
                R.string.sync_notif_upload_error
        }

        mExpandedNotifView.setTextViewText(messageView, mContext.getString(textResource).format(remaining))
        showStatus(Type.UPLOADS, Status.ERROR)
        updateNotification()
    }

    private fun showNoPending(type: Type) {
        val messageView = retrieveMessageView(type)
        mExpandedNotifView.setTextViewText(messageView, mContext.getString(R.string.sync_notif_no_pending))
        showStatus(type, Status.OK)
        updateNotification()
    }

    private fun showWaitingService(type: Type) {
        val messageView = retrieveMessageView(type)
        mExpandedNotifView.setTextViewText(messageView, mContext.getString(R.string.sync_notif_waiting_service))
        showStatus(type, Status.PROGRESS)
        updateNotification()
    }

    private fun showStatus(type: Type, status: Status) {
        if (mCurrentStatus[type] != status) {
            when (status) {
                Status.PROGRESS -> showProgress(type, true)
                Status.ERROR -> showStatus(type, R.drawable.ic_warning_red_24dp)
                Status.OK -> showStatus(type, R.drawable.ic_accept_new)
            }
        }
        mCurrentStatus[type] = status
    }

    private fun showStatus(type: Type, iconResourceId: Int) {
        showProgress(type, false)
        val imageViewId = retrieveStatusView(type)
        mExpandedNotifView.setImageViewResource(imageViewId, iconResourceId)
    }


    private fun showProgress(type: Type, show: Boolean) {
        val progressId = retrieveProgressView(type)
        val statusId = retrieveStatusView(type)

        mExpandedNotifView.setViewVisibility(statusId, if (show) View.INVISIBLE else View.VISIBLE)
        mExpandedNotifView.setViewVisibility(progressId, if (show) View.VISIBLE else View.INVISIBLE)
    }

    private fun retrieveMessageView(type: Type) =
            when (type) {
                Type.UPLOADS -> R.id.tv_pending_uploads
                else -> R.id.tv_pending_downloads
            }

    private fun retrieveStatusView(type: Type) =
            when (type) {
                Type.UPLOADS -> R.id.iv_upload_status
                else -> R.id.iv_download_status
            }

    private fun retrieveProgressView(type: Type) =
            when (type) {
                Type.UPLOADS -> R.id.upload_progress
                else -> R.id.download_progress
            }

    private fun showOpenButton(show: Boolean) {
        //mExpandedNotifView.setViewVisibility(R.id.btn_open, if (show) View.VISIBLE else View.INVISIBLE)
        updateNotification()
    }

    private fun showNotification() {

        mNotificationManager =
                mContext.getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager

        mExpandedNotifView = RemoteViews(mContext.packageName, R.layout.notification_sync)

        mExpandedNotifView.setImageViewResource(R.id.icon, R.mipmap.ic_launcher_background)

        val notificationBuilder = NotificationCompat.Builder(mContext)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setCustomBigContentView(mExpandedNotifView)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            // Create a notification and set the notification channel.
            mNotification = notificationBuilder
                    .setOnlyAlertOnce(true)
                    .setChannelId(CHANNEL_ID)
                    .build()

            mNotificationManager.createNotificationChannel(mChannel)
        } else {
            mNotification = notificationBuilder.build()
        }

        updateNotification()
    }

    private fun updateNotification() {
        mNotificationManager.notify(NOTIF_ID, mNotification)
    }

}