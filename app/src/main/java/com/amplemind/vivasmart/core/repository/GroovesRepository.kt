package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.CollaboratorsDao
import com.amplemind.vivasmart.core.repository.local.GroovesDao
import com.amplemind.vivasmart.core.repository.model.Grooves
import com.amplemind.vivasmart.core.repository.model.GroovesAssingReponse
import com.amplemind.vivasmart.core.repository.model.GroovesResultReponse
import com.amplemind.vivasmart.core.repository.remote.ActivityCodeApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.core.utils.TIME_OUT_GROOVES
import com.amplemind.vivasmart.vo_core.repository.dao.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.ActivityLogUploader
import io.reactivex.Observable
import io.reactivex.Single
import org.jetbrains.anko.doAsync
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GroovesRepository @Inject constructor(private val api: ActivityCodeApi,
                                            private val apiAuthorization: ApiAuthorization,
                                            private val database: GroovesDao,
                                            private val groovesDao: com.amplemind.vivasmart.vo_core.repository.dao.GroovesDao,
                                            private val logsDao: ActivityLogsDao,
                                            private val collaboratorDatabase: CollaboratorsDao,
                                            private val logsUploader: ActivityLogUploader) {

    fun finishCollaboratorGroove(token: String, request: ActivityCodeRepository.FinishCollaboratorGrooveRequest): Observable<GroovesResultReponse> {
        return api.finishCollaboratorGrooves(apiAuthorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun loadAssignedGrooves(activityCode: ActivityCodeModel): Observable<List<GrooveModel>> {
        return Observable.fromCallable{
            return@fromCallable groovesDao.getAssigned(activityCode)
        }
    }

    fun saveGrooves(activityLog: ActivityLogModel, grooves: List<GrooveModel>): Observable<ActivityLogModel?> {
        return Observable.fromCallable{

            val savedGrooves = groovesDao.save(activityLog, grooves)

            return@fromCallable if (savedGrooves != null) {
                logsDao.assingGrooves(activityLog, savedGrooves)
            }
            else {
                null
            }
        }
        .doOnNext {
            logsUploader.update(it!!, true)
        }
    }

    fun getGroovesAssing(token: String, activity_code_id: Int): Observable<GroovesAssingReponse> {
        return api.getGroovesAssing(apiAuthorization.getAuthToken(token), activity_code_id)
                .timeout(TIME_OUT_GROOVES, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun updateGrooves(token: String, request: ActivityCodeRepository.GroovesUpdate): Observable<Any> {
        return api.updateGrooves(apiAuthorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun assingGrooves(token: String, request: ActivityCodeRepository.AssignGroovesRequest): Observable<GroovesResultReponse> {
        return api.assignGrooves(apiAuthorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalGrooves(activity_code_id: Int): Single<List<Grooves>> {
        return database.getGrooves(activity_code_id)
    }

    fun saveGrooves(grooves: List<Grooves>) {
        grooves.map { database.insertGroove(it) }
    }

    fun saveLocalGrooves(grooves: List<ActivityCodeRepository.GroovesAssing>?, activityLog: GroovesResultReponse, collaboratorId: Int): Observable<Long> {
        return Observable.fromCallable {
            val activityLogId = database.insertActivityLog(activityLog)
            val collaborator = collaboratorDatabase.getCollaboratorByIdAndCode(collaboratorId, activityLog.activity_code_id)
            val activityLogs = mutableListOf(activityLog)
            if (collaborator!!.collaborator.activityLogs != null) {
                collaborator.collaborator.activityLogs!!.map { activityLogs.add(it) }
            }
            collaborator.collaborator.activityLogs = activityLogs
            collaboratorDatabase.updateCollaborator(collaborator)

            activityLog.id = activityLogId
            if (grooves != null) {
                grooves.map { it ->
                    var groove = database.getGrooveBy(it.groove_no, it.table_no)
                    if (groove == null) {
                        groove = Grooves(activityLog, activityLog.id, it.groove_no, null, it.table_no)
                    } else {
                        groove.activity_log = activityLog
                    }
                    database.insertGroove(groove)
                }
            }
            activityLogId
        }
    }

    fun updateActivityLogQuantity(activityLogId: Long, quantiy: Int, collaboratorId: Int, activity_code_id: Int): Observable<Any> {
        return Observable.fromCallable {
            database.updateActivityLogQuantity(quantiy, activityLogId)
            val collaborator = collaboratorDatabase.getCollaboratorByIdAndCode(collaboratorId, activity_code_id)
            collaborator?.collaborator?.activityLogs?.map { log ->
                if (log.id == activityLogId) {
                    log.quantity = quantiy
                }
            }
            collaboratorDatabase.updateCollaborator(collaborator!!)
        }
    }

    fun updateCollaboratorGrooves(activityLogId: Long, grooves: List<ActivityCodeRepository.GroovesAssing>, collaboratorId: Int) {
        doAsync {
            var activityLog = database.getActivityLog(activityLogId)

            if (activityLog != null) {
                val collaborator = collaboratorDatabase.getCollaboratorById(collaboratorId)
                val filtered = collaborator!!.collaborator.activityLogs?.filter { it.id == activityLogId }
                if (filtered?.isNotEmpty() ?: false) {
                    filtered!!.first().quantity = grooves.size
                    collaboratorDatabase.updateCollaborator(collaborator)
                }

                database.updateActivityLogQuantity(grooves.size, activityLogId)
                activityLog = database.getActivityLog(activityLogId)
                database.getGroovesByLogId(activityLogId).map {
                    if (it.id != null) {
                        database.deleteGroove(it.id)
                    }
                }

                grooves.map { it ->
                    var groove = database.getGrooveBy(it.groove_no, it.table_no)
                    if (groove == null) {
                        groove = Grooves(activityLog!!, 0, it.groove_no, null, it.table_no)
                    } else {
                        groove.activity_log = activityLog!!
                    }
                    database.insertGroove(groove)
                }
            }
        }
    }

}
