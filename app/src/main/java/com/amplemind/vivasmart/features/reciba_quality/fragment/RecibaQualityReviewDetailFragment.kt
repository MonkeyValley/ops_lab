package com.amplemind.vivasmart.features.reciba_quality.fragment

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.model.ImagesModel
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.model.IssuesModel
import com.amplemind.vivasmart.features.gallery.GalleryDialog
import com.amplemind.vivasmart.features.production_quality.ItemReportQualityReviewDetailViewModel
import com.amplemind.vivasmart.features.reciba_quality.RecibaQualityReviewActivity
import com.amplemind.vivasmart.features.reciba_quality.RecibaQualityReviewDetailActivity
import com.amplemind.vivasmart.features.reciba_quality.adapter.RecibaQualityIssuesAdapter
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ReacibaQualityReviewViewModel
import com.amplemind.vivasmart.features.reciba_quality.viewModel.RecibaQualityReviewDetailViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.dialog_image_finished_product.view.*
import kotlinx.android.synthetic.main.fragment_product_quality_review_detail.*
import me.grantland.widget.AutofitTextView
import java.math.RoundingMode
import javax.inject.Inject

class RecibaQualityReviewDetailFragment : BaseFragment(), HasSupportFragmentInjector {
    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>
    lateinit var progressDialogInner: ProgressDialog

    var cropId = 0

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return childFragmentInjector
    }

    companion object {
        const val LIST_IMG_URL_JSON = "listImgUrlJSON"
        lateinit var totalExportacion: TextView
        lateinit var percentExportacion: TextView
        lateinit var totalDamaged: TextView
        lateinit var percentDamaged: TextView
        lateinit var totalSample: TextView
        lateinit var totalBrix: TextView
        var newReport: Boolean = false
        var idReport: String = ""
        var lotName: String = ""
        var packingName: String = ""

        fun newIntent(listImgUrlJSON: String?): Intent? {
            val intent = Intent()
            intent.putExtra(LIST_IMG_URL_JSON, listImgUrlJSON)
            return intent
        }

        fun ifConected(): Boolean {
            return if (RecibaQualityReviewDetailFragment().hasInternet())
                true
            else {
                Toast.makeText(RecibaQualityReviewDetailFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

    @Inject
    lateinit var viewModel: RecibaQualityReviewDetailViewModel
    var recibaQualityReviewOjb: ReacibaQualityReviewViewModel? = null
    lateinit var listaIssues: List<ItemReportQualityReviewDetailViewModel>
    lateinit var btnSend: Button
    lateinit var prodNameLayout: LinearLayout
    lateinit var headerFragment: LinearLayout
    lateinit var linearBrix: LinearLayout
    lateinit var idReportLabel: AutofitTextView
    lateinit var lotNameLabel: AutofitTextView
    lateinit var cropNameLabel: AutofitTextView
    lateinit var packingNameLabel: AutofitTextView

    fun newInstance(title: String, newReport: Boolean, idReport: String, lotName: String, packingName: String, cropId: Int): RecibaQualityReviewDetailFragment {
        val args = Bundle()
        args.putString("Title", title)
        args.putBoolean("newReport", newReport)
        args.putString("idReport", idReport)
        args.putString("lotName", lotName)
        args.putString("packingName", packingName)
        args.putInt("cropId", cropId)

        val fragment = RecibaQualityReviewDetailFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_reciba_quality_review_detail, container, false)
        addSubscribers()
        recibaQualityReviewOjb = RecibaQualityReviewDetailActivity.sharedPref.getSavedObjectFromPreference("recibaQualityReviewOjb", ReacibaQualityReviewViewModel::class.java)
        newReport = arguments!!.getBoolean("newReport")
        cropId = arguments!!.getInt("cropId", 0)
        idReport = arguments!!.getString("idReport", "0")
        lotName = arguments!!.getString("lotName", "0")
        packingName = arguments!!.getString("packingName", "0")

        setDialogLoadingInner()
        setViewElements(view)
        setListeners()
        getInfoIssues()

        return view
    }

    fun setViewElements(view: View) {
        btnSend = view.findViewById(R.id.btn_send)
        totalExportacion = view.findViewById(R.id.total_exportacion)
        percentExportacion = view.findViewById(R.id.percent_exportacion)
        totalDamaged = view.findViewById(R.id.total_damaged)
        percentDamaged = view.findViewById(R.id.percent_damaged)
        totalSample = view.findViewById(R.id.total_sample)

        prodNameLayout = view.findViewById(R.id.LL_header_detail)
        idReportLabel = view.findViewById(R.id.TV_id)
        lotNameLabel = view.findViewById(R.id.TV_lot)
        cropNameLabel = view.findViewById(R.id.TV_crop)
        packingNameLabel = view.findViewById(R.id.TV_packing)
        headerFragment = view.findViewById(R.id.LL_header)

        linearBrix = view.findViewById(R.id.LL_brix)
        totalBrix = view.findViewById(R.id.tv_brix)
    }

    fun setListeners() {
        btnSend.setOnClickListener { v: View -> onClickEvidenceAndSign(v) }
        totalExportacion.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    if (p0.isNotBlank()) {
                        var totalExpo: Int = Integer.parseInt(totalExportacion.text.toString())
                        var totalSampInt: Int = Integer.parseInt(totalSample.text.toString())

                        if (totalExpo <= totalSampInt) {
                            // Calculo de porcentajes y cantidades
                            var porcentajeExpo = ((totalExpo * 100.00) / totalSampInt)
                            var porTransf = porcentajeExpo.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toString() + "%"
                            percentExportacion.text = porTransf

                            var totalDamageInt = (totalSampInt - totalExpo)
                            var porcentajeDamage = ((totalDamageInt * 100.00) / totalSampInt)
                            var porDamg = porcentajeDamage.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toString() + "%"

                            totalDamaged.text = totalDamageInt.toString()
                            percentDamaged.text = porDamg
                            btnSend!!.visibility = View.VISIBLE
                        } else {
                            showSnackBar("La cantidad de exportacdión debe ser menor a la cantidad de la muestra")
                            percentExportacion.text = "0.00%"
                            totalDamaged.text = "0"
                            percentDamaged.text = "0.0%"
                            btnSend.visibility = View.GONE
                        }
                    } else {
                        btnSend.visibility = View.GONE
                    }

                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    fun setDialogLoadingInner() {
        progressDialogInner = ProgressDialog(context)
        progressDialogInner.setTitle(getString(R.string.app_name))
        progressDialogInner.setMessage(getString(R.string.server_loading))
        progressDialogInner.setCancelable(false)
    }

    fun getInfoIssues() {
        if (recibaQualityReviewOjb != null) {
            totalSample.text = recibaQualityReviewOjb!!.sample.toString()
            getListIssues(cropId)
        }
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_issues().subscribe(this::updateList_issues).addTo(subscriptions)
        viewModel.onSuccessRequest_report().subscribe(this::returnToMain).addTo(subscriptions)
    }

    fun onClickEvidenceAndSign(view: View) {
        val dialog = GalleryDialog().newInstance(totalDamaged.text.toString().toDouble(), 0)
        dialog.setTargetFragment(this, 100)
        dialog.show(fragmentManager!!, GalleryDialog.TAG)
    }

    fun getListIssues(id_crop: Int) {
        if (ifConected()) {
            viewModel.getIssuesList(id_crop)
        }
    }

    private fun updateList_issues(list: List<ItemReportQualityReviewDetailViewModel>) {
        val adapter = RecibaQualityIssuesAdapter(list.toMutableList(), recibaQualityReviewOjb!!, true)
        rv_quality_issues.hasFixedSize()
        rv_quality_issues.layoutManager = LinearLayoutManager(context)
        rv_quality_issues.itemAnimator = DefaultItemAnimator()
        rv_quality_issues.adapter = adapter
        adapter.onClick().subscribe(this::loadModalImages).addTo(subscriptions)
        listaIssues = list
    }

    fun loadModalImages(unit: ItemReportQualityReviewDetailViewModel) {
        Log.d("imagen", unit.image.toString())
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.dialog_image_finished_product, null, false)
        builder.setCancelable(true)
        builder.setView(mView)
        mView.TV_issue_name.text = unit.name
        if (unit.image != null) {
            mView.TV_issue_empty_name.visibility = View.GONE
            mView.IV_issue_image.visibility = View.VISIBLE

            var urlImg = unit.image
            Glide.with(this)
                    .load(urlImg)
                    .apply(RequestOptions().placeholder(R.drawable.ic_star_menu))
                    .into(mView.IV_issue_image)

        } else {
            mView.TV_issue_empty_name.visibility = View.VISIBLE
            mView.IV_issue_image.visibility = View.GONE
        }
        builder.show()
    }

    fun prepareData(listUrls: ArrayList<String>) {
        var exportNo = Integer.parseInt(totalExportacion.text.toString())
        var listIssues = ArrayList<IssuesModel>()
        var listImages = ArrayList<ImagesModel>()
        val issuMod = IssueModel(0, false, "", "", "")

        // el primer elemento del array es la firma
        var sign: String = listUrls[0]

        for (itemUrl in listUrls) {
            val imageModel = ImagesModel(itemUrl)
            listImages.add(imageModel)
        }

        for (issue in listaIssues) {
            val issueModel = IssuesModel(issue.id, Integer.parseInt(issue.frequency_no), issuMod)
            listIssues.add(issueModel)
        }

        recibaQualityReviewOjb?.export = exportNo
        recibaQualityReviewOjb?.sign = sign
        recibaQualityReviewOjb?.issues = listIssues
        recibaQualityReviewOjb?.images = listImages

        Log.d("ObjFinishedProdReport", Gson().toJson(recibaQualityReviewOjb))
        sendDataReport()
    }

    fun sendDataReport() {
        if (ifConected()) {
            showProgressDialog(true)
            viewModel.postFinishedRecibaQualityReview(recibaQualityReviewOjb!!)
        }
    }

    fun returnToMain(finishRecibaQuality: ReacibaQualityReviewViewModel) {
        Log.d("finalString", Gson().toJson(finishRecibaQuality))
        var intent = Intent()
        intent.putExtra("finishedProduct", Gson().toJson(finishRecibaQuality))
        activity!!.setResult(Activity.RESULT_OK, intent)
        activity!!.finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == 100) {
            val jsonList = data!!.getStringExtra(LIST_IMG_URL_JSON)
            val listUrls: ArrayList<String> = Gson().fromJson(jsonList, object : TypeToken<ArrayList<String?>?>() {}.getType())
            prepareData(listUrls)
        }
    }

    fun showSnackBar(msj: String){
        val snackbar = Snackbar.make(activity!!.findViewById(android.R.id.content), msj,
                Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text)
                .setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        snackbar.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        showProgressDialog(false)
    }

}
