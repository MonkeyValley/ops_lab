package com.amplemind.vivasmart.vo_features.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.amplemind.vivasmart.R

class SyncNotificationManager {

    companion object {
        const val NOTIFICATION_ID = 1
    }

    private var mServiceCounter: Int = 0
    private var mNotification: Notification? = null

    fun pair(service: Service) {

        if (mNotification == null) {
            build(service.applicationContext)
        }

        service.startForeground(NOTIFICATION_ID, mNotification)
        mServiceCounter ++
    }

    fun unpair(service: Service) {
        service.stopForeground(mServiceCounter == 1)
        mServiceCounter --
    }

    private fun build(context: Context) {
        val channelId =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel(context, "vivasmart-fm", "Viva Smart Sync Status")
                } else {
                    // If earlier version channel ID is not used
                    // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                    ""
                }
        // Create notification default intent.
        val intent = Intent()
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

        // Create notification builder.
        val builder = NotificationCompat.Builder(context, channelId)

        // Make notification show big text.
        val notif = NotificationCompat.BigTextStyle()
        notif.setBigContentTitle("Viva Smart Data Sync");
        notif.bigText("Viva Smart se encuentra ejecutándose en segundo plano...");
        // Set big text style.
        builder.setStyle(notif)
        builder.setWhen(System.currentTimeMillis())
        builder.setSmallIcon(R.mipmap.ic_launcher)
        //val largeIconBitmap = BitmapFactory.decodeResource (context.resources, R.drawable.con_music_32);
        //builder.setLargeIcon(largeIconBitmap);
        // Make the notification max priority.
        builder.priority = Notification.PRIORITY_LOW;
        // Make head-up notification.
        builder.setFullScreenIntent(pendingIntent, true);

        /*
        // Add Play button intent in notification.
        Intent playIntent = new Intent(this, MyForeGroundService.class);
        playIntent.setAction(ACTION_PLAY);
        PendingIntent pendingPlayIntent = PendingIntent . getService (this, 0, playIntent, 0);
        NotificationCompat.Action playAction = new NotificationCompat.Action(android.R.drawable.ic_media_play, "Play", pendingPlayIntent);
        builder.addAction(playAction);

        // Add Pause button intent in notification.
        Intent pauseIntent = new Intent(this, MyForeGroundService.class);
        pauseIntent.setAction(ACTION_PAUSE);
        PendingIntent pendingPrevIntent = PendingIntent . getService (this, 0, pauseIntent, 0);
        NotificationCompat.Action prevAction = new NotificationCompat.Action(android.R.drawable.ic_media_pause, "Pause", pendingPrevIntent);
        builder.addAction(prevAction);
        */

        // Build the notification.
        mNotification = builder.build()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(context: Context, channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

}