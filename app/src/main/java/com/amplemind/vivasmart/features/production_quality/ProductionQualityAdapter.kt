package com.amplemind.vivasmart.features.production_quality

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemControlQualityBinding
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlQualityItemViewModel
import io.reactivex.subjects.BehaviorSubject

class ProductionQualityAdapter constructor(val context: Context): RecyclerView.Adapter<ProductionQualityAdapter.ProductionQualityViewHolder>() {

    private var list = mutableListOf<ControlQualityItemViewModel>()
    private val clickItem:BehaviorSubject<ControlQualityItemViewModel> = BehaviorSubject.create()

    fun addElements(data : List<ControlQualityItemViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductionQualityViewHolder {
        val binding = ItemControlQualityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductionQualityViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ProductionQualityViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onItemClicked(): BehaviorSubject<ControlQualityItemViewModel> {
        return clickItem
    }

    inner class ProductionQualityViewHolder(private val binding: ItemControlQualityBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item : ControlQualityItemViewModel

        fun bind(item: ControlQualityItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()

            this.item = item
            val tvCounter = binding.root.findViewById<TextView>(R.id.tv_counter)
            tvCounter.visibility = View.GONE

            binding.root.setOnClickListener{
                //if (!item.isComplete()) {
                    clickItem.onNext(item)
                //}
            }
        }

    }

}