package com.amplemind.vivasmart.features.production_quality

import android.util.Log
import com.amplemind.vivasmart.core.repository.ProductionQualityRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ProductionCategoriesModel
import com.amplemind.vivasmart.core.repository.request.CloseIssueRequest
import com.amplemind.vivasmart.core.repository.response.IncidentsResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.ReplaySubject
import java.io.File
import java.lang.Math.round
import javax.inject.Inject

class ProductionCategoriesViewModel @Inject constructor(private val repository: ProductionQualityRepository,
                                                        private val preferences: UserAppPreferences,
                                                        private val uploadRepository: ProfileRepository,
                                                        private val apiErrors: HandleApiErrors) {


    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val requestFail: BehaviorSubject<String> = BehaviorSubject.create()
    private val requestSuccess: BehaviorSubject<List<ProductionCategoriesItemViewModel>> = BehaviorSubject.create()
    private val successGetOpenIssues: BehaviorSubject<MutableList<IncidentItemViewModel>> = BehaviorSubject.create()
    private val successCloseIncident: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val successUploadSign: BehaviorSubject<MutableList<File>> = BehaviorSubject.create()
    private val successGetCount: ReplaySubject<Boolean> = ReplaySubject.create()
    private val successGetGeneralQualityReport: ReplaySubject<Boolean> = ReplaySubject.create()
    private val successFinishActivity: BehaviorSubject<Boolean> = BehaviorSubject.create()

    private val categories = mutableListOf<ProductionCategoriesItemViewModel>()
    private val signs = mutableListOf<String>()

    private var issues =  mutableListOf<IncidentItemViewModel>()
    private var generalQualityAvg: Int = 0

    fun getSignatureTitle(): String {
        return "Terminar reporte"
    }

    fun getSignatureMessage(): String {
        return "¿Estas seguro de terminar sin haber registrado ningún reporte? Por favor firme para confirmar:"
    }

    fun getActivities() : Disposable {
        signs.clear()
        return repository.getActivities(preferences.token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> it.map { categories.add(ProductionCategoriesItemViewModel(it)) } }
                .subscribe({
                    requestSuccess.onNext(categories)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }


    fun getCountIssues(lotId: Int) : Disposable {
        return repository.getCountIssues(preferences.token, lotId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { list ->
                    mapCategories(list.data)
                    repository.saveCategories(lotId, list.data as MutableList<ProductionCategoriesModel>, categories)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    successGetCount.onNext(true)
                }, {
                    //There is not activities to update
                    progressStatus.onNext(false)
                    Log.e("Error", apiErrors.handleLoginError(it))
                })
    }

    fun getLocalCountIssues(lotId: Int) : Disposable {
        return repository.getLocalCountIssues(lotId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { list ->
                    mapCategories(list.data)
                }
                .subscribe({
                    successGetCount.onNext(true)
                }, {
                    Log.e("Error", apiErrors.handleLoginError(it))
                })
    }

    private fun mapCategories(list: List<ProductionCategoriesModel>) {
        /*
        list.map { it1 ->
            val modelId = it1.id
            val filtered = categories.filter { it.id == modelId }
            if (filtered.isNotEmpty()) {
                Log.i("Log", "Updated id: " + filtered.first().id + ", to count: " + it1.total)
                filtered.first().updateIncidences(it1.total)
            }
        }
        */
        if (list.isEmpty()){
            categories.map {
                it.updateIncidences(0)
            }
            return
        }
        for (cat in categories) {
            for (item in list) {
                if (cat.id == item.id) {
                    cat.updateIncidences(item.total)
                    break
                }
            }
        }
    }

    fun getOpenIncidents(stageId: Int): Disposable {
        return repository.getOpenIncidents(preferences.token, stageId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map(this::updateOpenIssues)
                .subscribe( {
                    successGetOpenIssues.onNext(issues)
                }, {
                    Log.i("Log", "Error " + it.message)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun closeIncident(incidentId: Int): Disposable {

        val request = CloseIssueRequest(true)

        return repository.closeIncident(preferences.token, incidentId, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe{ progressStatus.onNext(true) }
                .doOnTerminate{ progressStatus.onNext(false) }
                .subscribe({
                    successCloseIncident.onNext(it != null)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getGeneralQualityReport(lotId: Int, catItems: List<ProductionCategoriesItemViewModel>): Disposable {
        return repository.getGeneralQualityReport(preferences.token, lotId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe{progressStatus.onNext(true)}
                .doOnTerminate{progressStatus.onNext(false)}
                .map{

                    //Assign scores to each category
                    for (item in catItems) {
                        for (score in it[0].scores) {
                            if (item.id == score.catId) {
                                if (score.score > 0) {
                                    item.score = round(score.score).toInt()
                                }
                                else {
                                    item.score = 100
                                }
                            }
                        }
                    }

                    //Calculate average
                    catItems.forEach{ t -> generalQualityAvg += t.score }
                    generalQualityAvg /= catItems.size

                } .subscribe({
                    successGetGeneralQualityReport.onNext(true)
                },{
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun updateSign(files: MutableList<File>): Disposable {
        return uploadRepository.uploadImage(files.first(), "sign")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    if (files.isNotEmpty()) {
                        files.removeAt(0)
                    }
                    signs.add(it.relative_url)
                    successUploadSign.onNext(files)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun finishActivity(lotId: Int): Disposable? {
        if (signs.size >= 2) {
            return repository.finishActivities(preferences.token, signs[0], signs[1], lotId, 1)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { progressStatus.onNext(true) }
                    .doOnTerminate { progressStatus.onNext(false) }
                    .subscribe({
                        successFinishActivity.onNext(true)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                    })
        }
        requestFail.onNext("Son necesarias 2 firmas")
        return null
    }

    fun finishLocalActivity(sign1: String, sign2: String, lotId: Int): Disposable {
        return repository.finishLocalActivity(sign1, sign2, lotId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    successFinishActivity.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun updateOpenIssues( response: IncidentsResponse ) {
        issues.clear()
        response.data.forEach { t -> issues.add(IncidentItemViewModel(t)) }
    }

    fun getGeneralQualityAverage(): String {
        return "$generalQualityAvg%"
    }

    fun updateIncidents(index: Int, incidents: Int) {
        categories[index].updateIncidences(incidents)
    }

    fun hasReports(): Boolean {
        return categories.sumBy { it.count } > 0
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onRequestSuccess(): BehaviorSubject<List<ProductionCategoriesItemViewModel>> {
       return requestSuccess
    }

    fun onSuccessGetOpenIssues(): BehaviorSubject<MutableList<IncidentItemViewModel>>{
        return successGetOpenIssues
    }

    fun onSuccessCloseIncident(): BehaviorSubject<Boolean> {
        return successCloseIncident
    }

    fun onSuccessUpdateSign(): BehaviorSubject<MutableList<File>> {
        return successUploadSign
    }

    fun onSuccessFinishActivity(): BehaviorSubject<Boolean> {
        return successFinishActivity
    }

    fun onSuccessGetCountIssues(): ReplaySubject<Boolean> {
        return successGetCount
    }

    fun onSuccessGetGeneralReport(): ReplaySubject<Boolean> {
        return successGetGeneralQualityReport
    }
}