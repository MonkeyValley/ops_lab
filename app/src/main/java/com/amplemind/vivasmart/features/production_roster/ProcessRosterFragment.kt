package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.quality_control.ControlListAdapter
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PendingLogsModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.TimerFragmentViewModel
import kotlinx.android.synthetic.main.content_process_roster_fragment.*
import javax.inject.Inject

class ProcessRosterFragment : BaseFragment() {


    companion object {

        private val TAG: String = ProcessRosterFragment::class.java.simpleName

        private val PARAM_STAGE_UUID    = "$TAG.StageUuid"
        private val PRAM_STAGE_TYPE_ID  = "$TAG.StageTypeId"
        private var lisFilter = mutableListOf<PendingLogsModel>()
        private var stageList = mutableListOf<PendingLogsModel>()
        const val REQUEST_SIGN  = 200

        fun newInstance(stageUuid: String, stageTypeId: Int): ProcessRosterFragment =
            ProcessRosterFragment().apply {
                arguments = Bundle().apply {
                    putString(PARAM_STAGE_UUID, stageUuid)
                    putInt(PRAM_STAGE_TYPE_ID, stageTypeId)
                }
            }

    }

    init {
        argumentsNeeded = true
    }

    @Inject
    lateinit var mViewModel: ProcessRosterViewModel

    @Inject
    lateinit var mTViewModel: TimerFragmentViewModel

    @Inject
    lateinit var viewModel: RosterDetailViewModel

    private lateinit var mAdapter: ControlListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var inf = inflater.inflate(R.layout.content_process_roster_fragment, container, false)

        var txtsearch = inf.findViewById<androidx.appcompat.widget.SearchView>(R.id.search_view_process)
        txtsearch.setOnQueryTextListener(object: SearchView.OnQueryTextListener, androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText!!.isNotEmpty()){
                    lisFilter.clear()

                    stageList.forEach {
                        if(it.collaborator.name.toLowerCase().contains(newText.toLowerCase()) || it.collaborator.employeeCode.contains(newText.toLowerCase())) {
                            Log.e(TAG,"--IT: "+it.collaborator.name)
                            lisFilter.add(it)
                        }
                    }
                    mAdapter.items = lisFilter.toList()
                    Log.e(TAG,"--LIST BEFORE: "+mAdapter.items.count())
                    Log.e(TAG,"--LIST FILTER: "+ lisFilter.count())


                }else {
                    lisFilter.clear()
                    mAdapter.items = stageList
                }

                return true
            }
        })

        return inf
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mAdapter.cleanUp()
        mViewModel.cleanUp()
    }

    override fun readArguments(args: Bundle) {
        super.readArguments(args)
        mViewModel.stageUuid = requireNotNull(args.getString(PARAM_STAGE_UUID))
        mViewModel.stageTypeId = requireNotNull(args.getInt(PRAM_STAGE_TYPE_ID, 0))
    }

    override fun setUpCallbacks() {
        super.setUpCallbacks()

        mEventBus.observe<RosterDetailActivity.UpdateReportEvent>()
                .filter { event ->
                    mViewModel.stageUuid == event.stageUuid
                }
                .subscribe(this::onUpdateReportEvent)
                .addTo(subscriptions)
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()

        mEventBus.observe<ControlListAdapter.PendingLogsClickedEvent>()
                .filter { event ->
                    !event.readOnly && event.pendingLogs.stage.uuid == mViewModel.stageUuid
                }
                .subscribe(this::onPendingLogsClickedEvent)
                .addTo(subscriptions)

        mEventBus.observe<ControlListAdapter.PendingLogsLongClickedEvent>()
                .filter { event ->
                    !event.readOnly && event.pendingLogs.stage.uuid == mViewModel.stageUuid
                }
                .subscribe(this::onPendingLogsLongClickedEvent)
                .addTo(subscriptions)
    }

    override fun setUpUI() {
        super.setUpUI()
        initRecycler()
    }

    override fun loadData() {
        super.loadData()
        mViewModel.getPendingLogs().subscribe(this::onPendingLogsLoaded).addTo(subscriptions)
    }

    private fun onUpdateReportEvent(event: RosterDetailActivity.UpdateReportEvent) {
        loadData()
    }

    private fun onPendingLogsLoaded(pendingLogs: List<PendingLogsModel>) {

        mAdapter.items = pendingLogs
        lisFilter = pendingLogs.toMutableList()
        stageList = pendingLogs.toMutableList()

    }

    private fun onPendingLogsClickedEvent(event: ControlListAdapter.PendingLogsClickedEvent) {
        startActivity(Intent(context!!, RosterDetailActivity::class.java).apply {
            putExtra(RosterDetailActivity.PARAM_COLLABORATOR_UUID, event.pendingLogs.collaborator.uuid)
            putExtra(RosterDetailActivity.PARAM_STAGE_UUID, event.pendingLogs.stage.uuid)
            putExtra(RosterDetailActivity.PARAM_TYPE, RosterDetailActivity.TYPE_UNSIGNED)
        })
    }

    private fun onPendingLogsLongClickedEvent(event: ControlListAdapter.PendingLogsLongClickedEvent) {
        viewModel.stageUuid = event.pendingLogs.stage.uuid
        viewModel.collaboratorUuid = event.pendingLogs.collaborator.uuid
        viewModel.type = RosterDetailActivity.TYPE_UNSIGNED
        viewModel.loadActivityLogs().subscribe(this::onActivityLogsLoaded).addTo(subscriptions)
    }

    private fun onActivityLogsLoaded(activityLogs: List<ActivityLogModel>) {
        if(activityLogs.isNotEmpty()) {
            Log.e("activityLogs.size", activityLogs.size.toString())

            AlertDialog.Builder(activity!!)
                    .setTitle("ELIMINAR ACTIVIDAD")
                    .setMessage("¿Seguro que desea eliminar las actividades de este colaborador?")
                    .setPositiveButton(R.string.accept) { _, _ ->

                        showProgressDialog(true)

                        activityLogs.forEach { activityLog ->
                            mTViewModel.removeRosterDetail(activityLog)
                                    .subscribe({}, { error ->
                                        showErrorDialog(error)
                                    }).addTo(subscriptions)
                        }

                        reloadData()
                        showProgressDialog(false)
                    }
                    .setNegativeButton(R.string.cancel, null)
                    .show()
        }
    }

    private fun reloadData(){
        mAdapter.cleanUp()
        mViewModel.cleanUp()
        loadData()
    }

    private fun initRecycler() {
        rv_process.setHasFixedSize(true)
        rv_process.layoutManager = LinearLayoutManager(context)
        rv_process.itemAnimator = DefaultItemAnimator()
        rv_process.setPadding(0, 8.toPx(), 0, 70.toPx())
        rv_process.clipToPadding = false

        mAdapter = ControlListAdapter(mEventBus, false)
        rv_process.adapter = mAdapter
    }

    /*
    private fun initComparison() {
        startActivityForResult(Intent(context, ComparisonActivity::class.java)
                .putExtra("flow", 1)
                .putExtra("stage", mStage!!.id), 100)
    }

     */

    override fun onResume() {
        super.onResume()
        loadData()
    }
}
