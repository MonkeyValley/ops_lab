package com.amplemind.vivasmart.features.waste_control.fragment


import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.features.production_quality.UnitsViewModel
import com.amplemind.vivasmart.features.reciba_quality.model.UnitItem
import com.amplemind.vivasmart.features.waste_control.viewModel.WasteControlTestFragmentViewModel
import com.amplemind.vivasmart.features.waste_control.dialogs.SignDialog
import com.amplemind.vivasmart.features.waste_control.models.WasteControlItemModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import java.math.RoundingMode
import javax.inject.Inject

@SuppressLint("Registered")
class WasteControlTestFragment : BaseFragment(), AdapterView.OnItemSelectedListener, HasSupportFragmentInjector {

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>
    lateinit var progressDialogInner: ProgressDialog

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return childFragmentInjector
    }

    @Inject
    lateinit var viewModel: WasteControlTestFragmentViewModel

    companion object {
        const val IMG_URL_JSON = "listImgUrlJSON"
        fun newIntent(listImgUrlJSON: String?): Intent? {
            val intent = Intent()
            intent.putExtra(IMG_URL_JSON, listImgUrlJSON)
            return intent
        }

        fun ifConected(): Boolean {
            return if (WasteControlTestFragment().hasInternet())
                true
            else {
                Toast.makeText(WasteControlTestFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

    fun newInstance(title: String, crop: String): WasteControlTestFragment {
        val args = Bundle()
        args.putString("Title", title)
        args.putString("crop", crop)

        val fragment = WasteControlTestFragment()
        fragment.arguments = args
        return fragment
    }

    val listCrop = ArrayList<WasteControlTestFragmentViewModel.CropStageModelItem>()
    val listaUnits = ArrayList<WasteControlTestFragmentViewModel.UnitItem>()
    var crop_id: Int = 0
    var unit_id: Int? = null
    var sample: Int = 0
    var waste: Int = 0
    var national: Int = 0
    var natural_waste: Int = 0
    var operative_waste: Int = 0
    var export: Int = 0
    var signStr: String? = null
    var cropObj: WasteControlItemModel? = null

    lateinit var spinnerCrops: Spinner
    lateinit var spinnerUnit: Spinner

    lateinit var sampleLabel: EditText
    lateinit var exportTotalLabel: EditText
    lateinit var nationalTotalLabel: EditText
    lateinit var wasteTotalLabel: TextView
    lateinit var naturalTotalLabel: EditText
    lateinit var operativeTotalLabel: EditText

    lateinit var exportPercentLabel: TextView
    lateinit var nationalPercentLabel: TextView
    lateinit var wastePercentLabel: TextView
    lateinit var naturalPercentLabel: TextView
    lateinit var operativePercentLabel: TextView

    lateinit var buttonInit: Button
    lateinit var buttonSave: Button
    lateinit var layoutDetail: LinearLayout
    lateinit var layoutButton: LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_waste_control_test, container, false)
        val cropString: String? = arguments?.getString("crop")

        cropObj = if (!cropString.isNullOrEmpty())
            Gson().fromJson(cropString, WasteControlItemModel::class.java)
        else
            null

        addSubscribers()
        setDialogLoadingInner()
        setViewElements(view)
        setListener()
        getCatalogos()

        // Si viene con crop, muestra el detalle del mismo
        if (isDetail()) {
            Log.d("cropObj", Gson().toJson(cropObj))
            setElementsDetail()
        }

        return view
    }

    fun isDetail(): Boolean {
        return cropObj != null
    }

    fun setElementsDetail() {
        layoutButton.visibility = View.GONE

        crop_id = cropObj!!.id!!
        sampleLabel.setText(cropObj!!.sample.toString())
        layoutDetail.visibility = View.VISIBLE
        exportTotalLabel.setText(cropObj!!.export.toString())
        nationalTotalLabel.setText(cropObj!!.national.toString())
        wasteTotalLabel.text = cropObj!!.waste.toString()

        val natWasteStr = if (cropObj?.natural_waste == null) "0" else cropObj?.natural_waste.toString()
        naturalTotalLabel.setText(natWasteStr)

        val opeWasteStr = if (cropObj?.operation_waste == null) "0" else cropObj?.operation_waste.toString()
        operativeTotalLabel.setText(opeWasteStr)

        sampleLabel.isEnable(false)
        exportTotalLabel.isEnable(false)
        nationalTotalLabel.isEnable(false)
        naturalTotalLabel.isEnable(false)
        operativeTotalLabel.isEnable(false)
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_units().subscribe(this::updateList_units).addTo(subscriptions)
        viewModel.onSuccessRequest_wasteControl().subscribe(this::returnToMain).addTo(subscriptions)
    }

    fun setViewElements(view: View) {
        spinnerCrops = view.findViewById(R.id.spinner_crop_test)
        spinnerUnit = view.findViewById(R.id.spinner_unit_test)
        sampleLabel = view.findViewById(R.id.et_sample_test)

        exportTotalLabel = view.findViewById(R.id.et_export_total)
        nationalTotalLabel = view.findViewById(R.id.et_national_total)
        wasteTotalLabel = view.findViewById(R.id.tv_waste_total)
        naturalTotalLabel = view.findViewById(R.id.et_natural_total)
        operativeTotalLabel = view.findViewById(R.id.et_operative_total)

        exportPercentLabel = view.findViewById(R.id.tv_export_percentage)
        nationalPercentLabel = view.findViewById(R.id.tv_national_percentage)
        wastePercentLabel = view.findViewById(R.id.tv_waste_percentage)
        naturalPercentLabel = view.findViewById(R.id.tv_natural_percentage)
        operativePercentLabel = view.findViewById(R.id.tv_operative_percentage)

        spinnerCrops.onItemSelectedListener = this
        spinnerUnit.onItemSelectedListener = this
        sampleLabel.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    sample = if (p0.isNotBlank()) {
                        Integer.parseInt(sampleLabel.text.toString())
                    } else {
                        0
                    }

                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        buttonInit = view.findViewById(R.id.btn_init_test)
        buttonSave = view.findViewById(R.id.btn_save_test)

        layoutDetail = view.findViewById(R.id.layout_detail)
        layoutButton = view.findViewById(R.id.layout_button)

        buttonInit.setOnClickListener {
            when {
                crop_id == 0 -> {
                    Toast.makeText(context, "Debe selecionar el cultivo", Toast.LENGTH_SHORT).show()
                }
                sample == 0 -> {
                    Toast.makeText(context, "La muestra debe ser mayor a 0", Toast.LENGTH_SHORT).show()
                }
                unit_id == null -> {
                    Toast.makeText(context, "La unidad de medida es necesaria", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    buttonInit.visibility = View.GONE
                    buttonSave.visibility = View.VISIBLE
                    layoutDetail.visibility = View.VISIBLE
                    spinnerCrops.isEnable(false)
                    sampleLabel.isEnable(false)
                    spinnerUnit.isEnable(false)

                    exportTotalLabel.isEnable(true)
                    nationalTotalLabel.isEnable(true)

                    wasteCalc()
                }
            }
        }

        buttonSave.setOnClickListener {
            if (waste > 0) {
                if (waste == (natural_waste + operative_waste)) {
                    getSignature()
                } else {
                    sumDespMessage()
                }
            } else {
                getSignature()
            }
        }
    }

    fun sumDespMessage() {
        if (!isDetail())
            Toast.makeText(context, "La suma del desperdicio natural y operativo debe ser igual al desperdicio total", Toast.LENGTH_SHORT).show()
    }

    fun getSignature() {
        val dialog = SignDialog().newInstance()
        dialog.setTargetFragment(this, 100)
        dialog.show(fragmentManager!!, SignDialog.TAG)
    }

    fun prepareData(sign: String) {
        var wasteControlObj = WasteControlItemModel(0, 0, crop_id, unit_id, sample, waste, national, natural_waste, operative_waste, export, sign)

        viewModel.postFinishedWasteControl(wasteControlObj)
        Log.d("wasteControlObj", Gson().toJson(wasteControlObj))
    }

    fun wasteCalc() {
        waste = if (export >= 0 && national >= 0) {
            (sample - (export + national))
        } else {
            0
        }
        wasteTotalLabel.text = waste.toString()
        val percent = ((waste * 100.00) / sample)
        val percentStr = percent.toBigDecimal().setScale(2, RoundingMode.CEILING).toString() + "%"
        wastePercentLabel.text = percentStr

        if (waste > 0) {
            naturalTotalLabel.isEnable(true)
            operativeTotalLabel.isEnable(true)
        } else {
            naturalTotalLabel.isEnable(false)
            operativeTotalLabel.isEnable(false)

            natural_waste = 0
            operative_waste = 0

            naturalTotalLabel.setText("0")
            naturalPercentLabel.text = "0.00%"

            operativeTotalLabel.setText("0")
            operativePercentLabel.text = "0.00%"
        }
    }

    fun validateExpo(type: String): Boolean {
        return if ((export + national) <= sample) {
            true
        } else {
            Toast.makeText(context, "La suma de exportación y nacional no debe ser superior a la muestra", Toast.LENGTH_SHORT).show()
            when (type) {
                "export" -> {
                    export = 0
                    exportTotalLabel.setText("")
                    exportPercentLabel.setText("0.00%")
                }
                "national" -> {
                    national = 0
                    nationalTotalLabel.setText("")
                    nationalPercentLabel.setText("0.00%")
                }
            }
            false
        }
    }

    fun validateWaste(type: String): Boolean {
        return if ((natural_waste + operative_waste) <= waste)
            true
        else {
            sumDespMessage()
            when (type) {
                "natural" -> {
                    natural_waste = 0
                    naturalTotalLabel.setText("")
                    naturalPercentLabel.text = "0.00%"
                }
                "operative" -> {
                    operative_waste = 0
                    operativeTotalLabel.setText("")
                    operativePercentLabel.text = "0.00%"
                }
            }
            false
        }
    }

    fun setListener() {
        exportTotalLabel.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                        Log.d("p0", p0.toString())
                        if (p0 != null) {
                            if (p0.isNotBlank()) {
                                export = Integer.parseInt(p0.toString())
                                if (validateExpo("export")) {
                                    val percent = ((export * 100.00) / sample)
                                    val percentStr = percent.toBigDecimal().setScale(2, RoundingMode.CEILING).toString() + "%"
                                    exportPercentLabel.text = percentStr
                                    wasteCalc()
                                }
                            }
                        }
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        if (p0 != null) {
                            exportPercentLabel.text = "0.00%"
                        }
                    }
                })

        nationalTotalLabel.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                        if (p0 != null) {
                            if (p0.isNotBlank()) {
                                national = Integer.parseInt(p0.toString())
                                if (validateExpo("national")) {
                                    val percent = ((national * 100.00) / sample)
                                    val percentStr = percent.toBigDecimal().setScale(2, RoundingMode.CEILING).toString() + "%"
                                    nationalPercentLabel.text = percentStr
                                    wasteCalc()
                                }
                            }
                        }
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        if (p0 != null) {
                            nationalPercentLabel.text = "0.00%"
                        }
                    }
                })

        naturalTotalLabel.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                        if (p0 != null) {
                            if (p0.isNotBlank()) {
                                natural_waste = Integer.parseInt(p0.toString())
                                if (validateWaste("natural")) {
                                    val percent = ((natural_waste * 100.00) / waste)
                                    if (!percent.isNaN()) {
                                        val percentStr = percent.toBigDecimal().setScale(2, RoundingMode.CEILING).toString() + "%"
                                        naturalPercentLabel.text = percentStr
                                    } else {
                                        naturalPercentLabel.text = "0.00%"
                                    }
                                }
                            }
                        } else {
                            naturalPercentLabel.text = "0.00%"
                        }
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        if (p0 != null) {
                            naturalPercentLabel.text = "0.00%"
                        }
                    }
                })

        operativeTotalLabel.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                        if (p0 != null) {
                            if (p0.isNotBlank()) {
                                operative_waste = Integer.parseInt(p0.toString())
                                if (validateWaste("operative")) {
                                    val percent = ((operative_waste * 100.00) / waste)
                                    if (!percent.isNaN()) {
                                        val percentStr = percent.toBigDecimal().setScale(2, RoundingMode.CEILING).toString() + "%"
                                        operativePercentLabel.text = percentStr
                                    } else {
                                        operativePercentLabel.text = "0.00%"
                                    }
                                }
                            }
                        } else {
                            operativePercentLabel.text = "0.00%"
                        }
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        if (p0 != null) {
                            operativePercentLabel.text = "0.00%"
                        }
                    }
                })
    }

    fun getCatalogos() {
        showProgressDialogInner(true)
        listCrop.addAll(viewModel.getCropList())
        spinnerCrops.adapter = ArrayAdapter<WasteControlTestFragmentViewModel.CropStageModelItem>(context!!, R.layout.spinner_item_textview, listCrop)
        if (isDetail()) {
            var pos = -1
            for ((index, item) in listCrop.withIndex()) {
                if (item.getId() == cropObj!!.crop_id) {
                    pos = index
                }
            }
            spinnerCrops.setSelection(pos, true)
            spinnerCrops.isEnable(false)
        }

        viewModel.getUnits()
    }

    fun setDialogLoadingInner() {
        progressDialogInner = ProgressDialog(context)
        progressDialogInner.setTitle(getString(R.string.app_name))
        progressDialogInner.setMessage(getString(R.string.server_loading))
        progressDialogInner.setCancelable(false)
    }

    private fun updateList_units(list: List<UnitsViewModel>) {
        listaUnits.clear()

        var firtItem: WasteControlTestFragmentViewModel.UnitItem? = null

        for (item in list) {
            var obj = WasteControlTestFragmentViewModel.UnitItem(item.id, item.business_unit_id, item.name, item.business_unit_names)
            if(item.name == "Piezas") firtItem = obj
            listaUnits.add(obj)
        }

        if(firtItem != null) {
            listaUnits.remove(firtItem)
            listaUnits.add(0, firtItem)
        }

        spinnerUnit.adapter = ArrayAdapter<WasteControlTestFragmentViewModel.UnitItem>(context!!, R.layout.spinner_item_textview, listaUnits)



        if (isDetail()) {
            var pos = -1
            for ((index, item) in listaUnits.withIndex()) {
                if (item.getId() == cropObj!!.unit_id) {
                    pos = index
                }
            }
            spinnerUnit.setSelection(pos, true)
            spinnerUnit.isEnable(false)
        }
        showProgressDialogInner(false)
    }

    fun showProgressDialogInner(show: Boolean) {
        if (show) progressDialogInner.show() else progressDialogInner.dismiss()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        println("notSelected")
    }

    override fun onItemSelected(adapter: AdapterView<*>, view: View, position: Int, id: Long) {
        when (adapter.id) {
            R.id.spinner_crop_test -> {
                crop_id = if (position != 0) {
                    //  val nombre = adapter.getItemAtPosition(position).toString()
                    listCrop[position].getId()
                } else {
                    0
                }
            }
            R.id.spinner_unit_test -> {
                unit_id = listaUnits[position].getId()
            }
        }
    }

    fun returnToMain(wasteControlItemModel: WasteControlItemModel) {
        val intent = Intent()
        intent.putExtra("wasteControlItemModel", Gson().toJson(wasteControlItemModel))
        activity!!.setResult(Activity.RESULT_OK, intent)
        activity!!.finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == 100) {
            val jsonList = data!!.getStringExtra("listImgUrlJSON")
            val listUrls: ArrayList<String> = Gson().fromJson(jsonList, object : TypeToken<ArrayList<String?>?>() {}.getType())
            if (listUrls.size == 1) {
                prepareData(listUrls[0])
            }
        }
    }

}
