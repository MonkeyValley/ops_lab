package com.amplemind.vivasmart.features.operations.fragment


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.mipe.MipeMonitoringLotsActivity
import com.amplemind.vivasmart.features.operations.OperationsActivitiesActivity
import com.amplemind.vivasmart.features.operations.viewModel.MenuOperationsActivitiesViewModel
import com.amplemind.vivasmart.features.packaging_quality.RecibaActivity
import com.amplemind.vivasmart.features.phenology.adapters.ActivitiesPhenologyAdapter
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModel
import kotlinx.android.synthetic.main.content_payroll_activities_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

@SuppressLint("Registered")
class MenuActivitiesOperationsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelMenu: MenuOperationsActivitiesViewModel

    @Inject
    lateinit var prefer: UserAppPreferences

    private var section: String? = null

    fun newInstance(title: String): MenuActivitiesOperationsFragment {
        val args = Bundle()
        args.putString("Title", title)

        val fragment = MenuActivitiesOperationsFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_payroll_activities_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        lbl_track_title.text = "/ Operaciones /"
        viewModelMenu.setupData(arguments!!)
        viewModelMenu.getToolbarTitle().subscribe {
            section = it
            (activity as OperationsActivitiesActivity).supportActionBar!!.title = it
            viewModelMenu.loadMenuActivities().addTo(subscriptions)
            viewModelMenu.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)

        }.addTo(subscriptions)
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_menu_activities_payroll.adapter as ActivitiesPhenologyAdapter).addElements(activities)
        (rv_menu_activities_payroll.adapter as ActivitiesPhenologyAdapter).onClickItem().subscribe(this::clickActivityPhenology).addTo(subscriptions)
    }

    private fun clickActivityPhenology(item: ItemActivitiesPayrollViewModel) {
        if (item.id == "123123ED") {
            startActivity(Intent(context!!, RecibaActivity::class.java))
        } else if(item.id == "123123EF"){
            startActivity(Intent(context!!, MipeMonitoringLotsActivity::class.java).apply {
                putExtra(MipeMonitoringLotsActivity.PARAM_TYPE, "2")
                putExtra(MipeMonitoringLotsActivity.PARAM_TITTLE, "Lotes")
            })
        } else {
            Toast.makeText(context, "Próximamente...", Toast.LENGTH_SHORT).show()
        }
    }

    private fun callNewFragment(flow: String) {
        //TODO: saveServerResponse flow
        prefer.flow = flow
        if (section == "Calidad") {
//            (activity as AppCompatActivity)
//                    .supportFragmentManager
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
//                    .add(R.id.container_payroll, MenuaActivitiesQualityProductionFragment().newInstance("Producción"))
//                    .addToBackStack(MenuaActivitiesQualityProductionFragment.TAG)
//                    .commit()
        } else {
//            (activity as AppCompatActivity)
//                    .supportFragmentManager
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
//                    .add(R.id.container_payroll, getFragment(flow))
//                    .addToBackStack(MenuActivitiesProductionFragment.TAG)
//                    .commit()
        }
    }

    private fun initRecycler() {
        rv_menu_activities_payroll.hasFixedSize()
        rv_menu_activities_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_activities_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_activities_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_activities_payroll.adapter = ActivitiesPhenologyAdapter()
    }

    companion object {
        val TAG = MenuActivitiesOperationsFragment::class.java.simpleName
    }

}