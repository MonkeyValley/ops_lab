package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.model.UnitModel

class SpinnerUnitsViewModel(private val model: UnitModel) {

    val id = model.id

    override fun toString(): String {
        return model.name
    }

}