package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel

class ItemActivitiesPayrollViewModel(item: PayrollActivitiesModel) {

    val name = item.name
    val id = item.id
    val drawable = item.drawable
    val active = item.active

}
