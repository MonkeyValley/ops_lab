package com.amplemind.vivasmart.vo_features.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes

abstract class LiveFragmentPagerAdapter<T: Any>(fragmentManager: FragmentManager) : CustomFragmentPagerAdapter(fragmentManager) {

    companion object {
        private val TAG = LiveFragmentPagerAdapter::class.java.simpleName
    }

    private var mItems = listOf<T>()

    private lateinit var mViewPager: ViewPager

    var currentPosition: Int
        set(value) {
            mViewPager.currentItem = value
        }
        get() = mViewPager.currentItem

    override fun getCount(): Int = mItems.size

    val currentItem: T
            get() = mItems[currentPosition]

    val currentFragment: Fragment
            get() = getFragment(currentPosition) ?: throw IllegalStateException("Fragment at position $currentPosition could not be retrieved")

    fun setItems(changes: Changes<T>) {
        mItems = changes.data
        notifyDataSetChanged()

        changes.forEachUpdate { type, position ->
            when (type) {
                Changes.Type.INSERT -> {
                    onFragmentInsert(position)
                }
                Changes.Type.UPDATE -> onFragmentUpdate(position)
                Changes.Type.DELETE -> {
                    onFragmentDelete(position)
                    deleteFragment(position)
                }
            }
        }

    }

    fun setWithViewPager(viewPager: ViewPager) {
        mViewPager = viewPager
        viewPager.adapter = this
    }

    fun setCurrentItem(position: Int) {
        mViewPager.currentItem = position
    }

    open fun onFragmentInsert(position: Int) {}
    open fun onFragmentUpdate(position: Int) {}
    open fun onFragmentDelete(position: Int) {}

    operator fun get(position: Int): T = mItems[position]
}