package com.amplemind.vivasmart.core.extensions

import android.graphics.Color
import android.graphics.PorterDuff
import android.text.Editable
import android.text.TextWatcher
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import android.widget.EditText
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable


fun EditText.onChange(cb: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            cb(s.toString())
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

fun AutoCompleteTextView.onItemSelected(cb: (String) -> Unit) {
    this.onItemClickListener = AdapterView.OnItemClickListener { adapter, p1, p2, p3 -> cb(adapter!!.getItemAtPosition(p2).toString()) }
}

fun EditText.searchViewPadding() {
    this.setPadding(25.toPx(), 0, 0, 0)
}