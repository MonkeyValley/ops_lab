package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm
import io.realm.FieldAttribute

class Version_3 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        addCatalogDownloadObjectModel(realm)
    }

    override fun drop(realm: DynamicRealm) {
        dropCatalogDownloadObjectModel(realm)
    }

    private fun addCatalogDownloadObjectModel(realm: DynamicRealm) {
        realm.schema.get("DownloadObjectModel")
                ?.addField("isCatalog", Boolean::class.java, FieldAttribute.INDEXED)
    }


    private fun dropCatalogDownloadObjectModel(realm: DynamicRealm) {
        realm.schema.get("DownloadObjectModel")
                ?.removeIndex("isCatalog")
                ?.removeField("isCatalog")
    }


}