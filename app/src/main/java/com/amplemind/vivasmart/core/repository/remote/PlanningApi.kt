package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.response.CropReportResponse
import com.amplemind.vivasmart.core.repository.response.PlanningResponse
import io.reactivex.Observable
import retrofit2.http.*

interface PlanningApi {

    @GET("/v2/planning/activity")
    @Headers("Content-Type: application/json")
    fun getPlanning(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "activity,days,planning",
            @Query("planning.activity_area_id") areaId: Int,
            @Query("planning_day.day") date: String,
            @QueryMap() placeId: Map<String, Int>,
            //@Query("planning.activity_category_id") activity_category_id: Int,
            @Query("__logic") logic: String = "AND"
    ) : Observable<PlanningResponse>

    @GET("/v2/carry_order/report/{lotId}/{date}")
    @Headers("Content-Type: application/json")
    fun getCropReport(
            @Header("Authorization") authentication: String,
            @Path("lotId") lotId: Int,
            @Path("date") date: String
    ) : Observable<CropReportResponse>



}