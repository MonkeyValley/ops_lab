package com.amplemind.vivasmart.features.interfaces

interface ICategoriesDetailItemViewModel {

    fun increaseIncidence()
    fun getId() : Int

}