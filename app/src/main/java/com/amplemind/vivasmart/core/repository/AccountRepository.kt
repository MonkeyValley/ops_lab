package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.LoginModel
import com.amplemind.vivasmart.core.repository.model.RecoverPasswordModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.LoginApiHolder
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.vo_core.repository.models.realm.UserRolModel
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class AccountRepository @Inject constructor(private val loginApiHolder: LoginApiHolder,
                                                 private val apiAuthorization:ApiAuthorization,
                                                 private val preferences: UserAppPreferences) {

    data class LoginRequest(val email: String, val password: String)
    data class RecoverPassRequest(val email: String)
    data class SetPasswordRequest(val token: String, val password: String)
    data class SetPasswordResponse(val msg: String, val token: String)
    data class ChangePasswordRequest(val actual_password: String, val new_password: String)
    data class ChangePasswordResponse(val msg: String, val token: String)
    data class GenericResponse(val msg: String)
    data class ValidatePasswordRequest(val password: String)
    data class UpdateAgreedTutorial(@SerializedName("tutorial") val tutorial : Boolean)
    data class Firebasetoken(@SerializedName("firebase_token") val firebasetoken: String)


    private val accountApi
        get() = loginApiHolder.api

    /**
     *
     * call retrofit doLogin()
     *
     * */
    fun validateDeepLinkToken(token: String): Observable<GenericResponse> {
        return accountApi.validateDeepLinkToken(token)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    /**
     * @param LoginRequest
     * @return LoginResponse
     *
     * call retrofit doLogin()
     * */
    fun doLogin(request: LoginRequest): Observable<LoginModel.LoginResponse> {
        return accountApi.doLogin(request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    /**
     * @param RecoverPassRequest
     * @return RecoverPasswordModel
     *
     * call retrofit doLogin()
     * */
    fun recoverPassword(request: RecoverPassRequest): Observable<RecoverPasswordModel.RecoverPasswordResponse> {
        return accountApi.recoverPassword(request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    /**
     * @param SetPasswordRequest
     * @return SetPasswordResponse
     *
     * call retrofit doLogin()
     * */
    fun setPassword(request: SetPasswordRequest): Observable<SetPasswordResponse> {
        return accountApi.setPassword(request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    /**
     * @param ValidatePasswordRequest
     * @return GenericResponse
     *
     * */
    fun validateCurrentPassword(request: ValidatePasswordRequest): Observable<GenericResponse> {
        return accountApi.validateCurrentPassword(apiAuthorization.getAuthToken(preferences.token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    /**
     * @param ValiRequest
     * @return ChangePasswordResponse
     *
     * */
    fun changePassword(request: ChangePasswordRequest): Observable<ChangePasswordResponse> {
        return accountApi.changePassword(apiAuthorization.getAuthToken(preferences.token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    /**
     *
     * @return UserModel
     *
     * */
    fun getUser(): Observable<UserModel> {
        return accountApi.getUserInfo(apiAuthorization.getAuthToken(preferences.token))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getUserRol(): Observable<UserRolModel> {
        return accountApi.getUserRol(apiAuthorization.getAuthToken(preferences.token))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun putFirebaseToken(firebasetoken: Firebasetoken, id:Int): Observable<UserModel> {
        return accountApi.putFirebaseToken(apiAuthorization.getAuthToken(preferences.token),firebasetoken,id)
    }

    fun saveTokenUser(token : String) {
        preferences.token = token
    }

    fun getStoredUser(): UserModel {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java)
    }

    fun saveUser(user : String) {
        preferences.userInfo = user
    }

    fun saveServerUrl(url: String) {
        preferences.serverUrl = url
    }

    fun getServerUrl(): String {
        return preferences.serverUrl
    }

    fun saveCustomServerUrl(url: String) {
        preferences.customServerUrl = url
    }

    fun getCustomServerUrl(): String {
        return preferences.customServerUrl
    }

    fun saveServerUrlIsPreset(isPreset: Boolean) {
        preferences.serverUrlIsPreset = isPreset
    }

    fun getServerUrlIsPreset(): Boolean {
        return preferences.serverUrlIsPreset
    }

}
