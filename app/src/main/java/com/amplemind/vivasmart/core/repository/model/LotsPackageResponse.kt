package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class LotsPackageResponse(@SerializedName("data") val lots: List<LotsPackageResultResponse>)

@Entity()
data class LotsPackageResultResponse(
        @SerializedName("id") @ColumnInfo(name = "lot_id") @PrimaryKey(autoGenerate = false) val id : Int,
        @SerializedName("name") val name : String,
        @SerializedName("stage_id") val stage_id : Int)
