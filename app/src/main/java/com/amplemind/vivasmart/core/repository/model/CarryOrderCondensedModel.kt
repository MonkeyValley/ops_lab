package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import androidx.databinding.adapters.Converters
import com.google.gson.annotations.SerializedName

@Entity
class CarryOrderCondensedModel(@SerializedName("folio") val folio: String,
                               @SerializedName("id") @PrimaryKey val id: Int,
                               @SerializedName("lot_name") val lotName: String,
                               @TypeConverters(Converters::class)  @SerializedName("table") val tables: List<Int>,
                               var showInPackage : Boolean = false)