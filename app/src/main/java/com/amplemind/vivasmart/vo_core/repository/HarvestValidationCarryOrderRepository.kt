package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.remote.PackagingQualityApi
import com.amplemind.vivasmart.core.repository.remote.models.HarvestValidationCarryOrderModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HarvestValidationCarryOrderRepository @Inject constructor(
        private val mPreferences: UserAppPreferences,
        private val mPackagingQualityApi: PackagingQualityApi
) : Repository() {

    override fun cleanUp() {
    }

    fun loadHarvestValidationCarryOrders(date: String): Observable<List<HarvestValidationCarryOrderModel>> =
        mPackagingQualityApi.getHarvestValidationCarryOrders(
                mPreferences.authorizationToken,
                businessUnitId = mPreferences.user.businessUnitId,
                date = date
        )
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

}