package com.amplemind.vivasmart.features.packaging_quality.clean_report

import android.app.Dialog
import android.os.Bundle
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.features.production_quality.SignatureDialogFragment
import kotlinx.android.synthetic.main.content_add_total_dialog.*

/**
 * Created by
 *          amplemind on 7/13/18.
 */
class AddProblemDialogFragment : BaseDialogFragment() {

    companion object {
        val TAG = AddProblemDialogFragment::class.java.simpleName

        fun newInstance(type: Int, position: Int, name: String, currentTotal : Int, maxSize : Int): AddProblemDialogFragment {
            val dialog = AddProblemDialogFragment()

            val args = Bundle()
            args.putInt("type", type)
            args.putInt("position", position)
            args.putInt("total", currentTotal)
            args.putInt("maxSize", maxSize)
            args.putString("name", name)
            dialog.arguments = args

            return dialog
        }
    }

    private var add_problem : TextInputEditText? = null
    private var input_layout : TextInputLayout? = null

    private var eventListener : OnClickAddTotal? = null

    private fun getTotal() : Int {
        return arguments?.getInt("total", 0) ?: 0
    }

    private fun getMaxSize() : Int {
        return arguments?.getInt("maxSize", 0) ?: 0
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_add_total_dialog, null, false)
        builder.setView(mView)

        isCancelable = false

        add_problem = mView.findViewById(R.id.ed_add_problems)
        input_layout = mView.findViewById(R.id.ti_layout)
        mView.findViewById<TextView>(R.id.tv_title).text = arguments!!.getString("name")

        builder.setPositiveButton("Aceptar", null)
        builder.setNegativeButton("Cancelar") { _, i ->
            hideKeyboard(context!!)
            dismiss()
        }

        return builder.create()
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (add_problem!!.text.toString().isNotEmpty()){
                if ((add_problem!!.text.toString().toInt() + getTotal()) > getMaxSize()){
                    add_problem!!.setBackgroundResource(R.drawable.rectangule_dialog_error)
                    return@setOnClickListener
                }else{
                    add_problem!!.setBackgroundResource(R.drawable.rectangule_dialog)
                    eventListener!!.addTotal(add_problem!!.text.toString().toInt(), arguments!!.getInt("position"),
                            arguments!!.getInt("type"))
                }
            }else{
                input_layout!!.error = "Campo vacio"
            }
            dismiss()
        }
    }

    fun addEvent(event : OnClickAddTotal){
        this.eventListener = event
    }

    interface OnClickAddTotal{
        fun addTotal(problems : Int, position : Int, type: Int)
    }


}