package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import com.amplemind.vivasmart.core.utils.STAGE_TYPE_ID
import com.amplemind.vivasmart.vo_core.repository.ActivityChooserRepository
import javax.inject.Inject

class PayrollActivitiesViewModel @Inject constructor(
        private val repository: ActivityChooserRepository
) {

    var productionStageName: String = ""
    var stageUuid: String = ""
    var cropId: Long = -1
    var stageTypeId: Int = 0

    fun setStageType() {
        stageTypeId = when(productionStageName){
            "Cosecha" -> 1
            "Mantenimiento" -> 2
            "Fertiriego" -> 3
            "MIPE" -> 4
            "Labores Culturales" -> 5
            "Inocuidad" -> 6
            else -> 0
        }
        STAGE_TYPE_ID = stageTypeId
    }

    fun cleanUp() {
        repository.cleanUp()
    }

    fun loadStage() =
            repository.loadStage(stageUuid)

    fun loadActivities() =
            repository.loadActivities(cropId, stageTypeId)


}
