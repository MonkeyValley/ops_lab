package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.QualitySearchCodeRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class QualitySearchCodeViewModel @Inject constructor(private val repository : QualitySearchCodeRepository,
                                                     private val apiErrors: HandleApiErrors) {

    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val codeFound = BehaviorSubject.create<Int>()

    fun searchCode(code: String): Disposable {
        return repository.searchCarryOrder(code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    if (it.data.isNotEmpty()) {
                        codeFound.onNext(it.data.first().id)
                    } else {
                        requestFail.onNext("Lo sentimos, el código ingresado no se encuentra en la lista")
                    }
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onCodeFound(): BehaviorSubject<Int> {
        return codeFound
    }

}
