package com.amplemind.vivasmart.features.fertiriego.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.util.SparseArray
import com.amplemind.vivasmart.features.fertiriego.fragment.HydroponicReportFragment
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService

class FertirriegoHydroponicReportTab (fm: FragmentManager, private val date: String, private val lotId: Int, private val mode: String) : FragmentPagerAdapter(fm) {

    private val registeredFragments = SparseArray<Fragment>()

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                val fragment = HydroponicReportFragment().newInstance(date, lotId, "1", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            1 -> {
                val fragment = HydroponicReportFragment().newInstance(date, lotId, "2", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            2 -> {
                val fragment = HydroponicReportFragment().newInstance(date, lotId, "3", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            3 -> {
                val fragment = HydroponicReportFragment().newInstance(date, lotId, "4", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            4 -> {
                val fragment = HydroponicReportFragment().newInstance(date, lotId, "5", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
            else -> {
                val fragment = HydroponicReportFragment().newInstance(date, lotId, "6", mode)
                registeredFragments.put(position, fragment)
                return fragment
            }
        }
    }

    override fun getCount(): Int {
        return FertirriegoService().getTableCountPerLot(lotId, "Soil")
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Tabla 1"
            1 -> "Tabla 2"
            2 -> "Tabla 3"
            3 -> "Tabla 4"
            4 -> "Tabla 5"
            else -> "Tabla 6"
        }
    }

    fun getRegisteredFragment(position: Int): Fragment? {
        return registeredFragments[position]
    }

}