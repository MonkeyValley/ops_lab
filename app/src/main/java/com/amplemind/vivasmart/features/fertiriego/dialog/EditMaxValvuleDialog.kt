package com.amplemind.vivasmart.features.fertiriego.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.features.fertiriego.FertirriegoHydroponicMonitoringActivity
import com.amplemind.vivasmart.features.fertiriego.viewModel.FertirriegoHydroponicMonitoringViewModel
import javax.inject.Inject


class EditMaxValvuleDialog : BaseDialogFragment() {

    @Inject
    lateinit var viewModel: FertirriegoHydroponicMonitoringViewModel

    companion object {
        val TAG = BaseDialogFragment::class.java.simpleName
        var lotIdPulse = 0
        var max = 28
        var seletedDate = ""
    }

    fun newInstance(lotId: Int, maxPulse: Int, date: String): EditMaxValvuleDialog {
        lotIdPulse = lotId
        max = maxPulse
        seletedDate = date
        return EditMaxValvuleDialog()
    }

    private lateinit var mview: View
    var btnMinus: Button? = null
    var btnPlus: Button? = null
    var etMax: TextView? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mview = LayoutInflater.from(context).inflate(R.layout.fragment_edit_max_valvule_dialog, null, false)

        setUpUi(mview)

        viewModel.date = seletedDate

        builder.setCancelable(false)
        builder.setView(mview)
        builder.setPositiveButton("ACEPTAR", null)
        builder.setNegativeButton("CANCELAR") { _, _ -> dismiss() }
        val create = builder.create()
        create.setOnShowListener {
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        saveMaxPulse()
                    }
        }

        return create
    }

    fun setUpUi(view: View) {
        etMax = view.findViewById(R.id.et_max_pulse)
        etMax!!.text = max.toString()
        btnMinus = view.findViewById(R.id.btn_minus)
        btnMinus!!.setOnClickListener { view ->
            if (etMax!!.text.toString().toInt() > max) {
                val result = (etMax!!.text.toString().toInt() - 1)
                etMax!!.text = result.toString()
            }
        }
        btnPlus = view.findViewById(R.id.btn_plus)
        btnPlus!!.setOnClickListener { view ->
            if (etMax!!.text.toString().toInt() < 70) {
                val result = (etMax!!.text.toString().toInt() + 1)
                etMax!!.text = result.toString()
            }
        }

    }

    fun saveMaxPulse(){
        val builder = android.app.AlertDialog.Builder(context)
        builder.setTitle("Fertirriego")
        builder.setMessage("¿Desea modifica el maximo de pulso a "+ etMax!!.text.toString() + " para este lote?")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            viewModel.updateMaxPulse(lotIdPulse, etMax!!.text.toString().toInt())
            (activity as FertirriegoHydroponicMonitoringActivity?)!!.getData()
            dialog.dismiss()
            finishMaxPulse()
        }
        builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
        builder.show()
    }

    fun finishMaxPulse(){
        val builder = android.app.AlertDialog.Builder(context)
        builder.setTitle("Fertirriego")
        builder.setMessage("Maximo de pulsos actualizados a "+ etMax!!.text.toString())
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            dialog.dismiss()
            dismiss()
        }
        builder.show()
    }

}
