package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.model.CountBoxTotals
import com.amplemind.vivasmart.core.repository.model.SyncRosterDeleteCollaborators
import com.amplemind.vivasmart.core.repository.model.SyncRosterPackageRequest
import com.amplemind.vivasmart.core.repository.model.SyncRosterPackageResponse
import com.amplemind.vivasmart.core.repository.request.FinishAllCollaboratorWithBox
import com.amplemind.vivasmart.core.repository.request.FinishCollaboratorSkipSignatureRequest
import com.amplemind.vivasmart.core.repository.request.PauseTimerCollaboratorRequest
import com.amplemind.vivasmart.core.repository.request.RestarTimerCollaboratorLines
import com.amplemind.vivasmart.core.repository.response.CollaboratorFinishCostResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import io.reactivex.Observable
import retrofit2.http.*

interface TimerLinesApi {

    @GET("/v2/packingline_paysheet/collaborator")
    @Headers("Content-Type: application/json")
    fun getCollaboratorsByLine(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "collaborator,packing",
            @Query("packingline_id") packingline_id: Int,
            @Query("is_done") is_done: Boolean = false,
            @Query("__logic") logic: String = "AND"
    ): Observable<CollaboratorsInLinesResult>

    @PUT("/v2/packingline/collaborator")
    @Headers("Content-Type: application/json")
    fun pauseTimerCollaborators(
            @Header("Authorization") authentication: String,
            @Body body: PauseTimerCollaboratorRequest
    ): Observable<Any>

    @PUT("/v2/packingline_paysheet/collaborator/time_restart")
    @Headers("Content-Type: application/json")
    fun restartTimerCollaborators(
            @Header("Authorization") authentication: String,
            @Body body: RestarTimerCollaboratorLines
    ): Observable<Any>

    @DELETE("/v2/packingline_paysheet/collaborator/{collaborator_id}")
    @Headers("Content-Type: application/json")
    fun deleteCollaborator(
            @Header("Authorization") authentication: String,
            @Path("collaborator_id") collaborator_id : Int
    ): Observable<Any>

    @PUT("/v2/packingline/collaborator")
    @Headers("Content-Type: application/json")
    fun finishCollaboratorSkipSignature(
            @Header("Authorization") authentication: String,
            @Body body: FinishCollaboratorSkipSignatureRequest
    ): Observable<Any>

    @PUT("/v2/packingline_paysheet/collaborator/totals/{id_collaborator}")
    @Headers("Content-Type: application/json")
    fun getTotalsFinishCollaborator(
            @Header("Authorization") authentication: String,
            @Path("id_collaborator") id_collaborator : Int,
            @Body body: CountBoxTotals
    ): Observable<CollaboratorFinishCostResponse>

    @PUT("/v2/packingline_paysheet/collaborator/out")
    @Headers("Content-Type: application/json")
    fun finishCollaboratorWithBox(
            @Header("Authorization") authentication: String,
            @Body body: FinishAllCollaboratorWithBox
    ): Observable<Any>

    @POST("/v2/offline/sync/packingline")
    @Headers("Content-Type: application/json")
    fun syncCollaboratorsPackage(
            @Header("Authorization") authentication: String,
            @Body body: SyncRosterPackageRequest
    ): Observable<SyncRosterPackageResponse>

    @POST("/v2/packingline_paysheet/collaborator/delete")
    @Headers("Content-Type: application/json")
    fun syncDeleteCollaboratorsPackage(
            @Header("Authorization") authentication: String,
            @Body body: SyncRosterDeleteCollaborators
    ): Observable<Any>

}
