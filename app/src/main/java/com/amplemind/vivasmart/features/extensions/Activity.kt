package com.amplemind.vivasmart.features.extensions

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.amplemind.vivasmart.R

fun Activity.showErrorDialog(error: Throwable) {
    AlertDialog.Builder(this)
            .setTitle(R.string.error_dialog_title)
            .setMessage(error.localizedMessage)
            .setPositiveButton(R.string.accept) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
}