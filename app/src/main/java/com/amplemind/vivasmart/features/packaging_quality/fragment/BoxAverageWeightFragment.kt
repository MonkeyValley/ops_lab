package com.amplemind.vivasmart.features.packaging_quality.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.features.packaging_quality.adapters.RecibaAverageWeightPagerAdapter
import com.amplemind.vivasmart.features.packaging_quality.viewModel.BoxAverageWeightViewModel
import javax.inject.Inject

class BoxAverageWeightFragment : BaseFragment(), RecibaAverageWeightPagerAdapter.AverageWeightFragment {

    companion object {

        private const val BOX_WEIGHT = 1.5

        @JvmStatic
        fun newInstance(cropId: Long): BoxAverageWeightFragment{
            val fragment = BoxAverageWeightFragment()
            val args = Bundle()
            args.putLong("CROP_ID", cropId)
            fragment.arguments = args

            return fragment
        }
    }

    @Inject
    lateinit var mViewModel: BoxAverageWeightViewModel

    private val mBoxes = mutableListOf<EditText>()
    private lateinit var mAverage: TextView

    private var mAverageValue = 0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_box_average_weight, container, false)

        mBoxes.add(view.findViewById(R.id.box1))
        mBoxes.add(view.findViewById(R.id.box2))
        mBoxes.add(view.findViewById(R.id.box3))
        mBoxes.add(view.findViewById(R.id.box4))
        mBoxes.add(view.findViewById(R.id.box5))
        mBoxes.add(view.findViewById(R.id.box6))
        mBoxes.add(view.findViewById(R.id.box7))
        mBoxes.add(view.findViewById(R.id.box8))
        mBoxes.add(view.findViewById(R.id.box9))
        mBoxes.add(view.findViewById(R.id.box10))

        val textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                doAverageCalculations()
            }
        }

        mBoxes.forEach { box ->
            box.addTextChangedListener(textWatcher)
        }

        mAverage = view.findViewById(R.id.tv_avg_weight)

        doAverageCalculations()

        return view
    }

    private fun doAverageCalculations() {

        val sum = mBoxes.sumByDouble { box ->
            val value: Double = box.text.toString().toDoubleOrNull() ?: 0.0
            return@sumByDouble if (value > 0.0) (value - BOX_WEIGHT) else 0.0
        }

        var x = 0.0
        for (etBox in mBoxes) {
            if (etBox.text.toString() != "") {
                x++
            }
        }

        mAverageValue = sum / x

        mAverageValue = if (mAverageValue.isNaN()) 0.0 else mAverageValue

        if (mAverageValue != 0.0) {
            if (mViewModel.valitateAcerageBox(
                            mAverageValue,
                            arguments?.getLong("CROP_ID", 0L) ?: 0L)) {
                mAverage.setTextColor(resources.getColor(R.color.redAlert))
            } else {
                mAverage.setTextColor(resources.getColor(R.color.dark_gray))
            }

            mAverage.text = "%.1f Kg".format(mAverageValue)
        } else {
            mAverage.text = "%.1f Kg".format(mAverageValue)
        }
    }

    override fun calculateAverage(): Double {
        return mAverageValue
    }



}
