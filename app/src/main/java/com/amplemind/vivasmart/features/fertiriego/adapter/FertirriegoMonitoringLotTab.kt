package com.amplemind.vivasmart.features.fertiriego.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.util.SparseArray
import com.amplemind.vivasmart.features.fertiriego.fragment.MonitoringFragment
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService


class FertirriegoMonitoringLotTab (fm: FragmentManager, private val date: String, private val lotId: Int) : FragmentPagerAdapter(fm) {

    private val registeredFragments = SparseArray<Fragment>()

    override fun getItem(position: Int): Fragment {
         when (position) {
            0 -> {
                val fragment = MonitoringFragment().newInstance(date, lotId, "T1")
                registeredFragments.put(position, fragment)
                return fragment
            }
            1 -> {
                val fragment = MonitoringFragment().newInstance(date, lotId, "T2")
                registeredFragments.put(position, fragment)
                return fragment
            }
            2 -> {
                val fragment = MonitoringFragment().newInstance(date, lotId, "T3")
                registeredFragments.put(position, fragment)
                return fragment
            }
            else ->  {
                val fragment = MonitoringFragment().newInstance(date, lotId, "T4")
                registeredFragments.put(position, fragment)
                return fragment
            }
        }
    }

    override fun getCount(): Int {
        return FertirriegoService().getTableCountPerLot(lotId, "Soil")
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Tabla 1"
            1 -> "Tabla 2"
            2 -> "Tabla 3"
            else -> "Tabla 4"
        }
    }

    fun getRegisteredFragment(position: Int): Fragment? {
        return registeredFragments[position]
    }
}