package com.amplemind.vivasmart.features.fertiriego.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_TPYE
import com.amplemind.vivasmart.databinding.LotHydroponicFertirriegoItemBinding
import com.amplemind.vivasmart.features.fertiriego.viewModel.LotHydroponicFertirriegoItemViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.TablesPHCEPerDay
import com.amplemind.vivasmart.vo_core.repository.models.realm.TablesPerDay
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValvesPerLotFertirriegoModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.realm.Realm
import java.util.*

class LotsInHydroponicFertirriegoAdapter : RecyclerView.Adapter<LotsInHydroponicFertirriegoAdapter.HydroponicFertirriegoViewHolder>() {
    private var list = mutableListOf<LotHydroponicFertirriegoItemViewModel>()

    var listener: Listener? = null

    private val clickSubject = PublishSubject.create<LotHydroponicFertirriegoItemViewModel>()

    private var mRecyclerView: RecyclerView? = null

    fun addElements(data: List<LotHydroponicFertirriegoItemViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HydroponicFertirriegoViewHolder {
        val binding = LotHydroponicFertirriegoItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HydroponicFertirriegoViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HydroponicFertirriegoViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem() = clickSubject

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mRecyclerView = null
    }

    inner class HydroponicFertirriegoViewHolder(private val binding: LotHydroponicFertirriegoItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: LotHydroponicFertirriegoItemViewModel

        fun bind(item: LotHydroponicFertirriegoItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            when (validateDayRvision(item.lot_id)) {
                1 -> binding.imageView2.setImageResource(R.drawable.ic_inv_circle_pending)
                2 -> binding.imageView2.setImageResource(R.drawable.ic_inv_circle_complete)
                else -> binding.imageView2.setImageResource(R.drawable.ic_inv_circle_incomplete)
            }

            binding.root.setOnClickListener {
                clickSubject.onNext(this.item)
            }
        }

    }

    interface Listener {
        fun onGetCounterValue( osition: Int): Observable<Int>?
    }

    fun validateDayRvision(lotId: Int): Int {
        Realm.getDefaultInstance().use {
            var validate = 0
            val calendar = Calendar.getInstance()
            val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
            val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
            val date = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

            if (FERTIRRIEGO_TPYE == "(pH - CE)") {

                val result = it
                        .where(TablesPHCEPerDay::class.java)
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("date", date)
                        .findFirst()

                val lot = it
                        .where(LotModel::class.java)
                        .equalTo("id", lotId)
                        .findFirst()

                var count = 0
                var t1 = 0
                var t2 = 0
                var t3 = 0
                var t4 = 0
                var t5 = 0
                var t6 = 0
                if(lot != null) {
                    if (lot.table1 != 0) {
                        count++
                        t1 = 3
                    }
                    if (lot.table2 != 0) {
                        count++
                        t2 = 3
                    }
                    if (lot.table3 != 0) {
                        count++
                        t3 = 3
                    }
                    if (lot.table4 != 0) {
                        count++
                        t4 = 3
                    }
                    if (lot.table5 != 0) {
                        count++
                        t5 = 3
                    }
                    if (lot.table6 != 0) {
                        count++
                        t6 = 3
                    }
                }

                if (result != null) {
                    if(result.T1?.size!! != 0
                            || result.T2?.size!! != 0
                            || result.T3?.size!! != 0
                            || result.T4?.size!! != 0
                            || result.T5?.size!! != 0
                            || result.T6?.size!! != 0) {
                        validate = when {
                            result.T1?.size!! < t1 -> 1
                            result.T2?.size!! < t2 -> 1
                            result.T3?.size!! < t3 -> 1
                            result.T4?.size!! < t4 -> 1
                            result.T5?.size!! < t5 -> 1
                            result.T6?.size!! < t6 -> 1
                            else -> 2
                        }
                    } else validate = 0
                } else validate = 0
            }

            return validate
        }
    }

}
