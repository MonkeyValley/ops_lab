package com.amplemind.vivasmart.features.mipe.viewModel

import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.vo_core.repository.models.realm.PlagueDetail
import javax.inject.Inject

class ItemPlagueViewModel @Inject constructor(item : PlagueDetail ){

    val id = item.id
    val name = item.name
    val isDisease = item.isDesease != null

    var medition: Int = 0
    var lvl: Int = 0

}