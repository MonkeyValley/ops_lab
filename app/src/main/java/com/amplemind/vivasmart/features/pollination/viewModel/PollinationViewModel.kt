package com.amplemind.vivasmart.features.pollination.viewModel

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.response.ActivityLinesResponse
import com.amplemind.vivasmart.core.utils.FLOW_ACTIVITES
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemAddActivitiesViewModel
import com.amplemind.vivasmart.features.pollination.repository.PollinationRepository
import com.amplemind.vivasmart.features.pollination.repository.responses.Pollinationrevisions
import com.amplemind.vivasmart.features.pollination.service.PollinationService
import com.amplemind.vivasmart.vo_core.repository.models.realm.PollinationModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PollinationViewModel @Inject constructor(
        private val service: PollinationService,
        private val repository: PollinationRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    var table: Int = 0
    var lotId: Int = 0
    var week: Int = 0

    private val requestSuccess_polination = BehaviorSubject.create<PollinationModel>()
    private val requestSuccess_polination_report = BehaviorSubject.create<List<PollinationModel>>()

    fun setContext(context: Context) {
        service.context = context
        service.preferences = UserAppPreferences(context, null, null)
    }


    fun getData(): Disposable {
        return service.getPollination(lotId, week, table)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccess_polination.onNext(it)
                }, {
                    Log.e("Pollination", apiErrors.handleLoginError(it))
                    //requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSuccessRequest_pollination(): BehaviorSubject<PollinationModel> {
        return requestSuccess_polination
    }

    fun sendPollination(flowers: String, lvl0: String, porcent0: String, lvl1: String, porcent1: String,
                        lvl2: String, porcent2: String, lvl3: String, porcent3: String,
                        pollinationPorcent: String) {
        service.sendPollination(lotId, lvl0.toInt(), lvl1.toInt(), lvl2.toInt(), lvl3.toInt(),
                porcent0.toDouble(), porcent1.toDouble(), porcent2.toDouble(), porcent3.toDouble(),
                pollinationPorcent.toDouble(), table, week, flowers.toInt())
    }


    fun getDataReport(): Disposable {
        return repository.getPollinationReport(lotId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { mapModelToViewModels(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccess_polination_report.onNext(it)
                }, {
                    Log.e("Pollination", apiErrors.handleLoginError(it))
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModels(response: List<Pollinationrevisions>): List<PollinationModel> {
        val list = arrayListOf<PollinationModel>()

        response.forEach { pollination ->
            pollination.revision.forEach { revision ->
                if(revision.table == table) list.add(revision)
            }
        }

        return list
    }

    fun onSuccessRequest_pollination_Report(): BehaviorSubject<List<PollinationModel>> {
        return requestSuccess_polination_report
    }

}