package com.amplemind.vivasmart.vo_core.repository.responses

import com.google.gson.annotations.SerializedName

class ObjectSingleResponse <T: Any> (
        @SerializedName("data")         val data: T,
        @SerializedName("item_count")   val itemCount: Int
)