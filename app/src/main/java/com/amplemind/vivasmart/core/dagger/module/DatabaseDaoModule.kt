package com.amplemind.vivasmart.core.dagger.module

import com.amplemind.vivasmart.core.repository.local.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseDaoModule {

    @Provides
    @Singleton
    internal fun provideCollaboratorsDao(appDatabase: AppDatabase): CollaboratorsDao {
        return appDatabase.collaboratorsDao()
    }

    @Provides
    @Singleton
    internal fun provideLotsDao(appDatabase: AppDatabase): LotsDao {
        return appDatabase.lotsDao()
    }

    @Provides
    @Singleton
    internal fun provideActivityDao(appDatabase: AppDatabase): ActivitiesDao {
        return appDatabase.activitiesDao()
    }

    @Provides
    @Singleton
    internal fun providePackingLineDao(appDatabase: AppDatabase): PackingLineDao {
        return appDatabase.packaginLineDao()
    }

    @Provides
    @Singleton
    internal fun provideGroovesDao(appDatabase: AppDatabase): GroovesDao {
        return appDatabase.groovesDao()
    }

    @Provides
    @Singleton
    internal fun provideCollaboratorsLineDao(appDatabase: AppDatabase): CollaboratorsLineDao {
        return appDatabase.collaboratorsLineDao()
    }


    @Provides
    @Singleton
    internal fun provideControlQualityDao(appDatabase: AppDatabase): ControlQualityDao {
        return appDatabase.controlQualityDao()
    }

    @Provides
    @Singleton
    internal fun provideProductionQualityDao(appDatabase: AppDatabase): ProductionQualityDao {
        return appDatabase.productionQualityDao()
    }

    @Provides
    @Singleton
    internal fun provideRecibaDao(appDatabase: AppDatabase): RecibaDao {
        return appDatabase.recibaDao()
    }

}
