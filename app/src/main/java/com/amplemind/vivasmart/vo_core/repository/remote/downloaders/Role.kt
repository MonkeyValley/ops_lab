package com.amplemind.vivasmart.vo_core.repository.remote.downloaders

import android.content.Context
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.vo_core.repository.models.realm.AppModule
import io.realm.Realm

enum class Role(val id: Long, private val humanNameResource: Int) {

    UNKNOWN(0, R.string.role_unknown),
    SYS_ADMIN(1, R.string.role_sysadmin),
    PRODUCTION_SUPERVISOR(2, R.string.role_production_supervisor),
    ROL_DEV(4, R.string.role_production_supervisor);

    companion object {

        fun getForRoleId(id: Long): Role =
                values().find { role ->
                    role.id == id
                } ?: UNKNOWN

    }

    fun getFirstSyncTree(): Node =
            when (this) {
                SYS_ADMIN -> sysAdminTree()
                else -> productionSupervisorTree()
            }

    fun getHumanName(context: Context) =
            context.getString(humanNameResource)

    private fun productionSupervisorTree() : Node {
        var listNode = ArrayList<Node>()

        listNode.add(Node(RequestType.MESSAGE))
        listNode.add(Node(RequestType.DAILYPAY))

        if(moduleHasAccess(1)
                || moduleHasAccess(2)
                || moduleHasAccess(3)
                || moduleHasAccess(26)
                || moduleHasAccess(27)
                || moduleHasAccess(28)
                ) {
            listNode.add(Node(RequestType.COLLABORATOR))
        }

        if(moduleHasAccess(17)
                || moduleHasAccess(18)
                || moduleHasAccess(19)
        ) {
            listNode.add(Node(RequestType.LOT_FERTIRRIEGO))
        }
        if(moduleHasAccess(25)) {
            listNode.add(Node(RequestType.PRODUCITON_RANGES))
        }
        if(moduleHasAccess(1)
                || moduleHasAccess(2)
                || moduleHasAccess(3)
                || moduleHasAccess(26)
                || moduleHasAccess(27)
                || moduleHasAccess(28)
        ) {
            listNode.add(Node(RequestType.ACTIVITY))
        }

        var validateStageDownload = false
        if(moduleHasAccess(1)
                || moduleHasAccess(2)
                || moduleHasAccess(3)
                || moduleHasAccess(26)
                || moduleHasAccess(27)
                || moduleHasAccess(28)
        ) {
            listNode.add(Node(RequestType.STAGE,
                    Node(RequestType.ACTIVITY_CODE,
                            Node(RequestType.ACTIVITY_LOG),
                            Node(RequestType.GROOVE)
                    )
            ))
            validateStageDownload = true
        }


        if(!validateStageDownload) {
            if (moduleHasAccess(4)
                    || moduleHasAccess(5)
                    || moduleHasAccess(13)
                    || moduleHasAccess(14)
                    || moduleHasAccess(15)
                    || moduleHasAccess(16)
                    || moduleHasAccess(17)
                    || moduleHasAccess(18)
                    || moduleHasAccess(19)
                    || moduleHasAccess(20)
                    || moduleHasAccess(21)
                    || moduleHasAccess(22)
            ) {
                listNode.add(Node(RequestType.STAGE
                        ))
            } else {
                validateStageDownload = false
            }
        }

        if(!validateStageDownload) {
            if (moduleHasAccess(25)) {
                listNode.add(Node(RequestType.PRODUCITON_RANGES_STAGE))
            }
        }

        val node = Node()
        node.children = listNode.toList()

        return node
    }

    private fun sysAdminTree() = Node(
            Node(RequestType.UNIT),
            Node(RequestType.LOT_FERTIRRIEGO),
            Node(RequestType.PRODUCITON_RANGES),
            Node(RequestType.CROP),
            Node(RequestType.CLIENT),
            Node(RequestType.COLLABORATOR),
            Node(RequestType.ACTIVITY),
            Node(RequestType.MESSAGE),
            Node(RequestType.DAILYPAY),
            Node(RequestType.STAGE,
                    Node(RequestType.ACTIVITY_CODE,
                            Node(RequestType.ACTIVITY_LOG),
                            Node(RequestType.GROOVE)
                    )
            )
    )

    private fun defaultTree() = sysAdminTree()

    private fun moduleHasAccess(moduleId: Int): Boolean {
        Realm.getDefaultInstance().use { realm ->
            val result = realm.where(AppModule::class.java)
                    .equalTo("moduleId", moduleId)
                    .findFirst()

            return if(result != null) result.hasAccess!!
            else false
        }
    }

    class Node(
            val requestType: RequestType = RequestType.UNKNOWN,
            var children: List<Node> = listOf(),
            onDemand: Boolean = false
    ) {
        constructor(parent: RequestType, vararg children: Node) : this(parent, listOf(*children))
        constructor(vararg children: Node) : this(children = listOf(*children))
        constructor(parent: RequestType, vararg children: Node, onDemand: Boolean) : this(parent, listOf(*children), onDemand)

        var onDemand: Boolean = onDemand
            private set

        init {
            setChildrenOnDemand(this, onDemand)
        }

        private fun setChildrenOnDemand(node: Node, onDemand: Boolean) {
            if (onDemand) {
                for (child in node.children) {
                    child.onDemand = onDemand
                    setChildrenOnDemand(child, onDemand)
                }
            }
        }
    }
}