package com.amplemind.vivasmart.vo_features.production_roster.adapters

import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemAssingGrooveBinding
import com.amplemind.vivasmart.features.collaborators.viewModel.AssingGroovesViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class GroovesAdapter(
        private val mEventBus: EventBus
) : BaseAdapter<GrooveModel, GroovesAdapter.GrooveViewModel, GroovesAdapter.GrooveViewHolder>() {

    enum class State {
        AVAILABLE,
        DISABLED,
        SELECTED
    }

    companion object {
        private val TAG = GroovesAdapter::class.java.simpleName
    }

    init {
        setHasStableIds(true)
    }

    private var mDisposable: Disposable? = null

    var position: Int = 0
    var tableNo: Int = 0
    var isCanceled: Boolean = false
    var isLoading: Boolean = false

    private var grooveTotal: Int = 0
    lateinit var activityLogUuid: String
    lateinit var activityCodeUuid: String
    private lateinit var mOccupiedGrooves: List<GrooveModel>

    private var mSelectOwned = true
    private var mOccupiedIndex = 0
    private lateinit var mCachedItems: ArrayList<GrooveModel>

    var tableGrooves: AssingGroovesViewModel.TableGrooves? = null
        set(value) {
            if (value != null) {
                tableNo = value.tableNo
                grooveTotal = value.grooveTotal
                activityCodeUuid = value.activityCodeUuid
                activityLogUuid = value.activityLogUuid

                field = value
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): GrooveViewHolder {
        val binding = ItemAssingGrooveBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GrooveViewHolder(binding)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        reload()
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mDisposable?.dispose()
    }

    override fun buildViewModel(item: GrooveModel): GrooveViewModel = GrooveViewModel(item)

    override fun getItemKey(item: GrooveModel): String = item.uuid

    override fun getItemId(position: Int): Long {
        val groove = get(position)
        return groove.tableNo.toLong().shl( 32) + groove.grooveNo
    }

    override fun getItemCount(): Int = if (::mOccupiedGrooves.isInitialized) grooveTotal else 0

    override operator fun get(position: Int): GrooveModel =
            if (position < mCachedItems.size) {
                mCachedItems[position]
            }
            else {
                val groove = if (
                        mOccupiedIndex < mOccupiedGrooves.size &&
                        position + 1 == mOccupiedGrooves[mOccupiedIndex].grooveNo
                ) {
                    Log.e(TAG, "Occupied: $mOccupiedIndex, ${mOccupiedGrooves.size}")
                    Log.e(TAG, "Position: ${position + 1}, ${mOccupiedGrooves[mOccupiedIndex].grooveNo}")
                    Log.e(TAG, "OCCUPIED by ${mOccupiedGrooves[mOccupiedIndex].activityLogUuid}!")
                    mOccupiedGrooves[mOccupiedIndex++]
                } else {
                    GrooveModel(
                            tableNo = tableNo,
                            grooveNo = position + 1
                    )
                }
                mCachedItems.add(groove)
                groove
            }

    private fun onGroovesLoaded(grooves: List<GrooveModel>) {
        isLoading = false

        if (!isCanceled) {
            mOccupiedIndex = 0
            mCachedItems = ArrayList(grooveTotal)
            mOccupiedGrooves = grooves
            onDataSetChanged()
            sendGroovesLoadedEvent()
        }
    }

    fun reload() {
        if (isCanceled || mDisposable == null) {
            isCanceled = false
            isLoading = true
            mDisposable = tableGrooves?.changes
                    ?.delaySubscription(400, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                    ?.subscribe(this::onGroovesLoaded)
        }
    }

    fun cancelIfLoading() {
        if (isLoading) {
            isCanceled = true
            isLoading = false
            mDisposable?.dispose()
            mDisposable = null
        }
    }

    fun clearSelection() {
        forEachViewModel { viewModel ->
            if (viewModel.grooveState == State.SELECTED) {
                viewModel.grooveState = State.AVAILABLE
            }
        }
        mSelectOwned = false
        notifyDataSetChanged()
    }

    fun getSelected(): List<GrooveModel> {
        val selected = mutableListOf<GrooveModel>()
        for (position: Int in 0..itemCount) {
            val viewModel = getViewModel(position, false)
            val selectedGroove: GrooveModel? =
                if (viewModel != null ) {
                    if (viewModel.grooveState == State.SELECTED) {
                        viewModel.item
                    }
                    else {
                        null
                    }
                }
                else if (get(position).activityLogUuid == activityLogUuid){
                    get(position)
                }
                else {
                    null
                }

            if (selectedGroove != null) {
                selected.add(selectedGroove)
            }
        }

        return selected
    }

    fun sendGroovesLoadedEvent() {
        mEventBus.send(OnGroovesLoadedEvent(activityLogUuid, position))
    }

    fun sendSelectionChangedEvent(grooveNo: Int, isSelected: Boolean) {
        mEventBus.send(OnGrooveSelectionChangedEvent(activityLogUuid, tableNo, grooveNo, isSelected))
    }

    inner class GrooveViewHolder(private val binding: ItemAssingGrooveBinding) : ViewHolder<GrooveViewModel>(binding.root) {

        private val mSubscriptions = AndroidDisposable()

        override fun cleanUp() {
            mSubscriptions.dispose()
        }

        override fun bind(viewModel: GrooveViewModel) {

            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()

            RxView.clicks(binding.rlGroove)
                .subscribe {
                    viewModel.grooveState = when (viewModel.grooveState) {
                        State.AVAILABLE -> {
                            sendSelectionChangedEvent(viewModel.grooveNumber, true)
                            State.SELECTED
                        }
                        State.SELECTED -> {
                            sendSelectionChangedEvent(viewModel.grooveNumber, false)
                            State.AVAILABLE
                        }
                        State.DISABLED -> State.DISABLED
                    }

                }
                .addTo(mSubscriptions)
        }

    }

    inner class GrooveViewModel(item: GrooveModel) : BaseAdapter.ViewModel<GrooveModel>(item){

        var grooveState: State = State.DISABLED
            set(value) {
                state.set(value)
                field = value
            }

        var grooveNumber: Int = 0
            set(value) {
                name.set(value.toString())
                field = value
            }

        val name = ObservableField<String>()
        var state = ObservableField<State>(grooveState)

        override fun bind(item: GrooveModel) {
            grooveNumber = item.grooveNo
            grooveState = when {
                !mSelectOwned || (item.activityLogId == 0L && item.activityLogUuid.isEmpty()) -> State.AVAILABLE
                mSelectOwned && item.activityLogUuid == activityLogUuid -> State.SELECTED
                else -> State.DISABLED
            }
            //Log.e(TAG, "activityLogUuid: ${item.activityLogUuid}")
            //Log.e(TAG, "Groove: $grooveNumber, $grooveState")
        }
    }

    data class OnGrooveSelectionChangedEvent (
            val parentItemId: String,
            val tableNo: Int,
            val grooveNo: Int,
            val isSelected: Boolean
    )

    data class OnGroovesLoadedEvent(
            val parentItemId: String,
            val position: Int
    )
}
