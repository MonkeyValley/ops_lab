package com.amplemind.vivasmart.core.dagger.module

import android.util.Log
import com.amplemind.vivasmart.BuildConfig
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response

class HostSelectionInterceptor : Interceptor {

    private val LOG_TAG = HostSelectionInterceptor::class.java.simpleName

    private var httpUrl = HttpUrl.parse(BuildConfig.SERVER_URL_QA)

    fun setHost(url: String): Boolean {
        val prevUrl = httpUrl
        val fixedUrl = fixUrl(url)

        Log.i(LOG_TAG, "Fixed URL: $fixedUrl" )

        httpUrl = HttpUrl.parse(fixedUrl)

        val res  = httpUrl != null

        if (!res) {
            httpUrl = prevUrl
        }

        return res
    }


    private fun fixUrl(url: String): String {
        var newUrl: String = url
        if (!url.startsWith("http://", true) && !url.startsWith("https://") ) {
            newUrl = "http://$newUrl"
        }
        if (!url.endsWith("/")) {
            newUrl = "$newUrl/"
        }
        return newUrl
    }

    @Override
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val newUrl = request.url()
                .newBuilder()
                .scheme(httpUrl!!.scheme())
                .host(httpUrl!!.host())
                .port(httpUrl!!.port())
                .build()
        request = request.newBuilder()
                .url(newUrl)
                .build()
        return chain.proceed(request)
    }
}