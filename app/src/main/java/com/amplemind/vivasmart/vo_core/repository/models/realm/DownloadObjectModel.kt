package com.amplemind.vivasmart.vo_core.repository.models.realm

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import java.util.*

open class DownloadObjectModel (
        @PrimaryKey
        var type: String = "",

        var isServerFaulty: Boolean = false,

        @Index
        var firstSyncDone: Boolean = false,

        @Index
        var requestDate: Date? = null,

        @Index
        var lastUpdated: Date? = null,

        @Index
        var timesRequested: Int = 0,

        var deletes: RealmList<String> = RealmList(),

        @Index
        var isCatalog: Boolean = false,

        var firstSyncLastRequested: String = ""
): RealmObject()
