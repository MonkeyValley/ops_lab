package com.amplemind.vivasmart.core.repository.response

import com.google.gson.annotations.SerializedName

data class CarryOrderPendingResponse(
        @SerializedName("data") val data: List<CarryOrderPendingResult>,
        @SerializedName("total_count") val total_count: Int
)


data class CarryOrderPendingResult(
        @SerializedName("id") val id: Int,
        @SerializedName("lot") val lot_name: String,
        @SerializedName("percent_exports") val percent_exports: Double? = null,
        @SerializedName("table") val table: String
)
