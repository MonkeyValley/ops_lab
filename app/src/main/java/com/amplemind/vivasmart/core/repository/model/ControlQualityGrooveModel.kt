package com.amplemind.vivasmart.core.repository.model

data class ControlQualityModel(val name: String, val status: Int)

data class ControlQualityGrooveModel(val section : String , val date : String)