package com.amplemind.vivasmart.features.crop_validation.models

import com.amplemind.vivasmart.core.repository.model.ImagesModel
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.google.gson.annotations.SerializedName

data class CarryOrderHarvest(
        @SerializedName("box_no") var box_no: Double,
        @SerializedName("carry_order_id") var carry_order_id: Int,
        @SerializedName("crop_id") var crop_id: Int,
        @SerializedName("crop_name") var crop_name: String,
        @SerializedName("harvest_quality_ok") var harvest_quality_ok: Boolean,
        @SerializedName("has_harvest_quality") var has_harvest_quality: Boolean,
        @SerializedName("lot_id") var lot_id: Int,
        @SerializedName("lot_name") var lot_name: String
)

data class CropIssuesModel(
        @SerializedName("issue_id") var issueId: Int,
        @SerializedName("issue_name") var issueName: String? = null,
        @SerializedName("is_ok") var is_ok: Boolean? = true,
        @SerializedName("issue") var issue: CropIssue? = null
)

data class CropIssue(
        @SerializedName("id") var id: Int,
        @SerializedName("name") var name: String? = ""
)

data class CropIssuesResponse(
        @SerializedName("data") var data: List<CropIssuesModel>,
        @SerializedName("total_count") var total_count: Int
)

data class CropValidationPost(
        @SerializedName("carry_order_id") var carry_order_id: Int,
        @SerializedName("comment") var comment: String,
        @SerializedName("issues") var issues: List<CropIssuesModel>,
        @SerializedName("sign") var sign: String,
        @SerializedName("images") var images: List<ImagesModel>
)

data class CropValidationPostResponse(
        @SerializedName("box_no") var box_no: Double,
        @SerializedName("comment") var comment: String,
        @SerializedName("created_at") var created_at: String,
        @SerializedName("crop_id") var crop_id: Int,
        @SerializedName("folio") var folio: Int,
        @SerializedName("has_harvest_quality") var has_harvest_quality: Boolean,
        @SerializedName("id") var id: Int,
        @SerializedName("lot_id") var lot_id: Int,
        @SerializedName("lot_name") var lot_name: String

)

data class CropValidationDetailResponse(
        @SerializedName("harvest_quality_images") var harvest_quality_images:  List<ImagesModel>,
        @SerializedName("issues") var issues: List<CropIssuesModel>,
        @SerializedName("quality_comment") var quality_comment: String,
        @SerializedName("sign_harvest_quality") var sign_harvest_quality: String
)





