package com.amplemind.vivasmart.features.login

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.model.RecoverPasswordModel
import com.amplemind.vivasmart.features.login.viewModel.RecoverPasswordViewModel
import kotlinx.android.synthetic.main.content_recover_activity.*
import javax.inject.Inject

@SuppressLint("Registered")
open class RecoverPasswordActivity : BaseActivity(), View.OnClickListener {
    private val TAG = RecoverPasswordActivity::class.java.simpleName

    @Inject
    lateinit var viewModel: RecoverPasswordViewModel

    private lateinit var dialog: ProgressDialog

    fun getLayout() = R.layout.content_recover_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())


        dialog = ProgressDialog(this)
        dialog.setCancelable(false)

        addSubscribers()

        btn_recover.setOnClickListener(this)
        tv_log_in.setOnClickListener(this)
    }

    private fun addSubscribers() {
        viewModel.getFormValidation().subscribe(this::onFormValidationChange).addTo(subscriptions)
        viewModel.observeProgressStatus().subscribe(this::onProgressRequestChange).addTo(subscriptions)
        viewModel.observeFail().subscribe(this::showError).addTo(subscriptions)
        viewModel.observeSuccess().subscribe(this::onSuccessRecoveryPassword).addTo(subscriptions)
    }

    private fun onProgressRequestChange(requestInProgress: Boolean) {
        if (requestInProgress) {
            dialog.show()
        } else {
            dialog.dismiss()
        }
    }

    private fun onFormValidationChange(isValid: Boolean){
        if (isValid) {
            makeRequest()
        } else {
            textInputLayout_recover.error = getString(R.string.incorrect_user)
        }
    }

    private fun showError(throwable: Throwable){
        textInputLayout_recover.error = getString(R.string.incorrect_user)
    }

    fun onSuccessRecoveryPassword(response: RecoverPasswordModel.RecoverPasswordResponse){
        ln_form.visibility = View.INVISIBLE
        success_message.visibility = View.VISIBLE
    }

    fun makeRequest() {
        viewModel.recoverPassword(ed_username_recover.text.toString()).addTo(subscriptions)
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btn_recover -> {
                viewModel.validateForm(ed_username_recover.text.toString())
            }
            R.id.tv_log_in -> {
                finish()
            }
        }
    }

}
