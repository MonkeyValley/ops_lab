package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_2_9_1 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectMessageDataModelrealm(realm)
    }

    private fun migrateUploadObjectMessageDataModelrealm(realm: DynamicRealm){
        realm.schema.get("MessageDataModel")?.apply {
            addField("lot_id", Int::class.javaObjectType)
            addField("message_id", Int::class.javaObjectType)
            addField("notification_type_id", Int::class.javaObjectType)
        }
    }

    override fun drop(realm: DynamicRealm) {

    }
}