package com.amplemind.vivasmart.core.dagger.module.fragment

import com.amplemind.vivasmart.core.base.dagger.scope.FragmentScope
import com.amplemind.vivasmart.features.packaging_quality.dialog.SwipeablePictureDialog
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualityCarryOrderListFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualityFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualityRevisionListFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualitySampleDialogFragment
import com.amplemind.vivasmart.features.production_roster.AssignUnitDialogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ReceptionQualityFragmentProvider {

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindReceptionQualityCarryOrderListFragment(): ReceptionQualityCarryOrderListFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindReceptionQualitySampleDialogFragment(): ReceptionQualitySampleDialogFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindReceptionQualityFragment(): ReceptionQualityFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindReceptionQualityRevisionListFragment(): ReceptionQualityRevisionListFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindSwipeablePictureDialog(): SwipeablePictureDialog

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindAssignUnitDialogFragment(): AssignUnitDialogFragment

}
