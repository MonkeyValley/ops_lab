package com.amplemind.vivasmart.core.repository.request

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class CreateIssueRequest(@SerializedName("description") val description: String,
                              @SerializedName("stage_id") val stageId: Int,
                              @SerializedName("issue_id") val issueId: Int,
                              @SerializedName("table_no") val tableNo: Int,
                              @SerializedName("groove_no") val grooveNo: Int,
                              @SerializedName("image") var image: String,
                              @SerializedName("offline_id") @PrimaryKey var dbPrimaryKey: Int?,
                              val categoryId: Int = 0,
                              @SerializedName("status") val status: Boolean = false,
                              val deleted: Boolean = false)

data class CloseIssueRequest(@SerializedName("is_close") val isClosed: Boolean)
data class CloseIssueResponse(@SerializedName("close_date") val closeName: String,
                              @SerializedName("created_at") val createdAt: String,
                              @SerializedName("description") val description: String,
                              @SerializedName("groove_no") val grooveNumber: Int,
                              @SerializedName("grooves") val grooves: Int,
                              @SerializedName("id") val id: Int,
                              @SerializedName("image") val image: String,
                              @SerializedName("is_active") val isActive: Boolean,
                              @SerializedName("is_close") val isClosed: Boolean,
                              @SerializedName("issue_id") val issueId: Int,
                              @SerializedName("sign_super") val signSuper: String,
                              @SerializedName("sign_super_qa") val signSuperQA: String,
                              @SerializedName("stage_id") val stageId: Int,
                              @SerializedName("table_no") val tableNumber: Int)

data class SignModel(@SerializedName("stage_id") val stageId: Int,
                     @SerializedName("sign_super") val signSuper: String,
                     @SerializedName("sign_super_qa") val signSuperQa: String,
                     @SerializedName("offline_id") val offlineId: Int,
                     @SerializedName("category_id") val categoryId: Int = 1,
                     @SerializedName("status") val status: Boolean = false)

data class ReportIssuesRequest(@SerializedName("bulk") val bulk: List<CreateIssueRequest>,
                               @SerializedName("signs") val signs: List<SignModel>)
data class ReportIssuesResponse(@SerializedName("data") val data: List<CreateIssueRequest>,
                                @SerializedName("signs") val signs: List<SignModel>)

