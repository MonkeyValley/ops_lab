package com.amplemind.vivasmart.features.waste_control.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.WasteControlRepository
import com.amplemind.vivasmart.core.repository.response.UnitResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.finished_product_quality.services.FinishedProductQualityService
import com.amplemind.vivasmart.features.production_quality.UnitsViewModel
import com.amplemind.vivasmart.features.waste_control.models.WasteControlItemModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class WasteControlTestFragmentViewModel @Inject constructor(
        private val repository: WasteControlRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val finishedProductQualityService: FinishedProductQualityService = FinishedProductQualityService()
    private val requestSuccess_WasteControl = BehaviorSubject.create<WasteControlItemModel>()
    private val requestSuccess_units = BehaviorSubject.create<List<UnitsViewModel>>()
    private val listUnits = mutableListOf<UnitsViewModel>()

    fun getCropList(): ArrayList<CropStageModelItem> {
        val listCrop = ArrayList<CropStageModelItem>()
        var cropModel = CropStageModelItem(0, "Seleccionar")
        listCrop.add(cropModel)

        for (crop in finishedProductQualityService.listCrop()) {
            val cropModelNew = CropStageModelItem(crop.id, crop.name!!)
            listCrop.add(cropModelNew)
        }
        listCrop.sortBy { it.id }
        return listCrop
    }

    fun getUnits(): Disposable {
        return repository.getUnits()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_units)
                .subscribe({
                    listUnits.clear()
                    listUnits.addAll(it)
                    requestSuccess_units.onNext(listUnits)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }
    private fun mapModelToViewModel_units(result: UnitResponse): List<UnitsViewModel> {
        val list = arrayListOf<UnitsViewModel>()

        for (item in result.data) {
            list.add(UnitsViewModel(item))
        }
        list.sortBy { it.name }
        return list
    }
    fun onSuccessRequest_units(): BehaviorSubject<List<UnitsViewModel>> {
        return requestSuccess_units
    }

    fun postFinishedWasteControl(objWasteControlItemModel: WasteControlItemModel): Disposable {
        return repository.postFinishedWasteControl(objWasteControlItemModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestSuccess_WasteControl.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }
    fun onSuccessRequest_wasteControl(): BehaviorSubject<WasteControlItemModel> {
        return requestSuccess_WasteControl
    }

    class CropStageModelItem {
        var id: Int? = null
        var name: String? = null

        constructor(id: Int?, name: String?) {
            this.id = id
            this.name = name
        }

        override fun toString(): String {
            return name!!
        }

        fun getId(): Int{
            return id!!
        }
    }
    class UnitItem {
        var id: Int? = null
        var business_unit_id: Int? = null
        var name: String? = null
        var business_unit_names: String? = null

        constructor(id: Int?, business_unit_id: Int, name: String?, business_unit_names: String) {
            this.id = id
            this.business_unit_id = business_unit_id
            this.name = name
            this.business_unit_names = business_unit_names
        }

        override fun toString(): String {
            return name!!
        }

        fun getId(): Int {
            return id!!
        }
    }
}