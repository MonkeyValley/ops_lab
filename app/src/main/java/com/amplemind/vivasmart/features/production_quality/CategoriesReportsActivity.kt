package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.FileProvider
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.CustomCamera.CameraActivity
import kotlinx.android.synthetic.main.activity_categories_reports.*
import kotlinx.android.synthetic.main.logo_and_title.view.*
import java.io.IOException
import javax.inject.Inject


class CategoriesReportsActivity : BaseActivity() {

    private val TAG = ProductionQualityActivity::class.java.simpleName
    private val CAMERA_REQUEST_CODE = 1
    private var imageUri: Uri? = null

    @Inject
    lateinit var viewModel: CategoriesReportsViewModel

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mPermission: AndroidPermission

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories_reports)
        viewModel.setupTables(intent.getParcelableArrayListExtra("tables"))
        setupToolbar()
        setupListeners()
        setupView()
        setupListenners()
    }

    private fun setupListenners() {
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onFailRequest().subscribe(this::onError).addTo(subscriptions)
        viewModel.onSuccessRequest().subscribe(this::onSuccessRequest).addTo(subscriptions)
        viewModel.onSuccessUploadImage().subscribe(this::onSuccessUpload).addTo(subscriptions)
    }

    private fun onSuccessRequest(success: Boolean) {
        val intent = Intent()
        intent.putExtra("id", intent.getIntExtra("id", 0))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun onSuccessUpload(image: String) {
        val tableNo = viewModel.getTableAtPositon(spinner_tables.selectedItemPosition)
        viewModel.createReport(et_comments.text.toString(), intent.getIntExtra("stage_id", 0),
                intent.getIntExtra("id", 0), tableNo, et_num_surco.text.toString().toInt(), image).addTo(subscriptions)
    }

    private fun setupView() {
        val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item,
                viewModel.getTables())
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_tables.adapter = spinnerAdapter
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_reports
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.requestFocus()
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = intent.getStringExtra("title")
    }

    private fun initCamera() {
        /*try {
            var imageFile = picturesHelper.createImageFile(this)
            viewModel.setImageReport(imageFile.absolutePath)
            var callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (callCameraIntent.resolveActivity(packageManager) != null) {
                var authorities = "$packageName.fileprovider"
                var imageUri = FileProvider.getUriForFile(this, authorities, imageFile)
                callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Could not create file!", Toast.LENGTH_SHORT).show()
        }*/
        val iny = Intent(this, CameraActivity::class.java)
        this.startActivityForResult(iny, 1000)
    }

    private fun getCategory(): Int {
        return intent.getIntExtra("category_id", 0)
    }

    private fun getLotId(): Int {
        return intent.getIntExtra("lot_id", 0)
    }

    private fun setupListeners() {
        cancel_button.setOnClickListener { finish() }
        delete_button.setOnClickListener {
            viewModel.setImageBitmap(null)
            delete_button.visibility = View.GONE
            take_picture_btn.setImageResource(R.mipmap.agregar_foto)
        }


        confirm_button.addThrottle().subscribe {
            val tableNo = viewModel.getTableAtPositon(spinner_tables.selectedItemPosition)
            val validation = viewModel.validateForm(et_comments.text.toString(), tableNo, et_num_surco.text.toString())
            txt_num_surco.isErrorEnabled = false
            txt_comments.isErrorEnabled = false
            when (validation) {
                CategoriesReportsViewModel.REPORTS_VALIDATION.ERROR_GROVES_MAX -> {
                    txt_num_surco.error = validation.msg
                }
                CategoriesReportsViewModel.REPORTS_VALIDATION.ERROR_TABLE -> {
                }
                CategoriesReportsViewModel.REPORTS_VALIDATION.ERROR_DESCRIPTION -> {
                    txt_comments.error = validation.msg
                }
                CategoriesReportsViewModel.REPORTS_VALIDATION.ERROR_IMAGE -> {
                    onError(validation.msg)
                }
                CategoriesReportsViewModel.REPORTS_VALIDATION.ERROR_GROVES_NUM -> {
                    txt_num_surco.error = validation.msg
                }
                CategoriesReportsViewModel.REPORTS_VALIDATION.SUCCESS -> {
                    val image = picturesHelper.createImageFile(this)
                    picturesHelper.compressImage(picturesHelper.createBitmapToFile(this, viewModel.image!!), image, 400f, 400f)
                    if (true) {
                        viewModel.uploadImage(image).addTo(subscriptions)
                    } else {
                        viewModel.createLocalReport(et_comments.text.toString(), intent.getIntExtra("stage_id", 0),
                                intent.getIntExtra("id", 0), tableNo, et_num_surco.text.toString().toInt(), image, getCategory(),
                                getLotId())
                    }
                }
            }
        }.addTo(subscriptions)

        take_picture_btn.setOnClickListener {
            when {
                mPermission.cameraPermission(this)
                        && mPermission.writePermission(this) -> initCamera()
                else -> mPermission.verifyHavePermissions(this)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == mPermission.REQUEST_CODE_PERMISSIONS) {
            if (mPermission.verifyHavePermissions(this)) {
                initCamera()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val bitmap = picturesHelper.setScaledBitmap(take_picture_btn.width,
                            take_picture_btn.height, viewModel.getImagePath()!!)

                    viewModel.setImageBitmap(bitmap)
                    delete_button.visibility = View.VISIBLE
                    take_picture_btn.setImageBitmap(bitmap)
                }
            }
            1000->{
                if (resultCode == Activity.RESULT_OK) {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, data!!.data)

                    viewModel.setImageBitmap(bitmap)
                    delete_button.visibility = View.VISIBLE
                    take_picture_btn.setImageBitmap(bitmap)
                }
            }
        }
    }

}
