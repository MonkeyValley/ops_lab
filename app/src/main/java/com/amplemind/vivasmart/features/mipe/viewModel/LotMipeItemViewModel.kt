package com.amplemind.vivasmart.features.mipe.viewModel

import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel

class LotMipeItemViewModel (val lot: LotModel){

    var lot_id = lot.id
    var soil_id = lot.soilId
    var lot_name = lot.name

    enum class LOT_STATUS(val type: Int) {
        HOUSE_CIRCLE(1),
        HOUSE_CIRCLE_COMPLETE(2),
        HOUSE_RECT(3),
        HOUSE_RECT_COMPLETE(4),
        HOUSE_CIRCLE_PENDING(5),
        HOUSE_RECT_PENDING(6);

        companion object {
            fun from(value: Int): LOT_STATUS = values().first { it.type == value }
        }
    }

    var house: LOT_STATUS
        private set

    init {
        house = LOT_STATUS.HOUSE_CIRCLE
        if (false) {
            house = LOT_STATUS.HOUSE_RECT_COMPLETE
        } else if (false) {
            house = LOT_STATUS.HOUSE_RECT_PENDING
        }
    }

    fun getLotType(): Int {
        return house.type
    }

    fun isComplete(): Boolean {
        return false
    }

    fun isPending(): Boolean {
        return true
    }
}
