package com.amplemind.vivasmart.features.collaborators

import android.util.SparseBooleanArray
import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemAssingGroovesViewModel

class ManagerSelectedItems : IManagerSelectedItems {

    enum class ANIMATION_STATUS(val state : Int){
        DELETE_GROOVE(1),
        SELECTED_GROOVE(2),
        RESET_GROOVE(3),
        RESET_GROOVE_ASSING(4);

        companion object {
            fun from(value : Int) : ANIMATION_STATUS = ANIMATION_STATUS.values().first{ it.state == value}
        }
    }

    private var list = mutableListOf<ItemAssingGroovesViewModel>()

    private val seletedItems = SparseBooleanArray()

    private var reverseAllAnimations = false

    private val deleteItems = hashMapOf<Int,Int>()

    // saveServerResponse position in list and flag animation index
    private val animationItemsIndex = SparseBooleanArray()

    // index is used to animate only the selected row
    // dirty fix, find a better solution
    private var currentSelectedIndex = -1


    override fun addDeleteItems(position: Int) {
        if (list[position].isActive()){
            deleteItems[position] = list[position].id
        }
    }

    override fun addElements(data: List<ItemAssingGroovesViewModel>) {
        list.addAll(data)
    }

    override fun clearSelections() {
        list.forEach {
            it.state_animation = null
            it.item.animation_status = null
        }
        reverseAllAnimations = true
        seletedItems.clear()
        deleteItems.clear()
    }

    override fun resetAnimationIndex() {
        reverseAllAnimations = false
        animationItemsIndex.clear()
    }

    override fun resetCurrentIndex() {
        currentSelectedIndex = -1
    }

    override fun removeAll() {
        list.clear()
    }

    override fun getGroovesSelected(): MutableList<ActivityCodeRepository.GroovesIds> {
        val ids = mutableListOf<ActivityCodeRepository.GroovesIds>()
        for (i in 0 until seletedItems.size()) {
            ids.add(ActivityCodeRepository.GroovesIds(list[seletedItems.keyAt(i)].id))
        }
        return ids
    }

    override fun toogleSelection(position: Int) {
        currentSelectedIndex = position
        if (seletedItems.get(position, false)) {
            seletedItems.delete(position)
            animationItemsIndex.delete(position)
        } else {
            seletedItems.put(position, true)
            animationItemsIndex.put(position, true)
        }
    }

    private fun getGroovesSelectedEdit(): MutableList<ActivityCodeRepository.GroovesIds> {
        val ids = mutableListOf<ActivityCodeRepository.GroovesIds>()

        for (i in 0 until seletedItems.size()){
            ids.add(ActivityCodeRepository.GroovesIds(list[seletedItems.keyAt(i)].id))
        }

        return ids
    }

    /**
     *  La lista contendra los elementos que ya tenia el colaborador, mas los nuevos, los que se eliminan no se mandan en la lista
     */
    override fun getEditItems(): MutableList<ActivityCodeRepository.GroovesIds> {
        val data = mutableListOf<ActivityCodeRepository.GroovesIds>()
        val items_selected = getGroovesSelectedEdit().filter { !deleteItems.values.contains(it.id) }
        val items_previous_actives = list.asSequence()
                .filter { it.isActive() && !deleteItems.values.contains(it.id) }
                .map { ActivityCodeRepository.GroovesIds(it.id) }.toList()

        data.addAll(items_selected)
        data.addAll(items_previous_actives)

        return data
    }

    override fun isReverseAllAnimations(position: Int) : Boolean{
        return (reverseAllAnimations && animationItemsIndex.get(position, false))
    }

    override fun getAnimationState(holder: ItemAssingGroovesViewModel, position: Int) : ANIMATION_STATUS{
        val status = if (isSelectedItem(position) && isSeletedDeleteItem(position)){
            ANIMATION_STATUS.DELETE_GROOVE
        } else if (isSelectedItem(position)) {
            ANIMATION_STATUS.SELECTED_GROOVE
        }else{
            if (holder.isActive()){
                ANIMATION_STATUS.RESET_GROOVE_ASSING
            }else{
                ANIMATION_STATUS.RESET_GROOVE
            }
        }
        return status
    }

    /*
    override fun getActivityLog(id_collaborator: Int, table: Int) = list.first {
        it.isActive() && it.idCollaborator == id_collaborator && it.item.table == table
    }.item.activityLog!!.id!!
    */
    override fun getActivityLog(collaboratorUuid: String, table: Int): String {
        return list.first{
            it.isActive() && it.activityLog.collaboratorUuid == collaboratorUuid && it.item.table == table
        }.item.activityLog!!.uuid
    }

    override fun getItemCountEdit() = getEditItems().size

    override fun isCurrentPosition(position: Int) = currentSelectedIndex == position

    override fun isSeletedDeleteItem(position: Int) = deleteItems.keys.contains(position)

    override fun isSelectedItem(position: Int) = seletedItems.get(position, false)

    override fun getSelectedItemCount() = seletedItems.size()

    override fun getItemCount() = list.size

    override fun getItemAssingGroovesViewModel(position: Int) = list[position]
}

interface IManagerSelectedItems{

    fun getItemAssingGroovesViewModel(position: Int) : ItemAssingGroovesViewModel

    fun addDeleteItems(position : Int)

    fun addElements(data : List<ItemAssingGroovesViewModel>)

    fun removeAll()

    fun getGroovesSelected() : MutableList<ActivityCodeRepository.GroovesIds>

    fun getItemCount() : Int

    fun getItemCountEdit() : Int

    fun getSelectedItemCount() : Int

    fun clearSelections()

    fun resetAnimationIndex()

    fun resetCurrentIndex()

    fun toogleSelection(position: Int)

    fun isSeletedDeleteItem(position: Int) : Boolean

    fun isSelectedItem(position: Int): Boolean

    fun isCurrentPosition(position: Int): Boolean

    fun isReverseAllAnimations(position: Int) : Boolean

    fun getAnimationState(holder: ItemAssingGroovesViewModel, position: Int) : ManagerSelectedItems.ANIMATION_STATUS

    fun getEditItems(): MutableList<ActivityCodeRepository.GroovesIds>

    fun getActivityLog(collaboratorUuid: String, table: Int) : String
}