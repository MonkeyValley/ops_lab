package com.amplemind.vivasmart.features.operations.viewModel

import com.amplemind.vivasmart.core.repository.PayrollRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import io.reactivex.Observable
import javax.inject.Inject

class OperationsActivitiesViewModel @Inject constructor(private val repository : PayrollRepository){

    fun countActiveLogsInStage(stage: StageModel): Observable<Int> {
        return repository.countActiveLogsInStage(stage)
    }

    fun countActiveLogsInActivity(stage: StageModel, activity: ActivityModel): Observable<Int> {
        return repository.countActiveLogsInActivity(stage, activity)
    }
}
