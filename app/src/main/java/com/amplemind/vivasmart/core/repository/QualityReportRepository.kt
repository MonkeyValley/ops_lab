package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.extensions.dummyReportResultData
import com.amplemind.vivasmart.core.repository.model.QualityReportModelTemp
import com.amplemind.vivasmart.core.repository.remote.QualityReportApi
import com.amplemind.vivasmart.features.packaging_quality.quality_report_result.ItemQualityReportViewModel
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/12/18.
 */
open class QualityReportRepository @Inject constructor(qualityRepoprtApi : QualityReportApi){

    fun getQualityReport(): Observable<List<QualityReportModelTemp>> {
        return Observable.just(dummyReportResultData())
    }


}