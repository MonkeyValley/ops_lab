package com.amplemind.vivasmart.core.utils

import android.content.Context
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel

//region API
val TIME_OUT = 90L
val RETRY = 10L
val TIME_OUT_SYNC = 90L
val TIME_OUT_GROOVES = 90L
//endregion

val LOCAL_BROADCAST_USER_RECEIVED = "local_broadcast_user_received"
val LOCAL_BROADCAST_USER_PACKING_RECEIVED = "local_broadcast_user_packing_received"
val COLLABORATORS = "collaborators"
val HAS_BOXCOUNT = "has_boxcount"
val SINGLE_SCANNER = "single_scanner"
val CODE = "CODE"
val SYNC_REPORTS = "sync_reports"
val SYNC_ERRORS = "sync_errors"

val LOCAL_BROADCAST_FINISH_ACTIVITY = "local_broadcast_finish_activity"
val LOCAL_BROADCAST_SIGN_USER = "local_broadcast_sign_user"
val SYNC_DATABASE = "sync_database"

val BUSY_GROOVE = 1
val AVAILABLE_GROOVE = 2
val SELECT_GROOVE = 3

val COUNT_BOX_REQUEST = 1

val GROOVES_UPDATE_REQUEST = 2

val TIME_PRODUCTION = 1
val TIME_LINES = 2

val FRAGMENT_LINES = 1

val FLOW_ACTIVITES = 1
val FLOW_PRESENTATION = 2

val SUCCESS = 200

var USER_ID = 0

var STAGE_UUID = ""

var LOT_ID = 0

var CROP_ID = 0

var STAGE_ID = 0

var BUSINESS_ID = 0

var LOT : LotModel? = null

var FERTIRRIEGO_DATE : String = ""

var FERTIRRIEGO_TPYE : String = ""

var WEIGHT_PROM_LIST = ArrayList<String>()

var CATEGORY_ID: Int = 0

var VALIDATE_NO_SIGNATURE: Boolean = false

var ACTIVITY_CONTEXT: Context? = null

var STAGE_TYPE_ID: Int = 0

var CROP_ID_RECIBA: Int = 0

var EDITED_LOG_IN_TIMER: Boolean = false