package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.currentISODate
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class ActivityLogModel(
        @SerializedName("id")                       var id: Long = 0,
        @Index
        @SerializedName("activity_code_id")         var activityCodeId: Long = 0,
        @Index
        @SerializedName("activity_code_uuid")       var activityCodeUuid: String = "",
        @Index
        @SerializedName("collaborator_id")          var collaboratorId: Int = 0,
        @Index
        @SerializedName("collaborator_uuid")        var collaboratorUuid: String = "",
        @SerializedName("collaborator")             var collaborator: CollaboratorModel? = null,
        @SerializedName("date")                     var date: String = currentISODate("yyyy-MM-dd"),
        @SerializedName("is_training")              var isTraining: Boolean = false,
        @SerializedName("is_pending")               var isPending: Boolean = false,
        @SerializedName("is_done")                  var isDone: Boolean = false,
        @SerializedName("quantity")                 var quantity: Int = 0,
        @SerializedName("bad_quantity")             var badQuantity: Int? = null,
        @SerializedName("sign")                     var sign: String = "No Sign",
        @SerializedName("time")                     var time: Long = 0,
        @SerializedName("is_practice")              var isPractice: Boolean = false,
        @SerializedName("quality_sign")             var qualitySign: String = "No Sign",
        @SerializedName("sing_date")                var signDate: String = "",
        @SerializedName("supervisor_quality_sign")  var supervisorQualitySign: String = "No Sign",
        @SerializedName("start_time")               var startTime: Long = 0,
        @SerializedName("is_paused")                var isPaused: Boolean = true,
        @SerializedName("is_control")               var isControl: Boolean = false,
        @SerializedName("extra_hours")              var extra_hours: Int = 0,
        @SerializedName("collaborators_no")         var collaboratorNo: Int =0,
        @SerializedName("version")                  var version: Long = 0L,
        @PrimaryKey
        @SerializedName("uuid")                     override var uuid: String = getUUID(),

        //Local fields
        var activityCode: ActivityCodeModel? = null,
        var grooves: RealmList<GrooveModel>? = RealmList()

): RealmObject(), Identifiable
