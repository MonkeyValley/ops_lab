package com.amplemind.vivasmart.core.base

import android.os.Bundle
import android.view.View
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import dagger.android.support.DaggerAppCompatDialogFragment

open class BaseDialogFragment : DaggerAppCompatDialogFragment() {

    val subscriptions = AndroidDisposable()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpCallbacks()
        setUpUI()
        loadData()
        setUpUICallbacks()
    }

    override fun onDestroyView() {
        subscriptions.dispose()
        super.onDestroyView()
    }

    override fun onDestroy() {
        if (context != null) {
            hideKeyboard(context!!)
        }
        super.onDestroy()
    }

    open fun setUpCallbacks() {}
    open fun setUpUI() {}
    open fun setUpUICallbacks() {}
    open fun loadData() {}
}