package com.amplemind.vivasmart.vo_features.services.sync

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import androidx.core.content.ContextCompat
import android.util.Log
import com.amplemind.vivasmart.vo_core.repository.remote.utils.ReactiveHandlerThread
import com.amplemind.vivasmart.vo_features.services.ConnectivityManager
import com.amplemind.vivasmart.vo_features.services.SyncManager
import dagger.android.DaggerService
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class SyncService : DaggerService() {

    companion object {

        val TAG = SyncService::class.simpleName
        val THREAD_NAME = "$TAG.RequestThread"

        private val EXTRA_FOREGROUND = "$TAG.Foreground"

        internal fun start(context: Context, startForeground: Boolean = true) {
            val intent = Intent(context, SyncService::class.java).apply {
                putExtra(EXTRA_FOREGROUND, startForeground)
            }

            if (startForeground) {
                ContextCompat.startForegroundService(context, intent)
            } else {
                context.startService(intent)
            }
        }
    }

    @Inject
    lateinit var mConnectivityManager: ConnectivityManager

    @Inject
    lateinit var mSyncManager: SyncManager

    @Inject
    lateinit var mDownloadModule: SyncDownloadModule

    @Inject
    lateinit var mUploadModule: SyncUploadModule

    private val mRequestThread = ReactiveHandlerThread(THREAD_NAME)

    private var mRequestThreadDisposable: Disposable? = null

    override fun onCreate() {
        super.onCreate()
        mConnectivityManager.setUp()
        mRequestThread.start()
        setUpCallbacks()
    }

    override fun onDestroy() {
        super.onDestroy()
        mRequestThread.quitSafely()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (intent?.getBooleanExtra(EXTRA_FOREGROUND, false) == true) {
            mSyncManager.pair(this)
        }

        return Service.START_STICKY
    }

    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    private fun setUpCallbacks() {
        mRequestThreadDisposable = mRequestThread.onStatusChanged
                .subscribe(this::onThreadStatusChanged)
    }

    private fun onThreadStatusChanged(status: ReactiveHandlerThread.Status) {
        when (status) {

            ReactiveHandlerThread.Status.READY -> {
                Log.i(TAG, "RequestThread is ready, starting sync modules...")
                mRequestThread.scheduler.let {scheduler ->
                    mDownloadModule.subscriptionScheduler = scheduler
                    mUploadModule.subscriptionScheduler = scheduler
                }
                mDownloadModule.onReady.onNext(true)
                mUploadModule.onReady.onNext(true)
                mDownloadModule.setUp()
                mUploadModule.setUp()
            }

            ReactiveHandlerThread.Status.QUITTING_SAFELY -> {
                Log.i(TAG, "RequestThread is quitting safely...")
                Handler(mRequestThread.looper).apply {
                    post {
                        mDownloadModule.onReady.onNext(false)
                        mUploadModule.onReady.onNext(false)
                        mDownloadModule.cleanUp()
                        mUploadModule.cleanUp()
                    }
                }
            }

            ReactiveHandlerThread.Status.QUITTING -> {
                Log.e(TAG, "RequestThread is quitting unsafely, please call quitSafely() to properly kill the thread")
            }

            ReactiveHandlerThread.Status.NOT_READY -> {
                Log.i(TAG, "RequestThread is not ready, waiting...")
            }
        }

    }

}
