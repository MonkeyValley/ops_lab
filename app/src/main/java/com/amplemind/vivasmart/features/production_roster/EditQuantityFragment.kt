package com.amplemind.vivasmart.features.production_roster

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TableLayout
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.hideSoftKeyboard
import com.amplemind.vivasmart.core.extensions.snack
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import io.realm.Realm
import java.lang.Exception


class EditQuantityFragment : DialogFragment() {

    companion object {
        val TAG = EditQuantityFragment::class.java.simpleName

        private val PARAM_ACTIVITY_LOG_ID = "$TAG.ActivityLogId"
        private val PARAM_TITLE             = "$TAG.Title"
        private val PARAM_QUANTITY          = "$TAG.Quantity"
        private val PARAM_TIME_IN_HOURS     = "$TAG.TimeInHours"
        private val PARAM_ONLY_TIME         = "$TAG.OnlyTime"

        fun newInstance(activityLogid: String, title: String, quantity: Int, timeInHours: Int, onlyTime: Boolean = false): EditQuantityFragment =
            EditQuantityFragment().apply {
                arguments = Bundle().apply {
                    putString(PARAM_ACTIVITY_LOG_ID, activityLogid)
                    putString(PARAM_TITLE, title)
                    putInt(PARAM_QUANTITY, quantity)
                    putInt(PARAM_TIME_IN_HOURS, timeInHours)
                    putBoolean(PARAM_ONLY_TIME, onlyTime)
                }
            }

    }

    private var listener: OnEditQuantity? = null

    private lateinit var edCant: EditText
    private lateinit var edTimeHour: EditText
    private lateinit var edTimeMinutes: EditText
    private lateinit var tvAvailable: TextView
    private lateinit var mView: View

    private lateinit var mActivityLogId: String
    private lateinit var mTitle: String
    private var mQuantity: Int = 0
    private var mTimeInHours = 0
    private var mOnlyTime: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        readArguments()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mView = LayoutInflater.from(context).inflate(R.layout.content_edit_quantity, null, false)
        builder.setView(mView)

        setUpUi()

        builder.setPositiveButton("Aceptar", null)
        builder.setNegativeButton("Cancelar") { _, i -> dismiss() }

        return builder.create()
    }

    fun setUpUi(){
        mView.findViewById<TextView>(R.id.tv_title).text = mTitle

        edCant = mView.findViewById(R.id.ed_cant)
        edTimeHour = mView.findViewById(R.id.ed_time_hour)
        edTimeMinutes = mView.findViewById(R.id.ed_time_minute)
        tvAvailable = mView.findViewById(R.id.tv_available)
        tvAvailable.text = aviableQuantity()

        edCant.setText(mQuantity.toString())
        edTimeHour.setText(mTimeInHours.toString())

        if (mOnlyTime) {
            edCant.visibility = View.GONE
            mView.findViewById<TextView>(R.id.tv_quantity).visibility = View.GONE
            mView.findViewById<TextView>(R.id.tv_available).visibility = View.GONE
        }

        mView.findViewById<ImageView>(R.id.iv_close).setOnClickListener {
            listener?.onCancel()
        }


        edTimeHour.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (edTimeHour.text.isNotEmpty()) {
                    edTimeMinutes.setText("")
                }
            }
        })

        edTimeMinutes.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (edTimeMinutes.text.isNotEmpty()) {
                    if (edTimeMinutes.text.toString().toInt() >= 60) {
                        edTimeMinutes.setText("59")
                    }
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {

            val quantity = edCant.text.toString().toIntOrNull() ?: 0
            val hour = edTimeHour.text.toString().toIntOrNull() ?: 0
            val minutes = if(edTimeMinutes.text.toString() != "00")
                edTimeMinutes.text.toString().toIntOrNull() ?: 0
            else 0

            val timeInMinutes = minutes + (hour * 60)

            when {
                timeInMinutes <= 0 -> {
                    mView.snack("El campo de tiempo no puede quedar vacío o en cero")
                }
                quantity <= 0 -> {
                    mView.snack("La cantidad no puede quedar vacío o en cero")
                }
                mOnlyTime -> {
                    listener?.onEditItem(mActivityLogId, quantity, timeInMinutes)
                }
                validateQuantity(quantity) -> {
                    mView.snack("Las unidades que desea asignar son mayores a las disponibles")
                }
                else -> {
                    listener?.onEditItem(mActivityLogId, quantity, timeInMinutes)
                }
            }
        }
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
            listener?.onCancel()
        }
    }

    private fun validateQuantity(quantity: Int): Boolean {
        try {
            Realm.getDefaultInstance().use { realm ->
                var activityLog = realm
                        .where(ActivityLogModel::class.java)
                        .equalTo("id", mActivityLogId.toInt())
                        .findFirst()

                var unitUse =  activityLog!!.quantity

                var busy  = realm
                        .where(ActivityCodeModel::class.java)
                        .equalTo("id", activityLog.activityCodeId)
                        .findFirst()!!.occupiedSpaces

                var unitLimit = realm
                        .where(ActivityCodeModel::class.java)
                        .equalTo("id", activityLog.activityCodeId)
                        .findFirst()!!.totalSpaces

                var edityUnit = ((busy - unitUse) + quantity)

                return  unitLimit < edityUnit
            }
        } catch (e:Exception){
            return false
        }
    }

    private fun aviableQuantity(): String {
        try {
            Realm.getDefaultInstance().use { realm ->
                var activityLog = realm
                        .where(ActivityLogModel::class.java)
                        .equalTo("id", mActivityLogId.toInt())
                        .findFirst()

                var busy  = realm
                        .where(ActivityCodeModel::class.java)
                        .equalTo("id", activityLog!!.activityCodeId)
                        .findFirst()!!.occupiedSpaces

                var unitLimit = realm
                        .where(ActivityCodeModel::class.java)
                        .equalTo("id", activityLog!!.activityCodeId)
                        .findFirst()!!.totalSpaces

                return "Estado: $busy/$unitLimit"
            }
        } catch (e:Exception){
            return "Estado: 0/0"
        }
    }

    private fun readArguments() {
        arguments?.apply {
            mActivityLogId      = requireNotNull(getString(PARAM_ACTIVITY_LOG_ID))
            mTitle              = requireNotNull(getString(PARAM_TITLE))
            mQuantity           = getInt(PARAM_QUANTITY)
            mTimeInHours        = getInt(PARAM_TIME_IN_HOURS)
            mOnlyTime           = getBoolean(PARAM_ONLY_TIME)
        }
    }

    fun setSnackError() {
        mView.snack("Número de unidades invalido, favor de verificar")
    }

    fun setOnEditQuantity(listener: OnEditQuantity) {
        this.listener = listener
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.hideSoftKeyboard()
    }
}

interface OnEditQuantity {
    fun onEditItem(activityLogId: String, quantity: Int, timeInMinutes: Int)
    fun onCancel()
}
