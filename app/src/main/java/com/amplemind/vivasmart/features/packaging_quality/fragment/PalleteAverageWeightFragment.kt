package com.amplemind.vivasmart.features.packaging_quality.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.packaging_quality.adapters.RecibaAverageWeightPagerAdapter

class PalleteAverageWeightFragment : Fragment(), RecibaAverageWeightPagerAdapter.AverageWeightFragment {

    companion object {

        val PALLET_WEIGHT = 23.5
        val BOX_WEIGHT = 1.5

        @JvmStatic
        fun newInstance() = PalleteAverageWeightFragment()
    }

    private val mPallets = mutableListOf<View>()
    private lateinit var mAverage: TextView

    private var mAverageValue: Double = 0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_pallete_average_weight, container, false)

        mPallets.add(view.findViewById(R.id.pallet1))
        mPallets.add(view.findViewById(R.id.pallet2))
        mPallets.add(view.findViewById(R.id.pallet3))

        val textWatcher = object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                doAverageCalculations()
            }
        }

        mPallets.forEachIndexed{ index: Int, pallet: View ->
            pallet.findViewById<TextView>(R.id.tv_weight).text = "Peso ${index + 1}"
            pallet.findViewById<EditText>(R.id.et_weight).addTextChangedListener(textWatcher)
            pallet.findViewById<EditText>(R.id.et_boxes).addTextChangedListener(textWatcher)
        }

        mAverage = view.findViewById(R.id.tv_avg_weight)

        doAverageCalculations()

        return view
    }


    private fun doAverageCalculations() {

        var totalBoxes = 0.0

        val sum = mPallets.sumByDouble {pallet ->
            val weight = pallet.findViewById<EditText>(R.id.et_weight).text.toString().toDoubleOrNull() ?: 0.0
            val boxes = pallet.findViewById<EditText>(R.id.et_boxes).text.toString().toDoubleOrNull() ?: 0.0

            totalBoxes += boxes

            return@sumByDouble if (weight == 0.0 || boxes == 0.0) 0.0 else (weight - (boxes * BOX_WEIGHT)) - PALLET_WEIGHT
        }

        mAverageValue = if (totalBoxes > 0.0) sum / totalBoxes else 0.0

        mAverage.text = "%.1f Kg".format(mAverageValue)
    }

    override fun calculateAverage(): Double {
        return mAverageValue
    }


}
