package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class GroovesAssingReponse(
        @SerializedName("data") val data: List<Grooves>,
        @SerializedName("total_count") val totalCount: Int
)

@Entity
data class GroovesResultReponse(
        @SerializedName("activity_code_id") val activity_code_id: Int,
        @SerializedName("collaborator_id") val collaborator_id: Int,
        @SerializedName("date") val date: String,
        @SerializedName("id") @ColumnInfo(name = "primary_activity_log_id") @PrimaryKey(autoGenerate = true) var id: Long,
        @SerializedName("is_training") val is_training: Boolean,
        @SerializedName("is_pending") val is_pending: Boolean,
        @SerializedName("is_done") val isDone: Boolean,
        @SerializedName("quantity") var quantity: Int,
        @SerializedName("sign") val sign: String,
        @SerializedName("time") val time: Int
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readLong(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(activity_code_id)
        parcel.writeInt(collaborator_id)
        parcel.writeString(date)
        parcel.writeLong(id)
        parcel.writeByte(if (is_training) 1 else 0)
        parcel.writeByte(if (is_pending) 1 else 0)
        parcel.writeByte(if (isDone) 1 else 0)
        parcel.writeInt(quantity)
        parcel.writeString(sign)
        parcel.writeInt(time)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GroovesResultReponse> {
        override fun createFromParcel(parcel: Parcel): GroovesResultReponse {
            return GroovesResultReponse(parcel)
        }

        override fun newArray(size: Int): Array<GroovesResultReponse?> {
            return arrayOfNulls(size)
        }
    }

}

@Entity
data class Grooves(
        @Embedded @SerializedName("activity_log") var activity_log : GroovesResultReponse,
        @SerializedName("activity_log_id") val activity_log_id: Long,
        @SerializedName("groove_no") val groove_no: Int,
        @SerializedName("id") @PrimaryKey(autoGenerate = false) val id: Int?,
        @SerializedName("table_no") val table_no: Int
)