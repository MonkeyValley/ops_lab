package com.amplemind.vivasmart.vo_features.services.sync

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.login.LoginActivity
import com.amplemind.vivasmart.vo_core.repository.dao.realm.UploadObjectDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.utils.TYPE_JSON
import com.amplemind.vivasmart.vo_features.services.ConnectivityManager
import com.google.gson.JsonParser
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import okhttp3.*
import java.io.File
import java.io.FileNotFoundException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SyncUploadModule @Inject constructor(
        private val context: Context,
        private val mClient: OkHttpClient,
        private val mPref: UserAppPreferences,
        private val mConnectivityManager: ConnectivityManager,
        private val mUploadObjectDao: UploadObjectDao
) : SyncModule() {

    companion object {
        private val TAG = SyncUploadModule::class.java.simpleName
    }

    private var mUploaderDisposable: Disposable? = null

    val onEvent = BehaviorSubject.createDefault(UploadEvent(EventType.NOT_READY))

    fun setUp() {
        mUploaderDisposable =
                checkIfReady()
                        .switchMapCompletable {
                            mUploadObjectDao.peekQueue()
                                    .onMainThread()
                                    .doOnNext { queueStatus ->
                                        if (queueStatus.total > 0) {
                                            onEvent.value?.apply {
                                                if (this is RemainingUploadEvent) {
                                                    onEvent.onNext(createUpdated(queueStatus.total))
                                                } else {
                                                    sendUploadingEvent(queueStatus.total)
                                                }
                                            }
                                        } else {
                                            sendEvent(EventType.COMPLETE)
                                        }
                                    }
                                    .onCustomScheduler()
                                    .filter { queueStatus ->
                                        queueStatus.run {
                                            firstItem != null && firstItem.isValid
                                        }
                                    }
                                    .distinctUntilChanged { queueStatus ->
                                        queueStatus.firstItem?.uuid
                                    }
                                    .onMainThread()
                                    .doOnNext { queueStatus ->
                                        sendUploadingEvent(queueStatus.total)
                                    }
                                    .onCustomScheduler()
                                    .concatMapCompletable { queueStatus ->
                                        queueStatus.firstItem?.let { firstItem ->
                                            buildUploadCompletable(firstItem, queueStatus.total)
                                                    .andThen(mUploadObjectDao.remove(firstItem.uuid))
                                                    .onErrorResumeNext { error ->
                                                        if (error is UploadServerException) {
                                                            mUploadObjectDao.saveServerError(
                                                                    firstItem.uuid,
                                                                    error.code,
                                                                    error.serverResponse
                                                            )
                                                        } else {
                                                            Completable.error(error)
                                                        }
                                                    }
                                                    .doOnError { error ->
                                                        Log.e("setUp-update", Log.getStackTraceString(error))
                                                    }
                                                    .onErrorComplete()
                                        }
                                    }
                        }
                        .subscribeOn(subscriptionScheduler)
                        .onMainThread()
                        .subscribe()
    }

    override fun cleanUp() {
        mUploaderDisposable?.dispose()
    }

    private fun checkIfReady(): Observable<Boolean> =
            onReady
                    .distinctUntilChanged()
                    .doOnNext { isReady ->
                        if (isReady) {
                            sendEvent(EventType.READY)
                        } else {
                            sendEvent(EventType.NOT_READY)
                        }
                    }

    private fun buildUploadCompletable(obj: UploadObjectModel, remaining: Int): Completable =
            Completable.create { emitter ->
                try {
                    val response = mClient
                            .newCall(buildRequest(obj))
                            .execute()

                    Log.e("respuesta", response.body().toString())

                    when (response.code()) {
                        200 -> emitter.onComplete()
                        201 -> emitter.onComplete()
                        202 -> emitter.onComplete()
                        400 -> {
                            if (obj.verb == "DELETE") emitter.onComplete()
                            else emitter.onError(UploadServerException.build(response))
                        }
                        401 -> {
                            onLogOut()
                            emitter.onError(UploadServerException.build(response))
                        }
                        404 -> {
                            if (obj.verb == "DELETE") emitter.onComplete()
                            else emitter.onError(UploadServerException.build(response))
                        }
                        409 -> emitter.onComplete()
                        500 -> {
                            when(obj.verb){
                                "PUT" -> if(mUploadObjectDao.createNewPostFromCurrentError(context, obj, "POST", 1)) emitter.onComplete()
                                "DELETE" -> if(mUploadObjectDao.deleteFromUploadObject(obj)) emitter.onComplete()
                                else -> emitter.onError(UploadServerException.build(response))
                            }
                        }
                        502 -> {
                            when(obj.verb){
                                "PUT" -> if(mUploadObjectDao.createNewPostFromCurrentError(context, obj, "POST", 1)) emitter.onComplete()
                                else -> emitter.onError(UploadServerException.build(response))
                            }
                        }
                        else -> {
                            Log.e("error-update", response.message())
                            if (response.isSuccessful) {
                                emitter.onComplete()
                            } else {
                                emitter.onError(UploadServerException.build(response))
                            }
                        }
                    }

                    response.close()
                } catch (e: FileNotFoundException) {
                    Log.e("FileNotFoundException", e.toString())
                    emitter.onComplete()
                } catch (e: Throwable) {
                    Log.e("Throwable", e.message.toString())

                    if(e.message == "timeout") {
                        if (obj.verb == "PUT") {
                            if (mUploadObjectDao.validateBulkCount(obj) > 10) {
                                if (mUploadObjectDao.createNewPostFromCurrentError(context, obj, "PUT", 2)) {
                                    emitter.onComplete()
                                }
                            }
                        }
                    } else {
                        emitter.onError(e)
                    }
                }
            }
                    .onMainThread()
                    .doOnError { error ->
                        sendErrorEvent(error, remaining)
                    }
                    .onCustomScheduler()
                    .retryWhen { errors ->
                        errors.switchMap { error ->
                            when (error) {
                                is SocketTimeoutException -> {
                                    buildCountdownTimer()
                                            .doOnNext { remainingSeconds ->
                                                sendCountdownEvent(remainingSeconds, remaining)
                                            }
                                            .takeLast(1)
                                }
                                is UnknownHostException -> {
                                    Log.e("UnknownHostException1", "conexion")
                                    mConnectivityManager
                                            .notifyAndWaitConnection(subscriptionScheduler)
                                            .onMainThread()
                                            .doOnNext {
                                                Log.e("UnknownHostException2", "conexion")
                                                if (onEvent.value is RemainingUploadEvent) {
                                                    Log.e("UnknownHostException3", "conexion")
                                                    val lastEvent = onEvent.value as RemainingUploadEvent
                                                    sendUploadingEvent(lastEvent.remaining)
                                                }
                                            }
                                            .onCustomScheduler()
                                            .toFlowable(BackpressureStrategy.DROP)
                                }
                                else -> {
                                    Log.e("UnknownHostException0", "conexion $error")
                                    Flowable.error(error)
                                }
                            }
                        }
                    }

    private fun buildRequest(obj: UploadObjectModel): Request {
        return when (obj.contentType) {
            TYPE_JSON -> buildJsonRequest(obj)
            else -> buildFileRequest(obj)
        }
    }

    private fun buildFileRequest(obj: UploadObjectModel): Request {

        val requestData = JsonParser().parse(obj.body).asJsonObject

        val fileName = requestData["name"].asString
        val path = requestData["file"].asString
        val type = requestData["type"].asString

        val imageFile = File(path)

        val imageBody = RequestBody.create(MediaType.parse(obj.contentType), imageFile)
        val filePart = MultipartBody.Part.createFormData("img", imageFile.name, imageBody)

        val body = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file_name", fileName)
                .addFormDataPart("type", type)
                .addPart(filePart)
                .build()

        return Request.Builder()
                .addHeader("Authorization", mPref.authorizationToken)
                .url(obj.url)
                .post(body)
                .build()

    }

    private fun buildJsonRequest(obj: UploadObjectModel): Request {

        val body = RequestBody.create(MediaType.parse(obj.contentType), obj.body.toByteArray())

        val url = HttpUrl.parse(obj.url)

        return Request.Builder()
                .addHeader("Authorization", mPref.authorizationToken)
                .url(url!!)
                .method(obj.verb, body)
                .build()

    }

    private fun buildCountdownTimer() =
            com.amplemind.vivasmart.vo_core.utils.buildCountdownTimer(15, 2)
                    .map { value ->
                        value.toInt()
                    }

    private fun sendEvent(eventType: EventType) =
            onEvent.onNext(UploadEvent(eventType))

    private fun sendUploadingEvent(remaining: Int) =
            onEvent.onNext(UploadingEvent(remaining))

    private fun sendCountdownEvent(remainingSeconds: Int, remaining: Int) =
            onEvent.onNext(RetryCountdownEvent(remainingSeconds, remaining))

    private fun sendErrorEvent(error: Throwable, remaining: Int) =
            onEvent.onNext(UploadErrorEvent(remaining, error))


    class UploadServerException private constructor(val code: Int, val serverResponse: String) : Exception(serverResponse) {

        companion object {
            internal fun build(response: Response): UploadServerException {
                return UploadServerException(response.code(), response.body()?.string()
                        ?: "No response")
            }
        }

    }

    open class UploadEvent(
            val eventType: EventType
    )

    open class RemainingUploadEvent(
            eventType: EventType,
            val remaining: Int
    ) : UploadEvent(eventType) {

        open fun createUpdated(remaining: Int): RemainingUploadEvent =
                RemainingUploadEvent(
                        eventType,
                        remaining
                )

    }

    class UploadingEvent(
            remaining: Int
    ) : RemainingUploadEvent(EventType.UPLOADING, remaining) {

        override fun createUpdated(remaining: Int): UploadingEvent =
                UploadingEvent(remaining)

    }

    class UploadErrorEvent(
            remaining: Int,
            val error: Throwable
    ) : RemainingUploadEvent(EventType.ERROR, remaining) {

        override fun createUpdated(remaining: Int): UploadErrorEvent =
                UploadErrorEvent(remaining, error)

    }

    class RetryCountdownEvent(
            val remainingSeconds: Int,
            remaining: Int
    ) : RemainingUploadEvent(EventType.COUNTDOWN, remaining) {

        override fun createUpdated(remaining: Int): RemainingUploadEvent =
                RetryCountdownEvent(remainingSeconds, remaining)

    }

    enum class EventType {
        NOT_READY,
        READY,
        UPLOADING,
        COUNTDOWN,
        ERROR,
        COMPLETE
    }


    private fun onLogOut() {
        val intent = Intent(context, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
    }
}