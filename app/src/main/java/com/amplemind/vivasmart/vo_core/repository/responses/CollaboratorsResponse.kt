package com.amplemind.vivasmart.vo_core.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.google.gson.annotations.SerializedName

data class CollaboratorsResponse (
        @SerializedName("data")        val data: List<CollaboratorModel>,
        @SerializedName("total_count") val totalCount: Int)

