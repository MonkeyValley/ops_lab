package com.amplemind.vivasmart.features.phenology.viewmodel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.PhenologyRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyResponse
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVarieties
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVars
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PhenologyVarietiesFragmentViewModel @Inject constructor(
        private val repository: PhenologyRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val requestSuccess_lots = BehaviorSubject.create<PhenologyResponse>()
    private var listLots  = PhenologyResponse(emptyList(), emptyList())

    fun getLotsVarieties(stage_id: Int, is_commercial: Int): Disposable {
        return repository.getLotsVarieties(stage_id, is_commercial)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModels)
                .subscribe({
                    listLots = it
                    requestSuccess_lots.onNext(listLots)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModels(models: PhenologyResponse): PhenologyResponse {
        val listVars = arrayListOf<PhenologyVars>()
        val listVarieties = arrayListOf<PhenologyVarieties>()

        for (itemVars in models.phenology_vars!!) {
            val vars = PhenologyVars(
                    itemVars.phenology_var_id,
                    itemVars.phenology_var_name,
                    itemVars.phenology_var_description,
                    itemVars.phenology_var_image,
                    itemVars.unit_id,
                    itemVars.unit_name,
                    itemVars.balance_from,
                    itemVars.balance_to,
                    itemVars.generative_from,
                    itemVars.generative_to,
                    itemVars.vegetative_from,
                    itemVars.vegetative_to)
            listVars.add(vars)
        }

        for (itemVariety in models.varieties!!) {
            val variety = PhenologyVarieties(itemVariety.has_cluster, itemVariety.tables, itemVariety.variety_id, itemVariety.variety_name)
            listVarieties.add(variety)
        }

        return PhenologyResponse(listVars, listVarieties)
    }

    fun onSuccessRequest_lots(): BehaviorSubject<PhenologyResponse> {
        return requestSuccess_lots
    }

}