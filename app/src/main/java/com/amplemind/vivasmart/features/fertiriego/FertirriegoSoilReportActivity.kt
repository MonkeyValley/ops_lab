package com.amplemind.vivasmart.features.fertiriego

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import android.util.Log
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_DATE
import com.amplemind.vivasmart.features.fertiriego.adapter.FertirriegoSoilReportTab
import com.amplemind.vivasmart.features.fertiriego.fragment.SoilReportFragment
import com.amplemind.vivasmart.features.fertiriego.viewModel.FertirriegoHydroponicMonitoringViewModel
import com.github.badoualy.datepicker.DatePickerTimeline
import kotlinx.android.synthetic.main.activity_fertirriego_hydroponic_report.tabs_main
import kotlinx.android.synthetic.main.activity_fertirriego_hydroponic_report.timeline
import kotlinx.android.synthetic.main.activity_fertirriego_hydroponic_report.viewpager_main
import kotlinx.android.synthetic.main.activity_fertirriego_soil_report.*
import kotlinx.android.synthetic.main.categories_toolbar.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.*
import javax.inject.Inject

class FertirriegoSoilReportActivity : BaseActivityWithFragment() {

    companion object {
        val TAG = FertirriegoSoilReportActivity::class.simpleName
        val TAG_NAME = "$TAG.TagName"
        val LOT_ID = "$TAG.LotId"
        var LOT_NAME = "$TAG.LotName"
        var MODE = "$TAG.Mode"
    }

    var lotId: String = "0"

    lateinit var fragmentAdapter: FertirriegoSoilReportTab

    @Inject
    lateinit var viewModel: FertirriegoHydroponicMonitoringViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fertirriego_soil_report)

        lotId = intent?.getStringExtra(LOT_ID) ?: "0"
        val lotName = intent?.getStringExtra(LOT_NAME) ?: ""

        setupToolbar()
        loadDateTime()

        fragmentAdapter = FertirriegoSoilReportTab(supportFragmentManager, FERTIRRIEGO_DATE, lotId.toInt(), lotName)
        callTabFragment()
        tabs_main.setupWithViewPager(viewpager_main)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_fertirriego_soil_report
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = "" + intent?.getStringExtra(LOT_NAME) + "\n" + intent?.getStringExtra(TAG_NAME)
        lbl_track_title.text = "/ Fertirriego / " + intent?.getStringExtra(TAG_NAME) + " / " + intent?.getStringExtra(LOT_NAME) + " /"
        lbl_track_title.setTextColor(this.resources.getColor(R.color.white))
        icon_track_title.setColorFilter(this.resources.getColor(R.color.white))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun loadDateTime() {
        val date = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        var cal_begin = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        cal_begin.add(Calendar.DATE, -15)

        Log.e("date", "" + date.get(Calendar.DST_OFFSET))
        Log.e("cal_begin", "" + date.get(Calendar.DST_OFFSET))

        if (date.get(Calendar.DST_OFFSET) != 0) {
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH) + 1)
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) + "-" + cal_begin.get(Calendar.MONTH) + "-" + cal_begin.get(Calendar.DAY_OF_MONTH) + 1)
            timeline.setLastVisibleDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH) + 1)
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH) + 1)
            timeline.setSelectedDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH) + 1)
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH) + 1)
        } else {
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH))
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) + "-" + cal_begin.get(Calendar.MONTH) + "-" + cal_begin.get(Calendar.DAY_OF_MONTH))
            timeline.setLastVisibleDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH))
            timeline.setSelectedDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH))
        }

        val sti = date.get(Calendar.YEAR).toString()
        val month = if ((date.get(Calendar.MONTH) + 1) < 10) "0" + (date.get(Calendar.MONTH) + 1).toString() else (date.get(Calendar.MONTH) + 1).toString()
        val day = if ((date.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (date.get(Calendar.DAY_OF_MONTH)).toString() else (date.get(Calendar.DAY_OF_MONTH)).toString()
        FERTIRRIEGO_DATE = ((date.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        val stringYear = sti.subSequence(2, 4)
        timeline.setDateLabelAdapter { calendar,
                                       index ->
            Integer.toString(calendar[Calendar.MONTH] + 1) + "/" + stringYear
        }

        timeline.onDateSelectedListener = DatePickerTimeline.OnDateSelectedListener { year, month, day, index ->
            Log.d("dateSelected", "year:$year month:$month day:$day index: $index")
            val month = if ((month + 1) < 10) "0" + (month + 1).toString() else (month + 1).toString()
            val day = if ((day) < 10) "0" + (day).toString() else (day).toString()
            FERTIRRIEGO_DATE = ((year).toString() + "-" + month + "-" + day)

            val pos: Int = viewpager_main.currentItem
            val fragment: Fragment = fragmentAdapter.getRegisteredFragment(pos)!!
            (fragment as SoilReportFragment).loadDataReport()
        }
    }

    fun callTabFragment() {
        viewpager_main.adapter = fragmentAdapter
        viewpager_main.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                val fragment: Fragment = fragmentAdapter.getRegisteredFragment(position)!!
                (fragment as SoilReportFragment).loadDataReport()
            }
        })
        tabs_main.setupWithViewPager(viewpager_main)
    }

    override fun onBackPressed() {
        finish()
    }
}