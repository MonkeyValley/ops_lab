package com.amplemind.vivasmart.features.scannerbarcode.source

import android.graphics.Bitmap
import android.media.Image
import com.amplemind.vivasmart.features.scannerbarcode.source.FrameMetadata
import com.google.firebase.ml.common.FirebaseMLException
import java.nio.ByteBuffer

/** An inferface to process the images with different ML Kit detectors and custom image models.  */
interface VisionImageProcessor {

    /** Processes the images with the underlying machine learning models.  */
    @Throws(FirebaseMLException::class)
    fun process(data: ByteBuffer, frameMetadata: FrameMetadata)

    /** Processes the bitmap images.  */
    fun process(bitmap: Bitmap)

    /** Processes the images.  */
    fun process(bitmap: Image, rotation: Int)

    /** Stops the underlying machine learning model and release resources.  */
    fun stop()
}

