package com.amplemind.vivasmart.vo_core.repository.remote.downloaders

import android.content.res.Resources
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm.*
import com.amplemind.vivasmart.vo_core.utils.dateFromToday
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DownloaderHolder @Inject constructor(
        private val mCollaboratorDownloader: CollaboratorDownloader,
        private val mActivityDownloader: ActivityDownloader,
        private val mStageDownloader: StageDownloader,
        private val mActivityCodeDownloader: ActivityCodeDownloader,
        private val mActivityLogDownloader: ActivityLogDownloader,
        private val mGrooveDownloader: GrooveDownloader,
        private val mUnitDownloader: UnitDownloader,
        private val mClientDownloader: ClientDownloader,
        private val mCropDownloader: CropDownloader,
        private val mLotFertirriegoDownloader: LotFertirriegoDownloader,
        private val mProductionRangesDownloader: ProductionRangesDownloader,
        private val mMegasseDownloader: MegasseDownloader,
        private val mDailyPayDownloader: DailyPayDownloader,
        private val mProductionRangeDownloader: ProductionRangeDownloader
){

    fun setDateRange(days: Int) {
        val date = dateFromToday(days, "yyyy-MM-dd")
        mActivityCodeDownloader.fromDate = date
        mActivityLogDownloader.fromDate = date
    }

    @Suppress("UNCHECKED_CAST", "IMPLICIT_CAST_TO_ANY")
    fun getDownloaderForRequestType(requestType: RequestType) =
            when (requestType) {
                RequestType.MESSAGE         -> mMegasseDownloader
                RequestType.DAILYPAY        -> mDailyPayDownloader
                RequestType.COLLABORATOR    -> mCollaboratorDownloader
                RequestType.LOT_FERTIRRIEGO -> mLotFertirriegoDownloader
                RequestType.ACTIVITY        -> mActivityDownloader
                RequestType.STAGE           -> mStageDownloader
                RequestType.ACTIVITY_CODE   -> mActivityCodeDownloader
                RequestType.ACTIVITY_LOG    -> mActivityLogDownloader
                RequestType.PRODUCITON_RANGES -> mProductionRangesDownloader
                RequestType.PRODUCITON_RANGES_STAGE -> mProductionRangeDownloader
                RequestType.GROOVE          -> mGrooveDownloader
                RequestType.UNIT            -> mUnitDownloader
                RequestType.CLIENT          -> mClientDownloader
                RequestType.CROP            -> mCropDownloader
                else                        -> throw Resources.NotFoundException("No downloader found for type '${requestType.remoteName}'")
            } as Downloader<Any?, Any>

    @Suppress("UNCHECKED_CAST", "IMPLICIT_CAST_TO_ANY")
    fun getDownloaderForRequestTypeForce(requestType: RequestType) =
            when (requestType) {
                RequestType.MESSAGE         -> mMegasseDownloader
                RequestType.DAILYPAY        -> mDailyPayDownloader
                RequestType.COLLABORATOR    -> mCollaboratorDownloader
                RequestType.LOT_FERTIRRIEGO -> mLotFertirriegoDownloader
                RequestType.ACTIVITY        -> mActivityDownloader
                RequestType.STAGE           -> mStageDownloader
                RequestType.ACTIVITY_CODE   -> mActivityCodeDownloader
                RequestType.ACTIVITY_LOG    -> mActivityLogDownloader
                RequestType.PRODUCITON_RANGES -> mProductionRangesDownloader
                RequestType.PRODUCITON_RANGES_STAGE -> mProductionRangeDownloader
                RequestType.GROOVE          -> mGrooveDownloader
                else                        -> mMegasseDownloader
                    //throw Resources.NotFoundException("No downloader found for type '${requestType.remoteName}'")
            } as Downloader<Any?, Any>

    fun cleanUp() {
        RequestType.values().forEach {  requestType ->
            if (requestType != RequestType.UNKNOWN) {
                getDownloaderForRequestType(requestType).clearCache()
                getDownloaderForRequestType(requestType).clear()
            }
        }
    }

    fun cleanUpForce() {
        RequestType.values().forEach {  requestType ->
            if (requestType != RequestType.UNKNOWN) {
                getDownloaderForRequestTypeForce(requestType).clearCache()
                getDownloaderForRequestTypeForce(requestType).clear()
            }
        }
    }

    fun clearCache() {
        RequestType.values().forEach {  requestType ->
            if (requestType != RequestType.UNKNOWN) {
                getDownloaderForRequestType(requestType).clearCache()
            }
        }
    }

    fun clearCacheForce() {
        RequestType.values().forEach {  requestType ->
            if (requestType != RequestType.UNKNOWN) {
                getDownloaderForRequestTypeForce(requestType).clearCache()
            }
        }
    }

}