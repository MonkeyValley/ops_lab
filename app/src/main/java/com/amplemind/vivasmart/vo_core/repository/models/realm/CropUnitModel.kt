package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class CropUnitModel(
        @SerializedName("id")                       var id: Int = 0,
        @SerializedName("business_unit_names")  var businessUnitNames: String? = null,
        @SerializedName("cost")                 var cost: Double? = 0.0,
        @SerializedName("equivalent")           var equivalent: Double? = 0.0,
        @SerializedName("image")                var image: String? = null,
        @SerializedName("is_active")            var isActive: Boolean? = false,
        @SerializedName("name")                 var name: String? = null
) : RealmObject(), Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(businessUnitNames)
        parcel.writeValue(cost)
        parcel.writeValue(equivalent)
        parcel.writeString(image)
        parcel.writeValue(isActive)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CropUnitModel> {
        override fun createFromParcel(parcel: Parcel): CropUnitModel {
            return CropUnitModel(parcel)
        }

        override fun newArray(size: Int): Array<CropUnitModel?> {
            return arrayOfNulls(size)
        }
    }
}
