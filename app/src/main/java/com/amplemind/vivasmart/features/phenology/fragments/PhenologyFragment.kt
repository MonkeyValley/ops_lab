package com.amplemind.vivasmart.features.phenology.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.*
import com.amplemind.vivasmart.features.phenology.adapters.LotsInLinesPhenologyAdapter
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyLot
import com.amplemind.vivasmart.features.phenology.viewmodel.LotPhenologyItemViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.PhenologyFragmentViewModel
import com.google.gson.Gson
import dagger.android.DispatchingAndroidInjector
import io.reactivex.Observable
import kotlinx.android.synthetic.main.content_selection_of_lots_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@SuppressLint("Registered")
class PhenologyFragment : BaseFragment(), LotsInLinesPhenologyAdapter.Listener {

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    private var mListener: LotsInLinesPhenologyAdapter.Listener? = null

    @Inject
    lateinit var viewModel: PhenologyFragmentViewModel

    private var mAdapter = LotsInLinesPhenologyAdapter()

    companion object {
        fun ifConected(): Boolean {
            return if (PhenologyFragment().hasInternet())
                true
            else {
                Toast.makeText(PhenologyFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

    fun newInstance(title: String, itemCode: String): PhenologyFragment {
        val args = Bundle()
        args.putString("Title", title)
        args.putString("itemCode", itemCode)

        val fragment = PhenologyFragment()
        fragment.arguments = args
        return fragment
    }

    var itemCode: String? = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_selection_of_lots_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        itemCode = arguments?.getString("itemCode")

        setupRecycler()

        when(itemCode){
            "1"->lbl_track_title.text = "/ Fenología / Variedades comerciales /"
            "0"->lbl_track_title.text = "/ Fenología / Variedades de pruebas /"
        }

        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getLots(itemCode!!.toInt())
        viewModel.onSuccessRequest_lots().subscribe(this::addElements).addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun getData() {
        viewModel.getLots(itemCode!!.toInt())
    }

    fun addElements(data: List<LotPhenologyItemViewModel>) {
        mAdapter.addElements(data)
    }

    private fun clickItemLot() {
        mAdapter.onClickItem()
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe(this::showLotDetail).addTo(subscriptions)
    }

    private fun showLotDetail(item: LotPhenologyItemViewModel) {
        if(ifConected()) {
            val support = activity?.supportFragmentManager
            val commit = support?.beginTransaction()
                    ?.replace(R.id.container, PhenologyVarietiesFragment().newInstance("PhenologyVarietiesFragment", item.lot_id!!, item.lot_name!!, item.stage_id!!, itemCode!!.toInt()))
                    ?.addToBackStack("PhenologyVarietiesFragment")
                    ?.commit()
        }
    }

    private fun setupRecycler() {
        rv_selection_lots.hasFixedSize()
        rv_selection_lots.layoutManager = GridLayoutManager(context, getSpanCount(context!!))
        rv_selection_lots.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

        mAdapter.listener = this
        rv_selection_lots.adapter = mAdapter
        clickItemLot()
    }

    override fun onGetCounterValue(position: Int): Observable<Int>? {
        return mListener?.onGetCounterValue(position)
    }
}
