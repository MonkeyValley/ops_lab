package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName

data class ReceptionQualityCarryOrderModel (
        @SerializedName("id")       val id: Long,
        @SerializedName("folio")    val folio: String,
        @SerializedName("lot_name") val lotName: String,
        @SerializedName("quality")  val quality: Double,
        @SerializedName("table")    val tables: String
)

