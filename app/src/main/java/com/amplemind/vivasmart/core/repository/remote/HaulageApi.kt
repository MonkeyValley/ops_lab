package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.core.repository.request.CarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.HaulageSignRequest
import com.amplemind.vivasmart.core.repository.response.VehiclesResponse
import io.reactivex.Observable
import retrofit2.http.*

interface HaulageApi {

    @PUT("/v2/carry_order/sign/{carryOrder}")
    @Headers("Content-Type: application/json")
    fun haulageAssignSigns(
            @Header("Authorization") authentication: String,
            @Path("carryOrder") carryOrder: Int,
            @Body body: HaulageSignRequest
    ): Observable<Any>

    @POST("/v2/carry_order")
    @Headers("Content-Type: application/json")
    fun createCarryOrder(
            @Header("Authorization") authentication: String,
            @Body body: CarryOrderRequest
    ): Observable<CarryOrderModel>

    @GET("/v2/vehicle_unit")
    @Headers("Content-Type: application/json")
    fun getVehicles(
            @Header("Authorization") authentication: String
    ): Observable<VehiclesResponse>

}