package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_13 : MigrationExecutable {
    override fun run(realm: DynamicRealm) {
        createMipeMonitoringModel(realm)
    }

    private fun createMipeMonitoringModel(realm: DynamicRealm){
        val findingsSchema = realm.schema.create("FindingsModel")
                .addField("plagueId", Int::class.javaObjectType)
                .addField("finding", Int::class.javaObjectType)

        val plantsMonitoringSchema = realm.schema.create("PlantsMonitoringModel")
                .addField("plantNo", Int::class.javaObjectType)
                .addField("quadrant", Int::class.javaObjectType)
                .addRealmListField("findings", findingsSchema)

        realm.schema.create("MipeMonitoringModel")
                .addField("lotId", Int::class.javaObjectType)
                .addField("table", Int::class.javaObjectType)
                .addField("grooveNo", Int::class.javaObjectType)
                .addField("date", String::class.javaObjectType)
                .addField("week", Int::class.javaObjectType)
                .addField("finished", Boolean::class.javaObjectType)
                .addRealmListField("plants", plantsMonitoringSchema)
    }

    override fun drop(realm: DynamicRealm) {

    }


}