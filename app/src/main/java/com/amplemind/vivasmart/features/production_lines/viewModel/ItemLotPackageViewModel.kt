package com.amplemind.vivasmart.features.production_lines.viewModel

import com.amplemind.vivasmart.core.repository.model.LotsPackageResultResponse

class ItemLotPackageViewModel(val item: LotsPackageResultResponse) {

    val name = item.name.toUpperCase()
    val id = item.id
    val stage_id = item.stage_id
    var status = false

    fun changeStatus(){
        this.status = !this.status
    }

}
