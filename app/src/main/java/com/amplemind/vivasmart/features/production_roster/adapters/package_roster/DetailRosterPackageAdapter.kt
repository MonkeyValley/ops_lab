package com.amplemind.vivasmart.features.production_roster.adapters.package_roster

//import com.amplemind.vivasmart.features.production_roster.UpdateResponse
import androidx.core.content.ContextCompat
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.production_roster.ItemRosterDetailViewModel
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import io.reactivex.subjects.BehaviorSubject

class DetailRosterPackageAdapter(val list: MutableList<RosterDetailPackageViewModel>) :
        ExpandableRecyclerViewAdapter<DetailRosterPackageAdapter.DetailRosterPackageViewHolder, DetailRosterPackageAdapter.ItemDetailRosterViewHolder>(list), ControlAdapterPackage {

    private val onClickReport = BehaviorSubject.create<ItemRosterDetailViewModel>()


    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int): DetailRosterPackageViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.roster_detail_title, parent, false)
        return DetailRosterPackageViewHolder(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int): ItemDetailRosterViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_roster_detail_package, parent, false)
        return ItemDetailRosterViewHolder(view)
    }

    override fun onBindChildViewHolder(holder: ItemDetailRosterViewHolder?, flatPosition: Int, group: ExpandableGroup<*>?, childIndex: Int) {
        val itemDetail = (group as RosterDetailPackageViewModel).items[childIndex]
        holder?.bind(itemDetail, flatPosition)
    }

    override fun onBindGroupViewHolder(holder: DetailRosterPackageViewHolder?, flatPosition: Int, group: ExpandableGroup<*>?) {
        holder?.bind(group)
    }

    override fun onNotifyItemChanged(position: Int) {
        notifyItemChanged(position)
    }

    override fun onNotifyDataSetChange() {
        notifyDataSetChanged()
    }

    override fun onRemoveItem(position: Int) {
        e("Remove", "Position es $position")
    }

    fun onClickReport(): BehaviorSubject<ItemRosterDetailViewModel> {
        return onClickReport
    }

    /*
    fun onChangeItem(item: UpdateResponse) {
        list.forEach { report ->
            val selected = report.reports.filter { it.id_log == item.idLog }
            if (selected.isNotEmpty()) {
                selected.first().changeValues(item.cant, (item.time / 60).toInt())
                report.updateTitle()
            }
        }
        notifyItemChanged(item.position)
    }
     */

    fun getActivitiesId(): List<Int> {
        val allIds = mutableListOf<Int>()

        list.forEach { report ->
            val result = report.reports.asSequence().filter { !it.status }.map { it.id_log!! }.toList()
            result.forEach {
                allIds.add(it)
            }
        }

        return allIds
    }


    inner class DetailRosterPackageViewHolder constructor(val view: View) : GroupViewHolder(view) {

        private val title: TextView = view.findViewById(R.id.tv_title)
        private val tv_hrs_total: TextView = view.findViewById(R.id.tv_hrs_total)
        private val tv_unit_total: TextView = view.findViewById(R.id.tv_unit_total)
        private val tv_hrse_total: TextView = view.findViewById(R.id.tv_hrse_total)
        private val tv_money_total: TextView = view.findViewById(R.id.tv_money_total)
        private val tv_type_act: TextView = view.findViewById(R.id.tv_type_act)

        fun bind(group: ExpandableGroup<*>?) {
            setupData(group)

            (group as RosterDetailPackageViewModel).getNotifyChange().subscribe {
                group.setup()
                setupData(group)
            }
        }

        private fun setupData(group: ExpandableGroup<*>?){
            title.text = group?.title
            tv_hrs_total.text = (group as RosterDetailPackageViewModel).totalTime
            tv_unit_total.text = group.totalUnit
            tv_hrse_total.text = group.extraTime
            tv_money_total.text = group.totalMoney
            tv_type_act.text = group.type

            changeColor(title, tv_hrs_total, tv_unit_total, tv_hrse_total, tv_money_total,status = group.status)
            changeColorSync(title, tv_hrs_total, tv_unit_total, tv_hrse_total, tv_money_total,status = group.statusSync)

            group.reports.map { it.headerPosition = adapterPosition }
        }

        private fun changeColor(vararg textViews: TextView,status : Boolean){
            if (status) {
                textViews.forEach { textView ->
                    textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colorPrimary))
                }
            } else {
                textViews.forEach { textView ->
                    textView.setTextColor(ContextCompat.getColor(textView.context, R.color.textDisabledColor))
                }
            }
        }
        private fun changeColorSync(vararg textViews: TextView,status : Boolean?){
            if (status == null){
                return
            }
            if (status) {
                textViews.forEach { textView ->
                    textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colorPrimary))
                }
            } else {
                textViews.forEach { textView ->
                    textView.setTextColor(ContextCompat.getColor(textView.context, R.color.redAlert))
                }
            }
        }

    }

    inner class ItemDetailRosterViewHolder(val view: View) : ChildViewHolder(view) {

        private val tv_presentation: TextView = view.findViewById(R.id.tv_presentation)
        private val tv_quantity: TextView = view.findViewById(R.id.tv_quantity)
        private val tv_money: TextView = view.findViewById(R.id.tv_money)
        private val tv_time_pa: TextView = view.findViewById(R.id.tv_time_pa)

        fun bind(item: ItemRosterDetailViewModel, position: Int) {
            tv_presentation.text = item.packing_name
            tv_quantity.text = item.cantFormat()
            tv_money.text = item.formatMoneyPackage()
            tv_time_pa.text = item.hrs
            if (adapterPosition > -1 && !(item.status)) {
                view.setOnClickListener {
                    item.setPosition(item.headerPosition)
                    onClickReport.onNext(item)
                }
            }
        }
    }

}