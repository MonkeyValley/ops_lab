package com.amplemind.vivasmart.features.phenology.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.mipe.MipeMonitoringLotsActivity
import com.amplemind.vivasmart.features.phenology.PhenologyActivitiesActivity
import com.amplemind.vivasmart.features.phenology.PhenologyActivity
import com.amplemind.vivasmart.features.phenology.adapters.ActivitiesPhenologyAdapter
import com.amplemind.vivasmart.features.phenology.viewmodel.MenuPhenologyActivitiesViewModel
import com.amplemind.vivasmart.features.pollination.PollinationLotsActivity
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModel
import kotlinx.android.synthetic.main.content_payroll_activities_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

@SuppressLint("Registered")
class MenuActivitiesPhenologyFragment : BaseFragment() {

    @Inject
    lateinit var viewModelMenu: MenuPhenologyActivitiesViewModel

    @Inject
    lateinit var prefer: UserAppPreferences

    private var section: String? = null

    fun newInstance(title: String): MenuActivitiesPhenologyFragment {
        val args = Bundle()
        args.putString("Title", title)

        val fragment = MenuActivitiesPhenologyFragment()
        fragment.arguments = args
        return fragment
    }

    fun ifConected(): Boolean {
        return if (hasInternet())
            true
        else {
            Toast.makeText(MenuActivitiesPhenologyFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_payroll_activities_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lbl_track_title.text = "/ Fenología /"
        initRecycler()
        viewModelMenu.setupData(arguments!!)
        viewModelMenu.getToolbarTitle().subscribe {
            section = it
            (activity as PhenologyActivitiesActivity).supportActionBar!!.title = it
            viewModelMenu.loadMenuActivities().addTo(subscriptions)
            viewModelMenu.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)

        }.addTo(subscriptions)
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_menu_activities_payroll.adapter as ActivitiesPhenologyAdapter).addElements(activities)
        (rv_menu_activities_payroll.adapter as ActivitiesPhenologyAdapter).onClickItem().subscribe(this::clickActivityPhenology).addTo(subscriptions)
    }

    private fun clickActivityPhenology(item: ItemActivitiesPayrollViewModel) {
        // Dif de Polinizacion
        when(item.id){
            "0" -> callNewActivity(item.id)
            "1" -> callNewActivity(item.id)
            "2" -> {
                startActivity(Intent(context!!, PollinationLotsActivity::class.java).apply {
                    putExtra(PollinationLotsActivity.PARAM_TYPE, "3")
                    putExtra(PollinationLotsActivity.PARAM_TITTLE, "Polinización")
                })
            }
            else -> Toast.makeText(context, "Próximamente...", Toast.LENGTH_SHORT).show()
        }
    }

    private fun callNewActivity(id: String) {
        val intent = Intent(activity, PhenologyActivity::class.java)
        intent.putExtra("itemCode", id)
        startActivity(intent)
    }

    private fun callNewFragment(flow: String) {
        //TODO: saveServerResponse flow
        prefer.flow = flow
        if (section == "Calidad") {
//            (activity as AppCompatActivity)
//                    .supportFragmentManager
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
//                    .add(R.id.container_payroll, MenuaActivitiesQualityProductionFragment().newInstance("Producción"))
//                    .addToBackStack(MenuaActivitiesQualityProductionFragment.TAG)
//                    .commit()
        } else {
//            (activity as AppCompatActivity)
//                    .supportFragmentManager
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
//                    .add(R.id.container_payroll, getFragment(flow))
//                    .addToBackStack(MenuActivitiesProductionFragment.TAG)
//                    .commit()
        }
    }

    private fun initRecycler() {
        rv_menu_activities_payroll.hasFixedSize()
        rv_menu_activities_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_activities_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_activities_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_activities_payroll.adapter = ActivitiesPhenologyAdapter()
    }

    companion object {
        val TAG = MenuActivitiesPhenologyFragment::class.java.simpleName
    }

}
