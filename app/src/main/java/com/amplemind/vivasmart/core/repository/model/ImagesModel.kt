package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class ImagesModel(
        @SerializedName("name") var name: String
)

