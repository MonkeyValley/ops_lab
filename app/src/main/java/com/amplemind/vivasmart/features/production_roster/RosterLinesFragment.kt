package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.snackWithoutInternetMessage
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.utils.FRAGMENT_LINES
import com.amplemind.vivasmart.features.production_lines.fragment.LotsInLinesFragment
import com.amplemind.vivasmart.features.production_roster.adapters.RosterLinesAdapter
import com.amplemind.vivasmart.features.production_roster.viewModels.ItemRosterLinesViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.RosterLinesViewModel
import kotlinx.android.synthetic.main.content_lines_roster_fragment.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RosterLinesFragment : BaseFragment() {

    companion object {
        val TAG = RosterLinesFragment::class.java.simpleName
        fun newInstance() = RosterLinesFragment()
    }

    @Inject
    lateinit var mediator: Mediator

    @Inject
    lateinit var viewModel: RosterLinesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_lines_roster_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()

        getLines()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = getString(R.string.title_lines_fragment)

        viewModel.onSearchLocalLines().subscribe(this::onLoadLocalData).addTo(subscriptions)
        viewModel.onGetLines().subscribe(this::setDataAdapter).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        (rv_lines_roster.adapter as RosterLinesAdapter).onClickItem().throttleFirst(2,TimeUnit.SECONDS).subscribe(this::clickItem).addTo(subscriptions)

    }

    private fun getLines() {
        if (hasInternet()) {
            viewModel.getLines().addTo(subscriptions)
        } else {
            onLoadLocalData(true)
        }
    }

    private fun onLoadLocalData(result: Boolean) {
        if (result) {
            view_lines.snackWithoutInternetMessage()
            viewModel.getLocalLines().addTo(subscriptions)
        }
    }

    private fun setDataAdapter(activities: List<ItemRosterLinesViewModel>) {
        (rv_lines_roster.adapter as RosterLinesAdapter).addElements(activities)
    }

    private fun clickItem(item: ItemRosterLinesViewModel) {
        mediator.bundleItemLinesPackage = item.id_line

        if (item.actual_stage != null) {
            startActivityForResult((Intent(activity, TimerLinesActivity::class.java)
                    .putExtra("name_line", item.name)
                    .putExtra("line_id", item.id_line)
                    .putExtra("lot_id", item.actual_stage)),
                    FRAGMENT_LINES)
        } else {
            showLotInLines(item.name)
        }
    }

    private fun showLotInLines(name: String) {
        (activity as AppCompatActivity)
                .supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                .add(R.id.container_payroll, LotsInLinesFragment.newInstance(name))
                .addToBackStack(LotsInLinesFragment.TAG)
                .commit()
    }

    private fun initRecycler() {
        rv_lines_roster.hasFixedSize()
        rv_lines_roster.layoutManager = LinearLayoutManager(context!!)
        rv_lines_roster.itemAnimator = DefaultItemAnimator()
        rv_lines_roster.adapter = RosterLinesAdapter()
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = getString(R.string.title_packing_fragment)
    }

}
