package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import com.amplemind.vivasmart.vo_core.repository.FinalizeUserTimerRepository
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getPayment
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.utils.formatPayment
import javax.inject.Inject

class FinalizeUserTimerViewModel @Inject constructor(
        private val repository: FinalizeUserTimerRepository
){

    lateinit var activityLogUuid: String
    lateinit var stageUuid: String

    lateinit var activityLog: ActivityLogModel

    var isTraining: Boolean
        set(value) {
            activityLog.isTraining = value
        }
        get() = activityLog.isTraining

    var isPractice: Boolean
        set(value) {
            activityLog.isPractice = value
        }
        get() = activityLog.isPractice

    var isControl: Boolean
        set(value) {
            activityLog.isControl = value
        }
        get() = activityLog.isControl

    fun loadActivityLog() =
            repository.loadActivityLog(activityLogUuid)
                    .doOnSuccess { activityLog: ActivityLogModel ->
                        this.activityLog = activityLog
                    }


    fun getFormattedPayment() =
            formatPayment(activityLog.getPayment())

}