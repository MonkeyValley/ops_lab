package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.vo_core.repository.HarvestValidationRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.HarvestValidationModel
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class HarvestValidationViewModel @Inject constructor (
        private val mRepository: HarvestValidationRepository
) {

    var carryOrderId: Long = -1
    var hasHarvestQuality: Boolean = false

    val onValidationLoaded = PublishSubject.create<HarvestValidationModel>()

    fun cleanUp() {
        mRepository.cleanUp()
    }

    fun loadHarvestQuality(): Observable<List<HarvestQualitySettingViewModel>> =
            if (hasHarvestQuality) {
                mRepository.getHarvestValidation(carryOrderId)
                        .doOnNext { validation ->
                            onValidationLoaded.onNext(validation)
                        }
                        .map {validation ->
                            validation.issues.map { issue ->
                                HarvestQualitySettingViewModel(issue)
                            }
                        }
            }
            else {
                mRepository.loadCriteria()
                        .map { criteria ->
                            criteria.map { issue ->
                                HarvestQualitySettingViewModel(issue)
                            }
                        }
            }

    fun createHarvestValidation(settings: List<HarvestQualitySettingViewModel>, comments: String): Single<HarvestValidationModel> {
        val validationSettings = settings.map {setting ->
            HarvestValidationModel.IssueModel(
                    setting.id,
                    setting.isOk.get()
            )
        }

        val validation = HarvestValidationModel(carryOrderId, validationSettings, comments = comments)

        return mRepository.createHarvestValidation(validation)
    }
}