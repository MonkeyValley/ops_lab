package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.remote.models.HarvestValidationCarryOrderModel

class HarvestValidationCarryOrderViewModel (
        val carryOrder: HarvestValidationCarryOrderModel
){

    val code: String
        get() = carryOrder.carryOrderId.toString()

    val lotName: String
        get() = carryOrder.lotName

    val boxCount: String
        get() = "%.1f".format(carryOrder.boxNo)

    val hasHarvestQuality: Boolean
        get() = carryOrder.hasHarvestQuality ?: false

    val isHarvestQualityOk: Boolean
        get() = carryOrder.harvestQualityOk ?: false

}