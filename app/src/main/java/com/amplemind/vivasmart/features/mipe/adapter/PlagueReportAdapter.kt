package com.amplemind.vivasmart.features.mipe.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemPlagueReportMipeBinding
import com.amplemind.vivasmart.features.mipe.viewModel.ItemPlagueViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.MipeMonitoringModel
import io.reactivex.subjects.PublishSubject
import io.realm.Realm

class PlagueReportAdapter : (RecyclerView.Adapter<PlagueReportAdapter.PlagueViewHolder>)() {

    var lotId = 0
    var groove = 0
    var week = 0
    var table = "0"
    var plant = 0

    fun setData(lotId: Int, groove: Int, week: Int, table: String, plant: Int) {
        this.lotId = lotId
        this.groove = groove
        this.week = week
        this.table = table
        this.plant = plant
    }

    private var list = listOf<ItemPlagueViewModel>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    fun addElements(data: List<ItemPlagueViewModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlagueViewHolder {
        val binding = ItemPlagueReportMipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PlagueViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: PlagueViewHolder, position: Int) {
        holder.bind(list[position])

    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class PlagueViewHolder(private val binding: ItemPlagueReportMipeBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ItemPlagueViewModel

        fun bind(item: ItemPlagueViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.tvPlagueName.text = item.name

            Realm.getDefaultInstance().use {
                val result = it
                        .where(MipeMonitoringModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .equalTo("week", week)
                        .endGroup()
                        .findAll()

                if (result.size != 0) {
                    var plantsTotal: Double = 0.0
                    var countPlantRevised: Double = 0.0
                    result.forEach{ mipe->
                        plantsTotal += mipe.plants!!.size.toDouble()
                        mipe.plants!!.forEach  { plant ->
                            plant.findings!!.forEach { find ->
                                if(find.finding != null && find.plagueId == item.id) countPlantRevised += find.finding!!
                            }
                        }
                    }
                    if(countPlantRevised != 0.0) binding.tvWeek1.text = String.format("%.2f", (countPlantRevised/plantsTotal))
                    //else binding.tvWeek1.text = "0"
                }

                val result2 = it
                        .where(MipeMonitoringModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .equalTo("week", week-1)
                        .endGroup()
                        .findAll()

                if (result2.size != 0) {
                    var plantsTotal: Double = 0.0
                    var countPlantRevised: Double = 0.0
                    result2.forEach{ mipe->
                        plantsTotal += mipe.plants!!.size.toDouble()
                        mipe.plants!!.forEach  { plant ->
                            plant.findings!!.forEach { find ->
                                if(find.finding != null && find.plagueId == item.id) countPlantRevised += find.finding!!
                            }
                        }
                    }
                    if(countPlantRevised != 0.0) binding.tvWeek2.text = String.format("%.2f", (countPlantRevised/plantsTotal))
                    //else binding.tvWeek2.text = "0"
                }

                val result3 = it
                        .where(MipeMonitoringModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .equalTo("week", week-2)
                        .endGroup()
                        .findAll()

                if (result3.size != 0) {
                    var plantsTotal: Double = 0.0
                    var countPlantRevised: Double = 0.0
                    result3.forEach{ mipe->
                        plantsTotal += mipe.plants!!.size.toDouble()
                        mipe.plants!!.forEach  { plant ->
                            plant.findings!!.forEach { find ->
                                if(find.finding != null && find.plagueId == item.id) countPlantRevised += find.finding!!
                            }
                        }
                    }
                    if(countPlantRevised != 0.0) binding.tvWeek3.text = String.format("%.2f", (countPlantRevised/plantsTotal))
                    //else binding.tvWeek2.text = "0"
                }
            }
        }
    }


}
