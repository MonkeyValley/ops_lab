package com.amplemind.vivasmart.vo_core.repository.models.realm

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class ProductionRangesDataModel(
        @PrimaryKey @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("week_0") var week0: ProductionRangeWeekModel? = null,
        @SerializedName("week_1") var week1: ProductionRangeWeekModel? = null,
        @SerializedName("week_2") var week2: ProductionRangeWeekModel? = null
) : RealmObject()

open class ProductionRangeWeekModel(
        @SerializedName("start_date") var startDate: String? = null,
        @SerializedName("end_date") var endDate: String? = null,
        @SerializedName("production_vars") var productionVars: RealmList<ProductionVar>? = RealmList()
) : RealmObject()

open class ProductionVar(
        @SerializedName("id") var id: Int? = 0,
        @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("week_number") var weekNumer: Int? = 0,
        @SerializedName("max_val") var maxVal: String? = null,
        @SerializedName("min_val") var minVal: String? = null,
        @SerializedName("production_var_id") var productionVarId: Int? = null,
        @SerializedName("production_var_option_id") var productionVarOptionId: Int? = null,
        @SerializedName("uuid") var uuid: String? = "",
        @SerializedName("value") var value: String? = null
) : RealmObject()