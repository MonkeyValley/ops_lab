package com.amplemind.vivasmart.features.mipe.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.util.SparseArray
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService
import com.amplemind.vivasmart.features.mipe.fragment.SelectGrooveFragment

class MipeTableTab (fm: FragmentManager, private val week: Int, private val lotId: Int, private val lotName: String) : FragmentPagerAdapter(fm) {

    private val registeredFragments = SparseArray<Fragment>()

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                val fragment = SelectGrooveFragment().newInstance(week, lotId, "1", lotName)
                registeredFragments.put(position, fragment)
                return fragment
            }
            1 -> {
                val fragment = SelectGrooveFragment().newInstance(week, lotId, "2", lotName)
                registeredFragments.put(position, fragment)
                return fragment
            }
            2 -> {
                val fragment = SelectGrooveFragment().newInstance(week, lotId, "3", lotName)
                registeredFragments.put(position, fragment)
                return fragment
            }
            3 -> {
                val fragment = SelectGrooveFragment().newInstance(week, lotId, "4", lotName)
                registeredFragments.put(position, fragment)
                return fragment
            }
            4 -> {
                val fragment = SelectGrooveFragment().newInstance(week, lotId, "5", lotName)
                registeredFragments.put(position, fragment)
                return fragment
            }
            else ->  {
                val fragment = SelectGrooveFragment().newInstance(week, lotId, "6", lotName)
                registeredFragments.put(position, fragment)
                return fragment
            }
        }
    }

    override fun getCount(): Int {
        return FertirriegoService().getTableCountPerLot(lotId, "")
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Tabla 1"
            1 -> "Tabla 2"
            2 -> "Tabla 3"
            3 -> "Tabla 4"
            4 -> "Tabla 5"
            else -> "Tabla 6"
        }
    }

    fun getRegisteredFragment(position: Int): Fragment? {
        return registeredFragments[position]
    }
}