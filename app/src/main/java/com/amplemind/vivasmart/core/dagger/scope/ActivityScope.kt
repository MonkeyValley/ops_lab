package com.amplemind.vivasmart.core.base.dagger.scope

import javax.inject.Scope

/**
 * Created by
 *          amplemind on 6/29/18.
 */

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope