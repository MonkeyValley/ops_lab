package com.amplemind.vivasmart.core.repository.request

import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel
import com.google.gson.annotations.SerializedName

data class CarryOrderRequest(@SerializedName("business_unit_id") val businessUnitId: Int,
                             @SerializedName("lot_id") val lotId: Int,
                             @SerializedName("crop_id") val cropId: Int,
                             @SerializedName("table") val table: List<Int>,
                             @SerializedName("variety_box") val boxCounts: List<VarietyBoxCount>,
                             @SerializedName("second_quality_box") val secondQualityCount: Double,
                             @SerializedName("box_no") val boxTotal: Double,
                             @SerializedName("valid_box_no") val boxTotalValid: Double,
                             @SerializedName("temperature") val temperature: Double,
                             @SerializedName("collaborator_id") val collaboratorId: Int?,
                             @SerializedName("vehicle_unit_id") val vehicleId: Int,
                             @SerializedName("user_id") val userId: Int,
                             @SerializedName("comment") val comments: String
)

data class VarietyBoxCount(
        @SerializedName("variety_id")   val varietyId: Long,
        @SerializedName("box_no")       var boxCount: Double,
        @SerializedName("valid_box_no")       var boxCountValid: Double,
        @SerializedName("variety")      var variety: VarietyModel? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readParcelable(VarietyModel::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(varietyId)
        parcel.writeDouble(boxCount)
        parcel.writeDouble(boxCountValid)
        parcel.writeParcelable(variety, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<VarietyBoxCount> {
        override fun createFromParcel(parcel: Parcel): VarietyBoxCount {
            return VarietyBoxCount(parcel)
        }

        override fun newArray(size: Int): Array<VarietyBoxCount?> {
            return arrayOfNulls(size)
        }
    }
}

