package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class StagesModel(
        @SerializedName("code") val code: Int?,
        @SerializedName("crop") val crop: CropStage,
        @SerializedName("crop_id") val cropId: Int,
        @SerializedName("cycle") val cycle: Cycle,
        @SerializedName("cycle_id") val cycleId: Int,
        @SerializedName("density") val density: Int,
        @SerializedName("id") val id: Int,
        @SerializedName("is_active") val isActive: Boolean,
        @SerializedName("lot_id") val lotId: Int,
        @SerializedName("plant_date") val plantDate: String,
        @SerializedName("plants_per_groove") val plantsPerGroove: Int
)

@Entity
data class CropStage(
        @SerializedName("id") @ColumnInfo(name = "crop_id_primary") @PrimaryKey(autoGenerate = false) val id: Int,
        @SerializedName("crop_type_id") val cropTypeId: Int,
        @SerializedName("graft_name") val graftName: String?,
        @SerializedName("is_active") val isActive: Boolean,
        @SerializedName("is_graft") val isGraft: Boolean,
        @SerializedName("name") @ColumnInfo(name = "crop_stage_name") val name: String,
        @SerializedName("presentation_names") val presentationNames: String,
        @SerializedName("variety") val variety: String,
        @Embedded @SerializedName("crop_type") val cropType: CropType?
)

data class Cycle(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String
)

@Entity
data class CropType(
        @SerializedName("id") @ColumnInfo(name = "crop_type_id") @PrimaryKey(autoGenerate = false) val id: Int,
        @SerializedName("name") @ColumnInfo(name = "crop_type_name") val name: String
)