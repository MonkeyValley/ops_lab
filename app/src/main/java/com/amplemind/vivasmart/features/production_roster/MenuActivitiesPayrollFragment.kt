package com.amplemind.vivasmart.features.production_roster

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.production_roster.fragments.GeneralRosterFragment
import com.amplemind.vivasmart.features.production_roster.viewModels.MenuaActivitiesQualityProductionFragment
import kotlinx.android.synthetic.main.content_payroll_activities_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

@SuppressLint("Registered")
class MenuActivitiesPayrollFragment : BaseFragment(){

    @Inject
    lateinit var viewModel: PayrollActivitiesViewModel

    @Inject
    lateinit var prefer : UserAppPreferences

    private var section: String? = null

    fun newInstance(title: String): MenuActivitiesPayrollFragment {
        val args = Bundle()
        args.putString("Title", title)

        val fragment = MenuActivitiesPayrollFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_payroll_activities_fragment, container, false)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = arguments!!.getString("Title")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()


        val currentView = arguments!!.getString("Title")
        when(currentView){
            "Nómina"->lbl_track_title.text = "/ Nómina /"
            "Calidad"->lbl_track_title.text = "/ Calidad /"
        }

        viewModel.setupData(arguments!!)

        viewModel.getToolbarTitle().subscribe {

            section = it

            (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = it

            viewModel.loadMenuActivitiesPayroll(it == "Nómina").addTo(subscriptions)

            viewModel.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)

        }.addTo(subscriptions)
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_menu_activities_payroll.adapter as ActivitiesPayrollAdapter).addElements(activities)

        (rv_menu_activities_payroll.adapter as ActivitiesPayrollAdapter).onClickItem().subscribe(this::clickActivityPayroll).addTo(subscriptions)
    }

    /**
     *  @param item click item activity
     *
     *  start of the different flows
     */
    private fun clickActivityPayroll(item : String) {
        if (section == "Calidad") {
            callNewActivity(item)
        }else{
            callNewFragment(item)
        }
    }

    /**
     *  call new activity
     */
    private fun callNewActivity(flow : String) {
        if (flow == "Empaque") {
            (activity as AppCompatActivity)
                    .supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                    .add(R.id.container_payroll, MenuActivitiesProductionPacking.newInstance())
                    .addToBackStack(MenuActivitiesProductionPacking.TAG)
                    .commit()
        } else {
            callNewFragment(flow)
        }
    }

    /**
     *  get fragment in diferent flow
     */
    private fun getFragment(flow: String) : Fragment {
        return if (flow == "Empaque") {
            RosterLinesActivitiesFragment.newInstance()
        } else if(flow == "General") {
            GeneralRosterFragment.newInstance()
        } else {
            MenuActivitiesProductionFragment().newInstance()
        }
    }

    /**
     *  call transaction with new fragment
     */
    private fun callNewFragment(flow: String) {
        //TODO: saveServerResponse flow
        prefer.flow = flow
        if (section == "Calidad") {
            (activity as AppCompatActivity)
                    .supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                    .add(R.id.container_payroll, MenuaActivitiesQualityProductionFragment().newInstance("Producción"))
                    .addToBackStack(MenuaActivitiesQualityProductionFragment.TAG)
                    .commit()
        } else {
            (activity as AppCompatActivity)
                    .supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                    .add(R.id.container_payroll, getFragment(flow))
                    .addToBackStack(MenuActivitiesProductionFragment.TAG)
                    .commit()
        }
    }

    private fun initRecycler() {
        rv_menu_activities_payroll.hasFixedSize()
        rv_menu_activities_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_activities_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_activities_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_activities_payroll.adapter = ActivitiesPayrollAdapter()
    }

    companion object {
        val TAG = MenuActivitiesPayrollFragment::class.java.simpleName
    }

}
