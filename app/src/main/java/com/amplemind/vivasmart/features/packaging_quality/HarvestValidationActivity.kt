package com.amplemind.vivasmart.features.packaging_quality

import android.os.Bundle
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.remote.models.HarvestValidationCarryOrderModel
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.packaging_quality.fragment.HarvestValidationCarryOrderListFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.HarvestValidationFragment
import kotlinx.android.synthetic.main.activity_harvest_validation.*
import javax.inject.Inject

class HarvestValidationActivity : BaseActivityWithFragment() {

    @Inject
    lateinit var mEventBus: EventBus

    private val mCarryOrderListFragmentTag = "${HarvestValidationActivity::class.java.name}.${HarvestValidationCarryOrderListFragment::class.java.simpleName}"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_harvest_validation)

        setUpCallbacks()
        setUpUI()
        setUpUICallbacks()
    }

    private fun setUpCallbacks() {
        mEventBus.observe(this::onValidationFinished).addTo(subscriptions)
        mEventBus.observe(this::onSetToolbarTitleEvent).addTo(subscriptions)
    }

    private fun setUpUICallbacks() {
        mEventBus.observe(this::onCarryOrderClicked).addTo(subscriptions)
    }

    private fun setUpUI() {
        setupToolbar()
        showCarryOrderListFragment()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_harvest_validation.findViewById(R.id.toolbar))
        supportActionBar?.setTitle(R.string.hva_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun showCarryOrderListFragment() {
        supportFragmentManager.beginTransaction()
                .add(R.id.container, HarvestValidationCarryOrderListFragment.newInstance())
                .commit()
    }

    private fun showHarvestValidationCriteriaFragment(carryOrder: HarvestValidationCarryOrderModel) {
        supportFragmentManager.beginTransaction()
                .addToBackStack(mCarryOrderListFragmentTag)
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                .replace(R.id.container, HarvestValidationFragment.newInstance(carryOrder))
                .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            super.onBackPressed()
        }
        else {
            supportFragmentManager.popBackStack()
        }
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onCarryOrderClicked(event: HarvestValidationCarryOrderListFragment.OnCarryOrderClickedEvent) {
        showHarvestValidationCriteriaFragment(event.carryOrder)
    }

    private fun onValidationFinished(event: HarvestValidationFragment.OnHarvestValidationFinished) {
        supportFragmentManager.popBackStack()
    }

    private fun onSetToolbarTitleEvent(event: BaseFragment.SetToolbarTitleEvent) {
        if (event.context == this) {
            event.title?.apply {
                supportActionBar?.title = this
            }

            event.subtitle?.apply {
                supportActionBar?.subtitle = this
            }
        }
    }

}

