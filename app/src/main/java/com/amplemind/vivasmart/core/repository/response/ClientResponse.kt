package com.amplemind.vivasmart.core.repository.response

import com.google.gson.annotations.SerializedName

data class ClientResponse(
        @SerializedName("data") val data: List<ClientResponseResult>,
        @SerializedName("total_count") val total_count: Int
)

data class ClientResponseResult(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String? = null
)
