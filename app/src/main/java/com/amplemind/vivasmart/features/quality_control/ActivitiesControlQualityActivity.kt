package com.amplemind.vivasmart.features.quality_control

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.quality_control.adapter.ControlActivitiesAdapter
import com.amplemind.vivasmart.features.quality_control.viewModel.ActivitiesControlViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlActivitiesItemViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlGrooveViewModel
import kotlinx.android.synthetic.main.content_activities_control_activity.*
import javax.inject.Inject

/**
 * Created by
 *           amplemind on 7/17/18.
 */
@SuppressLint("Registered")
open class ActivitiesControlQualityActivity : BaseActivity(){

    @Inject
    lateinit var viewModel: ActivitiesControlViewModel

    private val DETAIL_CONTROL_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_activities_control_activity)

        viewModel.getDataFromIntent(intent)

        setupToolbar()

        showData()

        initRecycler()

        viewModel.getActivities().subscribe(this::loadData).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)

       viewModel.getControlListActivities(intent.getIntExtra("collaborator_id", 0),
               intent.getIntExtra("stage_id", 0)).addTo(subscriptions)

    }

    private fun isSyncFlow(): Boolean {
        return intent.getBooleanExtra("SyncFlow", false)
    }

    private fun showData() {
        viewModel.getName().subscribe {
            tv_name.text = it
        }.addTo(subscriptions)

        viewModel.getNumber().subscribe {
            tv_number.text = it
            supportActionBar!!.title = intent.getStringExtra("title")
            supportActionBar!!.subtitle = intent.getStringExtra("lot_name")
        }.addTo(subscriptions)
    }

    private fun initRecycler() {
        rv_activities_control.hasFixedSize()
        rv_activities_control.layoutManager = GridLayoutManager(this, 1)
        rv_activities_control.adapter = ControlActivitiesAdapter(this)
    }


    private fun loadData(list : List<ControlActivitiesItemViewModel>) {
        calculateActivitiesControlQualityAverage(list)
        (rv_activities_control.adapter as ControlActivitiesAdapter).addElements(list)

        (rv_activities_control.adapter as ControlActivitiesAdapter).clickItem().subscribe(this::onClickItem).addTo(subscriptions)
    }

    private fun onClickItem(item : Pair<ControlGrooveViewModel, Int>) {
        if(viewModel.canEditGroove(item.second) && !item.first.isFinished && !isSyncFlow()) {
            val intentDetail = Intent(this, DetailControlQualityActivity::class.java)
            intentDetail.putExtra("groove_id", item.first.id)
            intentDetail.putExtra("position", item.second)
            intentDetail.putExtra("title", supportActionBar!!.title)
            intentDetail.putExtra("username", tv_name.text.toString())
            intentDetail.putExtra("activity_name", viewModel.getActivityName(item.second))
            intentDetail.putExtra("activity_id", viewModel.getActivityId(item.second))
            intentDetail.putExtra("activity_log", viewModel.getActivityLog(item.second))
            intentDetail.putExtra("activity_area", viewModel.getActivityArea(item.second))
            intentDetail.putExtra("activity_category", viewModel.getActivityCategory(item.second))
            intentDetail.putExtra("activity_sub_category", viewModel.getActivitySubCategory(item.second))
            intentDetail.putExtra("collaborator_id", intent.getIntExtra("collaborator_id", 0))
            startActivityForResult(intentDetail, DETAIL_CONTROL_REQUEST)
        }
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_list_activities
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == DETAIL_CONTROL_REQUEST && resultCode == Activity.RESULT_OK) {
            val position = data!!.getIntExtra("position", 0)
            val grooveId = data.getIntExtra("groove_id", 0)
            viewModel.updateCount(position, grooveId)
            (rv_activities_control.adapter as ControlActivitiesAdapter).notifyItemChanged(position)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun calculateActivitiesControlQualityAverage(items: List<ControlActivitiesItemViewModel>) {
        var sum = 0
        val average = findViewById<TextView>(R.id.average)
        if (items.isNotEmpty()) {
            for (item in items) {
                sum += item.score
            }
            average.text = applicationContext.getString(R.string.percentage, sum / items.size)
        }
        else {
            average.text = applicationContext.getString(R.string.percentage, 100)
        }
    }

}