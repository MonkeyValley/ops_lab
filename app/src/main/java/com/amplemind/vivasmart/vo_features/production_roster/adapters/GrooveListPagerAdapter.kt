package com.amplemind.vivasmart.vo_features.production_roster.adapters

import androidx.viewpager.widget.PagerAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import collections.forEach
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.collaborators.viewModel.AssingGroovesViewModel
import kotlinx.android.synthetic.main.layout_assign_groove.view.*

class GrooveListPagerAdapter(
        private val mEventBus: EventBus
) : PagerAdapter() {

    companion object {
        private val TAG: String = GrooveListPagerAdapter::class.java.simpleName
    }

    var currentTab: Int = 0

    private val mLayouts = SparseArray<View>()

    private val mSubscriptions = AndroidDisposable()

    var tableGrooves: List<AssingGroovesViewModel.TableGrooves> = listOf()
        set(value) {

            var shouldUpdate = false

            if (tableGrooves.size != value.size) {
                shouldUpdate = true
            }
            else {
                for (i: Int in value.indices) {
                    if (value[i].grooveTotal != tableGrooves[i].grooveTotal) {
                        shouldUpdate = true
                        break
                    }
                }
            }

            if (shouldUpdate) {
                field = value
                notifyDataSetChanged()
            }

        }

    init {
        mEventBus.observe<GroovesAdapter.OnGroovesLoadedEvent>()
                .filter { event ->
                    tableGrooves[event.position].activityLogUuid == event.parentItemId
                }
                .subscribe(this::onGroovesLoadedEvent)
                .addTo(mSubscriptions)
    }

    fun setUpTab(position: Int) {
        mLayouts[position]?.apply {
            var adapter = rv_grooves.adapter as? GroovesAdapter
            if (adapter == null) {
                adapter = GroovesAdapter(mEventBus)
                adapter.position = position
                adapter.tableGrooves = tableGrooves[position]
                rv_grooves.adapter = adapter
            }
            else {
                adapter.reload()
            }
        }
    }

    fun cancelIfLoading(position: Int) {
        (mLayouts[position]?.rv_grooves?.adapter as? GroovesAdapter)?.cancelIfLoading()
    }

    private fun onGroovesLoadedEvent(event: GroovesAdapter.OnGroovesLoadedEvent) {
        val layout = mLayouts[event.position]

        Log.e(TAG, "${layout.width} x ${layout.height}")

        layout.progress.visibility = View.GONE
        layout.rv_grooves.visibility = View.VISIBLE
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layout = LayoutInflater.from(container.context)
                .inflate(R.layout.layout_assign_groove, container, false)

        layout.rv_grooves.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(container.context, 7)
            itemAnimator = DefaultItemAnimator()
        }

        mLayouts.put(position, layout)

        layout.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)

        container.addView(layout)

        if (position == currentTab) {
            setUpTab(position)
        }

        return layout
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(mLayouts[position])
        mLayouts.remove(position)
    }

    override fun isViewFromObject(view: View, layout: Any): Boolean =
            (layout as View) == view

    override fun getPageTitle(position: Int): CharSequence? = "Tabla ${tableGrooves[position].tableNo}"

    override fun getCount(): Int = tableGrooves.size

    fun clearSelection() {
        mLayouts.forEach { _, layout ->
            (layout.rv_grooves.adapter as? GroovesAdapter)?.clearSelection()
        }
    }

    fun getSelectedGrooves(): SparseArray<List<Int>> {
        val selectedGrooves = SparseArray<List<Int>>()

        mLayouts.forEach { _, layout ->
            (layout.rv_grooves.adapter as? GroovesAdapter)?.let { adapter ->
                val selected = adapter.getSelected()

                if (selected.isNotEmpty()) {
                    selectedGrooves.put(adapter.tableNo, selected.map { groove ->
                        groove.grooveNo
                    })
                }
            }
        }

        return selectedGrooves
    }

    fun cleanUp() {
        mLayouts.forEach { _, layout ->
            layout.rv_grooves.adapter = null
        }
        mSubscriptions.dispose()
    }

}
