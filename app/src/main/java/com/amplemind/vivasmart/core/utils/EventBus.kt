package com.amplemind.vivasmart.core.utils

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EventBus @Inject constructor() {

    val mBus: PublishSubject<Any> = PublishSubject.create<Any>()

    fun send(item: Any) =
            mBus.onNext(item)

    inline fun <reified T> observe(): Observable<T> =
            mBus.filter {
                it is T
            }.map {
                it as T
            }

    inline fun <reified T> observe(noinline callback: (T) -> Unit): Disposable =
            observe<T>().subscribe(callback)


}