package com.amplemind.vivasmart.features.gallery.viewModel

import android.annotation.SuppressLint
import android.util.Log
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.models.realm.PlagueImageModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import java.io.File
import javax.inject.Inject

class GalleryViewModel @Inject constructor(
        private val uploadRepository: ProfileRepository,
        private val apiErrors: HandleApiErrors) {

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val successUploadImageUrl: BehaviorSubject<MutableList<String>> = BehaviorSubject.create()

    private val requestFail = BehaviorSubject.create<String>()
    val imageList = mutableListOf<String>()

    @SuppressLint("CheckResult")
    fun uploadImage(files: MutableList<File>, type: String) {
        for (file in files) {
            uploadRepository.uploadImage(file, type)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { progressStatus.onNext(true) }
                    .doOnTerminate { progressStatus.onNext(false) }
                    .doOnTerminate {
                        progressStatus.onNext(false)
                        onSuccessUpdateImageUrl().onNext(imageList)
                    }
                    .subscribe({
                        if (files.isNotEmpty()) {
                            files.removeAt(0)
                        }
                        imageList.add(it.relative_url)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                    })
        }
    }

    fun onSuccessUpdateImageUrl(): BehaviorSubject<MutableList<String>> {
        return successUploadImageUrl
    }

    fun getImageUrl(plagueId: Int, lvl: Int): String {
        Realm.getDefaultInstance().use {
            var result = it
                    .where(PlagueImageModel::class.java)
                    .equalTo("plagueId", plagueId)
                    .and()
                    .equalTo("level", lvl)
                    .findFirst()
                    ?.image ?: ""

            Log.e("urlPlague", "lvl-" + lvl +"-https://backend-filestorage.s3-us-west-1.amazonaws.com/" + result)
            return "https://backend-filestorage.s3-us-west-1.amazonaws.com/" + result
        }
    }
}