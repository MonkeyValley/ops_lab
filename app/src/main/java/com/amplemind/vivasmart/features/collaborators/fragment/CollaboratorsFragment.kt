package com.amplemind.vivasmart.features.collaborators.fragment

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.CATEGORY_ID
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_features.production_roster.adapters.CollaboratorsAdapter
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.CollaboratorsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_collaborators.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CollaboratorsFragment : BaseFragment() {

    companion object {

        val TAG: String = CollaboratorsFragment::class.java.simpleName

        private val PARAM_SELECTED = "$TAG.Selected"
        private val PARAM_SELECT_ALL = "$TAG.SelectAll"

        fun newInstance() = CollaboratorsFragment()

        fun newInstance(selected: ArrayList<String>, selectAll: Boolean) = newInstance().apply {
            arguments = Bundle().apply {
                argumentsNeeded = true
                putStringArrayList(PARAM_SELECTED, selected)
                putBoolean(PARAM_SELECT_ALL, selectAll)
            }
        }
    }

    @Inject
    lateinit var mViewModel: CollaboratorsViewModel

    private lateinit var listCollaborators: Changes<CollaboratorModel>
    private lateinit var listCollaboratorsActivity: Changes<CollaboratorModel>

    private lateinit var mAdapter: CollaboratorsAdapter

    //private lateinit var maAdapter: CollaboratorsActivityAdapter
    private val mQueryFilter = PublishSubject.create<String>()
    private val mQueryFilterActivity = PublishSubject.create<String>()

    private var mSelectAll: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        CATEGORY_ID = 0
        return inflater.inflate(R.layout.fragment_collaborators, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mViewModel.cleanUp()
    }

    fun getSelectedCollaborators() = mAdapter.getSelected()
    //fun getSelectedCollaboratorsByActivity() = maAdapter.getSelected()

    override fun readArguments(args: Bundle) {
        super.readArguments(args)

        val selectedUuids = args.getStringArrayList(PARAM_SELECTED)

        selectedUuids?.forEach {
            Log.e("UUID", it)
        }

        if (selectedUuids != null) {
            mViewModel.selectedUuids = selectedUuids
        }

        mSelectAll = args.getBoolean(PARAM_SELECT_ALL, false)
    }

    override fun setUpCallbacks() {
        super.setUpCallbacks()
        mEventBus.observe(this::onSelectionChangedEvent).addTo(subscriptions)
        //mEventBus.observe(this::onSelectionActivityChangedEvent).addTo(subscriptions)
    }

    override fun loadData() {
        super.loadData()
        mQueryFilter.onNext("")
        mQueryFilterActivity.onNext("")
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()

        mEventBus.observe(this::onItemSelectionChangedEvent).addTo(subscriptions)
        //mEventBus.observe(this::onActivityItemSelectionChangedEvent).addTo(subscriptions)

        setUpQueryFilter()
        setUpQueryFilterActivity()

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mQueryFilter.onNext(newText ?: "")
                mQueryFilterActivity.onNext(newText ?: "")
                return false
            }
        })

        btn_all.setOnClickListener { v ->
            //rv_collaborators_database.visibility = View.VISIBLE
            //rv_collaborators_activity_database.visibility = View.GONE

            btn_all.setBackgroundColor(Color.parseColor("#638EFD"))
            btn_activity.setBackgroundColor(Color.parseColor("#E3E3E3"))
            reloadData(listCollaborators, false)
        }
        btn_activity.setOnClickListener { v ->
            //rv_collaborators_database.visibility = View.GONE
            //rv_collaborators_activity_database.visibility = View.VISIBLE

            btn_all.setBackgroundColor(Color.parseColor("#E3E3E3"))
            btn_activity.setBackgroundColor(Color.parseColor("#638EFD"))
            reloadData(listCollaboratorsActivity, true)
        }
    }

    override fun setUpUI() {
        super.setUpUI()
        initRecycler()
    }

    fun selectAll(selectAll: Boolean) {
        arguments?.putBoolean(PARAM_SELECT_ALL, selectAll)

        if (::mAdapter.isInitialized) {
            mAdapter.selectAll = selectAll
        }

        /*if (::maAdapter.isInitialized) {
            maAdapter.selectAll = selectAll
        }*/
    }

    private fun setUpQueryFilter() =
            mQueryFilter
                    .debounce(250, TimeUnit.MICROSECONDS, AndroidSchedulers.mainThread())
                    .map {
                        it.trim()
                    }
                    .switchMap(mViewModel::find)
                    .subscribe({
                        onCollaboratorsLoaded(it)
                    }, {
                        Log.e("SearchColab", "error", it)
                    })
                    .addTo(subscriptions)

    private fun setUpQueryFilterActivity() =
            mQueryFilter
                    .debounce(250, TimeUnit.MICROSECONDS, AndroidSchedulers.mainThread())
                    .map {
                        it.trim()
                    }
                    .switchMap(mViewModel::findByActivity)
                    .subscribe({
                        onCollaboratorsActivityLoaded(it)
                    }, {
                        Log.e("SearchColabActivity", "error", it)
                    })
                    .addTo(subscriptions)


    private fun onCollaboratorsLoaded(items: Changes<CollaboratorModel>) {
        listCollaborators = items
        mAdapter.turnOnActivity = false
        if (items != null) {
            mAdapter.setItems(items)
        }
    }

    private fun onCollaboratorsActivityLoaded(items: Changes<CollaboratorModel>) {
        listCollaboratorsActivity = items
    }

    private fun reloadData(items: Changes<CollaboratorModel>, turnOnActivity: Boolean) {
        mAdapter.turnOnActivity = turnOnActivity
        mAdapter.scroll = false
        mAdapter.setItems(items)
        mAdapter.notifyDataSetChanged()
    }

    private fun onItemSelectionChangedEvent(event: CollaboratorsAdapter.OnItemSelectionChangedEvent) {
        sendOnActionModeUpdateEvent(event.totalSelected)
    }

    private fun onSelectionChangedEvent(event: CollaboratorsAdapter.OnSelectionChangedEvent) {
        sendOnActionModeUpdateEvent(event.totalSelected)
    }

    private fun sendOnActionModeUpdateEvent(count: Int) {
        mEventBus.send(OnActionModeUpdateEvent(this, count))
    }

    /*private fun onActivityItemSelectionChangedEvent(event: CollaboratorsActivityAdapter.OnItemSelectionChangedEvent) {
        sendOnActionModeUpdateEvent(event.totalSelected)
    }

    private fun onSelectionActivityChangedEvent(event: CollaboratorsActivityAdapter.OnSelectionChangedEvent) {
        sendOnActionModeUpdateEvent(event.totalSelected)
    }*/

    private fun initRecycler() {

        mAdapter = CollaboratorsAdapter(mEventBus, mSelectAll)

        rv_collaborators_database.apply {
            setHasFixedSize(true)
            adapter = mAdapter
            //layoutManager = LinearLayoutManager(context!!)
            layoutManager = WrapContentLinearLayoutManager(context!!)
        }
        rv_collaborators_database.addOnScrollListener(rvScrollManager)

        /*maAdapter = CollaboratorsActivityAdapter(mEventBus, mSelectAll)

        rv_collaborators_activity_database.apply {
            setHasFixedSize(true)
            adapter = maAdapter
            layoutManager = LinearLayoutManager(context!!)
        }
        rv_collaborators_activity_database.addOnScrollListener(rvScrollManager)*/
    }

    private val rvScrollManager = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            mAdapter.scroll = true
        }
    }

    data class OnActionModeUpdateEvent(
            val fragment: CollaboratorsFragment,
            val count: Int
    )
}

class WrapContentLinearLayoutManager : LinearLayoutManager {
    constructor(context: Context?) : super(context) {}

    override fun onLayoutChildren(recycler: Recycler, state: RecyclerView.State) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (e: IndexOutOfBoundsException) {
            Log.e("Error", "IndexOutOfBoundsException in RecyclerView happens")
        }
    }

    override fun supportsPredictiveItemAnimations(): Boolean {
        return false
    }
}
