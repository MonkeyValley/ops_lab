package com.amplemind.vivasmart.features.production_roster

import android.util.SparseArray
import com.amplemind.vivasmart.vo_core.repository.RosterDetailRepository
import com.amplemind.vivasmart.vo_core.repository.models.extensions.isControlAvailable
import com.amplemind.vivasmart.vo_core.repository.models.extensions.isPracticeAvailable
import com.amplemind.vivasmart.vo_core.repository.models.extensions.isTrainingAvailable
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.RosterDetailActivityLogViewModel
import io.reactivex.Single
import io.realm.Realm
import java.io.File
import javax.inject.Inject

class RosterDetailViewModel @Inject constructor(private val repository: RosterDetailRepository) {

    //Arguments passed to RosterDetailActivity
    lateinit var collaboratorUuid: String
    lateinit var stageUuid: String
    lateinit var type: String

    //Loaded in a query
    var activityLogViewModels = mutableListOf<RosterDetailActivityLogViewModel>()
    var payment: String = "$0.0000"

    var isTraining: Boolean = false
    var isControl: Boolean = false
    var isPractice: Boolean = false

    var setTraining: Boolean = false
    var setControl: Boolean = false
    var setPractice: Boolean = false
    var setTrainingValidate: Boolean = true
    var setControlValidate: Boolean = true
    var setPracticeValidate: Boolean = true

    var trainingName = ArrayList<String>()
    var controlName = ArrayList<String>()
    var practiceName = ArrayList<String>()

    fun getPersonalUI(activityLogs: List<ActivityLogModel>){
        activityLogs.forEach {
            if(it.isTrainingAvailable()) setTraining = true
            else {
                setTrainingValidate= false
                trainingName.add(it.activityCode?.activity?.name ?: "Sin Nombre")
            }

            if(it.isControlAvailable()) setControl = true
            else {
                setControlValidate = false
                controlName.add(it.activityCode?.activity?.name ?: "Sin Nombre")
            }

            if(it.isPracticeAvailable()) setPractice = true
            else {
                setPracticeValidate = false
                practiceName.add(it.activityCode?.activity?.name ?: "Sin Nombre")
            }
        }
        retrieveSwitchSettings(activityLogs)
    }

    val isReadOnly: Boolean
        get() = RosterDetailRepository.Type.valueOf(type) == RosterDetailRepository.Type.SIGNED

    fun loadCollaborator() = repository.loadCollaborator(collaboratorUuid)

    fun loadActivityLogs() = repository.loadActivityLogs(stageUuid, collaboratorUuid, RosterDetailRepository.Type.valueOf(type))

    fun setQuantityAndTime(activityLog: ActivityLogModel, quantity: Int, timeInMinutes: Int): Single<ActivityLogModel> =
            repository.setQuantityAndTime(activityLog, quantity, timeInMinutes)

    fun assignGrooves(activityLog: ActivityLogModel, grooves: SparseArray<List<Int>>): Single<ActivityLogModel> = repository.assignGrooves(activityLog, grooves)

    fun finishActivityLogs(activityLogs: List<ActivityLogModel>, isTraining: Boolean, isPractice: Boolean, isControl: Boolean, signature: File?) = repository.finishActivityLogs(activityLogs, isTraining, isPractice, isControl, signature)

    fun retrieveSwitchSettings(activityLogs: List<ActivityLogModel>) { isTraining = activityLogs.all { it.isTraining }; isPractice = activityLogs.all { it.isPractice }; isControl = activityLogs.all { it.isControl } }

    fun hasActivatedSwitch() = isTraining || isPractice || isControl


}