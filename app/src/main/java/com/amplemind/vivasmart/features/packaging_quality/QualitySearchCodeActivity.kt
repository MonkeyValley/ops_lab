package com.amplemind.vivasmart.features.packaging_quality

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.onChange
import com.amplemind.vivasmart.features.packaging_quality.viewModel.QualitySearchCodeViewModel
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.content_quality_search_code_activity.*
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/6/18.
 */
@SuppressLint("Registered")
open class QualitySearchCodeActivity : BaseActivity() {


    @Inject
    lateinit var viewModel: QualitySearchCodeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_quality_search_code_activity)

        setupToolbar()

        enableButton()

        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onCodeFound().subscribe(this::onCodeFound).addTo(subscriptions)
        searchCodeClick().addTo(subscriptions)

    }

    private fun searchCodeClick(): Disposable {
        return RxView.clicks(btn_code_done).subscribe {
            viewModel.searchCode(ed_code.text.toString()).addTo(subscriptions)
        }
    }

    private fun enableButton() {
        ed_code.onChange {
            btn_code_done.isEnabled = it.isNotEmpty()
        }
    }

    private fun onCodeFound(id: Int) {
        startActivity(Intent(this,
                DetailOrdenActivity::class.java).putExtra("id", id))
        finish()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_search_code
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Calidad en empaque"
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_quality_search_code, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.search_package_code -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}