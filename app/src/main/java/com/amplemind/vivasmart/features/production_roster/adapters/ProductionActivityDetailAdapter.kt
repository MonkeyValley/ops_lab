package com.amplemind.vivasmart.features.production_roster.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemProductionActivityDetailBinding
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ProductionActivityDetailItemViewModel
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit

class ProductionActivityDetailAdapter constructor(val context: Context) : RecyclerView.Adapter<ProductionActivityDetailAdapter.ProductionActivityDetailViewHolder>() {

    private var list = mutableListOf<ProductionActivityDetailItemViewModel>()
    private val clickItem: BehaviorSubject<ProductionActivityDetailItemViewModel> = BehaviorSubject.create()

    fun addElements(data: List<ProductionActivityDetailItemViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductionActivityDetailViewHolder {
        val binding = ItemProductionActivityDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductionActivityDetailViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ProductionActivityDetailViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onItemClicked(): BehaviorSubject<ProductionActivityDetailItemViewModel> {
        return clickItem
    }

    inner class ProductionActivityDetailViewHolder(private val binding: ItemProductionActivityDetailBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ProductionActivityDetailItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            RxView.clicks(binding.root).throttleFirst(1, TimeUnit.SECONDS).subscribe {
                if (!item.isDone) {
                    clickItem.onNext(item)
                }
            }
        }

    }

}