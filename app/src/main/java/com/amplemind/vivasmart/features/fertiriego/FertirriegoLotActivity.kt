package com.amplemind.vivasmart.features.fertiriego

import android.os.Bundle
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_TPYE
import com.amplemind.vivasmart.features.fertiriego.adapter.FertirriegoLotTabAdapter
import kotlinx.android.synthetic.main.activity_haulage.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.progress_track_component.*

class FertirriegoLotActivity : BaseActivityWithFragment() {

    companion object {
        val TAG = FertirriegoLotActivity::class.simpleName
        val TAG_NAME = "$TAG.TagName"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_haulage)

        setupToolbar()

        val fragmentAdapter = FertirriegoLotTabAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_haulage
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        FERTIRRIEGO_TPYE = intent?.getStringExtra(TAG_NAME) ?: ""
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text =  FERTIRRIEGO_TPYE
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        lbl_track_title.text = "/ Fertirriego / "+FERTIRRIEGO_TPYE+" /"
    }

    override fun onBackPressed() {
        finish()
    }

}

