package com.amplemind.vivasmart.features.production_quality

import android.graphics.Canvas
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.View
import android.widget.RelativeLayout

class ItemDeleteConfirmationReport (dragDirs: Int, swipeDirs: Int, private val listener: RecyclerItemTouchHelperListener) : ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs) {

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return true
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (viewHolder != null) {
            var foregroundView: CardView? = null
            if(viewHolder is ConfirmationReportsAdapter.ConfirmationReportsViewHolder) {
                foregroundView = viewHolder.viewForeground
            }
            getDefaultUIUtil().onSelected(foregroundView)
        }
    }

    override fun onChildDrawOver(c: Canvas, recyclerView: RecyclerView,
                                 viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float,
                                 actionState: Int, isCurrentlyActive: Boolean) {
        var foregroundView: CardView? = null
        if(viewHolder is ConfirmationReportsAdapter.ConfirmationReportsViewHolder) {
            foregroundView = viewHolder.viewForeground
        }
        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive)
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        var foregroundView: CardView? = null
        if(viewHolder is ConfirmationReportsAdapter.ConfirmationReportsViewHolder) {
            foregroundView = viewHolder.viewForeground
        }
        if (foregroundView != null){
            getDefaultUIUtil().clearView(foregroundView)
        }
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView,
                             viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float,
                             actionState: Int, isCurrentlyActive: Boolean) {
        var foregroundView: CardView? = null
        var deleteView: RelativeLayout? = null

        if(viewHolder is ConfirmationReportsAdapter.ConfirmationReportsViewHolder) {
            foregroundView = viewHolder.viewForeground
            deleteView = viewHolder.viewBackground
        }
        if (dX > 0) {
            deleteView?.visibility = View.GONE
        } else {
            deleteView?.visibility = View.VISIBLE
        }
        if (foregroundView != null){
            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                    actionState, isCurrentlyActive)
        }
    }


    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        listener.onSwiped(viewHolder, direction, viewHolder.adapterPosition)
    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    interface RecyclerItemTouchHelperListener {
        fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int)
    }
}