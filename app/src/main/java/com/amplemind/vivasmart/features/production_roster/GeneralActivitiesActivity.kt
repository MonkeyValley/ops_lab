package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_roster.viewModels.GeneralActivitiesViewModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ItemActivitiesPayrollViewModel
import kotlinx.android.synthetic.main.activity_general_activities.*
import javax.inject.Inject

class GeneralActivitiesActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: GeneralActivitiesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_general_activities)

        setupToolbar()
        initRecycler()
        getData()
        btn_ready.setOnClickListener {

        }
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_general_activities
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Actividades"
    }

    private fun getData() {
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onSuccessRequest().subscribe(this::setDataAdapter).addTo(subscriptions)
        viewModel.getDummyActivities()
    }

    private fun noActivities(show: Boolean) {
        if (show) {
            no_found_activities.visibility = View.VISIBLE
            btn_ready.visibility = View.GONE
        }
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        //(rv_chosse_activity.adapter as ActivitiesChooseAdapter).addElements(activities)
    }

    private fun initRecycler() {
        rv_chosse_activity.hasFixedSize()
        rv_chosse_activity.layoutManager = GridLayoutManager(this, 3)
        rv_chosse_activity.itemAnimator = DefaultItemAnimator()
        //rv_chosse_activity.adapter = ActivitiesChooseAdapter(this)
        //(rv_chosse_activity.adapter as ActivitiesChooseAdapter).onClickItem().subscribe(this::clickItem).addTo(subscriptions)
    }

    private fun clickItem(itemViewModel: ItemActivitiesPayrollViewModel) {
        val activityIntent = Intent(this, TimerGeneralActivity::class.java)
        startActivity(activityIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
