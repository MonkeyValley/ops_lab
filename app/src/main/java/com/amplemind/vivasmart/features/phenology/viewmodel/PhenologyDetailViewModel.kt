package com.amplemind.vivasmart.features.phenology.viewmodel

import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.PhenologyRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.phenology.dialog.ItemPhenologyDetailViewModel
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyDetailResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class PhenologyDetailViewModel @Inject constructor(val repository: PhenologyRepository, private val apiErrors: HandleApiErrors) : BaseViewModel() {
    private val mOnShowProgress = PublishSubject.create<Boolean>()
    private val mOnReportLoaded = PublishSubject.create<List<ItemPhenologyDetailViewModel>>()
    private val mOnReportLoadingError = PublishSubject.create<Throwable>()
    private var mCurrentDisposable: Disposable? = null
    var stage_id = 0
    var variety_id = 0
    var table_no = 0

    fun lodPhenologyDetail() =
            repository.loadPhenologyDetail(stage_id, variety_id, table_no)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        Log.e("stage_id", stage_id.toString())
                        Log.e("variety_id", variety_id.toString())
                        Log.e("table_no", table_no.toString())
                        mOnShowProgress.onNext(true)
                    }
                    .doOnComplete {
                        mOnShowProgress.onNext(false)
                        disposeCurrentRequest()
                    }
                    .doOnError {
                        mOnShowProgress.onNext(false)
                        it.printStackTrace()
                    }
                    .map(this::mapPhenologyDetail)
                    .subscribe({ mOnReportLoaded.onNext(it) } , { mOnReportLoadingError.onNext(it) })

    fun disposeCurrentRequest() {
        mCurrentDisposable?.apply {
            if (isDisposed) {
                dispose()
            }
            mCurrentDisposable = null
        }
    }

    private fun mapPhenologyDetail(phenologyDetail: PhenologyDetailResponse): List<ItemPhenologyDetailViewModel> {
        val data = phenologyDetail.data
        val viewModels = mutableListOf<ItemPhenologyDetailViewModel>()
        viewModels.add(ItemPhenologyDetailViewModel(data.valves, data.vars))
        return viewModels
    }

    fun onReportLoaded(callback: (List<ItemPhenologyDetailViewModel>) -> Unit) = mOnReportLoaded.subscribe(callback)

    fun onReportLoadingError(callback: (Throwable) -> Unit) = mOnReportLoadingError.subscribe(callback)

    fun onShowProgress(callback: (Boolean) -> Unit) = mOnShowProgress.subscribe(callback)

    override fun equals(other: Any?): Boolean = super.equals(other)

    override fun hashCode(): Int = super.hashCode()

    override fun toString(): String = super.toString()
}