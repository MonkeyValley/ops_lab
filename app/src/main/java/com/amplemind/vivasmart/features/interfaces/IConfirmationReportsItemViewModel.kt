package com.amplemind.vivasmart.features.interfaces

interface IConfirmationReportsItemViewModel {

    fun isHeader(): Boolean

}