package com.amplemind.vivasmart

import android.app.Application
import android.content.Context
import com.amplemind.vivasmart.core.base.BaseActivity
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(
        constants = BuildConfig::class,
        application = AndroidTest.ApplicationStub::class
)
abstract class AndroidTest {

    @Suppress("LeakingThis")
    @Rule
    @JvmField val injectMocks = InjectMocksRule.create(this@AndroidTest)

    fun context(): Context = RuntimeEnvironment.application

    fun activityContext(): Context = mock(BaseActivity::class.java)

    fun getString(res: Int): String = context().getString(res)

    internal class ApplicationStub : Application()
}
