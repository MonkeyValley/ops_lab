package com.amplemind.vivasmart.features.pollination.repository

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.pollination.repository.remote.PollinationApi
import com.amplemind.vivasmart.features.pollination.repository.responses.Pollinationrevisions
import com.google.gson.Gson
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PollinationRepository @Inject constructor(private val api: PollinationApi,
                                                private val preferences: UserAppPreferences) {

    val businessUnitId = Gson().fromJson(preferences.userInfo,
            UserModel::class.java).businessUnitId ?: ""

    fun getPollinationReport(lotId: Int): Single<List<Pollinationrevisions>> =
            api.getPollinationLastWeeks(
                    preferences.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Data-Pollination: ", response.data.toString())
                        response.data
                    }

}