package com.amplemind.vivasmart.core.repository.model

/**
 * Created by
 *          amplemind on 7/12/18.
 */
data class QualityReportModelTemp(val name : String , val number : Int, val percentage : Int)