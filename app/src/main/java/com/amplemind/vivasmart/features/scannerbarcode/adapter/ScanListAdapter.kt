package com.amplemind.vivasmart.features.scannerbarcode.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.amplemind.vivasmart.databinding.ItemCollaboratorsDatabaseBinding
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemCollaboratorsViewModel
import androidx.databinding.library.baseAdapters.BR
import kotlinx.android.synthetic.main.item_collaborators_database.view.*

class ScanListAdapter : RecyclerView.Adapter<ScanListAdapter.ScanListViewHolder>() {

    val list = mutableListOf<ItemCollaboratorsViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScanListViewHolder {
        val binding = ItemCollaboratorsDatabaseBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ScanListViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ScanListViewHolder, position: Int) {
        holder.bind(list[position])
        showPhoto(list[position].id, holder.itemView.icon_back, holder.itemView.image_profile)
    }

    //TODO: just to show the scenario
    private fun showPhoto(id: Int, icon_back: View, image_profile: ImageView) {
        if (id == -1){
            image_profile.visibility = View.GONE
            icon_back.visibility = View.VISIBLE
        }
    }

    fun addElement(item : ItemCollaboratorsViewModel){
        list.add(list.size, item)
        notifyDataSetChanged()
    }

    fun addAllElements(items : List<ItemCollaboratorsViewModel>){
        list.addAll(items)
        notifyDataSetChanged()
    }

    class ScanListViewHolder(val binding : ItemCollaboratorsDatabaseBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item : ItemCollaboratorsViewModel){
                    binding.setVariable(BR.viewModel, item)
                    binding.executePendingBindings()
                    if (adapterPosition > -1){

                    }
                }

    }

}
