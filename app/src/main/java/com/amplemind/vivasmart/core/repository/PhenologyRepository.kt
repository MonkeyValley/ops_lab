package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.PhenologyApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.menus.*
import com.amplemind.vivasmart.features.phenology.models.local.*
import com.google.gson.Gson
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PhenologyRepository @Inject constructor(private val api: PhenologyApi, private val authorization: ApiAuthorization, private val preferences: UserAppPreferences) {
    val userInfo: UserModel = Gson().fromJson(preferences.userInfo, UserModel::class.java)
    var businessUnit = userInfo.businessUnitId

    fun getPhenologyActivitiesMenu(): Observable<List<PayrollActivitiesModel>> {
        return Observable.just(getActivitiesPhenology())
    }

    fun getLots(is_commercial: Int): Observable<List<PhenologyLot>> {
        return api.getLots(authentication = authorization.getAuthToken(preferences.token),
                user_id = userInfo.id,
                is_commercial = is_commercial )
                .timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)
    }

    fun getLotsVarieties(stage_id: Int, is_commercial: Int): Observable<PhenologyResponse> {
        return api.getLotsVarieties(authentication = authorization.getAuthToken(preferences.token),
                stage_id = stage_id, is_commercial = is_commercial)
                .timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)
    }


    fun postPhenologyRevision(bodyObject: PhenologyPost): Observable<PhenologyPostRevisionResponse> {
        return api.postPhenologyRevision(authentication = authorization.getAuthToken(preferences.token),
                body = bodyObject)
                .timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)
    }

    fun postPhenologyPlant(bodyObject: PhenologyPlantItem, id_revisionStr: String): Observable<PhenologyPostPlantResponse> {
        return api.postPhenologyPlant(authentication = authorization.getAuthToken(preferences.token),
                body = bodyObject,
                id_revision = id_revisionStr)
                .timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)
    }

    fun postPhenologySignAndComment(bodyObject: PhenologySignComment, id_revisionStr: String): Observable<PhenologyPostRevisionResponse> {
        return api.postPhenologySignAndComment(authentication = authorization.getAuthToken(preferences.token),
                body = bodyObject,
                id_revision = id_revisionStr)
                .timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)
    }

    fun loadPhenologyDetail(stage: Int, varietyId: Int, tableNo: Int): Observable<PhenologyDetailResponse> {
        return api.getPhenologyDetailData(authentication = authorization.getAuthToken(preferences.token),
                stage = stage,
                varietyId = varietyId,
                tableNo = tableNo)
                .timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)
    }

    fun getVariableLastRecord(stage_id: Int, table_no: Int, groove_from: Int, groove_to: Int, var_id : Int,  plant_number: Int): Observable<VariableLastRecordResponse> {
        return api.getVariableLastRecord(authentication = authorization.getAuthToken(preferences.token),
                stage_id = stage_id,
                table_no = table_no,
                groove_from = groove_from,
                groove_to = groove_to,
                var_id = var_id,
                plant_number = plant_number)
                .timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)
    }

}