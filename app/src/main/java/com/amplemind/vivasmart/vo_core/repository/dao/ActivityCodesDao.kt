package com.amplemind.vivasmart.vo_core.repository.dao

import android.util.Log
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import io.reactivex.Observable
import io.realm.Realm

class ActivityCodesDao (val activitiesDao: ActivitiesDao, val stagesDao: StagesDao) : RealmDao<ActivityCodeModel>(ActivityCodeModel::class.java) {

    override fun retrieveServerData(response: Any?): List<ActivityCodeModel>? {
        return when(response) {
            is List<*> -> {
                val activityCodes = response as List<ActivityCodeModel>

                activityCodes.forEach {
                    it.activity = activitiesDao.findById(it.activityId.toLong())
                    it.activityUuid = it.activity?.uuid ?: ""

                    it.stage = stagesDao.findById(it.stageId.toLong())
                    it.stageUuid = it.stage?.uuid ?: ""
                }

                activityCodes
            }
            else -> null
        }
    }

    fun getCodes(activityId: Int, stageId: Int): List<ActivityCodeModel> {
        val realm = Realm.getDefaultInstance()

        val results = realm.where(mModelClazz)
                .equalTo("activityId", activityId)
                .and()
                .equalTo("stageId", stageId)
                .findAll()

        val ret = realm.copyFromRealm(results)

        //TODO: Check how to calculate unit total
        /*
        ret.forEach { activityCode ->

            realm.where(ActivityLogModel::class.java)
                    .equalTo("activityCodeUuid", activityCode.uuid)
                    .sum("quantity")

            activityCode.total
        }
        */

        realm.close()

        return ret
    }

    fun createCode(activityId: Int, stageId: Int): ActivityCodeModel? {

        val activity = findActivityById(activityId)
        val stage = findStageById(stageId)

        val code = generateCode(activity!!, stageId)
        val total = calculateCodeLimit(activity, stage!!)

        val activityCode = ActivityCodeModel(
                activityId = activityId,
                activityUuid = activity.uuid,
                stageId = stageId,
                stageUuid = stage.uuid,
                code = code,
                total = "0/$total",
                activity = activity,
                stage = stage)

        activityCode.occupiedSpaces = 0
        activityCode.totalSpaces = total

        return upsert(activityCode)

    }

    fun calculateUnitTotals(activityCode: ActivityCodeModel): Observable<String> {

        return mRealm.where(ActivityLogModel::class.java)
                .equalTo("activityCodeUuid", activityCode.uuid)
                .findAll()
                .asChangesetObservable()
                .map {changes ->
                    val occupied = changes.collection.sum("quantity").toInt()
                    val total = activityCode.totalSpaces
                    return@map "$occupied/$total"
                }

    }


    fun calculateCodeLimit(activity: ActivityModel, stage: StageModel): Int {
        return if (activity.isGrooves == true) {
                    val lot = stage.lot!!
                    lot.table1 +
                            lot.table2 +
                            lot.table3 +
                            lot.table4 +
                            lot.table5 +
                            lot.table6
                }
                else {
                    activity.unitLimit
                }
    }

    fun generateCode(activity: ActivityModel, stageId: Int): Int {

        val realm = Realm.getDefaultInstance()

        val results = realm.where(mModelClazz)
                .equalTo("activityId", activity.id)
                .and()
                .equalTo("stageId", stageId)
                .max("code")

        val lastCode = results?.toInt() ?: 0

        Log.i("HOLA", "LAST CODE: $lastCode")
        Log.i("HOLA", "RANGE FROM: ${activity.rangeFrom}")
        Log.i("HOLA", "RANGE FROM: ${activity.rangeTo}")

        val newCode: Int =
                when {
                    lastCode == 0 -> {
                        Log.i("HOLA", "Last code was zero!")
                        activity.rangeFrom
                    }
                    lastCode + 1 > activity.rangeTo -> {
                        Log.i("HOLA", "Hitted last code!")
                        lastCode
                    }
                    else -> {
                        Log.i("HOLA", "New code generated!")
                        lastCode + 1
                    }
                }

        realm.close()

        return newCode
    }

    private fun findActivityById(id: Int): ActivityModel? {

        val realm = Realm.getDefaultInstance()

        val result = realm.where(ActivityModel::class.java)
                .equalTo("id", id)
                .findFirst()

        val activity = realm.copyFromRealm(result!!) ?: null

        realm.close()

        return activity
    }

    private fun findStageById(id: Int): StageModel? {

        val realm = Realm.getDefaultInstance()

        val result = realm.where(StageModel::class.java)
                .equalTo("id", id)
                .findFirst()

        val stage = realm.copyFromRealm(result!!) ?: null

        realm.close()

        return stage
    }




}