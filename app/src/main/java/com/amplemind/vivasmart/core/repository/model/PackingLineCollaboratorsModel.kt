package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class PackingLineCollaboratorsModel (
        @SerializedName("activity_id") val activity_id : Int?,
        @SerializedName("collaborator_id") val collaborator_id : Int?,
        @SerializedName("id") @ColumnInfo(name = "id_collaborator_line") @PrimaryKey(autoGenerate = true) var id : Int?,
        @SerializedName("is_done") val is_done : Boolean?,
        @SerializedName("is_paused") val is_paused : Boolean?,
        @SerializedName("is_time") val is_time : Boolean?,
        @SerializedName("packing_id") val packing_id : Int?,
        @SerializedName("packingline_id") val packingline_id : Int?,
        @SerializedName("temp_time") var temp_time : Long,
        @SerializedName("time_counter") var time_counter : Double?,
        @SerializedName("is_training") @ColumnInfo(name = "training") val isTraining: Boolean? = false,
        @SerializedName("is_practice") @ColumnInfo(name = "practice") val isPractice: Boolean? = false,
        @SerializedName("sign") var sign: String? = "No sign",
        var units : Float?,
        var units_olds : Float?,
        var id_line : Int? = null,
        var isOnlineCollaborator : Boolean = false,
        @Expose(serialize = false, deserialize = false) var date : Long,
        @Embedded @SerializedName("packing") val packing : PresentationResultResponse?,
        @Embedded @SerializedName("collaborator") val collaborator  : CollaboratorsReponse?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readLong(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString(),
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readByte() != 0.toByte(),
            parcel.readLong(),
            parcel.readParcelable(PresentationResultResponse::class.java.classLoader),
            parcel.readParcelable(CollaboratorsReponse::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(activity_id)
        parcel.writeValue(collaborator_id)
        parcel.writeValue(id)
        parcel.writeValue(is_done)
        parcel.writeValue(is_paused)
        parcel.writeValue(is_time)
        parcel.writeValue(packing_id)
        parcel.writeValue(packingline_id)
        parcel.writeLong(temp_time)
        parcel.writeValue(time_counter)
        parcel.writeValue(isTraining)
        parcel.writeValue(isPractice)
        parcel.writeString(sign)
        parcel.writeValue(units)
        parcel.writeValue(units_olds)
        parcel.writeValue(id_line)
        parcel.writeByte(if (isOnlineCollaborator) 1 else 0)
        parcel.writeLong(date)
        parcel.writeParcelable(packing, flags)
        parcel.writeParcelable(collaborator, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PackingLineCollaboratorsModel> {
        override fun createFromParcel(parcel: Parcel): PackingLineCollaboratorsModel {
            return PackingLineCollaboratorsModel(parcel)
        }

        override fun newArray(size: Int): Array<PackingLineCollaboratorsModel?> {
            return arrayOfNulls(size)
        }
    }
}