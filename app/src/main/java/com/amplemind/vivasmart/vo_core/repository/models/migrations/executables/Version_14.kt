package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_14 : MigrationExecutable {
    override fun run(realm: DynamicRealm) {
        migrateUploadObjectModel(realm)
    }

    private fun migrateUploadObjectModel(realm: DynamicRealm){
        realm.schema.get("PulseForTable")?.apply {
            addField("phDrain", Double::class.javaObjectType)
            addField("ceDrain", Double::class.javaObjectType)
            addField("pulseRegisted", Int::class.javaObjectType)
        }

        realm.schema.get("PHCEForTable")?.apply {
            addField("type", String::class.javaObjectType)
        }
    }

    override fun drop(realm: DynamicRealm) {

    }
}