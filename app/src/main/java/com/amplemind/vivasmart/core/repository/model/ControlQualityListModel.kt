package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ControlQualityListModel(@SerializedName("name") var name : String,
                                   @SerializedName("collaborator_id") var collaboratorId: Int,
                                   @SerializedName("employee_code") @PrimaryKey(autoGenerate = false) var employeeCode: String,
                                   @SerializedName("signed") var signed: Int,
                                   @SerializedName("no_signed") var noSigned: Int,
                                   @SerializedName("total") var total: Int,
                                   @SerializedName("is_done") var isDone: Boolean?,
                                   var completed:Boolean = false,
                                   var stageId: Int = 0,
                                   @SerializedName("score") var score: Int = 0 ) {
    constructor() : this("", 0, "", 0, 0, 0, null )
}