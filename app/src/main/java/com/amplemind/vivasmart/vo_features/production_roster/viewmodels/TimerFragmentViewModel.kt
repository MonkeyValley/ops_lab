package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import com.amplemind.vivasmart.vo_core.repository.TimerFragmentRepository
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getActiveTime
import com.amplemind.vivasmart.vo_core.repository.models.extensions.isHarvest
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject

class TimerFragmentViewModel @Inject constructor(
        private val timerFragmentRepository: TimerFragmentRepository
) {

    protected val progressStatus = BehaviorSubject.create<Boolean>()
    protected val requestFail = BehaviorSubject.create<String>()

    lateinit var activityCodeUuid: String

    lateinit var activityCode: ActivityCodeModel

    val activity: ActivityModel
            get() = activityCode.activity ?: throw IllegalStateException("ActivityCode with uuid ${activityCode.uuid} has no Activity object")

    val stage: StageModel
            get() = activityCode.stage ?: throw IllegalStateException("ActivityCode with uuid ${activityCode.uuid} has no Stage object")

    fun cleanUp() {
        timerFragmentRepository.cleanUp()
    }

    fun showProgress(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun showError(): BehaviorSubject<String> {
        return requestFail
    }

    fun canLoadData() = ::activityCodeUuid.isInitialized

    fun loadActivityCode(): Flowable<ActivityCodeModel> =
            timerFragmentRepository.loadActivityCode(activityCodeUuid)
                    .doOnNext{activityCode ->
                        this.activityCode = activityCode
                    }

    fun loadActivityLogs() =
            timerFragmentRepository.loadActiveLogs(activityCodeUuid)

    fun create(collaboratorUuids: Array<String>) =
            timerFragmentRepository.create(activityCode, collaboratorUuids)

    fun setUnits(activityLog: ActivityLogModel, quantity: Int, badQuantity: Int = 0) =
            timerFragmentRepository.setUnits(activityLog, quantity, badQuantity)

    fun startTimer(activityLog: ActivityLogModel) =
            timerFragmentRepository.startTimer(activityLog)

    fun startTimerForAll(activityLogs: List<ActivityLogModel>) =
            timerFragmentRepository.startTimer(activityLogs)

    fun pauseTimer(activityLog: ActivityLogModel) =
            timerFragmentRepository.pauseTimer(activityLog)

    fun stopTimerForAll(activityLogs: List<ActivityLogModel>) =
            timerFragmentRepository.pauseTimer(activityLogs)

    fun remove(activityLog: ActivityLogModel) =
            timerFragmentRepository.remove(activityLog)

    fun removeRosterDetail(activityLog: ActivityLogModel) =
            timerFragmentRepository.removeRosterDetail(activityLog)

    fun finish(activityLog: ActivityLogModel, signature: File? = null) =
            timerFragmentRepository.finish(activityLog, signature)

    fun finishAll(activityLogs: List<ActivityLogModel>) =
            timerFragmentRepository.finish(activityLogs)

    fun canAssignUnits(activityLog: ActivityLogModel) = !activityLog.isPaused

    fun hasCollaborators(activityLogs: List<ActivityLogModel>) = activityLogs.isNotEmpty()

    fun hasCollaboratorsWorking(activityLogs: List<ActivityLogModel>) = activityLogs.any {
        !it.isPaused
    }

    fun hasCollaboratorsWithTime(activityLogs: List<ActivityLogModel>) = activityLogs.any {
        it.getActiveTime() > 0 || !it.isPaused
    }

    fun checkMissingUnits(activityLogs: List<ActivityLogModel>, func: (Int, ActivityLogModel) -> Unit): Boolean {
        var canFinishAll = true
        for ((position: Int, activityLog: ActivityLogModel) in activityLogs.withIndex()) {
            if (!canFinish(activityLog)) {
                canFinishAll = false
                func.invoke(position, activityLog)
            }
        }
        return canFinishAll
    }

    fun canFinish(activityLog: ActivityLogModel): Boolean =
            activityLog.quantity > 0

    fun isGrooveActivity() = activity.isGrooves == true

    fun isHarvestActivity() = activity.isHarvest

}