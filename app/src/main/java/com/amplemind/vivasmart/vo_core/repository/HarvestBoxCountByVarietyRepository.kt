package com.amplemind.vivasmart.vo_core.repository
import com.amplemind.vivasmart.vo_core.repository.dao.StagesDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel
import io.realm.Realm
import javax.inject.Inject

class HarvestBoxCountByVarietyRepository @Inject constructor(val stagesDao: StagesDao) {

    fun loadVarieties(stageUuid: String) =
            stagesDao.findByUuid(stageUuid)
                    ?.asFlowable<StageModel>()
                    ?.map(this::extractVarieties)


    private fun extractVarieties(stage: StageModel): List<VarietyModel>  {
        Realm.getDefaultInstance().use {
            val varieties = mutableListOf<VarietyModel>()

            val result = it
                    .where(VarietyModel::class.java)
                    .equalTo("stageId", stage.id)
                    .findAll()

            result.forEach { variety ->
                if (variety?.isCommercial == true) {
                    varieties.add(0, variety)
                } else if (variety != null) {
                    varieties.add(variety)
                }
            }

            return varieties
        }
    }

}