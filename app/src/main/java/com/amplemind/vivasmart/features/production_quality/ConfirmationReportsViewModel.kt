package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.ProductionQualityRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ProductionCategoriesReportModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject


class ConfirmationReportsViewModel @Inject constructor(private val pref: UserAppPreferences,
                                                       private val repository: ProductionQualityRepository,
                                                       private val preferences: UserAppPreferences,
                                                       private val uploadRepository: ProfileRepository,
                                                       private val apiErrors: HandleApiErrors) {

    private val reports = mutableListOf<ConfirmationReportsItemViewModel>()
    private val dataSubject = BehaviorSubject.create<MutableList<ConfirmationReportsItemViewModel>>()
    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val requestDeleteIssue = BehaviorSubject.create<Pair<Int,Int>>()
    private val successUploadSign: BehaviorSubject<MutableList<File>> = BehaviorSubject.create()
    private val successFinishActivity: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val signs = mutableListOf<String>()

    fun loadStaticReports(lotId: Int): Disposable {
        return repository.getIssuesReport(preferences.token, lotId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { list ->
                    repository.saveIssuesReportes(list)
                    mapReports(list.data)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dataSubject.onNext(reports)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun loadLocalStaticReports(lotId: Int, syncFlow: Boolean): Disposable {
        return repository.getLocalIssuesReport(lotId, syncFlow)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { list -> mapReports(list.data) }
                .subscribe({
                    dataSubject.onNext(reports)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun deleteIssue(issueId: Int, tableNo : Int): Disposable {
        return repository.deleteIssue(preferences.token, issueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestDeleteIssue.onNext(Pair(issueId,tableNo))
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun deleteLocalIssue(issueId: Int, tableNo : Int, isLocalIssue : Boolean, categoryId: Int): Disposable {
        return repository.deleteLocalIssue(issueId, isLocalIssue, categoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestDeleteIssue.onNext(Pair(issueId,tableNo))
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun updateSign(files: MutableList<File>): Disposable {
         return uploadRepository.uploadImage(files.first(), "sign")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    if (files.isNotEmpty()) {
                        files.removeAt(0)
                    }
                    signs.add(it.relative_url)
                    successUploadSign.onNext(files)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun finishActivity(lotId: Int): Disposable? {
        if (signs.size >= 2) {
            return repository.finishActivities(preferences.token, signs[0], signs[1], lotId, 1)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { progressStatus.onNext(true) }
                    .doOnTerminate { progressStatus.onNext(false) }
                    .subscribe({
                        successFinishActivity.onNext(true)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                    })
        }
        requestFail.onNext("Son necesarias 2 firmas")
        return null
    }

    fun finishLocalActivity(sign1: String, sign2: String, lotId: Int): Disposable {
        return repository.finishLocalActivity(sign1, sign2, lotId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    successFinishActivity.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapReports(model: ProductionCategoriesReportModel) {
        if (model.table1 != null && model.table1.isNotEmpty()) {
            reports.add(ConfirmationReportsItemViewModel(pref, "Tabla 1", position_item = ""))
            var position = 1
            model.table1.map {
                reports.add(ConfirmationReportsItemViewModel(pref, null, it, position.toString()))
                position ++
            }
        }
        if (model.table2 != null && model.table2.isNotEmpty()) {
            reports.add(ConfirmationReportsItemViewModel(pref, "Tabla 2", position_item = ""))
            var position = 1
            model.table2.map {
                reports.add(ConfirmationReportsItemViewModel(pref, null, it, position.toString()))
                position ++
            }
        }
        if (model.table3 != null && model.table3.isNotEmpty()) {
            reports.add(ConfirmationReportsItemViewModel(pref, "Tabla 3", position_item = ""))
            var position = 1
            model.table3.map {
                reports.add(ConfirmationReportsItemViewModel(pref, null, it, position.toString()))
                position ++
            }
        }
        if (model.table4 != null && model.table4.isNotEmpty()) {
            reports.add(ConfirmationReportsItemViewModel(pref, "Tabla 4", position_item = ""))
            var position = 1
            model.table4.map {
                reports.add(ConfirmationReportsItemViewModel(pref, null, it, position.toString()))
                position ++
            }
        }
        if (model.table5 != null && model.table5.isNotEmpty()) {
            reports.add(ConfirmationReportsItemViewModel(pref, "Tabla 5", position_item = ""))
            var position = 1
            model.table5.map {
                reports.add(ConfirmationReportsItemViewModel(pref, null, it, position.toString()))
                position ++
            }
        }
        if (model.table6 != null && model.table6.isNotEmpty()) {
            reports.add(ConfirmationReportsItemViewModel(pref, "Tabla 6", position_item = ""))
            var position = 1
            model.table6.map {
                reports.add(ConfirmationReportsItemViewModel(pref, null, it, position.toString()))
                position ++
            }
        }
    }

    fun getReport(id: Int): ConfirmationReportsItemViewModel {
        return reports.filter { it.issueId == id }.first()
    }

    fun getData(): BehaviorSubject<MutableList<ConfirmationReportsItemViewModel>> {
        return dataSubject
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onDeleteIssue(): BehaviorSubject<Pair<Int, Int>> {
        return requestDeleteIssue
    }

    fun isHeaderView(position: Int): Boolean {
        return reports[position].isHeader()
    }

    fun getReportAtPosition(position: Int): ConfirmationReportsItemViewModel? {
        if (position < reports.size) {
            return reports[position]
        }
        return null
    }

    fun getSignatureTitle(): String {
        return "Terminar reporte"
    }

    fun getSignatureMessage(): String {
        return "¿Estas seguro de querer enviar el reporte? Por favor firme para confirmar:"
    }

    fun onSuccessUpdateSign(): BehaviorSubject<MutableList<File>> {
        return successUploadSign
    }

    fun onSuccessFinishActivity(): BehaviorSubject<Boolean> {
        return successFinishActivity
    }

}