package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

data class HaulageSignRequest(@SerializedName("sign_collaborator") val sign: String,
                              @SerializedName("sign_super") val supervisorSign: String)