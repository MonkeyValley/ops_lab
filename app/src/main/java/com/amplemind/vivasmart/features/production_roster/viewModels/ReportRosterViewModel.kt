package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.vo_core.repository.ReportRosterRepository
import javax.inject.Inject

class ReportRosterViewModel @Inject constructor (
        private val repository: ReportRosterRepository
) {

    lateinit var stageUuid: String
    var stageTypeId: Int = 0

    fun cleanUp() {
        repository.cleanUp()
    }

    fun hasPeopleWorking() =
            repository.hasPeopleWorking(stageUuid, stageTypeId)

    fun hasPeopleWorkingDailyPay() =
            repository.hasPeopleWorkingDailyPay(stageUuid, stageTypeId)

    fun hasUnsignedLogs() =
            repository.hasUnsignedLogs(stageUuid, stageTypeId)

    fun hasDailyPayCollaboratorsPending() =
            repository.hasDailyPayCollaboratorsPending(stageUuid, stageTypeId)

    fun hasUnsignedLogsDailyPay() =
            repository.hasUnsignedLogsDailyPay(stageUuid, stageTypeId)

    fun setAllAsDone() =
            repository.setAsDone(stageUuid, stageTypeId)

    fun setAllAsDoneDailyPay() =
            repository.setAsDoneDailyPay(stageUuid, stageTypeId)

    fun closeStage() =
            repository.closeStage(stageUuid, stageTypeId)
}