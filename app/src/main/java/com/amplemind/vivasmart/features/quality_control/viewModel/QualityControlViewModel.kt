package com.amplemind.vivasmart.features.quality_control.viewModel

import com.amplemind.vivasmart.core.repository.LotRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class QualityControlViewModel @Inject constructor(private val repository : LotRepository,
                                                  private val preferences: UserAppPreferences,
                                                  private val apiErrors: HandleApiErrors){

    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val getLots: BehaviorSubject<List<ControlQualityItemViewModel>> = BehaviorSubject.create()
    private val requestFail: BehaviorSubject<String> = BehaviorSubject.create()
    enum class LOT_FLOW {
        QUALITY_CONTROL,
        QUALITY_PRODUCTION,
        ROSTER_PRODUCTION
    }
    var flow = LOT_FLOW.ROSTER_PRODUCTION

    fun getLots(): Disposable? {
        val user = GsonBuilder().create().fromJson(preferences.userInfo, UserModel::class.java)
        return repository.getLots(preferences.token, user.businessUnitId.toString())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { it ->
                    //repository.saveStages(it.data)
                    mapModelToViewModels(it)
                }
                .observeOn(AndroidSchedulers.mainThread())

                .subscribe({
                    getLots.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    /*
    fun getLocalLots(): Disposable {
        return repository.getStages()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelToViewModels(it) }
                .subscribe({
                    getLots.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }
    */

    fun userHasBusinessUnitId(): Boolean {
        val user = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        return user.businessUnitId != null
    }

    private fun mapModelToViewModels(models: List<StageModel>): List<ControlQualityItemViewModel> {
        val list = arrayListOf<ControlQualityItemViewModel>()
        for (item in models) {
            list.add(ControlQualityItemViewModel(item, flow))
        }
        return list
    }

    private fun mapModelToViewModels(response: LotRepository.LotResponse): List<ControlQualityItemViewModel> {
        return mapModelToViewModels(response.data)
    }

    fun onGetLots(): BehaviorSubject<List<ControlQualityItemViewModel>> {
        return getLots
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

}
