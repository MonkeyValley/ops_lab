package com.amplemind.vivasmart.core.repository.local

import android.content.Context
import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.vo_core.repository.models.realm.UserModel
import com.google.gson.Gson

open class UserAppPreferences(
        context: Context,
        private val mGson: Gson?,
        private val mApiAuth: ApiAuthorization?
) {

    private var NAME = context.getString(R.string.app_name)

    private var prefer = context.getSharedPreferences(NAME, Context.MODE_PRIVATE)

    var agreedTutorial: Boolean
        get() = prefer.getBoolean("agreed_tutorial", false)
        set(value) = prefer.edit().putBoolean("agreed_tutorial", value).apply()

    private var mCachedUser: UserModel? = null
    private var mCachedAuth: String? = null

    val user: UserModel
        @Synchronized
        get() {
            if (mCachedUser == null) {
                val userInfo = this.userInfo

                if (userInfo.isEmpty()) {
                    throw UndefinedException("No user info defined at this moment")
                }

                mCachedUser = mGson!!.fromJson(userInfo, UserModel::class.java)
            }
            return mCachedUser!!
        }

    val authorizationToken: String
        @Synchronized
        get() {
            if (mCachedAuth == null) {
                val token = this.token

                if (token.isEmpty()) {
                    throw UndefinedException("No token defined at this moment")
                }

                mCachedAuth = mApiAuth!!.getAuthToken(token)
            }

            return mCachedAuth!!
        }

    var token: String
        get() = prefer.getString("token", "") ?: ""
        set(value) {
            prefer.edit().putString("token", value).apply()
            mCachedAuth = null
        }

    var flow: String
        get() = prefer.getString("flow", "") ?: ""
        set(value) = prefer.edit().putString("flow", value).apply()

    var userInfo: String
        get() = prefer.getString("userInfo", "") ?: ""
        set(value) {
            prefer.edit().putString("userInfo", value).apply()
            mCachedUser = null
        }

    var activityCodeId: Int
        get() = prefer.getInt("activity_code_id", 0)
        set(value) = prefer.edit().putInt("activity_code_id", value).apply()

    var activityCode: Int
        get() = prefer.getInt("activity_code", 0)
        set(value) = prefer.edit().putInt("activity_code", value).apply()

    var isOnline: Boolean
        get() = prefer.getBoolean("is_online", true)
        set(value) = prefer.edit().putBoolean("is_online", value).apply()

    var agreedPlanningTutorial: Boolean
        get() = prefer.getBoolean("planning_tutorial", true)
        set(value) = prefer.edit().putBoolean("planning_tutorial", value).apply()

    var serverUrl: String
        get() = prefer.getString("server_url", BuildConfig.SERVER_URL_DEV) ?: ""
        set(value) = prefer.edit().putString("server_url", value).apply()

    var customServerUrl: String
        get() = prefer.getString("custom_server_url", "") ?: ""
        set(value) = prefer.edit().putString("custom_server_url", value).apply()

    var serverUrlIsPreset: Boolean
        get() = prefer.getBoolean("server_url_is_preset", true)
        set(value) = prefer.edit().putBoolean("server_url_is_preset", value).apply()

    val selectedServerUrl: String
        get() = if (serverUrlIsPreset) serverUrl else customServerUrl

    val serverImageStorage: String
        get() = "https://backend-filestorage.s3-us-west-1.amazonaws.com/"

    var syncStatus: String
        get() = prefer.getString("sync_status", "")!!
        set(value) = prefer.edit().putString("sync_status", value).apply()

    var sync: String
        get() = prefer.getString("sync", "")!!
        set(value) = prefer.edit().putString("sync", value).apply()

    var table: Boolean
        get() = prefer.getBoolean("table", false)
        set(value) = prefer.edit().putBoolean("table", value).apply()

    class UndefinedException(msg: String) : Exception(msg)
}