package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.navegation.data_mediator.MediatorNavigation
import com.amplemind.vivasmart.features.planning.PlanningActivity
import com.amplemind.vivasmart.features.planning.viewmodels.PlanningViewModel
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity.Companion.ADD_COLLABORATORS_BOX
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity.Companion.CHANGE_LOT
import com.amplemind.vivasmart.features.scannerbarcode.ReadBarcodeActivity
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.activity_timer_lines.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface ICountHasBox {
    fun showCountBox(show : Pair<Boolean, Int>)
}

class TimerLinesActivity : BaseActivityWithFragment() , ICountHasBox {

    private val COLLABORATORS_INTENT = 1

    @Inject
    lateinit var navigationMediator: MediatorNavigation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer_lines)

        navigationMediator.clear()

        setupToolbar()
        onEvents()

        setupTimerFragment()
    }

    private fun onEvents() {
        RxView.clicks(btn_change_lot).throttleFirst(1, TimeUnit.SECONDS).subscribe {
            fab_menu_lines.collapse()
            haveCountBox(CHANGE_LOT)
        }.addTo(subscriptions)

        RxView.clicks(btn_count_box).throttleFirst(1, TimeUnit.SECONDS).subscribe {
            haveCountBox(BoxCountLinesActivity.BOX_COUNT)
            fab_menu_lines.collapse()
        }.addTo(subscriptions)

        RxView.clicks(btn_add_users_lines).throttleFirst(1, TimeUnit.SECONDS).subscribe {
            fab_menu_lines.collapse()
            haveCountBox(ADD_COLLABORATORS_BOX)
        }.addTo(subscriptions)

        RxView.clicks(btn_report_roster).throttleFirst(1, TimeUnit.SECONDS).subscribe {
            val activityIntent = Intent(this, ReportRosterActivity::class.java)
            //activityIntent.putExtra("flow", RosterDetailActivity.ReportDetailFlow.PACKING_LINE.type)
            activityIntent.putExtra("stage_id", getLotID())
            activityIntent.putExtra("id", intent.getIntExtra("line_id", 0))
            startActivity(activityIntent)
            fab_menu_lines.collapse()
        }.addTo(subscriptions)

        RxView.clicks(btn_planning).throttleFirst(1, TimeUnit.SECONDS).subscribe {
            val activityIntent = Intent(this, PlanningActivity::class.java)
            activityIntent.putExtra("flow", PlanningViewModel.PlanningFlow.LINES.type)
            activityIntent.putExtra("id", intent.getIntExtra("line_id", 0))
            startActivity(activityIntent)
        }.addTo(subscriptions)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getNameLine()
    }

    private fun getNameLine(): String {
        return intent.getStringExtra("name_line") ?: ""
    }

    private fun getLotID(): Int {
        return intent.getIntExtra("lot_id", -1)
    }

    private fun haveCountBox(type: Int) {
        val fragment = supportFragmentManager.findFragmentById(R.id.content_fragment_timer)
        if (fragment != null && fragment is TimerLinesFragment) {
            fragment.isAllPause(type)
        }
    }

    override fun showCountBox(show: Pair<Boolean, Int>) {
        startCountActivity(show.first, show.second)
    }

    private fun startCountActivity(show : Boolean, type : Int){
        if (show) {
            startActivityForResult(Intent(this, BoxCountLinesActivity::class.java)
                    .putExtra("type", type)
                    .putExtra("name_line", getNameLine())
                    .putExtra("lot_id", getLotID()),
                    COLLABORATORS_INTENT)
            return
        }
        startBoxCountLines(type)
    }

    private fun startBoxCountLines(type: Int) {
        when (type) {
            ADD_COLLABORATORS_BOX -> {
                startActivity(Intent(this, ReadBarcodeActivity::class.java).putExtra("showActivities", false))
            }
            CHANGE_LOT -> {
                startActivity(Intent(this, ChangeLotActivity::class.java)
                        .putExtra("lot_id", getLotID())
                        .putExtra("name_line", getNameLine()))
            }
        }
    }

    private fun setupTimerFragment() {
        val fragment = TimerLinesFragment.newInstance(getNameLine(), getLotID(), this)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.content_fragment_timer, fragment)
                .commit()
    }

    fun showArrow() {
        empty_view_lines.visibility = View.VISIBLE
    }

    fun hideArrow() {
        empty_view_lines.visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return false
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
