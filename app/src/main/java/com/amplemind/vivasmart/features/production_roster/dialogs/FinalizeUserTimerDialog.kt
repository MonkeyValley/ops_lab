package com.amplemind.vivasmart.features.production_roster.dialogs

import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.custom_views.Signature
import com.amplemind.vivasmart.core.custom_views.SignatureListener
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.EDITED_LOG_IN_TIMER
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.core.utils.GROOVES_UPDATE_REQUEST
import com.amplemind.vivasmart.core.utils.VALIDATE_NO_SIGNATURE
import com.amplemind.vivasmart.features.collaborators.AssignGroovesActivity
import com.amplemind.vivasmart.features.production_roster.EditQuantityFragment
import com.amplemind.vivasmart.features.production_roster.OnEditQuantity
import com.amplemind.vivasmart.features.production_roster.RosterDetailViewModel
import com.amplemind.vivasmart.features.production_roster.SelectedTypeEdit
import com.amplemind.vivasmart.vo_core.repository.models.extensions.*
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.utils.formatTime
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.FinalizeUserTimerViewModel
import com.jakewharton.rxbinding2.view.clicks
import de.hdodenhof.circleimageview.CircleImageView
import javax.inject.Inject

class FinalizeUserTimerDialog : BaseDialogFragment(), CompoundButton.OnCheckedChangeListener {


    companion object {

        val TAG = FinalizeUserTimerDialog::class.java.simpleName

        val PARAM_ACTIVITY_LOG_UUID  = "$TAG.ActivityLog"

        fun newInstance(activityLogUuid: String): FinalizeUserTimerDialog {
            val dialog = FinalizeUserTimerDialog()
            val args = Bundle()
            args.putString(PARAM_ACTIVITY_LOG_UUID, activityLogUuid)
            dialog.arguments = args
            return dialog
        }

        fun newInstance(title: String, message: String, userName: String,
                        time: String, payment: String, totalWork: String, isSection: Boolean = false,
                        unitName: String, trainingPayment: String,
                        practicePayment: String,
                        isTrainingEnabled: Boolean,
                        isBonoEnabled: Boolean = false, name_line : String = "",
                        isPractice: Boolean = false): FinalizeUserTimerDialog {

            val dialog = FinalizeUserTimerDialog()
            val args = Bundle()
            args.putString("title", title)
            args.putString("message", message)
            args.putString("user_name", userName)
            args.putString("time", time)
            args.putString("payment", payment)
            args.putString("trainingPayment", trainingPayment)
            args.putString("practicePayment", practicePayment)
            args.putString("total_work", totalWork)
            args.putBoolean("isSection", isSection)
            args.putString("name_unit", unitName)
            args.putString("name_line", name_line)
            args.putBoolean("is_training_enabled", isTrainingEnabled)
            args.putBoolean("is_bono_enabled", isBonoEnabled)
            args.putBoolean("is_practice", isPractice)
            dialog.arguments = args
            return dialog
        }

    }

    @Inject
    lateinit var mViewModel: FinalizeUserTimerViewModel

    @Inject
    lateinit var mEventBus: EventBus

    @Inject
    lateinit var viewModel: RosterDetailViewModel

    var listener: OnFinalizeCollaborator? = null

    // - Mark Properties
    private var skipSignature = true

    // - Mark View Variables
    private lateinit var payment: TextView
    private lateinit var switchTraining: SwitchCompat
    private lateinit var switchPractice: SwitchCompat
    private lateinit var switchControl: SwitchCompat
    private lateinit var tvUserName: TextView
    private lateinit var profileImage: CircleImageView
    private lateinit var mView: View
    private lateinit var activityLogOld: ActivityLogModel
    private var timeOld: Int = 0
    private var quantityOld: Int = 0

    private var signType: TextView? = null
    private var signature: Signature? = null

    private var mSnackbar: Snackbar? = null

    var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        readArguments()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mView = LayoutInflater.from(context).inflate(R.layout.content_finalize_user_timer, null, false)


        builder.setView(mView)

        builder.setPositiveButton(getString(R.string.skip), null)
        builder.setNegativeButton(getString(R.string.cancel), null )
        builder.setNeutralButton(getString(R.string.no_signature), null)

        dialog =  builder.create()

        EDITED_LOG_IN_TIMER = false
        setUpUI(dialog!!)
        setUpUICallbacks(dialog!!)
        loadData()

        return dialog!!
    }

    override fun onDestroyView() {
        super.onDestroyView()
        VALIDATE_NO_SIGNATURE = false
    }

    private fun readArguments() {
        mViewModel.activityLogUuid = requireNotNull(arguments?.getString(PARAM_ACTIVITY_LOG_UUID))
    }

    private fun setUpUI(dialog: Dialog) {
        payment = mView.findViewById(R.id.tv_payment)
        signature = mView.findViewById(R.id.signature)
        signType = mView.findViewById(R.id.signature_type)
        switchTraining = mView.findViewById(R.id.sw_is_training)
        switchPractice = mView.findViewById(R.id.sw_is_practice)
        switchControl = mView.findViewById(R.id.sw_is_control)
        tvUserName = mView.findViewById(R.id.tv_user_name)
        profileImage = mView.findViewById(R.id.profileImage)
    }

    private fun setUpUICallbacks(dialog: Dialog) {
        switchTraining.setOnCheckedChangeListener(this)
        switchPractice.setOnCheckedChangeListener(this)
        switchControl.setOnCheckedChangeListener(this)

        mView.findViewById<ImageView>(R.id.btn_reload).setOnClickListener {
            signature!!.clear()
            skipSignature = true
            (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).text = getString(R.string.skip)
        }

        signature?.setOnSignatureListener(object: SignatureListener{
            override fun onDraw() {
                if (skipSignature) {
                    skipSignature = false
                    (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).text = getString(R.string.finalize)
                }
            }
        })

        dialog.setCancelable(false)
        dialog.setOnShowListener {
            (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE).apply {
                setTextColor(Color.RED)
                clicks()
                        .subscribe {
                            if(EDITED_LOG_IN_TIMER) {
                                viewModel.setQuantityAndTime(activityLogOld, quantityOld, 0)
                                        .subscribe({
                                            listener?.onCancel()
                                            dialog.dismiss()
                                        }, {
                                            showSnackError(it.localizedMessage)
                                            listener?.onCancel()
                                            dialog.dismiss()
                                        })
                                        .addTo(subscriptions)
                            } else {
                                listener?.onCancel()
                                dialog.dismiss()
                            }
                        }
                        .addTo(subscriptions)
            }

            dialog.getButton(AlertDialog.BUTTON_POSITIVE).apply {
                clicks()
                        .subscribe {
                            val signature = if (skipSignature) null else signature?.save()
                            listener?.onFinalize(mViewModel.activityLog, signature)
                            dialog.dismiss()
                        }
                        .addTo(subscriptions)
            }

            dialog.getButton(AlertDialog.BUTTON_NEUTRAL).apply {
                setTextColor(Color.GREEN)
                clicks()
                        .subscribe {
                            val builder = android.app.AlertDialog.Builder(context)
                            builder.setMessage("¿Seguro que desea enviar sin firmar al colaborador?")
                            builder.setPositiveButton("Enviar") { msjDialog, _ ->
                                val signature = null
                                VALIDATE_NO_SIGNATURE = true
                                listener?.onFinalize(mViewModel.activityLog, signature)
                                msjDialog.dismiss()
                                dialog.dismiss()
                            }
                            builder.setNegativeButton(android.R.string.no) { msjDialog, _ -> msjDialog.dismiss() }
                            builder.show()
                        }
                        .addTo(subscriptions)
            }
        }

    }

    override fun loadData() {
        mViewModel.loadActivityLog().subscribe(this::onActivityLogLoaded).addTo(subscriptions)
    }

    private fun onActivityLogLoaded(activityLog: ActivityLogModel) {
        mView.findViewById<TextView>(R.id.title_signature).text = context?.getText(R.string.futd_title)
        mView.findViewById<TextView>(R.id.message_signature).text = context?.getText(R.string.futd_message)
        mView.findViewById<TextView>(R.id.tv_user_name).text = activityLog.getCollaboratorName(context!!)

        mView.findViewById<TextView>(R.id.tv_time).text =
                if(!EDITED_LOG_IN_TIMER)
                    formatTime(activityLog.getActiveTime())
                else
                    formatTime(activityLog.getActiveOnlyTime())

        mView.findViewById<TextView>(R.id.tv_work).text = activityLog.quantity.toString()
        mView.findViewById<TextView>(R.id.label_time).text = activityLog.getUnitName(context!!)

        if(!EDITED_LOG_IN_TIMER) {
            activityLogOld = activityLog
            var timeOld = activityLog.time.toInt()
            quantityOld = activityLog.quantity
        }


        tvUserName.setOnLongClickListener {
            editElement(activityLog)
            true
        }

        profileImage.setOnLongClickListener {
            editElement(activityLog)
            true
        }

        switchPractice.isEnabled = activityLog.isPracticeAvailable()
        switchTraining.isEnabled = activityLog.isTrainingAvailable()
        switchControl.isEnabled = activityLog.isControlAvailable()

        displayPayment()
    }

    private fun switchValidation(switch: SwitchCompat) {

        if (switch.isChecked) {

            showPaymentModeWarningMesssage(switch)

            when (switch) {
                switchControl -> {
                    switchPractice.isChecked = false
                    switchTraining.isChecked = false
                }
                switchTraining -> {
                    switchPractice.isChecked = false
                    switchControl.isChecked = false
                }
                //switchPractice
                else -> {
                    switchTraining.isChecked = false
                    switchControl.isChecked = false
                }
            }
        }


    }

    private fun displayPayment() {
        payment.text = mViewModel.getFormattedPayment()
    }

    private fun showPaymentModeWarningMesssage(switch: SwitchCompat) {

        if (mSnackbar?.isShown == true) {
            mSnackbar?.dismiss()
        }

        val msgId = when (switch) {
            switchTraining -> if (switch.isChecked)
                R.string.training_activated
            else
                R.string.training_deactivated
            switchPractice -> if (switch.isChecked)
                R.string.practice_activated
            else
                R.string.practice_deactivated
            //switchControl
            else -> if (switch.isChecked)
                R.string.control_activated
            else
                R.string.control_deactivated
        }

        showSnackError(getString(msgId))
    }

    fun showSnackError(msg: String){
        mSnackbar = Snackbar.make(mView, msg, Snackbar.LENGTH_LONG)
        val snackText = mSnackbar?.view?.findViewById<TextView>(R.id.snackbar_text)
        mSnackbar?.view?.setBackgroundColor(ContextCompat.getColor(context!!, R.color.bgAlert))
        snackText?.setTextColor(Color.WHITE)
        snackText?.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context!!, R.drawable.ic_warning), null)
        mSnackbar?.view?.layoutParams?.width = ViewGroup.LayoutParams.MATCH_PARENT
        mSnackbar?.show()
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        switchValidation(buttonView as SwitchCompat)

        mViewModel.isControl = switchControl.isChecked
        mViewModel.isTraining = switchTraining.isChecked
        mViewModel.isPractice = switchPractice.isChecked

        displayPayment()
    }

    interface OnFinalizeCollaborator {
        fun onFinalize(activityLog: ActivityLogModel, signature: Bitmap?)
        fun onCancel()
    }

    private fun editElement(activityLog: ActivityLogModel) {
        if (activityLog.activityCode?.activity?.isGrooves == true) editGrooveOrHrs(activityLog)
        else showEditUnitiesDialog(activityLog)
    }

    private fun editGrooveOrHrs(activityLog: ActivityLogModel) {
        val dialog = SelectedTypeEdit().newInstance()
        dialog.show(this.fragmentManager!!, SelectedTypeEdit.TAG)
        dialog.setListener(object : SelectedTypeEdit.OnSelectedTypeEdit {
            override fun onEditGroove() = editGrooves(activityLog)
            override fun onEditTime() = showEditUnitiesDialog(activityLog, true)
        })
    }

    private fun editGrooves(activityLog: ActivityLogModel) {
        startActivityForResult(Intent(activity!!, AssignGroovesActivity::class.java)
                .putExtra(AssignGroovesActivity.PARAM_ACTIVITY_CODE_UUID, activityLog.activityCodeUuid)
                .putExtra(AssignGroovesActivity.PARAM_ACTIVITY_LOG_UUID, activityLog.uuid),
                GROOVES_UPDATE_REQUEST)
    }

    private fun showEditUnitiesDialog(activityLog: ActivityLogModel, onlyTime: Boolean = false) {
        val dialog = EditQuantityFragment.newInstance(
                activityLog.id.toString(),
                activityLog.collaborator?.name ?: getString(R.string.default_collaborator_name),
                activityLog.quantity,
                if(!EDITED_LOG_IN_TIMER) activityLog.getActiveHours()
                else activityLog.getActiveOnlyHours(),
                onlyTime
        )

        dialog.show(this.fragmentManager!!, EditQuantityFragment.TAG)
        dialog.setOnEditQuantity(object : OnEditQuantity {
            override fun onCancel() = dialog.dismiss()
            override fun onEditItem(activityLogId: String, quantity: Int, timeInMinutes: Int) {
                viewModel.setQuantityAndTime(activityLog, quantity, timeInMinutes)
                        .subscribe({
                            switchControl.isChecked = false
                            switchPractice.isChecked = false
                            switchTraining.isChecked = false
                            displayPayment()
                            dialog.dismiss()
                            EDITED_LOG_IN_TIMER = true
                            loadData()
                        }, { showSnackError(it.localizedMessage) })
                        .addTo(subscriptions)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        switchTraining.isChecked = false
        switchPractice.isChecked = false
        switchControl.isChecked = false
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

}

