package com.amplemind.vivasmart.features.production_roster.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.DialogFragment
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.ToggleButton
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment


class PauseTimerDialog : BaseDialogFragment(), CompoundButton.OnCheckedChangeListener {

    companion object {
        val TAG = PauseTimerDialog::class.java.simpleName!!
    }

    var toggle_bathroom: ToggleButton? = null
    var toggle_food: ToggleButton? = null
    var toggle_other: ToggleButton? = null

    fun newInstance(): PauseTimerDialog {
        return PauseTimerDialog()
    }

    private var eventListener : OnPauseTimerListenner? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_pause_timer_dialog, null, false)
        builder.setView(mView)

        toggle_bathroom = mView.findViewById(R.id.btn_bathroom)
        toggle_food = mView.findViewById(R.id.btn_food)
        toggle_other = mView.findViewById(R.id.btn_other)

        toggle_bathroom!!.setOnCheckedChangeListener(this)
        toggle_food!!.setOnCheckedChangeListener(this)
        toggle_other!!.setOnCheckedChangeListener(this)

        builder.setPositiveButton("PAUSAR", null)
        builder.setNegativeButton("CANCELAR") { _, _ -> dismiss() }

        val create = builder.create()

        create.setOnShowListener {
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if (toggle_bathroom!!.isChecked) {
                            eventListener!!.pauseTimer(0)
                        }
                        if (toggle_food!!.isChecked) {
                            eventListener!!.pauseTimer(0)
                        }
                        if (toggle_other!!.isChecked) {
                            eventListener!!.pauseTimer(0)
                        }
                        val snackbar = Snackbar.make(mView, "Seleccione un elemento", Snackbar.LENGTH_SHORT)
                        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
                        snackbar.view.findViewById<TextView>(R.id.snackbar_text)
                                .setTextColor(Color.WHITE)
                        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                        snackbar.show()
                    }

        }

        return create
    }


    override fun onCheckedChanged(view: CompoundButton?, isChecked: Boolean) {
        when (view!!.id) {
            R.id.btn_bathroom -> {
                if (isChecked) {
                    toggle_food!!.isChecked = false
                    toggle_other!!.isChecked = false
                }
            }

            R.id.btn_food -> {
                if (isChecked) {
                    toggle_bathroom!!.isChecked = false
                    toggle_other!!.isChecked = false
                }
            }

            R.id.btn_other -> {
                if (isChecked) {
                    toggle_bathroom!!.isChecked = false
                    toggle_food!!.isChecked = false
                }
            }
        }
    }

    fun setListener(listener : OnPauseTimerListenner){
        eventListener = listener
    }

    interface OnPauseTimerListenner {
        fun pauseTimer(type: Int)
    }

}