package com.amplemind.vivasmart.core.dagger.module.fragment

import com.amplemind.vivasmart.core.base.dagger.scope.FragmentScope
import com.amplemind.vivasmart.features.production_roster.ChooseActivityFragment
import com.amplemind.vivasmart.features.production_roster.fragments.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ExtraHoursFragmentProvider {

    @ContributesAndroidInjector()
    @FragmentScope
    internal abstract fun bindExtraHoursFragment(): ExtraHoursTabHostFragment

    @ContributesAndroidInjector()
    @FragmentScope
    internal abstract fun bindExtraHoursAuthorizationFragment(): ExtraHoursAuthorizationFragment

    @ContributesAndroidInjector()
    @FragmentScope
    internal abstract fun bindExtraHoursRequestFragment(): ExtraHoursRequestsFragment

    @ContributesAndroidInjector()
    @FragmentScope
    internal abstract fun bindExtraHoursChooseActivityFragment(): ChooseActivityFragment

    @ContributesAndroidInjector()
    @FragmentScope
    internal abstract fun bindExtraHoursCreateRequestFragment(): ExtraHoursCreateRequestFragment

    @ContributesAndroidInjector()
    @FragmentScope
    internal abstract fun bindExtraHoursInboxFragment(): ExtraHoursInboxFragment
}