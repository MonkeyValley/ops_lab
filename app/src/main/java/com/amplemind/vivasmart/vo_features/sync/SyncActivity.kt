package com.amplemind.vivasmart.vo_features.sync

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.main.MainActivity
import com.amplemind.vivasmart.features.sync_forced.viewModel.SyncForcedViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_features.services.sync.SyncDownloadModule
import kotlinx.android.synthetic.main.activity_sync.*
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject


class SyncActivity : BaseActivity() {
    @Inject
    lateinit var mDownloadModule: SyncDownloadModule

    @Inject
    lateinit var forcedViewModel: SyncForcedViewModel

    private var mDetailsDialog: Dialog? = null

    lateinit var preferences: UserAppPreferences

    companion object {

        val TAG = SyncActivity::class.simpleName

        val EXTRA_FIRST_SYNC = "$TAG.FirstSync"
        val EXTRA_SYNC = "$TAG.Sync"

        fun startForFirstSync(context: Context) {
            val intent = Intent(context, SyncActivity::class.java).apply {
                putExtra(EXTRA_FIRST_SYNC, true)
                putExtra(EXTRA_SYNC, "sync")
            }
            context.startActivity(intent)
        }

    }

    private var mIsFirstSync: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sync)

        mIsFirstSync = intent?.getBooleanExtra(EXTRA_FIRST_SYNC, false) ?: false

        preferences = UserAppPreferences(this, null, null)

        preferences.sync = intent?.getStringExtra(EXTRA_SYNC) ?: ""

        setUpCallbacks()

        if (mIsFirstSync) {
            executeSync()
        } else {
            startMainActivity()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (mDetailsDialog?.isShowing == true) {
            mDetailsDialog?.dismiss()
        }

        mDetailsDialog = null
    }

    private fun setUpCallbacks() {

        mDownloadModule.onEvent
                .ofType(SyncDownloadModule.DownloadEvent::class.java)
                .subscribe(this::onDownloadEvent)
                .addTo(subscriptions)

        mDownloadModule.onEvent
                .ofType(SyncDownloadModule.DownloadErrorEvent::class.java)
                .subscribe(this::onDownloadErrorEvent)
                .addTo(subscriptions)

    }

    private fun executeSync() =
            mDownloadModule.download()
                    .subscribe()
                    .addTo(subscriptions)

    private fun onDownloadEvent(event: SyncDownloadModule.DownloadEvent) {
        val humanName = event.requestType.getHumanName(this)
        when (event.eventType) {
            SyncDownloadModule.EventType.NOT_READY -> {
                preferences.syncStatus = "NOT_READY"
                tv_status.text = getString(R.string.sync_activity_waiting_service)
            }
            SyncDownloadModule.EventType.READY -> {
                preferences.syncStatus = "READY"
                tv_status.text = getString(R.string.sync_activity_service_started)
            }
            SyncDownloadModule.EventType.DOWNLOADING -> {
                preferences.syncStatus = "DOWNLOADING"
                tv_status.text = getString(R.string.sync_notif_downloading).format(humanName)
            }
            SyncDownloadModule.EventType.SAVING -> {
                preferences.syncStatus = "SAVING"
                tv_status.text = getString(R.string.sync_activity_saving).format(humanName)
            }
            SyncDownloadModule.EventType.FIRST_SYNC_NEEDED -> {
                preferences.syncStatus = "FISRT_SYNC_NEDED"
                tv_status.text = getString(R.string.sync_activity_sync_needed).format(humanName)
            }
            SyncDownloadModule.EventType.COUNTDOWN -> {
                preferences.syncStatus = "COUNTDOWN"
                val secondsRemaining = (event as SyncDownloadModule.RetryCountdownEvent).remainingSeconds
                tv_status.text = getString(R.string.sync_activity_server_timeout_countdown).format(humanName, secondsRemaining)
            }
            SyncDownloadModule.EventType.COMPLETE -> {
                preferences.syncStatus = "COMPLETE"
                tv_status.text = getString(R.string.sync_activity_finished)
                startMainActivity()
            }
        }
    }

    private fun onDownloadErrorEvent(event: SyncDownloadModule.DownloadErrorEvent) {
        when (event.error) {
            is SocketTimeoutException ->
                tv_status.text = getString(R.string.sync_activity_server_timeout)
            is UnknownHostException ->
                tv_status.text = getString(R.string.sync_activity_connection_error)
            is HttpException ->
                tv_status.text = getString(R.string.sync_activity_server_error)
            else ->
                tv_status.text = getString(R.string.sync_activity_error)
        }
        Log.e(TAG, Log.getStackTraceString(event.error))
    }


    override fun onBackPressed() {

    }

    private fun startMainActivity() {
        Handler().postDelayed(
                {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                },
                1000
        )
    }

}
