package com.amplemind.vivasmart.core.navegation.flow

import android.content.Context

interface State {
    fun changeToPreviousState()
    fun doSomeAwesomeThing(param: String)
    fun getSomeAwesomeThing(): String
    fun startActivity(context: Context)
    fun getFragment()
}