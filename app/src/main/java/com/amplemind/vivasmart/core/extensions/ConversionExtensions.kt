package com.amplemind.vivasmart.core.extensions

import android.content.res.Resources

fun Int.toDp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()
fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()

//TODO: Dejar solo si lleva la misma logica el costo por hora
fun calculatePaymentByHrs(time: Float, cost: Float) : Float{
    val hrs = time.toInt()
    val half_hours = time - hrs

    var payment = hrs * cost
    if (half_hours > 0.75) {
        payment += cost
    }
    return payment
}