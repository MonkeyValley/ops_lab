package com.amplemind.vivasmart.features.production_roster

import android.app.Activity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.features.production_roster.adapters.EditQuantityRosterAdapter
import com.amplemind.vivasmart.features.production_roster.viewModels.EditQuantityRosterViewModel
import kotlinx.android.synthetic.main.activity_edit_quantity_roster.*
import javax.inject.Inject
import androidx.databinding.adapters.TextViewBindingAdapter.setText
import android.widget.TimePicker
import android.app.TimePickerDialog
import android.content.Intent
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_roster.viewModels.ItemEditQuantityRosterViewModel


class EditQuantityRosterActivity : BaseActivity() {

    @Inject
    lateinit var viewModel : EditQuantityRosterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_quantity_roster)

        setupToolbar()
        initRecycler()
        setupClicks()
        setData(viewModel.getItems())
    }

    private fun setupClicks() {
        cv_cancel.setOnClickListener { finish() }
        cv_done.addThrottle().subscribe {
            val intent = Intent()
            intent.putExtra("position", intent.getIntExtra("position", 0))
            intent.putExtra("quantity", viewModel.getQuantityAtIndex(0))
            intent.putExtra("time", viewModel.getTimeAtIndex(0))
            setResult(Activity.RESULT_OK, intent)
            finish()
        }.addTo(subscriptions)
    }

    private fun initRecycler() {
        rv_edit_quantity.hasFixedSize()
        rv_edit_quantity.layoutManager = LinearLayoutManager(this)
        rv_edit_quantity.itemAnimator = DefaultItemAnimator()
        rv_edit_quantity.adapter = EditQuantityRosterAdapter()
    }

    private fun setData(items: MutableList<ItemEditQuantityRosterViewModel>) {
        (rv_edit_quantity.adapter as EditQuantityRosterAdapter).addElements(items)
        (rv_edit_quantity.adapter as EditQuantityRosterAdapter).onClick().subscribe(this::showTimerDialog).addTo(subscriptions)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Editar cantidad"
    }

    private fun showTimerDialog(item: ItemEditQuantityRosterViewModel) {
        val mTimePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener
        { _, selectedHour, selectedMinute ->
            val minutes = if (selectedMinute < 10) "0${selectedMinute}" else "${selectedMinute}"
            item.time = "${selectedHour}:${minutes}"
            rv_edit_quantity.adapter!!.notifyItemChanged(item.position ?: 0)
        }, 0, 0, true)
        mTimePicker.show()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
