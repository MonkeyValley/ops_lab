package com.amplemind.vivasmart.features.planning.viewmodels

import com.amplemind.vivasmart.core.repository.model.PlanningModel

class PlanningItemViewModel constructor(private val model: PlanningModel) {

    val activityName = model.activity.name
    val priority = "${model.priority}"
    val peopleNum = getPeople()
    val unitsNum = getUnits()
    val estimatedHours = getHrs()

    private fun getPeople(): String {
        if (model.days.isEmpty()) return ""
        return "${model.days.first().people}"
    }

    private fun getUnits(): String {
        if (model.days.isEmpty()) return ""
        return "${model.days.first().units}"
    }

    private fun getHrs(): String {
        if (model.days.isEmpty()) return ""
        return "%02d:00".format(model.days.first().time)
    }

}