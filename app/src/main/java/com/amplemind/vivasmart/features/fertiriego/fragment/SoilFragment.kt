package com.amplemind.vivasmart.features.fertiriego.fragment


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.getSpanCount
import com.amplemind.vivasmart.core.extensions.toDp
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_TPYE
import com.amplemind.vivasmart.features.fertiriego.FertirriegoHydroponicReportActivity
import com.amplemind.vivasmart.features.fertiriego.FertirriegoMonitoringLotActivity
import com.amplemind.vivasmart.features.fertiriego.FertirriegoPHCELotActivity
import com.amplemind.vivasmart.features.fertiriego.FertirriegoSoilReportActivity
import com.amplemind.vivasmart.features.fertiriego.adapter.LotsInSoilFertirriegoAdapter
import com.amplemind.vivasmart.features.fertiriego.viewModel.LotSoilFertirriegoItemViewModel
import com.amplemind.vivasmart.features.fertiriego.viewModel.SoilFragmentViewModel
import dagger.android.DispatchingAndroidInjector
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_floor.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SoilFragment : BaseFragment(), LotsInSoilFertirriegoAdapter.Listener {

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    private var mListener: LotsInSoilFertirriegoAdapter.Listener? = null

    @Inject
    lateinit var viewModel: SoilFragmentViewModel

    private var mAdapter = LotsInSoilFertirriegoAdapter()

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    companion object {
        fun ifConected(): Boolean {
            return if (SoilFragment().hasInternet())
                true
            else {
                Toast.makeText(SoilFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

    fun newInstance(title: String, itemCode: String): SoilFragment {
        val args = Bundle()
        args.putString("Title", title)
        args.putString("itemCode", itemCode)

        val fragment = SoilFragment()
        fragment.arguments = args
        return fragment
    }

    var itemCode: String? = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_floor, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecycler()
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getLots()
        viewModel.onSuccessRequest_lots().subscribe(this::addElements).addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun getData() {
        viewModel.getLots()
    }

    fun addElements(data: List<LotSoilFertirriegoItemViewModel>) {
        mAdapter.addElements(data)
    }

    private fun clickItemLot() {
        mAdapter.onClickItem()
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe(this::showLotDetail).addTo(subscriptions)
    }

    private fun showLotDetail(item: LotSoilFertirriegoItemViewModel) {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now
        if(ifConected()) {
            if(FERTIRRIEGO_TPYE == "Monitoreo") {
                val intent= Intent(context, FertirriegoMonitoringLotActivity::class.java).apply {
                    putExtra(FertirriegoMonitoringLotActivity.LOT_ID, item.lot_id.toString())
                    putExtra(FertirriegoMonitoringLotActivity.TAG_NAME, "Monitoreo de suelo")
                    putExtra(FertirriegoMonitoringLotActivity.LOT_NAME, item.lot_name)
                    putExtra(FertirriegoSoilReportActivity.MODE, "suelo")
                }
                startActivity(intent)
            } else if( FERTIRRIEGO_TPYE == "Reporte de fertirriego") {
                val intent  = Intent(context, FertirriegoSoilReportActivity::class.java).apply {
                    putExtra(FertirriegoSoilReportActivity.LOT_ID, item.lot_id.toString())
                    putExtra(FertirriegoSoilReportActivity.TAG_NAME, "Reporte de suelo")
                    putExtra(FertirriegoSoilReportActivity.LOT_NAME, item.lot_name)
                    putExtra(FertirriegoSoilReportActivity.MODE, "suelo")
                }
                startActivity(intent)
            } else {
                val intent  = Intent(context, FertirriegoPHCELotActivity::class.java).apply {
                    putExtra(FertirriegoPHCELotActivity.LOT_ID, item.lot_id.toString())
                    putExtra(FertirriegoPHCELotActivity.TAG_NAME, "CE-pH")
                    putExtra(FertirriegoPHCELotActivity.LOT_NAME, item.lot_name)
                    putExtra(FertirriegoPHCELotActivity.MODE, "suelo")
                }
                startActivity(intent)
            }

        }
    }

    private fun setupRecycler() {
        rv_selection_lots_soil.hasFixedSize()
        rv_selection_lots_soil.layoutManager = GridLayoutManager(context, getSpanCount(context!!))
        rv_selection_lots_soil.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

        mAdapter.listener = this
        rv_selection_lots_soil.adapter = mAdapter
        clickItemLot()
    }

    override fun onGetCounterValue(position: Int): Observable<Int>? {
        return mListener?.onGetCounterValue(position)
    }
}
