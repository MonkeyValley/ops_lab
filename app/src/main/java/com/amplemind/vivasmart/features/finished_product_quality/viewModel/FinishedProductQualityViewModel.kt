package com.amplemind.vivasmart.features.finished_product_quality.viewModel


import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import javax.inject.Inject

class FinishedProductQualityViewModel @Inject constructor(
        private val repository: PackagingQualityRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel(){

}