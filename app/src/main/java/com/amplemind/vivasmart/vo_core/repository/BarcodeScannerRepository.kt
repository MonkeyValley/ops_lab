package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.vo_core.repository.dao.realm.CollaboratorsDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class BarcodeScannerRepository @Inject constructor(
        private val collaboratorsDao: CollaboratorsDao,
        private val mEventBus: EventBus
) : Repository() {

    override fun cleanUp() {
        collaboratorsDao.cleanUp()
    }

    fun findByCode(code: String): Maybe<CollaboratorModel> =
            collaboratorsDao.findByCode(code)

    fun findByCodeX(code: String): Single<CollaboratorModel> =
            collaboratorsDao.findByCodeX(code)

}