package com.amplemind.vivasmart.features.phenology.viewmodel

import com.amplemind.vivasmart.features.phenology.models.local.PhenologyTable

class VarietiesTablesPhenologyItemViewModel(phenology_table: PhenologyTable){
    var table_no = phenology_table.table_no.toString()
    var groove_from = phenology_table.groove_from
    var groove_to = phenology_table.groove_to
    var groove_from_to = (phenology_table.groove_from.toString() + "-" + phenology_table.groove_to.toString())
    var sampling = phenology_table.sampling.toString()
    var is_complete = if (phenology_table.is_complete == null) false else phenology_table.is_complete
    var revision_id = phenology_table.revision_id
    var status = phenology_table.status
    var revised_plants = phenology_table.revised_plants
}


