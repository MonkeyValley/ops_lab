package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import androidx.collection.LongSparseArray
import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivitiesDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.CollaboratorsDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.StagesDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_core.repository.remote.ActivityCodeApi
import com.amplemind.vivasmart.vo_core.utils.currentISODate
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ActivityLogDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mPref: UserAppPreferences,
        private val mActivityCodesApi: ActivityCodeApi,
        private val mCollaboratorsDao: CollaboratorsDao,
        private val mStagesDao: StagesDao,
        private val mActivitiesDao: ActivitiesDao
): RealmDownloader<ActivityCodeModel, ActivityLogModel>(realmHolder, ActivityLogModel::class.java){


    var fromDate: String = currentISODate()

    private val mCollaboratorCache = LongSparseArray<CollaboratorModel>()

    override fun buildServerRequest(parent: ActivityCodeModel, lastVersion: Long): Single<List<ActivityLogModel>> =
            mActivityCodesApi.getActivityLogsForCode(
                    mPref.authorizationToken,
                    activityCodeId = parent.id,
                    sinceDate = fromDate,
                    version = lastVersion
            ).map { response ->
                Log.e("Version: ", lastVersion.toString())
                Log.e("Data-ActicityLog: ", response.data.toString())
                response.data
            }

    override fun beforeSaving(parent: ActivityCodeModel, child: ActivityLogModel): Single<ActivityLogModel> =
            Single.just(
                    child.apply {
                        activityCodeUuid = parent.uuid
                        this.activityCode = parent
                    }
            )
            .flatMap {
                findCollaborator(child.collaboratorId.toLong())
                        .flatMapSingle { collaborator ->
                            Single.fromCallable {
                                child.apply {
                                    this.collaborator = collaborator
                                    collaboratorUuid = collaborator.uuid
                                }
                            }
                        }.onErrorReturnItem(child)
            }
            .flatMap {activityLog ->
                executeTransaction { realm ->
                    activityLog.apply {
                        if (!activityLog.isPending && !activityLog.isDone) {
                            if (!parent.isOpen) {
                                parent.isOpen = true
                            }
                            parent.stage?.let { stage ->
                                mStagesDao.addLogCountAux(realm, stage, 1, parent.activity!!.activitySubCategoryId)
                                parent.activity?.let {activity ->
                                    mActivitiesDao.addLogCountAux(realm, activity, stage, 1)
                                }
                            }
                        }
                    }
                }
            }

    private fun findCollaborator(id: Long): Maybe<CollaboratorModel?> =
            Maybe.fromCallable {
                mCollaboratorCache[id]
            }.switchIfEmpty(
                    mCollaboratorsDao.findById(id)
                            .doOnSuccess { collaborator ->
                                mCollaboratorCache.put(id, collaborator)
                            }
            )

    override fun clearCache() {
        mCollaboratorCache.clear()
    }


}