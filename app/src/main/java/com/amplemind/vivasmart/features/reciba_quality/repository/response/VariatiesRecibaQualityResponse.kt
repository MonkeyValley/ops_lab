package com.amplemind.vivasmart.features.reciba_quality.repository.response

import com.google.gson.annotations.SerializedName

data class VariatiesRecibaQualityResponse (
        @SerializedName("lot_name") val lotName: String,
        @SerializedName("table") val table: String,
        @SerializedName("valid_box_no") val validBoxNo: Double,
        @SerializedName("crop_id") val cropId: Int,
        @SerializedName("varieties") val varietiesDetail: List<VarietiesDetail>
)

data class VarietiesDetail(
        @SerializedName("variety_id") val id: Int,
        @SerializedName("variety_name") val name: String
)