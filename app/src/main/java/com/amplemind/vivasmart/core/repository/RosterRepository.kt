package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.PackingLineDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.remote.BoxCountLinesRepository
import com.amplemind.vivasmart.core.repository.remote.RosterApi
import com.amplemind.vivasmart.core.repository.response.RosterLineResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RosterRepository @Inject constructor(private val api: RosterApi,
                                           private val apiAuthorization: ApiAuthorization,
                                           private val preferences: UserAppPreferences,
                                           private val database: PackingLineDao,
                                           private val countBoxRepository: BoxCountLinesRepository) {

    data class ChangeLotRequest(
            @SerializedName("stage_id") val stage_id: Int,
            @SerializedName("times") val times : HashMap<Int, Long>,
            @SerializedName("presentations") val presentation: List<CountBoxModel>?
    )

    data class ActualStageRequest(
            @SerializedName("actual_stage") val actual_stage: Int
    )

    fun getLines(): Observable<RosterLineResponse> {
        val businessUnitId = Gson().fromJson(preferences.userInfo, UserModel::class.java).businessUnitId
        return api.getLines(apiAuthorization.getAuthToken(preferences.token), businessUnitId ?: "")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)

    }

    fun getLots(packing_line: Int): Observable<LotsPackageResponse> {
        val businessUnitId = Gson().fromJson(preferences.userInfo, UserModel::class.java).businessUnitId
        return api.getLotsPackage(apiAuthorization.getAuthToken(preferences.token), packing_line, businessUnitId
                ?: "")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun changeLot(request: ChangeLotRequest, packing_line: Int): Observable<Any> {
        return api.changeLotInLine(apiAuthorization.getAuthToken(preferences.token), body = request, packing_line = packing_line)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun assignLot(stage_id: Int, linesPackage: Int): Observable<Any> {
        return api.assignLot(apiAuthorization.getAuthToken(preferences.token), linesPackage, ActualStageRequest(stage_id))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun assignLotLocal(stage_id: Int, linesPackage: Int): Observable<Any> {
        return Observable.fromCallable {
            database.assignLotLocal(stage_id, linesPackage)
        }
    }

    fun saveLine(lines: List<RosterLineModel>) {
        lines.forEach { line ->
            database.insertLines(line)
        }
    }

    fun changeLotOffline(stageId: Int, data: CountBoxRequest?, idLine: Int): Observable<Any> {
        return Observable.fromCallable {
            database.assignLotLocal(stageId,idLine)
            countBoxRepository.distributeCountingBox(idLine,data)
        }
    }


    fun geLocalLines(): Single<List<RosterLineModel>> {
        return database.getLocalLines()
    }

    fun getLocalLots(): Single<List<LotsPackageResultResponse>> {
        return database.getLocalLots()
    }

    fun insertLots(lots: List<LotsPackageResultResponse>) {
        lots.forEach { lot ->
            database.insertLots(lot)
        }
    }


}