package com.amplemind.vivasmart.vo_core.repository.dao

import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable

interface Dao<Model: Any, ChangesType> {

    fun findByUuid(uuid: String): Flowable<Model>?

    fun create(type: String, items: List<Model>, after: ((String, List<Model>) -> Unit)? = null)
    fun create(type: String, count: Int = 1, func: (String, Int, Model) -> Unit, after: ((String, List<Model>) -> Unit)? = null)
    fun update(type: String, items: List<Model>, func: ((String, Int, Model) -> Unit)? = null, after: ((String, List<Model>) -> Unit)? = null)
    fun delete(type: String, items: List<Model>, func: (String, Int, Model) -> Unit, after: ((String, List<Model>) -> Unit)? = null)

    fun onSuccess(type: String, callback: (List<Model>) -> Unit): Disposable
    fun onError(type: String, callback: (Throwable) -> Unit): Disposable

    fun mapChanges(changes: ChangesType): Changes<Model>

}