package com.amplemind.vivasmart.features.interfaces

import io.reactivex.Observable


interface ISplashViewModel {

    fun onShowMessage() : Observable<String>

}
