package com.amplemind.vivasmart.features.crop_validation.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.ImagesModel
import com.amplemind.vivasmart.databinding.ItemImagesCropValidationBinding
import androidx.databinding.library.baseAdapters.BR
import com.bumptech.glide.Glide
import io.reactivex.subjects.BehaviorSubject

class CropImagesValidationAdapter constructor(val list: List<ImagesModel> ) : RecyclerView.Adapter<CropImagesValidationAdapter.CropIssuesValidationViewHolder>() {
    private val onClickSubject = BehaviorSubject.create<ImagesModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CropIssuesValidationViewHolder {
        val binding = ItemImagesCropValidationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CropIssuesValidationViewHolder(binding)
    }

    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(holder: CropIssuesValidationViewHolder, position: Int) = holder.bind(list[position])
    fun onClick(): BehaviorSubject<ImagesModel> = onClickSubject
    fun isEmpty() = list.isEmpty()

    inner class CropIssuesValidationViewHolder(private val binding: ItemImagesCropValidationBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ImagesModel) {
            // dbinding.setVariable(BR.viewModel, item)
            // binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.ivImage.setOnClickListener { onClickSubject.onNext(item) }
                val url_host = binding.root.resources.getString(R.string.url_img_root)
                val url = url_host + item.name
                Glide.with(binding.root).load(url).into(binding.ivImage)
            }
        }

    }

}