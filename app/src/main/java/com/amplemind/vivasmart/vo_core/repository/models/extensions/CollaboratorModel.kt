package com.amplemind.vivasmart.vo_core.repository.models.utils

import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel

val CollaboratorModel.profileImageUrl
    get() = "https://s3-us-west-1.amazonaws.com/viva-organica-profile-pictures/fotosvo/$employeeCode.JPG"
