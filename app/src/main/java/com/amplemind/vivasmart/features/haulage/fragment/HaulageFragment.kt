package com.amplemind.vivasmart.features.haulage.fragment

import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.cardview.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.backgroundTables
import com.amplemind.vivasmart.core.extensions.onItemSelected
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.core.utils.STAGE_UUID
import com.amplemind.vivasmart.features.fertiriego.fragment.PHCEFragment
import com.amplemind.vivasmart.features.haulage.HaulageActivity
import com.amplemind.vivasmart.features.haulage.dialog.HaulageNumberDialog
import com.amplemind.vivasmart.features.haulage.dialog.HaulageSelectTablesDialog
import com.amplemind.vivasmart.features.haulage.dialog.OnHaulageComplete
import com.amplemind.vivasmart.features.haulage.dialog.OnHaulageSelecteTables
import com.amplemind.vivasmart.features.haulage.viewModel.HaulageViewModel
import com.amplemind.vivasmart.features.haulage.viewModel.SpinnerCollaboratorViewModel
import com.amplemind.vivasmart.features.haulage.viewModel.SpinnerVehicleViewModel
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import com.amplemind.vivasmart.features.production_quality.SignatureDialogFragment
import com.amplemind.vivasmart.features.production_roster.fragments.HarvestBoxCountByVarietyDialog
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.content_haulage_activity.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.io.File
import javax.inject.Inject

class HaulageFragment() : BaseFragment(), HasSupportFragmentInjector {

    companion object {
        val TAG = HaulageFragment::class.simpleName
        val PARAM_STAGE_UUID = "$TAG.StageUuid"
    }

    fun newInstance(stageId: Int): HaulageFragment {
        val args = Bundle()
        args.putInt("STAGE_ID", stageId)

        val fragment = HaulageFragment()
        fragment.arguments = args
        return fragment
    }

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>
    lateinit var progressDialogInner: ProgressDialog

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return childFragmentInjector
    }

    @Inject
    lateinit var viewModel: HaulageViewModel
    @Inject
    lateinit var picturesHelper: PicturesHelper

    lateinit var actOperators: AutoCompleteTextView
    lateinit var tvLotName: TextView
    lateinit var tvCropName: TextView
    lateinit var tvVariety: TextView
    lateinit var tvBusinessName: TextView
    lateinit var txOperator: TextView
    lateinit var tvHalfbox: TextView
    lateinit var edBox: EditText
    lateinit var edTemperature: EditText
    lateinit var edComments: EditText
    lateinit var lnSelectedOperator: LinearLayout
    lateinit var lnTables: LinearLayout
    lateinit var selectTables: ImageView
    lateinit var searchDriver: ImageView
    lateinit var ivOperator: ImageView
    lateinit var spinnerVehicle: Spinner
    lateinit var swHalfbox: Switch
    lateinit var cvCancel: CardView
    lateinit var cvAdd: CardView

    private lateinit var collaboratorsAdapter: ArrayAdapter<SpinnerCollaboratorViewModel>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.content_haulage_activity, container, false)

        setViewElements(view)

        setDialogLoadingInner()

        viewModel.stageUuid = STAGE_UUID
        viewModel.stageId = arguments!!.getInt("STAGE_ID", 0)

        loadData()

        viewModel.getListOperator().subscribe(this::listOperatorsAdapter).addTo(subscriptions)

        addObservers()
        autoCompleteListener()
        setupUI()

        removeOperator()

        paintTables()

        return view
    }

    private fun setViewElements(view: View) {
        actOperators = view.findViewById(R.id.act_operators)
        tvLotName = view.findViewById(R.id.tv_lot_name)
        tvCropName = view.findViewById(R.id.tv_crop_name)
        tvVariety = view.findViewById(R.id.tv_variety)
        tvBusinessName = view.findViewById(R.id.tv_business_name)
        txOperator = view.findViewById(R.id.tx_operator)
        tvHalfbox = view.findViewById(R.id.tv_halfbox)
        edBox = view.findViewById(R.id.ed_box)
        edTemperature = view.findViewById(R.id.ed_temperature)
        edComments = view.findViewById(R.id.ed_comments)
        lnSelectedOperator = view.findViewById(R.id.ln_selected_operator)
        lnTables = view.findViewById(R.id.ln_tables)
        selectTables = view.findViewById(R.id.select_tables)
        searchDriver = view.findViewById(R.id.search_driver)
        ivOperator = view.findViewById(R.id.iv_operator)
        spinnerVehicle = view.findViewById(R.id.spinner_vehicle)
        swHalfbox = view.findViewById(R.id.sw_halfbox)
        cvCancel = view.findViewById(R.id.cv_cancel)
        cvAdd = view.findViewById(R.id.cv_add)
    }

    override fun loadData() {
        viewModel.getStage().subscribe(this::onStageLoaded).addTo(subscriptions)
    }

    private fun onStageLoaded(stage: StageModel) {
        viewModel.cropId = stage.cropId
        viewModel.lotId = stage.lotId
        tvLotName.text = stage.lot?.name
        tvCropName.text = stage.crop?.name
        //tv_variety.text = viewModel.variety
        tvVariety.visibility = View.GONE
        tvBusinessName.text = stage.lot?.businessUnit?.name
    }

    private fun setupUI() {
        /*tvLotName.text = viewModel.lotName
        tvCropName.text = viewModel.cropName
        tvVariety.text = viewModel.variety
        tvBusinessName.text = viewModel.businessName*/

        edBox.apply {
            isFocusable = false
            isClickable = true
            isLongClickable = false
        }

        edBox.addThrottle().subscribe(this::showAlertHarvestBoxCountByVariety).addTo(subscriptions)

        selectTables.addThrottle().subscribe(this::showAlertSelectTables).addTo(subscriptions)

        searchDriver.setOnClickListener {
            if (actOperators.text.isNotEmpty()) {
                viewModel.getOperators(actOperators.text.toString())?.addTo(subscriptions)
            }
        }

        swHalfbox.setOnCheckedChangeListener{ _, checked ->
            if (checked) {
                tvHalfbox.visibility = View.VISIBLE
            }
            else {
                tvHalfbox.visibility = View.GONE
            }
        }

        cvCancel.setOnClickListener {
            getActivity()?.finish()
        }

        cvAdd.addThrottle().subscribe {
            if (viewModel.validateForm(viewModel.getSelectTables(), edBox.text.toString(), edTemperature.text.toString(), spinnerVehicle.selectedItemPosition)) {
                showSignature()
            }
        }.addTo(subscriptions)
    }

    private fun showAlertSelectTables(any: Any) {
        val dialog = HaulageSelectTablesDialog.newInstance(viewModel.getTablesModel(), viewModel.getSelectTables() as ArrayList<Int>)
        dialog.setOnHaulageSelected(object : OnHaulageSelecteTables {
            override fun onSelectTables(list: List<Int>) {
                viewModel.setSelectTables(list)
                paintTables(list)
            }
        })
        dialog.show(fragmentManager!!, HaulageSelectTablesDialog.TAG)
    }


    private fun showAlertHarvestBoxCountByVariety(any: Any) {
        val dialog = HarvestBoxCountByVarietyDialog.newInstance(viewModel.stageUuid, viewModel.boxCounts, viewModel.secondQualityBoxCount).apply {
            onBoxCountResult { list, secondQuality, total ->
                edBox.setText("%.1f".format(total))
                viewModel.boxTotal = total
                viewModel.boxCounts = list
                viewModel.secondQualityBoxCount = secondQuality
            }
        }

        dialog.show(fragmentManager!!, HarvestBoxCountByVarietyDialog.TAG)
    }

    private fun paintTables(list: List<Int>? = null) {
        val tables = viewModel.getTablesModel()
        tables.forEachIndexed { index, _ ->
            val tableView = lnTables.getChildAt(index)
            tableView.visibility = View.VISIBLE
            val selected = list?.any { it == (index + 1) }
            tableView.backgroundTables(selected ?: false)
        }
    }

    private fun addObservers() {
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onAssignSigns().subscribe(this::onAssignSigns).addTo(subscriptions)
        viewModel.onSuccessUpload().subscribe(this::onSignsUpload).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onGetVehicles().subscribe(this::onGetVehicles).addTo(subscriptions)
        viewModel.onCreateCarryOrder().subscribe(this::onCreateCarryOrder).addTo(subscriptions)
        viewModel.getVehicles().addTo(subscriptions)
    }

    private fun onGetVehicles(vehicles: List<SpinnerVehicleViewModel>) {
        val spinnerAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item,
                vehicles)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerVehicle.adapter = spinnerAdapter
    }

    private fun onSignsUpload(signs: MutableList<File>) {
        if (signs.isEmpty()) {

            val boxes = edBox.text.toString() + if (swHalfbox.isChecked) ".5" else ""

            viewModel.createOrder(viewModel.getSelectTables(), edTemperature.text.toString(),
                    viewModel.getCollaboratorId(actOperators.text.toString()), spinnerVehicle.selectedItemPosition, edComments.text.toString()).addTo(subscriptions)
        } else {
            viewModel.uploadSign(signs).addTo(subscriptions)
        }
    }

    private fun onCreateCarryOrder(carryId: Int) {
        viewModel.assignOrders(carryId).addTo(subscriptions)
    }

    private fun onAssignSigns(code: String) {
        showOrderNumber(code)
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(activity!!.applicationContext)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(activity!!.applicationContext, bitmap), image, 400f, 400f)
        return image
    }

    private fun showSignature() {
        val dialog = SignatureDialogFragment().newInstance(
                "Enviar orden de acarreo",
                "¿Está seguro de querer enviar la orden de acarreo? Por favor firme para confirmar.",
                false,
                arrayOf("Firma 1 (Operador):", "Firma 2 (Supervisor):"),
                arrayOf("Regresar", "Regresar"),
                arrayOf("Siguiente", "Enviar"),
                mode = 0)
        dialog.show(fragmentManager!!, SignatureDialogFragment.TAG)

        dialog.setOnCompleteListener(object : OnSignatureCompleteListener {
            override fun onDismiss() {

            }

            override fun completeSign(sign1: Bitmap, sign2: Bitmap?) {
                val signs = mutableListOf<File>()
                signs.add(compressImage(sign1))
                if (sign2 != null) {
                    signs.add(compressImage(sign2))
                }
                viewModel.uploadSign(signs).addTo(subscriptions)
            }
        })
    }

    private fun showOrderNumber(number: String) {
        val dialog = HaulageNumberDialog.newInstance(number)
        dialog.isCancelable = false
        dialog.show(fragmentManager!!, HaulageNumberDialog.TAG)
        dialog.setOnHaulageComplete(object : OnHaulageComplete {
            override fun onDismissDialog() {
                getActivity()?.finish()
            }
        })
    }

    private fun removeOperator() {
        ivOperator.setOnClickListener {
            actOperators.visibility = View.VISIBLE
            lnSelectedOperator.visibility = View.GONE
            searchDriver.visibility = View.VISIBLE
        }
    }

    private fun autoCompleteListener() {
        actOperators.onItemSelected {
            lnSelectedOperator.visibility = View.VISIBLE
            searchDriver.visibility = View.GONE
            actOperators.visibility = View.GONE
            txOperator.text = it
            actOperators.setText("")
        }
    }

    private fun listOperatorsAdapter(list: List<SpinnerCollaboratorViewModel>) {
        collaboratorsAdapter = ArrayAdapter(activity!!, android.R.layout.simple_dropdown_item_1line,
                list)

        actOperators.setAdapter(collaboratorsAdapter)
        actOperators.showDropDown()

    }

    fun setDialogLoadingInner() {
        progressDialogInner = ProgressDialog(context)
        progressDialogInner.setTitle(getString(R.string.app_name))
        progressDialogInner.setMessage(getString(R.string.server_loading))
        progressDialogInner.setCancelable(false)
    }

    fun showProgressDialogInner(show: Boolean) {
        if (show) progressDialogInner.show() else progressDialogInner.dismiss()
    }

}