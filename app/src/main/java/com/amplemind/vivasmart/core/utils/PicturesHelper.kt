package com.amplemind.vivasmart.core.utils

import android.content.Context
import android.database.Cursor
import android.graphics.*
import android.graphics.Paint.FILTER_BITMAP_FLAG
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.os.SystemClock
import android.provider.MediaStore
import android.util.Log
import com.amplemind.vivasmart.vo_core.utils.currentDate
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.random.Random


@Singleton
class PicturesHelper @Inject constructor() {

    fun createSignatureImageFile(context: Context, sign: Bitmap, path: String? = "signs"): Single<File> {
        Log.e("Sign: ", sign.toString())
        Log.e("Path: ", path.toString())
        var file =  Single.fromCallable {
            val compressedFileName = generateFileName() + ".jpg"
            val imageFile = createBitmapToFile(context, sign)
            val compressedFile = createImageFile(context, compressedFileName, path)
            compressImage(imageFile, compressedFile, 400f, 400f)
            imageFile.delete()

            return@fromCallable compressedFile
        }

        Log.e("File: ", file.toString())
        return file
    }

    private fun generateFileName(): String {
        return currentDate().toString() + "_" + ((Random(SystemClock.uptimeMillis()).nextInt() % 9000) + 1000).toString()
    }

    @Throws(IOException::class)
    fun createImageFile(context: Context, path: String? = null): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName: String = "JPEG_" + timeStamp + "_.jpg"
        //val storageDir: File = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        //if(!storageDir.exists()) storageDir.mkdirs()
        //return File(storageDir, imageFileName)
        return createImageFile(context, imageFileName, path)
    }

    @Throws(IOException::class)
    fun createImageFile(context: Context, fileName: String, path: String? = null): File {
        //val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        //val imageFileName: String = "JPEG_" + timeStamp + "_.jpg"

        val storageDir: File? = if (path == null) {
            context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        }
        else {
            context.getDir(path, Context.MODE_PRIVATE)
        }

        if(storageDir?.exists() == null) storageDir?.mkdirs()
        return File(storageDir, fileName)
    }

    fun createBitmapToFile(context: Context, bitmap: Bitmap): File {
        val file = createImageFile(context)
        val fOut = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fOut)
        fOut.flush()
        fOut.close()
        return file
    }

    fun setScaledBitmap(imageViewWidth: Int, imageViewHeight: Int, imageFilePath: String): Bitmap {
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(imageFilePath, bmOptions)
        val bitmapWidth = bmOptions.outWidth
        val bitmapHeight = bmOptions.outHeight

        val scaleFactor = Math.min(bitmapWidth/imageViewWidth, bitmapHeight/imageViewHeight)

        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor

        return BitmapFactory.decodeFile(imageFilePath, bmOptions)

    }

    fun getRealPathFromURI(context: Context, contentUri: Uri): String {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor!!.getString(column_index)
        } catch (e: Exception) {
            Log.e("getRealPathFromURI", "getRealPathFromURI Exception : " + e.toString())
            return ""
        } finally {
            if (cursor!! != null) {
                cursor.close()
            }
        }
    }

    /**
     * Scale an image to given width and height
     *
     * @param image_uri      image to scale
     * @param image_last_uri final scaled image
     * @return If success true else false
     */
    fun compressImage(image_uri: File, image_last_uri: File, maxHeight: Float, maxWidth: Float): Boolean {
        try {
            val filePath = image_uri.absolutePath //getRealPathFromURI(imageUri);
            var scaledBitmap: Bitmap
            val options = BitmapFactory.Options()

            //      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
            //      you try the use the bitmap here, you will get null.
            options.inJustDecodeBounds = true
            val bmp: Bitmap
            BitmapFactory.decodeFile(filePath, options)
            var actualHeight = options.outHeight
            var actualWidth = options.outWidth

            //      max Height and width values of the compressed image is taken as 816x612

            var imgRatio = (actualWidth / actualHeight).toFloat()
            val maxRatio = maxWidth / maxHeight

            //      width and height values are set maintaining the aspect ratio of the image
            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight
                    actualWidth = (imgRatio * actualWidth).toInt()
                    actualHeight = maxHeight.toInt()
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth
                    actualHeight = (imgRatio * actualHeight).toInt()
                    actualWidth = maxWidth.toInt()
                } else {
                    actualHeight = maxHeight.toInt()
                    actualWidth = maxWidth.toInt()

                }
            }

            //      setting inSampleSize value allows to load a scaled down version of the original image
            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)

            //      inJustDecodeBounds set to false to load the actual bitmap
            options.inJustDecodeBounds = false

            //      this options allow android to claim the bitmap memory if it runs low on memory
            //options.inPurgeable = true;
            //options.inInputShareable = true;
            options.inTempStorage = ByteArray(16 * 1024)


            //          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options)

            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)

            val ratioX = actualWidth / options.outWidth.toFloat()
            val ratioY = actualHeight / options.outHeight.toFloat()
            val middleX = actualWidth / 2.0f
            val middleY = actualHeight / 2.0f

            val scaleMatrix = Matrix()
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)

            val canvas = Canvas(scaledBitmap)
            canvas.setMatrix(scaleMatrix)
            canvas.drawBitmap(bmp, middleX - bmp.width / 2, middleY - bmp.height / 2, Paint(FILTER_BITMAP_FLAG))

            //      check the rotation of the image and display it properly
            val exif: ExifInterface

            exif = ExifInterface(filePath)

            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0)
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90F)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == 3) {
                matrix.postRotate(180F)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == 8) {
                matrix.postRotate(270F)
                Log.d("EXIF", "Exif: $orientation")
            }

            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)


            val out: FileOutputStream
            val filename = image_last_uri.absolutePath

            out = FileOutputStream(filename)
            //          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out)
            return true
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        return false
    }

    /**
     * Get a ration based on the image size requirement
     *
     * @param options   used to open only the image bounds
     * @param reqWidth  requirement Width
     * @param reqHeight requirement Height
     * @return a homogeneous image size
     */
    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }
        return inSampleSize
    }

}