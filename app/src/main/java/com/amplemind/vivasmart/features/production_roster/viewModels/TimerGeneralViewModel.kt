package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.navegation.data_mediator.MediatorNavigation
import javax.inject.Inject

class TimerGeneralViewModel @Inject constructor(private val mediatorNavigation: MediatorNavigation) {

    private val list = mutableListOf<TimerUserViewModel>()

    private fun getDummyUser(name: String): TimerUserViewModel {
        val user = TimerUserViewModel()
        user.name = name
        user.registerId = 0
        user.setMediator(mediatorNavigation)
        return user
    }

    fun getDummyListUsers(): List<TimerUserViewModel> {
        list.add(getDummyUser("Adam Gray"))
        list.add(getDummyUser("Adam Gray"))
        list.add(getDummyUser("Adam Gray"))
        list.add(getDummyUser("Adam Gray"))
        list.add(getDummyUser("Adam Gray"))
        return list
    }

    fun getCollaboratorName(atPosition: Int): String {
        if (atPosition > list.size) return ""
        return list[atPosition].name ?: ""
    }

    fun getCollaboratorImage(atPosition: Int): String {
        if (atPosition > list.size) return ""
        return list[atPosition].getUserImage() ?: ""
    }

    fun deleteCollaborator(atPosition: Int) {
        if (atPosition > list.size) return
        list.removeAt(atPosition)
    }

}