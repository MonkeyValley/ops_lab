package com.amplemind.vivasmart.core.repository.response

import com.google.gson.annotations.SerializedName

data class ComparisonResponse(
        @SerializedName("data") val data: List<ComparisonResultResponse>
)

data class ComparisonResultResponse(
        @SerializedName("activity_id") val activity_id: Int,
        @SerializedName("activity_name") val activity_name: String,
        @SerializedName("days") val days: List<DaysResponse>,
        @SerializedName("percent") val percent: Boolean,
        @SerializedName("planning_id") val planning_id: Int? = null,
        @SerializedName("priority") val priority: Int,
        @SerializedName("unit_id") val unit_id: Int? = null,
        @SerializedName("unit_name") val unit_name: String
)

data class DaysResponse(
        @SerializedName("day") val day: String,
        @SerializedName("people_executed_no") val people_executed_no: Double,
        @SerializedName("people_no") val people_no: Double,
        @SerializedName("percent_executed_units") val percent_executed_units: Double,
        @SerializedName("total_estimated_units") val total_estimated_units: Double,
        @SerializedName("total_executed_units") val total_executed_units: Double
)