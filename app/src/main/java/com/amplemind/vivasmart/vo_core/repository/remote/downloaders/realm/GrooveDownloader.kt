package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel
import com.amplemind.vivasmart.vo_core.repository.remote.ActivityCodeApi
import io.reactivex.Single
import io.realm.Realm
import javax.inject.Inject

class GrooveDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mPref: UserAppPreferences,
        private val mActivityCodeApi: ActivityCodeApi
) : RealmDownloader<ActivityCodeModel, GrooveModel>(realmHolder, GrooveModel::class.java) {

    override fun buildServerRequest(parent: ActivityCodeModel, lastVersion: Long): Single<List<GrooveModel>> =
            mActivityCodeApi.getGrooves(mPref.authorizationToken, activityCodeId = parent.id).map { response ->
                Log.e("Data-Groove: ", response.toString())

                setActivityLog(response)
            }

    private fun setActivityLog(list: List<GrooveModel>): List<GrooveModel>? {
        var result = ArrayList<GrooveModel>()
        list.forEach {
            result.add(it.apply {
                activityLog = getActivityLog(activityLogUuid)
            })
        }
        return result
    }

    private fun getActivityLog(uuid: String): ActivityLogModel? =
            Realm.getDefaultInstance().use {
                it.where(ActivityLogModel::class.java)
                        .equalTo("uuid", uuid)
                        .findFirst()
            }

    override fun beforeSaving(parent: ActivityCodeModel, child: GrooveModel): Single<GrooveModel> =
            Single.just(
                    child.apply {
                        activityCode = parent
                        activityCodeUuid = parent.uuid
                    }
            )
    
}