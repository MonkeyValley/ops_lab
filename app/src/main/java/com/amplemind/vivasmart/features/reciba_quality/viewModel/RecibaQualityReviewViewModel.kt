package com.amplemind.vivasmart.features.reciba_quality.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.response.UnitResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.production_quality.UnitsViewModel
import com.amplemind.vivasmart.features.reciba_quality.repository.RecibaQualityRepository
import com.amplemind.vivasmart.features.reciba_quality.repository.response.VariatiesRecibaQualityResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class RecibaQualityReviewViewModel  @Inject constructor(
        private val repository: RecibaQualityRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    var carryOrderId = ""
    var validBoxNo = 0.0
    var cropId = 0
    var unitId = 0
    var varietyId = 0

    private val listUnits = mutableListOf<UnitsViewModel>()
    private val listVarieties = mutableListOf<VarietiesViewModel>()
    private val requestSuccess_units = BehaviorSubject.create<List<UnitsViewModel>>()
    private val requestSuccess_varieties = BehaviorSubject.create<List<VarietiesViewModel>>()


    fun getUnits( ): Disposable {
        return repository.getUnits( )
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .doOnTerminate { getVarieties()  }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_units)
                .subscribe({
                    listUnits.clear()
                    listUnits.addAll(it)
                    requestSuccess_units.onNext(listUnits)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getVarieties( ): Disposable {
        return repository.getVarieties( carryOrderId )
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_varieties)
                .subscribe({
                    listVarieties.clear()
                    listVarieties.addAll(it)
                    requestSuccess_varieties.onNext(listVarieties)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModel_units(result: UnitResponse): List<UnitsViewModel> {
        val list = arrayListOf<UnitsViewModel>()

        for (item in result.data) {
            list.add(UnitsViewModel(item))
        }
        list.sortBy { it.name }
        return list
    }

    private fun mapModelToViewModel_varieties(result: VariatiesRecibaQualityResponse): List<VarietiesViewModel> {
        val list = arrayListOf<VarietiesViewModel>()

        validBoxNo = result.validBoxNo
        cropId = result.cropId

        for (item in result.varietiesDetail) {
            list.add(VarietiesViewModel(item))
        }
        list.sortBy { it.name }
        return list
    }

    fun onSuccessRequest_units(): BehaviorSubject<List<UnitsViewModel>> {
        return requestSuccess_units
    }

    fun onSuccessRequest_varieties(): BehaviorSubject<List<VarietiesViewModel>> {
        return requestSuccess_varieties
    }


}