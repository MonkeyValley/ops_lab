package com.amplemind.vivasmart.core.platform

import android.app.IntentService
import android.content.Intent
import android.os.Message
import android.os.Parcelable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.amplemind.vivasmart.core.repository.PendingRequestRepository
import com.amplemind.vivasmart.core.repository.model.SyncReportModel
import com.amplemind.vivasmart.core.repository.model.SyncReportSectionModel
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.SYNC_DATABASE
import com.amplemind.vivasmart.core.utils.SYNC_ERRORS
import com.amplemind.vivasmart.core.utils.SYNC_REPORTS
import dagger.android.AndroidInjection
import java.util.ArrayList
import javax.inject.Inject

class SynchronizeDataBaseService : IntentService("SynchronizeDataBaseService"), SyncNotification {

    @Inject
    lateinit var repository: PendingRequestRepository

    private var totalSyncs = 0
    private var syncsComplete = 0

    val subscriptions = AndroidDisposable()

    private val reports = mutableListOf<SyncReportSectionModel>()
    private val errors = ArrayList<String>()

    enum class SynchronizeRequest(val type: Int) {

        ROSTER_PRODUCTION(0),
        CONTROL_QUALITY(1),
        PRODUCTION_QUALITY(2),
        ROSTER_PACKAGE(3),
        QUALITY_CARRY_ORDER(4),
        RECIBA(5);

        companion object {
            fun from(value : Int) : SynchronizeRequest = SynchronizeRequest.values().first{ it.type == value}
        }
    }

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.dispose()
    }

    override fun onHandleIntent(p0: Intent?) {
        val syncs = listOf(
                SynchronizeRequest.ROSTER_PRODUCTION,
                SynchronizeRequest.CONTROL_QUALITY,
                SynchronizeRequest.PRODUCTION_QUALITY,
                SynchronizeRequest.ROSTER_PACKAGE,
                SynchronizeRequest.QUALITY_CARRY_ORDER,
                SynchronizeRequest.RECIBA
        )
        totalSyncs = syncs.size
        synchronizeRequests(syncs)
    }

    @Synchronized override fun onTerminate(results: List<SyncReportModel>, type: SynchronizeRequest, error: String) {
        syncsComplete += 1
        if (results.isNotEmpty()) {
            reports.add(SyncReportSectionModel(results, type.type))
        }
        if (error.isNotEmpty()){
            errors.add(error)
        }
        if (syncsComplete >= totalSyncs) {
            val intent = Intent(SYNC_DATABASE)
            intent.putExtra(SYNC_REPORTS, reports as java.util.ArrayList<out Parcelable>)
            intent.putStringArrayListExtra(SYNC_ERRORS,errors)
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        }
        // else maybe could we notify about the progress
    }

    private fun synchronizeRequests(requests: List<SynchronizeRequest>) {
        requests.map { pendingRequest ->
            when (pendingRequest) {
                SynchronizeRequest.RECIBA -> { repository.syncReciba(this, subscriptions)}
                SynchronizeRequest.CONTROL_QUALITY -> { repository.syncQualityControl(this, subscriptions) }
                SynchronizeRequest.PRODUCTION_QUALITY -> { repository.syncLabors(this, subscriptions) }
                SynchronizeRequest.QUALITY_CARRY_ORDER -> { repository.syncQualityCarryOrder(this, subscriptions) }
                SynchronizeRequest.ROSTER_PACKAGE -> { repository.syncRosterPackage(this, subscriptions)}
                SynchronizeRequest.ROSTER_PRODUCTION -> { repository.syncRosterProduction(this, subscriptions) }
            }
        }
    }

}

interface SyncNotification {
    fun onTerminate(results: List<SyncReportModel>, type: SynchronizeDataBaseService.SynchronizeRequest, error : String = "")
}