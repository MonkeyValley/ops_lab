package com.amplemind.vivasmart.features.reciba_quality.viewModel

import androidx.room.Entity
import com.amplemind.vivasmart.core.repository.model.ImagesModel
import com.amplemind.vivasmart.core.repository.model.IssuesModel
import com.google.gson.annotations.SerializedName

@Entity
data class ReacibaQualityReviewViewModel(
        @SerializedName("carry_order_id") var carryOrderId: String,
        @SerializedName("sample_size") var sample: Int,
        @SerializedName("unit_id") var unitId: Int,
        @SerializedName("user_id") var userId: Int,
        @SerializedName("export") var export: Int? = null,
        @SerializedName("variety_id") var varietyId: Int,
        @SerializedName("sign") var sign: String? = null,
        @SerializedName("harvest_week") var harvestWeek: Int,


        @SerializedName("issues") var issues: ArrayList<IssuesModel>? = null,
        @SerializedName("images") var images: ArrayList<ImagesModel>? = null
)