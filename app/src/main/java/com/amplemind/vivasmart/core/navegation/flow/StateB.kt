package com.amplemind.vivasmart.core.navegation.flow

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.core.navegation.AppFlow

class StateB(val previousState: State, val appFlow: AppFlow): State {

    override fun changeToPreviousState() {
        appFlow.state = previousState
    }

    override fun startActivity(context: Context) {
        // TODO
    }

    override fun getFragment() {
        // TODO
    }

    override fun doSomeAwesomeThing(param: String) {
        Log.d("StateB", "doing something awesome with $param")
        appFlow.state = StateC(this, appFlow)
    }

    override fun getSomeAwesomeThing(): String {
        return "getting something awesome from StateB"
    }
}