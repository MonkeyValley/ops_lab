package com.amplemind.vivasmart.features.quality_control

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.features.quality_control.viewModel.DetailControlQualityViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.IssueViewModel
import kotlinx.android.synthetic.main.activity_detail_control_quality.*
import javax.inject.Inject

class DetailControlQualityActivity : BaseActivity() {

    private val TAG = DetailControlQualityActivity::class.java.simpleName

    @Inject
    lateinit var viewModel: DetailControlQualityViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_control_quality)
        setupToolbar()
        initRecycler()
        setupListeners()
    }

    private fun setupListeners() {
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onGetIssues().subscribe(this::onGetIssues).addTo(subscriptions)
        viewModel.onCreatedIssue().subscribe(this::onCreateIssue).addTo(subscriptions)
        viewModel.getControlListActivities(intent.getIntExtra("activity_id", 0),
                intent.getIntExtra("activity_area", 0),
                intent.getIntExtra("activity_category", 0),
                intent.getIntExtra("activity_sub_category", 0)).addTo(subscriptions)

        btn_done.setOnClickListener {
            hideKeyboard(this)
            viewModel.createIssues(
                    viewModel.getComments(),
                    intent.getIntExtra("groove_id", 0),
                    true,
                    intent.getIntExtra("activity_log", 0),
                    intent.getIntExtra("collaborator_id", 0)).addTo(subscriptions)
        }
    }

    private fun onCreateIssue(success: Boolean) {
        val result = Intent()
        result.putExtra("groove_id", intent.getIntExtra("groove_id", 0))
        result.putExtra("position", intent.getIntExtra("position", 0))
        setResult(Activity.RESULT_OK, result)
        finish()
    }

    private fun onGetIssues(items: List<IssueViewModel>) {
        (rv_tasks.adapter as DetailControlQualityAdapter).addElements(items)
    }

    private fun initRecycler() {
        rv_tasks.hasFixedSize()
        rv_tasks.layoutManager = LinearLayoutManager(this)
        rv_tasks.adapter = DetailControlQualityAdapter(this)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = intent.getStringExtra("title")
        tv_user_name.text = intent.getStringExtra("username")
        tv_activity_name.text = intent.getStringExtra("activity_name").toUpperCase()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}