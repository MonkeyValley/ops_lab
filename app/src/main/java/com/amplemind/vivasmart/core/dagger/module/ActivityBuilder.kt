package com.amplemind.vivasmart.core.dagger.module

import com.amplemind.vivasmart.core.base.dagger.scope.ActivityScope
import com.amplemind.vivasmart.core.dagger.module.fragment.*
import com.amplemind.vivasmart.features.MLBarcodeScanner.BarcodeScannerXFragment
import com.amplemind.vivasmart.features.collaborators.*
import com.amplemind.vivasmart.features.comparison.ComparisonActivity
import com.amplemind.vivasmart.features.crop_validation.CropIssuesValidationActivity
import com.amplemind.vivasmart.features.crop_validation.fragment.CropIssuesValidationFragment
import com.amplemind.vivasmart.features.crop_validation.CropValidationActivity
import com.amplemind.vivasmart.features.crop_validation.dialogs.SignPhotoCropValidationDialog
import com.amplemind.vivasmart.features.fertiriego.*
import com.amplemind.vivasmart.features.fertiriego.fragment.*
import com.amplemind.vivasmart.features.finished_product_quality.FinishedProductQualityActivity
import com.amplemind.vivasmart.features.finished_product_quality.fragment.ProductQualityReviewDetailFragment
import com.amplemind.vivasmart.features.finished_product_quality.fragment.ProductQualityReviewFragment
import com.amplemind.vivasmart.features.gallery.GalleryDialog
import com.amplemind.vivasmart.features.haulage.HaulageActivity
import com.amplemind.vivasmart.features.haulage.dialog.HaulageNumberDialog
import com.amplemind.vivasmart.features.haulage.dialog.HaulageSelectTablesDialog
import com.amplemind.vivasmart.features.haulage.fragment.HaulageFragment
import com.amplemind.vivasmart.features.haulage.fragment.HaulageReportFragment
import com.amplemind.vivasmart.features.login.ChangePasswordActivity
import com.amplemind.vivasmart.features.login.LoginActivity
import com.amplemind.vivasmart.features.login.RecoverPasswordActivity
import com.amplemind.vivasmart.features.login.SetPasswordActivity
import com.amplemind.vivasmart.features.main.MainActivity
import com.amplemind.vivasmart.features.mipe.*
import com.amplemind.vivasmart.features.fertiriego.dialog.EditMaxValvuleDialog
import com.amplemind.vivasmart.features.gallery.HourDialog
import com.amplemind.vivasmart.features.gallery.VisualSupportDialog
import com.amplemind.vivasmart.features.mipe.fragment.MenuActivitiesMipeFragment
import com.amplemind.vivasmart.features.mipe.fragment.MipeLotFragment
import com.amplemind.vivasmart.features.mipe.fragment.MipeReportFragment
import com.amplemind.vivasmart.features.mipe.fragment.SelectGrooveFragment
import com.amplemind.vivasmart.features.notification.MessageActivity
import com.amplemind.vivasmart.features.notification.fragment.InboxFragment
import com.amplemind.vivasmart.features.operations.fragment.MenuActivitiesOperationsFragment
import com.amplemind.vivasmart.features.operations.OperationsActivitiesActivity
import com.amplemind.vivasmart.features.packaging_quality.*
import com.amplemind.vivasmart.features.packaging_quality.clean_report.CleanReportActivity
import com.amplemind.vivasmart.features.packaging_quality.clean_report.CleaningReportTotalActivity
import com.amplemind.vivasmart.features.packaging_quality.dialog.RecibaAverageWeightDialog
import com.amplemind.vivasmart.features.packaging_quality.fragment.BoxAverageWeightFragment
import com.amplemind.vivasmart.features.packaging_quality.quality_report_result.QualityReportActivity
import com.amplemind.vivasmart.features.phenology.PhenologyActivitiesActivity
import com.amplemind.vivasmart.features.phenology.PhenologyActivity
import com.amplemind.vivasmart.features.phenology.PhenologyVarietiesPlantsActivity
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyDetailDialog
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyVarClusterDialog
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyVarMeasureDialog
import com.amplemind.vivasmart.features.phenology.dialog.SignCommentDialog
import com.amplemind.vivasmart.features.phenology.fragments.MenuActivitiesPhenologyFragment
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyFragment
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyVarietiesFragment
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyVarietiesPlantsFragment
import com.amplemind.vivasmart.features.planning.PlanningActivity
import com.amplemind.vivasmart.features.pollination.PollinationActivity
import com.amplemind.vivasmart.features.pollination.PollinationLotsActivity
import com.amplemind.vivasmart.features.pollination.PollinationReportActivity
import com.amplemind.vivasmart.features.pollination.fragment.PollinationFragment
import com.amplemind.vivasmart.features.pollination.fragment.PollinationReportFragment
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity
import com.amplemind.vivasmart.features.production_quality.*
import com.amplemind.vivasmart.features.production_range.ProductionRangeActivity
import com.amplemind.vivasmart.features.production_roster.*
import com.amplemind.vivasmart.features.profile.ProfileActivity
import com.amplemind.vivasmart.features.quality_control.ActivitiesControlQualityActivity
import com.amplemind.vivasmart.features.quality_control.DetailControlQualityActivity
import com.amplemind.vivasmart.features.quality_control.ListControlActivity
import com.amplemind.vivasmart.features.quality_control.QualityControlActivity
import com.amplemind.vivasmart.features.reciba_quality.RecibaQualityDetailActivity
import com.amplemind.vivasmart.features.reciba_quality.RecibaQualityReviewActivity
import com.amplemind.vivasmart.features.reciba_quality.RecibaQualityReviewDetailActivity
import com.amplemind.vivasmart.features.reciba_quality.ReportRecibaQualityActivity
import com.amplemind.vivasmart.features.reciba_quality.fragment.RecibaQualityReviewDetailFragment
import com.amplemind.vivasmart.features.root_quality.RootQualityDialog
import com.amplemind.vivasmart.features.scannerbarcode.ReadBarcodeActivity
import com.amplemind.vivasmart.features.scannerbarcode.ScanListActivity
import com.amplemind.vivasmart.features.splash.SplashActivity
import com.amplemind.vivasmart.features.sync_forced.SyncForcedActivity
import com.amplemind.vivasmart.features.waste_control.WasteControlActivity
import com.amplemind.vivasmart.features.waste_control.WasteControlTestActivity
import com.amplemind.vivasmart.features.waste_control.fragment.WasteControlTestFragment
import com.amplemind.vivasmart.features.waste_control.dialogs.SignDialog
import com.amplemind.vivasmart.vo_features.sync.SyncActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * Created by
 *              amplemind on 6/29/18.
 */
@Module
abstract class ActivityBuilder {

    //region main flow for user registration
    /**
     * SPLASH ACTIVITY
     * */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindSplashActivity(): SplashActivity

    /**
     * LOGIN ACTIVITY
     * */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindLoginActivity(): LoginActivity

    /**
     * RECOVER PASSWORD ACTIVITY
     * */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindRecoverPasswordActivity(): RecoverPasswordActivity

    /**
     * SET PASSWORD ACTIVITY
     * */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindSetPasswordActivity(): SetPasswordActivity


    /**
     * CHANGE PASSWORD ACTIVITY
     * */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindChangePasswordActivity(): ChangePasswordActivity

    //endregion

    /**
     * MAIN ACTIVITY
     * */
    @ContributesAndroidInjector(modules = [(HomeFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindMainActivity(): MainActivity

    /**
     * PROFILE ACTIVITY
     * */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindProfileActivity(): ProfileActivity


    /**
     *  QUALITY PACKAGING ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPackagingQualityActivity(): PackagingQualityActivity


    /**
     *  QUALITY PACKAGING DETAIL ORDER ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPackagingQualityDetailActivity(): DetailOrdenActivity

    /**
     *  Packagind search code
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindQualitySearchCodeActivity(): QualitySearchCodeActivity

    /**
     *  PRODUCTION QUALITY ACTIVITY
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindProductionQualityActivity(): ProductionQualityActivity

    /**
     *  Packagind search code
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindCleanReportActivity(): CleanReportActivity

    /**
     *  PRODUCTION CATEGORIES ACTIVITY
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindProductionCategoriesActivity(): ProductionCategoriesActivity

    /**
     *  CATEGORIES DETAIL ACTIVITY
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindCategoriesDetailActivity(): CategoriesDetailActivity

    /**
     *  CATEGORIES REPORTS ACTIVITY
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindCategoriesReportsActivity(): CategoriesReportsActivity

    /**
     *  QUALITY REPORT
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindQualityReportActivity(): QualityReportActivity

    /**
     *  CLEAN REPORT TOTAL
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindCleanReportTotalActivity(): CleaningReportTotalActivity

    /**
     *  CONFIRMATION REPORTS ACTIVITY
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindConfirmationReportsActivity(): ConfirmationReportsActivity

    /**
     *  DETAIL CONTROL QUALITY ACTIVITY
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindDetailControlActivity(): DetailControlQualityActivity

    /**
     *  CONFIRMATION REPORTS DETAIL ACTIVITY
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindConfirmationReportsDetailActivity(): ConfirmationReportsDetailActivity

    /**
     *  CONTROL QUALITY
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindQualityControlActivity(): QualityControlActivity

    /**
     *  CONTROL QUALITY LIST
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindListControlActivity(): ListControlActivity

    /**
     *  CONTROL QUALITY LIST
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindActivitiesControlActivity(): ActivitiesControlQualityActivity

    /**
     *  BARCODE SCANNER
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindReadBarcodeActivity(): ReadBarcodeActivity

    /**
     *  ADD COLLABORATORS ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(CollaboratorsFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindAddCollaboratorsActivity(): AddCollaboratorsActivity

    /**
     *  ADD COLLABORATORS ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(CollaboratorsFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindAssignGroovesActivity(): AssignGroovesActivity

    /**
     *  PAYROLL ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(HomeFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindPayrollActivity(): ProductionRosterActivitiesActivity

    /**
     *  PRODUCTION ROSTER TIMER ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(ProductionRosterFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindTimerActivity(): TimerActivity

    /**
     *  EDIT QUANTITY ROSTER
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindEditQuantityRosterActivity(): EditQuantityRosterActivity

    /**
     *  LINES TIMER ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(ProductionRosterFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindTimerLinesActivity(): TimerLinesActivity

    /**
     *  PRODUCTION ROSTER REPORT
     */
    @ContributesAndroidInjector(modules = [(ProductionRosterFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindReportRosterActivity(): ReportRosterActivity

    /**
     *  PRODUCTION ACTIVITY DETAIL ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindProductionActivityDetailActivity(): ProductionActivityDetailActivity

    /**
     *  ADD ACTIVITY FOR COLLABORATOR
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindAddActivityForCollaborator(): AddActivityForCollaborator

    /**
     *  DETAIL ROSTER ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindRosterDetailActivity(): RosterDetailActivity

    /**
     *  SCAN LIST ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(CollaboratorsFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindScanListActivity(): ScanListActivity

    /**
     *  HAULAGE ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(HaulageFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindHaulageActivity(): HaulageActivity

    /**
     *  HAULAGE FRAGMENT
     */
    @ContributesAndroidInjector(modules = [(HaulageFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindHaulageFragment(): HaulageFragment

    /**
     *  HAULAGE REPORT FRAGMENT
     */
    @ContributesAndroidInjector(modules = [(HaulageFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindHaulageReportFragment(): HaulageReportFragment

    /**
     *  HAULAGE TAB FRAGMENT
     */
    @ContributesAndroidInjector(modules = [(HaulageFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindHaulageSelectTablesDialog(): HaulageSelectTablesDialog

    /**
     *  HAULAGE NUMBER DIALOG
     */
    @ContributesAndroidInjector(modules = [(HaulageFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindHaulageNumberDialog(): HaulageNumberDialog


    /**
     *  CHANGE LOT ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindChangeLotActivity(): ChangeLotActivity

    /**
     *  GENERAL ACTIVITIES
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindGeneralActivities(): GeneralActivitiesActivity

    /**
     *  TIMER GENERAL ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindTimerGeneralActivity(): TimerGeneralActivity

    /**
     *  BOX COUNT LINES ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindBoxCountLinesActivity(): BoxCountLinesActivity

    /**
     *  PRESENTATION FOR COLLABORATORS PACKAGE
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPresentationForCollaboratorActivity(): PresentationForCollaboratorActivity

    /**
     *  RECIBA ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindRecibaActivity(): RecibaActivity

    /**
     *  RECIBA DETAIL ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(HaulageFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindRecibaDetailActivity(): RecibaDetailActivity

    /**
     *  RECIBA REPORT ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(HaulageFragmentProvider::class), (RecibaFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindRecibaReportActivity(): RecibaReportActivity

//    /**
//     *  CATEGORIES REPORTS ACTIVITY
//     */
//    @ContributesAndroidInjector()
//    @ActivityScope
//    internal abstract fun bindCategorySyncReportActivity(): CategorySyncReportActivity

    /**
     *  COMPARATION ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindComparationActivity(): ComparisonActivity

//    /**
//     *  REPORTS ACTIVITY
//     */
//    @ContributesAndroidInjector(modules = [(SyncFragmentProvider::class)])
//    @ActivityScope
//    internal abstract fun bindReportSyncActivity(): ReportSyncActivity


    /**
     *  SYNC FORCED
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSyncForcedActivity(): SyncForcedActivity

    /**
     *  PLANNING ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPlanningActivity(): PlanningActivity

    /**
     *  DETAIL IN PACKAGE ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindRosterDetailPackageActivity(): RosterDetailPackageActivity

    /**
     *  CROP REPORT ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindCropReportActivity(): CropReportActivity

    /**
     *  REPORT QUALITY ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindReportQualityActivity(): ReportQualityActivity

    /**
     *  REPORT QUALITY FINISHED PRODUCT ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindReportQualityFinishedProductActivity(): ReportQualityFinishedProductActivity

    /**
     *  REPORT WASTE CONTROL ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindWasteControlActivity(): WasteControlActivity

    /**
     *  REPORT WASTE CONTROL TEST ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindWasteControlTestActivity(): WasteControlTestActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindWasteControlTestFragmentFragment(): WasteControlTestFragment

    /**
     *  REPORT QUALITY FINISHED PRODUCT ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFinishedProductQualityActivity(): FinishedProductQualityActivity

    /**
     *  REPORT QUALITY ORDER ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindQualityOrderActivity(): QualityOrderActivity

    /**
     *  REPORT QUALIFIED CARRY ORDER
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindQualifiedCarryOrderActivity(): QualifiedCarryOrderActivity

    /**
     * EXTRA HOURS ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(ExtraHoursFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindExtraHoursActivity(): ExtraHoursActivity

    /**
     * MESSAGES ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(ExtraHoursFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindMessagesActivity(): ExtraHoursMessagesActivity

    /**
     * DAILY REPORT ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(ProductionRosterFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindDailyReportActivity(): DailyReportActivity

    /**
     * SEARCH COLLABORATOR ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(CollaboratorsFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindSearchCollaboratorActivity(): SearchCollaboratorActivity

    /**
     * DAILY REPORT ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindProductQualityReviewFragment(): ProductQualityReviewFragment

    /**
     * HYDROPONIC REPORT ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFertirriegoHydroponicReportActivity(): FertirriegoHydroponicReportActivity

    /**
     *  SOIL REPORT ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFertirriegoSoilReportActivity(): FertirriegoSoilReportActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindProductQualityReviewDetailFragment(): ProductQualityReviewDetailFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindGalleryDialog(): GalleryDialog

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindRootQualityDialog(): RootQualityDialog

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindEditMaxValvuleDialog(): EditMaxValvuleDialog

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyDetailDialog(): PhenologyDetailDialog

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignDialog(): SignDialog

    /* PHENOLOGY */

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyActivitiesActivity(): PhenologyActivitiesActivity


    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMenuActivitiesPhenologyFragment(): MenuActivitiesPhenologyFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyActivity(): PhenologyActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyFragment(): PhenologyFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyVarietiesFragment(): PhenologyVarietiesFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyVarietiesPlantsActivity(): PhenologyVarietiesPlantsActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyVarietiesPlantsFragment(): PhenologyVarietiesPlantsFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyVarMeasureDialog(): PhenologyVarMeasureDialog

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPhenologyVarClusterDialog(): PhenologyVarClusterDialog

    /**
     * HARVEST VALIDATION ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(HarvestValidationFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindHarvestValidationActivity(): HarvestValidationActivity

    /**
     * RECEPTION QUALITY ACTIVITY
     */
    @ContributesAndroidInjector(modules = [(ReceptionQualityFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindReceptionQualityActivity(): ReceptionQualityActivity

    /**
     * SYNC ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSyncActivity(): SyncActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignCommentDialog(): SignCommentDialog

    /**
     * SIGNATURE DIALOG
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignatureDialogFragment(): SignatureDialogFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindOperationsActivitiesActivity(): OperationsActivitiesActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMenuActivitiesOperationsFragment(): MenuActivitiesOperationsFragment

    /* FERTIRRIEGO*/
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFertiriegoActivitiesActivity(): FertiriegoActivitiesActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMenuMenuActivitiesFertiriegoFragment(): MenuActivitiesFertiriegoFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFertirriegoLotActivity(): FertirriegoLotActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFertirriegoMonitoringLotActivity(): FertirriegoMonitoringLotActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFertirriegoPHCELotActivity(): FertirriegoPHCELotActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFertirriegoHydroponicMonitoringActivity(): FertirriegoHydroponicMonitoringActivity

    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun findFertirriegoPulseActivity(): FertirriegoPulseActivity

    /**
     *  FLOOR FRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindFloorFragment(): SoilFragment

    /**
     *  MOMITORING FRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMonitoringFragment(): MonitoringFragment

    /**
     *  PH-CE FRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPHCEFragment(): PHCEFragment

    /**
     *  PULSE FRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPulseFragment(): PulseFragment

    /**
     *  SUBTRATE
     */
    @ContributesAndroidInjector(modules = [(HaulageFragmentProvider::class)])
    @ActivityScope
    internal abstract fun bindSubstrateFragment(): hydroponicFragment

    /* MIPE*/
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMipeActivitiesActivity(): MipeActivitiesActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMipeMonitoringLotsActivity(): MipeMonitoringLotsActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMipeSelectTableActivity(): MipeSelectTableActivity

    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindMipeMonitoringActivity(): MipeMonitoringActivity

    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindMipeReportActivity(): MipeReportActivity

    /**
     * MenuActivitiesMipeFragment
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMenuActivitiesMipeFragment(): MenuActivitiesMipeFragment

    /**
     * MIPELOTFRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindMipeLotFragment(): MipeLotFragment

    /**
     * SelectGrooveFragment
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindSelectGrooveFragment(): SelectGrooveFragment

    /**
     * MIPEREPORT
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindMipeReportFragment(): MipeReportFragment

    /**
     * MipeSelectPlantActivity
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindMipeSelectPlantActivity(): MipeSelectPlantActivity

    /**
     * ProductionRangeActivity
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindProductionRangeActivity(): ProductionRangeActivity

    /* CROP VALIDATION*/
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindCropValidationActivity(): CropValidationActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindCropIssuesValidationActivity(): CropIssuesValidationActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindCropIssuesValidationFragment(): CropIssuesValidationFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignPhotoCropValidationDialog(): SignPhotoCropValidationDialog

    /* RECIBA QUALITY*/
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignReportRecibaQuality(): ReportRecibaQualityActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignRecibaQualityReview(): RecibaQualityReviewActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignRecibaQualityDetailActivity(): RecibaQualityDetailActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignRecibaQualityReviewDetaill(): RecibaQualityReviewDetailActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSignRecibaQualityReviewDetailFragment(): RecibaQualityReviewDetailFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindHydroponicReportFragment(): HydroponicReportFragment

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindSoilReportFragment(): SoilReportFragment

    /**
     * MessageActivity
     */
    @ContributesAndroidInjector
    @ActivityScope
    internal abstract fun bindMessageActivity(): MessageActivity

    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindInboxFragment(): InboxFragment

    /**
     * VISUAL SUPPORT DIALOG
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindVisualSupportDialog(): VisualSupportDialog

    /**
     * HOUR DIALOG
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindHourDialog(): HourDialog

    /**
     *  POLLINATION LOTS ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPolinationLotsActivity(): PollinationLotsActivity

    /**
     *  POLLINATION ACTIVITY
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPollinationActivity(): PollinationActivity

    /**
     *  POLLINATION FRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPollinationFragment(): PollinationFragment

    /**
     *  POLLINATION REPORT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPollinationReportActivity(): PollinationReportActivity

    /**
     *  POLLINATION REPORT FRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindPollinationReportFragment(): PollinationReportFragment

    /**
     *  BARCODESCANNERX FRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindBarcodeScannerXFragment(): BarcodeScannerXFragment

    /**
     *  BoxAverageWeight FRAGMENT
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindBoxAverageWeightFragment(): BoxAverageWeightFragment

    /**
     *  RecibaAverageWeightDialog dialog
     */
    @ContributesAndroidInjector()
    @ActivityScope
    internal abstract fun bindRecibaAverageWeightDialog(): RecibaAverageWeightDialog
}