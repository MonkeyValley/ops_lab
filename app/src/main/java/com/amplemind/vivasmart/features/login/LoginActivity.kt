package com.amplemind.vivasmart.features.login

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.content.res.AppCompatResources
import android.view.View
import android.widget.*
import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.forceFocus
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.LoginModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.RetrofitHolder
import com.amplemind.vivasmart.core.utils.USER_ID
import com.amplemind.vivasmart.features.login.viewModel.LoginViewModel
import com.amplemind.vivasmart.vo_features.sync.SyncActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.content_login_activity.*
import javax.inject.Inject

@SuppressLint("Registered")
open class LoginActivity : BaseActivity(), View.OnClickListener, View.OnLongClickListener {

    companion object {

        val TAG = LoginActivity::class.java.simpleName
        val LOGIN_ERROR_MESSAGE = "Usuario y/o contraseña incorrecto, favor de verificar"

    }
    @Inject
    lateinit var viewModel: LoginViewModel

    @Inject
    lateinit var mRetrofitHolder: RetrofitHolder

    @Inject
    lateinit var preferences: UserAppPreferences

    lateinit var dialog: ProgressDialog

    fun getLayout() = R.layout.content_login_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())

        logo.setOnLongClickListener(this)
        logo.setOnClickListener(this)
        tv_forgotPassword.setOnClickListener(this)
        btn_login.setOnClickListener(this)

        setProgressDialog()

        viewModel.observeValidationChanges().subscribe(this::onInputValidationChange).addTo(subscriptions)
        viewModel.observeProgressStatus().subscribe(this::onProgressRequestChange).addTo(subscriptions)
        viewModel.observeSuccessLogin().subscribe(this::onLoginSuccess).addTo(subscriptions)
        viewModel.observeFailLogin().subscribe(this::changeInputPasswordToErrorStatus).addTo(subscriptions)
        viewModel.observeSuccessGetUser().subscribe(this::onGetUserSuccess).addTo(subscriptions)

        if(BuildConfig.FLAVOR == "local") {
            mRetrofitHolder.setHost(preferences.selectedServerUrl)
        }

    }

    private fun setProgressDialog() {
        dialog = ProgressDialog(this)
        dialog.setMessage(getString(R.string.server_connection))
        dialog.setCancelable(false)
    }

    override fun onLongClick(p0: View?): Boolean {
        when (p0!!.id) {
            R.id.logo -> {
                selectServerUrl()
            }
        }

        return true
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.logo -> {
                if(BuildConfig.FLAVOR.equals("local")) {
                    selectServerUrl()
                }
            }
            R.id.tv_forgotPassword -> {
                startActivity(Intent(
                        this,
                        RecoverPasswordActivity::class.java)
                )
            }
            R.id.btn_login -> {
                viewModel.validateAccessLoginTextFields(
                        ed_username.text.toString(),
                        ed_password.text.toString()
                )
            }
        }
    }

    fun selectServerUrl() {

        var isServerUrlPreset = viewModel.serverUrlIsPreset
        var serverUrl = viewModel.serverUrl
        var customServerUrl = viewModel.customServerUrl

        val view: View = layoutInflater.inflate(R.layout.dialog_server_select, null)

        val swPreset = view.findViewById<Switch>(R.id.presets)
        val spAddresses = view.findViewById<Spinner>(R.id.addresses)
        val etIpAddress = view.findViewById<EditText>(R.id.ipAddress)
        val btnOk = view.findViewById<Button>(R.id.ok)
        val btnCancel = view.findViewById<Button>(R.id.cancel)

        swPreset.isChecked = isServerUrlPreset
        spAddresses.isEnabled = isServerUrlPreset
        etIpAddress.isEnabled = !isServerUrlPreset

        swPreset.setOnCheckedChangeListener { buttonView, isChecked ->
            isServerUrlPreset = isChecked
            spAddresses.isEnabled = isChecked
            etIpAddress.isEnabled = !isChecked
        }

        val items = arrayOf(
                BuildConfig.SERVER_URL_DEV
                //BuildConfig.SERVER_URL_QA
                //BuildConfig.SERVER_URL_QAE
        )

        spAddresses.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items)
        spAddresses.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                etIpAddress.setText(spAddresses.getItemAtPosition(position).toString())
                serverUrl = spAddresses.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        spAddresses.setSelection(items.indexOf(serverUrl))
        etIpAddress.setText(customServerUrl)

        val dialog = AlertDialog.Builder(this)
                .setView(view)
                .show()

        btnOk.setOnClickListener { v: View ->
            var url: String
            if (isServerUrlPreset) {
                serverUrl = spAddresses.selectedItem.toString()
                url = serverUrl
            } else {
                customServerUrl = etIpAddress.text.toString()
                url = customServerUrl
            }

            if(url != "" || isServerUrlPreset) {
                if (mRetrofitHolder.setHost(url)) {
                    viewModel.serverUrl = serverUrl
                    viewModel.customServerUrl = customServerUrl
                    viewModel.serverUrlIsPreset = isServerUrlPreset
                    if (dialog.isShowing) dialog.dismiss()
                } else {
                    etIpAddress.setError("Esta dirección ya esta ingresada")
                }
            } else {
                etIpAddress.setError("Campo de dirección vacio")
            }

        }

        btnCancel.setOnClickListener{ v: View ->
            if (dialog.isShowing) dialog.dismiss()
        }
    }

    fun changeInputPasswordToDefaultStatus() {
        txtInputPassword.setPasswordVisibilityToggleTintList(
                AppCompatResources.getColorStateList(this, android.R.color.white))
        txtInputPassword.isErrorEnabled = false
        txtInputPassword.error = null
        txtInputPassword.setHintTextAppearance(R.style.CustomTextInputLayout)
    }

    fun changeInputPasswordToErrorStatus(msg: String) {
        txtInputPassword.setPasswordVisibilityToggleTintList(
                AppCompatResources.getColorStateList(this, android.R.color.holo_red_dark))
        txtInputPassword.isErrorEnabled = true
        txtInputPassword.error = msg
        txtInputPassword.setHintTextAppearance(R.style.CustomTextInputError)

        textInputLayout_user.setHintTextAppearance(R.style.CustomTextInputError)
        forceFocus(txtInputPassword)
        ed_username.background = ContextCompat.getDrawable(this, R.drawable.edittext_selector)
        hideKeyboard(this)
    }

    private fun onProgressRequestChange(requestInProgress: Boolean) {
        if (requestInProgress) {
            changeInputPasswordToDefaultStatus()
            dialog.show()
        } else {
            dialog.dismiss()
        }
    }

    fun onLoginSuccess(loginResponse: LoginModel.LoginResponse) {
        viewModel.saveToken(loginResponse.token)
        viewModel.getUserRol()
        viewModel.getUser()
    }

    fun onGetUserSuccess(response: Boolean) {
        if (response) {
            viewModel.updateTokenToDataUpload()
            //TODO: Se necesita generar el evento de UnsubscribeTopics al momento de cerrar sesión
            //Log.i(TAG, "LOGGED IN!")
            USER_ID = Gson().fromJson(preferences.userInfo, UserModel::class.java).id
            SyncActivity.startForFirstSync(this)
            finish()
        }
    }

    fun onInputValidationChange(isValid: Boolean) {
        if (isValid) {
            viewModel.doLogin(AccountRepository.LoginRequest(
                    ed_username.text.toString(),
                    ed_password.text.toString())).addTo(subscriptions)
        } else {
            changeInputPasswordToErrorStatus(LOGIN_ERROR_MESSAGE)
        }
    }

}
