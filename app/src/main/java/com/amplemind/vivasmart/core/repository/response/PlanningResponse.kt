package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.PlanningModel
import com.google.gson.annotations.SerializedName

data class PlanningResponse(@SerializedName("data") val data: List<PlanningModel>)