package com.amplemind.vivasmart.features.packaging_quality.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemReceptionQualityCriteriaBinding
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ItemReceptionQualityCriteriaViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.IssueModel
import com.jakewharton.rxbinding3.view.clicks
import com.jakewharton.rxbinding3.view.longClicks
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class ReceptionQualityCriteriaAdapter (private val mEventBus: EventBus, private val mSampleSize: Int)
    : RecyclerView.Adapter<ReceptionQualityCriteriaAdapter.ReceptionQualityCriteriaViewHolder>() {

    private var mItems = listOf<ItemReceptionQualityCriteriaViewModel>()

    var totalDamaged: Int = 0

    var totalConditionallyDamaged: Int = 0

    val totalPermanentlyDamaged: Int
        get() = totalDamaged - totalConditionallyDamaged


    override fun onViewRecycled(holder: ReceptionQualityCriteriaViewHolder) {
        super.onViewRecycled(holder)
        holder.cleanUp()
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ReceptionQualityCriteriaViewHolder {
        val binding = ItemReceptionQualityCriteriaBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReceptionQualityCriteriaViewHolder(binding)
    }

    override fun getItemCount(): Int = mItems.size

    override fun onBindViewHolder(viewHolder: ReceptionQualityCriteriaViewHolder, position: Int) {
        viewHolder.bind(mItems[position])
    }

    fun setItems(items: List<ItemReceptionQualityCriteriaViewModel>) {
        mItems = items
        notifyDataSetChanged()
    }

    fun getIssues() =
            mItems.filter {viewModel ->
                viewModel.count > 0
            }

    private fun sendCriteriaCountUpdatedEvent() {
        mEventBus.send(OnCriteriaCountUpdatedEvent(this))
    }

    fun addCount(position: Int, count: Int) {
        val viewModel = mItems[position]
        totalDamaged += count
        viewModel.count += count
        if (viewModel.issue?.isCondition == true) {
            totalConditionallyDamaged += count
        }
        viewModel.updateCountFields(mSampleSize)
        sendCriteriaCountUpdatedEvent()
    }

    inner class ReceptionQualityCriteriaViewHolder(val binding: ItemReceptionQualityCriteriaBinding)
        : RecyclerView.ViewHolder(binding.root) {

        private val subscriptions = AndroidDisposable()

        fun cleanUp() {
            subscriptions.dispose()
        }

        fun bind(viewModel: ItemReceptionQualityCriteriaViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()

            binding.tvIssueName.clicks()
                    .subscribe {
                        if (viewModel.issue != null) {
                            mEventBus.send(OnIssueClickedEvent(this@ReceptionQualityCriteriaAdapter, adapterPosition, viewModel.issue))
                        }
                    }
                    .addTo(subscriptions)

            binding.tvTotal.longClicks()
                    .filter {
                        viewModel.count > 0
                    }
                    .subscribe {
                        addCount(adapterPosition, -1)
                    }
                    .addTo(subscriptions)

            binding.tvTotal.clicks()
                    .buffer(1500, TimeUnit.MILLISECONDS, 2)
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter{
                        totalDamaged < mSampleSize
                    }
                    .subscribe {clicks ->
                        addCount(adapterPosition, clicks.size / 2)
                    }
                    .addTo(subscriptions)


        }
    }

    data class OnIssueClickedEvent(
            val adapter: ReceptionQualityCriteriaAdapter,
            val position: Int,
            val issue: IssueModel
    )

    data class OnCriteriaCountUpdatedEvent(
            val adapter: ReceptionQualityCriteriaAdapter
    )
}