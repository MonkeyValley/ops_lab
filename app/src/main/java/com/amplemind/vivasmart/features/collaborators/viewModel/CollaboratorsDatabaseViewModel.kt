package com.amplemind.vivasmart.features.collaborators.viewModel

import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.CollaboratorsRepository
import com.amplemind.vivasmart.core.repository.local.CollaboratorsDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class CollaboratorsDatabaseViewModel @Inject constructor(private val repository: CollaboratorsRepository,
                                                         private val repository2: com.amplemind.vivasmart.vo_core.repository.CollaboratorsRepository,
                                                         private val preferences: UserAppPreferences,
                                                         private val database: CollaboratorsDao,
                                                         var mediator: Mediator,
                                                         private val apiErrors: HandleApiErrors) {

    private var collaborators = mutableListOf<ItemCollaboratorsViewModel>()

    private var searchCollaborators = mutableListOf<ItemCollaboratorsViewModel>()

    private var collaboratorsSubject = BehaviorSubject.create<List<ItemCollaboratorsViewModel>>()

    private var collaboratorsRecentSubject = BehaviorSubject.create<List<ItemCollaboratorsViewModel>>()

    private var searchResultSubject = BehaviorSubject.create<List<ItemCollaboratorsViewModel>>()

    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val onFail: BehaviorSubject<String> = BehaviorSubject.create()

    private val progressItem = BehaviorSubject.create<Pair<Int, Boolean>>()

    private val assingSubject = BehaviorSubject.create<CollaboratorsInLinesResult>()

    private val sendCollaboratorsFinish = BehaviorSubject.create<List<ActivityLogModel>>()

    private val isLoading = BehaviorSubject.create<Boolean>()

    private var searchOnline: String? = null

    private var totalCollaborators = 0

    private var totalSearchCollaborators = 0

    private var stage: Int? = null

    fun setupData(models: List<CollaboratorModel>) {
        val viewModels = mutableListOf<ItemCollaboratorsViewModel>()

        for (model in models) {
            viewModels.add(ItemCollaboratorsViewModel(model))
        }

        collaborators.addAll(viewModels)
        collaboratorsSubject.onNext(collaborators)
    }

    fun getListCollaborators(lines: Boolean): Disposable {
        TODO("NOT IMPLEMENTED")
    }

    /**
     *  obtains the limit of available users
     */
    fun getTotalCollaborators(): Int {
        return totalCollaborators
    }

    /**
     *  obtains the limit of available users in the search
     */
    fun getTotalCollaboratorsSearch(): Int {
        return totalSearchCollaborators
    }

    fun getMoreCollaborators(offset: Int, position: Int, lines: Boolean): Disposable {
        TODO("NOT IMPLEMENTED")
    }

    fun isLoading(): BehaviorSubject<Boolean> {
        return isLoading
    }

    fun onProgressItem(): BehaviorSubject<Pair<Int, Boolean>> {
        return progressItem
    }

    fun sendCollaborators(activityCode: ActivityCodeModel, collaborators: List<CollaboratorModel>): Observable<List<ActivityLogModel>?> {
        TODO("NOT IMPLEMENTED")
    }

    fun sendCollaboradors(selectedIDs: MutableList<Int>, code_activity: Int, activityCode: Int): Disposable {
        val data = sendCollaborators(selectedIDs, code_activity)
        return repository.sendCollaborators(preferences.token, CollaboratorsRepository.SendCollaboratorsReponse(data))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { it -> mapSendCollaborator(it, selectedIDs) }
                .subscribe({
                    doAsync {
                        it.map {
                            it.activityCode = activityCode
                            it.stageId = stage ?: 0
                            it.activityId = mediator.bundleItemActivitiesPayroll?.activityId ?: 0
                            database.insertCollaborator(it)
                        }
                    }
                    //sendCollaboratorsFinish.onNext(it)
                }, {
                    onFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    /*
    fun saveLocalCollaborators(selectedIDs: MutableList<Int>, code_activity: Int, activityCode: Int) {
        progressStatus.onNext(true)
        doAsync {
            val collaboratorsList = saveCollaborators(selectedIDs, code_activity, activityCode)
            uiThread {
                progressStatus.onNext(false)
                sendCollaboratorsFinish.onNext(collaboratorsList)
            }
        }
    }
    */

    private fun saveCollaborators(selectedIDs: MutableList<Int>, code_activity: Int, activityCode: Int): List<CollaboratorsListReponse> {
        val collaboratorsList = mutableListOf<CollaboratorsListReponse>()
        selectedIDs.map { code ->
            val collaboratorsFiltered = collaborators.filter { it.id == code }
            if (collaboratorsFiltered.isNotEmpty()) {
                val vc = collaboratorsFiltered.first()
                var collaborator = database.getCollaboratorById(vc.id)?.collaborator
                if (collaborator == null) {
                    collaborator = CollaboratorsReponse(vc.birthday, vc.employeeCode!!, null, vc.url, vc.employeeCode, null)
                }
                val collaboratorResponse = CollaboratorsListReponse(code_activity, collaborator, code, null, true, false,
                        0.0, true, false, 0, date = 0, stageId = stage!!, activityId = mediator.bundleItemActivitiesPayroll?.activityId
                        ?: 0,
                        activityCode = activityCode)
                val id = database.insertCollaborator(collaboratorResponse)
                collaboratorResponse.id = id.toInt()
                collaboratorsList.add(collaboratorResponse)
            }

        }
        return collaboratorsList
    }

    fun getCollaboratorSelected(selectedIDs: MutableList<Int>): MutableList<CollaboratorModel> {
        return collaborators.filter { selectedIDs.contains(it.id) }.map { viewModel -> return@map viewModel.model_data }.toMutableList()
    }

    fun mapSendCollaborator(data: CollaboratorsReponseActivities, selectedIDs: MutableList<Int>): List<CollaboratorsListReponse> {
        return data.data.filter { selectedIDs.contains(it.collaboratorId) && !it.isDone }
    }

    fun finishSendCollaborators() = sendCollaboratorsFinish

    private fun sendCollaborators(selectedIDs: MutableList<Int>, code_activity: Int): MutableList<SendCollaboratorsModel> {
        val bulk = mutableListOf<SendCollaboratorsModel>()
        selectedIDs.forEach {
            bulk.add(SendCollaboratorsModel(code_activity, it))
        }
        return bulk
    }

    private fun mapModelToViewModels(models: List<CollaboratorModel>): List<ItemCollaboratorsViewModel> {
        val list = arrayListOf<ItemCollaboratorsViewModel>()
        for (item in models.sortedBy { it.name }) {
            list.add(ItemCollaboratorsViewModel(item))
        }
        return list
    }

    fun getCollaborators(): BehaviorSubject<List<ItemCollaboratorsViewModel>> {
        return collaboratorsSubject
    }

    fun getCollaboratorsSearchResult(): BehaviorSubject<List<ItemCollaboratorsViewModel>> {
        return searchResultSubject
    }

    fun getCollaboratorsRecent(): BehaviorSubject<List<ItemCollaboratorsViewModel>> {
        return collaboratorsRecentSubject
    }

    fun getListCollaboratorsRecent(): Disposable {
        return repository.getCollaboratorsRecent()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelRecentToViewModels(it) }
                .subscribe({
                    collaboratorsRecentSubject.onNext(it)
                }, {
                    onFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelRecentToViewModels(data: List<CollaboratosRecent>): List<ItemCollaboratorsViewModel> {
        val list = arrayListOf<ItemCollaboratorsViewModel>()
        data.forEach {
            list.add(ItemCollaboratorsViewModel(CollaboratorModel(it.id_collaborator, it.name, it.image, it.employee_code, it.birthdate
                    ?: "")))
        }
        return list
    }

    fun searchCollaborators(query: String): List<ItemCollaboratorsViewModel> {
        if (!query.isEmpty()) {
            return collaborators.filter { it.employeeCode!!.contains(query) }
        }
        return collaborators
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return onFail
    }

    fun searchCollaboratorName(query: String, offset: Int, lines: Boolean): Disposable {
        TODO("NOT IMPLEMENTED")
    }

    fun searchMoreCollaboratorName(offset: Int, position: Int, lines: Boolean): Disposable {
        TODO("NOT IMPLEMENTED")
    }

    fun saveRecentCollaborators(collaborators: List<CollaboratorsListReponse>): Disposable {
        return io.reactivex.Observable.fromCallable {
            repository.saveCollaboratorsRecent(collaborators)
        }.subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe()
    }

    fun setStage(stage: Int) {
        this.stage = stage
    }

    fun assingCollaborators(hasInternet: Boolean, users: MutableList<CollaboratorModel>, activity_id: Int?, nameLine: String, collaborator_left: Int, idLine: Int): Disposable {
        val list = mutableListOf<AssignCollaboratorModel>()

        for (user in users) {
            list.add(AssignCollaboratorModel(mediator.bundleItemLinesPackage!!, activity_id!!, user.id, null, true))
        }
        val observer = if (hasInternet) {
            assignCollaboratorByHour(list)
        } else {
            assignCollaboratorByHourOffline(list, users, nameLine, collaborator_left, activity_id!!, idLine)
        }
        return observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({ collaborators ->
                    assingSubject.onNext(collaborators)
                }, {
                    onFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun assignCollaboratorByHourOffline(list: MutableList<AssignCollaboratorModel>, idCollaborator: List<CollaboratorModel>, nameLine: String,
                                                collaborator_left: Int, activity_id: Int, idLine: Int): Observable<CollaboratorsInLinesResult> {
        return repository.saveCollaboratorsAndLineLocal(list, idCollaborator, nameLine, collaborator_left, activity_id, idLine, null, mediator.getMapCountBoxRequest())
    }


    private fun assignCollaboratorByHour(list: MutableList<AssignCollaboratorModel>): Observable<CollaboratorsInLinesResult> {
        val assing = CollaboratorsRepository.AssingCollaborators(list, mediator.getMapCountBoxRequest())
        return repository.assingCollaborator(preferences.token, assing)
    }

    fun onAssingSucess(): BehaviorSubject<CollaboratorsInLinesResult> {
        return assingSubject
    }


}
