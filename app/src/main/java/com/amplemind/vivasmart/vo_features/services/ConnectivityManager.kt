package com.amplemind.vivasmart.vo_features.services

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityManager @Inject constructor () {

    companion object {
        val TAG = ConnectivityManager::class.java.simpleName
    }

    val onConnectionChanged = BehaviorSubject.createDefault(true)

    private var mDisposable: Disposable? = null

    fun setUp() {
        mDisposable = onConnectionChanged
            .distinctUntilChanged()
            .filter {hasInternet ->
                !hasInternet
            }
            .switchMap {
                ReactiveNetwork.observeInternetConnectivity()
                        .takeUntil {hasInternet -> hasInternet }
                        .distinctUntilChanged()
                        .filter { hasInternet -> hasInternet }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {hasInternet ->
                onConnectionChanged.onNext(hasInternet)
            }.subscribe()
    }

    fun cleanUp() {
        mDisposable?.dispose()
    }

    fun notifyNoInternetDetected() {
        onConnectionChanged.onNext(false)
    }

    fun notifyAndWaitConnection(scheduler: Scheduler = AndroidSchedulers.mainThread()): Observable<Boolean> {
        notifyNoInternetDetected()
        return onConnectionChanged
                .observeOn(scheduler)
                .filter { hasInternet -> hasInternet }
    }




}
