package com.amplemind.vivasmart.features.production_roster.adapters.package_roster

import com.amplemind.vivasmart.core.extensions.formatTime
import com.amplemind.vivasmart.features.production_roster.ItemRosterDetailViewModel
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import io.reactivex.subjects.BehaviorSubject
import java.util.*

class RosterDetailPackageViewModel constructor(titleModel: DetailRosterTitlePackageViewModel, details: MutableList<ItemRosterDetailViewModel>) :
        ExpandableGroup<ItemRosterDetailViewModel>(titleModel.name, details) {

    val reports = details

    var totalTime = ""
    var extraTime = ""
    var totalUnit = ""
    var totalMoney = ""
    var status = false
    var statusSync : Boolean? = null
    var type = ""
    var extraQuantity = 0.0

    private val notifyCountChange = BehaviorSubject.create<Boolean>()

    init {
        setup()
    }

    fun updateTitle() {
        notifyCountChange.onNext(true)
    }

    fun getNotifyChange(): BehaviorSubject<Boolean> {
        return notifyCountChange
    }

    fun setup() {
        totalTime = formatTime(reports.sumBy { it.timeMinutes } * 60L)
        extraTime = if(reports.sumBy { it.timeExtra }  > 0) formatTime(reports.sumBy { it.timeExtra } * 60L) else "-"

        extraQuantity = reports.sumByDouble { it.extraQuantity }

        totalUnit = "%.2f".format((reports.sumByDouble { it.cant ?: 0.0 } + extraQuantity))

        totalMoney = "%.2f".format(Locale.US,reports.sumByDouble {
            when {
                it.isTraining -> it.moneyTraining + it.moneyHrsExtra
                it.isPractice -> it.moneyPractice + it.moneyHrsExtra
                else -> it.money + it.moneyHrsExtra
            }
        })
        status = reports.first().status
        statusSync = reports.first().statusSync
        type = if (reports.first().isTimeLine) "H" else "D"
    }

}
