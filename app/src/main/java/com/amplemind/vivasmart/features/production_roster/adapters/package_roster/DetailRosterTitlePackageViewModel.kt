package com.amplemind.vivasmart.features.production_roster.adapters.package_roster

import com.amplemind.vivasmart.core.repository.model.DetailtRosterLinesResponse

class DetailRosterTitlePackageViewModel(val name : String,val id_activity : Int, val list : List<DetailtRosterLinesResponse>) {
    val name_title = name
    val hrs_result = ""
    val cant_result = 0
    val hrsE_result = 0
    val money = 0
}
