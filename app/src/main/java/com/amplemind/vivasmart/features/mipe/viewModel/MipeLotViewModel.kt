package com.amplemind.vivasmart.features.mipe.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.mipe.service.MipeService
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MipeLotViewModel @Inject constructor(
        private val apiErrors: HandleApiErrors,
        private val service: MipeService) : BaseViewModel() {

    private val requestSuccess_lots = BehaviorSubject.create<List<LotMipeItemViewModel>>()
    private val listLots = mutableListOf<LotMipeItemViewModel>()

    fun getLots(): Disposable {
        return service.getLots()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModels)
                .subscribe({
                    listLots.clear()
                    listLots.addAll(it)
                    requestSuccess_lots.onNext(listLots)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModels(models: List<LotModel>): List<LotMipeItemViewModel> {
        val list = arrayListOf<LotMipeItemViewModel>()
        for (item in models) {
            val lotmodel = LotMipeItemViewModel(item)
            list.add(lotmodel)
        }
        return list
    }

    fun onSuccessRequest_lots(): BehaviorSubject<List<LotMipeItemViewModel>> {
        return requestSuccess_lots
    }

}