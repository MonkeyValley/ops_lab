package com.amplemind.vivasmart.core.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import com.amplemind.vivasmart.R
import javax.inject.Inject

class AndroidPermission @Inject constructor(){


    val REQUEST_CODE_PERMISSIONS = 300
    val ALL_PERMISSIONS = 0

    val PERMISSION = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE)


    /**
     * @param context        current activity or fragment in app
     * @param permissions    arraylist of mPermission for app
     * @param allPermissions flag all mPermission
     * @return true if have all mPermission
     */
    fun verifyHavePermissions(context: Activity, mPermissions: kotlin.Array<String> = PERMISSION ): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            for (permission in mPermissions)
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(context, permission)) {
                        createDialogPermissions(context, ALL_PERMISSIONS)
                    } else {
                        ActivityCompat.requestPermissions(context, mPermissions, REQUEST_CODE_PERMISSIONS)
                    }
                    return false
                }
        return true
    }

    /**
     * create alert dialog active all mPermission
     *
     * @param activity       context app
     * @param typePermission all mPermission
     */
    private fun createDialogPermissions(activity: Activity, typePermission: Int) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.warning)
                .setCancelable(false)
                .setMessage(R.string.message_permission)
                .setPositiveButton(R.string.configuration)
                {
                    dialogInterface, i ->
                    val uri = Uri.fromParts("package", activity.packageName, null)
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    intent.data = uri
                    activity.startActivityForResult(intent,REQUEST_CODE_PERMISSIONS)
                }.create().show()
    }

    /**
     * @param context current activity
     * @return is activate mPermission camera
     */
    fun cameraPermission(context: Context): Boolean {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    /**
     * @param context current activity
     * @return is activate mPermission camera
     */
    fun galleryPermission(context: Context): Boolean {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    /**
     * @param context current activity
     * @return is activate mPermission camera
     */
    fun writePermission(context: Context): Boolean {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

}