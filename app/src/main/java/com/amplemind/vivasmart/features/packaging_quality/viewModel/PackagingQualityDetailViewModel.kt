package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.core.repository.response.UnitsDataResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.haulage.viewModel.CarryOrderViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PackagingQualityDetailViewModel @Inject constructor(private val repository: PackagingQualityRepository,
                                                          private val apiErrors: HandleApiErrors) {

    private val getCarryOrder = BehaviorSubject.create<CarryOrderViewModel>()
    private val requestGetUnits = BehaviorSubject.create<List<SpinnerUnitsViewModel>>()
    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()

    private var model: CarryOrderModel? = null
    private val listUnits = mutableListOf<SpinnerUnitsViewModel>()

    enum class FlagStartReports {
        PIECE,
        TOTAL
    }

    fun getDataQualityPackages(id: Int): Disposable {
        return repository.getItemQualityPackages(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { models ->
                    models.data.map {
                        model = it
                        CarryOrderViewModel(it)
                    }.first()
                }
                .subscribe({
                    getCarryOrder.onNext(it)
                    it.cropId
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getUnits(): Disposable {
        return repository.getUnits()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    mapUnits(it)
                }
                .subscribe({
                    listUnits.clear()
                    listUnits.addAll(it)
                    requestGetUnits.onNext(listUnits)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLocalUnits(): Disposable {
        return repository.getLocalUnits()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { mapUnits(it) }
                .subscribe({
                    listUnits.clear()
                    listUnits.addAll(it)
                    requestGetUnits.onNext(listUnits)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapUnits(response: UnitsDataResponse): List<SpinnerUnitsViewModel> {
        val units = mutableListOf<SpinnerUnitsViewModel>()
        response.data.map { data ->
            data.units.map { unitsList ->
                units.add(SpinnerUnitsViewModel(unitsList.unit))
            }
        }
        return units
    }

    fun getCropId(): Int {
        if (model == null) return 0
        return model!!.cropId.toInt()
    }

    fun getCropName(): String {
        if (model == null) return ""
        return model!!.crop?.name ?: ""
    }

    fun getCarryOrderId(): Int {
        if (model == null) return 0
        return model!!.id
    }

    fun getUnitId(position: Int): Int {
        if (listUnits.size <= position) return 0
        return listUnits[position].id.toInt()
    }

    fun onRequestSuccess(): BehaviorSubject<CarryOrderViewModel> {
        return getCarryOrder
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestGetUnits(): BehaviorSubject<List<SpinnerUnitsViewModel>> {
        return requestGetUnits
    }

}