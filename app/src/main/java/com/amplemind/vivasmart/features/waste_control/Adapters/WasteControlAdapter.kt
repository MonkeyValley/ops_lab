package com.amplemind.vivasmart.features.waste_control.Adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.WasteControlItemDataBinding
import com.amplemind.vivasmart.features.waste_control.models.WasteControlItemViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.BehaviorSubject

class WasteControlAdapter constructor(val list: MutableList<WasteControlItemViewModel>) : RecyclerView.Adapter<WasteControlAdapter.WasteControlViewHolder>() {

    private val onClickSubject = BehaviorSubject.create<WasteControlItemViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WasteControlViewHolder {
        val binding = WasteControlItemDataBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WasteControlViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: WasteControlViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<WasteControlItemViewModel> {
        return onClickSubject
    }

    fun isEmpty() = list.isEmpty()


    inner class WasteControlViewHolder(private val binding: WasteControlItemDataBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: WasteControlItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.cvUser.setOnClickListener {
                    onClickSubject.onNext(item)
                }
                binding.cvUser.setOnLongClickListener {
                    onClickSubject.onNext(item)
                    return@setOnLongClickListener true
                }
                if (item.percentWasteDiv!! >= 99) {
                    binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.backGreen))
                }
//              else if (item.percentWasteDiv > 89 && item.percentWasteDiv <= 93 ) {
//                  binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.yellowAlert))
//              }
                else {
                    binding.TVPercentIssue.setBackgroundColor(binding.root.resources.getColor(R.color.redAlert))
                }
            }
        }

    }
}
