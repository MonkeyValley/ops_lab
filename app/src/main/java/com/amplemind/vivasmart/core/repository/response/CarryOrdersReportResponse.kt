package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.CarryOrderReportModel
import com.google.gson.annotations.SerializedName

data class CarryOrdersReportResponse(@SerializedName("data") val data: List<CarryOrderReportModel>)