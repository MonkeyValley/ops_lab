package com.amplemind.vivasmart.vo_core.repository.remote

import com.amplemind.vivasmart.core.repository.model.UpdateCarryOrderSyncRequest
import com.amplemind.vivasmart.core.repository.model.UpdateCarryOrderSyncResponse
import com.amplemind.vivasmart.core.repository.remote.models.HarvestValidationCarryOrderModel
import com.amplemind.vivasmart.core.repository.request.UpdateCarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.UpdateSignCarryOrderRequest
import com.amplemind.vivasmart.core.repository.response.CarryOrderCondensedResponse
import com.amplemind.vivasmart.core.repository.response.CarryOrderPendingResponse
import com.amplemind.vivasmart.core.repository.response.CarryOrdersResponse
import com.amplemind.vivasmart.core.repository.response.UnitsDataResponse
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.responses.HarvestValidationCriteriaResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by
 *          amplemind on 7/6/18.
 */
interface PackagingQualityApi {

    @GET("/v2/carry_order/quality/condensed/{businessUnitId}")
    @Headers("Content-Type: application/json")
    fun getCarryOrders(
            @Header("Authorization") authentication: String,
            @Path("businessUnitId") businessUnitId: String
    ): Observable<CarryOrderCondensedResponse>

    @GET("/v2/carry_order/revision/condensed/{businessUnitId}")
    @Headers("Content-Type: application/json")
    fun getCarryOrdersReciba(
            @Header("Authorization") authentication: String,
            @Path("businessUnitId") businessUnitId: String
    ): Observable<CarryOrderCondensedResponse>

    @GET("/v2/carry_order")
    @Headers("Content-Type: application/json")
    fun getItemCarryOrder(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "lot,crop,crop.crop_type,user",
            @Query("lot.business_unit_id") businessUnitId: Long,
            @Query("id") id: Long,
            @Query("__logic") logic: String = "AND"
    ): Observable<CarryOrdersResponse>

    @GET("/v2/business_unit")
    @Headers("Content-Type: application/json")
    fun getBusinessUnit(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "units,units.unit",
            @Query("id") businessUnitId: String,
            @Query("__logic") logic: String = "AND"
    ): Observable<UnitsDataResponse>

    @PUT("/v2/carry_order/{carryOrderId}")
    @Headers("Content-Type: application/json")
    fun updateCarryOrder(
            @Header("Authorization") authentication: String,
            @Path("carryOrderId") carryOrderId: Int,
            @Body body: UpdateCarryOrderRequest
    ): Observable<Any>

    @PUT("/v2/carry_order/revision/sign/{carryOrderId}")
    @Headers("Content-Type: application/json")
    fun updateSignCarryOrder(
            @Header("Authorization") authentication: String,
            @Path("carryOrderId") carryOrderId: Int,
            @Body body: UpdateSignCarryOrderRequest
    ): Observable<Any>

    @PUT("/v2/offline/sync/carry_order/revision")
    @Headers("Content-Type: application/json")
    fun syncCarryOrder(
            @Header("Authorization") authentication: String,
            @Body body: UpdateCarryOrderSyncRequest
    ): Observable<UpdateCarryOrderSyncResponse>

    @GET("/v2/quality_carry_order/pending/{businessUnitId}")
    @Headers("Content-Type: application/json")
    fun getCarryOrdersPending(
            @Header("Authorization") authentication: String,
            @Path("businessUnitId") businessUnitId: String
    ): Observable<CarryOrderPendingResponse>

    //https://vivaorganica.tk/v1/carry_order?embed=lot&limit=100&order_by=id.desc&received_date__between=2019-05-01,2019-05-15&reception=true&__logic=AND
    @GET("/v2/carry_order")
    fun getCarryOrdersBetween(
            @Header("Authorization")            authentication: String,
            @Query("embed")                     embed: String = "lot",
            @Query("limit")                     limit: Int = 10000,
            @Query("order_by")                  orderBy: String = "id.desc",
            @Query("__logic")                   logic: String = "AND",
            @Query("revision")                 reception: Boolean = true,
            @Query("lot.business_unit_id")      businessUnitId: Int,
            @Query("received_date__between")    betweenDates: String
    ): Observable<CarryOrdersResponse>

    @GET("/v2/carry_order")
    fun getCarryOrdersForStageBetween(
            @Header("Authorization")            authentication: String,
            @Query("limit")                     limit: Int = 10000,
            @Query("__logic")                   logic: String = "AND",
            @Query("revision")                 reception: Boolean = true,
            @Query("lot_id")                    lotId: Int,
            @Query("received_date__between")    betweenDates: String
    ): Observable<CarryOrdersResponse>

    @GET("/v2/carry_order_harvest")
    fun getHarvestValidationCarryOrders(
            @Header("Authorization")            authentication: String,
            @Query("limit")                     limit: Int = 10000,
            @Query("__logic")                   logic: String = "AND",
            @Query("business_unit_id")          businessUnitId: Long,
            @Query("day")                       date: String
    ): Observable<List<HarvestValidationCarryOrderModel>>

    @GET("/v2/issue")
    fun getHarvestValidationCriteria(
            @Header("Authorization")            authentication: String,
            @Query("limit")                     limit: Int = 10000,
            @Query("__logic")                   logic: String = "AND",
            @Query("business_unit_id")          businessUnitId: Long,
            @Query("area_id")                   areaId: Long = 2,
            @Query("category_id")               categoryId: Long = 6
    ): Observable<HarvestValidationCriteriaResponse>

    @GET("/v2/harvest_quality/{carry_order_id}")
    fun getHarvestValidation(
            @Header("Authorization")            authentication: String,
            @Path("carry_order_id")            carryOrderId: Long
    ): Observable<HarvestValidationModel>

    @POST("/v2/harvest_quality")
    @Headers("Content-Type: application/json")
    fun createHarvestValidation(
            @Header("Authorization")            authentication: String,
            @Body body: HarvestValidationModel
    ): Single<HarvestValidationModel>

    @GET("/v2/quality_carry_order/day/{date}")
    fun getReceptionQualityCarryOrders(
            @Header("Authorization")            authentication: String,
            @Path("date")                       date: String
    ): Observable<ObjectResponse<ReceptionQualityCarryOrderModel>>

    @GET("/v2/unit")
    fun getUnits(
            @Header("Authorization")                        authentication: String,
            @Query("embed")                                 embed: String = "unit_business_unit",
            @Query("unit_business_unit.business_unit_id")   businessUnitId: Long
    ): Single<ObjectResponse<UnitModel>>

    @GET("/v2/crop")
    fun getCrops(
            @Header("Authorization")                        authentication: String
    ): Single<ObjectResponse<CropModel>>

    @GET("/v2/crop")
    fun getClients(
            @Header("Authorization")                        authentication: String
    ): Single<ObjectResponse<ClientModel>>

    @GET("/v2/issue")
    fun getReceptionQualityIssues(
            @Header("Authorization")        authentication: String,
            @Query("embed")                 embed: String = "issue_crop",
            @Query("issue_crop.crop_id")    cropId: Long,
            @Query("business_unit_id")      businessUnitId: Long
    ): Observable<ObjectResponse<IssueModel>>

    @POST("/v2/quality_carry_order")
    @Headers("Content-Type: application/json")
    fun createReceptionQualityRevision(
            @Header("Authorization")        authentication: String,
            @Body body: ReceptionQualityRevisionModel
    ): Single<ReceptionQualityRevisionModel>

    @GET("/v2/quality_carry_order")
    fun getReceptionQualityRevisions(
            @Header("Authorization")        authentication: String,
            @Query("carry_order_id")        carryOrderId: Long
    ): Observable<ObjectResponse<ReceptionQualityRevisionModel>>
}