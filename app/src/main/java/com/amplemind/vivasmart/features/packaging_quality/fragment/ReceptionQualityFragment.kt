package com.amplemind.vivasmart.features.packaging_quality.fragment


import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.model.UnitModel
import com.amplemind.vivasmart.features.packaging_quality.dialog.SwipeablePictureDialog
import com.amplemind.vivasmart.features.packaging_quality.adapters.ReceptionQualityCriteriaAdapter
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ReceptionQualityViewModel
import com.amplemind.vivasmart.features.production_roster.AssignUnitDialogFragment
import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityRevisionModel
import com.amplemind.vivasmart.vo_features.custom.listeners.SwipeDismissTouchListener
import com.jakewharton.rxbinding3.view.clicks
import kotlinx.android.synthetic.main.fragment_reception_quality.*
import retrofit2.HttpException
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject


class ReceptionQualityFragment : BaseFragment() {

    companion object {

        private val TAG = ReceptionQualityFragment::class.java.simpleName

        private val PARAM_CARRY_ORDER_ID = "$TAG.CarryOrderId"
        private val PARAM_SAMPLE_SIZE = "$TAG.SampleSize"
        private val PARAM_UNIT_ID = "$TAG.UnitId"

        private val TAG_CRITERIA_IMAGE_DIALOG = "$TAG.CriteriaImageDialog"

        fun newInstance(carryOrderId: Long, sampleSize: Int, unit: UnitModel) =
                ReceptionQualityFragment().apply {
                    arguments = Bundle().apply {
                        putLong(PARAM_CARRY_ORDER_ID, carryOrderId)
                        putInt(PARAM_SAMPLE_SIZE, sampleSize)
                        putLong(PARAM_UNIT_ID, unit.id)
                    }
                }

    }

    @Inject
    lateinit var mViewModel: ReceptionQualityViewModel

    private lateinit var mAdapter: ReceptionQualityCriteriaAdapter

    init {
        argumentsNeeded = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater?.inflate(R.menu.menu_reception_quality_fragment, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reception_quality, container, false)
    }

    override fun onResume() {
        super.onResume()
        sendSetToolbarTitleEvent(getString(R.string.rqf_subtitle))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mViewModel.cleanUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.send -> showSendConfirmationDialog()
        }
        return true
    }

    override fun readArguments(args: Bundle) {
        super.readArguments(args)

        args.apply {
            mViewModel.carryOrderId = getLong(PARAM_CARRY_ORDER_ID, -1)
            mViewModel.unitId = getLong(PARAM_UNIT_ID, -1)
            mViewModel.sampleSize = getInt(PARAM_SAMPLE_SIZE, 0)
        }

        require(mViewModel.carryOrderId > -1L) {
            "$TAG: $PARAM_CARRY_ORDER_ID is required!"
        }

        require(mViewModel.unitId > -1L) {
            "$TAG: $PARAM_UNIT_ID is required!"
        }

        require (mViewModel.sampleSize > 0) {
            "$TAG: $PARAM_SAMPLE_SIZE is required!"
        }

    }

    override fun setUpCallbacks() {
        super.setUpCallbacks()
        mEventBus.observe(this::onCriteriaCountUpdatedEvent).addTo(subscriptions)
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()
        mEventBus.observe(this::onIssueClickedEvent).addTo(subscriptions)
        mEventBus.observe(this::onIssueImageSwiped).addTo(subscriptions)
        btn_edit_export.clicks()
                .subscribe {
                    onExportEditClicked()
                }.addTo(subscriptions)
    }

    override fun setUpUI() {
        super.setUpUI()
        setUpRecyclerView()
        setUpUIInfo()
    }

    override fun loadData() {
        mViewModel.loadReceptionQualityData().subscribe(this::onDataLoaded).addTo(subscriptions)
    }

    private fun setUpRecyclerView() {
        mAdapter = ReceptionQualityCriteriaAdapter(mEventBus, mViewModel.sampleSize)

        rv_criteria.setHasFixedSize(true)
        rv_criteria.layoutManager = LinearLayoutManager(context!!)
        rv_criteria.adapter = mAdapter
    }

    private fun setUpUIInfo() {
        tv_total.text = mViewModel.sampleSize.toString()

        tv_export_total.text = mViewModel.totalExport.toString()
        tv_export_per.text = formatPercentage(
                (mViewModel.totalExport / mViewModel.sampleSize.toFloat() * 100).toInt())

        tv_damaged_total.text = mViewModel.totalDamaged.toString()
        tv_damaged_per.text = formatPercentage(
                (mViewModel.totalDamaged / mViewModel.sampleSize.toFloat() * 100).toInt())
    }

    private fun showSendConfirmationDialog() {
        AlertDialog.Builder(context!!)
                .setMessage(R.string.rqf_send_confirmation_msg)
                .setPositiveButton(R.string.accept) { _, _ ->
                    sendRevision()
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun sendRevision() {
        showProgressLayout(true)
        mViewModel.createReceptionQualityRevision(
                mAdapter.getIssues(),
                mViewModel.totalExport,
                mAdapter.totalPermanentlyDamaged,
                mAdapter.totalConditionallyDamaged
        )
                .subscribe(this::onRevisionCreated, this::onRevisionCreationError)
                .addTo(subscriptions)
    }

    private fun showProgressLayout(show: Boolean) {
        layout_progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun formatPercentage(value: Int): String =
            context!!.getString(R.string.percentage).format(value)

    private fun onDataLoaded(data: ReceptionQualityViewModel.ReceptionQualityData) {
        mAdapter.setItems(data.issues)
    }

    private fun onRevisionCreated(revision: ReceptionQualityRevisionModel) {
        if (revision.id > 0) {
            showProgressLayout(false)
            mEventBus.send(OnRevisionFinishedEvent(this, revision))
        }
        else {
            onRevisionCreationError(null)
        }
    }

    private fun onExportEditClicked() {
        val dialog = AssignUnitDialogFragment.newInstance(
                getString(R.string.rqf_export), mViewModel.sampleSize, mViewModel.totalExport)
        dialog.setChangeListenner(object: AssignUnitDialogFragment.OnChangeUnit {
            override fun changeUnit(newValue: Int, previousValue: Int) {
                mViewModel.totalExport = newValue
                setUpUIInfo()
            }

            override fun cancelEdit() {
                dialog.dismiss()
            }
        })
        dialog.show(fragmentManager!!, "ExportDialog")


    }

    private fun onRevisionCreationError(error: Throwable?) {
        showProgressLayout(false)
        val msg = when (error) {
            is IOException -> getString(R.string.network_error)
            is HttpException -> getString(R.string.server_error).format(error.response())
            is TimeoutException -> getString(R.string.timeout_error)
            else -> getString(R.string.default_error)
        }

        AlertDialog.Builder(context!!)
                .setTitle(R.string.title_network_error)
                .setMessage(msg)
                .setPositiveButton(R.string.accept, null)
                .show()
    }

    private fun onCriteriaCountUpdatedEvent(event: ReceptionQualityCriteriaAdapter.OnCriteriaCountUpdatedEvent) {
        if (event.adapter == mAdapter) {
            setUpUIInfo()
        }
    }

    private fun onIssueClickedEvent(event: ReceptionQualityCriteriaAdapter.OnIssueClickedEvent) {
        if (event.adapter == mAdapter) {

            val options = Bundle().apply {
                putInt("position", event.position)
            }

            SwipeablePictureDialog.newInstance(Uri.parse("https://picsum.photos/id/136/200/300"), event.issue.name, options)
                    .show(fragmentManager!!, TAG_CRITERIA_IMAGE_DIALOG)
        }
    }

    private fun onIssueImageSwiped(event: SwipeablePictureDialog.OnSwipedEvent) {
        if (event.dialog.tag == TAG_CRITERIA_IMAGE_DIALOG) {
            if (event.direction == SwipeDismissTouchListener.Direction.RIGHT) {
                event.options?.apply {
                    mAdapter.addCount(getInt("position"), 1)
                }
            }
        }
    }

    data class OnRevisionFinishedEvent(
            val adapter: ReceptionQualityFragment,
            val revision: ReceptionQualityRevisionModel
    )
}
