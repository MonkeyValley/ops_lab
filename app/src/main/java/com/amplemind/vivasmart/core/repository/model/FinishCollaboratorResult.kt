package com.amplemind.vivasmart.core.repository.model

data class FinishCollaboratorResult(
        val registerId: Int,
        val time : Long,
        val relative_url: String,
        val position: Int,
        val training: Boolean,
        val isBox : Boolean,
        val isPractice: Boolean,
        val units : Double? = null
)
