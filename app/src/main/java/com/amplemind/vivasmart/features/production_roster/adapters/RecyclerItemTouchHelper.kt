package com.amplemind.vivasmart.features.production_roster.adapters

import android.graphics.Canvas
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper.Callback.getDefaultUIUtil
import androidx.recyclerview.widget.ItemTouchHelper
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserGeneralViewModel


class RecyclerItemTouchHelper(dragDirs: Int, swipeDirs: Int, private val listener: RecyclerItemTouchHelperListener) : ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs) {

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return true
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (viewHolder != null) {
            var foregroundView: ConstraintLayout? = null
            if(viewHolder is TimerUserViewHolder) {
                foregroundView = viewHolder.viewForeground
            } else if (viewHolder is TimerUserGeneralViewModel) {
                foregroundView = viewHolder.viewForeground
            }
            getDefaultUIUtil().onSelected(foregroundView)
        }
    }

    override fun onChildDrawOver(c: Canvas, recyclerView: RecyclerView,
                                 viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float,
                                 actionState: Int, isCurrentlyActive: Boolean) {
        var foregroundView: ConstraintLayout? = null
        if(viewHolder is TimerUserViewHolder) {
            foregroundView = viewHolder.viewForeground
        } else if (viewHolder is TimerUserGeneralViewModel) {
            foregroundView = viewHolder.viewForeground
        }
        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive)
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        var foregroundView: ConstraintLayout? = null
        if(viewHolder is TimerUserViewHolder) {
            foregroundView = viewHolder.viewForeground
        } else if (viewHolder is TimerUserGeneralViewModel) {
            foregroundView = viewHolder.viewForeground
        }
        if (foregroundView != null){
            getDefaultUIUtil().clearView(foregroundView)
        }
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView,
                             viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float,
                             actionState: Int, isCurrentlyActive: Boolean) {
        var foregroundView: ConstraintLayout? = null
        var deleteView: RelativeLayout? = null
        var finishView: RelativeLayout? = null
        if(viewHolder is TimerUserViewHolder) {
            foregroundView = viewHolder.viewForeground
            deleteView = viewHolder.viewBackground
            finishView = viewHolder.viewFinish
        } else if (viewHolder is TimerUserGeneralViewModel) {
            foregroundView = viewHolder.viewForeground
            deleteView = viewHolder.viewBackground
            finishView = viewHolder.viewFinish
        }
        if (dX > 0) {
            deleteView?.visibility = View.GONE
            finishView?.visibility = View.VISIBLE
        } else {
            deleteView?.visibility = View.VISIBLE
            finishView?.visibility = View.GONE
        }
        if (foregroundView != null){
            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                    actionState, isCurrentlyActive)
        }
    }


    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        listener.onSwiped(viewHolder, direction, viewHolder.adapterPosition)
    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    interface RecyclerItemTouchHelperListener {
        fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int)
    }
}