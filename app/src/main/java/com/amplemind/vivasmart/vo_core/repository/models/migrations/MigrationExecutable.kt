package com.amplemind.vivasmart.vo_core.repository.models.migrations

import com.amplemind.vivasmart.vo_core.repository.models.migrations.executables.*
import io.realm.DynamicRealm

interface MigrationExecutable {

    companion object {

        val EXECS = listOf(
                Version_1(),
                Version_2(),
                Version_3(),
                Version_4(),
                Version_5(),
                Version_6(),
                Version_7(),
                Version_8(),
                Version_9(),
                Version_10(),
                Version_11(),
                Version_12(),
                Version_13(),
                Version_14(),
                Version_15(),
                Version_16(),
                Version_2_7_0(),
                Version_2_8_0(),
                Version_2_9_0(),
                Version_2_9_1(),
                Version_2_10_0(),
                Version_2_10_5(),
                Version_3_1_0(),
                Version_3_3_0(),
                Version_3_3_1(),
                Version_3_6_0()
        )

        val CURRENT_VERSION = EXECS.size.toLong()

        fun getExecutables(oldVersion: Long, newVersion: Long): List<MigrationExecutable> {

            val firstIndex = oldVersion.toInt()
            val lastIndex  = newVersion.toInt()

            return if (firstIndex < lastIndex)
                EXECS.subList(firstIndex, lastIndex)
            else
                EXECS.subList(lastIndex, firstIndex).reversed()

        }

    }

    fun run(realm: DynamicRealm)
    fun drop(realm: DynamicRealm)

}