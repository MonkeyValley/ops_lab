package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class WeightRestriction(
        @SerializedName("id")                    var id: Long? = 0L,
        @SerializedName("business_unit_id")      var businessUnitId: Long? = 0L,
        @SerializedName("crop_id")               var cropId: Long? = 0L,
        @SerializedName("max_weight")            var maxWeight: Double? = 0.0,
        @SerializedName("min_weight")            var minWeight: Double? = 0.0
) : RealmObject()