package com.amplemind.vivasmart.vo_core.repository.requests

import com.google.gson.annotations.SerializedName

data class CreateActivityCodeRequest(
        @SerializedName("uuid") val uuid: String,
        @SerializedName("code") val code: Int,
        @SerializedName("activity_id") val activityId: Int,
        @SerializedName("stage_id") val stageId: Int)
