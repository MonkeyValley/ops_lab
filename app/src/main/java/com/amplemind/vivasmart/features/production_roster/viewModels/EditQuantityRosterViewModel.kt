package com.amplemind.vivasmart.features.production_roster.viewModels

import javax.inject.Inject

class EditQuantityRosterViewModel @Inject constructor(){

    private lateinit var items: MutableList<ItemEditQuantityRosterViewModel>

    fun getItems(): MutableList<ItemEditQuantityRosterViewModel> {
        items = mutableListOf()
        items.add(ItemEditQuantityRosterViewModel("Pepino", "100", "00:20", "$10"))
        items.add(ItemEditQuantityRosterViewModel("Tomate", "100", "00:20", "$10"))
        items.add(ItemEditQuantityRosterViewModel("Sweet pepper", "100", "00:50", "$10"))
        return items
    }

    fun getQuantityAtIndex(index: Int): String {
        return items[index].quantity
    }

    fun getTimeAtIndex(index: Int): String {
        return items[index].time
    }
}