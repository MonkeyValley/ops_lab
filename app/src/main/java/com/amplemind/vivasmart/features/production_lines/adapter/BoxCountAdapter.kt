package com.amplemind.vivasmart.features.production_lines.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.onChange
import com.amplemind.vivasmart.databinding.ItemCountBoxLinesBinding
import com.amplemind.vivasmart.features.count_box.ItemBoxCountViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.item_count_box_lines.view.*

class BoxCountAdapter : RecyclerView.Adapter<BoxCountAdapter.BoxCountViewHolder>() {

    private var list = mutableListOf<ItemBoxCountViewModel>()

    private val onClick = BehaviorSubject.create<ItemBoxCountViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoxCountViewHolder {
        val binding = ItemCountBoxLinesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BoxCountViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: BoxCountViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(data: MutableList<ItemBoxCountViewModel>) {
        this.list = data
        notifyDataSetChanged()
    }

    inner class BoxCountViewHolder(private val binding: ItemCountBoxLinesBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemBoxCountViewModel) {
            binding.setVariable(BR.viewItem, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.root.setOnClickListener {
                    onClick.onNext(item)
                }
            }
            binding.root.current_count.onChange { count ->
                binding.root.til_current_count.isErrorEnabled = false
                if (count.isNotEmpty() && count.toInt() >= item.last_count) {
                    item.current_count = count.toInt()
                    item.isError = false
                } else {
                    item.isError = true
                    binding.root.til_current_count.isErrorEnabled = true
                    binding.root.til_current_count.error = binding.root.context.getString(R.string.current_count_error)
                }
            }
        }
    }

    fun onClickItem(): BehaviorSubject<ItemBoxCountViewModel> {
        return onClick
    }

}
