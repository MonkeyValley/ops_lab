package com.amplemind.vivasmart.features.packaging_quality.clean_report

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import javax.inject.Inject

class SensorCardReload @Inject constructor(val activity : Context) : SensorEventListener {

    private var mShakeTime: Long = 0

    private var listener : SensorListener? = null

    /**
     * Constants for sensors
     */
    private val SHAKE_THRESHOLD = 2.1f
    private val SHAKE_WAIT_TIME_MS = 500

    private var mSensorManager : SensorManager? = null
    private var mSensorAcc : Sensor? = null

    fun initSensor(){
        mSensorManager = activity.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        if (mSensorManager != null){
            mSensorAcc = mSensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        }
    }

    fun registerSensor(){
        mSensorManager!!.registerListener(this,mSensorAcc!!,SensorManager.SENSOR_DELAY_NORMAL)
    }

    fun unRegisterSensor(){
        mSensorManager!!.unregisterListener(this)
    }

    override fun onSensorChanged(sensor: SensorEvent?) {
        if (sensor!!.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            if (sensor.sensor.type == Sensor.TYPE_ACCELEROMETER) {
                Log.e("UnRELIABLE", "No se generan datos de accelerometro")
            }
            return
        }

        if (sensor.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            detectShake(sensor)
        }
    }

    /**
     *  previuos card shake device
     */
    private fun detectShake(sensor: SensorEvent) {
        val now = System.currentTimeMillis()

        if (now - mShakeTime > SHAKE_WAIT_TIME_MS) {
            mShakeTime = now

            val gX = sensor.values[0] / SensorManager.GRAVITY_EARTH
            val gY = sensor.values[1] / SensorManager.GRAVITY_EARTH
            val gZ = sensor.values[2] / SensorManager.GRAVITY_EARTH

            // gForce will be close to 1 when there is no movement
            val gForce = Math.sqrt((gX * gX + gY * gY + gZ * gZ).toDouble())

            // Change background color if gForce exceeds threshold;
            // otherwise, reset the color
            if (gForce > SHAKE_THRESHOLD) {
                listener!!.shakeDetect()
            }
        }
    }

    fun setListener(listener : SensorListener){
        this.listener = listener
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        //no necesary
    }

    interface SensorListener{
        fun shakeDetect()
    }

}