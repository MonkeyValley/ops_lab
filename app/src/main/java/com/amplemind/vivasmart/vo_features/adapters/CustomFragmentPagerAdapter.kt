package com.amplemind.vivasmart.vo_features.adapters

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import android.util.Log
import android.util.SparseArray
import android.util.SparseBooleanArray
import android.view.View
import android.view.ViewGroup
import io.reactivex.subjects.PublishSubject

abstract class CustomFragmentPagerAdapter(private val mFragmentManager: FragmentManager) : PagerAdapter() {

    companion object {
        private val TAG = CustomFragmentPagerAdapter::class.java.simpleName
    }

    private var mTransaction: FragmentTransaction? = null
    private val mPositionTagTracking = SparseArray<String>()
    private val mDeleteList = SparseBooleanArray()

    private var mViewPager: ViewPager? = null

    val onFragmentCreated = PublishSubject.create<Int>()

    abstract fun getFragmentTag(position: Int): String
    abstract fun buildFragment(position: Int): Fragment

    @SuppressLint("CommitTransaction")
    private fun beginTransaction() {
        if (mTransaction == null) {
            mTransaction = mFragmentManager.beginTransaction()
        }
    }

    fun getFragment(position: Int): Fragment? = mFragmentManager.findFragmentByTag(mPositionTagTracking[position])

    protected fun deleteFragment(position: Int) {
        mDeleteList.put(position, true)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        Log.e(TAG, "INSTANTIATE!")

        beginTransaction()

        val tag = getFragmentTag(position)
        var fragment = mFragmentManager.findFragmentByTag(tag)

        if (fragment == null) {
            fragment = buildFragment(position)
            mTransaction?.add(container.id, fragment, tag)
            mPositionTagTracking.put(position, tag)
        }
        else {
            mTransaction?.attach(fragment)
        }

        return fragment
    }

    override fun destroyItem(container: ViewGroup, position: Int, fragment: Any) {
        beginTransaction()

        Log.e(TAG, "destroyItem")

        if (mDeleteList[position]) {
            mTransaction?.remove(fragment as Fragment)
            mDeleteList.delete(position)
        }
        else {
            mTransaction?.detach(fragment as Fragment)
        }
    }

    override fun finishUpdate(container: ViewGroup) {
        mTransaction?.commitNowAllowingStateLoss()
        mTransaction = null
    }

    override fun isViewFromObject(view: View, fragment: Any): Boolean =
            (fragment as Fragment).view == view

}
