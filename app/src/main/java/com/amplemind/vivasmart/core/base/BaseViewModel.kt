package com.amplemind.vivasmart.core.base

import io.reactivex.subjects.BehaviorSubject

open class BaseViewModel {

    protected val requestFail = BehaviorSubject.create<String>()
    protected val progressStatus = BehaviorSubject.create<Boolean>()
    protected val searchLocalData = BehaviorSubject.create<Boolean>()


    fun showProgress(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun showError(): BehaviorSubject<String> {
        return requestFail
    }

    fun onLoadLocalData(): BehaviorSubject<Boolean> {
        return searchLocalData
    }

}