package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class IssuesModel(
        @SerializedName("issue_id") var issue_id: Int,
        @SerializedName("frequency_no") var frequency_no: Int,
        @SerializedName("issue") var issue: IssueModel
)
