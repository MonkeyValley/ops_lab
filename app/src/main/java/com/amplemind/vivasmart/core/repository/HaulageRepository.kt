package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.CollaboratorsApi
import com.amplemind.vivasmart.core.repository.remote.HaulageApi
import com.amplemind.vivasmart.core.repository.request.CarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.HaulageSignRequest
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.amplemind.vivasmart.core.repository.response.VehiclesResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.vo_core.repository.dao.realm.StagesDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.google.gson.Gson
import io.reactivex.Maybe
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class HaulageRepository @Inject constructor(private val collaboratorsApi: CollaboratorsApi,
                                            private val api: HaulageApi,
                                            private val stagesDao: StagesDao,
                                            private val preferences: UserAppPreferences) {

    @Inject
    lateinit var authorization: ApiAuthorization

    fun getStage(stageId: Int): Maybe<StageModel> =
            stagesDao.findByStageId(stageId)

    fun getOperators(name: String, stageId: Int) : Observable<CollaboratorsRepository.CollaboratorsResponse> {
        return collaboratorsApi.getOperators(authorization.getAuthToken(preferences.token), stageId, name)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun assignSigns(signCollaborator: String, signSuper: String, carryOrderId: Int) : Observable<Any> {
        return api.haulageAssignSigns(authorization.getAuthToken(preferences.token), carryOrderId, HaulageSignRequest(signCollaborator, signSuper))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun createCarryOrder(lotId: Int, cropId: Int, table: List<Int>, boxCounts: List<VarietyBoxCount>, secondQualityCount: Double, boxTotal: Double, temperature: Double, collaboratorId: Int?, vehicleId: Int, comments : String) : Observable<CarryOrderModel> {
        val user = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        val request = CarryOrderRequest(user.businessUnitId?.toInt() ?: 1, lotId, cropId, table, boxCounts, secondQualityCount, boxTotal, 0.0, temperature, collaboratorId, vehicleId, user.id, comments)
        return api.createCarryOrder(authorization.getAuthToken(preferences.token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getVehicles() : Observable<VehiclesResponse> {
        return api.getVehicles(authorization.getAuthToken(preferences.token))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

}
