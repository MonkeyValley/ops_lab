package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.QuantityOrderRepository
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderDetailResponse
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderDetailResult
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject

class QualityOrderViewModel @Inject constructor(private val repository: QuantityOrderRepository,
                                                private val profileRepo: ProfileRepository,
                                                private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private var issuesList = mutableListOf<IssuesItemViewModel>()

    private val issuesSubject = BehaviorSubject.create<List<IssuesItemViewModel>>()
    private val signSend = BehaviorSubject.create<String>()

    private val sendReport = BehaviorSubject.create<Boolean>()


    fun getCarryOrderDetail(id_carry: Int): Disposable {
        return repository.getQualityCarryOrderDetail(id_carry)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .doOnError { progressStatus.onNext(false) }
                .map(this::mapModelToViewModel)
                .subscribe({ issues ->
                    issuesList = issues.toMutableList()
                    issuesSubject.onNext(issues)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModel(list: QualityCarryOrderDetailResponse): List<IssuesItemViewModel> {
        val viewModels = mutableListOf<IssuesItemViewModel>()

        val item = list.data.first()

        list.data.forEach {
            it.issues?.forEach { issue ->
                viewModels.add(IssuesItemViewModel(issue = issue, type = QualityOrderAdapter.QualityTypeHolder.ISSUES.type, id_carry = item.id))
            }
        }
        viewModels.add(IssuesItemViewModel(null, photos = listPhotos(list.data.first()), type = QualityOrderAdapter.QualityTypeHolder.PHOTOS.type, id_carry = item.id))
        viewModels.add(IssuesItemViewModel(null, type = QualityOrderAdapter.QualityTypeHolder.EVUALUATION.type, id_carry = item.id, evaluation = item.evaluation ?: false, comment = item.comment ?: ""))
        return viewModels
    }

    private fun listPhotos(data: QualityCarryOrderDetailResult): List<String> {

        val basePhoto = "${BuildConfig.SERVER_URL}uploads/"

        val photos = mutableListOf<String>()
        if (data.image1 != null) photos.add("$basePhoto${data.image1}")
        if (data.image2 != null) photos.add("$basePhoto${data.image2}")
        if (data.image3 != null) photos.add("$basePhoto${data.image3}")
        if (data.image4 != null) photos.add("$basePhoto${data.image4}")
        return photos
    }

    fun getIssuesList(): BehaviorSubject<List<IssuesItemViewModel>> {
        return issuesSubject
    }

    fun signSendSuccess(): BehaviorSubject<String> {
        return signSend
    }

    fun sendReportSuccess(): BehaviorSubject<Boolean> {
        return sendReport
    }

    fun sendReportCarrySign(sign: File): Disposable {
        return profileRepo.uploadImage(sign, "sign")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnError {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                }
                .subscribe({
                    signSend.onNext(it.relative_url)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun sendReportDataCarry(sign: String, id_carry: Int): Disposable {
        val item = issuesList.first { it.typeHolder == QualityOrderAdapter.QualityTypeHolder.EVUALUATION.type }
        return repository.sendCarryOrderEvaluation(sign, item.evaluationResult, item.commentsIssue, item.id_carry)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .doOnError { progressStatus.onNext(false) }
                .subscribe({
                    sendReport.onNext(true)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getCarryReadDetail(idCarry: Int): Disposable {
        return repository.getCarryForReadEvaluation(idCarry)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .doOnError { progressStatus.onNext(false) }
                .map(this::mapModelToViewModel)
                .subscribe({ issues ->
                    issuesList = issues.toMutableList()
                    issuesSubject.onNext(issues)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

}
