package com.amplemind.vivasmart.vo_core.repository.dao.realm

import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogCountModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.utils.getUUID
import io.reactivex.Observable
import io.realm.Realm
import io.realm.Sort
import javax.inject.Inject

class ActivitiesDao @Inject constructor(realmHolder: RealmHolder) : RealmDao<ActivityModel>(realmHolder, ActivityModel::class.java) {

    fun findAll(cropId: Long, stageType: Int): Observable<Changes<ActivityModel>> =
            findAllLiveAsync { realm ->
                query(realm)
                        .beginGroup()
                        .equalTo("crops.cropId", cropId)
                        .and()
                        .equalTo("isActive", true)
                        .and()
                        .equalTo("activitySubCategoryId", stageType)
                        .endGroup()
                        .sort("name", Sort.ASCENDING)

            }

    fun addLogCountAux(realm: Realm, activity: ActivityModel, stage: StageModel, add: Int) {
        var activityLogCount = activity.activityLogCounts?.find { activityLogCount ->
            activityLogCount.stageUuid == stage.uuid
        }

        if (activityLogCount == null) {
            activityLogCount = realm.createObject(ActivityLogCountModel::class.java, getUUID())
            activityLogCount?.activityUuid = activity.uuid
            activityLogCount?.stageUuid = stage.uuid
            activity.activityLogCounts?.add(activityLogCount)
            realm.copyToRealmOrUpdate(activity)
        }

        activityLogCount?.apply {
            this.count += add
            realm.copyToRealmOrUpdate(this)
        }
    }
}