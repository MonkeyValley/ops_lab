package com.amplemind.vivasmart.features.interfaces

import com.amplemind.vivasmart.features.production_quality.ConfirmationReportsItemViewModel
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject

interface IConfirmationReportsViewModel {

    fun loadStaticReports(): Disposable
    fun isHeaderView(position: Int): Boolean
    fun getSignatureTitle(): String
    fun getSignatureMessage(): String
    fun getData() : BehaviorSubject<MutableList<ConfirmationReportsItemViewModel>>

}