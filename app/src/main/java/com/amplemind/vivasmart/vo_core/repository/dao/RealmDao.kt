package com.amplemind.vivasmart.vo_core.repository.dao

import android.util.Log
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.crashlytics.android.Crashlytics
import io.realm.*
import io.realm.rx.CollectionChange

abstract class RealmDao<Model : RealmObject>(
       val mModelClazz: Class<Model>
) : IDao<Model> {

    companion object {

        val TAG = RealmDao::class.java.simpleName

        fun deleteDatabase() {
            val realm = Realm.getDefaultInstance()
            realm.beginTransaction()
            realm.deleteAll()
            realm.commitTransaction()
            realm.close()
        }
    }

    val mRealm = Realm.getDefaultInstance()

    abstract fun retrieveServerData(response: Any?): List<Model>?

    fun findAll(): List<Model>? {
        val realm = Realm.getDefaultInstance()
        val results = realm
                ?.where(mModelClazz)
                ?.findAll()
        val ret = copy(results)
        realm.close()
        return ret
    }

    override fun findByUuid(uuid: String): Model? {
        var result: Model? = null
        Realm.getDefaultInstance().use {realm ->
            result = realm.where(mModelClazz)
                    .equalTo("uuid", uuid)
                    .findFirst()
        }
        return result
    }

    override fun findById(id: Long): Model? {
        val realm = Realm.getDefaultInstance()

        val results = realm
                ?.where(mModelClazz)
                ?.equalTo("id", id)
                ?.findFirst()

        val ret = if (results != null) copy(results) else null

        realm.close()

        return ret
    }

    fun copy(results: List<Model>?): List<Model> {
        val realm = Realm.getDefaultInstance()
        val obj = if (results?.all{ it.isManaged } == true)
            realm.copyFromRealm(results) ?: listOf()
        else results ?: listOf()
        realm.close()
        return obj
    }

    fun copy(results: Model?): Model? {
        val realm = Realm.getDefaultInstance()
        val obj = if (results?.isManaged == true)
            realm.copyFromRealm(results)
        else results
        realm.close()
        return obj
    }

    override fun isEmpty(): Boolean {
        val realm = Realm.getDefaultInstance()
        val count = realm
                .where(mModelClazz)
                .count()
        val res = count == 0L
        realm.close()
        return res
    }

    override fun saveServerResponse(response: Any?): Boolean {
        val realm = Realm.getDefaultInstance()
        val data = retrieveServerData(response)

        var result = true
        if (data != null) {
            try {
                realm?.beginTransaction()
                realm?.insertOrUpdate(data)
                realm?.commitTransaction()
            }
            catch (e: Throwable) {
                result = false
                Log.e(TAG, e.stackTrace.toString())
                Crashlytics.logException(e)
            }
        }
        realm.close()
        return result
    }

    override fun upsert(obj: Model): Model? {
        val realm = Realm.getDefaultInstance()

        try {
            realm.beginTransaction()
            realm.insertOrUpdate(obj)
            realm.commitTransaction()
        }
        catch (e: Throwable) {
            Crashlytics.logException(e)
            throw e
        }

        realm.close()

        return obj
    }

    override fun upsert(obj: List<Model>): List<Model>? {

        val realm = Realm.getDefaultInstance()

        try {
            realm.beginTransaction()
            realm.insertOrUpdate(obj)
            realm.commitTransaction()
        }
        catch (e: Throwable) {
            Crashlytics.logException(e)
            throw e
        }

        realm.close()

        return obj

    }

    override fun delete(uuid: String): Model? {

        val realm = Realm.getDefaultInstance()

        val result = realm.where(mModelClazz)
                .equalTo("uuid", uuid)
                .findFirst()

        var obj: Model? = null
        if (result != null) {

            obj = copy(result)

            try {
                realm.beginTransaction()
                result.deleteFromRealm()
                realm.commitTransaction()
            } catch (e: Throwable) {
                Crashlytics.logException(e)
                throw e
            }
        }

        realm.close()

        return obj
    }

    override fun <U> buildCompatibleList(): RealmList<U> {
        return RealmList()
    }

    fun liveChangeSetConverter(changes: CollectionChange<RealmResults<Model>>, copyData: Boolean = false): Changes<Model> {

        val insertions = mutableListOf<Changes.Range>()
        val updates = mutableListOf<Changes.Range>()
        val deletions = mutableListOf<Changes.Range>()

        changes.changeset?.insertionRanges?.forEach {range ->
            insertions.add(Changes.Range(range.startIndex, range.length))
        }

        changes.changeset?.changeRanges?.forEach {range ->
            updates.add(Changes.Range(range.startIndex, range.length))
        }

        changes.changeset?.deletionRanges?.forEach {range ->
            deletions.add(Changes.Range(range.startIndex, range.length))
        }

        val state = when (changes.changeset?.state) {
            OrderedCollectionChangeSet.State.INITIAL -> Changes.State.INITIAL
            else -> Changes.State.UPDATE
        }

        val data = if (copyData) copy(changes.collection) else changes.collection

        return Changes( insertions, updates, deletions, data, state )
    }

    override fun getLastVersion(versionField: String): Long {
        val realm = Realm.getDefaultInstance()

        val max = realm
                ?.where(mModelClazz)
                ?.max(versionField)
        val res = max?.toLong() ?: 0
        realm.close()

        return res
    }

}