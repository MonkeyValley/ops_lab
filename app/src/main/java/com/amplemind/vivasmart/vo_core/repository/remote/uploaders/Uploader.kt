package com.amplemind.vivasmart.vo_core.repository.remote.uploaders

import android.net.Uri
import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.dao.UploadObjectDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.annotations.Uploadable
import com.amplemind.vivasmart.vo_core.repository.remote.utils.API_VERSION
import com.google.gson.Gson
import kotlin.reflect.full.findAnnotation

abstract class Uploader (
        private val mPref: UserAppPreferences,
        private val mDao: UploadObjectDao
) {

    companion object {
        const val POST      = "POST"
        const val PUT       = "PUT"
        const val DELETE    = "DELETE"
    }

    protected fun <T: Any> buildRequest(body: T, action: String) =
            buildRequest(listOf(body), action).first()

    protected fun <T: Any> buildRequest(bodies: List<T>, action: String): List<UploadObjectModel> {

        val objs = bodies.map {body ->
            val uploadable = getUploadable(body)

            val strBody = Gson().toJson(body)
            Log.e("BodyUpload: ", strBody)

            val url = Uri.parse(mPref.selectedServerUrl)
                    .buildUpon()
                    .appendPath(API_VERSION)
                    .appendEncodedPath(uploadable.path)
                    .build()
                    .toString()

            return@map UploadObjectModel(
                    url = url,
                    token = mPref.token,
                    contentType = uploadable.contentType,
                    verb = action,
                    body = strBody
            )
        }

        return objs
    }

    protected fun <T: Deletable> buildDeleteRequest(body: T): UploadObjectModel {
        val uploadable = getUploadable(body)

        val path = if (uploadable.deletePath.isEmpty()) uploadable.path else uploadable.deletePath

        val url = Uri.parse(mPref.selectedServerUrl)
                .buildUpon()
                .appendPath(API_VERSION)
                .appendEncodedPath(path)
                .appendPath(body.identifier)
                .build()
                .toString()

        return UploadObjectModel(
                url = url,
                token = mPref.token,
                contentType = uploadable.contentType,
                verb = DELETE,
                body = ""
        )

    }

    private fun <T: Any> getUploadable(body: T): Uploadable =
            body::class.findAnnotation() ?: throw IllegalArgumentException("Class has no Uploadable annotation")


    interface Deletable {
        val identifier: String
    }

}