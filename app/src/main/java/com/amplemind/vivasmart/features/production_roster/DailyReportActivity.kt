package com.amplemind.vivasmart.features.production_roster

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.production_roster.fragments.DailyReportFragment
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.activity_close_day_report.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DailyReportActivity : BaseActivityWithFragment() {

    companion object {
        private val TAG = DailyReportActivity::class.java.simpleName

        val PARAM_STAGE_UUID = "$TAG.StageUuid"
    }

    @Inject
    lateinit var mEventBus: EventBus

    lateinit var fragment: DailyReportFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_close_day_report)
        setupToolbar()

        val stageUuid = intent.getStringExtra(PARAM_STAGE_UUID)

        mEventBus.observe(this::onSetLotName).addTo(subscriptions)

        fragment = DailyReportFragment.newInstance(stageUuid)

        val stageName = this.intent.getStringExtra("StageName")

        if(stageName != "Labores Culturales"){
            fab_menu_lines.visibility = View.GONE
        }else{
            fab_menu_lines.visibility = View.VISIBLE
        }

        addFragment(fragment)

        setUpUi()
    }

    fun setUpUi(){
        RxView.clicks(btn_daily_pay).throttleFirst(1, TimeUnit.SECONDS).subscribe {
            fragment.onlyDailyPay()
        }.addTo(subscriptions)

        RxView.clicks(btn_only_report).throttleFirst(1, TimeUnit.SECONDS).subscribe {
            fragment.onlyReport()
        }.addTo(subscriptions)

        RxView.clicks(btn_all_report).throttleFirst(1, TimeUnit.SECONDS).subscribe {
            fragment.allReport()
        }.addTo(subscriptions)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addFragment(fragment: Fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.closeday_fragment_container, fragment, "Extra")
                .commit()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_closeday
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Reporte diario"
    }

    private fun onSetLotName(event: DailyReportFragment.SetLotNameEvent) {
        supportActionBar?.subtitle = event.stage.lot?.name
    }

}
