package com.amplemind.vivasmart.core.repository


import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.FinishedProductReportModel

import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.FinishedProductApi
import com.amplemind.vivasmart.core.repository.response.*
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ReacibaQualityReviewViewModel
import com.google.gson.Gson
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class FinishedProductRepository @Inject constructor(private val api: FinishedProductApi,
                                                    private val authorization: ApiAuthorization,
                                                    private val preferences: UserAppPreferences) {

    data class ReportFinishedResponse(val token: String)

    val businessUnitId = Gson().fromJson(preferences.userInfo, UserModel::class.java).businessUnitId
            ?: ""

    fun getReportList(day: String): Observable<List<FinishedProductResult>> {
        return api.getReportList(authentication = authorization.getAuthToken(preferences.token), day = day, businessId = businessUnitId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getUnitCropList(crop_id: Int): Observable<CropUnitResponse> {
        Log.d("crop_id", crop_id.toString())
        return api.getUnitCropList(authentication = authorization.getAuthToken(preferences.token), crop_id = crop_id, __logic = "AND")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getClients(): Observable<ClientResponse> {
        return api.getClients(authentication = authorization.getAuthToken(preferences.token))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLots(crop_id: Int): Observable<LotsResponse> {
        return api.getLots(authentication = authorization.getAuthToken(preferences.token),
                embed = "lot", is_active = true, __logic = "AND", business_unit_id = businessUnitId, crop_id = crop_id )
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getUnits(): Observable<UnitResponse> {
        return api.getUnits(authentication = authorization.getAuthToken(preferences.token),
                business_unit_id = businessUnitId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getIssues(crops_id__contains: Int): Observable<IssuesResponse> {
        return api.getIssues(authentication = authorization.getAuthToken(preferences.token),
                category_id = 4, crops_id__contains = crops_id__contains, __logic = "AND")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun postFinishedProduct(ObjFinishedProdReport: FinishedProductReportModel): Observable<FinishedProductReportModel> {
        return api.postFinishedProduct(authorization.getAuthToken(preferences.token),ObjFinishedProdReport)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun postRecibaQualityReview(ObjFinishedProdReport: ReacibaQualityReviewViewModel): Observable<ReacibaQualityReviewViewModel> {
        return api.postRecibaQualityReview(authorization.getAuthToken(preferences.token),ObjFinishedProdReport)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getReportDetail(idReport: Int): Observable<FinishedProductReportModel> {
        return api.getReportDetail(authentication = authorization.getAuthToken(preferences.token),
                idReport = idReport, embed = "issues,issues.issue")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }
}
