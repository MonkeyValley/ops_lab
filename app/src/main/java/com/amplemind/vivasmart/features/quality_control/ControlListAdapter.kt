package com.amplemind.vivasmart.features.quality_control

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemPendingLogBinding
import com.amplemind.vivasmart.vo_core.repository.models.realm.PendingLogsModel
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.PendingLogsItemViewModel
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class ControlListAdapter constructor(
        private val mEventBus: EventBus,
        private val mReadOnly: Boolean
) : BaseAdapter<PendingLogsModel, PendingLogsItemViewModel, ControlListAdapter.ControlListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ControlListViewHolder {
        val binding = ItemPendingLogBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ControlListViewHolder(binding)
    }

    override fun buildViewModel(item: PendingLogsModel): PendingLogsItemViewModel
        = PendingLogsItemViewModel(item)

    override fun getItemKey(item: PendingLogsModel): String = item.collaborator.uuid

    private fun sendPendingLogsClickedEvent(pendingLogs: PendingLogsModel) {
        mEventBus.send(PendingLogsClickedEvent(pendingLogs, mReadOnly))
    }

    private fun sendPendingLogsLongClickedEvent(pendingLogs: PendingLogsModel) {
        mEventBus.send(PendingLogsLongClickedEvent(pendingLogs, mReadOnly))
    }

    inner class ControlListViewHolder(private val binding : ItemPendingLogBinding) : BaseAdapter.ViewHolder<PendingLogsItemViewModel>(binding.root) {

        private var mDisposable: Disposable? = null

        override fun cleanUp() {
            super.cleanUp()
            mDisposable?.dispose()
        }

        override fun bind(viewModel : PendingLogsItemViewModel){
            binding.setVariable(BR.viewItem, viewModel)
            binding.executePendingBindings()

            mDisposable = RxView.clicks(binding.root)
                    .throttleFirst(300, TimeUnit.MILLISECONDS)
                    .subscribe {
                        sendPendingLogsClickedEvent(viewModel.item)
                    }

            mDisposable = RxView.longClicks(binding.root)
                    .subscribe {
                        sendPendingLogsLongClickedEvent(viewModel.item)
                    }
        }

    }

    data class PendingLogsClickedEvent(
            val pendingLogs: PendingLogsModel,
            val readOnly: Boolean
    )

    data class PendingLogsLongClickedEvent(
            val pendingLogs: PendingLogsModel,
            val readOnly: Boolean
    )
}
