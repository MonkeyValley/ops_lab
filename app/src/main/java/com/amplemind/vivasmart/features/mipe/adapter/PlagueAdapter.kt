package com.amplemind.vivasmart.features.mipe.adapter

import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.databinding.ItemPlagueMipeBinding
import com.amplemind.vivasmart.features.gallery.VisualSupportDialog
import com.amplemind.vivasmart.features.mipe.MipeMonitoringActivity
import com.amplemind.vivasmart.features.mipe.viewModel.ItemPlagueViewModel
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyDetailDialog
import com.amplemind.vivasmart.vo_core.repository.models.realm.MipeMonitoringModel
import io.reactivex.subjects.PublishSubject
import io.realm.Realm

class PlagueAdapter : (RecyclerView.Adapter<PlagueAdapter.PlagueViewHolder>)() {

    var lotId = 0
    var groove = 0
    var week = 0
    var table = "0"
    var plant = 0
    var activity: MipeMonitoringActivity? = null

    fun setData(lotId: Int, groove: Int, week: Int, table: String, plant: Int){
        this.lotId = lotId
        this.groove = groove
        this.week = week
        this.table = table
        this.plant = plant
    }

    private var list = listOf<ItemPlagueViewModel>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    fun addElements(data: List<ItemPlagueViewModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlagueViewHolder {
        val binding = ItemPlagueMipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PlagueViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: PlagueViewHolder, position: Int) {
        holder.bind(list[position])

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class PlagueViewHolder(private val binding: ItemPlagueMipeBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ItemPlagueViewModel

        fun bind(item: ItemPlagueViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.tvPlagueName.text = item.name

            binding.tvPlagueName.setOnClickListener { view ->
                if(activity!!.ifConected()) {
                    val dialog = VisualSupportDialog().newInstance(item.id!!, item.name!!, 2)
                    dialog.show(activity?.supportFragmentManager!!, PhenologyDetailDialog.TAG)
                }
            }

            binding.btnPlus.setOnClickListener { view ->
                if (binding.etMedition.text.isEmpty()) {
                    binding.etMedition.setText("1")
                    item.medition = 1
                } else {
                    if (binding.etMedition.text.toString().toInt() < 9999) {
                        item.medition = (binding.etMedition.text.toString().toInt() + 1)
                        binding.etMedition.setText(item.medition.toString())
                    }
                }
            }

            binding.btnMinus.setOnClickListener { view ->
                if (binding.etMedition.text.isNotEmpty()) {
                    if (binding.etMedition.text.toString().toInt() > 1) {
                        item.medition = (binding.etMedition.text.toString().toInt() - 1)
                        binding.etMedition.setText(item.medition.toString())
                    } else {
                        binding.etMedition.setText("")
                        item.medition = 0
                    }
                }
            }

            binding.etMedition.addTextChangedListener(object : TextWatcher{
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (binding.etMedition.text.isNotEmpty()) item.medition = binding.etMedition.text.toString().toInt()
                    else item.medition = 0
                }
            })

            Realm.getDefaultInstance().use {
                val result = it
                        .where(MipeMonitoringModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .equalTo("grooveNo", groove)
                        .and()
                        .equalTo("week", week)
                        .endGroup()
                        .findFirst()

                if(result != null){
                    val plantResult = result.plants!!.where().equalTo("plantNo", plant).findFirst()
                    if(plantResult != null){
                        val data = plantResult.findings!!.where().equalTo("plagueId", item.id).findFirst()
                        if(data != null){
                            if(data.finding == null) binding.etMedition.setText("")
                            else binding.etMedition.setText(data.finding.toString())
                        }
                        binding.etMedition.isEnable(false)
                        binding.btnPlus.isEnable(false)
                        binding.btnMinus.isEnable(false)
                    }
                }
            }
        }

    }


}
