package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.graphics.drawable.Drawable
import android.view.View
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import io.reactivex.Observable

class ItemActivitiesPayrollViewModel(private val stageUuid: String, item: ActivityModel): BaseAdapter.ViewModel<ActivityModel>(item) {

    var name = ObservableField<String>()

    var count = ObservableField<String>()

    var showCounter = ObservableInt(View.GONE)

    var counter: Observable<Int>? = null

    var isExtraHour = false

    var state = ObservableBoolean(false)
    var iconDrawable: Drawable? = null

    override fun bind(item: ActivityModel) {
        name.set(item.name)
        isExtraHour = false

        val activityLogCount = item.activityLogCounts?.find { count ->
            count.stageUuid == stageUuid
        }

        count.set(activityLogCount?.count?.toString() ?: "0")
        state.set((activityLogCount?.count ?: 0) > 0)
        showCounter.set(if ((activityLogCount?.count ?: 0) > 0) View.VISIBLE else View.GONE)
    }

}
