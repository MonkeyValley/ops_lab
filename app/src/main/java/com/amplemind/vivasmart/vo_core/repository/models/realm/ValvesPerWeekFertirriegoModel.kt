package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class ValvesPerWeekFertirriegoModel(
        @SerializedName("lot_id") var lotId: Int = 0,
        @SerializedName("date0") var date0: RealmList<TablesPerDay>? = RealmList(),
        @SerializedName("date1") var date1: RealmList<TablesPerDay>? = RealmList(),
        @SerializedName("date2") var date2: RealmList<TablesPerDay>? = RealmList(),
        @SerializedName("date3") var date3: RealmList<TablesPerDay>? = RealmList(),
        @SerializedName("date4") var date4: RealmList<TablesPerDay>? = RealmList(),
        @SerializedName("date5") var date5: RealmList<TablesPerDay>? = RealmList(),
        @SerializedName("date6") var date6: RealmList<TablesPerDay>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()

) : RealmObject(), Identifiable

open class TablesPerDay(
        @SerializedName("lot_id") var lotId: Int = 0,
        @SerializedName("date") var date: String = "",
        @SerializedName("T1") var T1: RealmList<ValveForTable>? = RealmList(),
        @SerializedName("T2") var T2: RealmList<ValveForTable>? = RealmList(),
        @SerializedName("T3") var T3: RealmList<ValveForTable>? = RealmList(),
        @SerializedName("T4") var T4: RealmList<ValveForTable>? = RealmList(),
        @SerializedName("T5") var T5: RealmList<ValveForTable>? = RealmList(),
        @SerializedName("T6") var T6: RealmList<ValveForTable>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable


open class ValveForTable(
        @SerializedName("barrena") var barrena: Int = 0,
        @SerializedName("date") var date: String = "",
        @SerializedName("medition_12") var medition12: String? = "",
        @SerializedName("medition_18") var medition18: String? = "",
        @SerializedName("medition_24") var medition24: String? = "",
        @SerializedName("medition_6") var medition6: String? = "",
        @SerializedName("medition_type") var meditionType: Int = 0,
        @SerializedName("valve_id") var valveId: Int = 0,
        @SerializedName("m3ha") var m3ha: Int? = null,
        @SerializedName("minutes") var minutes: Double? = null,
        @SerializedName("pulses_no") var pulsesNo: Int? = null,
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable