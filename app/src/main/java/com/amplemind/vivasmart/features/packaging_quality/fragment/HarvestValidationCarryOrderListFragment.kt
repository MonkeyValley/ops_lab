package com.amplemind.vivasmart.features.packaging_quality.fragment


import android.app.DatePickerDialog
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.remote.models.HarvestValidationCarryOrderModel
import com.amplemind.vivasmart.features.packaging_quality.adapters.HarvestValidationCarryOrderAdapter
import com.amplemind.vivasmart.features.packaging_quality.viewModel.HarvestValidationCarryOrderListViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.HarvestValidationCarryOrderViewModel
import com.amplemind.vivasmart.vo_core.utils.currentDateInMillis
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.fragment_harvest_validation_carry_order_list.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class HarvestValidationCarryOrderListFragment : BaseFragment() {

    companion object {

        fun newInstance() = HarvestValidationCarryOrderListFragment()

    }

    @Inject
    lateinit var mViewModel: HarvestValidationCarryOrderListViewModel

    private lateinit var mAdapter: HarvestValidationCarryOrderAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_harvest_validation_carry_order_list, container, false)
    }

    override fun setUpUI() {
        super.setUpUI()
        displayHumanDate()
        setUpRecyclerView()
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()

        sr_refresh.setOnRefreshListener {
            mViewModel.refresh()
        }

        RxView.clicks(btn_date)
                .filter{
                    !sr_refresh.isRefreshing
                }
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe(this::onDateClicked)
                .addTo(subscriptions)

        RxView.clicks(btn_date_prev)
                .filter{
                    !sr_refresh.isRefreshing
                }
                .map {
                    mViewModel.previousDate()
                }
                .subscribe(this::setUIForDateChange)
                .addTo(subscriptions)

        RxView.clicks(btn_date_next)
                .filter{
                    !sr_refresh.isRefreshing
                }
                .map {
                    mViewModel.nextDate()
                }
                .subscribe(this::setUIForDateChange)
                .addTo(subscriptions)

    }

    override fun loadData() {
        super.loadData()
        mViewModel.loadHarvestValidationCarryOrders()
                .subscribe(this::onCarryOrdersLoaded, this::onError)
                .addTo(subscriptions)
    }

    override fun onStart() {
        super.onStart()
        refreshData()
    }

    override fun onResume() {
        super.onResume()
        sendSetToolbarTitleEvent(getString(R.string.hvlf_subtitle))
    }

    private fun onCarryOrdersLoaded(carryOrders: List<HarvestValidationCarryOrderViewModel>) {
        showProgress(false)
        sr_refresh.isRefreshing = false
        if (carryOrders.isEmpty()) {
            showNoCarryOrdersFound(true)
            showRecyclerView(false)
        }
        else {
            showNoCarryOrdersFound(false)
            showRecyclerView(true)
            mAdapter.setItems(carryOrders)
        }
    }

    private fun refreshData() {
        mViewModel.refresh()
        setUIForDateChange(true)
    }

    private fun setUpRecyclerView() {
        mAdapter = HarvestValidationCarryOrderAdapter(mEventBus)
        rv_carry_orders.hasFixedSize()
        rv_carry_orders.adapter = mAdapter
        rv_carry_orders.layoutManager = LinearLayoutManager(context!!)
    }

    private fun onDateClicked(obj: Any) {
        val dialog = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
            setUIForDateChange(
                    mViewModel.setDate(selectedYear, selectedMonth, selectedDay)
            )
        }, mViewModel.year, mViewModel.month, mViewModel.day)

        dialog.apply {
            datePicker.maxDate = currentDateInMillis()
            show()
        }
    }

    private fun onError(error: Throwable) {
        showErrorDialog(error)
    }

    private fun displayHumanDate() {
        btn_date.text = mViewModel.humanDate
    }

    private fun setUIForDateChange(setUI: Boolean) {
        if (setUI) {
            displayHumanDate()
            showNoCarryOrdersFound(false)
            showRecyclerView(false)
            showProgress(true)
        }
    }

    private fun showProgress(show: Boolean) {
        progress.visibility = if (show) View.VISIBLE else View.GONE
        sr_refresh.isEnabled = !show
    }

    private fun showNoCarryOrdersFound(show: Boolean) {
        no_carry_orders_found.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun showRecyclerView(show: Boolean) {
        rv_carry_orders.visibility = if (show) View.VISIBLE else View.GONE
    }

    data class OnCarryOrderClickedEvent(
            val carryOrder: HarvestValidationCarryOrderModel
    )
}
