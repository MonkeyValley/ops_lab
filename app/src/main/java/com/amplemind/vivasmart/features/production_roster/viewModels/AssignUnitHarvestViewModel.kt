package com.amplemind.vivasmart.features.production_roster.viewModels

import android.util.Log
import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.repository.response.CarryOrdersResponse
import com.amplemind.vivasmart.vo_core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.utils.currentISODate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class AssignUnitHarvestViewModel @Inject constructor() {

    lateinit var activityLog: ActivityLogModel

    private var mTotalBoxes = ""
    var mTotalAssignedBoxes = 0
    var mTotalReceivedBoxes = 0

    val badBoxes: Int
        get() = activityLog.badQuantity ?: 0

    val goodBoxes: Int
        get() = activityLog.quantity

    val totalBoxes: Int
        get() = badBoxes + goodBoxes

    var newBadBoxes: Int = 0
    var newGoodBoxes: Int = 0
    val totalNewBoxes: Int get() = newGoodBoxes + newBadBoxes

    fun initialize(activityLog: ActivityLogModel) {
        this.activityLog = activityLog
        newBadBoxes = badBoxes
        newGoodBoxes = goodBoxes
    }

    fun valuesChanged() =
            badBoxes != newBadBoxes || goodBoxes != newGoodBoxes

    fun isBoxCountExceeded(): Boolean {
        Log.e("total", mTotalReceivedBoxes.toString())
        Log.e("assign", mTotalAssignedBoxes.toString())
        return totalNewBoxes > (mTotalReceivedBoxes - mTotalAssignedBoxes)
    }


}