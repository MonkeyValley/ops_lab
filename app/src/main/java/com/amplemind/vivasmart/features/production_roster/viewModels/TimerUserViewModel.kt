package com.amplemind.vivasmart.features.production_roster.viewModels

import android.os.Handler
import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.core.extensions.formatTime
import com.amplemind.vivasmart.core.navegation.data_mediator.MediatorNavigation
import com.amplemind.vivasmart.core.repository.model.CollaboratorsListReponse
import io.reactivex.subjects.BehaviorSubject
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class TimerUserViewModel : Parcelable {

    private var data: CollaboratorsListReponse? = null

    /*
    * Behavior Observables
    * they are used to notify the view about any change
    * */
    private val clockAnimationObservable = BehaviorSubject.create<Boolean>()
    private val clockObservable = BehaviorSubject.create<String>()

    var mediatorNavigation: MediatorNavigation? = null

    var name: String? = null
    var activityLogId: Long? = null
    var registerId: Int? = null
    var showErrorMissingUnits = false
    var units = 0
    var count_box : Float? = null
    var olds_count_box : Float? = null
    var name_unit: String? = null
    var cost: Double? = null

    var isOnLine : Boolean = false

    private val timer = Handler()
    var timeIsRunning = false
    var time = 25200L
        private set
    var elapsedTime = 0L

    val nameTab = 1

    var is_time: Boolean? = null
    var packing_id: Int? = null
    var packing_line: Int? = null
    var name_presentation: String? = null
    var isCreatedOffline = false
        private set

    fun startTimer() {
        if (!timeIsRunning) {
            timeIsRunning = true
            clockAnimationObservable.onNext(true)
            timer.postDelayed(object : Runnable {
                override fun run() {
                    time += 1
                    clockObservable.onNext(getElapsedTime())
                    timer.postDelayed(this, 1000)
                }
            }, 1000)
        }
    }

    fun getElapsedTime(): String {
        return formatTime(time)
    }

    fun getMinutesTime(): Int {
        return TimeUnit.SECONDS.toMinutes(time).toInt()
    }

    fun stopTimer() {
        if (timeIsRunning) {
            timer.removeCallbacksAndMessages(null)
            timeIsRunning = false
            clockAnimationObservable.onNext(false)
        }
    }

    fun onDestroy() {
        timer.removeCallbacksAndMessages(null)
    }

    fun getCollaboratorId(): Int {
        return data?.collaboratorId ?: 0
    }

    fun getCollaboratorIdDB(): Int {
        return data?.id ?: 0
    }

    fun getActivityCode(): Int {
        return data?.activityCodeId ?: 0
    }

    fun getCollaboratorIdLogLine() : Int{
        return data?.id ?: 0
    }

    fun getActivityCodeCollaboratorId(): Int {
        return data?.id ?: 0
    }

    fun getUserImage(): String? {
        return data?.collaborator?.image
    }


    //region BehaviorSubject
    fun getClockAnimationObservable(): BehaviorSubject<Boolean> {
        return clockAnimationObservable
    }

    fun getClockObservable(): BehaviorSubject<String> {
        return clockObservable
    }

    fun canAssignUnits(): Boolean {
        if (activityLogId != null) {
            return true
        }
        return timeIsRunning
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    fun setActivities(data: CollaboratorsListReponse) {
        this.data = data
        name = data.collaborator.name
        this.packing_id = data.packing_id
        this.packing_line = data.packingline_id
        this.is_time = data.is_time
        this.name_presentation = data.name_presentation
        registerId = data.id
        time = if (data.isPaused) data.localTime else data.timerCount.toLong() + data.localTime
        if (!data.isPaused) {
            startTimer()
        }
        isCreatedOffline = data.collaborator.name == data.collaborator.employeeCode
    }

    fun setMediator(mediatorNavigation: MediatorNavigation) {
        this.mediatorNavigation = mediatorNavigation
    }

    companion object CREATOR : Parcelable.Creator<TimerUserViewModel> {
        override fun createFromParcel(parcel: Parcel): TimerUserViewModel {
            return TimerUserViewModel()
        }

        override fun newArray(size: Int): Array<TimerUserViewModel?> {
            return arrayOfNulls(size)
        }
    }

    fun getCurrentDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return sdf.format(Date())
    }

    fun setUserTime(userTime: Long) {
        time = userTime
    }

}