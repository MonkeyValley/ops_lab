package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class LotModel (
        @SerializedName("id")                           var id: Int = 0,
        @SerializedName("business_unit_id")             var businessUnitId: Int = 0,
        @SerializedName("company_id")                   var companyId: Int = 0,
        @SerializedName("lot_type_id")                  var lotTypeId: Int = 0,
        @SerializedName("name")                         var name: String? = null,
        @SerializedName("is_closed")                    var isClosed: Boolean = false,
        @SerializedName("last_close")                   var lastClose: String? = null,
        @SerializedName("is_quality_control_closed")    var isQualityControlClosed: Boolean = false,
        @SerializedName("is_qa_done")                   var isQaDone: String? = "",
        @SerializedName("business_unit")                var businessUnit: BusinessUnitModel? = null,
        @SerializedName("table_1")                      var table1: Int = 0,
        @SerializedName("table_2")                      var table2: Int = 0,
        @SerializedName("table_3")                      var table3: Int = 0,
        @SerializedName("table_4")                      var table4: Int = 0,
        @SerializedName("table_5")                      var table5: Int = 0,
        @SerializedName("table_6")                      var table6: Int = 0,
        @SerializedName("soil_id")                      var soilId: Int? = null)
    : RealmObject(), Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readParcelable<BusinessUnitModel>(BusinessUnitModel::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt())


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(businessUnitId)
        parcel.writeInt(companyId)
        parcel.writeInt(lotTypeId)
        parcel.writeString(name)
        parcel.writeByte(if (isClosed) 1 else 0)
        parcel.writeString(lastClose)
        parcel.writeByte(if (isQualityControlClosed) 1 else 0)
        parcel.writeString(isQaDone)
        parcel.writeParcelable(businessUnit, flags)
        parcel.writeInt(table1)
        parcel.writeInt(table2)
        parcel.writeInt(table3)
        parcel.writeInt(table4)
        parcel.writeInt(table5)
        parcel.writeInt(table6)
        parcel.writeInt(soilId!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LotModel> {
        override fun createFromParcel(parcel: Parcel): LotModel {
            return LotModel(parcel)
        }

        override fun newArray(size: Int): Array<LotModel?> {
            return arrayOfNulls(size)
        }
    }
}



