package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class RoleModel(@SerializedName("id") @PrimaryKey val roleId: Int,
                     @SerializedName("name") val roleName: String)