package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index

open class StageVarietyModel(
        @Index
        @SerializedName("id")           var id: Long = 0,
        @SerializedName("is_priority")  var isPriority: Boolean = false,
        @SerializedName("sampling")     var sampling: Int = 0,
        @Index
        @SerializedName("stage_id")     var stageId: Long = 0,
        @SerializedName("table_no")     var tableNo: Int = 0,
        @Index
        @SerializedName("variety_id")   var varietyId: Long = 0,
        @SerializedName("variety")      var variety: VarietyModel? = null
) : RealmObject(), Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readLong(),
                parcel.readByte() != 0.toByte(),
                parcel.readInt(),
                parcel.readLong(),
                parcel.readInt(),
                parcel.readLong(),
                parcel.readParcelable(VarietyModel::class.java.classLoader))

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeLong(id)
                parcel.writeByte(if (isPriority) 1 else 0)
                parcel.writeInt(sampling)
                parcel.writeLong(stageId)
                parcel.writeInt(tableNo)
                parcel.writeLong(varietyId)
                parcel.writeParcelable(variety, flags)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<StageVarietyModel> {
                override fun createFromParcel(parcel: Parcel): StageVarietyModel {
                        return StageVarietyModel(parcel)
                }

                override fun newArray(size: Int): Array<StageVarietyModel?> {
                        return arrayOfNulls(size)
                }
        }
}