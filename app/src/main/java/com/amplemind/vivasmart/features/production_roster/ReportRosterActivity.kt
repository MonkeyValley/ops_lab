package com.amplemind.vivasmart.features.production_roster

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.MenuItem
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.forceFocus
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.extensions.showErrorDialog
import com.amplemind.vivasmart.features.production_roster.viewModels.ReportRosterViewModel
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.content_report_roster_activity.*
import javax.inject.Inject

class ReportRosterActivity : BaseActivityWithFragment(){

    private val onSearchChange: BehaviorSubject<String> = BehaviorSubject.create()
    private val codeSearch: BehaviorSubject<String> = BehaviorSubject.create()

    companion object {

        val TAG = ReportRosterActivity::class.java
        val PARAM_STAGE_UUID: String = "$TAG.StageUuid"
        val PRAM_STAGE_TYPE_ID: String = "$TAG.StageTypeId"

    }

    @Inject
    lateinit var mViewModel: ReportRosterViewModel

    @Inject
    lateinit var mEventBus: EventBus

    lateinit var fragmentAdapter: ReportRosterViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_report_roster_activity)

        readArguments()
        setUpUI()
        setUpUICallbacks()

    }

    override fun onDestroy() {
        super.onDestroy()
        mViewModel.cleanUp()
    }

    private fun readArguments() {
        mViewModel.stageUuid = requireNotNull( intent.getStringExtra(PARAM_STAGE_UUID) )
        mViewModel.stageTypeId = requireNotNull( intent.getIntExtra(PRAM_STAGE_TYPE_ID, 0) )
    }

    private fun setUpUI() {
        setupToolbar()
        val nameStage = this.intent.getStringExtra("nameStage")
        fragmentAdapter = ReportRosterViewPager(this, supportFragmentManager,
                mViewModel.stageUuid,  mViewModel.stageTypeId, nameStage)
        viewPager_report_roster.adapter = fragmentAdapter
        viewPager_report_roster.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                btn_send.visibility = if(position == 2 || position == 0) View.GONE else View.VISIBLE
                /*val fragment: Fragment = fragmentAdapter.getRegisteredFragment(position)!!
                (fragment as SelectGrooveFragment)*/
            }
        })
        tabs_roster.setupWithViewPager(viewPager_report_roster)
    }

    private fun setUpUICallbacks() {
        btn_send.clicks()
                .subscribe {
                    validateDailyPayCollaboratorsPending()
                }
                .addTo(subscriptions)
    }


    private fun validateDailyPayCollaboratorsPending(){
        mViewModel.hasDailyPayCollaboratorsPending()
                .subscribe({ hasUnsignedLogs ->
                    if (hasUnsignedLogs) {
                        AlertDialog.Builder(this@ReportRosterActivity)
                                .setTitle(R.string.rra_send_report_title)
                                .setMessage(R.string.rra_daily_pay_pending)
                                .setPositiveButton(R.string.ok) { dialog, _ ->
                                    dialog.dismiss()
                                }
                                .show()
                    }
                    else {
                        onSendClicked()
                    }
                }, { error ->
                    showErrorDialog(error)
                })
                .addTo(subscriptions)
    }

    private fun onSendClicked() {
        mViewModel.hasPeopleWorking()
                .subscribe ({ hasPeopleWorking ->
                    if (hasPeopleWorking) {

                        AlertDialog.Builder(this@ReportRosterActivity)
                                .setTitle(R.string.rra_send_report_title)
                                .setMessage(R.string.rra_has_people_working)
                                .show()
                    }
                    else {
                        checkUnsignedLogs()
                    }
                }, { error ->
                    showErrorDialog(error)
                }
                )
                .addTo(subscriptions)
    }

    private fun checkUnsignedLogs() {
        mViewModel.hasUnsignedLogs()
                .subscribe({ hasUnsignedLogs ->
                    if (hasUnsignedLogs) {
                        AlertDialog.Builder(this@ReportRosterActivity)
                                .setTitle(R.string.rra_send_report_title)
                                .setMessage(R.string.rra_has_unsigned)
                                .setPositiveButton(R.string.ok) { dialog, _ ->
                                    dialog.dismiss()
                                }
                                .show()
                    }
                    else {
                        sendReport()
                    }
                }, { error ->
                    showErrorDialog(error)
                })
                .addTo(subscriptions)
    }

    private fun sendReport() {
        AlertDialog.Builder(this@ReportRosterActivity)
                .setTitle(R.string.rra_send_report_title)
                .setMessage(R.string.rra_send_report)
                .setNegativeButton(R.string.yes) { _, _ ->
                    mViewModel.setAllAsDone()
                            .subscribe({
                                mEventBus.send(CloseLotEvent(mViewModel.stageUuid))
                                setResult(RESULT_OK, intent.putExtra("code", 200))
                                finish()
                            }, this::showErrorDialog)
                            .addTo(subscriptions)
                }
                .setPositiveButton(R.string.no) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_report_roster
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.rra_activity_title)
//        search_view.findViewById<EditText>(android.support.v7.appcompat.R.id.search_src_text).searchViewPadding()
//        search_view.setOnQueryTextListener(object: SearchView.OnQueryTextListener, android.support.v7.widget.SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(query: String?): Boolean {
//                onSearchChange.onNext(query ?: "")
//                return true
//            }
//
//            override fun onQueryTextChange(newText: String?): Boolean {
//                onSearchChange.onNext(newText ?: "")
//                return true
//            }
//        })
        forceFocus(toolbar_report_roster)
        toolbar_report_roster.postDelayed({
            hideKeyboard(this)
        }, 500)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    data class CloseLotEvent(
            val stageUuid: String
    )

    fun onSearchQueryChange(): BehaviorSubject<String> {
        return onSearchChange
    }

    fun onCodeSearch(): BehaviorSubject<String> {
        return codeSearch
    }

}
