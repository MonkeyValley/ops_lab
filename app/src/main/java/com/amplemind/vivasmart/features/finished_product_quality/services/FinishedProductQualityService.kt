package com.amplemind.vivasmart.features.finished_product_quality.services

import android.util.Log
import com.amplemind.vivasmart.vo_core.repository.models.realm.CropStageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.CropUnitModel
import com.google.gson.Gson
import io.realm.Realm
import io.realm.Sort


class FinishedProductQualityService{

    fun listCrop(): ArrayList<CropStageModel>{
        val list: ArrayList<CropStageModel> = ArrayList()

        try {
            Realm.getDefaultInstance().use { realm ->
                val results = realm.where(CropStageModel::class.java)
                        .distinct("name")
                        .sort("name", Sort.ASCENDING)
                        .findAll()

                list.addAll(realm.copyFromRealm(results))
            }
        } catch (e: Exception){
                Log.e("error", e.toString())
        }

        Log.d("listCrop", Gson().toJson(list))

        return list
    }

    fun listCropUnit(): ArrayList<CropUnitModel>{
        val list: ArrayList<CropUnitModel> = ArrayList()

        try {
            Realm.getDefaultInstance().use { realm ->
                val results = realm.where(CropUnitModel::class.java)
                        .distinct("name")
                        .sort("name", Sort.ASCENDING)
                        .findAll()

                list.addAll(realm.copyFromRealm(results))
            }
        } catch (e: Exception){
            Log.e("error", e.toString())
        }

        Log.d("listCropUnit", Gson().toJson(list))

        return list
    }




}