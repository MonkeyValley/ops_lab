package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.model.IssuesByCategoryResponse
import com.amplemind.vivasmart.core.repository.request.*
import com.amplemind.vivasmart.core.repository.response.IncidentsResponse
import io.reactivex.Observable
import retrofit2.http.*

interface ProductionQualityApi {


    @GET("v2/activity_sub_category")
    @Headers("Content-Type: application/json")
    fun getActivities(
            @Header("Authorization") authentication: String,
            @Query("activity_category_id") activityCategoryId: Int = 1
    ): Observable<CategoriesQualityResponse>

    @PUT("v2/lot/sign/{lotId}/{categoryId}")
    @Headers("Content-Type: application/json")
    fun finishActivities(
            @Header("Authorization") authentication: String,
            @Path("lotId") lotId: Int,
            @Path("categoryId") categoryId: Int,
            @Body request: ProductionQulitySignsRequest
    ): Observable<Any>

    @GET("v2/activity_sub_category/lot/{lotId}/{categoryId}")
    @Headers("Content-Type: application/json")
    fun getCountIssues(
            @Header("Authorization") authentication: String,
            @Path("lotId") lotId: Int,
            @Path("categoryId") categoryId: Int = 1
    ): Observable<ProductionCategoriesResponse>

    @GET("v2/issue_item/lot/{lot_id}/{sub_category_id__eq}")
    @Headers("Content-Type: application/json")
    fun getIssues(
            @Header("Authorization") authentication: String,
            @Path("lot_id") lot_id: Int,
            @Path("sub_category_id__eq") sub_category_id__eq: Int
    ): Observable<IssuesByCategoryResponse>

    @POST("v2/issue_item")
    @Headers("Content-Type: application/json")
    fun createIssue(
            @Header("Authorization") authentication: String,
            @Body body: CreateIssueRequest
    ): Observable<Any>

    @GET("v2/issue_item/report/{lotId}")
    @Headers("Content-Type: application/json")
    fun getIssuesReport(
            @Header("Authorization") authentication: String,
            @Path("lotId") lotId: Int
    ): Observable<ProductionCategoriesReportResponse>

    @DELETE("v2/issue_item/{issueId}")
    @Headers("Content-Type: application/json")
    fun deleteIssue(
            @Header("Authorization") authentication: String,
            @Path("issueId") issueId: Int
    ): Observable<Any>

    @POST("v2/offline/sync/issue_item")
    @Headers("Content-Type: application/json")
    fun syncIssuesReport(
            @Header("Authorization") authentication: String,
            @Body body: ReportIssuesRequest
    ): Observable<ReportIssuesResponse>

    @GET("v2/issue_item")
    @Headers("Content-Type: application/json")
    fun getOpenIncidents(
            @Header("Authorization") authentication: String,
            @Query("stage_id") stageId: Int,
            @Query("is_close") isClose: Boolean = false,
            @Query("embed") embed: String = "issue.sub_category",
            @Query("__logic") logic: String = "AND"
    ): Observable<IncidentsResponse>

    @PUT("v2/issue_item/{incident_id}")
    @Headers("Content-Type: application/json")
    fun closeIncident(
            @Header("Authorization") authentication: String,
            @Path("incident_id") incidentId: Int,
            @Body body: CloseIssueRequest
    ): Observable<CloseIssueResponse>

    @GET("/v2/quality_control/general_quality_report/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getGeneralQualityReport(
            @Header("Authorization") authentication: String,
            @Path("lot_id") lotId: Int
    ): Observable<List<GeneralQualityResponse>>
}