package com.amplemind.vivasmart.features.pollination.adapter

import android.util.SparseArray
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService
import com.amplemind.vivasmart.features.pollination.fragment.PollinationFragment

class PollinationLotTab (fm: FragmentManager, private val weekNumber: Int, private val lotId: Int) : FragmentPagerAdapter(fm) {

    private val registeredFragments = SparseArray<Fragment>()

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                val fragment = PollinationFragment().newInstance(weekNumber, lotId, 1)
                registeredFragments.put(position, fragment)
                return fragment
            }
            1 -> {
                val fragment = PollinationFragment().newInstance(weekNumber, lotId, 2)
                registeredFragments.put(position, fragment)
                return fragment
            }
            2 -> {
                val fragment = PollinationFragment().newInstance(weekNumber, lotId, 3)
                registeredFragments.put(position, fragment)
                return fragment
            }
            else ->  {
                val fragment = PollinationFragment().newInstance(weekNumber, lotId, 4)
                registeredFragments.put(position, fragment)
                return fragment
            }
        }
    }

    override fun getCount(): Int {
        return FertirriegoService().getTableCountPerLot(lotId, "")
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Tabla 1"
            1 -> "Tabla 2"
            2 -> "Tabla 3"
            else -> "Tabla 4"
        }
    }

    fun getRegisteredFragment(position: Int): Fragment? {
        return registeredFragments[position]
    }
}