package com.amplemind.vivasmart.features.login.viewModel

import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.utils.Validations
import com.amplemind.vivasmart.features.login.LoginActivity
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class SetPasswordViewModel @Inject constructor(private val repository: AccountRepository, val validation: Validations) {

    /*
   * Behavior Observables
   * they are used to notify the view about any change
   * */
    private val formValidation = BehaviorSubject.create<Validations.PasswordValidation>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val successGetUser = BehaviorSubject.create<Boolean>()
    private val failRequest = BehaviorSubject.create<String>()

    fun validatePassword(password: String, repeatPassword: String) {
        formValidation.onNext(validation.validatePassword(password, repeatPassword))
    }

    fun getUser(): Disposable {
        return repository.getUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    repository.saveUser(Gson().toJson(it))
                    successGetUser.onNext(true)
                }, {
                    failRequest.onNext(LoginActivity.LOGIN_ERROR_MESSAGE)
                } )
    }

    fun saveToken(token: String) {
        repository.saveTokenUser(token)
    }

    fun setNewPassword(request: AccountRepository.SetPasswordRequest): Observable<AccountRepository.SetPasswordResponse> {
        return repository.setPassword(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    //region BehaviorSubject
    fun getFormValidation(): BehaviorSubject<Validations.PasswordValidation> {
        return formValidation
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return failRequest
    }

    fun onSuccessGetUser(): BehaviorSubject<Boolean> {
        return successGetUser
    }
}