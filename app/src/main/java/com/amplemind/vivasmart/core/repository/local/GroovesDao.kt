package com.amplemind.vivasmart.core.repository.local

import androidx.room.*
import com.amplemind.vivasmart.core.repository.model.Grooves
import com.amplemind.vivasmart.core.repository.model.GroovesResultReponse
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface GroovesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertActivityLog(recent: GroovesResultReponse): Long

    @Query("UPDATE GroovesResultReponse SET quantity = :quantity WHERE primary_activity_log_id = :id")
    fun updateActivityLogQuantity(quantity: Int, id: Long)

    @Query("SELECT * FROM GroovesResultReponse WHERE primary_activity_log_id = :id")
    fun getActivityLog(id: Long): GroovesResultReponse?


    @Query("SELECT * FROM GroovesResultReponse")
    fun getActivityLogTest(): List<GroovesResultReponse>


    @Query("SELECT * FROM GroovesResultReponse WHERE collaborator_id = :id_collaborator AND activity_code_id = :id_code")
    fun getActivityLogUnits(id_collaborator :Int, id_code : Int): GroovesResultReponse?



    @Query("DELETE FROM GroovesResultReponse WHERE primary_activity_log_id = :id")
    fun deleteActivityLog(id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGroove(recent: Grooves)

    @Query("DELETE FROM Grooves WHERE id = :grooveId")
    fun deleteGroove(grooveId: Int)

    @Query("SELECT * FROM Grooves WHERE activity_code_id = :activityCode")
    fun getGrooves(activityCode: Int) : Single<List<Grooves>>

    @Query("SELECT * FROM Grooves")
    fun getGroovesTest() : List<Grooves>

    @Query("SELECT * FROM Grooves WHERE primary_activity_log_id = :activityLogId ")
    fun getGroovesByLogId(activityLogId: Long) : List<Grooves>

    @Query("SELECT * FROM Grooves WHERE groove_no = :grooveNo AND table_no = :table")
    fun getGrooveBy(grooveNo: Int, table: Int): Grooves?

    @Query("SELECT * FROM Grooves WHERE activity_log_id = :activityLogId")
    fun getGrooveByActivityLog(activityLogId: Long): List<Grooves>


    @Query("SELECT * FROM Grooves WHERE activity_code_id = :activity_code_id AND collaborator_id = :collaborator_id")
    fun getGrooveByActivityAndIdCollaborator(collaborator_id : Int,activity_code_id: Int): List<Grooves>?

}