package com.amplemind.vivasmart.features.quality_control.viewModel

import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.core.repository.model.ControlQualityGrooveModel
import com.amplemind.vivasmart.core.repository.model.ControlQualityGroovesModel
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by amplemind on 7/19/18.
 */
class ControlGrooveViewModel constructor(groove : ControlQualityGroovesModel) {

    val id = groove.id
    val section = "${groove.tableNo}-${groove.grooveNo}"
    val date = dateFormat(groove.date ?: "")
    val done = ObservableInt(if (groove.qualityControl) View.VISIBLE else View.GONE)
    val isFinished = groove.qualityControl
    val syncError = groove.errorSync


    private fun dateFormat(date_groove : String) : String {
        val date = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date_groove)

        return SimpleDateFormat("dd/MM", Locale.ENGLISH).format(date)
    }

}