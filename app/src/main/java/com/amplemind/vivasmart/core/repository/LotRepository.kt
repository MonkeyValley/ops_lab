package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.CollaboratorsDao
import com.amplemind.vivasmart.core.repository.local.LotsDao
import com.amplemind.vivasmart.core.repository.model.Stage
import com.amplemind.vivasmart.core.repository.remote.LotApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LotRepository @Inject constructor(private val api: LotApi, private val database: LotsDao, private val databaseCollaborator : CollaboratorsDao) {

    //TODO: Check if this can be in another way
    data class LotResponse(@SerializedName("data") val data: List<StageModel>)

    @Inject
    lateinit var  authorization: ApiAuthorization

    fun getLots(token: String, businessUnitId: String): Observable<LotResponse> {
        return api.getLots(authentication = authorization.getAuthToken(token), businessUnitId = businessUnitId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun reOpenLot(token: String, lot_id: Int): Observable<Any> {
        return api.reOpenLots(authorization.getAuthToken(token), lot_id)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun saveStages(stages: ArrayList<Stage>) {
        for (stage in stages) {
            database.insertLot(stage.lot)
            database.insertStage(stage)
        }
    }

    fun getStages():  Observable<List<Stage>> {
        return Observable.fromCallable {
            return@fromCallable database.getLocalStages()
        }
    }
}