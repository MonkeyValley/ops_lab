package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_2_9_0 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectModelCollaboratorModel(realm)
    }

    private fun migrateUploadObjectModelCollaboratorModel(realm: DynamicRealm){
        realm.schema.get("CollaboratorModel")?.apply {
            addField("activityDate", String::class.javaObjectType)
            addField("activityId", Int::class.javaObjectType)
        }
    }

    override fun drop(realm: DynamicRealm) {

    }
}