package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_2_8_0 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectModelProductionRangeWeek(realm)
        migrateUploadObjectModelProductionRangeVarieties(realm)
        migrateUploadObjectModelLotModel(realm)
    }

    private fun migrateUploadObjectModelProductionRangeWeek(realm: DynamicRealm) {
        val productionVarSchema = realm.schema
                .create("ProductionVar")
                .addField("id", Int::class.javaObjectType)
                .addField("lotId", Int::class.javaObjectType)
                .addField("weekNumer", Int::class.javaObjectType)
                .addField("maxVal", String::class.javaObjectType)
                .addField("minVal", String::class.javaObjectType)
                .addField("productionVarId", Int::class.javaObjectType)
                .addField("productionVarOptionId", Int::class.javaObjectType)
                .addField("uuid", String::class.javaObjectType)
                .addField("value", String::class.javaObjectType)

        val productionRangeWeekModelSchema = realm.schema
                .create("ProductionRangeWeekModel")
                .addField("startDate", String::class.javaObjectType)
                .addField("endDate", String::class.javaObjectType)
                .addRealmListField("productionVars", productionVarSchema)

        realm.schema
                .create("ProductionRangesDataModel")
                .addField("lotId", Int::class.javaObjectType)
                .addRealmObjectField("week0", productionRangeWeekModelSchema)
                .addRealmObjectField("week1", productionRangeWeekModelSchema)
                .addRealmObjectField("week2", productionRangeWeekModelSchema)
    }

    private fun migrateUploadObjectModelProductionRangeVarieties(realm: DynamicRealm) {

        val activitySubCategorySchema = realm.schema
                .create("ActivitySubCategory")
                .addField("activityCategoryId", Int::class.javaObjectType)
                .addField("id", Int::class.javaObjectType)
                .addField("name", String::class.javaObjectType)
                .addField("qualityPercentage", Double::class.javaObjectType)

        val cropSchema = realm.schema
                .create("Crop")
                .addField("id", Int::class.javaObjectType)
                .addField("cropTypeId", Int::class.javaObjectType)
                .addField("graftName", String::class.javaObjectType)
                .addField("hasCluster", Boolean::class.javaObjectType)
                .addField("isActive", Boolean::class.javaObjectType)
                .addField("isGraft", Boolean::class.javaObjectType)
                .addField("name", String::class.javaObjectType)
                .addField("presentationNames", String::class.javaObjectType)
                .addField("uuid", String::class.javaObjectType)
                .addField("variety", String::class.javaObjectType)

        val cropsSchema = realm.schema
                .create("Crops")
                .addField("cropId", Int::class.javaObjectType)
                .addField("productionVarId", Int::class.javaObjectType)
                .addRealmObjectField("crop", cropSchema)

        val soilTypeSchema = realm.schema
                .create("SoilType")
                .addField("id", Int::class.javaObjectType)
                .addField("name", String::class.javaObjectType)

        val productionVarTypeSchema = realm.schema
                .create("ProductionVarType")
                .addField("id", Int::class.javaObjectType)
                .addField("name", String::class.javaObjectType)

        val productionVarDataTypeSchema = realm.schema
                .create("ProductionVarDataType")
                .addField("id", Int::class.javaObjectType)
                .addField("name", String::class.javaObjectType)
                .addField("productionVarTypeId", Int::class.javaObjectType)

        val optionsSchema = realm.schema
                .create("ProductionRangesOption")
                .addField("id", Int::class.javaObjectType)
                .addField("name", String::class.javaObjectType)
                .addField("productionVarId", Int::class.javaObjectType)

        realm.schema
                .create("ProductionRangeVariatiesModel")
                .addField("id", Int::class.javaObjectType)
                .addField("name", String::class.javaObjectType)
                .addRealmListField("options", optionsSchema)
                .addField("productionVarDataTypeId", Int::class.javaObjectType)
                .addRealmObjectField("productionVarDataType", productionVarDataTypeSchema)
                .addField("productionVarTypeId", Int::class.javaObjectType)
                .addRealmObjectField("productionVarType", productionVarTypeSchema)
                .addField("soilTypeId", Int::class.javaObjectType)
                .addRealmObjectField("soilType", soilTypeSchema)
                .addRealmListField("crops", cropsSchema)
                .addField("businessUnitId", Int::class.javaObjectType)
                .addField("activitySubCategoryId", Int::class.javaObjectType)
                .addRealmObjectField("activitySubCategory", activitySubCategorySchema)
                .addField("value", String::class.javaObjectType)
                .addField("minVal", String::class.javaObjectType)
                .addField("maxVal", String::class.javaObjectType)
                .addField("productionVarId", String::class.javaObjectType)
                .addField("productionVarOptionId", String::class.javaObjectType)
                .addField("mode", String::class.javaObjectType)
    }

    private fun migrateUploadObjectModelLotModel(realm: DynamicRealm){
        realm.schema.get("LotModel")?.apply {
            addField("soilId", Int::class.javaObjectType)
        }
    }

    override fun drop(realm: DynamicRealm) {

    }
}

