package com.amplemind.vivasmart.features.production_roster.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel

class LotInfoDialogFragment : DialogFragment() {

    companion object {
        val TAG = LotInfoDialogFragment::class.java.simpleName
        fun newInstance(stage: StageModel): LotInfoDialogFragment{
            val fragment = LotInfoDialogFragment()
            val args = Bundle()
            args.putParcelable("stage", stage)
            fragment.arguments = args
            return fragment
        }
    }


    private var mView : View? = null
    private var eventChange : OnChangeUnit?= null


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val mStage = arguments!!.getParcelable<Parcelable>("stage") as StageModel
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mView = LayoutInflater.from(context).inflate(R.layout.fragment_lot_info_dialog, null, false)
        builder.setView(mView)

        val txtSurface = mView!!.findViewById(R.id.lotinfo_txt_surface) as TextView
        val txtCrop = mView!!.findViewById(R.id.lotinfo_txt_crop) as TextView
        val txtDate = mView!!.findViewById(R.id.lotinfo_txt_date) as TextView
        val txtHead = mView!!.findViewById(R.id.lotinfo_txt_heads) as TextView
        val txtTables = mView!!.findViewById(R.id.lotinfo_txt_tables) as TextView

        txtCrop.text = mStage.crop!!.name
        txtDate.text = mStage.plantDate
        txtHead.text = mStage.plantsPerGroove.toString()
        // txtSurface.text = mStage.lot!!.real_surface
        txtTables.text = mStage.crop!!.presentationNames

        builder.setPositiveButton("Aceptar", null)
        builder.setNegativeButton("Cancelar") { _, i ->
            hideKeyboard(context!!)
            dismiss()
        }

        return builder.create()
    }

    private fun getBusy(): Int {
        return arguments!!.getInt("busy",0)
    }

    private fun getUnits(): Int {
        return arguments!!.getInt("units",0)
    }


    fun setChangeListenner(listenner: LotInfoDialogFragment.OnChangeUnit){
        this.eventChange = listenner
    }

    override fun onStart() {
        super.onStart()
        /*
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (!value!!.text.toString().isEmpty() && (value!!.text.toString().toInt() > 0)){
                if (value!!.text.toString().toInt() <= getBusy()){
                    eventChange!!.changeUnit(value!!.text.toString().toInt(), getUnits())
                    dismiss()
                }else{
                    mView!!.snack("Las unidades que desea asignar son mayores a las disponibles")
                }
            }else{
                mView!!.snack("No puede quedar vacío")
            }
        }
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
            eventChange!!.cancelEdit()
        }
        */
    }

    interface OnChangeUnit{
        fun changeUnit(new_value : Int, previous_value : Int)
        fun cancelEdit()
    }

}
