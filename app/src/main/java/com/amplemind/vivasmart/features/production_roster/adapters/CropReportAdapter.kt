package com.amplemind.vivasmart.features.production_roster.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemCropReportBinding
import com.amplemind.vivasmart.features.production_roster.viewModels.ItemCropReportViewModel

class CropReportAdapter constructor(val list: List<ItemCropReportViewModel>): RecyclerView.Adapter<CropReportAdapter.CropReportViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CropReportViewHolder {
        val binding = ItemCropReportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CropReportViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: CropReportViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class CropReportViewHolder(val binding: ItemCropReportBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemCropReportViewModel) {

            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()

        }
    }

}
