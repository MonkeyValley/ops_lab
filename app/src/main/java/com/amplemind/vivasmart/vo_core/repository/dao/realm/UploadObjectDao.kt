package com.amplemind.vivasmart.vo_core.repository.dao.realm

import android.content.Context
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.realm.Realm
import io.realm.Sort
import org.json.JSONArray
import org.json.JSONObject
import javax.inject.Inject

class UploadObjectDao @Inject constructor (
        realmHolder: RealmHolder
) : RealmDao<UploadObjectModel>(realmHolder, UploadObjectModel::class.java) {

    fun peekQueue(): Flowable<QueueStatusContainer> =
            Realm.getDefaultInstance().use { realm ->
                findAllLive {
                    query(realm).sort("order", Sort.ASCENDING)
                }.map { changes ->
                    changes.run {
                        QueueStatusContainer(data.firstOrNull(), data.size)
                    }
                }
            }

    fun saveServerError(uuid: String, code: Int, serverResponse: String) =
            executeTransaction { realm ->
                query(realm)
                        .equalTo("uuid", uuid)
                        .findFirst()
                        ?.apply {
                            errorCode = code.toString()
                            errorServerResponse = serverResponse
                        }
            }
            .ignoreElement()

    fun remove(uuid: String): Completable =
            deleteByUuid(uuid, null, null)
                    .ignoreElement()

    fun queueForUploadAux(realm: Realm, obj: UploadObjectModel) =
            queueForUploadAux(realm, listOf(obj))

    fun queueForUploadAux(realm: Realm, objs: List<UploadObjectModel>) {
        objs.forEach {obj ->
            val order = query(realm)
                    .max("order")?.toLong()?.inc() ?: 0L

            obj.order = order

            realm.insertOrUpdate(obj)
        }
    }

    data class QueueStatusContainer(
            val firstItem: UploadObjectModel?,
            val total: Int
    )

    fun getValuesForGivenKey(jsonArrayStr: String, key: String): String {
        try {
            val obj = JSONArray(jsonArrayStr).getJSONObject(0)
            return obj.getString(key)
        } catch (e: Exception) {
            return JSONObject(jsonArrayStr).optString(key)
        }
    }

    fun validateBulkCount(obj: UploadObjectModel): Int{
        return try {
            val items = JSONObject(obj.body).getJSONArray("bulk")
            items.length()
        } catch (e: Exception){
            1
        }
    }

    fun createNewPostFromCurrentError(context: Context, obj: UploadObjectModel, verb: String, mode: Int): Boolean {
        val prefer = context.getSharedPreferences(
                context.getString(R.string.app_name), Context.MODE_PRIVATE)
        try {
            val items = JSONObject(obj.body).getJSONArray("bulk")
            for (i in 0 until items.length()) {
                val item = items.getString(i)
                generatePost(obj.url, prefer.getString("token", "")!!,
                        obj.contentType, item, verb)
            }
        } catch (e: Exception){
            generatePost(obj.url, prefer.getString("token", "")!!,
                    obj.contentType, obj.body, verb)
        }

        return when(mode) {
            1 -> uploadOrder(obj)
            else -> deleteFromUploadObject(obj)
        }
    }

    private fun generatePost(url: String, token: String, contentType: String, body: String, verb: String){
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
            val obj = UploadObjectModel(
                    url = url,
                    token = token,
                    contentType = contentType,
                    verb = verb,
                    body = body)

                realm.insertOrUpdate(obj)
            }
        }
    }

    private fun uploadOrder(obj: UploadObjectModel): Boolean{
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                obj.order = obj.order + 100
                realm.insertOrUpdate(obj)
            }
        }
        return true
    }

    fun deleteFromUploadObject(obj: UploadObjectModel): Boolean{
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                obj.deleteFromRealm()
            }
        }
        return true
    }

}