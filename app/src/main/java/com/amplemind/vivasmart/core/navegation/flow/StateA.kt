package com.amplemind.vivasmart.core.navegation.flow

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.core.navegation.AppFlow

class StateA(val appFlow: AppFlow): State {

    override fun changeToPreviousState() {
        // TODO
    }

    override fun startActivity(context: Context) {
        // TODO
    }

    override fun getFragment() {
        // TODO
    }

    override fun doSomeAwesomeThing(param: String) {
        Log.d("StateA", "doing something awesome with $param")
        // YOU CAN EVEN CHANGE THE APP STATE FROM EVERY SINGLE STATE.. !!
        appFlow.state = StateB(this, appFlow)
    }

    override fun getSomeAwesomeThing(): String {
        return "getting something awesome from StateA"
    }
}