package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_16 : MigrationExecutable {
    override fun run(realm: DynamicRealm) {
        migrateUploadObjectModel(realm)
    }

    private fun migrateUploadObjectModel(realm: DynamicRealm){
        realm.schema.get("ValveFertirriego")?.apply {
            addField("valveExpense", Double::class.javaObjectType)
        }

        realm.schema.get("ValveForTable")?.apply {
            addField("m3ha", Int::class.javaObjectType)
            addField("minutes", Double::class.javaObjectType)
            addField("pulsesNo", Int::class.javaObjectType)
        }

        realm.schema.get("PulseForTable")?.apply {
            addField("isCalculated", Boolean::class.javaObjectType)
        }
    }

    override fun drop(realm: DynamicRealm) {

    }
}