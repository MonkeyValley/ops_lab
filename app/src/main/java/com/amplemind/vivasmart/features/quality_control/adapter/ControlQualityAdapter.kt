package com.amplemind.vivasmart.features.quality_control.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemControlQualityBinding
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlQualityItemViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

class ControlQualityAdapter : RecyclerView.Adapter<ControlQualityAdapter.ControlQualityViewHolder>() {

    private var list = mutableListOf<ControlQualityItemViewModel>()

    var listener: Listener? = null

    private val clickSubject = PublishSubject.create<ControlQualityItemViewModel>()

    private var mRecyclerView: RecyclerView? = null

    fun addElements(data: List<ControlQualityItemViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ControlQualityViewHolder {
        val binding = ItemControlQualityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ControlQualityViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ControlQualityViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem() = clickSubject

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        clearDisposables()
        mRecyclerView = null
    }

    private fun clearDisposables() {
        for (i in 0 until itemCount) {
            (mRecyclerView?.findViewHolderForAdapterPosition(i) as ControlQualityViewHolder?)
                    ?.clearDisposables()
        }
    }

    inner class ControlQualityViewHolder(private val binding: ItemControlQualityBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ControlQualityItemViewModel

        private var counterDisposable: Disposable? = null

        fun clearDisposables() {
            if (counterDisposable?.isDisposed == false) {
                counterDisposable?.dispose()
            }
        }

        fun bind(item: ControlQualityItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            clearDisposables()

            val tvCounter = binding.root.findViewById<TextView>(R.id.tv_counter)

            item.counter = listener?.onGetCounterValue(item.stage, adapterPosition)

            if (item.counter == null) {
                tvCounter.visibility = View.GONE
            }
            else {
                tvCounter.bringToFront()
                counterDisposable = item.counter?.subscribe({value ->
                        tvCounter.text = value.toString()
                    }, {
                        tvCounter.text = "N/D"
                    }
                )
            }

            binding.root.setOnClickListener {
                item.position = adapterPosition
                clickSubject.onNext(this.item)
            }
        }

    }

    interface Listener {
        fun onGetCounterValue(stage: StageModel, position: Int): Observable<Int>?
    }

}
