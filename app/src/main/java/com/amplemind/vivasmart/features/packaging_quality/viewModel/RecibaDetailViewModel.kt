package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.amplemind.vivasmart.core.repository.response.UnitsDataResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.haulage.viewModel.CarryOrderViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.WeightRestriction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import java.io.File
import javax.inject.Inject

class RecibaDetailViewModel @Inject constructor(private val repository : PackagingQualityRepository,
                                                private val uploadRepository: ProfileRepository,
                                                private val preferences: UserAppPreferences,
                                                private val apiErrors: HandleApiErrors) {

    private val getCarryOrder = BehaviorSubject.create<CarryOrderViewModel>()
    private val requestGetUnits = BehaviorSubject.create<List<SpinnerUnitsViewModel>>()
    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val successUploadSign: BehaviorSubject<MutableList<File>> = BehaviorSubject.create()
    private val updateOrder = BehaviorSubject.create<Boolean>()
    private val updateSigns = BehaviorSubject.create<Boolean>()
    private val signs = mutableListOf<String>()
    private val listUnits = mutableListOf<SpinnerUnitsViewModel>()

    var varieties: List<VarietyModel> = listOf()
    var newBoxCounts: List<VarietyBoxCount> = listOf()
    var newSecondQuality: Double = 0.0
    var newTotalBoxes: Double = 0.0
    var cropId: Long = 0L

    var carryOrderBoxSize: Double = 0.0
        private set

    fun getCarryOrder(id: Int): Disposable {
        return repository.getItemQualityPackages(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {models ->
                    val carryOrder = models.data.map { CarryOrderViewModel(it) }.first()

                    newBoxCounts = carryOrder.boxCounts
                    newSecondQuality = carryOrder.secondQualityCount
                    newTotalBoxes = carryOrder.boxSizeNumber

                    varieties = carryOrder.boxCounts.mapNotNull {
                        val variety = it.variety
                        it.variety = null
                        return@mapNotNull variety
                    }

                    return@map carryOrder
                }
                .subscribe({
                    carryOrderBoxSize = it.boxSizeNumber
                    getCarryOrder.onNext(it)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }


    fun getUnits(): Disposable {
        return repository.getUnits()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    mapUnits(it)
                }
                .subscribe({
                    listUnits.clear()
                    listUnits.addAll(it)
                    requestGetUnits.onNext(listUnits)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getWeightRestriction(): Disposable {
        return repository.getWeightRestriction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    updateWeightRestriction(it)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun updateWeightRestriction(list: List<WeightRestriction>){
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm->
                realm.delete(WeightRestriction::class.java)
                realm.insert(list)
            }
        }
    }

    fun updateCarryOrder(boxNo: Double, boxCounts: List<VarietyBoxCount>, secondQualityCount: Double, weigth: Double, temperature: Double, unitsId: Int, carryOrderId: Int): Disposable {
        return repository.updateCarryOrder(boxNo, boxCounts, secondQualityCount, weigth, temperature, unitsId, carryOrderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    updateOrder.onNext(true)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun uploadSign(files: MutableList<File>): Disposable {
        return uploadRepository.uploadImage(files.first(), "sign")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    if (files.isNotEmpty()) {
                        files.removeAt(0)
                    }
                    signs.add(it.relative_url)
                    successUploadSign.onNext(files)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun updateSigns(carryOrderId: Int): Disposable {
        return repository.updateSigns(signs[0], signs[1], carryOrderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    updateSigns.onNext(true)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getUnitId(position: Int): Int {
        if (position >= listUnits.size) return -1
        return listUnits[position].id.toInt()
    }

    fun isValidForm(boxNo: String, boxNoActive: Boolean, weigth: String, unit: Int, temperature: String): Boolean {
        /*if (boxNoActive && boxNo.toDoubleOrNull() == null) {
            requestFail.onNext("Ingrese el número de cajas")
            return false
        }*/
        /*
        if (weigth.toDoubleOrNull() == null) {
            requestFail.onNext("Ingrese el peso promedio")
            return false
        }
        */
        /*
        if (unit <= -1) {
            requestFail.onNext("Ingrese la unidad")
            return false
        }
        */
        if (temperature.toDoubleOrNull() == null) {
            requestFail.onNext("Ingrese la temperatura")
            return false
        }
        return true
    }

    private fun mapUnits(response: UnitsDataResponse): List<SpinnerUnitsViewModel> {
        val units = mutableListOf<SpinnerUnitsViewModel>()
        response.data.map { data ->
            data.units.map { unitsList ->
                units.add(SpinnerUnitsViewModel(unitsList.unit))
            }
        }
        return units
    }

    fun onRequestSuccess(): BehaviorSubject<CarryOrderViewModel> {
        return getCarryOrder
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestGetUnits(): BehaviorSubject<List<SpinnerUnitsViewModel>> {
        return requestGetUnits
    }

    fun onSuccessUpdateSign(): BehaviorSubject<MutableList<File>> {
        return successUploadSign
    }

    fun onUpdateCarryOrder(): BehaviorSubject<Boolean> {
        return updateOrder
    }

    fun onUpdateSigns(): BehaviorSubject<Boolean> {
        return updateSigns
    }

}