package com.amplemind.vivasmart.vo_core.repository.dao.realm

import android.util.Log
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.repository.remote.utils.Optional
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.crashlytics.android.Crashlytics
import io.reactivex.*
import io.reactivex.rxkotlin.toMaybe
import io.realm.*
import io.realm.rx.CollectionChange

open class RealmDao<Model: RealmObject> constructor (
        private val mRealmHolder: RealmHolder,
        protected val modelClass: Class<Model>
) {

    companion object {

        val TAG: String = RealmDao::class.java.simpleName

    }


    fun getRealm() =
            mRealmHolder.getRealm()

    fun cleanUp() =
            mRealmHolder.close()

    protected fun query(realm: Realm = getRealm()): RealmQuery<Model> = realm.where(modelClass)

    fun findAllLiveAsync(): Observable<Changes<Model>> =
            findAllLiveAsync(this::query)

    fun findAll(snapshot: Boolean = false): Single<List<Model>> =
            findAll(this::query, snapshot)

    fun findAllAsList(snapshot: Boolean = false): List<Model> =
            query().findAll()
                    .run {
                        if (snapshot) {
                            realm.copyFromRealm(this)
                        }
                        else {
                            this
                        }
                    }

    fun findById(id: Long, snapshot: Boolean = false): Maybe<Model> =
            findFirst({realm ->
                query(realm)
                        .equalTo("id", id)
            }, snapshot)

    fun findByStageId(stageId: Int, snapshot: Boolean = false): Maybe<Model> =
            findFirst({ realm ->
                query(realm)
                        .equalTo("id", stageId)
            }, snapshot)

    fun findByUuid(uuid: String, snapshot: Boolean = false): Maybe<Model> =
            findFirst({ realm ->
                query(realm)
                        .equalTo("uuid", uuid)
            }, snapshot)

    fun findByUuidLiveAsync(uuid: String): Flowable<Model> =
            findFirstLiveAsync { realm ->
                query(realm)
                        .equalTo("uuid", uuid)
            }

    fun create( item: Model, after: ((Realm, List<Model>) -> Unit)?): Single<Model> =
            create(listOf(item), after)
                    .map {items ->
                        items.first()
                    }

    fun create( items: List<Model>, after: ((Realm, List<Model>) -> Unit)?): Single<List<Model>> =
            executeListTransactionAsync {realm ->
                realm.insertOrUpdate(items)
                after?.invoke(realm, items)
                return@executeListTransactionAsync items
            }

    fun create(func: (Realm, Model) -> Unit, after: ((Realm, Model) -> Unit)?): Single<Model> =
            create(1, { realm, _, item ->
                func.invoke(realm, item)
            }, { realm, items ->
                after?.invoke(realm, items.first())
            }).map { items ->
                items.first()
            }

    fun create( count: Int, func: (Realm, Int, Model) -> Unit, after: ((Realm, List<Model>) -> Unit)?): Single<List<Model>> {
        val items = mutableListOf<Model>()
        return executeListTransactionAsync { realm ->
            for (i in 0 until count) {
                val item = realm.createObject(modelClass, getUUID())
                func.invoke(realm, i, item)
                items.add(realm.copyFromRealm(item))
            }
            after?.invoke(realm, items)
            return@executeListTransactionAsync items
        }
    }

    fun update(item: Model, func: ((Realm, Model) -> Unit)?, after: ((Realm, Model) -> Unit)?): Single<Model> =
            update(listOf(item), { realm, _, obj ->
                func?.invoke(realm, obj)
            }, { realm, items ->
                after?.invoke(realm, items.first())
            }).map { items ->
                items.first()
            }

    fun update(items: List<Model>, func: ((Realm, Int, Model) -> Unit)?, after: ((Realm, List<Model>) -> Unit)?): Single<List<Model>> =
            updateByUuid(getUuids(items), func, after)

    fun updateByUuid(uuid: String, func: ((Realm, Model) -> Unit)?, after: ((Realm, Model) -> Unit)?): Single<Model> =
            updateByUuid(listOf(uuid), { realm, _, item ->
                func?.invoke(realm, item)
            }, { realm, items ->
                after?.invoke(realm, items.first())
            }).map {items ->
                items.first()
            }

    fun updateByUuid(uuids: List<String>, func: ((Realm, Int, Model) -> Unit)?, after: ((Realm, List<Model>) -> Unit)?): Single<List<Model>> {
        return executeListTransactionAsync { realm ->
            val auxItems = mutableListOf<Model>()
            val copy = mutableListOf<Model>()

            for ((index, uuid) in uuids.iterator().withIndex()) {

                val item = query(realm)
                        .equalTo("uuid", uuid)
                        .findFirst()
                        ?: throw IllegalStateException("${modelClass::class.java.simpleName} with uuid: $uuid does not exists")

                Log.e(TAG, "ITEM WITH UUID: $uuid UPDATED")

                auxItems.add(item)
                copy.add(realm.copyFromRealm(item))

                func?.invoke(realm, index, item)

            }
            after?.invoke(realm, auxItems)
            return@executeListTransactionAsync copy
        }
    }

    fun delete(item: Model, func: (Realm, Model) -> Unit, after: ((Realm, Model) -> Unit)?): Single<Model> =
            delete(listOf(item), { realm, _, currentItem ->
                func.invoke(realm, currentItem)
            }, { realm, items ->
                after?.invoke(realm, items.first())
            }).map { items ->
                items.first()
            }

    fun delete(items: List<Model>, func: (Realm, Int, Model) -> Unit, after: ((Realm, List<Model>) -> Unit)?): Single<List<Model>> =
            deleteByUuid(getUuids(items), func, after)

    fun deleteByUuid(uuid: String, func: ((Realm, Model) -> Unit)?, after: ((Realm, Model) -> Unit)?): Single<Model> =
            deleteByUuid(listOf(uuid), { realm, _, item ->
                func?.invoke(realm, item)
            }, { realm, items ->
                after?.invoke(realm, items.first())
            }).map {items ->
                items.first()
            }

    fun deleteByUuid(uuids: List<String>, func: ((Realm, Int, Model) -> Unit), after: ((Realm, List<Model>) -> Unit)?): Single<List<Model>> {

        val copy = mutableListOf<Model>()

        return executeListTransactionAsync { realm ->

            val auxItems = query(realm)
                    .`in`("uuid", uuids.toTypedArray())
                    .findAll()

            for ((index, item) in auxItems.iterator().withIndex()) {
                func.invoke(realm, index, item)
                copy.add(realm.copyFromRealm(item))
                item.deleteFromRealm()
            }

            after?.invoke(realm, copy)

            return@executeListTransactionAsync copy
        }
    }

    protected fun <T> executeListTransaction (func: (Realm) -> List<T>): Single<List<T>> =
            Single.create { emitter: SingleEmitter<List<T>> ->
                var items: List<T> = listOf()
                try {
                    getRealm().executeTransaction { realm ->
                        items = func.invoke(realm)
                    }
                    emitter.onSuccess(items)
                }
                catch (error: Throwable) {
                    emitter.onError(error)
                }
            }
            .doOnError { error ->
                Log.e(TAG, Log.getStackTraceString(error))
                Crashlytics.logException(error)
            }

    protected fun <T> executeTransaction (func: (Realm) -> T): Single<T> =
            executeListTransaction { realm ->
                    listOf(func.invoke(realm))
            }
            .map { results ->
                results.firstOrNull()
            }

    protected fun <T> executeListTransactionAsync (func: (Realm) -> List<T>): Single<List<T>> =
            Single.create { emitter: SingleEmitter<List<T>> ->
                var result = listOf<T>()
                getRealm().executeTransactionAsync ({ realm ->
                    result = func.invoke(realm)
                }, {
                    emitter.onSuccess(result)
                }, { error ->
                    emitter.onError(error)
                })
            }
            .doOnError { error ->
                Log.e(TAG, Log.getStackTraceString(error))
                Crashlytics.logException(error)
            }

    protected fun <T> executeTransactionAsync (func: (Realm) -> T): Single<T> =
            executeListTransactionAsync { realm ->
                listOf(func.invoke(realm))
            }.map { results ->
                results.firstOrNull()
            }

    protected fun findFirst (func: (Realm) -> RealmQuery<Model>, snapshot: Boolean = false): Maybe<Model> =
            func.invoke(getRealm())
                    .findFirst()
                    .toMaybe()
                    .map { obj ->
                        if (snapshot && obj.isManaged) {
                            obj.realm.copyFromRealm(obj)
                        }
                        else {
                            obj
                        }
                    }

    protected fun findFirstAsync (func: (Realm) -> RealmQuery<Model>, snapshot: Boolean = false): Maybe<Model> =
            func.invoke(getRealm())
                    .findFirstAsync()
                    .asFlowable<Model>()
                    .filter { item ->
                        item.isLoaded
                    }
                    .firstElement()
                    .map { obj ->
                        if (snapshot && obj.isManaged) {
                            obj.realm.copyFromRealm(obj)
                        }
                        else {
                            obj
                        }
                    }

    protected fun findFirstLive (func: (Realm) -> RealmQuery<Model>): Flowable<Optional<Model>> =
            func.invoke(getRealm())
                    .limit(1)
                    .findAll()
                    .asFlowable()
                    .map { results ->
                        Optional(results.firstOrNull())
                    }

    protected fun findFirstLiveAsync (func: (Realm) -> RealmQuery<Model>) =
        func.invoke(getRealm())
                .findFirstAsync()
                .asFlowable<Model>()
                .filter { obj ->
                    obj.isLoaded
                }

    protected fun findAll (func: (Realm) -> RealmQuery<Model>, snapshot: Boolean): Single<List<Model>> =
            Single.fromCallable {
                func.invoke(getRealm())
                        .findAll()
            }.map { results ->
                if (snapshot && results.isManaged) {
                    results.realm.copyFromRealm(results)
                }
                else {
                    results
                }
            }

    protected fun findAllAsync(func: (Realm) -> RealmQuery<Model>, snapshot: Boolean = false): Single<List<Model>> =
            func.invoke(getRealm())
                    .findAllAsync()
                    .asFlowable()
                    .map { results ->
                        if (snapshot && results.isManaged) {
                            results.realm.copyFromRealm(results)
                        }
                        else {
                            results
                        }
                    }
                    .first(listOf())

    protected fun findAllLive(func: (Realm) -> RealmQuery<Model>): Flowable<Changes<Model>> =
            func.invoke(getRealm())
                    .findAll()
                    .asChangesetObservable()
                    .filter { changes ->
                        changes.collection.isLoaded
                    }
                    .map { results ->
                        mapChanges(results)
                    }
                    .toFlowable(BackpressureStrategy.LATEST)

    protected fun findAllLiveAsync(func: (Realm) -> RealmQuery<Model>): Observable<Changes<Model>> =
            func.invoke(getRealm())
                    .findAllAsync()
                    .asChangesetObservable()
                    .map { results ->
                        mapChanges(results)
                    }

    private fun getUuids(objs: List<Model>) =
            objs.mapNotNull { obj ->
                getUuid(obj)
            }


    private fun getUuid(obj: Model): String? =
            if (obj is Identifiable) {
                obj.uuid
            }
            else {
                null
            }

    private fun <T: RealmObject> mapChanges(changes: CollectionChange<RealmResults<T>>): Changes<T> {

        val insertions = mutableListOf<Changes.Range>()
        val updates = mutableListOf<Changes.Range>()
        val deletions = mutableListOf<Changes.Range>()

        changes.changeset?.insertionRanges?.forEach {range ->
            insertions.add(Changes.Range(range.startIndex, range.length))
        }

        changes.changeset?.changeRanges?.forEach {range ->
            updates.add(Changes.Range(range.startIndex, range.length))
        }

        changes.changeset?.deletionRanges?.forEach {range ->
            deletions.add(Changes.Range(range.startIndex, range.length))
        }

        val state = when (changes.changeset?.state) {
            OrderedCollectionChangeSet.State.INITIAL -> Changes.State.INITIAL
            OrderedCollectionChangeSet.State.UPDATE -> Changes.State.UPDATE
            else -> Changes.State.ERROR
        }

        val data = changes.collection as List<T>

        return Changes( insertions, updates, deletions, data, state )
    }



}