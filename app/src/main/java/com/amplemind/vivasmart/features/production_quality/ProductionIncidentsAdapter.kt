package com.amplemind.vivasmart.features.production_quality

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemProductionIncidentBinding
import com.amplemind.vivasmart.features.adapters.SwipableViewHolder

class ProductionIncidentsAdapter constructor(val context: Context) : RecyclerView.Adapter<ProductionIncidentsAdapter.ProductionIncidentsViewHolder>() {

    var items = mutableListOf<IncidentItemViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductionIncidentsViewHolder {
        val binding = ItemProductionIncidentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductionIncidentsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(viewHolder: ProductionIncidentsViewHolder, pos: Int) {
        viewHolder.bind(items.get(pos))
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.toLong()
    }

    fun addElements(data: List<IncidentItemViewModel>) {
        items = data as MutableList<IncidentItemViewModel>
        notifyDataSetChanged()
    }

    fun deleteElement(pos: Int) {
        items.removeAt(pos)
        notifyItemRemoved(pos)
    }

    inner class ProductionIncidentsViewHolder constructor(val binding: ItemProductionIncidentBinding ) : RecyclerView.ViewHolder(binding.root), SwipableViewHolder {
        override val viewForeground: View?
            get() = binding.viewForeground
        override val viewDelete: View?
            get() = null
        override val viewFinish: View?
            get() = binding.viewFinish

        fun bind (item: IncidentItemViewModel) {
            binding.setVariable(BR.model, item)
            binding.executePendingBindings()
        }

    }
}