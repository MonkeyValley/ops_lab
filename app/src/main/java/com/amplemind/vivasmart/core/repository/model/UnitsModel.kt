package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class UnitsList(@SerializedName("unit") val unit: UnitModel)

@Entity
data class UnitModel(@SerializedName("name") val name: String,
                     @SerializedName("id") @PrimaryKey(autoGenerate = false) val id: Long,
                     @SerializedName("image") val image: String?)