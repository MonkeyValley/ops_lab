package com.amplemind.vivasmart.vo_features.adapters.recyclerview

import android.util.Log
import android.view.View
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

//NOTE: To avoid unnecessary memory consumption, just use this class when handling states for
//      large collections of data. If no particular states where saved, functions like forEachViewModels
//      and mapViewModels will iterate the entire generated viewmodel list
abstract class StatefulAdapter<
        T: Any,
        VM: StatefulAdapter.ViewModel<T>,
        VH: StatefulAdapter.ViewHolder<VM>> : BaseAdapter<T, VM, VH>() {

    companion object {
        private val TAG = StatefulAdapter::class.java.simpleName
    }

    private var mStateViewModels = HashMap<String, VM>()

    private val mOnSaveState = PublishSubject.create<ViewHolder.OnSaveStateEvent<VM>>()

    private var mOnSaveStateDisposable: Disposable? = null

    init {
        mOnSaveStateDisposable = mOnSaveState.subscribe(::onSaveState)
    }

    override fun onViewRecycled(holder: VH) {
        holder.checkIfShouldSaveState()
        super.onViewRecycled(holder)
    }

    override fun onBindViewHolder(viewHolder: VH, position: Int) {
        super.onBindViewHolder(viewHolder, position)

        if (viewHolder.onSaveState == null) {
            viewHolder.onSaveState = mOnSaveState
        }

        viewHolder.checkIfShouldSaveState()
    }

    override fun getViewModel(position: Int, createIfNotExists: Boolean): VM? {
        var viewModel = super.getViewModel(position, false)

        if (viewModel == null) {
            viewModel = mStateViewModels[getItemKey(get(position))]
            if (viewModel != null) {
                Log.e(TAG, "View model in position $position has state")
                mViewModels.add(viewModel)
            }
            else if (createIfNotExists) {
                Log.e(TAG, "Could not find view model with state, creating...")
                viewModel = buildViewModelAndBind(position)
            }
        }

        return viewModel
    }

    override fun buildViewModelAndBind(position: Int): VM {
        val viewModel = super.buildViewModelAndBind(position)
        setDefaultValues(viewModel)
        return viewModel
    }

    override fun onDeletedViewModels(viewModels: List<VM>) {
        super.onDeletedViewModels(viewModels)

        for (viewModel in viewModels) {
            if (viewModel.hasState) {
                viewModel.hasState = false
                mStateViewModels.remove(getItemKey(viewModel.item))
            }
        }
    }

    protected fun getStateViewModel(item: T) =
        mStateViewModels[getItemKey(item)]

    override fun forEachViewModel(func: (VM) -> Unit) =
            if (mStateViewModels.isNotEmpty()) {
                forEachViewModel(mStateViewModels.iterator(), { it.value }, func)
            }
            else {
                super.forEachViewModel(func)
            }

    override fun <T> mapViewModels(func: (VM) -> T?): List<T> =
        if (mStateViewModels.isNotEmpty()) {
            mapViewModels(mStateViewModels.iterator(), mStateViewModels.size, { it.value }, func)
        }
        else {
            super.mapViewModels(func)
        }

    override fun clearViewModels() {
        clearStateViewModels()
        super.clearViewModels()
    }

    fun clearStateViewModels() {
        mStateViewModels = HashMap()
    }

    override fun cleanUp() {
        super.cleanUp()
        mOnSaveStateDisposable?.dispose()
    }

    open fun setDefaultValues(viewModel: VM) {}

    private fun onSaveState(event: ViewHolder.OnSaveStateEvent<VM>) {
        event.viewModel.hasState = event.save
        if (event.save) {
            mStateViewModels[event.viewModel.key] = event.viewModel
            Log.i(TAG, "State item inserted key: ${event.viewModel.key}, Map: ${mStateViewModels.size}")
        }
        else {
            mStateViewModels.remove(event.viewModel.key)
            Log.i(TAG, "State item delete key: ${event.viewModel.key}, Map: ${mStateViewModels.size}")
        }
    }


    abstract class ViewHolder<VM: ViewModel<*>>(
            itemView: View
    ) : BaseAdapter.ViewHolder<VM>(itemView) {

        internal var onSaveState: PublishSubject<OnSaveStateEvent<VM>>? = null

        fun checkIfShouldSaveState() {
            if (viewModel.contentsChanged) {
                onSaveState?.onNext(OnSaveStateEvent(
                        viewModel,
                        adapterPosition,
                        shouldSave()
                ))
                viewModel.contentsChanged = false
            }
        }

        open fun shouldSave(): Boolean = false

        data class OnSaveStateEvent<VM: ViewModel<*>>(
                val viewModel: VM,
                val position: Int,
                val save: Boolean
        )

    }

    abstract class ViewModel<T: Any>(item: T) : BaseAdapter.ViewModel<T>(item) {

        internal var hasState: Boolean = false
        internal var contentsChanged: Boolean = false

    }

}