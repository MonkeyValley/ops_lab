package com.amplemind.vivasmart.features.menus

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel
import com.amplemind.vivasmart.core.repository.model.PayrollMenuModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.AppModule
import io.realm.Realm

fun getMenuPayroll(): List<PayrollMenuModel> {
    val data = mutableListOf<PayrollMenuModel>()

    if (moduleHasAccess(1)
            || moduleHasAccess(2)
            || moduleHasAccess(26)
            || moduleHasAccess(27)
            || moduleHasAccess(28)
    ) data.add(PayrollMenuModel("Nómina", R.drawable.ic_nomina_color, true))
    else data.add(PayrollMenuModel("Nómina", R.drawable.ic_nomina_color, false))

    if (moduleHasAccess(3)
            || moduleHasAccess(4)
            || moduleHasAccess(5)
            || moduleHasAccess(13)
            || moduleHasAccess(14)
            || moduleHasAccess(15)
    ) data.add(PayrollMenuModel("Calidad", R.drawable.ic_quality_color, true))
    else data.add(PayrollMenuModel("Calidad", R.drawable.ic_quality_color, false))

    if (moduleHasAccess(16)) data.add(PayrollMenuModel("MIPE", R.drawable.ic_mipe_color, true))
    else data.add(PayrollMenuModel("MIPE", R.drawable.ic_mipe_color))

    if (moduleHasAccess(17)
            || moduleHasAccess(18)
            || moduleHasAccess(19)
    ) data.add(PayrollMenuModel("Fertirriego", R.drawable.ic_fertirriego_color, true))
    else data.add(PayrollMenuModel("Fertiriego", R.drawable.ic_fertirriego_color, false))

    if (moduleHasAccess(20)
            || moduleHasAccess(21)
            || moduleHasAccess(22)
    ) data.add(PayrollMenuModel("Fenología", R.drawable.ic_phenology_color, true))
    else data.add(PayrollMenuModel("Fenología", R.drawable.ic_phenology_color, false))

    if (moduleHasAccess(23)
            || moduleHasAccess(25)
    ) data.add(PayrollMenuModel("Operaciones", R.drawable.ic_operaciones_color, true))
    else data.add(PayrollMenuModel("Operaciones", R.drawable.ic_operaciones_color, false))

    return data
}

fun getActivitiesPayrollChooseLines(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()

    if (moduleHasAccess(2)) data.add(PayrollActivitiesModel("1321JK", "Líneas", R.drawable.ic_lines_color, true))
    else data.add(PayrollActivitiesModel("1321JK", "Líneas", R.drawable.ic_lines_color))

    data.add(PayrollActivitiesModel("1028HGHJ", "Calidad", R.drawable.ic_quality_color))
    data.add(PayrollActivitiesModel("10GSKdqwL", "Cartón", R.drawable.ic_carton_color))
    data.add(PayrollActivitiesModel("10GSKasL", "Inocuidad", R.drawable.ic_inocuidad_color))
    data.add(PayrollActivitiesModel("10GSKawsL", "Embarque", R.drawable.ic_embarque_color))
    data.add(PayrollActivitiesModel("10GSwdKL", "General", R.drawable.ic_general_color))
    return data
}

fun getActivitiesPayroll(all: Boolean): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()

    data.add(PayrollActivitiesModel("12213SA", "Producción", R.drawable.ic_production_color, true))
    data.add(PayrollActivitiesModel("123123ED", "Empaque", R.drawable.ic_paking_color, true))

    if (all) {
        data.add(PayrollActivitiesModel("189272HJ", "General", R.drawable.ic_general_color))
    }

    return data
}

fun getActivitiesPhenology(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()

    if (moduleHasAccess(20))
    data.add(PayrollActivitiesModel("1", "Variedades Comerciales", R.drawable.ic_production_color, true))
    else
        data.add(PayrollActivitiesModel("1", "Variedades Comerciales", R.drawable.ic_production_color))

    if (moduleHasAccess(21))
    data.add(PayrollActivitiesModel("0", "Variedades de Pruebas", R.drawable.ic_test_color, true))
    else
        data.add(PayrollActivitiesModel("0", "Variedades de Pruebas", R.drawable.ic_test_color))

    if (moduleHasAccess(22))
    data.add(PayrollActivitiesModel("2", "Polinización", R.drawable.ic_pollition_color, true))
    else
        data.add(PayrollActivitiesModel("2", "Polinización", R.drawable.ic_pollition_color))

    return data
}

fun getActivitiesPayrollProduction(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()

    if (moduleHasAccess(1)) data.add(PayrollActivitiesModel("1321JHJK", "Labores Culturales", R.drawable.ic_labores_color, true))
    else data.add(PayrollActivitiesModel("1321JHJK", "Labores Culturales", R.drawable.ic_labores_color))

    if (moduleHasAccess(26)) data.add(PayrollActivitiesModel("12730GSKL", "Fertiriego", R.drawable.ic_fertirriego_color, true))
    else data.add(PayrollActivitiesModel("12730GSKL", "Fertiriego", R.drawable.ic_fertirriego_color))

    if (moduleHasAccess(27)) data.add(PayrollActivitiesModel("14082FJKA", "Mantenimiento", R.drawable.ic_mantenimiento, true))
    else data.add(PayrollActivitiesModel("14082FJKA", "Mantenimiento", R.drawable.ic_mantenimiento))

    if (moduleHasAccess(28)) data.add(PayrollActivitiesModel("109128HGHJ", "MIPE", R.drawable.ic_mipe_color, true))
    else data.add(PayrollActivitiesModel("109128HGHJ", "MIPE", R.drawable.ic_mipe_color))

    return data
}

fun getActivitiesPackingQuality(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()

//    data.add(PayrollActivitiesModel("12213SA", "Calidad en empaque", R.drawable.ic_box_reciba, true))
//    data.add(PayrollActivitiesModel("123123ED", "Reciba", R.drawable.ic_control_producc, true))
//    data.add(PayrollActivitiesModel("123123SE", "Reporte de calidad", R.drawable.ic_star_menu, true))

    if (moduleHasAccess(5))
    data.add(PayrollActivitiesModel("123123FR", "Validación de cosecha", R.drawable.ic_crop_validation_color, true))
    else
        data.add(PayrollActivitiesModel("123123FR", "Validación de cosecha", R.drawable.ic_crop_validation_color))

    if (moduleHasAccess(13))
    data.add(PayrollActivitiesModel("123123FS", "Calidad Reciba", R.drawable.ic_box_reciba_color, true))
    else
        data.add(PayrollActivitiesModel("123123FS", "Calidad Reciba", R.drawable.ic_box_reciba_color))

    if (moduleHasAccess(14))
    data.add(PayrollActivitiesModel("123123FP", "Calidad de producto terminado", R.drawable.ic_quiality_finally_product_color, true))
    else
        data.add(PayrollActivitiesModel("123123FP", "Calidad de producto terminado", R.drawable.ic_quiality_finally_product_color))

    if (moduleHasAccess(15))
    data.add(PayrollActivitiesModel("123123FQ", "Control de desperdicio", R.drawable.ic_waste_controll_color, true))
    else
        data.add(PayrollActivitiesModel("123123FQ", "Control de desperdicio", R.drawable.ic_waste_controll_color))

    return data
}

fun getActivitiesOperation(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()

    if (moduleHasAccess(23)) data.add(PayrollActivitiesModel("123123ED", "Reciba", R.drawable.ic_reciba_color, true))
    else data.add(PayrollActivitiesModel("123123ED", "Reciba", R.drawable.ic_reciba_color))

    if(moduleHasAccess(25)) data.add(PayrollActivitiesModel("123123EF", "Rangos de Producción", R.drawable.ic_production_ranges_color, true))
    else data.add(PayrollActivitiesModel("123123EF", "Rangos de Producción", R.drawable.ic_production_ranges_color, false))

    return data
}

fun getActivitiesFertirriego(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()

    if (moduleHasAccess(17))
    data.add(PayrollActivitiesModel("0", "Monitoreo", R.drawable.ic_monitoring_color, true))
    else
        data.add(PayrollActivitiesModel("0", "Monitoreo", R.drawable.ic_monitoring_color))

    if (moduleHasAccess(18))
    data.add(PayrollActivitiesModel("1", "Fertiriego – CE y pH", R.drawable.ic_phce_color, true))
    else
        data.add(PayrollActivitiesModel("1", "Fertiriego – CE y pH", R.drawable.ic_phce_color))

    if (moduleHasAccess(19))
    data.add(PayrollActivitiesModel("2", "Reporte", R.drawable.ic_control_producc_color, true))
    else
        data.add(PayrollActivitiesModel("2", "Reporte", R.drawable.ic_control_producc_color))

    return data
}

fun getActivitiesMipe(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()
    if (moduleHasAccess(16)) data.add(PayrollActivitiesModel("0", "Monitoreo", R.drawable.ic_monitoring_color, true))
    else data.add(PayrollActivitiesModel("0", "Monitoreo", R.drawable.ic_monitoring_color))
    return data
}

fun getProductionFromQualityMenu(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()

    if (moduleHasAccess(3))
    data.add(PayrollActivitiesModel("12213SA", "Labores", R.drawable.ic_activitys_color, true))
    else
        data.add(PayrollActivitiesModel("12213SA", "Labores", R.drawable.ic_activitys_color))

    if (moduleHasAccess(4))
    data.add(PayrollActivitiesModel("123123ED", "Control", R.drawable.ic_control_producc_color, true))
    else
        data.add(PayrollActivitiesModel("123123ED", "Control", R.drawable.ic_control_producc_color, false))

    return data
}

fun moduleHasAccess(moduleId: Int): Boolean {
    Realm.getDefaultInstance().use { realm ->
        val result = realm.where(AppModule::class.java)
                .equalTo("moduleId", moduleId)
                .findFirst()

        return if(result != null) result.hasAccess!!
        else false
    }
}
