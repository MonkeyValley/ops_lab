package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderDetailResult

class ItemQualifiedViewModel(val order: QualityCarryOrderDetailResult) {

    val folio = order.carry_order.folio
    val evaluation = order.evaluation ?: false
    val tablas = getTables()

    val id_carry = order.id

    private fun getTables(): String {
        var tables = ""
        order.carry_order.table?.forEachIndexed { index, table ->
            if (index == (order.carry_order.table.size - 1)) tables += table else tables += "$table,"
        }
        return tables
    }

}
