package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.response.IssuesModel

class IssuesItemViewModel(issue: IssuesModel? = null, photos: List<String>? = null, type: Int, val id_carry: Int, evaluation: Boolean = false, comment: String = "") {

    val typeHolder = type

    val name = (issue?.name ?: "")
    val numberIssue = (issue?.count ?: "").toString()
    val percentage = (issue?.percent_sample ?: 0.0).toString()

    val photosIssues = photos

    var evaluationServer = evaluation
    var evaluationText = if (evaluation) "Buena" else "Mala"
    var commentServer = comment

    var evaluationResult = false
    var commentsIssue = ""

}

