package com.amplemind.vivasmart.core.repository.response

import com.google.gson.annotations.SerializedName

data class LotsResponse(
        @SerializedName("data") val data: List<LotsResponseResult>,
        @SerializedName("total_count") val total_count: Int
)

data class LotsResponseResult(
        @SerializedName("id") val id: Int,
        @SerializedName("business_unit_id") val business_unit_id: Int,
        @SerializedName("code") val code: Int? = null,
        @SerializedName("crop_id") val crop_id: Int? = null,
        @SerializedName("cycle_id") val cycle_id: Int? = null,
        @SerializedName("lot") val lot: com.amplemind.vivasmart.core.repository.model.LotModel? = null
)
