package com.amplemind.vivasmart.features.phenology.viewmodel

import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVarieties

class VarietiesPhenologyItemViewModel(phenology_varieties: PhenologyVarieties){
    var variety_id = phenology_varieties.variety_id
    var variety_name = phenology_varieties.variety_name
    var tables = phenology_varieties.tables
    var has_cluster = phenology_varieties.has_cluster

//    fun getTables(): List<PhenologyTable>{
//        return tables
//    }
}


