package com.amplemind.vivasmart.core.dagger.module.fragment

import com.amplemind.vivasmart.core.base.dagger.scope.FragmentScope
import com.amplemind.vivasmart.features.production_roster.fragments.HarvestBoxCountByVarietyDialog
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HaulageFragmentProvider {
    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindHarvestBoxCountByVarietyDialog(): HarvestBoxCountByVarietyDialog
}
