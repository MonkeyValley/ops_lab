package com.amplemind.vivasmart.core.repository.model

import androidx.room.*
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class DetailtRosterResponse(
        @Embedded @SerializedName("activity_code") val activity_code: ActivityCodeDetail?,
        @SerializedName("activity_code_collaborator_id") val activityCodeCollaboratorId: Int?,
        @SerializedName("activity_code_id") val activityCodeId: Int?,
        @SerializedName("collaborator_id") val collaboratorId: Int?,
        @SerializedName("date") val date: String?,
        @SerializedName("id") @PrimaryKey(autoGenerate = true) val id: Int?,
        @SerializedName("is_done") val is_done: Boolean?,
        @SerializedName("is_training") val isTraining: Boolean?,
        @SerializedName("is_practice") val isPractice: Boolean?,
        @SerializedName("quantity") val quantity: Double?,
        @SerializedName("sign") val sign: String?,
        @SerializedName("time") val time: Int?,
        @SerializedName("extra_time") val extra_time: Int?,
        @SerializedName("extra_quantity") val extra_quantity: Double?,
        val isLine: Boolean = false,
        val isTimeLine: Boolean = false,
        val presentationCost: Double = 0.0,
        val activityLogIdLocalDB: Long,
        val deleted: Boolean = false,
        val statusSync : Boolean? = null,
        val packing_name : String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(ActivityCodeDetail::class.java.classLoader),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Double,
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readDouble(),
            parcel.readLong(),
            parcel.readByte() != 0.toByte(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString()!!) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(activity_code, flags)
        parcel.writeValue(activityCodeCollaboratorId)
        parcel.writeValue(activityCodeId)
        parcel.writeValue(collaboratorId)
        parcel.writeString(date)
        parcel.writeValue(id)
        parcel.writeValue(is_done)
        parcel.writeValue(isTraining)
        parcel.writeValue(isPractice)
        parcel.writeValue(quantity)
        parcel.writeString(sign)
        parcel.writeValue(time)
        parcel.writeValue(extra_time)
        parcel.writeValue(extra_quantity)
        parcel.writeByte(if (isLine) 1 else 0)
        parcel.writeByte(if (isTimeLine) 1 else 0)
        parcel.writeDouble(presentationCost)
        parcel.writeLong(activityLogIdLocalDB)
        parcel.writeByte(if (deleted) 1 else 0)
        parcel.writeValue(statusSync)
        parcel.writeString(packing_name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DetailtRosterResponse> {
        override fun createFromParcel(parcel: Parcel): DetailtRosterResponse {
            return DetailtRosterResponse(parcel)
        }

        override fun newArray(size: Int): Array<DetailtRosterResponse?> {
            return arrayOfNulls(size)
        }
    }
}


@Entity
data class ActivityCodeDetail(
        @Ignore @SerializedName("activity") var activity : ActivitiesModel?,
        @SerializedName("activity_id") var activityId: Int?,
        @SerializedName("code") var code: Int?,
        @SerializedName("id") @ColumnInfo(name = "activity_code_id") @PrimaryKey(autoGenerate = true) var id: Int?,
        @SerializedName("is_active") @ColumnInfo(name = "active") var isActive: Boolean?,
        @SerializedName("stage_id") var stageId: Int?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(ActivitiesModel::class.java.classLoader),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Int::class.java.classLoader) as? Int) {
    }

    constructor() : this(null, null,null,null, null, null)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(activity, flags)
        parcel.writeValue(activityId)
        parcel.writeValue(code)
        parcel.writeValue(id)
        parcel.writeValue(isActive)
        parcel.writeValue(stageId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActivityCodeDetail> {
        override fun createFromParcel(parcel: Parcel): ActivityCodeDetail {
            return ActivityCodeDetail(parcel)
        }

        override fun newArray(size: Int): Array<ActivityCodeDetail?> {
            return arrayOfNulls(size)
        }
    }
}
