package com.amplemind.vivasmart.features.phenology.viewmodel

import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVars

class VarMeasurePhenologyItemViewModel(phenology_varieties: PhenologyVars){
    var phenology_var_id = phenology_varieties.phenology_var_id
    var phenology_var_name = phenology_varieties.phenology_var_name
    var phenology_var_description = phenology_varieties.phenology_var_description
    var phenology_var_image = phenology_varieties.phenology_var_image

    var unit_id = phenology_varieties.unit_id
    var unit_name = phenology_varieties.unit_name

    var balance_from = phenology_varieties.balance_from
    var balance_to = phenology_varieties.balance_to
    var generative_from = phenology_varieties.generative_from
    var generative_to = phenology_varieties.generative_to
    var vegetative_from = phenology_varieties.vegetative_from
    var vegetative_to = phenology_varieties.vegetative_to

    var medition: Double? = -10.0

    var meditionString: String? = ""

    var date : String? = ""

}
