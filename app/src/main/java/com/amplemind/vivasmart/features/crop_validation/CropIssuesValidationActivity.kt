package com.amplemind.vivasmart.features.crop_validation

import android.os.Bundle
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.crop_validation.CropValidationActivity.Companion.CROP
import com.amplemind.vivasmart.features.crop_validation.fragment.CropIssuesValidationFragment
import com.amplemind.vivasmart.features.crop_validation.viewmodel.CropsIssuesValidationActivityViewModel
import com.amplemind.vivasmart.features.crop_validation.viewmodel.ItemCarryOrderHarvestViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_crop_issues_validation.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject

class CropIssuesValidationActivity() : BaseActivityWithFragment() {
    @Inject
    lateinit var viewModel: CropsIssuesValidationActivityViewModel

    companion object{
        lateinit var objCrop: ItemCarryOrderHarvestViewModel
    }

    fun newInstance(obj : ItemCarryOrderHarvestViewModel) : CropIssuesValidationActivity{
        objCrop = obj
        Log.e("Primero: ", Gson().toJson(objCrop))
        return CropIssuesValidationActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crop_issues_validation)
        setupToolbar()
        setFragment()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_crop_issues
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.crop_validation_title)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun onBackPressed() {
        finish()
    }

    protected fun getFragmentCount(): Int {
        return supportFragmentManager.backStackEntryCount
    }

    fun setFragment() {
        val support = supportFragmentManager
        Log.e("Segundo: ", Gson().toJson(objCrop))
        val commit = support.beginTransaction()
                .replace(R.id.container, CropIssuesValidationFragment().newInstance("CropIssuesValidationFragment", objCrop))
                .addToBackStack("CropIssuesValidationFragment")
                .commit()

    }

}
