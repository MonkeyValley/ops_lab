package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/6/18.
 */
class PackagingQualityViewModel @Inject constructor(private val repository: PackagingQualityRepository,
                                                    private val apiErrors: HandleApiErrors) {

    private var listData = BehaviorSubject.create<List<CarryOrderCondensedViewModel>>()
    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()

    private val carryOrders = mutableListOf<CarryOrderCondensedViewModel>()

    enum class FlagStartReports {
        PIECE,
        TOTAL
    }

    fun getDataQualityPackages(): Disposable {
        return repository.getDataQualityPackages()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { models ->
                    models.data.map { it.showInPackage = true }

                    models.data.map { CarryOrderCondensedViewModel(it) }
                }
                .subscribe({
                    carryOrders.clear()
                    carryOrders.addAll(it)
                    listData.onNext(carryOrders)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLocalDataQualityPackages(): Disposable {
        return repository.getLocalRecibaCondensedList(true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { models -> models.data.map { CarryOrderCondensedViewModel(it) } }
                .subscribe({
                    carryOrders.clear()
                    carryOrders.addAll(it)
                    listData.onNext(carryOrders)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun deleteCarryOrder(carryOrderId: Int) {
        val filtered = carryOrders.filter { it.id == carryOrderId }
        if (filtered.isNotEmpty()) {
            carryOrders.remove(filtered.first())
        }
        repository.deleteCarryOrder(carryOrderId)
    }

    fun getList(): BehaviorSubject<List<CarryOrderCondensedViewModel>> {
        return listData
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

}