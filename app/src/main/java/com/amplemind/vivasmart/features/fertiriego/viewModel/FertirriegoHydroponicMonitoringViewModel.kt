package com.amplemind.vivasmart.features.fertiriego.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.features.fertiriego.fragment.HydroponicReportFragment
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService
import java.util.*
import com.amplemind.vivasmart.vo_core.repository.models.realm.PHCEForTable
import com.amplemind.vivasmart.vo_core.repository.models.realm.PulseForTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import kotlin.collections.ArrayList

class FertirriegoHydroponicMonitoringViewModel @Inject constructor(
        private val service: FertirriegoService
) : BaseViewModel() {

    private val listPulse = ArrayList<Int>()

    var lotId = "0"
    var date = ""

    fun getData(): ArrayList<Int> = onSuccessRequest_pulse()

    lateinit var fragment: HydroponicReportFragment

    fun onSuccessRequest_pulse(): ArrayList<Int> {
        listPulse.clear()
        for(i in 1..service.getMaxPulse(lotId.toInt(), date)) listPulse.add(i)
        return listPulse
    }

    fun getPorcentDrain(lotId: String, date: String): String = service.getLotPorcentDraint(lotId, date)

    fun updateMaxPulse(lotId: Int, maxPulse: Int){ service.updateMaxPulse(lotId, maxPulse, date) }

    fun validateDate(): Boolean {
        val calendar = Calendar.getInstance()

        var month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        var day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val todayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        calendar.add(Calendar.DATE, -1)
        month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val yesterdayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        return date == todayDate /*|| date == yesterdayDate*/
    }

    fun getPocentTableDrain(lotId: Int, date: String, table: Int) = service.getLotPorcentTableDraint(lotId, date, table)

    private val requestSuccessPHCEDataPerDay = BehaviorSubject.create<List<PHCEForTable>>()

    fun getPHCEDataForTable(lotId: Int, date: String, table: String): Disposable {
        return service.getPHCETableDataPerDay(lotId, date, table)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccessPHCEDataPerDay.onNext(it)
                }, {
                    fragment.setDataToZero()
                })
    }
    fun onSuccesPHCEDataPerDay(): BehaviorSubject<List<PHCEForTable>> = requestSuccessPHCEDataPerDay


    private val requestSuccessPulseForTable = BehaviorSubject.create<List<PulseForTable>>()

    fun getPulseForTable(lotId: Int, date: String, table: String): Disposable {
        return service.getPulseForTable(lotId, date, table)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccessPulseForTable.onNext(it)
                }, {
                    fragment.setDataToZero()
                })
    }
    fun onSuccesPulseForTable(): BehaviorSubject<List<PulseForTable>> = requestSuccessPulseForTable

}