package com.amplemind.vivasmart.features.collaborators

import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel

interface IAssingGroovesManager {

    fun addGroove(groove: GrooveModel, position : Int, state : Int)

}
