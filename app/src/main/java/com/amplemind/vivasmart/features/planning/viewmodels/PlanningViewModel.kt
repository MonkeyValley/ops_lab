package com.amplemind.vivasmart.features.planning.viewmodels

import com.amplemind.vivasmart.core.repository.PlanningRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap

class PlanningViewModel @Inject constructor(private val repository: PlanningRepository,
                                            private val apiErrors: HandleApiErrors) {

    enum class PlanningFlow(val type: Int) {
        PRODUCTION(1),
        LINES(2);

        companion object {
            fun from(value : Int) : PlanningFlow = PlanningFlow.values().first{ it.type == value}
        }
    }

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val requestFail = BehaviorSubject.create<String>()
    private val planning = BehaviorSubject.create<List<PlanningItemViewModel>>()

    private fun currentDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return sdf.format(Date())
    }

    fun getTitleDate(): String {
        val sdf = SimpleDateFormat("EEEE dd 'de' MMMM", Locale("es", "ES"))
        return sdf.format(Date())
    }

    /*
     * Get Planning
     * */
    fun getPlanning(flow: PlanningFlow, stageOrPackingId: Int): Disposable {
        val placeId = HashMap<String, Int>()
        var category = 2
        if (flow == PlanningFlow.PRODUCTION) placeId["planning.stage_id"] = stageOrPackingId  else placeId["planning.packingline_id"] = stageOrPackingId
        if (flow == PlanningFlow.LINES) {
            category = 4
        }
        return repository.getPlanning(flow.type, currentDate(), placeId, category)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    it.data.map { PlanningItemViewModel(it) }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    planning.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    /*
     * Return Observers
     * */

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onError(): BehaviorSubject<String> {
        return requestFail
    }

    fun onGetPlanning(): BehaviorSubject<List<PlanningItemViewModel>> {
        return planning
    }

}