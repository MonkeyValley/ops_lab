package com.amplemind.vivasmart.features.collaborators.viewModel

import com.amplemind.vivasmart.core.repository.model.PresentationResultResponse
import com.amplemind.vivasmart.core.repository.response.ActivitiesResponse
import com.amplemind.vivasmart.core.utils.FLOW_ACTIVITES
import javax.inject.Inject

class ItemAddActivitiesViewModel @Inject constructor(val model: ActivitiesResponse? = null,val presentation : PresentationResultResponse? = null, val flow: Int) {

    val name = if (flow == FLOW_ACTIVITES) model?.name else presentation?.name

    var status = false
    var position: Int? = null

    fun selected(status: Boolean) {
        this.status = status
    }

    val activity_id = model?.id

    val packing_id = presentation?.id

    val collaborator_left = model?.collaborator_left

}
