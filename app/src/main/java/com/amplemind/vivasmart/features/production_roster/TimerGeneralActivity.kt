package com.amplemind.vivasmart.features.production_roster

import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.MenuItem
import android.view.View
import android.widget.TimePicker
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_roster.adapters.RecyclerItemTouchHelper
import com.amplemind.vivasmart.features.production_roster.adapters.TimerGeneralAdapter
import com.amplemind.vivasmart.features.production_roster.dialogs.DeleteCollaboratorDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.EditTimerGeneralDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.EditTimerGeneralListener
import com.amplemind.vivasmart.features.production_roster.dialogs.FinalizeUserTimerDialog
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerGeneralViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import com.amplemind.vivasmart.features.scannerbarcode.ReadBarcodeActivity
import kotlinx.android.synthetic.main.activity_timer_general.*
import javax.inject.Inject

class TimerGeneralActivity : BaseActivity(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private val COLLABORATORS_INTENT = 1

    @Inject
    lateinit var viewModel: TimerGeneralViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer_general)
        setupToolBar()
        setupRecycler()
        touchHelper()
        fab_collaborators.setOnClickListener {
            startActivityForResult(Intent(this, ReadBarcodeActivity::class.java).putExtra("showActivities", true), COLLABORATORS_INTENT)
        }
    }

    private fun setupToolBar() {
        setSupportActionBar(toolbar_timer.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = intent.extras!!.getString("title")
    }

    private fun setupRecycler() {
        rv_general_time.hasFixedSize()
        rv_general_time.layoutManager = LinearLayoutManager(this)
        rv_general_time.itemAnimator = DefaultItemAnimator()
        rv_general_time.adapter = TimerGeneralAdapter()
        (rv_general_time.adapter as TimerGeneralAdapter).onUserClick().subscribe(this::onUserClick).addTo(subscriptions)
        (rv_general_time.adapter as TimerGeneralAdapter).onTimeClick().subscribe(this::onTimeClick).addTo(subscriptions)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            COLLABORATORS_INTENT -> {
                empty_view.visibility = View.GONE
                ll_collaborators.visibility = View.VISIBLE
                (rv_general_time.adapter as TimerGeneralAdapter).addElements(viewModel.getDummyListUsers())
            }
        }
    }

    private fun touchHelper() {
        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rv_general_time)
    }

    private fun onUserClick(item: Pair<TimerUserViewModel, Int>) {
        val itemViewModel = item.first
        val position = item.second
        val dialog = EditTimerGeneralDialog.newInstance(intent.extras!!.getString("title")!!)
        dialog.onEditTimerGeneral(object: EditTimerGeneralListener {
            override fun onChange(time: Int?, quantity: Int?, extraTime: Int?) {
                //To change body of created functions use File | Settings | File Templates.
            }

        })
        dialog.show(supportFragmentManager, EditTimerGeneralDialog.TAG)
    }

    private fun onTimeClick(item: Pair<TimerUserViewModel, Int>) {
        val itemViewModel = item.first
        val position = item.second
        val timerDialog = TimePickerDialog(this, object: TimePickerDialog.OnTimeSetListener{
            override fun onTimeSet(p0: TimePicker?, hours: Int, minutes: Int) {
                itemViewModel.setUserTime(((hours * 60 + minutes) * 60).toLong())
                (rv_general_time.adapter as TimerGeneralAdapter).notifyItemChanged(position)
            }

        }, 0, 0, true)
        timerDialog.show()
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        when (direction) {
            ItemTouchHelper.LEFT -> {
                val dialog = DeleteCollaboratorDialog.newInstance(viewModel.getCollaboratorImage(position), viewModel.getCollaboratorName(position))
                dialog.setOnDeleteCollaboratorListener(object: DeleteCollaboratorDialog.DeleteCollaboratorListener{
                    override fun onDelete(isDeleted: Boolean) {
                        if (isDeleted) {
                            viewModel.deleteCollaborator(position)
                            (rv_general_time.adapter as TimerGeneralAdapter).notifyItemRemoved(position)
                        } else {
                            (rv_general_time.adapter as TimerGeneralAdapter).notifyItemChanged(position)
                        }
                    }
                })
                dialog.show(supportFragmentManager, DeleteCollaboratorDialog.TAG)
            }
            ItemTouchHelper.RIGHT -> {
                val dialog = FinalizeUserTimerDialog.newInstance("Finalizar actividad", "¿Esta seguro de querer enviar este reporte? Por favor firme para confirmar",
                        viewModel.getCollaboratorName(position), "00:00",
                       "0",
                        "0", false, "Super Unidad",
                        "0", "0",
                                true, true)
                dialog.isCancelable = false
                dialog.show(supportFragmentManager, FinalizeUserTimerDialog.TAG)

                /*
                dialog.setOnCompleteListener(object : OnFinalizeUserListener {
                    override fun cancel() {
                        (rv_general_time.adapter as TimerGeneralAdapter).notifyItemChanged(position)
                    }

                    override fun completeSign(sign: Bitmap, isTrainig: Boolean, isPractice: Boolean) {
                        (rv_general_time.adapter as TimerGeneralAdapter).notifyItemChanged(position)
                    }

                    override fun skipSignature(isTrainig: Boolean, isPractice: Boolean) {
                        (rv_general_time.adapter as TimerGeneralAdapter).notifyItemChanged(position)
                    }
                })
                */
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
