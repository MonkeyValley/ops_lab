package com.amplemind.vivasmart.features.sync_forced.service

import android.util.Log
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import io.realm.Realm
import io.realm.Sort
import javax.inject.Inject

class SyncForcedService @Inject constructor() {

    fun getCollaboratorLastVersion(): Long? =
            Realm.getDefaultInstance().use {
                it.where(CollaboratorModel::class.java)
                        .sort("version", Sort.DESCENDING)
                        .findFirst()
                        ?.version ?: 0
            }

    fun uploadCollaborator(list: List<CollaboratorModel>): Boolean {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("ColaboratorSize", list.size.toString())
                for (item in list) {
                    var result = realm.where(CollaboratorModel::class.java)
                            .equalTo("id", item.id)
                            .findFirst()

                    if (result != null) {
                        result.name = item.name
                        result.image = item.image
                        result.employeeCode = item.employeeCode
                        result.birthdate = item.birthdate
                        result.isWorking = item.isWorking
                        result.version = item.version

                        realm.insertOrUpdate(result)
                    } else realm.insertOrUpdate(item)
                }
            }
        }
        return true
    }

    fun getActivitiesLastVersion(): Long? =
            Realm.getDefaultInstance().use {
                it.where(ActivityModel::class.java)
                        .sort("version", Sort.DESCENDING)
                        .findFirst()
                        ?.version ?: 0
            }

    fun uploadActivity(list: List<ActivityModel>): Boolean {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("ActivitySize", list.size.toString())
                for (item in list) {
                    var result = realm.where(ActivityModel::class.java)
                            .equalTo("id", item.id)
                            .findFirst()

                    if (result != null) {
                        result.activityAreaId = item.activityAreaId
                        result.activityCategoryId = item.activityCategoryId
                        result.activitySubCategoryId = item.activitySubCategoryId
                        result.activityTypeId = item.activityTypeId
                        result.businessUnitId = item.businessUnitId
                        result.code = item.code
                        result.cost = item.cost
                        if(item.crops!!.size != 0) {
                            result.crops!!.clear()
                            result.crops!!.addAll(item.crops!!.toList())
                        }
                        result.costPerHour = item.costPerHour
                        result.extraTimeCost = item.extraTimeCost
                        result.practiceCost = item.practiceCost
                        result.isHead = item.isHead
                        result.sundayRate = item.sundayRate
                        result.isPractice = item.isPractice
                        result.controlCode = item.controlCode
                        result.controlCost = item.controlCost
                        result.description = item.description
                        result.icon = item.icon
                        result.isActive = item.isActive
                        result.isGrooves = item.isGrooves
                        result.isRemovable = item.isRemovable
                        result.name = item.name
                        result.performance = item.performance
                        result.rangeFrom = item.rangeFrom
                        result.rangeTo = item.rangeTo
                        if (item.unit != null) result.unit = realm.copyToRealm(item.unit!!)
                        result.unitId = item.unitId
                        result.unitLimit = item.unitLimit
                        result.stageActivities = item.stageActivities
                        result.version = item.version

                        realm.insertOrUpdate(result)
                    } else realm.insert(item)
                }
            }
        }
        return true
    }

    fun getStageLastVersion(): Long? =
            Realm.getDefaultInstance().use {
                it.where(StageModel::class.java)
                        .sort("version", Sort.DESCENDING)
                        .findFirst()
                        ?.version ?: 0
            }

    fun uploadStage(list: List<StageModel>) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("StageSize", list.size.toString())
                for (item in list) {
                    var result = realm.where(StageModel::class.java)
                            .equalTo("id", item.id)
                            .findFirst()


                    if (result != null) {
                        result.lot = realm.copyToRealm(item.lot!!)
                        result.code = item.code
                        result.cropId = item.cropId
                        result.cycleId = item.cycleId
                        result.density = item.density
                        result.isActive = item.isActive
                        result.isClosed = item.isClosed
                        result.lastClose = item.lastClose
                        result.lotId = item.lotId
                        if(item.crop != null) result.crop = realm.copyToRealm(item.crop!!)
                        result.plantDate = item.plantDate
                        result.plantsPerGroove = item.plantsPerGroove
                        result.stageActivities = item.stageActivities

                        item.varieties?.forEach { stageVariety ->
                            var varieties = realm
                                    .where(StageVarietyModel::class.java)
                                    .equalTo("id", stageVariety.id)
                                    .findFirst()

                            if (varieties != null) {
                                varieties.isPriority = stageVariety.isPriority
                                varieties.sampling = stageVariety.sampling
                                varieties.stageId = stageVariety.stageId
                                varieties.tableNo = stageVariety.tableNo
                                varieties.varietyId = stageVariety.varietyId

                                if (varieties.variety != null) {
                                    varieties.variety!!.cropId = stageVariety.variety!!.cropId
                                    varieties.variety!!.graftName = stageVariety.variety!!.graftName
                                    varieties.variety!!.isCommercial = stageVariety.variety!!.isCommercial
                                    varieties.variety!!.isGraft = stageVariety.variety!!.isGraft
                                    varieties.variety!!.supplier = stageVariety.variety!!.supplier
                                    varieties.variety!!.name = stageVariety.variety!!.name
                                    varieties.variety!!.version = stageVariety.variety!!.version
                                } else {
                                    realm.insertOrUpdate(stageVariety.variety!!)
                                }

                                realm.insertOrUpdate(varieties)
                            } else realm.insertOrUpdate(stageVariety)
                        }

                        result.version = item.version

                    } else {
                        var stageResult = realm
                                .where(StageModel::class.java)
                                .equalTo("lotId", item.lotId)
                                .findAll()

                        if(stageResult.size != 0){

                            stageResult.forEach { stage ->
                                var activityLogsResult = realm
                                        .where(ActivityLogModel::class.java)
                                        .beginGroup()
                                        .equalTo("isDone", false)
                                        .equalTo("activityCode.stage.id", stage.id)
                                        .endGroup()
                                        .findAll()

                                Log.e("activityLogsResult-size", activityLogsResult.size.toString())

                                activityLogsResult.forEach { activityLog ->
                                    var collaborator = realm
                                            .where(CollaboratorModel::class.java)
                                            .equalTo("uuid", activityLog.collaboratorUuid)
                                            .findFirst()

                                    if (collaborator != null) {
                                        collaborator.isWorking = false
                                        realm.insertOrUpdate(collaborator)
                                    }
                                }
                            }

                            stageResult.deleteAllFromRealm()
                        }

                        realm.insertOrUpdate(item)
                    }
                }
            }
        }
    }

    fun getPlagueLastVersion(): Long? =
            Realm.getDefaultInstance().use {
                it.where(PlagueDetail::class.java)
                        .sort("version", Sort.DESCENDING)
                        .findFirst()
                        ?.version ?: 0
            }

    fun getActivityCodeLastVersion(): Long? =
            Realm.getDefaultInstance().use {
                it.where(ActivityCodeModel::class.java)
                        .sort("version", Sort.DESCENDING)
                        .findFirst()
                        ?.version ?: 0
            }

    fun uploadActivityCode(list: List<ActivityCodeModel>?) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("ActivityCodeSize", list!!.size.toString())
                for (item in list!!) {
                    val result = realm.where(ActivityCodeModel::class.java)
                            .equalTo("id", item.id)
                            .findFirst()

                    if (result != null) {
                        result.activityId = item.activityId
                        result.activityUuid = item.activityUuid
                        result.code = item.code
                        result.total = item.total
                        result.stageId = item.stageId
                        result.stageUuid = item.stageUuid
                        result.version = item.version

                        realm.insertOrUpdate(result)
                    } else realm.insertOrUpdate(item)
                }
            }
        }
    }

    fun getActivityLogLastVersion(): Long? =
            Realm.getDefaultInstance().use {
                it.where(ActivityLogModel::class.java)
                        .sort("version", Sort.DESCENDING)
                        .findFirst()
                        ?.version ?: 0
            }

    fun uploadActivityLog(list: List<ActivityLogModel>?) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("ActivityLogSize", list!!.size.toString())
                for (item in list!!) {
                    var result = realm.where(ActivityLogModel::class.java)
                            .equalTo("id", item.id)
                            .findFirst()

                    if (result != null) {
                        result.activityCodeId = item.activityCodeId
                        //result.activityCodeUuid = item.activityCodeUuid
                        result.collaboratorId = item.collaboratorId
                        //result.collaboratorUuid = item.collaboratorUuid

                        if (item.collaborator != null) {
                            result.collaborator!!.name = item.collaborator!!.name
                            result.collaborator!!.image = item.collaborator!!.image
                            result.collaborator!!.employeeCode = item.collaborator!!.employeeCode
                            result.collaborator!!.birthdate = item.collaborator!!.birthdate
                            result.collaborator!!.isWorking = item.collaborator!!.isWorking
                            result.collaborator!!.version = item.collaborator!!.version
                        }

                        result.date = item.date
                        //result.isTraining = item.isTraining
                        //result.isPending = item.isPending
                        //result.isDone = item.isDone
                        result.quantity = item.quantity
                        result.badQuantity = item.badQuantity
                        //result.sign = item.sign
                        result.time = item.time
                        //result.isPractice = item.isPractice
                        //result.qualitySign = item.qualitySign
                        //result.signDate = item.signDate
                        //result.supervisorQualitySign = item.supervisorQualitySign
                        result.startTime = item.startTime
                        //result.isPaused = item.isPaused
                        result.isControl = item.isControl
                        result.extra_hours = item.extra_hours
                        result.collaboratorNo = item.collaboratorNo
                        result.version = item.version

                        realm.insertOrUpdate(result)
                    }
                }
            }
        }
    }

    fun insertPollinationData(list: List<PollinationModel>?) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                list!!.forEach { item ->
                    realm.insertOrUpdate(item)
                }
            }
        }
    }

    fun uploadGroove(list: List<GrooveModel>?) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("GrooveSize", list!!.size.toString())
                for (item in list!!) {
                    var result = realm.where(GrooveModel::class.java)
                            .equalTo("id", item.id)
                            .findFirst()

                    if (result != null) {
                        result.activityLogId = item.activityLogId
                        result.activityCodeId = item.activityCodeId
                        result.grooveNo = item.grooveNo
                        result.tableNo = item.tableNo
                        result.qualityComment = item.qualityComment
                        result.qualityControl = item.qualityControl

                        realm.insertOrUpdate(result)
                    }
                }
            }
        }
    }

    fun uploadFertirriegoLots(list: List<LotFertirriegoModel>?): Boolean {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("LotFertirriegoSize", list!!.size.toString())
                list.forEach { item ->
                    realm.insertOrUpdate(item)
                }
            }
        }
        return true
    }

    fun uploadProductionRanges(list: List<ProductionRangeVariatiesModel>?): Boolean {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("ProductionRangeSize", list!!.size.toString())
                realm.insertOrUpdate(list)
            }
        }
        return true
    }

    fun getStageId(stageUuid: String): Int =
            Realm.getDefaultInstance().use {
                it.where(StageModel::class.java)
                        .equalTo("uuid", stageUuid)
                        .findFirst()!!.id
            }

    fun uploadVarieties(list: List<VarietyModel>): Boolean {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                Log.e("VarietySize", list.size.toString())
                list.forEach { variety ->
                    realm.insertOrUpdate(variety)
                }
            }
        }

        return true
    }

    fun getStage(): List<StageModel> {
        val list: ArrayList<StageModel> = ArrayList()
        Realm.getDefaultInstance().use {
            val result = it.where(StageModel::class.java).findAll()
            list.addAll(it.copyFromRealm(result))

            it.executeTransaction { realm ->
                realm.delete(VarietyModel::class.java)
            }

            return list
        }
    }

    fun moduleHasAccess(moduleId: Int): Boolean {
        Realm.getDefaultInstance().use { realm ->
            val result = realm.where(AppModule::class.java)
                    .equalTo("moduleId", moduleId)
                    .findFirst()

            return if (result != null) result.hasAccess!!
            else false
        }
    }

    fun uploadPlague(plagueDetail: PlagueDetail) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                var result = realm
                        .where(PlagueDetail::class.java)
                        .equalTo("id", plagueDetail.id)
                        .findAll()

                if (result.size != 0) {
                    result.forEach { item ->
                        item.date = plagueDetail.date
                        item.image = plagueDetail.image
                        item.isDesease = plagueDetail.isDesease
                        item.isQualitative = plagueDetail.isQualitative
                        item.lv1 = plagueDetail.lv1
                        item.lv2 = plagueDetail.lv2
                        item.lv3 = plagueDetail.lv3
                        item.medition = plagueDetail.medition
                        item.name = plagueDetail.name
                        item.propagation = plagueDetail.propagation
                        item.version = plagueDetail.version
                        realm.insertOrUpdate(item)
                    }
                } else {
                    realm.insert(plagueDetail)
                }

                var plaguesImages = realm
                        .where(PlagueImageModel::class.java)
                        .equalTo("plagueId", plagueDetail.id)
                        .findAll()

                if (plaguesImages.size != 0) plaguesImages.deleteAllFromRealm()

                if (plagueDetail.images != null) {
                    plagueDetail.images!!.forEach { image ->
                        realm.insertOrUpdate(image)
                    }
                }
            }
        }
    }

    fun deletePollination() {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                realm.delete(PollinationModel::class.java)
            }
        }
    }

    fun getLots() =
            Realm.getDefaultInstance().use {
                it.where(LotModel::class.java)
                        .findAll()
            }

    fun deleteActivity(uuid: String?) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->

                var result = realm.where(ActivityModel::class.java)
                        .equalTo("uuid", uuid)
                        .findFirst()

                if(result != null){
                    result.isActive = false
                    realm.insertOrUpdate(result)
                }
            }
        }
    }

    fun getDailyPayLastVersion(): Long? =
            Realm.getDefaultInstance().use {
                it.where(DailyPayModel::class.java)
                        .sort("dailypayVersion", Sort.DESCENDING)
                        .findFirst()
                        ?.dailypayVersion ?: 0
            }

    fun saveDailyPay(list: List<DailyPayModel>?) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->

                list!!.forEach { dailyPay ->
                    var result = realm
                            .where(DailyPayModel::class.java)
                            .equalTo("dailypayUuid", dailyPay.dailypayUuid)
                            .findFirst()

                    if(result != null){
                        result.collaboratorEmployeeCode = dailyPay.collaboratorEmployeeCode
                        result.collaboratorId = dailyPay.collaboratorId
                        result.collaboratorUuid = dailyPay.collaboratorUuid
                        result.dailypayUuid = dailyPay.dailypayUuid
                        result.dailypayVersion = dailyPay.dailypayVersion
                        result.date = dailyPay.date
                        realm.insertOrUpdate(result)
                    } else {
                        realm.insert(dailyPay)
                    }
                }

            }
        }
    }
}
