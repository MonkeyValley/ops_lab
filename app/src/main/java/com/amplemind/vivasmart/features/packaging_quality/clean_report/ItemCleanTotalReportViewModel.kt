package com.amplemind.vivasmart.features.packaging_quality.clean_report

import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.model.QualityReportModelTemp
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/13/18.
 */
class ItemCleanTotalReportViewModel @Inject constructor(item : IssueModel){

    val id = item.id
    val name = item.name
    var percentage = "0%"
    var number = ""
    val isPermanent = item.subCategory == 9

    val showNumber = ObservableInt(View.GONE)
    val showAdd = ObservableInt(View.VISIBLE)


    fun showNumber() {
        showNumber.set(View.VISIBLE)
        showAdd.set(View.GONE)
    }


}