package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.BarcodeScannerRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.core.base.BaseViewModel
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class BarcodeScannerViewModel @Inject constructor(
        private val barcodeScannerRepository: BarcodeScannerRepository,
        private val apiErrors: HandleApiErrors
): BaseViewModel(){

    private val requestSuccess_Scan = BehaviorSubject.create<CollaboratorModel>()

    fun cleanUp() {
        barcodeScannerRepository.cleanUp()
    }

    fun findByCode(code: String): Maybe<CollaboratorModel> =
            barcodeScannerRepository.findByCode(code)

    fun findByCodeX(code: String): Disposable {
        return barcodeScannerRepository.findByCodeX(code)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccess_Scan.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSuccessRequest_Scan(): BehaviorSubject<CollaboratorModel> {
        return requestSuccess_Scan
    }


}
