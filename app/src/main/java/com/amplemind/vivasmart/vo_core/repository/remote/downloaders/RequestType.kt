package com.amplemind.vivasmart.vo_core.repository.remote.downloaders

import android.content.Context
import com.amplemind.vivasmart.R


enum class RequestType (
        val remoteName: String,
        private val humanNameResource: Int,
        val isCatalog: Boolean = false
) {

    UNKNOWN(         "",                 R.string.request_type_unknown         ),
    COLLABORATOR (   "collaborator",     R.string.request_type_collaborator,    isCatalog = true),
    ACTIVITY (       "activity",         R.string.request_type_activity,        isCatalog = true),
    GROOVE (         "groove",           R.string.request_type_groove          ),
    ACTIVITY_LOG (   "activity_log",     R.string.request_type_activity_log    ),
    ACTIVITY_CODE (  "activity_code",    R.string.request_type_activity_code   ),
    STAGE (          "stage",            R.string.request_type_stage,           isCatalog = true),
    //LOT (            "lot",            R.string.request_type_lot,           isCatalog = true);
    MESSAGE (        "message",          R.string.request_type_message         ),
    DAILYPAY (        "daily_pay",          R.string.request_type_daily_pay         ),
    CROP(            "crop",             R.string.request_type_crop,            isCatalog = true),
    CLIENT(          "client",           R.string.request_type_client,          isCatalog = true),
    UNIT(            "unit",             R.string.request_type_units,           isCatalog = true),
    LOT_FERTIRRIEGO( "Fertirriego",      R.string.request_type_fertirriego_lot, isCatalog = true),
    PRODUCITON_RANGES("Production_ranges", R.string.request_type_fertirriego_lot, isCatalog = true),
    PRODUCITON_RANGES_STAGE("Production_ranges_stage", R.string.request_type_production_ranges, isCatalog = true);

    companion object {

        fun getCatalogs(): List<RequestType> =
                values().filter { requestType ->
                    requestType.isCatalog
                }

        fun getForRemoteName(remoteName: String): RequestType =
                values().find { requestType ->
                    remoteName == requestType.remoteName
                } ?: UNKNOWN
    }

    fun getHumanName(context: Context) =
            context.getString(humanNameResource)

}

