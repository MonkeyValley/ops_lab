package com.amplemind.vivasmart.core.repository.model

data class ChangeLotModel(val name : String, val status : Boolean)
