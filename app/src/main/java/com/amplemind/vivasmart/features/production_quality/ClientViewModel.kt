package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.response.ClientResponseResult

class ClientViewModel(client : ClientResponseResult) {
    val id = client.id
    val name =  client.name
}
