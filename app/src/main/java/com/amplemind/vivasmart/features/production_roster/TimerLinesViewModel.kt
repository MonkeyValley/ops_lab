package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.extensions.calculatePaymentByHrs
import com.amplemind.vivasmart.core.extensions.registerObserver
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.navegation.data_mediator.MediatorNavigation
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.TimerLinesRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.request.*
import com.amplemind.vivasmart.core.repository.response.ActivitiesResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorFinishCostResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserSectionViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.jetbrains.anko.doAsync
import java.io.File
import java.util.*
import javax.inject.Inject

class TimerLinesViewModel @Inject constructor(private val apiErrors: HandleApiErrors, private val timerLinesRepository: TimerLinesRepository
                                              , private val mediatorNavigation: MediatorNavigation, private val uploadRepository: ProfileRepository
                                              , private var preferences: UserAppPreferences, private val mediator: Mediator) : BaseViewModel() {

    private val finishAllCollaboratorSubject = BehaviorSubject.create<Boolean>()


    private val deleteResponse = BehaviorSubject.create<Int>()
    private val successUpload = BehaviorSubject.create<FinishCollaboratorResult>()

    private val sections = mutableListOf<TimerUserSectionViewModel>()
    private val sectionSubject = BehaviorSubject.create<MutableList<TimerUserSectionViewModel>>()

    private val informationFinishCollaborator = BehaviorSubject.create<CollaboratorFinishCostResponse>()

    private val addMoreCollaborators = BehaviorSubject.create<Boolean>()


    private val haveCount = BehaviorSubject.create<Pair<Boolean, Int>>()

    private val showCountActivity = BehaviorSubject.create<Pair<Boolean, HeaderModel>>()

    private val finishCollaboratorSubject = BehaviorSubject.create<Pair<Int, Int>>()

    private var showCount = false

    private var has_countbox = false

    fun getHaveCount(): BehaviorSubject<Pair<Boolean, Int>> {
        return haveCount
    }

    private fun haveTime(): Boolean {
        if (sections.isEmpty()) {
            return false
        }
        sections.forEach { users ->
            val result = users.users.filter { it.is_time == false }.sumBy { it.time.toInt() } > 0
            if(result){
                return true
            }
        }
        return false
    }

    fun getshowCountActivity(): BehaviorSubject<Pair<Boolean, HeaderModel>> {
        return showCountActivity
    }

    fun showCountBoxActivity(hasInternet: Boolean, type: Int, section: HeaderModel? = null): Disposable {
        return Observable.fromCallable {
            val time = haveTime()

            val haveCount = if (hasInternet) {
                has_countbox
            } else {
                timerLinesRepository.haveCount(mediator.bundleItemLinesPackage!!)
            }
            return@fromCallable time || haveCount && sections.isNotEmpty()
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (type == BoxCountLinesActivity.ADD_COLLABORATOR){
                        showCountActivity.onNext(Pair(result,section!!))
                    }else{
                        haveCount.onNext(Pair(result, type))
                    }
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun loadCollaborators(): Disposable {
        return timerLinesRepository.getCollaboratorsByLine()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .doOnError { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    has_countbox = it.has_boxcount
                    timerLinesRepository.insertCollaborators(it.data)
                    mapModelToViewModel(it.data)
                }
                .subscribe({ lines ->
                    sections.addAll(lines)
                    sectionSubject.onNext(sections)
                    showCountBox()
                }, {
                    progressStatus.onNext(false)
                    searchLocalData.onNext(true)
                })
    }

    fun getOfflineCollaborators(): Disposable {
        return getCollaboratorOffline()
                .registerObserver(progressStatus)
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    mapModelToViewModel(it)
                }
                .doOnNext { lines ->
                    sections.addAll(lines)
                }
                .subscribe({
                    sectionSubject.onNext(sections)
                    showCountBox()
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun getCollaboratorOffline(): Observable<List<CollaboratorsInLinesResponse>> {
        return timerLinesRepository.getLocalCollaborators()
    }


    fun isSectionVisible(): Boolean {
        return sections.size > 0
    }


    fun getSectionsCollaborators(): BehaviorSubject<MutableList<TimerUserSectionViewModel>> {
        return sectionSubject
    }

    fun allCollaboratorHaveTime(): Boolean {
        if (sections.isEmpty()) return false
        return sections.any { it.users.any { it.time > 0 } }
    }

    fun showCountBox() {
        showCount = sections.any { it.users.any { !(it.is_time ?: false) } }
        removeCountBox()
    }


    fun getShowCountBox(): Boolean {
        return showCount
    }

    fun addCollaborators(collaborators: List<CollaboratorsInLinesResponse>, hasInternet: Boolean, hasCount: Boolean) {
        val model = mapModelToViewModel(collaborators)

        this.has_countbox = hasCount

        if (hasInternet) {
            timerLinesRepository.insertCollaborators(collaborators)
        }

        model.forEach { user_section ->
            val isNewSection = sections.none { it.name == user_section.name }
            if (isNewSection) {
                sections.add(user_section)
            } else {
                user_section.users.forEach { user ->
                    val list = sections.first { it.name == user_section.name }
                    list.collaborator_left = user_section.collaborator_left
                    list.users.add(user)
                }
            }
        }
        if (model.isNotEmpty()) {
            addMoreCollaborators.onNext(true)
        }
        showCountBox()
    }

    fun addMoreCollaboratorsSucess(): BehaviorSubject<Boolean> {
        return addMoreCollaborators
    }

    /**
     *  create sections in lines
     */
    private fun mapModelToViewModel(model: List<CollaboratorsInLinesResponse>): List<TimerUserSectionViewModel> {
        val viewModels = mutableListOf<TimerUserSectionViewModel>()

        for (item in model) {
            val timerUser = mapPackingLineToTimerUser(item.packingline!!)

            val timerViewModel = TimerUserSectionViewModel(item.activity, item.collaborator_left, timerUser, item.id, item.id_activity)
            timerViewModel.setActivity(item.activity_info)

            viewModels.add(timerViewModel)
        }
        return viewModels
    }

    /**
     *  Create list of collaborators in lines and set activities model
     */
    private fun mapPackingLineToTimerUser(packingline: List<PackingLineCollaboratorsModel>): MutableList<TimerUserViewModel> {
        val viewModel = mutableListOf<TimerUserViewModel>()

        for (collaborator in packingline) {

            if (collaborator.date > 0) {
                collaborator.time_counter = ((System.currentTimeMillis() - collaborator.date) / 1000).toDouble()
            }

            val exist = sections.any { list -> list.users.any { user -> user.getCollaboratorIdLogLine() == collaborator.id } }
            if (!exist) {
                val timerViewModel = TimerUserViewModel()

                val activities = CollaboratorsListReponse(collaborator.activity_id!!, collaborator.collaborator!!, collaborator.collaborator_id!!
                        , collaborator.id!!, false, collaborator.is_done!!, collaborator.time_counter!!, collaborator.is_paused!!, collaborator.is_time, collaborator.temp_time
                        , collaborator.packing_id, collaborator.packingline_id, collaborator.packing?.name, date = 0)

                timerViewModel.setActivities(activities)
                timerViewModel.setMediator(mediatorNavigation)
                timerViewModel.count_box = collaborator.units
                timerViewModel.olds_count_box = collaborator.units_olds
                timerViewModel.isOnLine = collaborator.isOnlineCollaborator

                viewModel.add(timerViewModel)
            }

        }

        return viewModel
    }


    //region delete collaborators process
    fun deleteCollaborator(hasInternet: Boolean, registerId: Int, fromOnline: Boolean, position: Int): Disposable {
        val observer = if (hasInternet) {
            timerLinesRepository.deleteCollaborator(registerId, mediator.bundleItemLinesPackage!!)
        } else {
            timerLinesRepository.deleteCollaboratorOffline(registerId, fromOnline, mediator.bundleItemLinesPackage!!)
        }
        return observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    updateCollaboratorLeft(registerId)
                    deleteResponse.onNext(position)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun updateCollaboratorLeft(registerID: Int?) {
        sections.first { it.users.any { it.registerId == registerID } }.collaborator_left += 1
    }

    fun deleteResponse(): BehaviorSubject<Int> {
        return deleteResponse
    }

    //endregion

    //region pause in collaborators process
    fun pauseCollaborator(hasInternet: Boolean, registerId: Int, time: Long, position: Int): Disposable {
        val collaborator = PauseTimerCollaborator(registerId, time)
        return pauseAllCollaboratorsRequest(hasInternet, listOf(collaborator))
    }

    private fun getCollaboratorsTime(): MutableList<PauseTimerCollaborator> {
        val collaborators = mutableListOf<PauseTimerCollaborator>()

        sections.forEach { section ->
            for (item in section.users) {
                collaborators.add(PauseTimerCollaborator(item.registerId!!, item.time))
            }
        }
        return collaborators
    }

    fun pauseAllCollaborators(hasInternet: Boolean): Disposable {
        return pauseAllCollaboratorsRequest(hasInternet, getCollaboratorsTime())
    }

    private fun pauseAllCollaboratorsRequest(hasInternet: Boolean, collaborators: List<PauseTimerCollaborator>): Disposable {

        val idLine = mediator.bundleItemLinesPackage!!

        val observer = if (hasInternet) {
            timerLinesRepository.pauseCollaborators(PauseTimerCollaboratorRequest(collaborators))
        } else {
            timerLinesRepository.pauseCollaboratorOfflineObserver(collaborators, idLine)
        }
        return observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    doAsync {
                        timerLinesRepository.pauseCollaboratorOffline(collaborators, idLine)
                    }
                }
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({

                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    //endregion

    //region start collaborators process
    fun startCollaborator(hasInternet: Boolean, registerId: Int, position: Int): Disposable {
        val collaborator = RestartTimerCollaboratorLines(registerId)
        return startAllCollaboratorsRequest(hasInternet, listOf(collaborator))
    }

    fun startAllCollaborators(hasInternet: Boolean): Disposable {
        val collaborators = mutableListOf<RestartTimerCollaboratorLines>()
        sections.forEach { section ->
            for (item in section.users) {
                collaborators.add(RestartTimerCollaboratorLines(item.registerId!!))
            }
        }
        return startAllCollaboratorsRequest(hasInternet, collaborators)
    }

    private fun startAllCollaboratorsRequest(hasInternet: Boolean, collaborators: List<RestartTimerCollaboratorLines>): Disposable {

        val idLine = mediator.bundleItemLinesPackage!!

        val observer = if (hasInternet) {
            timerLinesRepository.startCollaborators(RestarTimerCollaboratorLines(collaborators))
        } else {
            timerLinesRepository.startCollaboratorsOfflineObservable(collaborators, idLine)
        }

        return observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    doAsync {
                        timerLinesRepository.startCollaboratorOffline(collaborators, idLine)
                    }
                }
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({

                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun removeAllCollaborator() {
        sections.clear()
    }

    fun getUser(registerId: Int): TimerUserViewModel {
        var item = TimerUserViewModel()

        sections.forEach {
            it.users.forEach { user ->
                if (user.registerId == registerId) {
                    item = user
                }
            }
        }

        return item
    }

    fun getUserPayment(result: CollaboratorFinishCostResponse): String {
        return "$%.2f".format(Locale.US,result.total)
    }

    fun getUnits(unit_total: Double?): String {
        if (unit_total == null) return ""
        return "%.2f".format(unit_total)
    }

    fun getUserPaymentInTraining(training_cost: Double?, user: TimerUserViewModel): String {
        return "$%.2f".format(Locale.US,calculatePaymentByHrs(user.getMinutesTime().toFloat() / 60f, training_cost?.toFloat()
                ?: 0.0f))
    }

    fun getUserPaymentInPractice(practice_cost: Double?, user: TimerUserViewModel): String {
        return "$%.2f".format(Locale.US,calculatePaymentByHrs(user.getMinutesTime().toFloat() / 60f, practice_cost?.toFloat()
                ?: 0.0f))
    }

    fun finishCollaboratorSignature(registerId: Int, time: Long, training: Boolean, position: Int, sign: String?, isPractice: Boolean, isBox: Boolean = false): Disposable {
        return if (isBox) {
            finishCollaboratorWithCountBoxRequest(registerId, time, sign, position, training, isPractice)
        } else {
            finishCollaboratorSignatureRequest(registerId, time, sign, position, training, isPractice)
        }
    }

    fun finishAllCollaborators(hasInternet: Boolean): Disposable {
        val allCollaborators = mapAllCollaboratorFinish()
        val observer = if (hasInternet) {
            timerLinesRepository.updateAllFinishCollaborators(allCollaborators)
            timerLinesRepository.finishCollaboratorSkipSignature(allCollaborators)
        } else {
            timerLinesRepository.finishCollaboratorSkipSignatureOffline(allCollaborators, mediator.bundleItemLinesPackage!!)
        }
        return observer
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    finishAllCollaboratorSubject.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onFinishAllCollaborator(): BehaviorSubject<Boolean> {
        return finishAllCollaboratorSubject
    }

    private fun mapAllCollaboratorFinish(): MutableList<FinishCollaboratorSignature> {
        val request = mutableListOf<FinishCollaboratorSignature>()
        sections.forEach { header ->
            header.users.forEach { user ->
                request.add(FinishCollaboratorSignature(user.registerId!!, user.time, sign = null, isPractice = false))
            }
        }
        return request
    }

    private fun finishCollaboratorSignatureRequest(registerId: Int, time: Long, sign: String?, position: Int, training: Boolean, isPractice: Boolean): Disposable {
        val request = FinishCollaboratorSignature(registerId, time, sign = sign, is_training = training, isPractice = isPractice)
        return timerLinesRepository.finishCollaboratorSkipSignature(listOf(request))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    updateCollaboratorLeft(registerId)
                    timerLinesRepository.updateFinishCollaborator(registerId, time, mediator.bundleItemLinesPackage!!, training, isPractice, sign)
                    finishCollaboratorSubject.onNext(Pair(registerId, position))
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun finishCollaboratorSuccess(): BehaviorSubject<Pair<Int, Int>> {
        return finishCollaboratorSubject
    }

    fun getNameUnit(registerID: Int?): String {
        return sections.first { it.users.any { it.registerId == registerID } }.name
    }

    fun getCostInActivity(registerId: Int): ActivitiesResponse? {
        return sections.first { it.users.any { it.registerId == registerId } }.getActivityInfo()
    }

    fun uploadSign(registerId: Int, time: Long, image: File, training: Boolean, position: Int, isPractice: Boolean, isBox: Boolean = false): Disposable {
        return uploadRepository.uploadImage(image, "user")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    val result = FinishCollaboratorResult(registerId, time, it.relative_url, position, training, isBox, isPractice)
                    successUpload.onNext(result)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun finishSignOfflineCollaborators(registerId: Int, time: Long, training: Boolean, position: Int, sign: File?, isPractice: Boolean, isBox: Boolean = false): Disposable {
        return timerLinesRepository.finishOfflineCollaborator(registerId, mediator.bundleItemLinesPackage!!, sign?.absolutePath, training, isPractice, time, isBox)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    updateCollaboratorLeft(registerId)
                    finishCollaboratorSubject.onNext(Pair(registerId, position))
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSuccessUpload(): BehaviorSubject<FinishCollaboratorResult> {
        return successUpload
    }

    fun getData(): MutableList<TimerUserSectionViewModel> {
        return sections
    }

    fun finishAllCollaboratorWithCountBox(hasInternet: Boolean): Disposable {
        val observer = if (hasInternet) {
            val bulk = mutableListOf<FinishCollaboratorSignature>()
            if (getShowCountBox()) {
                sections.forEach { section ->
                    section.users.forEach {
                        if (it.packing_id == null) {
                            bulk.add(FinishCollaboratorSignature(it.registerId!!, it.time, sign = null, isPractice = false))
                        }
                    }
                }
            }
            val finishRequest = FinishAllCollaboratorWithBox(packingline_id = mediator.bundleItemLinesPackage!!,
                    collaborators = if (bulk.isNotEmpty()) bulk else null,
                    presentation = mediator.count_box?.presentation,
                    times = mediator.mapTimesToHashMap(),
                    isPractice = false)

            timerLinesRepository.updateAllFinishCollaborators(finishRequest.collaborators)
            timerLinesRepository.finishCollaboratorWithBox(finishRequest)
        } else {
            timerLinesRepository.finishCollaboratorWithBoxOffline(sections, mediator.bundleItemLinesPackage!!, mediator.getMapCountBoxRequest())
        }
        return observer
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    finishAllCollaboratorSubject.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun finishCollaboratorWithCountBoxRequest(registerId: Int, time: Long, sign: String?, position: Int, training: Boolean, isPractice: Boolean): Disposable {
        val finishRequest = FinishAllCollaboratorWithBox(packingline_collaborator = registerId, packingline_id = mediator.bundleItemLinesPackage!!,
                collaborators = null, presentation = mediator.count_box?.presentation, times = mediator.mapTimesToHashMap(), sign = sign, isPractice = isPractice, training = training, time = time)
        return timerLinesRepository.finishCollaboratorWithBox(finishRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    updateCollaboratorLeft(registerId)
                    timerLinesRepository.updateFinishCollaborator(registerId, time, mediator.bundleItemLinesPackage!!, training, isPractice, sign)
                    finishCollaboratorSubject.onNext(Pair(registerId, position))
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getTotalsBeforeFinishCollaborator(hasInternet: Boolean, registerID: Int, position: Int, isBoxCount: Boolean): Disposable {
        val user = getUser(registerID)
        val observer = if (hasInternet) {
            timerLinesRepository.getTotalsForFinishCollaborator(time = if (isBoxCount) null else user.time, id_collaborator = registerID, count_box = if (isBoxCount) mediator.count_box?.presentation else null)
        } else {
            timerLinesRepository.getTotalsForFinishCollaboratorOffline(isBoxCount, user, mediator.count_box?.presentation, activity = getCostInActivity(registerID))
        }

        return observer
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({ response ->
                    response.isCountBox = isBoxCount
                    response.position = position
                    response.registerID = registerID

                    informationFinishCollaborator.onNext(response)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getInformationToFinishCollaborator(): BehaviorSubject<CollaboratorFinishCostResponse> {
        return informationFinishCollaborator
    }

    fun removeCountBox() {
        mediator.count_box = null
    }

    fun allTimerAreRunning(): Boolean {
        if (sections.isEmpty()) {
            return false
        }
        var isRunning = false
        sections.forEach {
            isRunning = it.users.all { it.timeIsRunning }
        }
        return isRunning
    }

    fun changePresentation(registerId: Int, name: String?) {
        val user = getUser(registerId)
        user.name_presentation = name
    }

    fun deleteLineOffline(id_lineOffline: Int): Disposable {
        return timerLinesRepository.deleteLineOffline(id_lineOffline)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    fun getTimesByCollaborators() {
        val times = mutableListOf<TimesCollaborator>()
        sections.forEach { section ->
            section.users.forEach { user ->
                times.add(TimesCollaborator(user.registerId!!, user.time))
            }
        }
        mediator.timesCollaborator = times
    }

    //endregion

}
