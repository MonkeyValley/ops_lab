package com.amplemind.vivasmart.features.packaging_quality.clean_report

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.extensions.snack
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderResponse
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.core.utils.ItemClickSupport
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.packaging_quality.quality_report_result.QualityReportActivity
import kotlinx.android.synthetic.main.content_cleaning_report_total_activity.*
import java.io.File
import java.io.IOException
import javax.inject.Inject

@SuppressLint("Registered")
open class CleaningReportTotalActivity : BaseActivity(), ItemClickSupport.OnItemClickListener {

    @Inject
    lateinit var viewModel: CleanReportTotalViewModel

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mPermission: AndroidPermission

    private val CAMERA_REQUEST_CODE = 1
    private var photo: File? = null
    private var redCircle: FrameLayout? = null
    private var countTextView: TextView? = null

    private enum class IssueType(val type: Int) {

        EXPORTATION(1),
        PERMANENT_DAMAGE(2),
        DEFECT(3),
        LIST_ISSUES(4);

        companion object {
            fun from(value: Int): IssueType = IssueType.values().first { it.type == value }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_cleaning_report_total_activity)
        viewModel.setMaxSize(intent.getIntExtra("total", 0))

        setupToolbar()

        initRecycler()

        loadData()

        btn_send_total.addThrottle().subscribe {
            if (viewModel.hasPhoto()){
                uploadPhotosIssues(true)
            }else{
                root_cleaning_report_total.snack("Es necesaria al menos una foto")
            }
        }.addTo(subscriptions)

        tv_exportation.addThrottle().subscribe{
            val currentTotal = (tv_permanent_damage.text.toString().toIntOrNull() ?: 0) + (tv_defect.text.toString().toIntOrNull() ?: 0)
            showEditDialog(IssueType.EXPORTATION, 0, "Exportación", currentTotal)
        }.addTo(subscriptions)

        tv_permanent_damage.addThrottle().subscribe{
            val currentTotal = (tv_exportation.text.toString().toIntOrNull() ?: 0) + (tv_defect.text.toString().toIntOrNull() ?: 0)
            showEditDialog(IssueType.PERMANENT_DAMAGE, 0, "Defecto Permanente", currentTotal)
        }.addTo(subscriptions)

        tv_defect.addThrottle().subscribe{
            val currentTotal = (tv_exportation.text.toString().toIntOrNull() ?: 0) + (tv_permanent_damage.text.toString().toIntOrNull() ?: 0)
            showEditDialog(IssueType.DEFECT, 0, "Defecto Condición", currentTotal)
        }.addTo(subscriptions)

        tv_total.text = intent.getIntExtra("total", 0).toString()
        tv_total_percent.text = viewModel.getTotalPercent(tv_exportation.text.toString(), tv_permanent_damage.text.toString(), tv_defect.text.toString())
    }

    private fun loadData() {
        tv_crop_name.text = intent.getStringExtra("crop")
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onGetIssues().subscribe(this::onGetIssues).addTo(subscriptions)
        viewModel.onIssuesUpdated().subscribe(this::onIssuesUpdated).addTo(subscriptions)

        viewModel.getUploadPhotoSuccess().subscribe(this::uploadPhotosIssues).addTo(subscriptions)
        viewModel.getUploadIssueSuccess().subscribe(this::uploadInfoIssue).addTo(subscriptions)

        viewModel.getIssues(intent.getIntExtra("cropId", 0)).addTo(subscriptions)
    }

    private fun uploadInfoIssue(action : Boolean){
        viewModel.uploadIssues(intent.getIntExtra("unitId", 0), intent.getIntExtra("carryOrderId", 0),
                    tv_exportation.text.toString(), tv_permanent_damage.text.toString(), tv_defect.text.toString(), true).addTo(subscriptions)
    }

    private fun uploadPhotosIssues(action : Boolean){
        if (true){
            viewModel.uploadPhotosIssues()?.addTo(subscriptions)
        }else{
            uploadInfoIssue(true)
        }
    }

    private fun onIssuesUpdated(response: QualityCarryOrderResponse) {
        val activityIntent = Intent(this, QualityReportActivity::class.java)
        activityIntent.putExtra("cleanReport", response)
        activityIntent.putExtra("crop", intent.getStringExtra("crop"))
        startActivity(activityIntent)
        val result = Intent()
        result.putExtra("carryOrderId", intent.getIntExtra("carryOrderId", 0))
        setResult(Activity.RESULT_OK, result)
        finish()
    }

    private fun onGetIssues(issues: List<ItemCleanTotalReportViewModel>) {
        (rv_report_total.adapter as QualityReportTotalAdapter).addElements(issues)
    }

    private fun showEditDialog(type: IssueType, position: Int, name: String, currentTotal : Int) {
        val dialog = AddProblemDialogFragment.newInstance(type.type, position, name, currentTotal, viewModel.getMaxSize())
        dialog.show(supportFragmentManager, AddProblemDialogFragment.TAG)

        dialog.addEvent(object : AddProblemDialogFragment.OnClickAddTotal {
            override fun addTotal(problems: Int, position: Int, type: Int) {
                if (dialog.context != null) {
                    hideKeyboard(dialog.context!!)
                }
                changeValues(type, problems, position)
                dialog.dismiss()
            }
        })
    }

    private fun changeValues(type: Int, problems: Int, position: Int) {
        when (IssueType.from(type)) {
            IssueType.EXPORTATION -> {
                tv_exportation.text = viewModel.validateProblems(problems).toString()
                tv_exportation_percent.text = viewModel.getPercentFromProblem(problems)
                tv_total_percent.text = viewModel.getTotalPercent(tv_exportation.text.toString(), tv_permanent_damage.text.toString(), tv_defect.text.toString())
            }
            IssueType.PERMANENT_DAMAGE -> {
                tv_permanent_damage.text = viewModel.validateProblems(problems).toString()
                tv_permanent_damage_percent.text = viewModel.getPercentFromProblem(problems)
                tv_total_percent.text = viewModel.getTotalPercent(tv_exportation.text.toString(), tv_permanent_damage.text.toString(), tv_defect.text.toString())
            }
            IssueType.DEFECT -> {
                tv_defect.text = viewModel.validateProblems(problems).toString()
                tv_defect_percent.text = viewModel.getPercentFromProblem(problems)
                tv_total_percent.text = viewModel.getTotalPercent(tv_exportation.text.toString(), tv_permanent_damage.text.toString(), tv_defect.text.toString())
            }
            IssueType.LIST_ISSUES -> {
                viewModel.updateIssue(position, problems)
                (rv_report_total.adapter as QualityReportTotalAdapter).notifyItemChanged(position)
                btn_send_total.visibility = View.VISIBLE
            }
        }
    }

    private fun initRecycler() {
        rv_report_total.hasFixedSize()
        rv_report_total.layoutManager = LinearLayoutManager(this)
        rv_report_total.itemAnimator = DefaultItemAnimator()
        rv_report_total.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        rv_report_total.adapter = QualityReportTotalAdapter()

        ItemClickSupport.addTo(rv_report_total).setOnItemClickListener(this)
    }

    override fun onItemDoubleClicked(recyclerView: RecyclerView, position: Int, v: View) {
        changeValues(IssueType.LIST_ISSUES.type, viewModel.addProblemByIssue(position), position)
    }

    override fun onLongClicked(recyclerView: RecyclerView, position: Int, v: View) {
        if (viewModel.getProblemsByIssue(position) > 0) {
            changeValues(IssueType.LIST_ISSUES.type, viewModel.lessProblemByIssue(position), position)
        }
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_report_total
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar?.title = getString(R.string.clean_report_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initCamera() {
        try {
            photo = picturesHelper.createImageFile(this)
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if(callCameraIntent.resolveActivity(packageManager) != null) {
                val authorities = "$packageName.fileprovider"
                val imageUri = FileProvider.getUriForFile(this, authorities, photo!!)
                callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Could not create file!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == mPermission.REQUEST_CODE_PERMISSIONS) {
            if (mPermission.verifyHavePermissions(this)) {
                initCamera()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_quality_report, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val alertMenu = menu!!.findItem(R.id.add_photo)
        val rootView = alertMenu.actionView
        rootView.setOnClickListener { onOptionsItemSelected(alertMenu) }
        redCircle = rootView.findViewById(R.id.view_alert_red_circle)
        countTextView = rootView.findViewById(R.id.view_alert_count_textview)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.add_photo -> {
                if(viewModel.isLimitPhotos()){
                    root_cleaning_report_total.snack("Solo puede agregar 4 fotografías")
                    return true
                }

                if (mPermission.cameraPermission(this)) {
                    initCamera()
                } else {
                    mPermission.verifyHavePermissions(this)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val image = picturesHelper.createImageFile(this)
                    picturesHelper.compressImage(photo!!, image, 400f, 400f)
                    viewModel.addPhoto(image)
                    redCircle?.visibility = View.VISIBLE
                    countTextView?.text = viewModel.getNumPhotos()


                    redCircle?.background = ContextCompat.getDrawable(this,if(viewModel.isLimitPhotos()) R.drawable.circle_blue else R.drawable.circle_white)
                    if(viewModel.isLimitPhotos()){
                        countTextView?.setTextColor(ContextCompat.getColor(this,R.color.whiteCard))
                    }
                }
            }
        }
    }
}
