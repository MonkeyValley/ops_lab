package com.amplemind.vivasmart.domain.controlQuality

import android.annotation.SuppressLint
import android.util.Log
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.platform.SyncNotification
import com.amplemind.vivasmart.core.platform.SynchronizeDataBaseService
import com.amplemind.vivasmart.core.repository.QualityControlRepository
import com.amplemind.vivasmart.core.repository.local.ControlQualityDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.SyncReportModel
import com.amplemind.vivasmart.core.repository.request.SyncControlQuality
import com.amplemind.vivasmart.core.repository.request.SyncControlQualityRequest
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.google.gson.Gson
import io.reactivex.Observable
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class UploadControlQuality @Inject constructor(private val repository: QualityControlRepository,
                                               private val preferences: UserAppPreferences,
                                               private val database: ControlQualityDao) {

    private val TAG = "---> ${this.javaClass.simpleName}"

    /**
     * interface delegated from PendingRequestRepository.class
     *
     * @see com.amplemind.vivasmart.core.platform.SynchronizeDataBaseService
     * */
    private var delegate: SyncNotification? = null
    private val type = SynchronizeDataBaseService.SynchronizeRequest.PRODUCTION_QUALITY
    private var subscriptions: AndroidDisposable? = null

    /*
     * Called first when user clicks on Synchronize from hamburger menu
     *
     * it gets issues from database and populate BULK model to be requested
     * request eg.
     * {
     *  "bulk":[
     *     {
     *        "activity_log_groove_id":Int,
     *        "quality_comment":String,
     *        "offline_id":Long,
     *        "is_ok":Boolean,
     *        "issue_id":Int,
     *        "status":Boolean
     *     }
     *   ]
     * }
     * */
    fun syncLabors(delegate: SyncNotification, subscriptions: AndroidDisposable) {
        this.delegate = delegate
        this.subscriptions = subscriptions

        Observable.fromCallable {
            val grooves = mutableListOf<SyncControlQuality>()

            for (issue in database.getReviewIssue()) {
                Log.d(TAG, "syncLabors (IssueRequest): ${Gson().toJson(issue)}")

                if (issue.bulk.isNotEmpty()) {
                    issue.bulk.map {
                        grooves.add(SyncControlQuality(
                                issueId = it.issueId,
                                isOk = it.isOk,
                                comment = issue.groove.comment,
                                activityLogGrooveId = issue.groove.grooveId,
                                id = issue.id!!,
                                collaboratorId = issue.collaboratorId
                        ))
                    }
                } else {
                    grooves.add(SyncControlQuality(
                            issueId = issue.id?.toInt()!!,
                            isOk = false,
                            comment = issue.groove.comment,
                            activityLogGrooveId = issue.groove.grooveId,
                            id = issue.id,
                            collaboratorId = issue.collaboratorId
                    ))
                }
            }
            uploadLabors(grooves)
        }.subscribe().addTo(subscriptions)
    }

    @SuppressLint("CheckResult")
    private fun uploadLabors(grooves: List<SyncControlQuality>) {
        val request = SyncControlQualityRequest(grooves)
        Log.d(TAG, "uploadLabors ${Gson().toJson(request)}")

        repository.syncControlQualityIssues(preferences.token, request)
                .subscribe({ syncResponse ->

                    delegate?.onTerminate(mapResponse(grooves, syncResponse.data), type)
                    deleteSyncGrooves(syncResponse.data)
                }, {
                    Log.e(TAG, "uploadLabors (Fail upload): ${Gson().toJson(it)}")
                    updateSyncErrors(grooves)
                    delegate?.onTerminate(mapResponse(grooves, grooves), type)
                }).addTo(subscriptions!!)
    }

    private fun mapResponse(grooves: List<SyncControlQuality>, syncResponse: List<SyncControlQuality>): List<SyncReportModel> {
        val reports = mutableListOf<SyncReportModel>()
        for (response in syncResponse) {
            val searchCollaborator = grooves.filter { it.id == response.id }
            if (searchCollaborator.isNotEmpty()) {
                val collaborator = database.getCollaboratorControlQuality(searchCollaborator.first().collaboratorId)
                if (reports.filter { collaborator.collaboratorId == it.offline_id }.isEmpty()) {
                    val total = syncResponse.filter { it.issueId == response.issueId }.size
                    val complete = syncResponse.filter { it.issueId == response.issueId && it.status }.size
                    reports.add(SyncReportModel(
                            collaborator.name,
                            total,
                            complete,
                            collaborator.collaboratorId
                    ))
                }
            }
        }
        return reports
    }

    private fun updateSyncErrors(grooves: List<SyncControlQuality>) {
        Log.d(TAG, "updateSyncErrors")
        doAsync({
            Log.e("Error", "updateSyncErrors ${it.message}")
            delegate?.onTerminate(mutableListOf(),type, "updateSyncErrors ${it.message}")
        }, {
            grooves.map { groove -> database.updateSyncStatusControlQualityGrooves(true, groove.id.toInt()) }
        })
    }

    private fun deleteSyncGrooves(grooves: List<SyncControlQuality>) {
        Log.d(TAG, "deleteSyncGrooves")
        grooves.map { groove ->
            database.updateSyncStatusControlQualityGrooves(!groove.status, groove.id.toInt())
            if (groove.status) {
                database.deleteReviewIssue(groove.id)
            }
        }
    }

}