package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ActivityCodeModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class TimerActivityViewModel @Inject constructor(private val repository: ActivityCodeRepository,
                                                 private val preferences: UserAppPreferences,
                                                 private val apiErrors: HandleApiErrors,
                                                 private val mediator: Mediator)  {

    private val activityCodes = mutableListOf<ActivityCodeModel>()
    private var maxRange: Int = 0
    var unlimitedCodes = false
        private set
    var selectPosition = 0
        private set

    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val requestFail: BehaviorSubject<String> = BehaviorSubject.create()
    private val requestSuccess: BehaviorSubject<Boolean> = BehaviorSubject.create()

    fun initViewModelWith(models: List<ActivityCodeModel>, itemSelectedId: Long, maxRange: Int, unlimited_codes: Boolean) {
        activityCodes.addAll(models)
        for (i in 0..models.size - 1) {
            if (models[i].id == itemSelectedId) {
                selectPosition = i
            }
        }
        this.maxRange = maxRange
        this.unlimitedCodes = unlimited_codes
    }

    fun getTabName(position: Int): String {
        if (position >= activityCodes.size) {
            return "+ Código"
        }
        return activityCodes[position].code.toString()
    }

    fun isActiveTab(index: Int): Boolean {
        return index < activityCodes.size
    }

    fun getActivityCodeId(index: Int): Int {
        if (index >= activityCodes.size) {
            return -1
        }
        return activityCodes[index].id?.toInt() ?: activityCodes[index].code
    }

    fun getRemaining(index: Int) : Int{
        if (index >= activityCodes.size) {
            return -1
        }
        return activityCodes[index].remaining
    }

    fun getActivityCode(index: Int): Int {
        if (index >= activityCodes.size) {
            return -1
        }
        return activityCodes[index].code
    }

    fun getTotalCount(index: Int): String {
        if (index >= activityCodes.size) {
            return "Estado: "
        }
        var total = activityCodes[index].total ?: "0/0"
        if (total == "None") {
            total = "0/0"
        }
        return "Estado: $total"
    }

    fun getLastAvailablePosition(): Int {
        if (activityCodes.size <= 0) {
            return 0
        }
        return activityCodes.size - 1
    }

    fun getCodesSize(): Int {
        return activityCodes.size
    }

    fun getInitialCode(): Int {
        return activityCodes[0].code
    }

    fun getLastCode(): Int {
        return maxRange
    }

    private fun getActivityId(): Int {
        if (activityCodes.isEmpty()) return 0 else return activityCodes.first().activityId
    }

    private fun getStageId(): Int {
        if (activityCodes.isEmpty()) return 0 else return activityCodes.first().stageId
    }

    fun createActivityCode(): Disposable {
        return repository.createCode(preferences.token, getActivityId(), getStageId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    doAsync { repository.saveLocalCode(it) }
                    activityCodes.add(it)
                    requestSuccess.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun isThereCodesLeft(numberTabs: Int): Boolean {
        return activityCodes.size < numberTabs - 1
    }

    private fun getRemainingSpaces(): Int {
        val split = activityCodes.first().total?.split("/")
        if (split != null && split.size >= 2) {
            return split[1].toIntOrNull() ?: 0
        }
        return 0
    }

    fun createLocalActivityCode(): Disposable {
        val code = activityCodes[activityCodes.lastIndex].code + 1
        val activityCode = ActivityCodeModel(null, getActivityId(), code, getRemainingSpaces(), activityCodes.first().total, getStageId(), false)
        return repository.savePendingRequestActivityCode(activityCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    activityCode.id = it
                    activityCodes.add(activityCode)
                    requestSuccess.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    /*
     * Region get behavior subjects
     */

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestSuccess(): BehaviorSubject<Boolean> {
        return requestSuccess
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun changeStatusLot() {
        doAsync {
            //repository.changeStatusLot(mediator.bundleControlQualityItemViewModel.stageId, mediator.bundleItemActivitiesPayroll?.activityId ?: 0)
        }
    }

}