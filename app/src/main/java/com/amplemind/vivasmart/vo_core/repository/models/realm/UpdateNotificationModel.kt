package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey
import java.util.*

open class UpdateNotificationModel (
        @PrimaryKey
        @SerializedName("id")           var id: String = UUID.randomUUID().toString(),
        @SerializedName("type")         var type: String = "",
        @SerializedName("UUID")         var uuid: String? = null,
        @SerializedName("is_delete")    var isDelete: Boolean = false,
        @SerializedName("is_working")   var isWorking: Boolean = false,
        @SerializedName("order")        var order: Int = 0,
        @SerializedName("msg_data")     var msgDate: MsgData? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString() ?: UUID.randomUUID().toString(),
            parcel.readString()!!,
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeString(uuid)
        parcel.writeByte(if (isDelete) 1 else 0)
        parcel.writeByte(if (isWorking) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UpdateNotificationModel> {
        override fun createFromParcel(parcel: Parcel): UpdateNotificationModel {
            return UpdateNotificationModel(parcel)
        }

        override fun newArray(size: Int): Array<UpdateNotificationModel?> {
            return arrayOfNulls(size)
        }
    }

}

open class MsgData(
        @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("message_id") var messageId: Int? = 138,
        @SerializedName("notification_type_id") var notificationTypeId: Int? =  0,
        @SerializedName("stage_id") var stageId: Int? = 0
)