package com.amplemind.vivasmart.core.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.amplemind.vivasmart.core.repository.model.LotModel
import com.amplemind.vivasmart.core.repository.model.Stage
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface LotsDao {

    /*
     * Lots Crud
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLot(recent: LotModel)

    @Query("DELETE FROM LotModel WHERE lot_id_primary NOT IN (SELECT lot_id_primary FROM LotModel ORDER BY lot_id_primary DESC)")
    fun deleteAllLots()

    @Query("SELECT * FROM LotModel ORDER BY lot_id_primary DESC")
    fun getLocalLots() : Single<List<LotModel>>

    /*
     * Stage Crud
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStage(recent: Stage)

    @Query("DELETE FROM Stage WHERE stage_id NOT IN (SELECT stage_id FROM Stage ORDER BY stage_id DESC)")
    fun deleteAllStages()

    @Query("SELECT * FROM Stage ORDER BY lot_name ASC")
    fun getLocalStages() : List<Stage>

    @Query("UPDATE Stage SET stage_activities = :status WHERE stage_id = :stageId")
    fun updateLotStatus(status : Boolean, stageId: Int)

    @Query("UPDATE Stage SET isQAClose = :status WHERE stage_id = :stageId")
    fun closeQualityLot(stageId: Int, status: String = "Done")


}