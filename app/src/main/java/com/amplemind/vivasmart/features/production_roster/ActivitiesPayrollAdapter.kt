package com.amplemind.vivasmart.features.production_roster

import android.os.SystemClock
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemActivitiesPayrollBinding
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ActivitiesPayrollAdapter : RecyclerView.Adapter<ActivitiesPayrollAdapter.ActivitiesPayrollViewHolder>() {

    private var list = mutableListOf<ItemActivitiesPayrollViewModel>()
    private var lastClickTime: Long = SystemClock.elapsedRealtime()
    private val clickSubject = PublishSubject.create<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivitiesPayrollViewHolder {
        val binding = ItemActivitiesPayrollBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ActivitiesPayrollViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ActivitiesPayrollViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(activities: List<ItemActivitiesPayrollViewModel>) {
        list.addAll(activities)
        notifyDataSetChanged()
    }

    fun onClickItem() = clickSubject

    inner class ActivitiesPayrollViewHolder(private val binding : ItemActivitiesPayrollBinding) : RecyclerView.ViewHolder(binding.root){

        private lateinit var item : ItemActivitiesPayrollViewModel

        fun bind(item : ItemActivitiesPayrollViewModel){
                    binding.setVariable(BR.viewModel, item)
                    binding.executePendingBindings()
                    this.item = item
                    if (adapterPosition > -1){
                        binding.imageView10.isEnabled = item.active
                        binding.textView26.isEnabled = item.active
                        binding.root.isEnabled = item.active
                        binding.root.setOnClickListener {
                            if (SystemClock.elapsedRealtime() - lastClickTime > 1000){
                                clickSubject.onNext(this.item.name)
                                lastClickTime = SystemClock.elapsedRealtime()
                            }

                        }
                    }
                }

    }

}
