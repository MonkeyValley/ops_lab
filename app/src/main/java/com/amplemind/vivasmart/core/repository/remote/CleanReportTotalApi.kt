package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderSyncRequest
import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderSyncResponse
import com.amplemind.vivasmart.core.repository.response.IssuesResponse
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderResponse
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by
 *           amplemind on 7/13/18.
 */
interface CleanReportTotalApi {

    @GET("/v2/issue")
    @Headers("Content-Type: application/json")
    fun getIssues(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "crops",
            @Query("sub_category_id__in") subCategory: String = "8,9",
            @Query("order_by") order : String = "sub_category_id.asc",
            @Query("IssueCrop.crop_id") cropId: Int,
            @Query("__logic") logic: String = "AND"
    ): Observable<IssuesResponse>

    @POST("/v2/quality_carry_order")
    @Headers("Content-Type: application/json")
    fun createIssues(
            @Header("Authorization") authentication: String,
            @Body body: QualityCarryOrderRequest
    ): Observable<QualityCarryOrderResponse>

    @POST("/v2/offline/sync/quality_carry_order")
    @Headers("Content-Type: application/json")
    fun syncQualityCarryOrder(
            @Header("Authorization") authentication: String,
            @Body body: QualityCarryOrderSyncRequest
    ) : Observable<QualityCarryOrderSyncResponse>

}