package com.amplemind.vivasmart.features.gallery

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.features.fertiriego.fragment.PulseFragment
import com.amplemind.vivasmart.features.fertiriego.viewModel.HourModel
import com.amplemind.vivasmart.features.finished_product_quality.fragment.ProductQualityReviewDetailFragment
import com.amplemind.vivasmart.features.gallery.adapter.HourAdapter
import com.amplemind.vivasmart.features.gallery.viewModel.GalleryViewModel
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVars
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import javax.inject.Inject


class HourDialog : BaseDialogFragment() {

    @Inject
    lateinit var viewModel: GalleryViewModel

    fun newInstance(list: ArrayList<HourModel>): HourDialog {
        listPulse = list
        return HourDialog()
    }

    companion object {
        val TAG = HourDialog::class.java.simpleName
        var listPulse = ArrayList<HourModel>()
    }

    var rvHour: RecyclerView? = null
    private var hourAdapter = HourAdapter()
    var btnSaveHour: Button? = null
    var mView: View? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mView = LayoutInflater.from(context).inflate(R.layout.fragment_hour_dialog, null, false)
        builder.setCancelable(false)
        builder.setView(mView)

        setUpUi(mView!!)
        setupRecycler()
        addElementsHour()
        return builder.create()
    }

    private fun setUpUi(v: View) {
        rvHour = v.findViewById(R.id.rv_hour)
        btnSaveHour = v.findViewById(R.id.btn_save_hour)
        btnSaveHour!!.setOnClickListener {
            sendData()
        }
    }

    private fun setupRecycler() {
        rvHour!!.hasFixedSize()
        rvHour!!.layoutManager = LinearLayoutManager(activity)
        rvHour!!.itemAnimator = DefaultItemAnimator()
        rvHour!!.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        hourAdapter.setHasStableIds(true)
        rvHour!!.adapter = hourAdapter
    }

    private fun addElementsHour(){
        hourAdapter.addElements(listPulse!!.toList())
    }

    private fun sendData() {
        if(validateData()){
            sendResult()
        } else {
            val snackbar = Snackbar.make(mView!!, "Se requieren capturar todas las horas", Snackbar.LENGTH_SHORT)
            snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
            snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
            snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            snackbar.show()
        }
    }

    private fun validateData(): Boolean {
        listPulse.forEach {
            if(it.hour == "") return false
        }
        return true
    }

    private fun sendResult() {
        if (targetFragment == null) {
            return
        }
        val intent: Intent? = PulseFragment.newIntent(listPulse)
        targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
        dismiss()
    }

}