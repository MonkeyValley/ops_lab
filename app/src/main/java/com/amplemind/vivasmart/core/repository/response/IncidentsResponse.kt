package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.IncidentModel
import com.google.gson.annotations.SerializedName

data class IncidentsResponse (
    @SerializedName("data") val data: List<IncidentModel>
)