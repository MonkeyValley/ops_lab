package com.amplemind.vivasmart.features.scannerbarcode.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.widget.CompoundButton
import android.widget.ToggleButton
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment

class ScanFragmentDialog : BaseDialogFragment(), CompoundButton.OnCheckedChangeListener {

    private var toggle_continue: ToggleButton? = null
    private var toggle_stop: ToggleButton? = null

    private var evenListener : OnScanListEvents? = null

    fun newInstance() = ScanFragmentDialog()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_scan_continue_alert_dialog, null, false)
        builder.setView(mView)

        toggle_continue = mView.findViewById(R.id.btn_reopen_lot)
        toggle_stop = mView.findViewById(R.id.btn_cancel_lot)

        toggle_continue!!.setOnCheckedChangeListener(this)
        toggle_stop!!.setOnCheckedChangeListener(this)


        builder.setPositiveButton("Aceptar", null)
        builder.setNegativeButton("Cancelar") { _, _ ->
            dismiss()
            activity!!.finish()
        }

        val create = builder.create()

        create.setOnShowListener { _ ->
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if (toggle_continue!!.isChecked){
                            evenListener!!.onContinueScan()
                        }
                        if (toggle_stop!!.isChecked){
                            evenListener!!.onStopScan()
                        }
                    }
        }

        return create
    }

    fun setListener(listener : OnScanListEvents){
        this.evenListener = listener
    }

    override fun onCheckedChanged(view: CompoundButton?, isChecked: Boolean) {
        when (view!!.id) {
            R.id.btn_reopen_lot -> {
                if (isChecked) {
                    toggle_stop!!.isChecked = false
                }
            }

            R.id.btn_cancel_lot -> {
                if (isChecked) {
                    toggle_continue!!.isChecked = false
                }
            }
        }
    }

    interface OnScanListEvents{
        fun onContinueScan()
        fun onStopScan()
    }

}
