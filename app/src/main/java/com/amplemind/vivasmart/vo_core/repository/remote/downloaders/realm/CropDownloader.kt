package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.models.realm.CropModel
import com.amplemind.vivasmart.vo_core.repository.remote.PackagingQualityApi
import io.reactivex.Single
import io.realm.RealmObject
import javax.inject.Inject

class CropDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mPref: UserAppPreferences,
        private val mPackagingQualityApi: PackagingQualityApi
) : RealmDownloader<RealmObject?, CropModel>(realmHolder, CropModel::class.java) {

    override fun buildServerRequest(parent: RealmObject?, lastVersion: Long): Single<List<CropModel>> =
            mPackagingQualityApi.getCrops(mPref.authorizationToken)
                    .map { response ->
                        Log.e("Version: ", lastVersion.toString())
                        Log.e("Data-Crop: ", response.data.toString())
                        response.data
                    }

}