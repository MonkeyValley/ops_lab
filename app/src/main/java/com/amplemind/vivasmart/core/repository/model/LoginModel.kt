package com.amplemind.vivasmart.core.repository.model

class LoginModel {

    /**
     * Retrofit POJO
     *
     * */
    data class LoginResponse (
            val token: String
    )

}