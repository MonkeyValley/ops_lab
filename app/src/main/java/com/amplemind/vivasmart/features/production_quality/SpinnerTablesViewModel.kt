package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.model.TableGrooves

class SpinnerTablesViewModel(val model: TableGrooves) {

    var status = false

    fun changeStatus(){
        this.status = !this.status
    }

    fun getTableId(): Int {
        return model.table
    }

    fun getMaxGroves(): Int {
        return model.grooves_no
    }

    var name = "Tabla ${model.table}"


    override fun toString(): String {
        return "Tabla ${model.table}"
    }
}