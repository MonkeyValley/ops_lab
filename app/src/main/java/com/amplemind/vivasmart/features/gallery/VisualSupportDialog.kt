package com.amplemind.vivasmart.features.gallery

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.setImageUrl
import com.amplemind.vivasmart.features.gallery.viewModel.GalleryViewModel
import javax.inject.Inject


class VisualSupportDialog : BaseDialogFragment() {

    @Inject
    lateinit var viewModel: GalleryViewModel

    fun newInstance(plague_id: Int, plague_name: String, mode: Int): VisualSupportDialog {
        val args = Bundle()
        args.putInt(PLAGUEID, plague_id)
        args.putString(NAME, plague_name)
        args.putInt(MODE, mode)

        val dialog = VisualSupportDialog()
        dialog.arguments = args
        return dialog
    }

    companion object {
        val TAG = VisualSupportDialog::class.java.simpleName
        const val PLAGUEID = "plague_id"
        const val NAME = "plague_name"
        const val MODE = "mode"
    }

    var plagueId = 0
    var mode = 0
    var name = ""

    var tvTittle: TextView? = null
    var btnClose: Button? = null
    var llSelectImage: LinearLayout? = null
    var tvLvl1: TextView? = null
    var tvLvl2: TextView? = null
    var tvLvl3: TextView? = null
    var ibPreviewImage1: ImageButton? = null
    var ibPreviewImage2: ImageButton? = null
    var ibPreviewImage3: ImageButton? = null
    var ivFullImage: ImageView? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.fragment_visual_support_dialog, null, false)
        builder.setCancelable(false)
        builder.setView(mView)

        arguments()
        setUpUi(mView)
        return builder.create()
    }

    fun arguments(){
        plagueId = arguments?.getInt(PLAGUEID) ?: 0
        mode = arguments?.getInt(MODE) ?: 0
        name = arguments?.getString(NAME) ?: ""
    }

    fun setUpUi(v: View) {
        var lvl = if(mode == 2) 0 else 1
        tvTittle = v.findViewById(R.id.tv_title_plague)
        tvTittle!!.text = "Apoyo visual\n" + name

        tvLvl1 = v.findViewById(R.id.tv_lvl1)
        tvLvl2 = v.findViewById(R.id.tv_lvl2)
        tvLvl3 = v.findViewById(R.id.tv_lvl3)

        btnClose = v.findViewById(R.id.btn_close)
        btnClose!!.setOnClickListener { dismiss() }

        llSelectImage = v.findViewById(R.id.ll_select_image)
        llSelectImage!!.visibility = if(mode == 1) View.VISIBLE else View.GONE

        ivFullImage = v.findViewById(R.id.iv_full_image)
        ivFullImage!!.setImageUrl(viewModel.getImageUrl(plagueId, lvl))
        ivFullImage!!.visibility = if(mode == 1) View.GONE else View.VISIBLE

        ibPreviewImage1 = v.findViewById(R.id.ib_preview_image1)
        ibPreviewImage1!!.setImageUrl(viewModel.getImageUrl(plagueId, 1))
        ibPreviewImage1!!.setOnClickListener {
            tvLvl1!!.setTextColor(Color.BLUE)
            tvLvl2!!.setTextColor(Color.BLACK)
            tvLvl3!!.setTextColor(Color.BLACK)
            ivFullImage!!.setImageUrl(viewModel.getImageUrl(plagueId, 1))
            ivFullImage!!.visibility = View.VISIBLE
        }

        ibPreviewImage2 = v.findViewById(R.id.ib_preview_image2)
        ibPreviewImage2!!.setImageUrl(viewModel.getImageUrl(plagueId, 2))
        ibPreviewImage2!!.setOnClickListener {
            tvLvl1!!.setTextColor(Color.BLACK)
            tvLvl2!!.setTextColor(Color.BLUE)
            tvLvl3!!.setTextColor(Color.BLACK)
            ivFullImage!!.setImageUrl(viewModel.getImageUrl(plagueId, 2))
            ivFullImage!!.visibility = View.VISIBLE
        }

        ibPreviewImage3 = v.findViewById(R.id.ib_preview_image3)
        ibPreviewImage3!!.setImageUrl(viewModel.getImageUrl(plagueId, 3))
        ibPreviewImage3!!.setOnClickListener {
            tvLvl1!!.setTextColor(Color.BLACK)
            tvLvl2!!.setTextColor(Color.BLACK)
            tvLvl3!!.setTextColor(Color.BLUE)
            ivFullImage!!.setImageUrl(viewModel.getImageUrl(plagueId, 3))
            ivFullImage!!.visibility = View.VISIBLE
        }
    }

}