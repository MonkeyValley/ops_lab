package com.amplemind.vivasmart.features.reciba_quality.model

class VariatyItem{
    var id: Int? = null
    var name: String? = null

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }

    override fun toString(): String {
        return name!!
    }

    fun getId(): Int{
        return id!!
    }

}