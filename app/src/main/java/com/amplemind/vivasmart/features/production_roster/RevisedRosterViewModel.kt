package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.vo_core.repository.RevisedRosterRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.PendingLogsModel
import javax.inject.Inject

class RevisedRosterViewModel @Inject constructor(private val repository: RevisedRosterRepository) {

    lateinit var stageUuid: String
    var mode: Int = 0
    var stageTypeId: Int = 0

    fun cleanUp() =
            repository.cleanUp()

    fun getPendingLogs() =
            repository.getPendingLogs(stageUuid, stageTypeId)

    fun filterLogs(pendingLogs: List<PendingLogsModel>) =
            repository.filterLogsByMode(pendingLogs, mode)

}
