package com.amplemind.vivasmart.features.packaging_quality.clean_report

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.custom_views.koloda_cards.KolodaListener
import com.amplemind.vivasmart.core.extensions.*
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderResponse
import com.amplemind.vivasmart.features.packaging_quality.fragment.StartReportDialogFragment
import com.amplemind.vivasmart.features.packaging_quality.quality_report_result.QualityReportActivity
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.content_cleaning_report_activity.*
import javax.inject.Inject

@SuppressLint("Registered")
open class CleanReportActivity : BaseActivity(), KolodaListener {

    @Inject
    lateinit var viewModel: CleanReportViewModel

    @Inject
    lateinit var sensorManager: SensorCardReload

    private var isReload = false

    override fun onResume() {
        super.onResume()
        sensorManager.registerSensor()
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unRegisterSensor()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_cleaning_report_activity)

        setupToolbar()

        setupAdapter()

        clickActions()

        viewModel.setNumberReviews(intent.getIntExtra("total", 0))
        viewModel.getListCards().subscribe(this::loadCard).addTo(subscriptions)
        viewModel.getAgreedTutorial().addTo(subscriptions)
        viewModel.isAgreed().subscribe(this::showTutorial).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onIssuesUpdated().subscribe(this::onIssuesUpload).addTo(subscriptions)
        tv_current_item_new.text = intent.getStringExtra("crop")
        sensorManager.initSensor()

        if (true) {
            viewModel.loadCards(intent.getIntExtra("cropId", 0)).addTo(subscriptions)
        } else {
            root_cleaning_report.snackWithoutInternetMessage()
            viewModel.loadLocalCards(intent.getIntExtra("cropId", 0)).addTo(subscriptions)
        }

        listenerSensor()
    }

    private fun listenerSensor() {
        sensorManager.setListener(object : SensorCardReload.SensorListener {
            override fun shakeDetect() {
                reverseCard()
            }
        })
    }

    private fun onIssuesUpload(response: QualityCarryOrderResponse) {
        val activityIntent = Intent(this, QualityReportActivity::class.java)
        activityIntent.putExtra("cleanReport", response)
        activityIntent.putExtra("crop", intent.getStringExtra("crop"))
        startActivity(activityIntent)
        val result = Intent()
        result.putExtra("carryOrderId", intent.getIntExtra("carryOrderId", 0))
        setResult(Activity.RESULT_OK, result)
        finish()
    }

    override fun onEmptyDeck() {
        super.onEmptyDeck()
        finishList()
    }

    override fun onCardSwipedLeft(position: Int) {
        super.onCardSwipedLeft(position)
        viewModel.validIssue(position)
    }

    override fun onCardSwipedRight(position: Int) {
        super.onCardSwipedRight(position)
        viewModel.invalidIssue(position)
    }

    override fun onCardSwipedUp(position: Int) {
        super.onCardSwipedUp(position)
        viewModel.notIssuesFound(position)
        if (!viewModel.isLastCard(position)) {
            cv_koloda.removeAllViews()
            finishList()
        }
    }

    override fun onClickLeft(position: Int) {
        super.onClickLeft(position)
        viewModel.validIssue(position)
    }

    override fun onClickRight(position: Int) {
        super.onClickRight(position)
        viewModel.invalidIssue(position)
    }

    override fun onClickUp(position: Int) {
        super.onClickUp(position)
        viewModel.notIssuesFound(position)
        if (!viewModel.isLastCard(position)) {
            cv_koloda.removeAllViews()
            finishList()
        }
    }

    private fun finishList() {
        viewModel.completeReview()
        if(viewModel.isTherePendingReview()) {
            cv_koloda.reloadAdapterData()
        } else {
            cv_koloda.removeAllViews()
            showDialogSendReport()
        }
    }


    private fun showDialogSendReport() {
        val dialog = SendReportDialogFragment().newInstance()
        dialog.show(supportFragmentManager, StartReportDialogFragment.TAG)

        dialog.isCancelable = false
        dialog.setClickListener(object : SendReportDialogFragment.OnClickListenner {
            override fun returnCard() {
                finish_card.visibility = View.GONE
                cv_koloda.reloadPreviousCard()
                viewModel.onRestoreCard()
                dialog.dismiss()
            }

            override fun sendCards() {
                dialog.dismiss()
                viewModel.uploadIssues(intent.getIntExtra("unitId", 0),
                        intent.getIntExtra("carryOrderId", 0),
                        true).addTo(subscriptions)
            }
        })
    }

    fun reverseCard() {
        if (!cv_koloda.isReload()) {
            isReload = true
        }
        finish_card.visibility = View.GONE
        cv_koloda.reloadPreviousCard()
        viewModel.onRestoreCard()
    }

    /**
     *  Manager all click
     */
    private fun clickActions() {
        fb_reverse_new.addThrottle(1)
                .subscribe {
                    reverseCard()
                }.addTo(subscriptions)
        fb_no_problem_new.addThrottle(1)
                .subscribe {
                    cv_koloda.onClickRight()
                }.addTo(subscriptions)
        fb_problem_new.addThrottle(1)
                .subscribe {
                    cv_koloda.onClickLeft()
                }.addTo(subscriptions)

        fb_healthy_new.addThrottle(1)
                .subscribe {
                    cv_koloda.onClickUp()
                }.addTo(subscriptions)
        closeTutorialClick()
        closeTutorialAgreed()
    }

    private fun closeTutorialAgreed(): Disposable {
        return RxView.clicks(btn_agreed_new)
                .subscribe {
                    viewModel.saveAgreed().addTo(subscriptions)
                    closeTutorial()
                    enableControls(true)
                }.addTo(subscriptions)
    }

    private fun closeTutorialClick(): Disposable {
        return RxView.clicks(btn_close_tutorial_new)
                .subscribe {
                    closeTutorial()
                    enableControls(true)
                }.addTo(subscriptions)
    }

    private fun showTutorial(isAgreed: Boolean) {
        if (isAgreed) {
            cv_koloda.isEnabled = false
            iv_no_problem_new.onStartAnimRight()
            iv_problem_new.onStartAnimLeft()
            iv_healthy_new.onStarAnimtUp()
            iv_previous_photo_new.onShakeAnimation()
            enableControls(false)
        } else {
            closeTutorial()
        }
    }

    private fun closeTutorial() {
        ln_tutorial_new.visibility = View.GONE
        cv_koloda.isEnabled = true
    }

    private fun enableControls(isActive: Boolean) {
        fb_reverse_new.isClickable = isActive
        fb_healthy_new.isClickable = isActive
        fb_no_problem_new.isClickable = isActive
        fb_problem_new.isClickable = isActive
    }

    /**
     *  create list card
     */
    private fun loadCard(cards: List<ItemCleanViewModel>) {
        (cv_koloda.adapter as CleanReportAdapter).setData(cards)
        cv_koloda.kolodaListener = this
    }

    private fun setupAdapter() {
        cv_koloda.adapter = CleanReportAdapter(this)
    }

    /**
     *  create toolbar in view
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_clean_report_new
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = getString(R.string.clean_report_title)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }


}