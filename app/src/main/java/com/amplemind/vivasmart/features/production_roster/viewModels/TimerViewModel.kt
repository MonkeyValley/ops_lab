package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.extensions.calculatePaymentByHrs
import com.amplemind.vivasmart.core.extensions.registerObserver
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.navegation.data_mediator.MediatorNavigation
import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.core.repository.GroovesRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.model.CollaboratorsListReponse
import com.amplemind.vivasmart.core.repository.model.GroovesResultReponse
import com.amplemind.vivasmart.core.repository.request.*
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModelNew
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.jetbrains.anko.doAsync
import java.io.File
import java.util.*
import javax.inject.Inject

class TimerViewModel @Inject constructor(private val repository: ActivityCodeRepository,
                                         private val apiErrors: HandleApiErrors,
                                         private val uploadRepository: ProfileRepository,
                                         private val groovesRepository: GroovesRepository,
                                         private val mediatorNavigation: MediatorNavigation,
                                         private val mediator: Mediator) {

    private var list_collaborators = mutableListOf<TimerUserViewModel>()

    private var sections: MutableList<TimerUserSectionViewModel>? = null

    private var changeStateEmptyCollaborators = BehaviorSubject.create<Boolean>()

    private val requestFail = BehaviorSubject.create<Pair<String, Int?>>()

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val isAllPause = BehaviorSubject.create<Boolean>()
    private val isAllRestart = BehaviorSubject.create<Boolean>()
    private val successUpload = BehaviorSubject.create<List<Any>>()
    private val successFinishCollaborator = BehaviorSubject.create<Int>()
    private val successFinishAllCollaborators = BehaviorSubject.create<Boolean>()
    private val deleteCollaborator = BehaviorSubject.create<Int>()
    private val missingUnits = BehaviorSubject.create<Boolean>()
    private val successSendUnit = BehaviorSubject.create<Int>()

    private val activities = BehaviorSubject.create<List<TimerUserViewModel>>()

    private var total_format: String? = "0/0"
    private var total: Int? = null
    private var signsUnits: Int? = null

    private var code : Int? = null

    private var remaining: Int? = null

    enum class SeekControlState {
        PAUSED,
        CONTINUE,
        FINISH
    }

    var seekState = SeekControlState.PAUSED
        private set

    /*
    * Behavior Observables
    * they are used to notify the view about any change
    * */
    private val seekStateControl = BehaviorSubject.create<SeekControlState>()


    //Methods

    fun getDummyUser(): TimerUserViewModel {
        val user = TimerUserViewModel()
        user.name = "Usuario Dummy"
        user.registerId = 0
        user.setMediator(mediatorNavigation)
        return user
    }


    fun listSize(): Int {
        return list_collaborators.size
    }

    fun setSeekControlState(position: Int) {
        when (position) {
            0 -> {
                seekStateControl.onNext(seekState)
            }
            2 -> {
                seekStateControl.onNext(SeekControlState.FINISH)
            }
        }
    }

    fun pauseTimers() {
        seekState = SeekControlState.CONTINUE
    }

    fun restartTimers() {
        seekState = SeekControlState.PAUSED
    }

    /*
     * User info at position
     */

    fun getUserName(index: Int): String {
        if (index > list_collaborators.size) {
            return ""
        }
        return list_collaborators[index].name ?: ""
    }

    fun getUserTime(index: Int): String {
        if (index >= list_collaborators.size) {
            return "00:00"
        }
        return list_collaborators[index].getElapsedTime()
    }

    fun getUserWork(index: Int): String {
        if (index >= list_collaborators.size) {
            return ""
        }
        return list_collaborators[index].units.toString()
    }

    fun getUnitName(index: Int): String {
        if (index >= list_collaborators.size) {
            return ""
        }
        return list_collaborators[index].name_unit.toString()
    }

    fun getCost(index: Int): Double? {
        if (index >= list_collaborators.size) {
            return 0.0
        }
        return list_collaborators[index].cost
    }

    fun getUserPayment(cost: Double, index: Int, payment: ItemActivitiesPayrollViewModelNew.PAYMENTTYPE, sunday_rate: Boolean = false, isPractice: Boolean = false,
        isHead: Boolean = false, plantPerGroove: Int = 0): String {
        if (index >= list_collaborators.size) {
            return ""
        }
        return when (payment) {
            ItemActivitiesPayrollViewModelNew.PAYMENTTYPE.HRS -> {
                val paymentTotal = calculatePaymentByHrs((list_collaborators[index].getMinutesTime().toFloat() / 60f), cost.toFloat())
                "$%.2f".format(Locale.US,if (sunday_rate) isCostSunday(paymentTotal.toDouble()) else paymentTotal)
            }
            else -> {
                var result = list_collaborators[index].units * cost
                if (isHead) {
                    result *= plantPerGroove
                }
                "$%.2f".format(Locale.US,if (sunday_rate) isCostSunday(result) else result)
            }
        }
    }

    private fun isCostSunday(cost: Double): Double {
        return cost * 1.5
    }

    fun getUserTrainingPayment(trainingCost: Double?, regularCost: Double, index: Int,
                               payment: ItemActivitiesPayrollViewModelNew.PAYMENTTYPE): String {
        if (trainingCost != null) {
            return getUserPayment(trainingCost, index, ItemActivitiesPayrollViewModelNew.PAYMENTTYPE.HRS)
        }
        return getUserPayment(regularCost, index, payment)
    }

    fun getUserPracticePayment(practiceCost: Double?, regularCost: Double, index: Int,
                               payment: ItemActivitiesPayrollViewModelNew.PAYMENTTYPE): String {
        if (practiceCost != null) {
            return getUserPayment(practiceCost, index, ItemActivitiesPayrollViewModelNew.PAYMENTTYPE.HRS)
        }
        return getUserPayment(regularCost, index, payment)
    }

    fun getPaymentByCode(cost: Double): String {
        return "$ %.2f por:".format(Locale.US,cost)
    }

    fun getNameUnit(unit: String, payment: ItemActivitiesPayrollViewModelNew.PAYMENTTYPE): String {
        return if (payment == ItemActivitiesPayrollViewModelNew.PAYMENTTYPE.HRS) "Hora" else unit
    }

    //region BehaviorSubject
    fun getSeekStateControl(): BehaviorSubject<SeekControlState> {
        return seekStateControl
    }

    fun setActivitLogToCollaborator(activity_code: Int, collaboratorId: Int, activityLogId: Long, quantity: Int, isGroove : Boolean = false) {
        val results = list_collaborators.filter { it.getCollaboratorId() == collaboratorId }
        if (!results.isEmpty()) {
            results.first().activityLogId = activityLogId
            results.first().showErrorMissingUnits = false
            if (isGroove) addRemainings(results.first().units)
            results.first().units = quantity
            if (isGroove) lessRemaining(quantity)
        }
        repository.updateActivityCodeQuantity(list_collaborators.sumBy { it.units }, activity_code, getCountTotal(), getRemainigs())
    }

    fun loadActivitiesByCode(activity_code_id: Int, stage_id: Int, activity_code: Int, activity_id: Int): Disposable {
        return repository.getActivitiesByCode(activity_code_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    doAsync { repository.saveCollaborators(it.data, stage_id, activity_code, activity_id) }
                    mapModeltoViewModel(it.data, activity_code_id)
                }
                .subscribe({
                    notifyCollaborators(it)
                }, {
                    requestFail.onNext(Pair("Error ${it.localizedMessage}", null))
                })
    }

    fun getLocalCollaborators(activity_code: Int): Disposable {
        return repository.getLocalCollaborators(activity_code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it ->
                    it.map { collaborator ->
                        if (collaborator.date > 0) {
                            collaborator.timerCount = ((System.currentTimeMillis() - collaborator.date) / 1000).toDouble()
                        }
                    }
                    mapModeltoViewModel(it, activity_code) }
                .subscribe({
                    notifyCollaborators(it)
                }, {
                    requestFail.onNext(Pair("Error ${it.localizedMessage}", null))
                })
    }

    private fun notifyCollaborators(collaborators: List<TimerUserViewModel>) {
        if (collaborators.isNotEmpty()) {
            list_collaborators.addAll(collaborators)
            activities.onNext(list_collaborators)
        }else{
            changeStateEmptyCollaborators.onNext(true)
        }
    }

    fun changeStateEmptyCollaborators(): BehaviorSubject<Boolean> {
        return changeStateEmptyCollaborators
    }

    fun addCollaborators(collaborators: List<CollaboratorsListReponse>, activityCode: Int) {
        list_collaborators.addAll(mapModeltoViewModel(collaborators, activityCode))
        activities.onNext(list_collaborators)
    }

    fun addListCollaborators(users: List<TimerUserViewModel>) {
        list_collaborators.addAll(users)
    }

    private fun mapModeltoViewModel(data: List<CollaboratorsListReponse>, activityCode: Int): List<TimerUserViewModel> {
        val models = mutableListOf<TimerUserViewModel>()
        data.forEach {
            val viewModel = TimerUserViewModel()
            viewModel.setActivities(it)
            viewModel.setMediator(mediatorNavigation)
            if (it.collaborator.activityLogs != null) {
                val activitiesLog = it.collaborator.activityLogs!!.filter { it.activity_code_id == activityCode && !it.is_pending }
                if (activitiesLog.isNotEmpty()) {
                    viewModel.activityLogId = activitiesLog.first().id
                    viewModel.units = activitiesLog.first().quantity
                }
            }
            if (!isExist(it.collaborator.id ?: it.collaborator.employeeCode.toInt())) {
                models.add(viewModel)
            }
        }
        return models
    }

    private fun isExist(id_collaborator: Int): Boolean {
        return list_collaborators.any { it.getCollaboratorId() == id_collaborator }
    }

    fun canFinalizeCollaborator(atPosition: Int): Boolean {
        if (list_collaborators.isEmpty()) return false
        return list_collaborators[atPosition].activityLogId != null
    }

    fun allTimersAreRunning(): Boolean {
        if (list_collaborators.isEmpty()) return false
        return list_collaborators.all { it.timeIsRunning }
    }

    fun allCallaboratorsHaveTime(): Boolean {
        if (list_collaborators.isEmpty()) return false
        return list_collaborators.size == list_collaborators.filter { it.time > 0 }.size
    }

    fun setUserErrorMissingUnits(showError: Boolean, position: Int?) {
        if (position == null) {
            for (item in list_collaborators) {
                item.showErrorMissingUnits = showError
            }
        } else if (position < list_collaborators.size) {
            list_collaborators[position].showErrorMissingUnits = showError
        }
    }

    fun finishCollaborator(token: String, position: Int, sign: String?, activityCode: Int?, isTraining: Boolean, isPractice: Boolean): Disposable {
        val finishCollaborator = FinishCollaborator(list_collaborators[position].getActivityCodeCollaboratorId())
        val updateCollaborator = UpdateCollaborator(list_collaborators[position].activityLogId!!, list_collaborators[position].time, sign, isTraining, isPractice)
        return finishCollaborators(listOf(finishCollaborator), listOf(updateCollaborator), token, position, activityCode)
    }

    fun finishAllCollaborator(token: String, sign: String?, activityCode: Int): Disposable? {
        val updateCollaborators = mutableListOf<UpdateCollaborator>()
        val finishCollaborators = mutableListOf<FinishCollaborator>()
        for (item in list_collaborators) {
            if (item.activityLogId != null) {
                updateCollaborators.add(UpdateCollaborator(item.activityLogId!!, item.time, sign, false, false))
                finishCollaborators.add(FinishCollaborator(item.getActivityCodeCollaboratorId()))
            }
        }
        if (updateCollaborators.size > 0) {
            return finishCollaborators(finishCollaborators, updateCollaborators, token, null, activityCode)
        }
        missingUnits.onNext(true)
        return null
    }

    /*
    fun finishAllLocalCollaborator(activityCode: Int): Disposable? {
        val finishCollaborators = list_collaborators.filter { it.activityLogId != null }
        if (finishCollaborators.size > 0) {
            return finishListLocalCollaborators(finishCollaborators, null, false, false, null, activityCode)
        }
        missingUnits.onNext(true)
        return null
    }
    */

    private fun finishCollaborators(collaborators: List<FinishCollaborator>, updateCollaborators: List<UpdateCollaborator>,
                                    token: String, position: Int?, activityCode: Int?): Disposable {
        return repository.finishCollaborator(token, collaborators)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    updateCollaborator(updateCollaborators, token, position, activityCode)
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), position))
                })
    }

    fun restartCollaboratorTimer(token: String, position: Int, hasInternet: Boolean): Disposable {
        val collaborator = RestartTimerCollaborator(list_collaborators[position].getActivityCodeCollaboratorId())
        isAllRestart.onNext(allTimersAreRunning())
        if (hasInternet) {
            return restartTimerRequest(token, listOf(collaborator))
        }
        return restartTimerOffLine(listOf(collaborator))
    }

    fun restartAllCollaboratorsTimer(token: String, hasInternet: Boolean): Disposable {
        val collaborators = mutableListOf<RestartTimerCollaborator>()
        for (item in list_collaborators) {
            collaborators.add(RestartTimerCollaborator(item.getActivityCodeCollaboratorId()))
        }
        if (hasInternet) {
            return restartTimerRequest(token, collaborators)
        }
        return restartTimerOffLine(collaborators)
    }

    fun pauseCollaborator(token: String, position: Int, hasInternet: Boolean): Disposable {
        val collaborator = PauseTimerCollaborator(list_collaborators[position].registerId!!, list_collaborators[position].time)
        isAllPause.onNext(allPause())
        if (hasInternet) {
            return pauseCollalaboratorsRequest(token, listOf(collaborator))
        }
        return pauseLocalCollalaborators(listOf(collaborator))
    }

    fun allPause(): Boolean {
        return list_collaborators.none { it.timeIsRunning }
    }

    fun isAllPause(): BehaviorSubject<Boolean> {
        return isAllPause
    }

    fun isAllRestart(): BehaviorSubject<Boolean> {
        return isAllRestart
    }

    fun pauseAllCollaborator(token: String, hasInternet: Boolean) : Disposable {
        val collaborators = mutableListOf<PauseTimerCollaborator>()
        for (item in list_collaborators) {
            collaborators.add(PauseTimerCollaborator(item.registerId!!, item.time))
        }
        if (hasInternet) {
            return pauseCollalaboratorsRequest(token, collaborators)
        }
        return pauseLocalCollalaborators(collaborators)
    }

    private fun pauseCollalaboratorsRequest(token: String, collaborators: List<PauseTimerCollaborator>): Disposable {
        return repository.pauseTimer(token, PauseTimerCollaboratorRequest(collaborators))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({

                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })
    }

    private fun restartTimerRequest(token: String, collaborators: List<RestartTimerCollaborator>): Disposable {
        return repository.restartTimer(token, RestartTimerCollaboratorRequest(collaborators))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({

                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })
    }

    private fun restartTimerOffLine(collaborators: List<RestartTimerCollaborator>): Disposable {
        return repository.restartLocalCollaborators(collaborators)
                .registerObserver(progressStatus)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })

    }

    private fun pauseLocalCollalaborators(collaborators: List<PauseTimerCollaborator>): Disposable {
        return repository.pauseLocalCollaborators(collaborators)
                .registerObserver(progressStatus)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })
    }

    private fun updateCollaborator(collaborators: List<UpdateCollaborator>, token: String, position: Int?, activityCode: Int?): Disposable {
        return repository.updateCollaborator(token, collaborators, activityCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    if (position == null) {
                        removeCollaboratorsFromList(collaborators)
                        successFinishAllCollaborators.onNext(list_collaborators.isEmpty())
                    } else {
                        successFinishCollaborator.onNext(position)
                    }
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), position))
                })
    }

    private fun removeCollaboratorsFromList(collaborators: List<UpdateCollaborator>) {
        collaborators.map { collaborator ->
            val collaboratorsInList = list_collaborators.filter { it.activityLogId != null && it.activityLogId == collaborator.id }
            if (!collaboratorsInList.isEmpty()) {
                list_collaborators.remove(collaboratorsInList.first())
            }
        }
    }

    fun deleteCollaborator(token: String, atPosition: Int): Disposable {
        return repository.deletCollaborator(token, list_collaborators[atPosition].registerId!!, list_collaborators[atPosition].activityLogId ?: 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    addRemainings(list_collaborators[atPosition].units)
                    deleteCollaborator.onNext(atPosition)
                    isAllPause.onNext(allPause())
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), atPosition))
                })
    }

    fun deleteLocalCollaborator(atPosition: Int): Disposable {
        val collaborator = list_collaborators[atPosition]
        return repository.deleteLocalCollaborator(collaborator.registerId!!.toLong(),
                collaborator.activityLogId ?: 0, collaborator.isCreatedOffline)
                .registerObserver(progressStatus)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    addRemainings(list_collaborators[atPosition].units)
                    deleteCollaborator.onNext(atPosition)
                    isAllPause.onNext(allPause())
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })
    }

    fun uploadSign(file: File, token: String, position: Int, isTraining: Boolean, isPractice: Boolean): Disposable {
        return uploadRepository.uploadImage(file, "user")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    file.delete()
                    val result = mutableListOf<Any>()
                    result.add(it.relative_url)
                    result.add(position)
                    result.add(isTraining)
                    result.add(isPractice)
                    successUpload.onNext(result)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), position))
                })
    }


    fun onSuccessUpload(): BehaviorSubject<List<Any>> {
        return successUpload
    }

    fun onSuccessFinishCollaborator(): BehaviorSubject<Int> {
        return successFinishCollaborator
    }

    fun onSuccessFinishAllCollaborators(): BehaviorSubject<Boolean> {
        return successFinishAllCollaborators
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<Pair<String, Int?>> {
        return requestFail
    }

    fun onDeleteCollaborator(): BehaviorSubject<Int> {
        return deleteCollaborator
    }

    fun getListActivitiesByCode(): BehaviorSubject<List<TimerUserViewModel>> {
        return activities
    }

    fun onMissingUnits(): BehaviorSubject<Boolean> {
        return missingUnits
    }

    fun sendUnit(quantity: Int, previous : Int, collaborator_id: Int, activity_code: Int, time: Long, date: String, is_training: Boolean, sign: String?, position: Int): Disposable {
        return if (list_collaborators[position].activityLogId == null) {
            addRemainings(previous)
            assingUnitCollaborator(quantity, collaborator_id, activity_code, time, date, is_training, sign, position)
        } else {
            addRemainings(previous)
            addMoreUnits(position, quantity, activity_code)
        }
    }

    fun sendLocalUnit(quantity: Int, previous : Int, collaborator_id: Int, activity_code: Int, time: Long, date: String, is_training: Boolean, sign: String?, position: Int): Disposable {
        return if (list_collaborators[position].activityLogId == null) {
            addRemainings(previous)
            assignUnitLocalCollaborator(quantity, collaborator_id, activity_code, time, date, is_training, sign, position)
        } else {
            addRemainings(previous)
            addMoreUnitsToLocalCollaborator(position, quantity, activity_code)
        }
    }

    fun addRemainings(remaining: Int){
        this.remaining = this.remaining!! + remaining
    }

    fun getRemainigs(): Int {
        return remaining ?: 0
    }

    /**
     *  assign the units to the collaborator and return the activity_log
     */
    private fun assingUnitCollaborator(quantity: Int, collaborator_id: Int, activity_code: Int
                                       , time: Long, date: String, is_training: Boolean, sign: String?, position: Int): Disposable {
        val request =
                ActivityCodeRepository.FinishCollaboratorUnitRequest(
                        collaborator_id, activity_code, quantity, time, date, is_training, sign)

        return repository.assingCollaboratorUnit(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    lessRemaining(it.quantity)
                    setActivitLogToCollaborator(activity_code, collaborator_id, it.id, it.quantity)
                    successSendUnit.onNext(position)
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })
    }

    private fun assignUnitLocalCollaborator(quantity: Int, collaborator_id: Int, activity_code: Int
                                       , time: Long, date: String, is_training: Boolean, sign: String?, position: Int): Disposable {

        val activityLog = GroovesResultReponse(activity_code, collaborator_id, date,
                System.currentTimeMillis(), is_training, false, false, quantity, sign ?: "No Sign", time.toInt())
        return groovesRepository.saveLocalGrooves(null, activityLog, collaborator_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    lessRemaining(quantity)
                    setActivitLogToCollaborator(activity_code, collaborator_id, it, quantity)
                    successSendUnit.onNext(position)
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })
    }

    fun lessRemaining(remaining: Int){
        this.remaining = this.remaining!! - remaining
    }

    /**
     *  edit the number of units the user has
     */
    private fun addMoreUnits(position: Int, quantity: Int, activity_code: Int): Disposable {
        val request = AddMoreUnitCollaborator(list_collaborators[position].activityLogId!!, quantity)
        return repository.addUnitsCollaborator(listOf(request))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    lessRemaining(quantity)
                    list_collaborators[position].units = quantity
                    repository.updateActivityCodeQuantity(list_collaborators.sumBy { it.units }, activity_code, getCountTotal(), getRemainigs())
                    successSendUnit.onNext(position)
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })
    }

    private fun addMoreUnitsToLocalCollaborator(position: Int, quantity: Int, activity_code: Int): Disposable {
        val collaborator = list_collaborators[position]
        return groovesRepository.updateActivityLogQuantity(collaborator.activityLogId!!, quantity, collaborator.getCollaboratorId(),
                collaborator.getActivityCode())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    lessRemaining(quantity)
                    list_collaborators[position].units = quantity
                    repository.updateActivityCodeQuantity(list_collaborators.sumBy { it.units }, activity_code, getCountTotal(), getRemainigs())
                    successSendUnit.onNext(position)
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), 0))
                })
    }

    fun onSuccessUnit(): BehaviorSubject<Int> {
        return successSendUnit
    }


    fun getCountTotal(): String? {
        changeAvalibleTotal()
        return total_format
    }

    //Destroy handlers
    fun onDestroy() {
        list_collaborators.map { it.onDestroy() }
    }

    /**
     *  if list_collaborators is empty show remaining + total
     *  if list_collaborators not empty show calculate units assig
     */
    private fun changeAvalibleTotal() {
        val total = if (this.total_format!!.contains("/")) this.total_format!!.split(" ", "/")[1] else "0"
        this.total_format = "Estado: ${getAvalible()}/$total"
    }

    private fun getAvalible(): Int {
        val totalUnits = if (this.total_format!!.contains("/")) this.total_format!!.split(" ", "/")[1] else "0"
        return (totalUnits.toInt() - remaining!!)
    }

    fun getBusy(index: Int): Int {
        return remaining!! + list_collaborators[index].units
    }

    fun setCountTotal(totals: String) {
        this.total_format = totals
    }

    fun setRemaining(remaining: Int?) {
        this.remaining = remaining
    }

    fun getCountUnit(position: Int): Int {
        return list_collaborators[position].units
    }

}


