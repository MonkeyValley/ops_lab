package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class RoleModel(
        @SerializedName("id")   var id: Long = 0,
        @SerializedName("name") var name: String = ""
) : RealmObject(), Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString()!!) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RoleModel> {
        override fun createFromParcel(parcel: Parcel): RoleModel {
            return RoleModel(parcel)
        }

        override fun newArray(size: Int): Array<RoleModel?> {
            return arrayOfNulls(size)
        }
    }
}