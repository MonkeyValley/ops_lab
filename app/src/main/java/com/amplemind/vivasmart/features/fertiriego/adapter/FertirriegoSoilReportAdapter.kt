package com.amplemind.vivasmart.features.fertiriego.adapter

import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.extensions.setEnableFont
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_DATE
import com.amplemind.vivasmart.databinding.ItemSoilReportBinding
import com.amplemind.vivasmart.features.fertiriego.viewModel.FertirriegoHydroponicMonitoringViewModel
import com.amplemind.vivasmart.features.fertiriego.viewModel.SoilReportValveModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveFertirriego
import io.reactivex.subjects.PublishSubject
import io.realm.Realm
import java.util.*
import javax.inject.Inject

class FertirriegoSoilReportAdapter : (RecyclerView.Adapter<FertirriegoSoilReportAdapter.FertirriegoSoilReportViewHolder>)() {

    var lotId = 0
    var groove = 0
    var week = 0
    var table = "0"
    var plant = 0

    fun setData(lotId: Int, groove: Int, week: Int, table: String, plant: Int) {
        this.lotId = lotId
        this.groove = groove
        this.week = week
        this.table = table
        this.plant = plant
    }

    private var list = listOf<SoilReportValveModel>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    fun addElements(data: List<SoilReportValveModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FertirriegoSoilReportViewHolder {
        val binding = ItemSoilReportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FertirriegoSoilReportViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: FertirriegoSoilReportViewHolder, position: Int) {
        holder.bind(list[position])

    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class FertirriegoSoilReportViewHolder(private val binding: ItemSoilReportBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: SoilReportValveModel

        fun bind(item: SoilReportValveModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.tvValveName.text = getValveName(item.valveId)

            binding.etPulse.setOnFocusChangeListener { view, foscus ->
                if (foscus && binding.etPulse.text.isEmpty())  binding.etPulse.requestFocus()
            }

            binding.etM3.setOnFocusChangeListener { view, foscus ->
                if (foscus && binding.etPulse.text.isEmpty())  binding.etPulse.requestFocus()
            }

            var valveId = 0

            if(validateDate()){
                binding.etPulse.isEnable(true)
                binding.etM3.isEnable(true)
            } else {
                binding.etPulse.isEnable(false)
                binding.etM3.isEnable(false)
            }

            item.valveForTable!!.forEach { valve ->
                if (valve.meditionType == 1) {
                    valveId = valve.valveId
                    binding.tv6Am.text = if (valve.medition6 != null) valve.medition6.toString() else "-"
                    binding.tv12Am.text = if (valve.medition12 != null) valve.medition12.toString() else "-"
                    binding.tv18Am.text = if (valve.medition18 != null) valve.medition18.toString() else "-"
                    binding.tv24Am.text = if (valve.medition24 != null) valve.medition24.toString() else "-"
                    binding.etBarAm.text = if(valve.barrena != 0) valve.barrena.toString() else "-"
                    if(valve.m3ha != null){
                        binding.etM3.setText(valve.m3ha.toString())
                        binding.etMins.setText(valve.minutes.toString())
                        binding.etPulse.setText(valve.pulsesNo.toString())

                        binding.etM3.isEnabled = false
                        binding.etPulse.isEnabled = false

                        item.active = false
                    } else {
                        binding.etM3.setText("")
                        binding.etMins.setText("")
                        binding.etPulse.setText("")
                    }
                } else {
                    binding.tv6Pm.text = if (valve.medition6 != null) valve.medition6.toString() else "-"
                    binding.tv12Pm.text = if (valve.medition12 != null) valve.medition12.toString() else "-"
                    binding.tv18Pm.text = if (valve.medition18 != null) valve.medition18.toString() else "-"
                    binding.tv24Pm.text = if (valve.medition24 != null) valve.medition24.toString() else "-"
                    binding.etBarPm.text = if(valve.barrena != 0) valve.barrena.toString() else "-"
                }
            }

            binding.etPulse.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (binding.etPulse.text.isNotEmpty() && binding.etM3.text.isNotEmpty()) {
                        binding.etMins.setText(calculateMinutesOfWatering(
                                valveId,
                                binding.etM3.text.toString().toInt(),
                                binding.etPulse.text.toString().toInt()
                        ))

                        if(binding.etM3.text.toString().toInt() != 0 && binding.etPulse.text.toString().toInt() != 0) {
                            item.m3ha = binding.etM3.text.toString().toInt()
                            item.minutes = binding.etMins.text.toString().toDouble()
                            item.pulsesNo = binding.etPulse.text.toString().toInt()
                        }
                    } else {
                        binding.etMins.setText("")
                        item.m3ha = 0
                        item.pulsesNo = 0
                    }
                }
            })

            binding.etM3.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (binding.etM3.text.isNotEmpty() && binding.etPulse.text.isNotEmpty()) {
                        binding.etMins.setText(calculateMinutesOfWatering(
                                valveId,
                                binding.etM3.text.toString().toInt(),
                                binding.etPulse.text.toString().toInt()
                        ))

                        if(binding.etM3.text.toString().toInt() != 0 && binding.etPulse.text.toString().toInt() != 0) {
                            item.m3ha = binding.etM3.text.toString().toInt()
                            item.minutes = binding.etMins.text.toString().toDouble()
                            item.pulsesNo = binding.etPulse.text.toString().toInt()
                        }
                    } else {
                        binding.etMins.setText("")
                        item.m3ha = 0
                        item.pulsesNo = 0
                    }
                }
            })

        }
    }

    private fun getValveName(valveId: Int?): String =
            Realm.getDefaultInstance().use {
                it.where(ValveFertirriego::class.java)
                        .equalTo("valveId", valveId)
                        .findFirst()!!.valveName
            }

    private fun calculateMinutesOfWatering(valveId: Int, m3ha: Int, pulse: Int): String {
        val valveExpense = Realm.getDefaultInstance().use {
            it.where(ValveFertirriego::class.java)
                    .equalTo("valveId", valveId)
                    .findFirst()?.valveExpense
                    ?: 0.0
        }
        return if(m3ha != 0 && pulse != 0) String.format("%.2f", (((m3ha)/valveExpense)/pulse)) else ""
    }

    fun validateDate(): Boolean {
        val calendar = Calendar.getInstance()
        var month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        var day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val todayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
        return FERTIRRIEGO_DATE == todayDate
    }

}
