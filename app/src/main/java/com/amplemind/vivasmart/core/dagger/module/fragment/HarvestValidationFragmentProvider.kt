package com.amplemind.vivasmart.core.dagger.module.fragment

import com.amplemind.vivasmart.core.base.dagger.scope.FragmentScope
import com.amplemind.vivasmart.features.packaging_quality.fragment.HarvestValidationCarryOrderListFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.HarvestValidationFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HarvestValidationFragmentProvider {

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindHarvestValidationFragment(): HarvestValidationFragment


    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindValidatedCarryOrderListFragment(): HarvestValidationCarryOrderListFragment

}
