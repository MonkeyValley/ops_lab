package com.amplemind.vivasmart.features.production_quality

import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.FinishedProductRepository
import com.amplemind.vivasmart.core.repository.response.FinishedProductResult
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class ReportQualityFinishedProductViewModel @Inject constructor(
        private val repository: FinishedProductRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val list = mutableListOf<ItemReportFinishedProductViewModel>()

    private val requestSuccess = BehaviorSubject.create<List<ItemReportFinishedProductViewModel>>()

    fun getReportList(day: String): Disposable {
        return repository.getReportList(day)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel)
                .subscribe({
                    list.clear()
                    list.addAll(it)
                    requestSuccess.onNext(list)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                    Log.e("getReportList", "error", it)
                })
    }


    fun mapModelToViewModel(result : List<FinishedProductResult>) : List<ItemReportFinishedProductViewModel>{
        val viewModels = mutableListOf<ItemReportFinishedProductViewModel>()
        result.forEach { obj ->
            viewModels.add(ItemReportFinishedProductViewModel(obj))
        }
        viewModels.sortBy { it.id }
        viewModels.reverse()
        return viewModels
    }

    fun onSuccessRequest(): BehaviorSubject<List<ItemReportFinishedProductViewModel>> {
        return requestSuccess
    }

}