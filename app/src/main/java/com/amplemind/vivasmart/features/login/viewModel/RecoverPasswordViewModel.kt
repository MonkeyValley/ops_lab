package com.amplemind.vivasmart.features.login.viewModel

import com.amplemind.vivasmart.core.extensions.isValidEmail
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.model.RecoverPasswordModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class RecoverPasswordViewModel @Inject constructor(private val repository: AccountRepository) {

    /*
    * Behavior Observables
    * they are used to notify the view about any change
    * */
    private val formValidation = BehaviorSubject.create<Boolean>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val onSucess: BehaviorSubject<RecoverPasswordModel.RecoverPasswordResponse> = BehaviorSubject.create()
    private val onFail: BehaviorSubject<Throwable> = BehaviorSubject.create()


    fun recoverPassword(email: String): Disposable {
        return repository.recoverPassword(AccountRepository.RecoverPassRequest(email))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({ onSucess.onNext(it) }, { onFail.onNext(it) })
    }

    fun validateForm(email: String) {
        formValidation.onNext(email.isValidEmail())
    }

    //region BehaviorSubject
    fun getFormValidation(): BehaviorSubject<Boolean> {
        return formValidation
    }

    fun observeProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun observeSuccess(): BehaviorSubject<RecoverPasswordModel.RecoverPasswordResponse> {
        return onSucess
    }

    fun observeFail(): BehaviorSubject<Throwable> {
        return onFail
    }

}
