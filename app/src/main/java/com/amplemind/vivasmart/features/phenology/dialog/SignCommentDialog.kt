package com.amplemind.vivasmart.features.phenology.dialog

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.custom_views.Signature
import com.amplemind.vivasmart.core.custom_views.SignatureListener
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyVarietiesPlantsFragment.Companion.ifConected
import com.amplemind.vivasmart.features.phenology.viewmodel.SignCommentDialogViewModel
import java.io.File
import javax.inject.Inject


class SignCommentDialog : BaseDialogFragment() {

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mPermission: AndroidPermission

    @Inject
    lateinit var viewModelComment: SignCommentDialogViewModel

    val signList = mutableListOf<File>()
    var skipSignature: Boolean = true

    companion object {
        val TAG = SignCommentDialog::class.java.simpleName
    }

    var table: TableLayout? = null
    var signature: Signature? = null
    var reload: ImageView? = null
    var headerTable: LinearLayout? = null

    var headerComment: LinearLayout? = null
    var LLcomment: LinearLayout? = null
    var textComment: TextView? = null
    var commentText: String = ""

    fun newInstance(): SignCommentDialog {
        return SignCommentDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.fragment_gallery, null, false)
        addSubscribers()
        builder.setCancelable(false)
        builder.setView(mView)

        signature = mView.findViewById(R.id.signature_gallery)
        reload = mView.findViewById(R.id.btn_reload_signature_gallery)
        headerTable = mView.findViewById(R.id.ln_orden)
        table = mView.findViewById(R.id.table)
        headerComment = mView.findViewById(R.id.ln_comment)
        LLcomment = mView.findViewById(R.id.LL_comments)
        textComment = mView.findViewById(R.id.et_comment)

        reload!!.setOnClickListener { v -> onClick(v) }

        table!!.visibility = View.GONE
        headerTable!!.visibility = View.GONE

        headerComment!!.visibility = View.VISIBLE
        LLcomment!!.visibility = View.VISIBLE

        signature!!.setOnSignatureListener(object : SignatureListener {
            override fun onDraw() {
                skipSignature = false
            }
        })

        builder.setPositiveButton("ENVIAR", null)
        builder.setNegativeButton("CANCELAR") { _, _ -> dismiss() }
        val create = builder.create()

        create.setOnShowListener {
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if (textComment!!.text.isNotEmpty()) {
                            commentText = textComment!!.text.toString()
                            if (!skipSignature) {
                                uploadSign()
                            } else {
                                showSnackBar(mView, "La firma es requerida")
                            }
                        } else {
                            showSnackBar(mView, "El comentario es requerido")
                        }
                    }
        }
        return create
    }

    fun showSnackBar(mview: View, message: String) {
        val snackbar = Snackbar.make(mview, message, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }


    fun onClick(view: View) {
        when (view.id) {
            R.id.btn_reload_signature_gallery -> {
                skipSignature = true
                signature!!.clear()
            }
        }
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(this.context!!)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(this.context!!, bitmap), image, 400f, 400f)
        return image
    }

    private fun uploadSign() {
        if (ifConected()) {
            val sign = signature!!.save()
            if (signature != null) {
                signList.add(compressImage(sign))
                viewModelComment.uploadImage(signList, "sign")
            } else {
                Toast.makeText(context, "La firma es requerida para hacer el envio", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun addSubscribers() {
        viewModelComment.onSuccessUpdateImageUrl().subscribe(this::getImagesList).addTo(subscriptions)
    }

    fun getImagesList(list: List<String>) {
        if (list.isNotEmpty())
            sendResult(list[0])
        else
            Toast.makeText(context, "Ocurrio un error al enviar la firma, intente de nuevo", Toast.LENGTH_SHORT).show()
    }

    private fun sendResult(message: String) {
        if (targetFragment == null) {
            return
        }

        val intent = activity?.intent
        intent?.putExtra("sign", message)
        intent?.putExtra("comment", commentText)
        targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
        dismiss()
    }

}
