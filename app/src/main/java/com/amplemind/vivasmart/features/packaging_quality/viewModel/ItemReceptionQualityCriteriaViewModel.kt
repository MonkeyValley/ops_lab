package com.amplemind.vivasmart.features.packaging_quality.viewModel

import androidx.databinding.ObservableField
import com.amplemind.vivasmart.vo_core.repository.models.realm.IssueModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityRevisionModel

class ItemReceptionQualityCriteriaViewModel (
        val issue: IssueModel? = null
) {

    private var issueCount: ReceptionQualityRevisionModel.IssueCountModel? = null

    constructor(issue: ReceptionQualityRevisionModel.IssueCountModel) : this(null) {
        issueCount = issue
    }

    var count: Int = 0
        get() = issueCount?.count ?: field

    val issueId: Long
        get() = issue?.id ?: issueCount?.issueId ?: 0

    val issueName: String
        get() = issue?.name ?: issueCount?.name ?: ""

    val isPermanent: Boolean
            get() = if (issue != null) {
                !issue.isCondition
            }
            else if (issueCount != null) {
                issueCount?.isPermanent ?: false
            }
            else {
                false
            }

    val countField = ObservableField<String>(count.toString())
    val qualityPercentage = ObservableField<String>("0 %")

    fun updateCountFields(sampleSize: Int) {
        countField.set(count.toString())
        qualityPercentage.set("${(count / sampleSize.toFloat() * 100).toInt()} %")
    }
}