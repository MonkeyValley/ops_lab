package com.amplemind.vivasmart.features.phenology.models.local

import com.google.gson.annotations.SerializedName

data class PhenologyResponse(
        @SerializedName("phenology_vars") var phenology_vars: List<PhenologyVars>? = null,
        @SerializedName("varieties") var varieties: List<PhenologyVarieties>? = null
)

data class PhenologyVars(
        @SerializedName("phenology_var_id") var phenology_var_id: Int,
        @SerializedName("phenology_var_name") var phenology_var_name: String? = null,
        @SerializedName("phenology_var_description") var phenology_var_description: String? = null,
        @SerializedName("phenology_var_image") var phenology_var_image: String? = null,
        @SerializedName("unit_id") var unit_id: Int,
        @SerializedName("unit_name") var unit_name: String? = null,
        @SerializedName("balance_from") var balance_from: Double,
        @SerializedName("balance_to") var balance_to: Double,
        @SerializedName("generative_from") var generative_from: Double,
        @SerializedName("generative_to") var generative_to: Double,
        @SerializedName("vegetative_from") var vegetative_from: Double,
        @SerializedName("vegetative_to") var vegetative_to: Double
)

data class PhenologyVarieties(
        @SerializedName("has_cluster") var has_cluster: Boolean,
        @SerializedName("tables") var tables: List<PhenologyTable>? = null,
        @SerializedName("variety_id") var variety_id: Int? = 0,
        @SerializedName("variety_name") var variety_name: String? = null
)

data class PhenologyLot(
        @SerializedName("lot_id") var lot_id: Int? = 0,
        @SerializedName("lot_name") var lot_name: String? = null,
        @SerializedName("stage_id") var stage_id: Int? = 0
)

data class PhenologyTable(
        @SerializedName("groove_from") var groove_from: Int? = 0,
        @SerializedName("groove_to") var groove_to: Int? = 0,
        @SerializedName("is_complete") var is_complete: Boolean? = false,
        @SerializedName("revised_plants") var revised_plants: Int = 0,
        @SerializedName("revision_id") var revision_id: Int? = 0,
        @SerializedName("sampling") var sampling: Int? = 0,
        @SerializedName("status") var status: Boolean? = false,
        @SerializedName("table_no") var table_no: Int? = 0
)

data class PhenologyPost(
        @SerializedName("stage_id") var stage_id: Int,
        @SerializedName("variety_id") var variety_id: Int,
        @SerializedName("table_no") var table_no: Int,
        @SerializedName("phenological_state") var phenological_state: String? = null,
        @SerializedName("groove_from") var groove_from: Int,
        @SerializedName("groove_to") var groove_to: Int,
        @SerializedName("sampling") var sampling: Int,
        @SerializedName("plants") var plants: List<PhenologyPlantItem>? = null
)

data class PhenologyPostRevisionResponse(
        @SerializedName("business_unit_id") var business_unit_id: Int,
        @SerializedName("comment") var comment: String? = null,
        @SerializedName("groove_from") var groove_from: Int,
        @SerializedName("groove_to") var groove_to: Int,
        @SerializedName("id") var id: Int,
        @SerializedName("is_complete") var is_complete: Boolean,
        @SerializedName("phenological_state") var phenological_state: String? = null,
        @SerializedName("sampling") var sampling: Int,
        @SerializedName("sign") var sign: String? = null,
        @SerializedName("stage_id") var stage_id: Int,
        @SerializedName("table_no") var table_no: Int,
        @SerializedName("variety_id") var variety_id: Int
)

data class PhenologyPostPlantResponse(
        @SerializedName("id") var id: Int,
        @SerializedName("phenology_revision_id") var phenology_revision_id: Int,
        @SerializedName("valve") var valve: Int
)

data class PhenologyPlantItem(
        @SerializedName("valve") var valve: Int? = 0,
        @SerializedName("vars") var vars: List<PhenologyVarItem>? = null,
        @SerializedName("cluster") var cluster: List<PhenologyClusterItem>? = null
)

data class PhenologyClusterItem(
        @SerializedName("button_status") var button_status: Int = 0,
        @SerializedName("flowering_status") var flowering_status: Int = 0,
        @SerializedName("cuaje_status") var cuaje_status: Int = 0,
        @SerializedName("cluster_no") var cluster_no: Int = 0,
        @SerializedName("fruit_no") var fruit_no: Int = 0
)

data class PhenologyVarItem(
        @SerializedName("phenology_var_id") var phenology_var_id: Int? = 0,
        @SerializedName("medition") var medition: Double? = 0.0,
        @SerializedName("phenological_state") var phenological_state: String? = null
)

data class PhenologySignComment(
        @SerializedName("sign") var sign: String,
        @SerializedName("comment") var comment: String
)

data class PhenologyDetailResponse(@SerializedName("data") val data: PhenologyDetailModel)

data class PhenologyDetailModel(
        @SerializedName("valves") var valves: List<ValvesModel>,
        @SerializedName("vars") val vars: List<VariablesModel>
)

data class ValvesModel(
        @SerializedName("date") var date: String,
        @SerializedName("valves") var valvesDetail: List<ValvesDetailModel>
)

data class ValvesDetailModel(
        @SerializedName("cluster") var cluster: Int,
        @SerializedName("has_cluster") var hasCluster: Boolean,
        @SerializedName("medition") var medition: Double,
        @SerializedName("phenology_var_id") var varId: Int,
        @SerializedName("valve") var valve: Int
)

data class VariablesModel(
        @SerializedName("var_id") var varId: Int,
        @SerializedName("var_name") var varName: String,
        @SerializedName("var_unit") var varUnit: Int,
        @SerializedName("var_unit_name") var varUnitName: String
)

data class VariableLastRecordResponse(
        @SerializedName("date") var date: String,
        @SerializedName("medition") var medition: Double
)

data class PhenologyPlant(
        @SerializedName("plant_name") var plantName: String? = "",
        @SerializedName("revision_id") var revisionId: Int? = 0,
        @SerializedName("plant") var plant: PhenologyPlantItem? = null
)