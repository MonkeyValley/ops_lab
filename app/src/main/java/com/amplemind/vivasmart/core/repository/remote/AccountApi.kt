package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.model.LoginModel
import com.amplemind.vivasmart.core.repository.model.RecoverPasswordModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UserRolModel
import io.reactivex.Observable
import retrofit2.http.*

interface AccountApi {

    @POST("v2/auth/signin")
    @Headers("Content-Type: application/json")
    fun doLogin(
            @Body body: AccountRepository.LoginRequest
    ): Observable<LoginModel.LoginResponse>

    @POST("v2/auth/recover")
    @Headers("Content-Type: application/json")
    fun recoverPassword(
            @Body body: AccountRepository.RecoverPassRequest
    ): Observable<RecoverPasswordModel.RecoverPasswordResponse>

    @POST("v2/auth/reset")
    @Headers("Content-Type: application/json")
    fun setPassword(
            @Body body: AccountRepository.SetPasswordRequest
    ): Observable<AccountRepository.SetPasswordResponse>

    @POST("v2/auth/change_pass")
    @Headers("Content-Type: application/json")
    fun changePassword(
            @Header("Authorization") authentication: String,
            @Body body: AccountRepository.ChangePasswordRequest
    ): Observable<AccountRepository.ChangePasswordResponse>

    @POST("v2/auth/check_pass")
    @Headers("Content-Type: application/json")
    fun validateCurrentPassword(
            @Header("Authorization") authentication: String,
            @Body body: AccountRepository.ValidatePasswordRequest
    ): Observable<AccountRepository.GenericResponse>

    @GET("v2/auth/check_reset/{token}")
    @Headers("Content-Type: application/json")
    fun validateDeepLinkToken(
            @Path("token") token: String
    ): Observable<AccountRepository.GenericResponse>

    @GET("v2/auth/whoami")
    @Headers("Content-Type: application/json")
    fun getUserInfo(
            @Header("Authorization") authentication: String
    ): Observable<UserModel>

    @GET("v2/auth/whoami")
    @Headers("Content-Type: application/json")
    fun getUserRol(
            @Header("Authorization") authentication: String
    ): Observable<UserRolModel>

    @PUT("/v2/user/{id}")
    @Headers("Content-Type: application/json")
    fun setAgreedTutorial(
            @Header("Authorization") authentication: String,
            @Body body: AccountRepository.UpdateAgreedTutorial,
            @Path("id") id : Int
    ) : Observable<UserModel>

    @PUT("/v2/user/{id}")
    @Headers("Content-Type: application/json")
    fun putFirebaseToken(
            @Header("Authorization") authentication: String,
            @Body body: AccountRepository.Firebasetoken,
            @Path("id") id : Int
    ) : Observable<UserModel>
}
