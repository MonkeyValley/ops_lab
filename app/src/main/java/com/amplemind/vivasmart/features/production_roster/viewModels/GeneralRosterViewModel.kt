package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModel
import javax.inject.Inject

class GeneralRosterViewModel @Inject constructor() {

    fun getMenu(): List<ItemActivitiesPayrollViewModel> {
        val menu = mutableListOf<ItemActivitiesPayrollViewModel>()
        menu.add(ItemActivitiesPayrollViewModel(PayrollActivitiesModel("", "Operaciones", R.drawable.ic_operaciones)))
        menu.add(ItemActivitiesPayrollViewModel(PayrollActivitiesModel("", "Mantenimiento", R.drawable.ic_general_mantenimiento)))
        menu.add(ItemActivitiesPayrollViewModel(PayrollActivitiesModel("", "Calidad", R.drawable.ic_general_lineas)))
        menu.add(ItemActivitiesPayrollViewModel(PayrollActivitiesModel("", "Inocuidad", R.drawable.ic_general_inocuidad)))
        menu.add(ItemActivitiesPayrollViewModel(PayrollActivitiesModel("", "Comedor", R.drawable.ic_comedor)))
        menu.add(ItemActivitiesPayrollViewModel(PayrollActivitiesModel("", "Trabajo social", R.drawable.ic_trabajosocial)))
        menu.add(ItemActivitiesPayrollViewModel(PayrollActivitiesModel("", "Seguridad", R.drawable.ic_seguridad)))
        return menu
    }

}