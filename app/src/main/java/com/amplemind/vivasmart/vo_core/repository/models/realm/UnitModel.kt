package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey


open class UnitModel(
        @Index
        @SerializedName("id") var id: Long = 0,
        @Index
        @SerializedName("name") var name: String = "",
        @SerializedName("image") var image: String? = null,

        @PrimaryKey
        var uuid: String = getUUID()
): RealmObject()