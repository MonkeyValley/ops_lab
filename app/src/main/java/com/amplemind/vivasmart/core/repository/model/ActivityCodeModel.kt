package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

@Entity
data class ActivityCodeModel(@SerializedName("id") @PrimaryKey(autoGenerate = true) var id: Long?,
                             @SerializedName("activity_id") val activityId: Int,
                             @SerializedName("code") val code: Int,
                             @SerializedName("remaining") val remaining: Int,
                             @SerializedName("total") val total: String?,
                             @SerializedName("stage_id") val stageId: Int,
                             @SerializedName("active_collaborators") var activeCollaborators: Boolean) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeInt(activityId)
        parcel.writeInt(code)
        parcel.writeInt(remaining)
        parcel.writeString(total)
        parcel.writeInt(stageId)
        parcel.writeByte(if (activeCollaborators) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActivityCodeModel> {
        override fun createFromParcel(parcel: Parcel): ActivityCodeModel {
            return ActivityCodeModel(parcel)
        }

        override fun newArray(size: Int): Array<ActivityCodeModel?> {
            return arrayOfNulls(size)
        }
    }

}