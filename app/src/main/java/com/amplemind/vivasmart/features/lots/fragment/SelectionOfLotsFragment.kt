package com.amplemind.vivasmart.features.lots.fragment

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.getSpanCount
import com.amplemind.vivasmart.core.extensions.toDp
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.utils.CROP_ID
import com.amplemind.vivasmart.core.utils.LOT
import com.amplemind.vivasmart.core.utils.LOT_ID
import com.amplemind.vivasmart.core.utils.STAGE_ID
import com.amplemind.vivasmart.features.lots.ReOpenLotDialog
import com.amplemind.vivasmart.features.lots.viewModel.SelectionLotsViewModel
import com.amplemind.vivasmart.features.quality_control.adapter.SelectionOfLotsAdapter
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import kotlinx.android.synthetic.main.content_selection_of_lots_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

class SelectionOfLotsFragment : BaseFragment() {

    companion object {
        val TAG: String = SelectionOfLotsFragment::class.java.simpleName

        fun newInstance(productionStageName: String): SelectionOfLotsFragment {
            val args = Bundle()
            args.putString("STAGE_NAME", productionStageName)
            val fragment = SelectionOfLotsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var mViewModel: SelectionLotsViewModel

    var productionStageName = ""

    private lateinit var mAdapter: SelectionOfLotsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.e("---","SelectionOfLotsFragment")
        argument()
        return inflater.inflate(R.layout.content_selection_of_lots_fragment, container, false)
    }

    fun argument(){
        productionStageName = arguments!!.getString("STAGE_NAME") ?: ""
    }

    override fun onResume() {
        super.onResume()
        setToolbar()
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewModel.cleanUp()
    }

    override fun onDestroyView() {
        rv_selection_lots.adapter = null
        super.onDestroyView()
    }

    private fun setToolbar() {

        lbl_track_title.text = "/ Nómina / Producción / $productionStageName /"

        sendSetToolbarTitleEvent(
                getString(R.string.solf_subtitle),
                getString(R.string.solf_title)
        )
    }

    private fun initRecycler() {
        rv_selection_lots.hasFixedSize()
        rv_selection_lots.layoutManager = GridLayoutManager(context, getSpanCount(context!!))
        rv_selection_lots.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

        mAdapter = SelectionOfLotsAdapter()
        rv_selection_lots.adapter = mAdapter
    }

    override fun loadData() {
        super.loadData()
        mViewModel.setStageType(productionStageName)
        mAdapter.mViewModel = mViewModel
        mViewModel.loadLots().subscribe(this::onLotsLoaded).addTo(subscriptions)
    }

    override fun setUpUI() {
        super.setUpUI()
        initRecycler()
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()
        mAdapter.onStageClicked.subscribe(this::onStageSelected).addTo(subscriptions)
    }

    private fun onLotsLoaded(data: Changes<StageModel>) {
        mAdapter.setItems(data)
    }

    private fun onStageSelected(stage: StageModel) {
        //AQUI MIPE
        //if (stage.lot?.isClosed == false) {
        if (!mViewModel.isClosed(stage.lotId)) {
            LOT_ID = stage.lotId
            CROP_ID = stage.cropId
            STAGE_ID = stage.id
            LOT = stage.lot
            sendStageSelectedEvent(stage)
        }
        else {
            showAlertReOpen(stage)
        }
    }

    private fun sendStageSelectedEvent(stage: StageModel) =
            mEventBus.send(StageSelectedEvent(stage, productionStageName))

    private fun showAlertReOpen(stage: StageModel) {
        if (activity != null) {
            val dialog = ReOpenLotDialog.newInstance()
            dialog.show(activity!!.supportFragmentManager, ReOpenLotDialog.TAG)

            dialog.setListener(object : ReOpenLotDialog.OnSelectedOption {
                override fun onReOpenLot() {
                    mViewModel.reOpenLot(stage).subscribe({
                        sendStageSelectedEvent(stage)
                        dialog.dismiss()
                    }, {error ->
                        showErrorDialog(error)
                    }).addTo(subscriptions)
                }
            })
        }
    }

    private fun onClosedStatusChanged(stages: List<StageModel>) {
        val stage = stages.first()
        if (stage.lot?.isClosed == false) {
            mEventBus
        }
    }

    //Event classes
    data class StageSelectedEvent (
            val stage: StageModel,
            val stageType: String
    )

}
