package com.amplemind.vivasmart.core.repository

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.RecibaDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.PackagingQualityApi
import com.amplemind.vivasmart.core.repository.request.UpdateCarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.UpdateSignCarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.amplemind.vivasmart.core.repository.response.*
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.LOT_ID
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.Single
import org.jetbrains.anko.doAsync
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/6/18.
 */
open class PackagingQualityRepository @Inject constructor(private val api: PackagingQualityApi,
                                                          private val authorization: ApiAuthorization,
                                                          private val preferences: UserAppPreferences,
                                                          private val database: RecibaDao) {

    fun getDataQualityPackages(): Observable<CarryOrderCondensedResponse> {
        val userInfo = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        return api.getCarryOrders(authentication = authorization.getAuthToken(preferences.token), businessUnitId = userInfo.businessUnitId
                ?: "")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getRecibaList(): Observable<CarryOrderCondensedResponse> {
        val userInfo = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        return api.getCarryOrdersReciba(authentication = authorization.getAuthToken(preferences.token), businessUnitId = userInfo.businessUnitId
                ?: "")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun saveRecibaList(list: List<CarryOrderCondensedModel>) {
        doAsync {
            database.insertRecibaCondensedList(list)
        }
    }

    fun getLocalRecibaCondensedList(showPackage: Boolean = false): Observable<CarryOrderCondensedResponse> {
        return Observable.fromCallable {
            CarryOrderCondensedResponse(database.getRecibaCondensedList(showPackage))
        }
    }

    fun deleteCarryOrder(id: Int) {
        doAsync {
            database.deleteRecibaCondensedList(id)
        }
    }

    fun getItemQualityPackages(id: Int): Observable<CarryOrdersResponse> {
        val userInfo = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        return api.getItemCarryOrder(authentication = authorization.getAuthToken(preferences.token), businessUnitId = userInfo.businessUnitId
                ?: "", id = id)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getUnits(): Observable<UnitsDataResponse> {
        val userInfo = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        return api.getUnits(authentication = authorization.getAuthToken(preferences.token), businessUnitId = userInfo.businessUnitId
                ?: "")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getWeightRestriction(): Single<List<WeightRestriction>> =
            api.getWeightRestriction(
                    authentication = authorization.getAuthToken(preferences.token),
                    businessUnitId = preferences.user.businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("WeightRestriction: ", response.data.toString())
                        response.data
                    }

    fun saveLocalUnits(units: UnitsDataResponse) {
        doAsync {
            units.data.map { list ->
                list.units.map {
                    database.insertUnit(it.unit)
                }
            }
        }
    }

    fun getLocalUnits(): Observable<UnitsDataResponse> {
        return Observable.fromCallable {
            UnitsDataResponse(listOf(UnitsResponse(database.getUnits().map { UnitsList(it) })))
        }
    }

    fun updateCarryOrder(boxNo: Double, boxCounts: List<VarietyBoxCount>, secondQualityCount: Double, weight: Double, temperature: Double, unitsId: Int, carryOrderId: Int): Observable<Any> {
        val request = UpdateCarryOrderRequest(boxNo, boxCounts, secondQualityCount, unitsId, weight, temperature)
        return api.updateCarryOrder(authorization.getAuthToken(preferences.token), carryOrderId, request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun createLocalCarryOrder(boxNo: Double, weight: Double, temperature: Double, unitsId: Int, carryOrderId: Int, sign1: String, sign2: String): Observable<Any> {
        return Observable.fromCallable {
            val request = UpdateCarryOrderSync(boxNo, unitsId, weight, temperature, sign2, sign1, carryOrderId)
            database.updateRecibaToPackage(carryOrderId)
            database.insertCarryOrderReadyToSync(request)
        }
    }

    fun syncCarryOrders(reports: List<UpdateCarryOrderSync>): Observable<UpdateCarryOrderSyncResponse> {
        val request = UpdateCarryOrderSyncRequest(reports)
        return api.syncCarryOrder(authorization.getAuthToken(preferences.token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun updateSigns(signCollaborator: String, signSuperReception: String, carryOrderId: Int): Observable<Any> {
        val request = UpdateSignCarryOrderRequest(signCollaborator, signSuperReception)
        return api.updateSignCarryOrder(authorization.getAuthToken(preferences.token), carryOrderId, request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun loadCarryOrderPending(): Observable<CarryOrderPendingResponse> {
        val userInfo = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        return api.getCarryOrdersPending(authentication = authorization.getAuthToken(preferences.token),businessUnitId =  userInfo.businessUnitId ?: "")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun loadCarryOrdersBetween(from: String, to: String): Observable<CarryOrdersResponse> {
        val userInfo = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        return api.getCarryOrdersBetween(authentication = authorization.getAuthToken(preferences.token),businessUnitId =  userInfo.businessUnitId?.toInt() ?: 1, betweenDates = "$from,$to")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun loadCarryOrdersReportBetween(from: String, to: String): Observable<CarryOrdersReportResponse> {
        val userInfo = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        return api.getCarryOrdersReportBetween(authentication = authorization.getAuthToken(preferences.token),businessUnitId =  userInfo.businessUnitId?.toInt() ?: 1, betweenDates = "$from,$to", lotId = LOT_ID)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun loadCarryOrdersForStageBetween(stage: StageModel, from: String, to: String): Observable<CarryOrdersResponse> {
        return api.getCarryOrdersForStageBetween(authorization.getAuthToken(preferences.token), lotId = stage.lot!!.id, betweenDates = "%s,%s".format(from, to))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getVarietiesByStage(stageId: Int): Single<List<VarietyModel>> =
            api.getVarietiesByStage(
                    preferences.authorizationToken,
                    stageId = stageId)

}