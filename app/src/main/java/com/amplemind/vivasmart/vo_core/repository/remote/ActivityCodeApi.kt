package com.amplemind.vivasmart.vo_core.repository.remote

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel
import com.amplemind.vivasmart.vo_core.repository.requests.CreateActivityCodeRequest
import com.amplemind.vivasmart.vo_core.repository.responses.ActivityCodeCollaboratorsResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ActivityLogsResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectResponse
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.*

interface ActivityCodeApi {

    @GET("v2/activity_code_totals/{stage_id}/{activity_id}")
    @Headers("Content-Type: application/json")
    fun getCodes(
            @Header("Authorization")    authentication: String,
            @Path("stage_id")           stageId: Int,
            @Path("activity_id")        activityId: Int,
            @Query("version")           version: Long? = null,
            @Query("sync")              sync: Boolean = true,
            @Query("limit")             limit: Int = 100000
    ): Call<ArrayList<ActivityCodeModel>>

    @GET("v2/activity_code_totals/{stage_id}")
    @Headers("Content-Type: application/json")
    fun getCodes(
            @Header("Authorization")    authentication: String,
            @Path("stage_id")           stageId: Long,
            @Query("closed_from")       closedFrom: String,
            @Query("version__gte")       version: Long? = null,
            @Query("limit")             limit: Int = 100000,
            @Query("__logic")           logic: String = "AND"
    ): Single<List<ActivityCodeModel>>

    @POST("v2/activity_code")
    @Headers("Content-Type: application/json")
    fun createCode(
            @Header("Authorization") authentication: String,
            @Query("type")           type: String = "sync",
            @Body                          body: CreateActivityCodeRequest
    ): Call<ActivityCodeModel>

    @GET("/v2/activity_code/collaborator/sync")
    @Headers("Content-Type: application/json")
    fun getActivitiesByCode(
            @Header("Authorization")    authentication: String,
            @Query("embed")             embed: String = "activity_logs",
            @Query("activity_code_id")  activityCodeId: Long,
            @Query("created_at")        createdAt: String = "",
            //@Query("is_done")                 isDone: Boolean = false,
            //@Query("__logic")                 logic: String = "AND",
            @Query("limit")             limit: Int = 100000
    ): Call<ActivityCodeCollaboratorsResponse>

    @GET("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun getActivityLogs(
            @Header("Authorization")    authentication: String,
            //@Query("embed")             embed: String = "collaborator",
            @Query("activity_code_id")  activityCodeId: Long,
            @Query("created_at__gte")   sinceDate: String,
            @Query("__logic")           logic: String = "AND",
            @Query("limit")             limit: Int = 100000
    ): Call<ActivityLogsResponse>

    @GET("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun getActivityLogsForStage(
            @Header("Authorization")            authentication: String,
            @Query("embed")                     embed: String = "activity_code",
            @Query("activity_code.stage_id")    stageId: Int,
            @Query("date__gte")                 sinceDate: String? = null,
            @Query("__logic")                   logic: String = "AND",
            @Query("limit")                     limit: Int = 100000,
            @Query("version")                   version: Long? = null
    ): Single<ObjectResponse<ActivityLogModel>>

    @GET("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun getActivityLogsForCode(
            @Header("Authorization")            authentication: String,
            //@Query("embed")                     embed: String = "activity_code",
            @Query("activity_code_id")          activityCodeId: Long,
            @Query("date__gte")                 sinceDate: String? = null,
            @Query("__logic")                   logic: String = "AND",
            @Query("limit")                     limit: Int = 100000,
            @Query("version_gte")                version: Long? = null
    ): Single<ActivityLogsResponse>

    @GET("/v2/activity_code/grooves/{activity_code_id}")
    @Headers("Content-Type: application/json")
    fun getGrooves(
            @Header("Authorization")            authentication: String,
            @Path("activity_code_id")           activityCodeId: Long
    ): Single<List<GrooveModel>>

}