package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import com.amplemind.vivasmart.vo_core.repository.models.realm.AppUserRolModel
import io.realm.DynamicRealm
import io.realm.FieldAttribute

class Version_2_7_0 : MigrationExecutable {
    override fun run(realm: DynamicRealm) {
        migrateUploadObjectModel(realm)
    }

    private fun migrateUploadObjectModel(realm: DynamicRealm) {


        val appModuleSchema = realm.schema
                .create("AppModule")
                .addField("moduleId", Int::class.javaObjectType)
                .addField("moduleName", String::class.javaObjectType)
                .addField("hasAccess", Boolean::class.javaObjectType)
                .addField("uuid", String::class.javaObjectType)
                .setRequired("uuid", true)

        val userRolModelSchema = realm.schema
                .create("AppUserRolModel")
                .addField("roleId", Int::class.javaObjectType)
                .addField("roleName", String::class.javaObjectType)
                .addRealmListField("appModules", appModuleSchema)
                .addField("uuid", String::class.javaObjectType)
                .setRequired("uuid", true)

        realm.schema
                .create("UserRolModel")
                .addField("id", Int::class.javaObjectType)
                .addField("name", String::class.javaObjectType)
                .addRealmObjectField("role", userRolModelSchema)
                .addField("uuid", String::class.javaObjectType)
                .setRequired("uuid", true)

    }

    override fun drop(realm: DynamicRealm) {

    }


}