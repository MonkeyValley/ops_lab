package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.RosterLineModel

class ItemRosterLinesViewModel constructor(val model: RosterLineModel) {

    var name = model.name
    var drawable = R.drawable.ic_line_activity
    val id_line = model.id

    val actual_stage = model.actual_stage

}