package com.amplemind.vivasmart.core.dagger.module

import com.amplemind.vivasmart.core.repository.remote.*
import com.amplemind.vivasmart.core.utils.LoginApiHolder
import com.amplemind.vivasmart.core.utils.RetrofitHolder
import com.amplemind.vivasmart.features.pollination.repository.remote.PollinationApi
import com.amplemind.vivasmart.features.production_range.repository.remote.ProductionRangeApi
import com.amplemind.vivasmart.features.reciba_quality.repository.remote.RecibaQualityApi
import com.amplemind.vivasmart.features.root_quality.remote.rootQualityApi
import com.amplemind.vivasmart.features.sync_forced.repository.remote.SyncForcedApi
import com.amplemind.vivasmart.vo_core.repository.remote.FertirriegoApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by
 *          amplemind on 7/5/18.
 */
@Module
class NetworkModule {

    @Provides
    @Singleton
    internal fun provideLoginApiHolder(retrofitHolder: RetrofitHolder): LoginApiHolder {
        return LoginApiHolder(retrofitHolder)
    }

    @Provides
    @Singleton
    internal fun provideLoginApi(retrofit: Retrofit): AccountApi {
        return retrofit.create(AccountApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideProfileApi(retrofit: Retrofit): ProfileApi {
        return retrofit.create(ProfileApi::class.java)
    }

    @Provides
    @Singleton
    internal fun providePayrollActivitiesApi(retrofit: Retrofit): PayrollActivitiesApi {
        return retrofit.create(PayrollActivitiesApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideRecoverApi(retrofit: Retrofit): RecoverPasswordApi {
        return retrofit.create(RecoverPasswordApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideSplashApi(retrofit: Retrofit): SplashApi {
        return retrofit.create(SplashApi::class.java)
    }

    @Provides
    @Singleton
    internal fun providePackagingQualityApi(retrofit: Retrofit): PackagingQualityApi {
        return retrofit.create(PackagingQualityApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideQualitySearchCodeApi(retrofit: Retrofit): QualitySearchCodeApi {
        return retrofit.create(QualitySearchCodeApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideCleaningApi(retrofit: Retrofit): CleaningApi {
        return retrofit.create(CleaningApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideProductionQualityApi(retrofit: Retrofit): ProductionQualityApi {
        return retrofit.create(ProductionQualityApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideQualityReportApi(retrofit: Retrofit): QualityReportApi {
        return retrofit.create(QualityReportApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideCleanReportTotalApi(retrofit: Retrofit): CleanReportTotalApi {
        return retrofit.create(CleanReportTotalApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideQualityControlApi(retrofit: Retrofit): QualityControlApi {
        return retrofit.create(QualityControlApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideLotApi(retrofit: Retrofit): LotApi {
        return retrofit.create(LotApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideCollaboratorsApi(retrofit: Retrofit): CollaboratorsApi {
        return retrofit.create(CollaboratorsApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideActivityCodeApi(retrofit: Retrofit): ActivityCodeApi {
        return retrofit.create(ActivityCodeApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideFinishedProductApi(retrofit: Retrofit): FinishedProductApi {
        return retrofit.create(FinishedProductApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideWasteControlApi(retrofit: Retrofit): WasteControlApi {
        return retrofit.create(WasteControlApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideReportsApi(retrofit: Retrofit): ReportsApi {
        return retrofit.create(ReportsApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideRosterApi(retrofit: Retrofit): RosterApi {
        return retrofit.create(RosterApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideHaulageApi(retrofit: Retrofit): HaulageApi {
        return retrofit.create(HaulageApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideCountBoxApi(retrofit: Retrofit): CountBoxApi {
        return retrofit.create(CountBoxApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideTimerLinesApi(retrofit: Retrofit): TimerLinesApi {
        return retrofit.create(TimerLinesApi::class.java)
    }

    @Provides
    @Singleton
    internal fun providePlanningApi(retrofit: Retrofit): PlanningApi {
        return retrofit.create(PlanningApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideComparisonApi(retrofit: Retrofit): ComparisonApi {
        return retrofit.create(ComparisonApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideCarryOrderApi(retrofit: Retrofit): CarryOrderApi {
        return retrofit.create(CarryOrderApi::class.java)
    }

    @Provides
    @Singleton
    internal fun providePhenologyApi(retrofit: Retrofit): PhenologyApi {
        return retrofit.create(PhenologyApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideCropValidationApi(retrofit: Retrofit): CropValidationApi {
        return retrofit.create(CropValidationApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideRecibaQualityApi(retrofit: Retrofit): RecibaQualityApi {
        return retrofit.create(RecibaQualityApi::class.java)
    }

    @Provides
    @Singleton
    internal fun SyncForcedApi(retrofit: Retrofit): SyncForcedApi {
        return retrofit.create(SyncForcedApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideLotFertirriegoApi(retrofit: Retrofit): FertirriegoApi {
        return retrofit.create(FertirriegoApi::class.java)
    }

    @Provides
    @Singleton
    internal fun prodiverootQualityApi(retrofit: Retrofit): rootQualityApi{
        return retrofit.create(rootQualityApi::class.java)
    }

    @Provides
    @Singleton
    internal fun prodiveProductionRangeApi(retrofit: Retrofit): ProductionRangeApi {
        return retrofit.create(ProductionRangeApi::class.java)
    }


    @Provides
    @Singleton
    internal fun prodivePollinationApi(retrofit: Retrofit): PollinationApi {
        return retrofit.create(PollinationApi::class.java)
    }
}