package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.model.IncidentModel
import java.text.SimpleDateFormat
import java.util.*

class IncidentItemViewModel constructor(incident: IncidentModel) {

    companion object {
        //Date parser. Change if remote date format changes!!
        @JvmStatic
        private val dateParser = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH)
    }

    val id              = incident.id
    val createdAt       = incident.createdAt
    val description     = incident.description
    val grooveNumber    = incident.grooveNumber
    val tableNumber     = incident.tableNumber
    val issue           = incident.issue
    val isClosed        = incident.isClosed

    fun getFormattedCreatedAt(format: String = "dd/MMM"): String {
        return SimpleDateFormat(format, Locale.getDefault()).format(dateParser.parse(createdAt))
    }
}