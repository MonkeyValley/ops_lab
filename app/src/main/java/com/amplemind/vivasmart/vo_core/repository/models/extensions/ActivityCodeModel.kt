package com.amplemind.vivasmart.vo_core.repository.models.extensions

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel

val ActivityCodeModel?.parseOccupiedSpaces
    get() = if (this?.total?.isNotEmpty() == true) total?.split("/")!![0].toInt() else 0

val ActivityCodeModel?.parseTotalSpaces
    get() = if (this?.total?.isNotEmpty() == true) total?.split("/")!![1].toInt() else 0

val ActivityCodeModel?.remainingSpaces
    get() = ((this?.totalSpaces) ?: 0) - (this?.occupiedSpaces ?: 0)
