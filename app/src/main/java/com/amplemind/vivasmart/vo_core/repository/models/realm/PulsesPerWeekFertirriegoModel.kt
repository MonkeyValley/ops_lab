package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class PulsesPerWeekFertirriegoModel (
        @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("date0") var date0: RealmList<TablesPulsePerDay>? = RealmList(),
        @SerializedName("date1") var date1: RealmList<TablesPulsePerDay>? = RealmList(),
        @SerializedName("date2") var date2: RealmList<TablesPulsePerDay>? = RealmList(),
        @SerializedName("date3") var date3: RealmList<TablesPulsePerDay>? = RealmList(),
        @SerializedName("date4") var date4: RealmList<TablesPulsePerDay>? = RealmList(),
        @SerializedName("date5") var date5: RealmList<TablesPulsePerDay>? = RealmList(),
        @SerializedName("date6") var date6: RealmList<TablesPulsePerDay>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()

) : RealmObject(), Identifiable

open class TablesPulsePerDay(
        @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("date") var date: String? = "",
        @SerializedName("T1") var T1: RealmList<PulseForTable>? = RealmList(),
        @SerializedName("T2") var T2: RealmList<PulseForTable>? = RealmList(),
        @SerializedName("T3") var T3: RealmList<PulseForTable>? = RealmList(),
        @SerializedName("T4") var T4: RealmList<PulseForTable>? = RealmList(),
        @SerializedName("T5") var T5: RealmList<PulseForTable>? = RealmList(),
        @SerializedName("T6") var T6: RealmList<PulseForTable>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class PulseForTable(
        @SerializedName("date") var date: String? = "",
        @SerializedName("drain") var drain: Int? = 0,
        @SerializedName("drain_percent") var drainPercent: Double? =  0.0,
        @SerializedName("hour") var hour: String? = "00:00AM",
        @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("minutes") var minutes: Double? = 0.0,
        @SerializedName("pulse") var pulse: Int? = 0,
        @SerializedName("sending") var sending: Int? = 0,
        @SerializedName("spike") var spike: Int? = 0,
        @SerializedName("table") var table: Int? =  0,
        @SerializedName("ph_drain") var phDrain: Double? = null,
        @SerializedName("ce_drain") var ceDrain: Double? = null,
        @SerializedName("pulseRegisted") var pulseRegisted: Int? = 0,
        @SerializedName("check") var check: String? = "",
        @SerializedName("is_calculated") var isCalculated: Boolean? = false,
        @SerializedName("uuid") override var uuid: String = getUUID(),
        @SerializedName("valveId") var valveId: Int? = null
) : RealmObject(), Identifiable

open class PulseLotSpike(
        @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("spike") var spike: Int? = 0,
        @SerializedName("table") var table: Int? = 0,
        @SerializedName("uuid") override var uuid: String = getUUID()
): RealmObject(), Identifiable
