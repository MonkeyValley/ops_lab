package com.amplemind.vivasmart.features.crop_validation.viewmodel

import com.amplemind.vivasmart.features.crop_validation.models.CropIssuesModel

class ItemCropIssuesViewModel (cropIssues: CropIssuesModel){
    var issueId = cropIssues.issueId
    var issueName = cropIssues.issueName
    var isChecked = cropIssues.is_ok ?: true
}