package com.amplemind.vivasmart.features.production_roster.adapters

import android.content.Context
import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.features.collaborators.AddCollaboratorsActivity
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserSectionViewModel
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup



class TimerUserSectionViewHolder constructor(private val view: View, private val context: Context) : GroupViewHolder(view) {

    private val title: TextView = view.findViewById(R.id.tv_section_name)
    private val totalUsers: TextView = view.findViewById(R.id.tv_section_count)
    private val arrow: ImageView = view.findViewById(R.id.arrow)
    val add: ImageView = view.findViewById(R.id.add_collaborators)

    fun setSectionTitle(group: ExpandableGroup<*>) {
        title.text = group.title
        (group as TimerUserSectionViewModel).getCollapsibleAnimObservable().subscribe { isCollapsed ->
            if (isCollapsed) {
                arrow.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_up))
            } else {
                arrow.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_down))
            }
        }
        setCountUser(group)

        group.getNotifyChange().subscribe{
               setCountUser(group)
        }
    }

    private fun setCountUser(group: ExpandableGroup<*>){
        totalUsers.text = (group as TimerUserSectionViewModel).getUsersSize().toString()
    }

}
