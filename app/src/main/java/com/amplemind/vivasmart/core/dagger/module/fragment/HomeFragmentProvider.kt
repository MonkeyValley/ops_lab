package com.amplemind.vivasmart.core.dagger.module.fragment

import com.amplemind.vivasmart.MenuActivitiesFragment
import com.amplemind.vivasmart.core.base.dagger.scope.FragmentScope
import com.amplemind.vivasmart.features.lots.fragment.SelectionOfLotsFragment
import com.amplemind.vivasmart.features.production_lines.fragment.LotsInLinesFragment
import com.amplemind.vivasmart.features.production_roster.*
import com.amplemind.vivasmart.features.production_roster.fragments.GeneralRosterFragment
import com.amplemind.vivasmart.features.production_roster.viewModels.MenuaActivitiesQualityProductionFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by aaronjags on 3/28/18.
 */
@Module
abstract class HomeFragmentProvider {

    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    @FragmentScope
    internal abstract fun bindMenuActivityFragment(): MenuActivitiesFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindPayrollProductionFragment(): PayrollProductionFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindMenuActivitiesPayrollFragment(): MenuActivitiesPayrollFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindMenuaActivitiesQualityProductionFragment(): MenuaActivitiesQualityProductionFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindMenuActivitiesProductionFragment(): MenuActivitiesProductionFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindChooseActivityFragment(): ChooseActivityFragment


    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindSelectionOfLotsFragment(): SelectionOfLotsFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindRosterLinesFragment(): RosterLinesFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindLotsInLinesFragment(): LotsInLinesFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindRosterLinesActivitiesFragment(): RosterLinesActivitiesFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindGeneralRosterFragment(): GeneralRosterFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindMenuActivitiesProductionPacking(): MenuActivitiesProductionPacking

}