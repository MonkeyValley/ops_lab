package com.amplemind.vivasmart.features.packaging_quality.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemReceptionQualityRevisionBinding
import com.amplemind.vivasmart.databinding.ItemReceptionQualityRevisionCriteriaBinding
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ItemReceptionQualityCriteriaViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ItemReceptionQualityRevisionViewModel

class ReceptionQualityRevisionAdapter : RecyclerView.Adapter<ReceptionQualityRevisionAdapter.ItemReceptionQualityRevisionViewHolder>() {

    private var mItems = listOf<ItemReceptionQualityRevisionViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ItemReceptionQualityRevisionViewHolder {
        val binding = ItemReceptionQualityRevisionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemReceptionQualityRevisionViewHolder(binding)
    }

    override fun getItemCount(): Int = mItems.size

    override fun onBindViewHolder(viewHolder: ItemReceptionQualityRevisionViewHolder, position: Int) {
        viewHolder.bind(mItems[position])
    }

    fun setItems(items: List<ItemReceptionQualityRevisionViewModel>) {
        mItems = items
        notifyDataSetChanged()
    }


    inner class ItemReceptionQualityRevisionViewHolder(
            private val binding: ItemReceptionQualityRevisionBinding) : RecyclerView.ViewHolder(binding.root)
    {

        fun bind(viewModel: ItemReceptionQualityRevisionViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()

            binding.llCondition.removeAllViews()
            binding.llPermanent.removeAllViews()

            for (issue in viewModel.revision.issues) {
                val itemBinding = if (issue.isPermanent) {
                    ItemReceptionQualityRevisionCriteriaBinding
                            .inflate(LayoutInflater.from(binding.root.context), binding.llPermanent, true)
                } else {
                    ItemReceptionQualityRevisionCriteriaBinding
                            .inflate(LayoutInflater.from(binding.root.context), binding.llCondition, true)
                }

                val itemViewModel = ItemReceptionQualityCriteriaViewModel(issue)
                itemViewModel.updateCountFields(viewModel.revision.sampleSize)

                itemBinding.setVariable(BR.viewModel, itemViewModel)
                itemBinding.executePendingBindings()
            }
        }

    }
}