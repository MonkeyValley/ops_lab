package com.amplemind.vivasmart.vo_core.repository.dao

import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import io.realm.Realm
import io.realm.Sort
import javax.inject.Inject

class UploadObjectDao @Inject constructor(private val mRealmHolder: RealmHolder){

    fun onNewUploadObject() =
        mRealmHolder.getRealm().where(UploadObjectModel::class.java)
                .findAll()
                .asFlowable()
                .toObservable()
                .doOnDispose {
                    mRealmHolder.close()
                }
                .map{
                    return@map it.size
                }
                .filter{
                    return@filter it > 0
                }

    fun peekFirst(): UploadObjectModel? {
        var results: UploadObjectModel? = null

        Realm.getDefaultInstance().use {realm ->
            val obj = realm.where(UploadObjectModel::class.java)
                    .sort("order", Sort.ASCENDING)
                    .findFirst()
            results = if (obj == null) null else realm.copyFromRealm(obj)
        }

        return results
    }

    fun remove(obj: UploadObjectModel) {
        Realm.getDefaultInstance().use {realm ->
            realm.executeTransaction {
                if (obj.isManaged) {
                    obj.deleteFromRealm()
                }
                else {
                    realm.where(UploadObjectModel::class.java)
                            .equalTo("uuid", obj.uuid)
                            .findFirst()
                            ?.deleteFromRealm()
                }
            }
        }
    }

    fun append(realm: Realm, obj: UploadObjectModel) = append(realm, listOf(obj))

    fun append(realm: Realm, objs: List<UploadObjectModel>) {
        objs.forEach {obj ->
            val order = realm.where(UploadObjectModel::class.java)
                    .max("order")?.toLong()?.inc() ?: 0L

            obj.order = order

            realm.insertOrUpdate(obj)
        }
    }

    fun setServerError(obj: UploadObjectModel, code: String?, msg: String?) {
        Realm.getDefaultInstance().use {realm ->
            realm.executeTransaction {
                obj.errorCode = code
                obj.errorServerResponse = msg

                if (!obj.isManaged)
                    realm.insertOrUpdate(obj)
            }
        }
    }

    fun update(obj: UploadObjectModel) {
        Realm.getDefaultInstance().use {realm ->
            realm.executeTransaction {
                realm.insertOrUpdate(obj)
            }
        }
    }

}