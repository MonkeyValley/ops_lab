package com.amplemind.vivasmart.features.production_roster

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemChooseActivityPackaginBinding
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ActivitiesChoosePackingAdapter constructor(val context: Context) : RecyclerView.Adapter<ActivitiesChoosePackingAdapter.ActivitiesPayrollViewHolder>() {

    private var list = mutableListOf<ItemActivitiesPayrollViewModel>()

    private val clickSubject = PublishSubject.create<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivitiesPayrollViewHolder {
        val binding = ItemChooseActivityPackaginBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ActivitiesPayrollViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ActivitiesPayrollViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(activities: List<ItemActivitiesPayrollViewModel>) {
        list.addAll(activities)
        notifyDataSetChanged()
    }

    val clickEvent: Observable<String> = clickSubject

    inner class ActivitiesPayrollViewHolder(private val binding: ItemChooseActivityPackaginBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ItemActivitiesPayrollViewModel

        fun bind(item: ItemActivitiesPayrollViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item
            if (adapterPosition > -1) {
                binding.imageView11.isEnabled = item.active
                binding.textView30.isEnabled = item.active
                binding.root.isEnabled = item.active
                binding.root.setOnClickListener {
                    clickSubject.onNext(item.name)
                }
            }
        }

    }
}