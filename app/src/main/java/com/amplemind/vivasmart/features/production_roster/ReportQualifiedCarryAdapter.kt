package com.amplemind.vivasmart.features.production_roster

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemQuantityOrderIssuesResultBinding
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.BehaviorSubject

class ReportQualifiedCarryAdapter constructor(val data: List<ItemQualifiedViewModel>) :
        RecyclerView.Adapter<ReportQualifiedCarryAdapter.ReportQualifiedViewHolder>() {

    private val onClickSubject = BehaviorSubject.create<ItemQualifiedViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportQualifiedViewHolder {
        val binding = ItemQuantityOrderIssuesResultBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReportQualifiedViewHolder(binding)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ReportQualifiedViewHolder, position: Int) {
        holder.bind(data[position])
    }

    fun onClick(): BehaviorSubject<ItemQualifiedViewModel> {
        return onClickSubject
    }

    inner class ReportQualifiedViewHolder(private val binding: ItemQuantityOrderIssuesResultBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemQualifiedViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.root.setOnClickListener {
                    onClickSubject.onNext(item)
                }
            }
        }
    }

}
