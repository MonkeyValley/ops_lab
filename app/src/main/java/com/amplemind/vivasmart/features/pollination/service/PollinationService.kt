package com.amplemind.vivasmart.features.pollination.service

import android.content.Context
import android.provider.CalendarContract.Attendees.query
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.models.realm.PollinationModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.SoilModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.toSingle
import io.realm.Realm
import io.realm.Sort
import javax.inject.Inject

class PollinationService @Inject constructor() {

    var context: Context? = null

    lateinit var preferences: UserAppPreferences

    fun sendPollination(lotId: Int, lvl0: Int, lvl1: Int, lvl2: Int, lvl3: Int, porcent0: Double,
                        porcent1: Double, porcent2: Double, porcent3: Double, pollinationPorcent: Double,
                        table: Int, week: Int, flowers: Int) {

        Realm.getDefaultInstance().use {
            var stageId = it
                    .where(StageModel::class.java)
                    .equalTo("lotId", lotId)
                    .findFirst()
                    ?.id ?: 0

            it.executeTransaction { realm ->
                var obj = realm.createObject(PollinationModel::class.java, getUUID())
                obj.stageId = stageId
                obj.lvl0 = lvl0
                obj.lvl1 = lvl1
                obj.lvl2 = lvl2
                obj.lvl3 = lvl3
                obj.porcentLvl0 = porcent0
                obj.porcentLvl1 = porcent1
                obj.porcentLvl2 = porcent2
                obj.porcentLvl3 = porcent3
                obj.pollinationPorcent = pollinationPorcent
                obj.table = table
                obj.week = week
                obj.flowers = flowers

                realm.insert(obj)


                val gson = Gson()
                val rootObject = gson.toJson(realm.copyFromRealm(obj))
                Log.e("MipeJson", rootObject)

                val prefer = context!!.getSharedPreferences(
                        context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

                val url = preferences.selectedServerUrl
                val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                uploadObject.verb = "POST"
                uploadObject.url = url + "v2/pollination"
                uploadObject.contentType = "application/json"
                uploadObject.body = rootObject.toString()
                uploadObject.token = prefer.getString("token", "") ?: ""
                uploadObject.order = 99
                realm.insertOrUpdate(uploadObject)
            }
        }
    }

    fun getPollination(lotId: Int, week: Int, table: Int): Single<PollinationModel> =
            Single.create<PollinationModel> { emitter ->
                Realm.getDefaultInstance().use {
                    var stageId = it
                            .where(StageModel::class.java)
                            .equalTo("lotId", lotId)
                            .findFirst()
                            ?.id ?: 0

                    val results = it.where(PollinationModel::class.java)
                            .equalTo("stageId", stageId)
                            .equalTo("week", week)
                            .equalTo("table", table)
                            .findFirst()

                    emitter.onSuccess(it.copyFromRealm(results!!))
                }
            }


}