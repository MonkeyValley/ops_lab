package com.amplemind.vivasmart.core.navegation

import com.amplemind.vivasmart.core.navegation.flow.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppFlow @Inject constructor() {
    internal var state: State = ProcessFlow(this)
}