package com.amplemind.vivasmart.features.crop_validation.adapter

import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemCarryOrderHarvestBinding
import com.amplemind.vivasmart.features.crop_validation.viewmodel.ItemCarryOrderHarvestViewModel
import androidx.databinding.library.baseAdapters.BR
import com.google.gson.Gson
import io.reactivex.subjects.BehaviorSubject

class CropValidationCarryOrderAdapter constructor(val list: MutableList<ItemCarryOrderHarvestViewModel>) : RecyclerView.Adapter<CropValidationCarryOrderAdapter.CropValidationCarryOrderViewHolder>() {

    private val onClickSubject = BehaviorSubject.create<ItemCarryOrderHarvestViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CropValidationCarryOrderViewHolder {
        val binding = ItemCarryOrderHarvestBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CropValidationCarryOrderViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CropValidationCarryOrderViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<ItemCarryOrderHarvestViewModel> {
        return onClickSubject
    }

    fun isEmpty() = list.isEmpty()


    inner class CropValidationCarryOrderViewHolder(private val binding: ItemCarryOrderHarvestBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemCarryOrderHarvestViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.cvUser.setOnClickListener {
                    Log.e("Click 1",  Gson().toJson(item))
                    onClickSubject.onNext(item)
                }
                binding.cvUser.setOnLongClickListener {
                    Log.e("Click 2",  Gson().toJson(item))
                    onClickSubject.onNext(item)
                    return@setOnLongClickListener true
                }
                if (item.has_harvest_quality) {
                    if (item.harvest_quality_ok) {
//                        binding.TVstatus.background = (binding.root.resources.getDrawable(R.drawable.ic_button_green))
                        binding.imgActionStatus.setImageResource(R.drawable.ic_action_ok)
                    }
                    else{
//                        binding.TVstatus.background = (binding.root.resources.getDrawable(R.drawable.ic_button_red))
                        binding.imgActionStatus.setImageResource(R.drawable.ic_action_error)
                    }
                }
                else{
//                    binding.TVstatus.background = binding.root.resources.getDrawable(R.drawable.ic_button_blue)
                    binding.imgActionStatus.setImageResource(R.drawable.ic_action_pending)
                }
            }
        }

    }

}