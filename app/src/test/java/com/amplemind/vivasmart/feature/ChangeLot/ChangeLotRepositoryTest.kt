package com.amplemind.vivasmart.feature.ChangeLot

import com.amplemind.vivasmart.core.repository.ChangeLotRepository
import org.junit.Before
import org.junit.Test

class ChangeLotRepositoryTest {

    lateinit var changeRepository : ChangeLotRepository

    @Before
    fun setup(){
        changeRepository = ChangeLotRepository()
    }

    //TODO: Esto tendria que cambiar cuando el repo contenga un api
    @Test
    fun `test get batches`(){
        //`when`(userApi.getLots()).thenReturn(Observable.just(emptyList<User>()))
        changeRepository.getLots()
                .test()
                .assertValue { it.isNotEmpty() }
    }

}