package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import com.amplemind.vivasmart.vo_core.repository.models.realm.PHCEForTable
import com.amplemind.vivasmart.vo_core.repository.models.realm.TablesPHCEPerDay
import io.realm.DynamicRealm

class Version_10 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        //createPHCEForTable(realm)
        //createTablesPHCEPerDay(realm)
        //createPHCEPerWeekFertirriegoModel(realm)
    }

    private fun createPHCEForTable(realm: DynamicRealm) {
        realm.schema.create("PHCEForTable")
                .addField("lotId", Int::class.java)
                .addField("ce", String::class.java)
                .addField("date", String::class.java)
                .addField("ph", String::class.java)
                .addField("table", Int::class.java)
                .addField("uuid", String::class.java)
    }

    private fun createTablesPHCEPerDay(realm: DynamicRealm) {
        realm.schema.create("TablesPHCEPerDay")
                .addField("lotId", Int::class.java)
                .addField("date", String::class.java)
                .addRealmListField("T1", PHCEForTable::class.java)
                .addRealmListField("T2", PHCEForTable::class.java)
                .addRealmListField("T3", PHCEForTable::class.java)
                .addRealmListField("T4", PHCEForTable::class.java)
                .addRealmListField("T5", PHCEForTable::class.java)
                .addRealmListField("T6", PHCEForTable::class.java)
                .addField("uuid", String::class.java)
    }

    private fun createPHCEPerWeekFertirriegoModel(realm: DynamicRealm){
        realm.schema.create("PHCEPerWeekFertirriegoModel")
                .addField("lotId", Int::class.java)
                .addRealmListField("date0", TablesPHCEPerDay::class.java)
                .addRealmListField("date1", TablesPHCEPerDay::class.java)
                .addRealmListField("date2", TablesPHCEPerDay::class.java)
                .addRealmListField("date3", TablesPHCEPerDay::class.java)
                .addRealmListField("date4", TablesPHCEPerDay::class.java)
                .addRealmListField("date5", TablesPHCEPerDay::class.java)
                .addRealmListField("date6", TablesPHCEPerDay::class.java)
                .addField("uuid", String::class.java)
    }

    override fun drop(realm: DynamicRealm) {

    }
}