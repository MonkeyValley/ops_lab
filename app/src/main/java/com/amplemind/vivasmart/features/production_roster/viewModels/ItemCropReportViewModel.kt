package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.repository.model.CropReportCarryOrderModel

class ItemCropReportViewModel constructor(private val model: CropReportCarryOrderModel) {

    val carryOrder: String = "• ${model.carryOrder}"
    val send: String = "${model.send}"

}