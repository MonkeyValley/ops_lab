package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivitiesDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.StagesDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import io.reactivex.Maybe
import javax.inject.Inject

class ActivityChooserRepository @Inject constructor(
        private val stagesDao: StagesDao,
        private val activitiesDao: ActivitiesDao
): Repository() {

    override fun cleanUp() {
        stagesDao.cleanUp()
        activitiesDao.cleanUp()
    }

    fun loadStage(stageUuid: String): Maybe<StageModel> =
            stagesDao.findByUuid(stageUuid)

    fun loadActivities(cropId: Long, stageType: Int) =
            activitiesDao.findAll(cropId, stageType)


}