package com.amplemind.vivasmart.features.fertiriego.viewModel

import android.content.Context
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_DATE
import com.amplemind.vivasmart.features.fertiriego.fragment.MonitoringFragment
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveFertirriego
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveForTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject

class MonitoringViewModel @Inject constructor(
        private val service: FertirriegoService) : BaseViewModel() {

    var table: String = ""
    var lotId: Int = 0
    var valveId: Int = 0

    lateinit var fragment: MonitoringFragment

    fun setServiceContect(context: Context){
        service.context = context
        service.preferences  = UserAppPreferences(context, null, null)
    }

    private val requestSuccessValves = BehaviorSubject.create<List<ValveFertirriego>>()
    private val requestSuccessValvesDataPerDay = BehaviorSubject.create<List<ValveForTable>>()
    private val requestSuccessValvesDataPerDayExt = BehaviorSubject.create<List<ValveForTable>>()

    fun getValves(): Disposable {
        return service.getValves(lotId, "Soil", table)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccessValves.onNext(it)
                }, {

                })
    }
    fun onSuccessRequestValves(): BehaviorSubject<List<ValveFertirriego>> = requestSuccessValves

    fun getValvesDataForTable(valveIdext:Int, identifier:String): Disposable {
        return service.getTableDataPerDay(lotId, FERTIRRIEGO_DATE, table, valveIdext)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if(identifier != ""){
                        requestSuccessValvesDataPerDayExt.onNext(it)
                    }else{
                        requestSuccessValvesDataPerDay.onNext(it)
                    }
                }, {
                    fragment.setValuesToZero()
                    fragment.initValue()
                })
    }
    fun onSuccessValvesDataPerDay(): BehaviorSubject<List<ValveForTable>> = requestSuccessValvesDataPerDay
    fun onSuccessValvesDataPerDayExt(): BehaviorSubject<List<ValveForTable>> = requestSuccessValvesDataPerDayExt

    fun validateDate(): Boolean {
        val calendar = Calendar.getInstance()
        val month = if((calendar.get(Calendar.MONTH) + 1) < 10) "0" +(calendar.get(Calendar.MONTH) + 1).toString() else  (calendar.get(Calendar.MONTH) + 1).toString()
        val day  = if((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" +(calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val todayDate = ( ( calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day )

        return FERTIRRIEGO_DATE == todayDate
    }

    fun saveValveData(type: Int, m6: String,  m12: String,  m18: String, m24: String, barrena: Int){
        service.saveValveData(valveId, FERTIRRIEGO_DATE, type, m6,  m12,
                m18, m24, barrena, table, lotId)
    }


    class ValveItem {
        var valveId: Int? = null
        var valveName: String? = null
        var status:Int = 0
        constructor(id: Int?, name: String?, status:Int=0) {
            this.valveId = id
            this.valveName = name
            this.status = status
        }

        override fun toString(): String {
            return valveName!!
        }

        fun getId(): Int{
            return valveId!!
        }
    }
}