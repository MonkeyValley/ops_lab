package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.model.LotModel
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel

class ItemRecibaReportViewModel(
        private val lot: LotModel?,
        private val carriesValue: Int,
        private val boxesValue: Double,
        private val avgWeightValue: Double,
        val varieties: List<VarietyModel>,
        val boxCounts: List<VarietyBoxCount>,
        val secondQualityBoxCount: Double
) {
    val lotId: Int
        get() = lot?.id ?: 0

    val lotName: String
        get() = lot?.name ?: "S/N"

    val carries: String
        get() = carriesValue.toString()

    val boxes: String
        get() = "%.1f".format(boxesValue)

    val avgWeight: String
        get() = "%.1f Kg".format(avgWeightValue)

    val cropId: Int
        get() = varieties[0].cropId.toInt()

}