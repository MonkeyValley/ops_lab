package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import com.amplemind.vivasmart.vo_core.repository.TimerActivityRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import javax.inject.Inject

class TimerActivityViewModel @Inject constructor(
        private val mRepository: TimerActivityRepository
) {

    lateinit var stageUuid: String
    lateinit var activityUuid: String

    lateinit var stage: StageModel
    lateinit var activity: ActivityModel

    fun cleanUp() {
        mRepository.cleanUp()
    }

    fun loadStage() =
            mRepository.loadStage(stageUuid)
                    .doOnSuccess { stage ->
                        this.stage = stage
                    }

    fun loadActivity() =
            mRepository.loadActivity(activityUuid)
                    .doOnSuccess { activity ->
                        this.activity = activity
                    }

    fun loadActivityCodes() =
            mRepository.loadActivityCodes(stageUuid, activityUuid)

    fun createActivityCode() =
            mRepository.createActivityCode(stageUuid, activityUuid)

    fun createActivityCodeWParams(stageUuidParam:String, activityUuidParams:String ) = mRepository.createActivityCode(stageUuidParam, activityUuidParams)

    fun createActivityLogs(activityCodeUuid: String, collaboratorUuids: Array<String>) =
            mRepository.createActivityLogs(activityCodeUuid, collaboratorUuids)


}