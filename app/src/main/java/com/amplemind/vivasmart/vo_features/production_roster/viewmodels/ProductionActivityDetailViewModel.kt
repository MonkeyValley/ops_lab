package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import android.content.Context
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ProductionActivityDetailViewModel @Inject constructor(private val repository: ActivityCodeRepository,
                                                            private val apiErrors: HandleApiErrors) {

    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val requestFail: BehaviorSubject<String> = BehaviorSubject.create()
    private val requestSuccess: BehaviorSubject<List<ProductionActivityDetailItemViewModel>> = BehaviorSubject.create()
    private val newCodeCreate: BehaviorSubject<ProductionActivityDetailItemViewModel> = BehaviorSubject.create()
    private val activitiesCodes = mutableListOf<ProductionActivityDetailItemViewModel>()

    private lateinit var mActivity: ActivityModel
    private lateinit var mStage: StageModel

    fun initialize(activity: ActivityModel, stage: StageModel) {
        mActivity = activity
        mStage = stage
    }

    fun getActivityCodes(context: Context) : Disposable {
        activitiesCodes.clear()
        return repository.getCodes(mActivity.id, mStage.id)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    mapActivityCodes(it.sortedBy { it.code })}
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    createNextCode()
                    requestSuccess.onNext(it)
                }
                .subscribe({
                    if (it.isEmpty()) {
                        createActivityCode(false)
                    }
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }


    fun createActivityCode(notifyIsNewCodeCreated: Boolean = true): Disposable {
        return repository.createCode(mActivity.id, mStage.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    if (it != null) {
                        assignNewCode(it, notifyIsNewCodeCreated)
                    }
                }, {
                    onRequestFail().onNext(it.localizedMessage)
                })
    }



    private fun assignNewCode(code: ActivityCodeModel, notifyIsNewCodeCreated: Boolean) {
        if (activitiesCodes.size > 0 && activitiesCodes[activitiesCodes.lastIndex].isNewCode()) {
            activitiesCodes.removeAt(activitiesCodes.lastIndex)
        }
        activitiesCodes.add(ProductionActivityDetailItemViewModel(code, code.code, "$ ${mActivity.cost}"))
        createNextCode()
        requestSuccess.onNext(activitiesCodes)
        if (notifyIsNewCodeCreated) {
            val removePositions = if (activitiesCodes[activitiesCodes.lastIndex].isNewCode()) 2 else 1
            newCodeCreate.onNext(activitiesCodes[activitiesCodes.size - removePositions])
        }
    }


    private fun mapActivityCodes(models: List<ActivityCodeModel>): List<ProductionActivityDetailItemViewModel> {
        activitiesCodes.clear()
        for (model in models) {
            activitiesCodes.add(ProductionActivityDetailItemViewModel(model, model.code, "$ ${mActivity.cost}"))
        }
        return activitiesCodes
    }

    private fun createNextCode() {
        if (activitiesCodes.size == 0) return
        val nextCode = activitiesCodes[activitiesCodes.lastIndex].code
        if (mActivity.rangeTo > nextCode || canCreateUnlimitedCodes()) {
            activitiesCodes.add(ProductionActivityDetailItemViewModel(null, nextCode + 1, "$ ${mActivity.cost}"))
        }
    }

    fun canCreateUnlimitedCodes(): Boolean {
        return mActivity.rangeFrom == mActivity.rangeTo
    }

    fun getAvailableCodes(): List<ActivityCodeModel> {
        val models = ArrayList<ActivityCodeModel>()
        for (code in activitiesCodes) {
            if (!code.isDone && code.activityCode != null) {
                models.add(code.activityCode)
            }
        }
        return models
    }

    fun getMaxRange(): Int {
        return mActivity.rangeTo
    }

    fun getActivityName(): String {
        return mActivity.name ?: ""
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onRequestSuccess(): BehaviorSubject<List<ProductionActivityDetailItemViewModel>> {
        return requestSuccess
    }

    fun onNewCodeCreated(): BehaviorSubject<ProductionActivityDetailItemViewModel> {
        return newCodeCreate
    }

}

