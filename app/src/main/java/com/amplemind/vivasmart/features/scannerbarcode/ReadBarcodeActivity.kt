package com.amplemind.vivasmart.features.scannerbarcode

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.HeaderModel
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.core.utils.CODE
import com.amplemind.vivasmart.core.utils.COLLABORATORS
import com.amplemind.vivasmart.core.utils.SINGLE_SCANNER
import com.amplemind.vivasmart.features.collaborators.AddCollaboratorsActivity
import com.amplemind.vivasmart.features.scannerbarcode.source.BarCodeScanningProcessor
import com.amplemind.vivasmart.features.scannerbarcode.source.CameraSource
import com.amplemind.vivasmart.features.scannerbarcode.viewModel.ReadBarcodeViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.content_scanner_barcode.*
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@SuppressLint("Registered")
open class ReadBarcodeActivity : BaseActivity() {

    companion object {
        val TAG = ReadBarcodeActivity::class.simpleName
    }

    @Inject
    lateinit var viewModel: ReadBarcodeViewModel

    private var cameraSource: CameraSource? = null

    @Inject
    lateinit var mPermission: AndroidPermission

    @Inject
    lateinit var prefer: UserAppPreferences

    private lateinit var mActivityCode: ActivityCodeModel

    override fun onDestroy() {
        firePreview.release()
        cameraSource = null
        super.onDestroy()
    }

    override fun onPause() {
        firePreview.release()
        cameraSource = null
        super.onPause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_scanner_barcode)

        FirebaseApp.initializeApp(this)
        initToolbar()
        addObservers()
    }

    override fun onResume() {
        super.onResume()
        startCamera()
    }

    private fun addObservers() {
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onGetCollaborators().subscribe(this::onGetCollaborators).addTo(subscriptions)
        viewModel.addUserInPackage().subscribe { code -> viewModel.addLocalEmployee(code) }.addTo(subscriptions)
    }

    private fun onGetCollaborators(collaborators: List<CollaboratorModel>) {
        if (collaborators.isNotEmpty()) {
            val scanIntent = Intent(this, ScanListActivity::class.java)
            //scanIntent.putExtra(ScanListActivity.PARAM_ACTIVITY_CODE, mActivityCode)
            scanIntent.putExtra("showActivities", showActivities())
            scanIntent.putExtra("section", getSection())
            scanIntent.putParcelableArrayListExtra(COLLABORATORS, collaborators as java.util.ArrayList<out Parcelable>)
            startActivityForResult(scanIntent, 200)

            if (getSection() != null) {
                return
            }

            if (showActivities() && getSection() == null) {
                finish()
            }
        }
    }

    private fun startCamera() {
        if (mPermission.cameraPermission(this)) {
            initScannerCamera()
        } else {
            mPermission.verifyHavePermissions(this)
        }
    }

    private fun getSection(): HeaderModel? {
        return intent.getParcelableExtra("section")
    }

    fun showActivities(): Boolean {
        return intent.getBooleanExtra("showActivities", false)
    }

    /**
     * Start service firebase
     */
    private fun initScannerCamera() {
        createCameraSource()
    }

    private fun initToolbar() {
        val mToolbar = toolbar_barcode
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Escanear colaboradores"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //if (!intent.hasExtra(SINGLE_SCANNER) && this.isOnlineMode(this)) {
            menuInflater.inflate(R.menu.menu_read_barcode, menu)
        //}
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.search_collaborators -> {
                val intent = Intent(this, AddCollaboratorsActivity::class.java)
                        .putExtra("onlySelected", showActivities())
                        //.putExtra(AddCollaboratorsActivity.PARAM_ACTIVITY_CODE, mActivityCode)
                startActivityForResult(intent, 200)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun isProductionFlow(): Boolean {
        return prefer.flow == "Producción"
    }

    /**
     * start the camera and the barcode scanner
     */
    private fun createCameraSource() {
        if (cameraSource == null) {
            cameraSource = CameraSource(this)
            val barcode = BarCodeScanningProcessor()
            cameraSource!!.setMachineLearningFrameProcessor(barcode)

            barcode.getBarcode().throttleFirst(2, TimeUnit.SECONDS).subscribe(this::barcodeSearch).addTo(subscriptions)

            try {
                if (cameraSource != null) {
                    firePreview.start(cameraSource!!)
                }
            } catch (e: IOException) {
                cameraSource!!.release()
                cameraSource = null
            }
        }
    }

    /**
     *  scanner result
     */
    fun barcodeSearch(barcode: String) {
        if (intent.hasExtra(SINGLE_SCANNER)) {
            val intent = Intent()
            intent.putExtra(CODE, barcode)
            setResult(Activity.RESULT_OK, intent)
            finish()
        } else {
            //if (isOnlineMode(this)) {
                viewModel.getListCollaborators(barcode, !isProductionFlow())
            /*
            } else {
                if (isProductionFlow()) {
                    mViewModel.addLocalEmployee(barcode)
                } else {
                    mViewModel.existCollaborator(barcode).addTo(subscriptions)
                }
            }
            */
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == mPermission.REQUEST_CODE_PERMISSIONS) {
            if (mPermission.verifyHavePermissions(this)) {
                initScannerCamera()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200) {
            finish()
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}
