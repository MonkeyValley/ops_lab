package com.amplemind.vivasmart.features.packaging_quality

import android.os.Bundle
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.RecibaReportFragment
import kotlinx.android.synthetic.main.activity_reciba_report.*

class RecibaReportActivity : BaseActivityWithFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reciba_report)
        setupToolbar()

        addFragment()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_reciba_report.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Reporte de reciba"
    }

    private fun addFragment() {
        supportFragmentManager.beginTransaction()
                .add(R.id.frame, RecibaReportFragment.newInstance())
                .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
