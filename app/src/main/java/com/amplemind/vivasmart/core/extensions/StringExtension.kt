package com.amplemind.vivasmart.core.extensions

import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

/**
 * Created by
 *          amplemind on 7/2/18.
 */
// used for validate if the current String is an email amplemind
fun String.isValidEmail(): Boolean {
    return Pattern.compile("^[a-z0-9!#\$%&‘*+\\/=?^_`{|}~.-]{2,64}@[(\\w+\\-+)|(\\w+)]{1,63}\\.[\\w]{2,6}(\\.[\\w]{2,6})*\$")
            .matcher(this)
            .matches()
}

fun String.isValidPassword(): Boolean {
    return Pattern.compile("^(?=.*[a-z])(?=.*\\d).{8,}\$")
            .matcher(this)
            .matches()
}

fun String.onlyDigits() : Boolean{
    return Pattern.compile("^[0-9]*\$")
            .matcher(this)
            .matches()
}

fun formatMoney(money : Double) : String {
    return "$%.2f".format(Locale.US,money)
}

/**
 *  in case of being greater than 60 minutes it will be formatted with hours of not being so we will show design of minutes and seconds
 */
fun formatTime(time: Long): String {
    val milisecounds = TimeUnit.SECONDS.toMillis(time)
    return String.format("%02d:%02d", TimeUnit.MILLISECONDS.toHours(milisecounds),
            TimeUnit.MILLISECONDS.toMinutes(milisecounds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milisecounds)))
}