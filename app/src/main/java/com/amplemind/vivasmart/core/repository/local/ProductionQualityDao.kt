package com.amplemind.vivasmart.core.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.amplemind.vivasmart.core.repository.model.IssuesResultResponse
import com.amplemind.vivasmart.core.repository.model.ProductionCategoriesModel
import com.amplemind.vivasmart.core.repository.model.ProductionCategoriesTable
import com.amplemind.vivasmart.core.repository.model.ProductionQualityOfflineRequest
import com.amplemind.vivasmart.core.repository.request.CreateIssueRequest

@Dao
interface ProductionQualityDao {

    /*
     * Production Categories
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategories(recent: List<ProductionCategoriesModel>)

    @Query("SELECT * FROM ProductionCategoriesModel WHERE stageId = :stageId ORDER BY id")
    fun getCategories(stageId: Int) : List<ProductionCategoriesModel>

    @Query("UPDATE ProductionCategoriesModel SET total = total + :total  WHERE id = :categoryId")
    fun incrementCountInCategory(categoryId: Int, total: Int)

    /*
     *   Issues Pending To Send
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProductionQualityOfflineRequest(recent: ProductionQualityOfflineRequest)

    @Query("SELECT * FROM ProductionQualityOfflineRequest")
    fun getProductionQualityOfflineRequest(): List<ProductionQualityOfflineRequest>

    @Query("DELETE FROM ProductionQualityOfflineRequest")
    fun deleteAllProductionQualityOfflineRequest()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertIssues(recent: List<IssuesResultResponse>)

    @Query("SELECT * FROM IssuesResultResponse WHERE categoryId = :categoryId AND stageId = :stageId")
    fun getIssues(categoryId: Int, stageId: Int) : List<IssuesResultResponse>

    @Query("UPDATE IssuesResultResponse SET count = count + :total WHERE id = :id")
    fun incrementCountIssues(id: Int, total: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertIssuePendingToSend(issue: CreateIssueRequest)

    @Query("SELECT * FROM CreateIssueRequest WHERE deleted = 0")
    fun getIssuesPendingToSend() : List<CreateIssueRequest>

    @Query("SELECT * FROM CreateIssueRequest WHERE tableNo = :table AND stageId = :stageId AND deleted = :deleted")
    fun getIssuesPendingToSendByTable(table: Int, stageId: Int, deleted: Boolean = false) : List<CreateIssueRequest>

    @Query("SELECT * FROM CreateIssueRequest WHERE dbPrimaryKey = :id")
    fun getIssuesPendingToSendById(id: Int) : CreateIssueRequest

    @Query("UPDATE CreateIssueRequest SET deleted = 1 WHERE dbPrimaryKey = :id")
    fun deleteIssuePendingToSend(id: Int)

    /*
     *   Reports
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReportIssues(recent: List<ProductionCategoriesTable>)

    @Query("SELECT * FROM ProductionCategoriesTable WHERE tableNo = :table AND stageId = :stageId")
    fun getReportIssuesByTable(table: Int, stageId: Int): List<ProductionCategoriesTable>

    @Query("SELECT * FROM ProductionCategoriesTable WHERE stageId = :stageId")
    fun getReportIssuesByStage(stageId: Int): List<ProductionCategoriesTable>

    @Query("DELETE FROM ProductionCategoriesTable WHERE id = :id")
    fun deleteReportIssue(id: Int)

}