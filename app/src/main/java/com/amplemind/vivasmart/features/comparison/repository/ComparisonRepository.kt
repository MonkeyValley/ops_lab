package com.amplemind.vivasmart.features.comparison.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.remote.ComparisonApi
import com.amplemind.vivasmart.core.repository.remote.PlanningApi
import com.amplemind.vivasmart.core.repository.response.ComparisonResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import io.reactivex.Observable
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ComparisonRepository @Inject constructor(private val comparisonApi : ComparisonApi,
                                               private val apiAuthorization: ApiAuthorization,
                                               private val preferences : UserAppPreferences) {

    private fun currentDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return sdf.format(Date())
    }

    fun getComparisonProduction(stage : String): Observable<ComparisonResponse> {
        return comparisonApi.getComparisonProduction(apiAuthorization.getAuthToken(preferences.token),stage,"1","2",currentDate())
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getComparisonPackage(idLine : String): Observable<ComparisonResponse> {
        return comparisonApi.getComparisonPackage(apiAuthorization.getAuthToken(preferences.token),idLine,"2","4",currentDate())
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

}
