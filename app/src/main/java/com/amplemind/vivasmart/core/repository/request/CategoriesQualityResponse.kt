package com.amplemind.vivasmart.core.repository.request

import com.amplemind.vivasmart.core.repository.model.ProductionCategoriesModel
import com.google.gson.annotations.SerializedName

data class CategoriesQualityResponse(@SerializedName("data") val data: List<ProductionCategoriesModel>)