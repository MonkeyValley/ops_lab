package com.amplemind.vivasmart.core.utils

import android.os.Looper
import android.util.Log
import io.realm.Realm
import java.io.Closeable
import java.util.concurrent.ConcurrentHashMap

class RealmHolder : Closeable {

    private var mCounter: Int = 0
    private lateinit var mMainThreadRealm: Realm

    companion object {
        private val TAG: String = RealmHolder::class.java.simpleName
        //A thread safe hash map was needed
        private val sRealms = ConcurrentHashMap<Long, Realm>()
    }

    fun getRealm() =
            // If this is the main thread, return the realm object from the holder
            if (getLooper() == Looper.getMainLooper()) {
                getRealmForMainThread()
            }
            else {
                val threadId = Thread.currentThread().id
                var realm = sRealms[threadId]

                if (realm == null || realm.isClosed) {
                    realm = Realm.getDefaultInstance()
                    sRealms[threadId] = realm
                }

                Log.e(TAG, "Getting realm for thread ${Thread.currentThread().id}")
                Log.e(TAG, "Current thread count: ${sRealms.size}")

                realm!!
            }

    override fun close() {
        if (getLooper() == Looper.getMainLooper()) {
            closeForMainThread()
        } else {
            sRealms.remove(Thread.currentThread().id)?.close()
            Log.e(TAG, "Closing realm for thread ${Thread.currentThread().id}")
            Log.e(TAG, "Current thread count: ${sRealms.size}")
        }
    }

    private fun getRealmForMainThread(): Realm {
        mCounter ++
        Log.e(TAG, "$mCounter DAOs registered")
        if (!::mMainThreadRealm.isInitialized || mMainThreadRealm.isClosed) {
            mMainThreadRealm = Realm.getDefaultInstance()
        }
        return mMainThreadRealm
    }

    private fun closeForMainThread() {
        mCounter --
        Log.e(TAG, "DAO unregistered, $mCounter DAOs registered")
        if (mCounter == 0) {
            mMainThreadRealm.close()
        }
    }

    private fun getLooper() =
            Looper.myLooper()
                    //?: throw IllegalThreadStateException("Realm objects should only be created or closed in a looper thread!")
}