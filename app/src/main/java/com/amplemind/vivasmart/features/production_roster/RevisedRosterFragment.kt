package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.utils.STAGE_TYPE_ID
import com.amplemind.vivasmart.features.production_roster.viewModels.ReportRosterViewModel
import com.amplemind.vivasmart.features.quality_control.ControlListAdapter
import com.amplemind.vivasmart.vo_core.repository.models.realm.PendingLogsModel
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.content_revised_fragment.*
import javax.inject.Inject

class RevisedRosterFragment : BaseFragment() {

    companion object {

        private val TAG: String = RevisedRosterFragment::class.java.simpleName

        private val PARAM_STAGE_UUID    = "$TAG.StageUuid"
        private val PRAM_STAGE_TYPE_ID  = "${TAG}.StageTypeId"
        private val PARAM_MODE   = "$TAG.Mode"

        fun newInstance(stageUuid: String, stageTypeId: Int, mode: Int): RevisedRosterFragment =
                RevisedRosterFragment().apply {
                    arguments = Bundle().apply {
                        putString(PARAM_STAGE_UUID, stageUuid)
                        putInt(PRAM_STAGE_TYPE_ID, stageTypeId)
                        putInt(PARAM_MODE, mode)
                    }
                }
    }

    init {
        argumentsNeeded = true
    }

    @Inject
    lateinit var mViewModel: RevisedRosterViewModel

    @Inject
    lateinit var mViewModelReport: ReportRosterViewModel

    private lateinit var mAdapter: ControlListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_revised_fragment, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mAdapter.cleanUp()
        mViewModel.cleanUp()
        mViewModelReport.cleanUp()
    }

    override fun readArguments(args: Bundle) {
        super.readArguments(args)
        mViewModel.stageUuid = requireNotNull(args.getString(PARAM_STAGE_UUID))
        mViewModelReport.stageUuid = requireNotNull(args.getString(PARAM_STAGE_UUID))
        mViewModel.stageTypeId = requireNotNull(args.getInt(PRAM_STAGE_TYPE_ID))
        mViewModel.mode = requireNotNull(args.getInt(PARAM_MODE))
    }

    override fun setUpCallbacks() {
        super.setUpCallbacks()

        mEventBus.observe<RosterDetailActivity.UpdateReportEvent>()
                .filter { event ->
                    mViewModel.stageUuid == event.stageUuid
                }
                .subscribe(this::onUpdateReportEvent)
                .addTo(subscriptions)

        btn_send.clicks()
                .subscribe {
                    onSendClicked()
                }
                .addTo(subscriptions)
    }

    private fun onSendClicked() {
        //dailyPay//
        mViewModelReport.hasPeopleWorkingDailyPay()
                .subscribe ({ hasPeopleWorking ->
                    if (hasPeopleWorking) {

                        AlertDialog.Builder(activity!!)
                                .setTitle(R.string.rra_send_title_daily_pay)
                                .setMessage(R.string.rra_working_daily_pay)
                                .show()
                    }
                    else {
                        checkUnsignedLogs()
                    }
                }, { error ->
                    showErrorDialog(error)
                }
                )
                .addTo(subscriptions)
    }

    private fun checkUnsignedLogs() {
        mViewModelReport.hasUnsignedLogsDailyPay()
                .subscribe({ hasUnsignedLogs ->
                    if (hasUnsignedLogs) {
                        AlertDialog.Builder(activity!!)
                                .setTitle(R.string.rra_send_title_daily_pay)
                                .setMessage(R.string.rra_has_unsigned_daily_pay)
                                .setPositiveButton(R.string.ok) { dialog, _ ->
                                    dialog.dismiss()
                                }
                                .show()
                    }
                    else {
                        sendReport()
                    }
                }, { error ->
                    showErrorDialog(error)
                })
                .addTo(subscriptions)
    }

    private fun sendReport() {
        AlertDialog.Builder(activity!!)
                .setTitle(R.string.rra_send_title_daily_pay)
                .setMessage(R.string.rra_send_report_daily_pay)
                .setNegativeButton(R.string.yes) { _, _ ->
                    mViewModelReport.setAllAsDoneDailyPay()
                            .subscribe({
                                //mEventBus.send(ReportRosterActivity.CloseLotEvent(mViewModel.stageUuid))
                                //setResult(DaggerAppCompatActivity.RESULT_OK, intent.putExtra("code", 200))
                                activity!!.finish()
                            }, this::showErrorDialog)
                            .addTo(subscriptions)
                }
                .setPositiveButton(R.string.no) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()

        mEventBus.observe<ControlListAdapter.PendingLogsClickedEvent>()
                .filter { event ->
                    event.readOnly && event.pendingLogs.stage.uuid == mViewModel.stageUuid
                }
                .subscribe(this::onPendingLogsClickedEvent)
                .addTo(subscriptions)

        /*
        RxView.clicks(btn_send_process).subscribe(this::checkForActiveLogs).addTo(subscriptions)
         */

    }

    override fun setUpUI() {
        super.setUpUI()
        initRecycler()
        initButton()
    }

    override fun loadData() {
        super.loadData()
        mViewModel.getPendingLogs().subscribe(this::onPendingLogsLoaded).addTo(subscriptions)
    }

    private fun onPendingLogsLoaded(pendingLogs: List<PendingLogsModel>) {
        if(STAGE_TYPE_ID == 5) {
            mAdapter.items = mViewModel.filterLogs(pendingLogs)
        } else {
            mAdapter.items = pendingLogs
        }
    }

    private fun onUpdateReportEvent(event: RosterDetailActivity.UpdateReportEvent) {
        loadData()
    }

    private fun onPendingLogsClickedEvent(event: ControlListAdapter.PendingLogsClickedEvent) {
        startActivity(Intent(context!!, RosterDetailActivity::class.java).apply {
            //flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            putExtra(RosterDetailActivity.PARAM_COLLABORATOR_UUID, event.pendingLogs.collaborator.uuid)
            putExtra(RosterDetailActivity.PARAM_STAGE_UUID, event.pendingLogs.stage.uuid)
            putExtra(RosterDetailActivity.PARAM_TYPE, RosterDetailActivity.TYPE_SIGNED)
        })
    }

    private fun initRecycler() {
        rv_revised.setHasFixedSize(true)
        rv_revised.layoutManager = LinearLayoutManager(context)
        rv_revised.itemAnimator = DefaultItemAnimator()
        rv_revised.setPadding(0, 8.toPx(), 0, 70.toPx())
        rv_revised.clipToPadding = false

        mAdapter = ControlListAdapter(mEventBus, true)
        rv_revised.adapter = mAdapter
    }

    private fun initButton(){
        btn_send.visibility = if (mViewModel.mode == 2) View.VISIBLE else View.GONE
    }

}
