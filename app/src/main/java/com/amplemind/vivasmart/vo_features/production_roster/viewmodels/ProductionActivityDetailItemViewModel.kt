package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import androidx.databinding.ObservableBoolean
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel

class ProductionActivityDetailItemViewModel constructor(val activityCode: ActivityCodeModel?, val code: Int, val price: String) {

    val id = activityCode?.id
    var isDone = false
    val totalActivity = activityCode?.total ?: "0"
        get() = if (isNewCode()) "+" else if (field == "None") "0" else field
    val isActive: Boolean
    val isActiveRow = ObservableBoolean(false)

    val remaining = (activityCode?.totalSpaces ?: 0) - (activityCode?.occupiedSpaces ?: 0)

    init {
        val values = activityCode?.total?.split("/")
        if (values != null && values.size == 2) {
            val initalValue = values[0].toIntOrNull()
            val finalValue = values[1].toIntOrNull()
            isDone = (initalValue ?: 0 >= finalValue ?: 0) //&& !activityCode!!.activeCollaborators
        }
        isActive = activityCode != null
        isActiveRow.set(isActive)
    }

    fun isNewCode(): Boolean {
        return activityCode == null
    }

    fun getStringCode(): String {
        return code.toString()
    }

}
