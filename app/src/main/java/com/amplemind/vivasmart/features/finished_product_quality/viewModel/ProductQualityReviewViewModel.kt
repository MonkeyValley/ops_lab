package com.amplemind.vivasmart.features.finished_product_quality.viewModel


import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.FinishedProductRepository
import com.amplemind.vivasmart.core.repository.response.ClientResponse
import com.amplemind.vivasmart.core.repository.response.CropUnitResponse
import com.amplemind.vivasmart.core.repository.response.LotsResponse
import com.amplemind.vivasmart.core.repository.response.UnitResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.finished_product_quality.services.FinishedProductQualityService
import com.amplemind.vivasmart.features.production_quality.ClientViewModel
import com.amplemind.vivasmart.features.production_quality.ItemCropUnitModelViewModel
import com.amplemind.vivasmart.features.production_quality.LotsViewModel
import com.amplemind.vivasmart.features.production_quality.UnitsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ProductQualityReviewViewModel @Inject constructor(
        private val repository: FinishedProductRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val finishedProductQualityService: FinishedProductQualityService = FinishedProductQualityService()
    private val listPresentaciones = mutableListOf<ItemCropUnitModelViewModel>()
    private val listClients = mutableListOf<ClientViewModel>()
    private val listLots = mutableListOf<LotsViewModel>()
    private val listUnits = mutableListOf<UnitsViewModel>()
    private val requestSuccess_unitCrop = BehaviorSubject.create<List<ItemCropUnitModelViewModel>>()
    private val requestSuccess_clientes = BehaviorSubject.create<List<ClientViewModel>>()
    private val requestSuccess_lots = BehaviorSubject.create<List<LotsViewModel>>()
    private val requestSuccess_units = BehaviorSubject.create<List<UnitsViewModel>>()

    fun getCropList(): ArrayList<CropStageModelItem>{
        val listCrop = ArrayList<CropStageModelItem>()
        var cropModel = CropStageModelItem(0, "Seleccionar")
        listCrop.add(cropModel)

        for( crop in finishedProductQualityService.listCrop()){
            val cropModelNew = CropStageModelItem(crop.id, crop.name!!)
            listCrop.add(cropModelNew)
        }

        listCrop.sortBy { it.id }
        return listCrop
    }

    fun getUnitCropList(id_crop: Int): Disposable {
        return repository.getUnitCropList(id_crop)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate {
                    progressStatus.onNext(false)
                    getLots(id_crop)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_cropUnits)
                .subscribe({
                    listPresentaciones.clear()
                    listPresentaciones.addAll(it)
                    requestSuccess_unitCrop.onNext(listPresentaciones)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getClients( ): Disposable {
        return repository.getClients( )
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .doOnTerminate { getUnits() }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_clientes)
                .subscribe({
                    listClients.clear()
                    listClients.addAll(it)
                    requestSuccess_clientes.onNext(listClients)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLots(crop_id : Int ): Disposable {
        return repository.getLots( crop_id )
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_lots)
                .subscribe({
                    listLots.clear()
                    listLots.addAll(it)
                    requestSuccess_lots.onNext(listLots)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getUnits( ): Disposable {
        return repository.getUnits( )
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_units)
                .subscribe({
                    listUnits.clear()
                    listUnits.addAll(it)
                    requestSuccess_units.onNext(listUnits)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModel_cropUnits(result: CropUnitResponse): List<ItemCropUnitModelViewModel> {
        val list = arrayListOf<ItemCropUnitModelViewModel>()

        for (item in result.data) {
            list.add(ItemCropUnitModelViewModel(item))
        }
        list.sortBy { it.name }
        return list
    }

    private fun mapModelToViewModel_clientes(result: ClientResponse): List<ClientViewModel> {
        val list = arrayListOf<ClientViewModel>()

        for (item in result.data) {
            list.add(ClientViewModel(item))
        }
        list.sortBy { it.name }

        return list
    }

    private fun mapModelToViewModel_lots(result: LotsResponse): List<LotsViewModel> {
        val list = arrayListOf<LotsViewModel>()

        for (item in result.data) {
            list.add(LotsViewModel(item))
        }
        list.sortBy { it.lot!!.name }
        return list
    }

    private fun mapModelToViewModel_units(result: UnitResponse): List<UnitsViewModel> {
        val list = arrayListOf<UnitsViewModel>()

        for (item in result.data) {
            list.add(UnitsViewModel(item))
        }
        list.sortBy { it.name }
        return list
    }

    fun onSuccessRequest_unitCrop(): BehaviorSubject<List<ItemCropUnitModelViewModel>> {
        return requestSuccess_unitCrop
    }

    fun onSuccessRequest_clients(): BehaviorSubject<List<ClientViewModel>> {
        return requestSuccess_clientes
    }

    fun onSuccessRequest_lots(): BehaviorSubject<List<LotsViewModel>> {
        return requestSuccess_lots
    }

    fun onSuccessRequest_units(): BehaviorSubject<List<UnitsViewModel>> {
        return requestSuccess_units
    }

    class CropStageModelItem {
        var id: Int? = null
        var name: String? = null

        constructor(id: Int?, name: String?) {
            this.id = id
            this.name = name
        }

        override fun toString(): String {
            return name!!
        }

        fun getId(): Int{
            return id!!
        }
    }

    class CropUnitModelItem {
        var id: Int? = null
        var id_crop: Int? = null
        var name: String? = null

        constructor(id: Int?, id_crop : Int?, name: String?) {
            this.id = id
            this.id_crop = id_crop
            this.name = name
        }

        override fun toString(): String {
            return name!!
        }

        fun getId(): Int{
            return id!!
        }

        fun getIdCrop(): Int{
            return id_crop!!
        }
    }

    class ClientItem {
        var id: Int? = null
        var name: String? = null

        constructor(id: Int?,  name: String?) {
            this.id = id
            this.name = name
        }

        override fun toString(): String {
            return name!!
        }

        fun getId(): Int{
            return id!!
        }
    }

    class LotItem {
        var id: Int? = null
        var crop_id: Int? = null
        var name: String? = null
        var business_unit_id: Int? = null

        constructor(id: Int?,crop_id:Int, name: String?, business_unit_id: Int) {
            this.id = id
            this.crop_id = crop_id
            this.name = name
            this.business_unit_id = business_unit_id
        }

        override fun toString(): String {
            return name!!
        }

        fun getId(): Int{
            return id!!
        }
    }

    class UnitItem {
        var id: Int? = null
        var business_unit_id: Int? = null
        var name: String? = null
        var business_unit_names: String?  = null

        constructor(id: Int?,business_unit_id:Int, name: String?, business_unit_names: String) {
            this.id = id
            this.business_unit_id = business_unit_id
            this.name = name
            this.business_unit_names = business_unit_names
        }

        override fun toString(): String {
            return name!!
        }

        fun getId(): Int{
            return id!!
        }
    }
}
