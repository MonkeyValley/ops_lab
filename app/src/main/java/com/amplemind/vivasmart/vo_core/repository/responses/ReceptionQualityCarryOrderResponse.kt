package com.amplemind.vivasmart.vo_core.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityCarryOrderModel
import com.google.gson.annotations.SerializedName

data class ReceptionQualityCarryOrderResponse(
        @SerializedName("data")         val data: List<ReceptionQualityCarryOrderModel>,
        @SerializedName("total_count")  val totalCount: Int
)