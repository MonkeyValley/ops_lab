package com.amplemind.vivasmart.features.notification.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.getSpanCount
import com.amplemind.vivasmart.features.notification.adapter.InboxAdapter
import com.amplemind.vivasmart.features.notification.viewModel.InboxViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import kotlinx.android.synthetic.main.fragment_inbox.*
import javax.inject.Inject

class InboxFragment : BaseFragment() {

    fun newInstance(): InboxFragment {
        val fragment = InboxFragment()
        return fragment
    }

    @Inject
    lateinit var viewModel: InboxViewModel

    private var mAdapter = InboxAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_inbox, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setContext(activity!!)
        setupRecycler()
        subcribers()
    }

    private fun setupRecycler() {
        rv_inbox.hasFixedSize()
        rv_inbox.layoutManager = GridLayoutManager(context, getSpanCount(context!!))
        rv_inbox.adapter = mAdapter
        rv_inbox.layoutManager = LinearLayoutManager(context!!)
        mAdapter.service = viewModel.service
    }

    fun subcribers(){
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_message().subscribe(this::addElements).addTo(subscriptions)
        viewModel.getInbox()
    }

    fun addElements(data: List<MessageModel>) {
        if(data.isNotEmpty()) no_message.text = ""
        mAdapter.addElements(data)
    }

}