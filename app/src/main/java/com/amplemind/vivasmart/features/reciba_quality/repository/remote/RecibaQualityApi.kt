package com.amplemind.vivasmart.features.reciba_quality.repository.remote

import com.amplemind.vivasmart.core.repository.response.UnitResponse
import com.amplemind.vivasmart.features.reciba_quality.repository.response.ReportRecibaQualityResponse
import com.amplemind.vivasmart.features.reciba_quality.repository.response.RewviewDetailRecibaQualityResponse
import com.amplemind.vivasmart.features.reciba_quality.repository.response.VariatiesRecibaQualityResponse
import io.reactivex.Observable
import retrofit2.http.*

interface RecibaQualityApi {

    //https://dev.vivasmart.com.mx/v2/quality_carry_order_day?business_unit_id=1&day=2020-02-25
    @GET("/v2/quality_carry_order_day")
    @Headers("Content-Type: application/json")
    fun getReportList(
            @Header("Authorization") authentication: String,
            @Query("day") day: String,
            @Query("business_unit_id") businessId: String
    ): Observable<List<ReportRecibaQualityResponse>>

    //https://dev.vivasmart.com.mx/v2/carry_order_with_varieties/1913
    @GET("/v2/carry_order_with_varieties/{carry_order_id}")
    @Headers("Content-Type: application/json")
    fun getVarieties(
            @Header("Authorization") authentication: String,
            @Path("carry_order_id") id: String
    ): Observable<VariatiesRecibaQualityResponse>

    //https://qa-ops.vivasmart.com.mx/v2/unit_list?business_unit_id=2
    @GET("/v2/unit")
    @Headers("Content-Type: application/json")
    fun getUnits(
            @Header("Authorization") authentication: String,
            @Query("business_unit_id") business_unit_id: String
    ): Observable<UnitResponse>

    //https://dev.vivasmart.com.mx/v2/quality_carry_order/carry_order/1930
    @GET("/v2/quality_carry_order/carry_order/{carry_order_id}")
    @Headers("Content-Type: application/json")
    fun getDetail(
            @Header("Authorization") authentication: String,
            @Path("carry_order_id") id: String
    ): Observable<List<RewviewDetailRecibaQualityResponse>>

}