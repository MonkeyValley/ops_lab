package com.amplemind.vivasmart.core.extensions

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import android.graphics.Color
import android.net.Uri
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.cardview.widget.CardView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.BUSY_GROOVE
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlListItemViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlQualityItemViewModel
import com.amplemind.vivasmart.vo_features.production_roster.adapters.GroovesAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import java.io.File


/**
 * Created by
 *          amplemind on 7/10/18.
 */

@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(url: String?) {
    if (url != null) {
        if (url.startsWith("http")) {
            Glide.with(context).load(url).into(this)
        } else {
            val file = File(url)
            if (file.exists()) {
                val imageUri = Uri.fromFile(file)
                Glide.with(context).load(imageUri).into(this)
            }
        }
    }
}

@BindingAdapter("imageUrlRounderCorners")
fun ImageView.setImageUrlRounderCorners(url: String?) {
    if (url != null) {
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
        if (url.startsWith("http")) {
            Glide.with(context).load(url).apply(requestOptions).into(this)
        } else {
            val file = File(url)
            if (file.exists()) {
                val imageUri = Uri.fromFile(file)
                Glide.with(context).load(imageUri).apply(requestOptions).into(this)
            }
        }
    }
}

@BindingAdapter("imageUrlSwipeCards")
fun ImageView.setImageUrlSwipeCards(url: String?) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions()
                    .placeholder(R.drawable.ic_koloda_placeholder))
            .into(this)
}

@BindingAdapter("imageUrlCirle")
fun ImageView.setImageUrlCircle(url: String?) {
    val options = if (url != null) RequestOptions.circleCropTransform() else RequestOptions.errorOf(R.drawable.ic_user)
    Glide.with(context).load(url).apply(options).into(this)
}


@BindingAdapter("imageDrawable")
fun ImageView.setImageDrawable(url: Int) {
    this.setImageDrawable(ContextCompat.getDrawable(context, url))
}


@BindingAdapter("house")
fun ImageView.setHouse(status: Int) {
    val type = ControlQualityItemViewModel.LOT_STATUS.from(status)
    val drawable = when (type) {
        ControlQualityItemViewModel.LOT_STATUS.HOUSE_CIRCLE -> {
            ContextCompat.getDrawable(context, R.drawable.ic_inv_circle_incomplete)
        }
        ControlQualityItemViewModel.LOT_STATUS.HOUSE_CIRCLE_COMPLETE -> {
            ContextCompat.getDrawable(context, R.drawable.ic_inv_circle_complete)
        }
        ControlQualityItemViewModel.LOT_STATUS.HOUSE_RECT -> {
            ContextCompat.getDrawable(context, R.drawable.ic_inv_circle)
        }
        ControlQualityItemViewModel.LOT_STATUS.HOUSE_RECT_COMPLETE -> {
            ContextCompat.getDrawable(context, R.drawable.ic_inv_malla_done)
        }
        ControlQualityItemViewModel.LOT_STATUS.HOUSE_CIRCLE_PENDING -> {
            ContextCompat.getDrawable(context, R.drawable.ic_inv_circle_pending)
        }
        ControlQualityItemViewModel.LOT_STATUS.HOUSE_RECT_PENDING -> {
            ContextCompat.getDrawable(context, R.drawable.ic_malla_pendiente)
        }
    }
    this.setImageDrawable(drawable)
}

@BindingAdapter("controlStatus")
fun ImageView.setItemControlStatus(status: Int) {
    val type = ControlListItemViewModel.ControlListItemStatus.from(status)
    val drawable = when (type) {
        ControlListItemViewModel.ControlListItemStatus.PENDING -> {
            ContextCompat.getDrawable(context, R.drawable.ic_circle_status_pending)
        }
        ControlListItemViewModel.ControlListItemStatus.DONE -> {
            ContextCompat.getDrawable(context, R.drawable.ic_circle_status_complete)
        }
        ControlListItemViewModel.ControlListItemStatus.NONE -> {
            null
        }
    }
    this.setImageDrawable(drawable)
}

@BindingAdapter("status")
fun ImageView.setStatus(status: String?) {
    val drawable = when (status!!.toInt()) {
        1 -> {
            ContextCompat.getDrawable(context, R.drawable.ic_circle_status_complete)
        }
        else -> {
            ContextCompat.getDrawable(context, R.drawable.ic_circle_status_pending)
        }
    }
    this.setImageDrawable(drawable)
}

@BindingAdapter("complete")
fun ImageView.complete(complete: Boolean) {
    val drawable = if (complete) {
        ContextCompat.getDrawable(context, R.drawable.ic_oval_circle_blue)
    } else {
        ContextCompat.getDrawable(context, R.drawable.ic_oval_circle)
    }
    this.setImageDrawable(drawable)
}

@BindingAdapter("report_complete")
fun ImageView.errors(hasErrors: Boolean) {
    val drawable = if (!hasErrors) {
        ContextCompat.getDrawable(context, R.drawable.ic_oval_circle_blue)
    } else {
        ContextCompat.getDrawable(context, R.drawable.ic_oval_circle_red)
    }
    this.setImageDrawable(drawable)
}

@BindingAdapter("textColor")
fun TextView.setColor(complete: Boolean) {
    if (complete) {
        this.setTextColor(Color.WHITE)
    }
}

@BindingAdapter("textTitleBold")
fun TextView.titleBold(message: String) {
    val str = SpannableStringBuilder(context.getString(R.string.description_title, message))
    str.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    this.text = str
}

@BindingAdapter("statusGroove")
fun LinearLayout.setGrooveStatus(state: Int) {
    val drawable = when (state) {
        BUSY_GROOVE -> {
            R.drawable.selector_groove
        }
        else -> { // AVAILABLE_GROOVE
            R.drawable.ic_groove_disable
        }
    }
    this.setBackgroundResource(drawable)
}

@BindingAdapter("inProcess")
fun LinearLayout.setBackgroundProcess(state: Boolean) {
    if (state) {
        this.setBackgroundResource(R.drawable.lot_in_process)
    } else {
        this.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
    }
}


@BindingAdapter("grooveState")
fun ImageView.setGrooveState(state: ObservableField<GroovesAdapter.State>) {
    visibility = when (state.get()) {
        GroovesAdapter.State.SELECTED -> View.VISIBLE
        else -> View.INVISIBLE
    }
}

@BindingAdapter("grooveState")
fun TextView.setGrooveState(state: ObservableField<GroovesAdapter.State>) {
    background = when (state.get()) {
        GroovesAdapter.State.DISABLED -> {
            setTextColor(ContextCompat.getColor(context, R.color.light_gray))
            ContextCompat.getDrawable(context, R.drawable.ic_groove_disable)
        }
        else -> {
            setTextColor(ContextCompat.getColor(context, R.color.dark_gray))
            ContextCompat.getDrawable(context, R.drawable.ic_normal_groove)
        }
    }
}




@BindingAdapter("statusLot")
fun TextView.setBackground(value: Boolean) {
    val drawable = if (value) {
        R.drawable.ic_button_blue
    } else {
        R.drawable.ic_button_gray
    }
    this.setBackgroundResource(drawable)
}

@BindingAdapter("selected")
fun ImageView.setSelected(value: Boolean) {
    val drawable = if (value) {
        R.drawable.ic_add_activity_select
    } else {
        R.drawable.ic_add_activity_no_select
    }
    this.setBackgroundResource(drawable)
}

@BindingAdapter("setEvaluation")
fun ImageView.setEvaluation(value: Boolean) {
    val drawable = if (value) {
        R.drawable.ic_accept_new
    } else {
        R.drawable.ic_no_acept
    }
    this.setBackgroundResource(drawable)
}

@BindingAdapter("setfont")
fun TextView.setEnableFont(enable: Boolean) {
    val font = if (enable) {
        ResourcesCompat.getFont(context, R.font.roboto_black)
    } else {
        ResourcesCompat.getFont(context, R.font.roboto_medium)
    }
    this.typeface = font
}

@BindingAdapter("textColorEnableActivity")
fun TextView.setColorEnableActivity(enable: Boolean) {
    if (enable) {
        this.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.textDisabledColor))
    }
}

@BindingAdapter("textColorSyncStatus")
fun TextView.setColorEnableSync(enable: Boolean?) {
    if (enable == null) {
        return
    }
    if (enable) {
        this.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.redAlert))
    }
}

@BindingAdapter("textColorEnable")
fun TextView.setColorEnable(enable: Boolean) {
    if (enable) {
        this.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.dark_gray))
    }
}

@BindingAdapter("textColorEnableLot")
fun TextView.setColorEnableLot(enable: Boolean) {
    if (!enable) {
        this.setTextColor(ContextCompat.getColor(context, R.color.dark_gray))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.whiteCard))
    }
}

@BindingAdapter("isActive")
fun CardView.isActive(active: Boolean) {
    this.setCardBackgroundColor(ContextCompat.getColor(context, if (active) android.R.color.white else R.color.light_gray))
}

@BindingAdapter("setStatusLot")
fun CardView.setStatusLot(active: Boolean) {
    this.setCardBackgroundColor(ContextCompat.getColor(context, if (active) R.color.colorPrimary else R.color.light_gray))
}

@BindingAdapter("disable")
fun View.disable(active: Boolean) {
    if (!active) {
        this.alpha = 0.6f
    }
}

@BindingAdapter("isEnable")
fun View.isEnable(active: Boolean) {
    this.isEnabled = active
}


@BindingAdapter("android:visibility")
fun setVisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}


