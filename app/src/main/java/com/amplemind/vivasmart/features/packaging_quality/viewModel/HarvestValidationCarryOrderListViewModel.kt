package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.vo_core.repository.HarvestValidationCarryOrderRepository
import com.amplemind.vivasmart.vo_core.utils.formatHumanDate
import com.amplemind.vivasmart.vo_core.utils.formatISODate
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class HarvestValidationCarryOrderListViewModel @Inject constructor(
        private val mRepository: HarvestValidationCarryOrderRepository
)  {

    private val mCurrentDate = Calendar.getInstance()
    private val mToday = Calendar.getInstance()

    private val mOnDateChanged = PublishSubject.create<String>()

    private val mDate = arrayOf(
            mCurrentDate.get(Calendar.YEAR),
            mCurrentDate.get(Calendar.MONTH),
            mCurrentDate.get(Calendar.DAY_OF_MONTH)
    )

    private var isoDate = formatISODate(mDate)

    var humanDate = formatHumanDate(mDate)

    val year: Int
        get() = mDate[0]

    val month: Int
        get() = mDate[1]

    val day: Int
        get() = mDate[2]

    fun previousDate(): Boolean {
        mCurrentDate.add(Calendar.DAY_OF_YEAR, -1)
        setDateComponents(mDate, mCurrentDate)
        updateDates()
        refresh()
        return true
    }

    fun nextDate(): Boolean {
        mCurrentDate.add(Calendar.DAY_OF_YEAR, 1)
        return if (mCurrentDate > mToday) {
            mCurrentDate.add(Calendar.DAY_OF_YEAR, -1)
            false
        }
        else {
            setDateComponents(mDate, mCurrentDate)
            updateDates()
            refresh()
            true
        }
    }

    fun setDate(year: Int, month: Int, day: Int): Boolean {

        mCurrentDate.set(Calendar.YEAR, year)
        mCurrentDate.set(Calendar.MONTH, month)
        mCurrentDate.set(Calendar.DAY_OF_MONTH, day)

        setDateComponents(mDate, year, month, day)
        updateDates()
        refresh()

        return true
    }

    fun refresh() {
        mOnDateChanged.onNext(isoDate)
    }

    private fun updateDates() {
        humanDate = formatHumanDate(mDate)
        isoDate = formatISODate(mDate)
    }

    private fun setDateComponents(date: Array<Int>, calendar: Calendar) {
        setDateComponents(
                date,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        )
    }

    private fun setDateComponents(date: Array<Int>, year: Int, month: Int, day: Int) {
        date[0] = year
        date[1] = month
        date[2] = day
    }

    private fun formatHumanDate(date: Array<Int>) =
            formatHumanDate(date[0], date[1], date[2])

    private fun formatISODate(date: Array<Int>) =
            formatISODate(date[0], date[1], date[2])


    fun loadHarvestValidationCarryOrders(): Observable<List<HarvestValidationCarryOrderViewModel>> =
            mOnDateChanged
                    .debounce(800, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .switchMap { date ->
                        mRepository.loadHarvestValidationCarryOrders(date)
                    }
                    .map {carryOrders ->
                        carryOrders.map {carryOrder ->
                            HarvestValidationCarryOrderViewModel(carryOrder)
                        }
                    }

}