package com.amplemind.vivasmart.features.interfaces

import com.amplemind.vivasmart.core.utils.Validations
import io.reactivex.Observable

interface IChangePasswordViewModel {

    fun validateUserPassword(password: String) : Observable<Boolean>
    fun validatePassword(password : String, repeatPassword: String) : Observable<Validations.PasswordValidation>

}