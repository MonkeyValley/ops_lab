package com.amplemind.vivasmart.features.splash

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isTablet
import com.amplemind.vivasmart.features.login.InvalidTokenActivity
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.VideoServiceContext
import com.amplemind.vivasmart.features.login.LoginActivity
import com.amplemind.vivasmart.features.login.SetPasswordActivity
import com.amplemind.vivasmart.features.main.MainActivity
import com.amplemind.vivasmart.features.splash.viewModel.SplashViewModel
import kotlinx.android.synthetic.main.content_splash_activity.*
import javax.inject.Inject

@SuppressLint("Registered")
open class SplashActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: SplashViewModel

    private lateinit var dialog: ProgressDialog

    val TAG = SplashActivity::class.java.simpleName!!

    fun getLayout() = R.layout.content_splash_activity

    @Inject
    lateinit var preferences: UserAppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        if (preferences.token != "") {
            showMainActivity()
        }
        viewModel.onValidToken().subscribe(this::onValidToken).addTo(subscriptions)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(VideoServiceContext.getContext(newBase!!))
    }

    private fun onValidToken(isValid: Boolean) {
        dialog.dismiss()
        if (isValid) {
            showPasswordActivity(viewModel.token)
        } else {
            showInvalidTokenActivity()
        }
    }

    private fun showInvalidTokenActivity() {
        val intent = Intent(this, InvalidTokenActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    fun showPasswordActivity(token: String) {
        val intent = Intent(this, SetPasswordActivity::class.java)
        intent.putExtra("token", token)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    fun showMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    fun showLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    //region DeepLink
    private fun manageDeepLinkStuff(intent: Intent) {
        //TODO: Manage deeplink data
        val action = intent.action
        val data = intent.data
        if (data != null && data.isHierarchical) {
            Log.d(TAG, "Received link click on URL: ${intent.dataString}")
            val token = data.getQueryParameter("token")
            dialog = ProgressDialog(this)
            dialog.setMessage(getString(R.string.server_connection))
            dialog.setCancelable(false)
            dialog.show()
            viewModel.validateToken(token!!).addTo(subscriptions)
        } else {
            setVideoProperties()
        }
    }
    //endregion

    //region Video
    private fun setVideoProperties() {
        val rawVideo = if (isTablet(this)) R.raw.splash_tablet else R.raw.splash
        val uri = Uri.parse("android.resource://$packageName/${rawVideo}")
        video_splash.setVideoURI(uri)
        video_splash.setOnCompletionListener {
            showLoginActivity()
        }
    }

    fun startVideo() {
        video_splash.start()
    }

    fun pauseVideo() {
        video_splash.pause()
    }
    //endregion

    //region Lifecycle
    override fun onResume() {
        super.onResume()
        startVideo()
        manageDeepLinkStuff(intent)
    }

    override fun onPause() {
        super.onPause()
        pauseVideo()
    }
    //endregion

}