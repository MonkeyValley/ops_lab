package com.amplemind.vivasmart.features.fertiriego.service

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.utils.getUUID
import io.reactivex.Single
import io.realm.Realm
import io.realm.Sort
import org.json.JSONObject
import java.util.*
import javax.inject.Inject
import kotlin.math.min

class FertirriegoService @Inject constructor() {

    var context: Context? = null

    lateinit var preferences: UserAppPreferences

    fun getLotsSoil(): Single<List<SoilModel>> =
            Realm.getDefaultInstance().use {
                Single.create<List<SoilModel>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->
                        val results = realm.where(SoilModel::class.java).findAll()
                        emitter.onSuccess(realm.copyFromRealm(results))
                    }
                }
            }

    fun getLotsHydroponic(): Single<List<HydroponicModel>> =
            Realm.getDefaultInstance().use {
                Single.create<List<HydroponicModel>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->
                        val results = realm.where(HydroponicModel::class.java).findAll()
                        emitter.onSuccess(realm.copyFromRealm(results))
                    }
                }
            }

    fun getTableCountPerLot(lotId: Int, type: String): Int {
        Realm.getDefaultInstance().use {

            val result = it
                    .where(LotModel::class.java)
                    .equalTo("id", lotId)
                    .findFirst()

            var count = 0
            if (result != null) {
                if (result.table1!! != 0) count++
                if (result.table2!! != 0) count++
                if (result.table3!! != 0) count++
                if (result.table4!! != 0) count++
                if (result.table5!! != 0) count++
                if (result.table6!! != 0) count++
            }
            return count
        }
    }

    fun getValves(lotId: Int, type: String, table: String): Single<List<ValveFertirriego>> =
            Realm.getDefaultInstance().use {

                Single.create<List<ValveFertirriego>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->

                        val result = realm.where(ValvesPerLotFertirriegoModel::class.java)
                                .equalTo("lotId", lotId)
                                .and()
                                .equalTo("type", type)
                                .findFirst()

                        when (table) {
                            "T1" -> emitter.onSuccess(realm.copyFromRealm(result!!.t1List!!.where().findAll()))
                            "T2" -> emitter.onSuccess(realm.copyFromRealm(result!!.t2List!!.where().findAll()))
                            "T3" -> emitter.onSuccess(realm.copyFromRealm(result!!.t3List!!.where().findAll()))
                            "T4" -> emitter.onSuccess(realm.copyFromRealm(result!!.t4List!!.where().findAll()))
                            "T5" -> emitter.onSuccess(realm.copyFromRealm(result!!.t5List!!.where().findAll()))
                            else -> emitter.onSuccess(realm.copyFromRealm(result!!.t6List!!.where().findAll()))
                        }
                    }
                }
            }

    fun getTableDataPerDay(lotId: Int, date: String, table: String, valveId: Int): Single<List<ValveForTable>> =
            Realm.getDefaultInstance().use {
                Single.create<List<ValveForTable>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->

                        Log.e("DateFertirriego", date)

                        val result = realm.where(TablesPerDay::class.java)
                                .equalTo("lotId", lotId)
                                .and()
                                .equalTo("date", date)
                                .findFirst()

                        when (table) {
                            "T1" -> emitter.onSuccess(realm.copyFromRealm(result!!.T1!!.where().equalTo("valveId", valveId).findAll()))
                            "T2" -> emitter.onSuccess(realm.copyFromRealm(result!!.T2!!.where().equalTo("valveId", valveId).findAll()))
                            "T3" -> emitter.onSuccess(realm.copyFromRealm(result!!.T3!!.where().equalTo("valveId", valveId).findAll()))
                            "T4" -> emitter.onSuccess(realm.copyFromRealm(result!!.T4!!.where().equalTo("valveId", valveId).findAll()))
                            "T5" -> emitter.onSuccess(realm.copyFromRealm(result!!.T5!!.where().equalTo("valveId", valveId).findAll()))
                            else -> emitter.onSuccess(realm.copyFromRealm(result!!.T6!!.where().equalTo("valveId", valveId).findAll()))
                        }
                    }
                }
            }

    fun getPHCETableDataPerDay(lotId: Int, date: String, table: String): Single<List<PHCEForTable>> =
            Realm.getDefaultInstance().use {
                Single.create<List<PHCEForTable>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->

                        Log.e("DateFertirriego", date)

                        val result = realm.where(TablesPHCEPerDay::class.java)
                                .equalTo("lotId", lotId)
                                .and()
                                .equalTo("date", date)
                                .findFirst()

                        when (table) {
                            "T1" -> emitter.onSuccess(realm.copyFromRealm(result!!.T1!!.where().findAll()))
                            "T2" -> emitter.onSuccess(realm.copyFromRealm(result!!.T2!!.where().findAll()))
                            "T3" -> emitter.onSuccess(realm.copyFromRealm(result!!.T3!!.where().findAll()))
                            "T4" -> emitter.onSuccess(realm.copyFromRealm(result!!.T4!!.where().findAll()))
                            "T5" -> emitter.onSuccess(realm.copyFromRealm(result!!.T5!!.where().findAll()))
                            else -> emitter.onSuccess(realm.copyFromRealm(result!!.T6!!.where().findAll()))
                        }
                    }
                }
            }

    fun getPulseForTable(lotId: Int, date: String, table: String): Single<List<PulseForTable>> =
            Realm.getDefaultInstance().use {
                Single.create<List<PulseForTable>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->

                        Log.e("DateFertirriego", date)

                        val result = realm.where(TablesPulsePerDay::class.java)
                                .equalTo("lotId", lotId)
                                .and()
                                .equalTo("date", date)
                                .findFirst()

                        when (table) {
                            "T1" -> emitter.onSuccess(realm.copyFromRealm(result!!.T1!!.where().sort("pulse", Sort.ASCENDING).findAll()))
                            "T2" -> emitter.onSuccess(realm.copyFromRealm(result!!.T2!!.where().sort("pulse", Sort.ASCENDING).findAll()))
                            "T3" -> emitter.onSuccess(realm.copyFromRealm(result!!.T3!!.where().sort("pulse", Sort.ASCENDING).findAll()))
                            "T4" -> emitter.onSuccess(realm.copyFromRealm(result!!.T4!!.where().sort("pulse", Sort.ASCENDING).findAll()))
                            "T5" -> emitter.onSuccess(realm.copyFromRealm(result!!.T5!!.where().sort("pulse", Sort.ASCENDING).findAll()))
                            else -> emitter.onSuccess(realm.copyFromRealm(result!!.T6!!.where().sort("pulse", Sort.ASCENDING).findAll()))
                        }
                    }
                }
            }

    fun saveValveData(valveId: Int, date: String, type: Int, m6: String, m12: String, m18: String, m24: String, barrena: Int, table: String, lotId: Int) {
        Realm.getDefaultInstance().use { realm ->
            var newValve = ValveForTable()
            var result: TablesPerDay?
            realm.executeTransaction {
                newValve = it.createObject(ValveForTable::class.java)
                newValve.valveId = valveId
                newValve.date = date
                newValve.meditionType = type
                newValve.medition6 = if (m6 != "") m6 else null
                newValve.medition12 = if (m12 != "") m12 else null
                newValve.medition18 = if (m18 != "") m18 else null
                newValve.medition24 = if (m24 != "") m24 else null
                newValve.barrena = barrena

                result = it.where(TablesPerDay::class.java)
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("date", date)
                        .findFirst()

                if (result == null) {
                    val newObj = realm.createObject(TablesPerDay::class.java)
                    newObj.lotId = lotId
                    newObj.date = date
                    result = newObj
                }

                when (table) {
                    "T1" -> result!!.T1!!.add(newValve)
                    "T2" -> result!!.T2!!.add(newValve)
                    "T3" -> result!!.T3!!.add(newValve)
                    "T4" -> result!!.T4!!.add(newValve)
                    "T5" -> result!!.T5!!.add(newValve)
                    else -> result!!.T6!!.add(newValve)
                }

                it.insertOrUpdate(result)
            }
            convertValveObjectToJson(newValve)
        }
    }

    private fun convertValveObjectToJson(obj: ValveForTable) {
        val prefer = context!!.getSharedPreferences(
                context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

        val rootObject = JSONObject()
        rootObject.put("valve_id", obj.valveId)
        rootObject.put("medition_6", obj.medition6)
        rootObject.put("medition_12", obj.medition12)
        rootObject.put("medition_18", obj.medition18)
        rootObject.put("medition_24", obj.medition24)
        rootObject.put("barrena", obj.barrena)
        rootObject.put("medition_type", obj.meditionType)
        rootObject.put("date", obj.date)
        rootObject.put("uuid", obj.uuid)

        Log.e("uuid - fertirriego", obj.uuid)

        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                val url = preferences.selectedServerUrl
                val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                uploadObject.verb = "POST"
                uploadObject.url = url + "v2/fertigation/monitoring/valve"
                uploadObject.contentType = "application/json"
                uploadObject.body = rootObject.toString()
                uploadObject.token = prefer.getString("token", "") ?: ""
                uploadObject.order = 99
                realm.insertOrUpdate(uploadObject)
            }
        }

    }

    fun savePHCEData(lotId: Int, etPH: String, etCE: String, table: String, date: String, type: String) {
        Realm.getDefaultInstance().use { realm ->
            var newPHCE = PHCEForTable()
            var result: TablesPHCEPerDay?
            realm.executeTransaction {
                newPHCE = it.createObject(PHCEForTable::class.java)
                newPHCE.lotId = lotId
                newPHCE.date = date
                newPHCE.ph = etPH
                newPHCE.ce = etCE
                newPHCE.type = type

                when (table) {
                    "T1" -> newPHCE.table = 1
                    "T2" -> newPHCE.table = 2
                    "T3" -> newPHCE.table = 3
                    "T4" -> newPHCE.table = 4
                    "T5" -> newPHCE.table = 5
                    else -> newPHCE.table = 6
                }

                result = it.where(TablesPHCEPerDay::class.java)
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("date", date)
                        .findFirst()

                if (result == null) {
                    val newObj = realm.createObject(TablesPHCEPerDay::class.java)
                    newObj.lotId = lotId
                    newObj.date = date
                    result = newObj
                }

                when (table) {
                    "T1" -> result!!.T1!!.add(newPHCE)
                    "T2" -> result!!.T2!!.add(newPHCE)
                    "T3" -> result!!.T3!!.add(newPHCE)
                    "T4" -> result!!.T4!!.add(newPHCE)
                    "T5" -> result!!.T5!!.add(newPHCE)
                    else -> result!!.T6!!.add(newPHCE)
                }

                it.insertOrUpdate(result)
            }
            convertPHCEObjectToJson(newPHCE)
        }
    }

    private fun convertPHCEObjectToJson(obj: PHCEForTable) {
        val prefer = context!!.getSharedPreferences(
                context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

        val rootObject = JSONObject()
        rootObject.put("lot_id", obj.lotId)
        rootObject.put("ph", obj.ph)
        rootObject.put("ce", obj.ce)
        rootObject.put("table", obj.table)
        rootObject.put("date", obj.date)
        rootObject.put("type", obj.type)

        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                val url = preferences.selectedServerUrl
                val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                uploadObject.verb = "POST"
                uploadObject.url = url + "v2/fertigation/monitoring/phce"
                uploadObject.contentType = "application/json"
                uploadObject.body = rootObject.toString()
                uploadObject.token = prefer.getString("token", "") ?: ""
                uploadObject.order = 99
                realm.insertOrUpdate(uploadObject)
            }
        }

    }

    fun getPulseTableDataPerDay(lotId: Int, date: String, table: String, pulse: Int, valveId: Int): Single<PulseForTable> =
            Realm.getDefaultInstance().use {
                Single.create<PulseForTable> { emitter ->
                    Realm.getDefaultInstance().use { realm ->

                        Log.e("DateFertirriego", date)

                        val result = if(valveId != 0) {
                            realm.where(PulseForTable::class.java)
                                    .beginGroup()
                                    .equalTo("lotId", lotId)
                                    .and()
                                    .equalTo("date", date)
                                    .and()
                                    .equalTo("pulse", pulse)
                                    .and()
                                    .equalTo("table", table.toInt())
                                    .and()
                                    .equalTo("valveId", valveId)
                                    .endGroup()
                                    .findFirst()
                        } else {
                            realm.where(PulseForTable::class.java)
                                    .beginGroup()
                                    .equalTo("lotId", lotId)
                                    .and()
                                    .equalTo("date", date)
                                    .and()
                                    .equalTo("pulse", pulse)
                                    .and()
                                    .equalTo("table", table.toInt())
                                    .and()
                                    .isNull("valveId")
                                    .endGroup()
                                    .findFirst()
                        }

                        Log.e("result", result.toString())

                        emitter.onSuccess(realm.copyFromRealm(result!!))
                    }
                }
            }

    fun getLotSpike(lotId: Int, table: Int): String = Realm.getDefaultInstance().use {
        it.where(PulseLotSpike::class.java)
                .equalTo("lotId", lotId)
                .and()
                .equalTo("table", table)
                .findFirst()?.spike?.toString() ?: ""
    }

    fun saveSpikeLot(lotId: Int, spike: Int, table: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                var result = realm.where(PulseLotSpike::class.java)
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("table", table)
                        .findFirst()

                if (result != null) {
                    result.spike = spike
                    realm.insertOrUpdate(result)
                } else {
                    var newSpike = realm.createObject(PulseLotSpike::class.java)
                    newSpike.lotId = lotId
                    newSpike.spike = spike
                    newSpike.table = table
                    newSpike.uuid = getUUID()
                    realm.insert(newSpike)
                }
            }
        }
    }

    fun savePulseData(lotId: Int, pulse: Int, spike: Int, sending: Int, drain: Int, minutes: Double,
                      drainPercent: Double, hour: String, date: String, table: String, ceDrain: String,
                      phDrain: String, pulseRegisted: Int, check: Boolean, valveId: Int) {
        Realm.getDefaultInstance().use { realm ->
            var newPulse = PulseForTable()
            var result: TablesPulsePerDay?
            realm.executeTransaction {
                newPulse = it.createObject(PulseForTable::class.java)
                newPulse.lotId = lotId
                newPulse.pulse = pulse
                newPulse.spike = spike
                newPulse.sending = sending
                newPulse.drain = drain
                newPulse.minutes = minutes
                newPulse.drainPercent = drainPercent
                newPulse.hour = hour
                newPulse.date = date
                newPulse.pulseRegisted = pulseRegisted
                newPulse.isCalculated = check
                newPulse.valveId = if(valveId == 0) null else valveId

                if (ceDrain != "") newPulse.ceDrain = ceDrain.toDouble()
                if (phDrain != "") newPulse.phDrain = phDrain.toDouble()

                when (table) {
                    "1" -> newPulse.table = 1
                    "2" -> newPulse.table = 2
                    "3" -> newPulse.table = 3
                    "4" -> newPulse.table = 4
                    "5" -> newPulse.table = 5
                    else -> newPulse.table = 6
                }

                result = it.where(TablesPulsePerDay::class.java)
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("date", date)
                        .findFirst()

                if (result == null) {
                    val newObj = realm.createObject(TablesPulsePerDay::class.java)
                    newObj.lotId = lotId
                    newObj.date = date
                    result = newObj
                }

                when (table) {
                    "1" -> result!!.T1!!.add(newPulse)
                    "2" -> result!!.T2!!.add(newPulse)
                    "3" -> result!!.T3!!.add(newPulse)
                    "4" -> result!!.T4!!.add(newPulse)
                    "5" -> result!!.T5!!.add(newPulse)
                    else -> result!!.T6!!.add(newPulse)
                }

                it.insertOrUpdate(result)
            }
            convertPulseObjectToJson(newPulse)
        }
    }

    private fun convertPulseObjectToJson(obj: PulseForTable) {
        val prefer = context!!.getSharedPreferences(
                context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

        val rootObject = JSONObject()
        rootObject.put("lot_id", obj.lotId)
        rootObject.put("table", obj.table)
        rootObject.put("pulse", obj.pulse)
        rootObject.put("spike", obj.spike)
        rootObject.put("sending", obj.sending)
        rootObject.put("drain", obj.drain)
        rootObject.put("minutes", obj.minutes)
        rootObject.put("drain_percent", obj.drainPercent)
        rootObject.put("hour", obj.hour)
        rootObject.put("date", obj.date)
        rootObject.put("ce_drain", obj.ceDrain)
        rootObject.put("ph_drain", obj.phDrain)
        rootObject.put("is_calculated", obj.isCalculated)
        rootObject.put("valve_id", obj.valveId)
        rootObject.put("is_sent", true)

        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                val url = preferences.selectedServerUrl
                val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                uploadObject.verb = "POST"
                uploadObject.url = url + "v2/fertigation/monitoring/pulse"
                uploadObject.contentType = "application/json"
                uploadObject.body = rootObject.toString()
                uploadObject.token = prefer.getString("token", "") ?: ""
                uploadObject.order = 99
                realm.insertOrUpdate(uploadObject)
            }
        }

    }

    fun getMaxPulse(lotId: Int, date: String): Int {
        Realm.getDefaultInstance().use {

            val result = it
                    .where(MaxPulseModel::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .findFirst()

            val pulsePerday = it
                    .where(PulseForTable::class.java)
                    .beginGroup()
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .endGroup()
                    .distinct("pulse")
                    .findAll()

            return if (result != null) result.max!!
            else { if(pulsePerday.size > 28) pulsePerday.size else 28 }
        }
    }

    fun updateMaxPulse(lotId: Int, max: Int, date: String){
        Realm.getDefaultInstance().use {

            val result = it
                    .where(MaxPulseModel::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .findFirst()

            it.executeTransaction { realm ->
                if(result != null){
                    result.max = max
                    realm.insertOrUpdate(result)
                } else{
                    val uploadObject = it.createObject(MaxPulseModel::class.java)
                    uploadObject.lotId = lotId
                    uploadObject.max = max
                    uploadObject.date = date
                    realm.insertOrUpdate(uploadObject)
                }
            }
        }
    }

    fun getLotPorcentDraint(lotId: String, date: String): String {
        Realm.getDefaultInstance().use {
            val result = it
                    .where(PulseForTable::class.java)
                    .equalTo("lotId", lotId.toInt())
                    .and()
                    .equalTo("date", date)
                    .findAll()

            return if (result.size != 0) {
                val porcent = (result.sum("drainPercent").toDouble() / result.size.toDouble())
                String.format("%.2f", porcent)
            } else ""
        }
    }

    fun getLotPorcentTableDraint(lotId: Int, date: String, table: Int): String {
        Realm.getDefaultInstance().use {
            val result = it
                    .where(PulseForTable::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .and()
                    .equalTo("table", table)
                    .findAll()

            return if (result.size != 0) {
                val porcent = (result.sum("drainPercent").toDouble() / result.size.toDouble())
                String.format("%.2f", porcent)
            } else ""
        }
    }

    fun getTableCeSend(lotId: Int, date: String, table: Int): String {
        Realm.getDefaultInstance().use {
            val result = it.where(PHCEForTable::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .and()
                    .equalTo("table", table)
                    .findFirst()

            return if (result != null) {
                String.format("%.2f", result.ce)
            } else ""
        }
    }

    fun getTablePhSend(lotId: Int, date: String, table: Int): String {
        Realm.getDefaultInstance().use {
            val result = it
                    .where(PulseForTable::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .and()
                    .equalTo("table", table)
                    .findAll()

            return if (result.size != 0) {
                val porcent = (result.sum("drainPercent").toDouble() / result.size.toDouble())
                String.format("%.2f", porcent)
            } else ""
        }
    }

    fun getTableCeSqueezed(lotId: Int, date: String, table: Int): String {
        Realm.getDefaultInstance().use {
            val result = it
                    .where(PulseForTable::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .and()
                    .equalTo("table", table)
                    .findAll()

            return if (result.size != 0) {
                val porcent = (result.sum("drainPercent").toDouble() / result.size.toDouble())
                String.format("%.2f", porcent)
            } else ""
        }
    }

    fun getTablePhSqueezed(lotId: Int, date: String, table: Int): String {
        Realm.getDefaultInstance().use {
            val result = it
                    .where(PulseForTable::class.java)
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("date", date)
                    .and()
                    .equalTo("table", table)
                    .findAll()

            return if (result.size != 0) {
                val porcent = (result.sum("drainPercent").toDouble() / result.size.toDouble())
                String.format("%.2f", porcent)
            } else ""
        }
    }

    fun countPulse(lotId: Int, table: String, date: String, valveId: Int): Int {
        Realm.getDefaultInstance().use {
            return if(valveId != 0) {
                it.where(PulseForTable::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("date", date)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .equalTo("valveId", valveId)
                        .endGroup()
                        .findAll().size
            } else {
                it.where(PulseForTable::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("date", date)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .isNull("valveId")
                        .endGroup()
                        .findAll().size
            }
        }
    }


    fun getValveForTable(lotId: Int, date: String, table: String): Single<List<ValveForTable>> =
            Realm.getDefaultInstance().use {
                Single.create<List<ValveForTable>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->

                        Log.e("DateFertirriego", date)

                        val result = realm.where(TablesPerDay::class.java)
                                .equalTo("lotId", lotId)
                                .and()
                                .equalTo("date", date)
                                .findFirst()

                        when (table) {
                            "T1" -> emitter.onSuccess(realm.copyFromRealm(result!!.T1!!.where().sort("valveId", Sort.ASCENDING).findAll()))
                            "T2" -> emitter.onSuccess(realm.copyFromRealm(result!!.T2!!.where().sort("valveId", Sort.ASCENDING).findAll()))
                            "T3" -> emitter.onSuccess(realm.copyFromRealm(result!!.T3!!.where().sort("valveId", Sort.ASCENDING).findAll()))
                            "T4" -> emitter.onSuccess(realm.copyFromRealm(result!!.T4!!.where().sort("valveId", Sort.ASCENDING).findAll()))
                            "T5" -> emitter.onSuccess(realm.copyFromRealm(result!!.T5!!.where().sort("valveId", Sort.ASCENDING).findAll()))
                            else -> emitter.onSuccess(realm.copyFromRealm(result!!.T6!!.where().sort("valveId", Sort.ASCENDING).findAll()))
                        }
                    }
                }
            }

    fun saveM3HA(valveUuId: String, pulsesNo: Int, minutes: Double, m3ha: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                var result = realm
                        .where(ValveForTable::class.java)
                        .equalTo("uuid", valveUuId)
                        .findFirst()

                if (result != null) {
                    result.m3ha = m3ha
                    result.pulsesNo = pulsesNo
                    result.minutes = minutes

                    realm.insertOrUpdate(result)
                }
            }
        }

        convertM3HAObjectToJson(valveUuId, pulsesNo, minutes, m3ha)
    }

    private fun convertM3HAObjectToJson(valveUuId: String, pulsesNo: Int, minutes: Double, m3ha: Int) {
        val prefer = context!!.getSharedPreferences(
                context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

        val rootObject = JSONObject()
        rootObject.put("m3ha", m3ha)
        rootObject.put("pulses_no", pulsesNo)
        rootObject.put("minutes", minutes)

        Log.e("valveUuId", valveUuId)

        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                val url = preferences.selectedServerUrl
                val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                uploadObject.verb = "PUT"
                uploadObject.url = url + "v2/fertigation/monitoring/valve/update/" + valveUuId
                uploadObject.contentType = "application/json"
                uploadObject.body = rootObject.toString()
                uploadObject.token = prefer.getString("token", "") ?: ""
                uploadObject.order = 99
                realm.insertOrUpdate(uploadObject)
            }
        }

    }

}
