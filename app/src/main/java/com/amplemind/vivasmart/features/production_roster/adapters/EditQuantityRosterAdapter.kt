package com.amplemind.vivasmart.features.production_roster.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.onChange
import com.amplemind.vivasmart.databinding.ItemEditQuantityRosterBinding
import com.amplemind.vivasmart.features.production_roster.viewModels.ItemEditQuantityRosterViewModel
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.item_edit_quantity_roster.view.*

class EditQuantityRosterAdapter : RecyclerView.Adapter<EditQuantityRosterAdapter.EditQuantityRosterViewHolder>() {

    private var list : MutableList<ItemEditQuantityRosterViewModel>? = null
    private val clickSubject = BehaviorSubject.create<ItemEditQuantityRosterViewModel>()!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditQuantityRosterViewHolder {
        val binding = ItemEditQuantityRosterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EditQuantityRosterViewHolder(binding)
    }

    override fun getItemCount() = list?.size ?: 0

    override fun onBindViewHolder(holder: EditQuantityRosterViewHolder, position: Int) {
        holder.bind(list!![position])
    }

    fun onClick(): BehaviorSubject<ItemEditQuantityRosterViewModel> {
        return clickSubject
    }

    inner class EditQuantityRosterViewHolder(val binding: ItemEditQuantityRosterBinding) : RecyclerView.ViewHolder(binding.root) {

        var item: ItemEditQuantityRosterViewModel? = null

        fun bind(item: ItemEditQuantityRosterViewModel) {
            this.item = item

            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            binding.root.ed_time.setOnClickListener {
                item.position = adapterPosition
                clickSubject.onNext(item)
            }
            binding.root.ed_quantity.onChange {
                item.quantity = it
            }
        }
    }

    fun addElements(data: MutableList<ItemEditQuantityRosterViewModel>) {
        list = data
        notifyDataSetChanged()
    }


}
