package com.amplemind.vivasmart.features.production_roster

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.amplemind.vivasmart.R

class ReportRosterViewPager(
        context: Context,
        supportFragmentManager: FragmentManager?,
        stageUuid: String,
        stageTypeId: Int,
        nameStage:String
        ) : FragmentPagerAdapter(supportFragmentManager!!) {

    private val titles = mutableListOf<String>()
    private val fragments = mutableListOf<Fragment>()

    init {
        createTitles(context, nameStage)
        createFragments(stageUuid, stageTypeId)
    }

    private fun createTitles(context: Context, nameStage: String) {
        titles.add(context.getString(R.string.rra_in_process))
        titles.add(context.getString(R.string.rra_done))
        if(nameStage == "Labores Culturales"){
        titles.add(context.getString(R.string.rra_daily_pay))
        }
    }

    private fun createFragments(stageUuid: String, stageTypeId: Int) {
        fragments.add(ProcessRosterFragment.newInstance(stageUuid, stageTypeId))
        fragments.add(RevisedRosterFragment.newInstance(stageUuid, stageTypeId, 1))
        fragments.add(RevisedRosterFragment.newInstance(stageUuid, stageTypeId, 2))
    }

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = titles.size

    override fun getPageTitle(position: Int) = titles[position]

    fun getRegisteredFragment(position: Int): Fragment? {
        return fragments[position]
    }

}
