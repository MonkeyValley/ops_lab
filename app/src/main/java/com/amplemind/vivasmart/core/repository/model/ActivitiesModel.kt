package com.amplemind.vivasmart.core.repository.model

import androidx.room.*
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.airbnb.lottie.LottieCompositionFactory.fromJson
import com.google.gson.reflect.TypeToken
import java.util.*
import com.google.gson.Gson




@Entity
data class ActivitiesModel(
        @SerializedName("id") @ColumnInfo(name = "activity_id") @PrimaryKey(autoGenerate = false) val id: Int?,
        @SerializedName("activity_area_id") val activityAreaId: Int?,
        @SerializedName("activity_category_id") val activityCategoryId: Int?,
        @SerializedName("activity_sub_category_id") val activitySubCategoryId: Int?,
        @SerializedName("activity_type_id") val activityTypeId: Int?,
        @SerializedName("business_unit_id") val businessUnitId: Int?,
        @SerializedName("code") val code: Int?,
        @SerializedName("cost") val cost: Double?,
        @TypeConverters(CropUnitTypeConverters::class) @SerializedName("crops") val crops: List<Crop>?,
        @SerializedName("cost_per_hour") val costPerHour: Double?,
        @SerializedName("extra_time_cost") val extra_time_cost: Double?,
        @SerializedName("practice_cost") val practiceCost: Double?,
        @SerializedName("is_head") var isHead: Boolean?,
        @SerializedName("sunday_rate") val sunday_rate: Boolean?,
        @SerializedName("is_practice") val isPractice: Boolean?,
        @SerializedName("description") val description: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("is_active") @ColumnInfo(name = "is_active_activity") val isActive: Boolean?,
        @SerializedName("is_grooves") val isGrooves: Boolean?,
        @SerializedName("is_removable") val isRemovable: Boolean?,
        @SerializedName("name") val name: String?,
        @SerializedName("performance") val performance: Double?,
        @SerializedName("range_from") val rangeFrom: Int?,
        @SerializedName("range_to") val rangeTo: Int?,
        @Embedded @SerializedName("unit") val unit: CropUnit?,
        @SerializedName("unit_id") val unitId: Int?,
        @SerializedName("unit_limit") val unitLimit: Int?,
        @SerializedName("stage_activities") var stage_activities: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.createTypedArrayList(Crop),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readParcelable(CropUnit::class.java.classLoader),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeValue(activityAreaId)
        parcel.writeValue(activityCategoryId)
        parcel.writeValue(activitySubCategoryId)
        parcel.writeValue(activityTypeId)
        parcel.writeValue(businessUnitId)
        parcel.writeValue(code)
        parcel.writeValue(cost)
        parcel.writeTypedList(crops)
        parcel.writeValue(costPerHour)
        parcel.writeValue(extra_time_cost)
        parcel.writeValue(practiceCost)
        parcel.writeValue(isHead)
        parcel.writeValue(sunday_rate)
        parcel.writeValue(isPractice)
        parcel.writeString(description)
        parcel.writeString(icon)
        parcel.writeValue(isActive)
        parcel.writeValue(isGrooves)
        parcel.writeValue(isRemovable)
        parcel.writeString(name)
        parcel.writeValue(performance)
        parcel.writeValue(rangeFrom)
        parcel.writeValue(rangeTo)
        parcel.writeParcelable(unit, flags)
        parcel.writeValue(unitId)
        parcel.writeValue(unitLimit)
        parcel.writeByte(if (stage_activities) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActivitiesModel> {
        override fun createFromParcel(parcel: Parcel): ActivitiesModel {
            return ActivitiesModel(parcel)
        }

        override fun newArray(size: Int): Array<ActivitiesModel?> {
            return arrayOfNulls(size)
        }
    }
}

@Entity
data class CropUnit(
        @SerializedName("id") @PrimaryKey(autoGenerate = false) val id: Int?,
        @SerializedName("business_unit_names") val business_unit_names: String?,
        @SerializedName("cost") @ColumnInfo(name = "crop_cost") val cost: Double?,
        @SerializedName("equivalent") val equivalent: Double?,
        @SerializedName("image") val image: String?,
        @SerializedName("is_active") @ColumnInfo(name = "is_active_crop") val is_active: Boolean?,
        @SerializedName("name") @ColumnInfo(name = "crop_name") val name: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(business_unit_names)
        parcel.writeValue(cost)
        parcel.writeValue(equivalent)
        parcel.writeString(image)
        parcel.writeValue(is_active)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CropUnit> {
        override fun createFromParcel(parcel: Parcel): CropUnit {
            return CropUnit(parcel)
        }

        override fun newArray(size: Int): Array<CropUnit?> {
            return arrayOfNulls(size)
        }
    }
}

@Entity
data class Crop(
        @PrimaryKey(autoGenerate = true) val id_local_crop : Int? = null,
        @SerializedName("activity_id") val activityId: Int?,
        @SerializedName("crop_id") val cropId: Int?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id_local_crop)
        parcel.writeValue(activityId)
        parcel.writeValue(cropId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Crop> {
        override fun createFromParcel(parcel: Parcel): Crop {
            return Crop(parcel)
        }

        override fun newArray(size: Int): Array<Crop?> {
            return arrayOfNulls(size)
        }
    }
}

class CropUnitTypeConverters {

    companion object {
        private val gson = Gson()

        @TypeConverter
        @JvmStatic
        fun stringToCropsList(data: String?): List<CropUnit> {
            if (data == null) {
                return Collections.emptyList()
            }
            val listType = object : TypeToken<List<CropUnit>>() {}.type
            return gson.fromJson(data, listType)
        }

        @TypeConverter
        @JvmStatic
        fun cropsListToString(crops: List<CropUnit>): String {
            return gson.toJson(crops)
        }
    }
}