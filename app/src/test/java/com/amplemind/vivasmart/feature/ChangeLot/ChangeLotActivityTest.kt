package com.amplemind.vivasmart.feature.ChangeLot

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.Button
import com.amplemind.vivasmart.AndroidTest
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.production_roster.ChangeLotActivity
import org.junit.Before
import org.junit.Test
import org.robolectric.Robolectric
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class ChangeLotActivityTest : AndroidTest(){

    private var changeLotActivity : ChangeLotActivity? = null

    @Before
    fun setup(){
        changeLotActivity = Robolectric.buildActivity(ChangeLotActivity::class.java)
                .create()
                .resume()
                .get()

    }

    @Test
    fun `Àctivity should not be null`(){
        assertNotNull(changeLotActivity)
    }

    @Test
    fun `verify status recycler`(){
        //given
        val recycler = changeLotActivity?.findViewById<RecyclerView>(R.id.rv_change_lot)

        //then
        assertEquals(View.VISIBLE, recycler!!.visibility)
        assertNotNull(recycler.adapter)
    }

    @Test
    fun `detect finish activity cancel`(){
        val btnCancel = changeLotActivity?.findViewById<Button>(R.id.btn_cancel)
        assertNotNull(btnCancel)

        btnCancel!!.performClick()

        assertTrue(changeLotActivity!!.isFinishing)
    }

    @Test
    fun `detect finish activity acept`(){
        val btnAcept = changeLotActivity?.findViewById<Button>(R.id.btn_acept)
        assertNotNull(btnAcept)

        btnAcept!!.performClick()

        assertTrue(changeLotActivity!!.isFinishing)
    }

}