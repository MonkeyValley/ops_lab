package com.amplemind.vivasmart.vo_core.repository.dao.realm

import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.extensions.grooveTotal
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.ActivityCodeUploader
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.Realm
import io.realm.Sort
import javax.inject.Inject

class ActivityCodesDao @Inject constructor(
        realmHolder: RealmHolder,
        private val mActivityCodeUploader: ActivityCodeUploader,
        private val mUploadObjectDao: UploadObjectDao
) : RealmDao<ActivityCodeModel>(realmHolder, ActivityCodeModel::class.java) {

    companion object {
        const val OPEN_LIMIT = 2
    }

    fun findAllForActivity(activityUuid: String): Single<List<ActivityCodeModel>> =
            findAll({ realm ->
                query(realm)
                        .equalTo("activityUuid", activityUuid)
            }, false)

    fun findAllForStageAndActivity(stageUuid: String, activityUuid: String): Observable<Changes<ActivityCodeModel>> =
            findAllLiveAsync { realm ->
                query(realm).equalTo("stageUuid", stageUuid)
                        .and()
                        .equalTo("activityUuid", activityUuid)
                        .and()
                        .equalTo("isOpen", true)
                        .sort("code", Sort.ASCENDING)
            }

    fun create(stageUuid: String, activityUuid: String): Single<ActivityCodeModel> {

        return create( func = { realm, code ->

            val count = query(realm)
                    .equalTo("stageUuid", stageUuid)
                    .and()
                    .equalTo("activityUuid", activityUuid)
                    .and()
                    .equalTo("isOpen", true)
                    .count()

            if (count >= OPEN_LIMIT) {
                throw OpenActivityCodesExceeded()
            }

            val auxStage = realm.where(StageModel::class.java)
                    .equalTo("uuid", stageUuid)
                    .findFirst()

            val auxActivity = realm.where(ActivityModel::class.java)
                    .equalTo("uuid", activityUuid)
                    .findFirst()

            if (auxStage != null && auxActivity != null) {
                val lastCode = query(realm)
                        .equalTo("stageUuid", stageUuid)
                        .and()
                        .equalTo("activityUuid", activityUuid)
                        .sort("code", Sort.DESCENDING)
                        .findFirst()

                val totalSpaces =
                        if (auxActivity.isGrooves == true)
                            auxStage.lot.grooveTotal()
                        else
                            auxActivity.unitLimit

                code.occupiedSpaces = 0
                code.totalSpaces = totalSpaces
                code.code = generateCode(lastCode, auxActivity)
                code.stage = auxStage
                code.stageId = auxStage.id
                code.stageUuid = stageUuid
                code.activity = auxActivity
                code.activityId = auxActivity.id
                code.activityUuid = activityUuid
            }
            else {
                throw IllegalStateException("Neither Stage with uuid: $stageUuid or Activity with uuid: $activityUuid could be found!")
            }
        }, after = { realm, code ->
            mUploadObjectDao.queueForUploadAux(realm, mActivityCodeUploader.create(code))
        })
    }

    fun addSpacesAux(realm: Realm, activityCodeUuid: String, spaces: Int) {
        query(realm).equalTo("uuid", activityCodeUuid)
                .findFirst()
                ?.apply {
                    occupiedSpaces += spaces
                }
    }

    fun closeIfFullAux(realm: Realm, activityCodeUuid: String) {
        query(realm)
                .equalTo("uuid", activityCodeUuid)
                .findFirst()
                ?.apply {
                    if (occupiedSpaces >= totalSpaces) {
                        isOpen = false
                    }
                }
    }


    private fun generateCode(lastCode: ActivityCodeModel?, activity: ActivityModel): Int =
            when {
                lastCode == null -> activity.rangeFrom
                lastCode.code < activity.rangeTo -> lastCode.code + 1
                else -> activity.rangeTo
            }

    class OpenActivityCodesExceeded() : Exception("There cannot be more than $OPEN_LIMIT activity codes open")

}