package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import androidx.databinding.ObservableField
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_core.repository.models.utils.profileImageUrl
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.StatefulAdapter

class ItemCollaboratorViewModel( item: CollaboratorModel )
    : StatefulAdapter.ViewModel<CollaboratorModel>(item) {

    val name = ObservableField<String>()
    val code = ObservableField<String>()
    val activityId = ObservableField<Int>()

    val profileImage: String
        get() = item.profileImageUrl

    var isSelected: Boolean = false
        set(value) {
            contentsChanged = field != value
            field = value
        }

    override fun bind(item: CollaboratorModel) {
        name.set(item.name)
        code.set(item.employeeCode)
        activityId.set(item.activityId)
    }

}