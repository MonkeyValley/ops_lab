package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.QuantityOrderRepository
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderDetailResponse
import io.reactivex.Observable
import retrofit2.http.*

interface CarryOrderApi {

    @GET("/v2/quality_carry_order")
    @Headers("Content-Type: application/json")
    fun getQualityCarryOrderDetail(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "carry_order",
            @Query("carry_order.id__eq") id_carry: Int,
            @Query("percent") percent: Boolean = true,
            @Query("__logic") logic: String = "AND"
    ): Observable<QualityCarryOrderDetailResponse>

    @PUT("/v2/quality_carry_order/detail/{id_carry}")
    @Headers("Content-Type: application/json")
    fun sendCarryOrderEvaluation(
            @Header("Authorization") authentication: String,
            @Path("id_carry") id_carry: Int,
            @Body body: QuantityOrderRepository.CarryEvaluationRequest
    ): Observable<Any>

    @GET("/v2/quality_carry_order/detail")
    @Headers("Content-Type: application/json")
    fun getQualifiedCarryOrders(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "carry_order",
            @Query("percent") percent: Boolean = true,
            @Query("carry_order.lot_id") id_lot: Int,
            @Query("__logic") logic: String = "AND"
    ): Observable<QualityCarryOrderDetailResponse>

    @GET("/v2/quality_carry_order/detail")
    @Headers("Content-Type: application/json")
    fun getQualifiedEvaluation(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "carry_order",
            @Query("id") idCarry: Int,
            @Query("percent") percent: Boolean = true
    ): Observable<QualityCarryOrderDetailResponse>


}
