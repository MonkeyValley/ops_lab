package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName

data class IssueModel(
        @SerializedName("id")               val id: Long,
        @SerializedName("is_active")        val isActive: Boolean,
        @SerializedName("name")             val name: String,
        @SerializedName("image")            val image: String?,
        @SerializedName("description")      val description: String?,
        @SerializedName("activity_id")      var activityId: Int = 0,
        @SerializedName("area_id")          var areaId: Int = 0,
        @SerializedName("category_id")      var category: Int = 0,
        @SerializedName("sub_category_id")  var subCategory: Int = 0,
        @SerializedName("is_condition")     var isCondition: Boolean = false
)

