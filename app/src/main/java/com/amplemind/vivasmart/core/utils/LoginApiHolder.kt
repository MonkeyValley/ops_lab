package com.amplemind.vivasmart.core.utils

import com.amplemind.vivasmart.core.repository.remote.AccountApi
import javax.inject.Inject

class LoginApiHolder @Inject constructor(private val retrofitHolder: RetrofitHolder) {

    val api
        get() = buildApi()

    private lateinit var mOldApi: AccountApi

    private fun buildApi(): AccountApi {

        if (retrofitHolder.isHostChanged || !::mOldApi.isInitialized) {
            mOldApi = retrofitHolder.create(AccountApi::class.java)
        }

        return mOldApi
    }
}