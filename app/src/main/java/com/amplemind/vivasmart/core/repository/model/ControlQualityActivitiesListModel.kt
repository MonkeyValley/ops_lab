package com.amplemind.vivasmart.core.repository.model

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity
data class ControlQualityActivitiesListModel(@SerializedName("activity_log_id") @PrimaryKey var activityLogId: Int,
                                             @SerializedName("activity_name") var activityName: String,
                                             @SerializedName("count") var total: Int,
                                             @SerializedName("activity_area_id") var activityAreaId: Int,
                                             @SerializedName("activity_category_id") var activityCategoryId: Int,
                                             @SerializedName("activity_id") var activityId: Int,
                                             @SerializedName("activity_sub_category_id") var activitySubCategoryId: Int,
                                             @SerializedName("quality_sign") var qualitySign: String,
                                             @SerializedName("sign_date") var signDate: String?,
                                             @SerializedName("supervisor_quality_sign") var supervisorQualitySign: String,
                                             @Ignore @SerializedName("grooves") var grooves: List<ControlQualityGroovesModel>,
                                             @ColumnInfo(name = "collaborator_id") var collaboratorId: Long?,
                                             @SerializedName("score") var score: Int = 0) {
    constructor(): this(0, "", 0, 0, 0, 0, 0, "",
            null, "", listOf(), null, 0)
}


@Entity(foreignKeys = arrayOf(ForeignKey(
        entity = ControlQualityActivitiesListModel::class,
        parentColumns = arrayOf("activityLogId"),
        childColumns = arrayOf("control_quality_id"))
))
data class ControlQualityGroovesModel(@SerializedName("groove_no") val grooveNo: Int,
                                      @SerializedName("quality_comment") val comment: String?,
                                      @SerializedName("created_at") val date: String?,
                                      @SerializedName("quality_control") var qualityControl: Boolean,
                                      @SerializedName("id") @PrimaryKey @ColumnInfo(name = "groove_id") val id: Int,
                                      @SerializedName("table_no") val tableNo: Int,
                                      @ColumnInfo(name = "control_quality_id") var controlQualityId: Int,
                                      val errorSync: Boolean = false)