package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.core.repository.model.PayrollMenuModel

class ItemMenuPayrollViewModel(item: PayrollMenuModel) {

    var name = item.name
    var drawable = item.drawable
    var active = item.active

}
