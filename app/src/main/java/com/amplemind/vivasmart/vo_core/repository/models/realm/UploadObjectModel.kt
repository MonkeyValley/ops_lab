package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.utils.TYPE_JSON
import com.amplemind.vivasmart.vo_core.utils.getUUID
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class UploadObjectModel (
        @PrimaryKey
        var uuid: String = getUUID(),
        var verb: String = "POST",
        var url: String = "",
        var contentType: String = TYPE_JSON,
        var body: String = "",
        var errorCode: String? = null,
        var errorServerResponse: String? = null,
        var token: String = "",
        var order: Long = 0
): RealmObject()

