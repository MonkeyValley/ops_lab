package com.amplemind.vivasmart.features.haulage.viewModel

import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.repository.model.CarryOrderReportModel
import com.amplemind.vivasmart.core.repository.response.CarryOrdersReportResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.sync_forced.service.SyncForcedService
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel
import com.amplemind.vivasmart.vo_core.utils.formatHumanDate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

class HaulageReportViewModel @Inject constructor(val repository: PackagingQualityRepository,
                                                 private val service: SyncForcedService,
                                                 private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val mOnShowProgress = PublishSubject.create<Boolean>()
    private val mOnReportLoaded = PublishSubject.create<List<ItemHaulageReportViewModel>>()
    private val mOnReportLoadingError = PublishSubject.create<Throwable>()
    private val requestVarietiesSuccess = BehaviorSubject.create<List<VarietyModel>>()

    private var mCurrentDisposable: Disposable? = null

    private val mDefaultFromTime = "00:00:00"
    private val mDefaultToTime = "23:59:59"

    private val mDate = Calendar.getInstance()

    private val mFromDate = arrayOf(
            mDate.get(Calendar.YEAR),
            mDate.get(Calendar.MONTH),
            mDate.get(Calendar.DAY_OF_MONTH)
    )

    private val mToDate = arrayOf(
            mDate.get(Calendar.YEAR),
            mDate.get(Calendar.MONTH),
            mDate.get(Calendar.DAY_OF_MONTH))

    var fromDate = formatDateTime(mFromDate, mDefaultFromTime)
    var toDate = formatDateTime(mToDate, mDefaultToTime)

    var humanFromDate = formatHumanDate(mFromDate)
    var humanToDate = formatHumanDate(mToDate)

    val toYear: Int
        get() = mToDate[0]

    val toMonth: Int
        get() = mToDate[1]

    val toDay: Int
        get() = mToDate[2]

    val fromYear: Int
        get() = mFromDate[0]

    val fromMonth: Int
        get() = mFromDate[1]

    val fromDay: Int
        get() = mFromDate[2]


    fun setFromDate(year: Int, month: Int, day: Int) {
        setDateComponents(mFromDate, year, month, day)
        humanFromDate = formatHumanDate(mFromDate)
        fromDate = formatDateTime(mFromDate, mDefaultFromTime)
    }

    fun setToDate(year: Int, month: Int, day: Int) {
        setDateComponents(mToDate, year, month, day)
        humanToDate = formatHumanDate(mToDate)
        toDate = formatDateTime(mToDate, mDefaultToTime)
    }

    private fun formatDateTime(date: Array<Int>, time: String) =
            "${formatISODate(date)} $time"

    private fun setDateComponents(date: Array<Int>, year: Int, month: Int, day: Int) {
        date[0] = year
        date[1] = month
        date[2] = day
    }

    private fun formatHumanDate(date: Array<Int>) =
            formatHumanDate(date[0], date[1], date[2])

    private fun formatISODate(date: Array<Int>) =
            com.amplemind.vivasmart.vo_core.utils.formatISODate(date[0], date[1], date[2])


    fun loadCarryOrdersReport() =
            repository.loadCarryOrdersReportBetween(fromDate, toDate)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        mOnShowProgress.onNext(true)
                    }
                    .doOnComplete {
                        mOnShowProgress.onNext(false)
                        disposeCurrentRequest()
                    }
                    .doOnError {
                        mOnShowProgress.onNext(false)
                        it.printStackTrace()
                    }
                    .map(this::mapCarryOrders)
                    .subscribe({
                        mOnReportLoaded.onNext(it)
                    }, {
                        mOnReportLoadingError.onNext(it)
                    })

    fun disposeCurrentRequest() {
        mCurrentDisposable?.apply {
            if (isDisposed) {
                dispose()
            }
            mCurrentDisposable = null
        }
    }

    private fun mapCarryOrders(carryOrders: CarryOrdersReportResponse): List<ItemHaulageReportViewModel> {
        val data = carryOrders.data

        val viewModels = mutableListOf<ItemHaulageReportViewModel>()

        data.forEachIndexed { i: Int, order: CarryOrderReportModel ->
            viewModels.add(ItemHaulageReportViewModel(
                    order.date,
                    order.folio,
                    order.boxNo,
                    order.validBoxNo!!,
                    order.reception
            ))
        }

        return viewModels
    }

    fun onShowProgress(callback: (Boolean) -> Unit) =
            mOnShowProgress.subscribe(callback)

    fun onReportLoaded(callback: (List<ItemHaulageReportViewModel>) -> Unit) =
            mOnReportLoaded.subscribe(callback)

    fun onReportLoadingError(callback: (Throwable) -> Unit) =
            mOnReportLoadingError.subscribe(callback)

    fun getVarietiesStage(stageId: Int): Disposable =
            repository.getVarietiesByStage(stageId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { }
                    .doOnTerminate { }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        it.forEach { variety ->
                            variety.stageId = stageId
                        }
                        requestVarietiesSuccess.onNext(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("GrooveForced", "error", it)
                    })

    fun onSuccessVaritiesRequest(): BehaviorSubject<List<VarietyModel>> = requestVarietiesSuccess
    fun uploadVarieties(list: List<VarietyModel>): Boolean = service.uploadVarieties(list)

    fun getStage(): List<StageModel> = service.getStage()

}
