package com.amplemind.vivasmart.features.mipe.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.LotMipeItemBinding
import com.amplemind.vivasmart.features.mipe.viewModel.LotMipeItemViewModel
import androidx.databinding.library.baseAdapters.BR
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_TPYE
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.realm.Realm
import java.util.*

class LotsInMipeAdapter : RecyclerView.Adapter<LotsInMipeAdapter.LotsInMipeViewHolder>() {
    private var list = mutableListOf<LotMipeItemViewModel>()

    var listener: Listener? = null
    var type = ""

    private val clickSubject = PublishSubject.create<LotMipeItemViewModel>()

    private var mRecyclerView: RecyclerView? = null

    fun addElements(data: List<LotMipeItemViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LotsInMipeViewHolder {
        val binding = LotMipeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LotsInMipeViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: LotsInMipeViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem() = clickSubject

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mRecyclerView = null
    }

    inner class LotsInMipeViewHolder(private val binding: LotMipeItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: LotMipeItemViewModel

        fun bind(item: LotMipeItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item


            Realm.getDefaultInstance().use {
                val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
                val week = calendar.get(Calendar.WEEK_OF_YEAR)

                val result = it
                        .where(MipeMonitoringModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", item.lot_id)
                        .and()
                        .equalTo("week", week)
                        .and()
                        .equalTo("finished", true)
                        .endGroup()
                        .findAll()

                val lot = it
                        .where(LotModel::class.java)
                        .equalTo("id", item.lot_id)
                        .distinct("id")
                        .findFirst()

                val grooveCount = lot!!.table1 + lot!!.table2 + lot!!.table3 + lot!!.table4 + lot!!.table5 + lot!!.table6

                if (result.size != 0) {
                    when (result.size) {
                        grooveCount -> binding.imageView2.setImageResource(R.drawable.ic_inv_circle_complete)
                        else -> binding.imageView2.setImageResource(R.drawable.ic_inv_circle_pending)
                    }
                } else {
                    binding.imageView2.setImageResource(R.drawable.ic_inv_circle_incomplete)
                }
            }

            binding.root.setOnClickListener {
                clickSubject.onNext(this.item)
            }

            if(type == "3") {
                when (validateDayRvision(item.lot_id)) {
                    1 -> binding.imageView2.setImageResource(R.drawable.ic_inv_circle_pending)
                    2 -> binding.imageView2.setImageResource(R.drawable.ic_inv_circle_complete)
                    else -> binding.imageView2.setImageResource(R.drawable.ic_inv_circle_incomplete)
                }
            }

        }

    }

    fun validateDayRvision(lotId: Int): Int {
        Realm.getDefaultInstance().use {
            var validate = 0
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
            var week = calendar.get(Calendar.WEEK_OF_YEAR)

            var stageId = it
                    .where(StageModel::class.java)
                    .equalTo("lotId", lotId)
                    .findFirst()
                    ?.id ?: 0

            val result = it
                    .where(PollinationModel::class.java)
                    .equalTo("stageId", stageId)
                    .and()
                    .equalTo("week", week)
                    .findAll()

            val lot = it
                    .where(LotModel::class.java)
                    .equalTo("id", lotId)
                    .findFirst()

            var count = 0
            if(lot != null) {
                if (lot.table1 != 0) {
                    count++
                }
                if (lot.table2 != 0) {
                    count++
                }
                if (lot.table3 != 0) {
                    count++
                }
                if (lot.table4 != 0) {
                    count++
                }
                if (lot.table5 != 0) {
                    count++
                }
                if (lot.table6 != 0) {
                    count++
                }
            }

            if (result.size != 0) {
                    validate = when {
                        result.size!! < count -> 1
                        else -> 2
                    }
            } else validate = 0

            return validate
        }
    }

    interface Listener {
        fun onGetCounterValue(osition: Int): Observable<Int>?
    }

}