package com.amplemind.vivasmart.features.root_quality

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.extensions.setImageUrl
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.CustomCamera.CameraActivity
import com.amplemind.vivasmart.features.fertiriego.FertirriegoHydroponicMonitoringActivity
import com.amplemind.vivasmart.features.root_quality.viewModel.RootQualityViewModel
import com.amplemind.vivasmart.features.root_quality.viewModel.rootQualityModel
import java.io.File
import java.io.IOException
import javax.inject.Inject

class RootQualityDialog : BaseDialogFragment() {

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mPermission: AndroidPermission

    @Inject
    lateinit var viewModel: RootQualityViewModel

    var btnTable1: Button?= null
    var btnTable2: Button?= null
    var btnTable3: Button?= null
    var btnTable4: Button?= null
    var btnTable5: Button?= null
    var btnTable6: Button?= null
    var btnVeryGood: Button?= null
    var btnGood: Button?= null
    var btnRegular: Button?= null
    var btnBad: Button?= null
    var btnImagen: ImageButton?= null
    var etComments: TextView? = null

    private lateinit var path: String
    private lateinit var imagenBitmap: Bitmap
    private lateinit var mview: View
    private val CAMERA_REQUEST_CODE = 1
    private var table = 1
    private var quality = 0
    private var rootWeek = ArrayList<rootQualityModel>()
    private var newRoot = true

    var image1: File? = null

    companion object {
        val TAG = BaseDialogFragment::class.java.simpleName
        var lotIdRoot = 0
    }

    fun newInstance(lotId: Int): RootQualityDialog {
        lotIdRoot = lotId
        return RootQualityDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mview = LayoutInflater.from(context).inflate(R.layout.fragment_root_quality_dialog, null, false)

        addSubscribers()
        setUpUi(mview)

        viewModel.setServiceContect(activity!!)

        builder.setCancelable(false)
        builder.setView(mview)
        builder.setPositiveButton("ENVIAR", null)
        builder.setNegativeButton("CANCELAR") { _, _ -> dismiss() }
        val create = builder.create()
        create.setOnShowListener {
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if(ifConected()) { if(validate()) uploadImage() }
                    }
        }

        viewModel.getRootQualityData(lotIdRoot)
        return create
    }

    private fun addSubscribers(){
        viewModel.onSuccessGetDataRootQuality().subscribe(this::getRootQualityData).addTo(subscriptions)
        viewModel.onSuccessUpdateImageUrl().subscribe(this::getImagesList).addTo(subscriptions)
    }

    fun getRootQualityData(list: List<rootQualityModel>){
        Log.e("rootQualityList", list.size.toString() )
        rootWeek.clear()
        rootWeek.addAll(list)
        loadRootTableData()
    }

    fun getImagesList(imageUrl: String) {
        viewModel.saveRootQuality(lotIdRoot, table, imageUrl, etComments!!.text.toString(), quality)

        val builder = android.app.AlertDialog.Builder(activity!!)
        builder.setTitle("Calida de raiz - Tabla " + table)
        builder.setMessage("Información de calidad de raiz de la tabla "+ table + " se ha guarado con exito.")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            setEnable(false)
            dialog.dismiss()
        }
        builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
        builder.show()
    }

    private fun setUpUi(view: View) {
        btnTable1 = view.findViewById(R.id.btn_table1)
        btnTable1!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
        btnTable1!!.setOnClickListener { v -> onClick(v) }
        btnTable2 = view.findViewById(R.id.btn_table2)
        btnTable2!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnTable2!!.setOnClickListener { v -> onClick(v) }
        btnTable3 = view.findViewById(R.id.btn_table3)
        btnTable3!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnTable3!!.setOnClickListener { v -> onClick(v) }
        btnTable4 = view.findViewById(R.id.btn_table4)
        btnTable4!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnTable4!!.setOnClickListener { v -> onClick(v) }
        btnTable5 = view.findViewById(R.id.btn_table5)
        btnTable5!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnTable5!!.setOnClickListener { v -> onClick(v) }
        btnTable6 = view.findViewById(R.id.btn_table6)
        btnTable6!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnTable6!!.setOnClickListener { v -> onClick(v) }
        btnVeryGood = view.findViewById(R.id.btn_very_good)
        btnVeryGood!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnVeryGood!!.setOnClickListener { v -> onClick(v) }
        btnGood = view.findViewById(R.id.btn_good)
        btnGood!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnGood!!.setOnClickListener { v -> onClick(v) }
        btnRegular = view.findViewById(R.id.btn_regular)
        btnRegular!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnRegular!!.setOnClickListener { v -> onClick(v) }
        btnBad = view.findViewById(R.id.btn_bad)
        btnBad!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
        btnBad!!.setOnClickListener { v -> onClick(v) }
        btnImagen = view.findViewById(R.id.img_btn)
        btnImagen!!.setBackgroundColor(Color.WHITE)
        btnImagen!!.setImageResource(R.mipmap.agregar_foto)
        btnImagen!!.setOnClickListener { v -> onClick(v) }
        etComments = view.findViewById(R.id.et_comments)

        when(viewModel.getTableCount(lotIdRoot)){
            1 -> btnTable1!!.visibility = View.VISIBLE
            2 -> {
                btnTable1!!.visibility = View.VISIBLE
                btnTable2!!.visibility = View.VISIBLE
            }
            3 -> {
                btnTable1!!.visibility = View.VISIBLE
                btnTable2!!.visibility = View.VISIBLE
                btnTable3!!.visibility = View.VISIBLE
            }
            4 -> {
                btnTable1!!.visibility = View.VISIBLE
                btnTable2!!.visibility = View.VISIBLE
                btnTable3!!.visibility = View.VISIBLE
                btnTable4!!.visibility = View.VISIBLE
                btnTable5!!.visibility = View.VISIBLE
                btnTable5!!.setText("")
                btnTable5!!.isEnable(false)
                btnTable6!!.visibility = View.VISIBLE
                btnTable6!!.setText("")
                btnTable6!!.isEnable(false)
            }
            5 -> {
                btnTable1!!.visibility = View.VISIBLE
                btnTable2!!.visibility = View.VISIBLE
                btnTable3!!.visibility = View.VISIBLE
                btnTable4!!.visibility = View.VISIBLE
                btnTable5!!.visibility = View.VISIBLE
                btnTable5!!.text = "Tabla 5"
                btnTable5!!.isEnable(true)
                btnTable6!!.visibility = View.VISIBLE
                btnTable6!!.text = ""
                btnTable6!!.isEnable(false)
            }
            6 -> {
                btnTable1!!.visibility = View.VISIBLE
                btnTable2!!.visibility = View.VISIBLE
                btnTable3!!.visibility = View.VISIBLE
                btnTable4!!.visibility = View.VISIBLE
                btnTable5!!.visibility = View.VISIBLE
                btnTable5!!.visibility = View.VISIBLE
                btnTable5!!.text = "Tabla 5"
                btnTable5!!.isEnable(true)
                btnTable6!!.visibility = View.VISIBLE
                btnTable6!!.text = "Tabla 6"
                btnTable6!!.isEnable(true)
            }
        }
    }

    fun onClick(view: View) {
        if(ifConected()) {
            viewModel.getRootQualityData(lotIdRoot)
            when (view.id) {
                R.id.btn_table1 -> {
                    if(table != 1) {
                        table = 1
                        cleanData()
                        loadRootTableData()
                    }
                    btnTable1!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    btnTable2!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable3!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable4!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable5!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable6!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                }
                R.id.btn_table2 -> {
                    if(table != 2) {
                        table = 2
                        cleanData()
                        loadRootTableData()
                    }
                    btnTable1!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable2!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    btnTable3!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable4!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable5!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable6!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                }
                R.id.btn_table3 -> {
                    if(table != 3) {
                        table = 3
                        cleanData()
                        loadRootTableData()
                    }
                    btnTable1!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable2!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable3!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    btnTable4!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable5!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable6!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                }
                R.id.btn_table4 -> {
                    if(table != 4) {
                        table = 4
                        cleanData()
                        loadRootTableData()
                    }
                    btnTable1!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable2!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable3!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable4!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    btnTable5!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable6!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                }
                R.id.btn_table5 -> {
                    if(table != 5) {
                        table = 5
                        cleanData()
                        loadRootTableData()
                    }
                    btnTable1!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable2!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable3!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable4!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable5!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    btnTable6!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                }
                R.id.btn_table6 -> {
                    if(table != 6) {
                        table = 6
                        cleanData()
                        loadRootTableData()
                    }
                    btnTable1!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable2!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable3!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable4!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable5!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnTable6!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                }
                R.id.btn_very_good -> {
                    quality = 4
                    btnVeryGood!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    btnGood!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnRegular!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnBad!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                }
                R.id.btn_good -> {
                    quality = 3
                    btnVeryGood!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnGood!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    btnRegular!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnBad!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                }
                R.id.btn_regular -> {
                    quality = 2
                    btnVeryGood!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnGood!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnRegular!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    btnBad!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                }
                R.id.btn_bad -> {
                    quality = 1
                    btnVeryGood!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnGood!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnRegular!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                    btnBad!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                }
                else -> {
                    if (mPermission.cameraPermission(this.context!!)) {
                        initCamera()
                    } else {
                        mPermission.verifyHavePermissions(this.activity!!)
                    }
                }
            }
        }
    }

    private fun initCamera() {
        /*try {
            val imageFile = this.context?.let { picturesHelper.createImageFile(it) }
            path = imageFile?.absolutePath.toString()
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (callCameraIntent.resolveActivity(activity?.packageManager!!) != null) {
                val authorities = activity?.packageName + ".fileprovider"
                val imageUri = this.context?.let { imageFile?.let { it1 -> FileProvider.getUriForFile(it, authorities, it1) } }
                callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
            }
        } catch (e: IOException) {
            val toast = Toast.makeText(this.context, "Could not create file!", Toast.LENGTH_LONG)
            toast.show()
        }*/
        val iny = Intent(this.context, CameraActivity::class.java)
        this.startActivityForResult(iny, CAMERA_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                            data!!.data)
                    btnImagen!!.setImageBitmap(imagenBitmap)
                    image1 = compressImage(imagenBitmap)
                }
            }
        }
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(this.context!!)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(this.context!!, bitmap), image, 400f, 400f)
        return image
    }

    fun validate() : Boolean{
        when {
            !newRoot -> showSnackBar("La informacion ya ha sido enviada.")
            quality == 0 -> showSnackBar("Seleccionar una calidad.")
            (etComments!!.text.isEmpty() || etComments!!.text == "") -> showSnackBar("Debe de agregarse un comentario.")
            image1 == null -> showSnackBar("Debe de agregarse una foto de evidencia.")
            else -> return true
        }
        return false
    }

    fun showSnackBar(msj: String){
        val snackbar = Snackbar.make(mview, msj, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    fun uploadImage(){
        val builder = android.app.AlertDialog.Builder(activity!!)
        builder.setTitle("Calida de raiz - Tabla " + table)
        builder.setMessage("¿Desea guardar la información?")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            newRoot = false
            setEnable(newRoot)
            viewModel.uploadImage(image1!!, "issue")
            dialog.dismiss()
        }
        builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
        builder.show()
    }

    fun cleanData(){
        quality = 0
        btnVeryGood!!.setBackgroundColor(Color.LTGRAY)
        btnGood!!.setBackgroundColor(Color.LTGRAY)
        btnRegular!!.setBackgroundColor(Color.LTGRAY)
        btnBad!!.setBackgroundColor(Color.LTGRAY)
        btnImagen!!.setBackgroundColor(Color.WHITE)
        btnImagen!!.setImageResource(R.mipmap.agregar_foto)
        image1 = null
        etComments!!.text = ""
    }

    fun loadRootTableData(){
        rootWeek.forEach {
            if(it.table == table){
                quality = it.quality!!
                when(quality){
                    4 -> btnVeryGood!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    3 -> btnGood!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    2 -> btnRegular!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                    else -> btnBad!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                }
                etComments!!.text = it.comment
                btnImagen!!.setImageUrl("https://backend-filestorage.s3-us-west-1.amazonaws.com/" + it.image)
                newRoot = false
                setEnable(newRoot)
                return
            } else{
                newRoot = true
                setEnable(newRoot)
            }
        }
    }

    private fun setEnable(enable: Boolean){
        btnVeryGood!!.isEnable(enable)
        btnGood!!.isEnable(enable)
        btnRegular!!.isEnable(enable)
        btnBad!!.isEnable(enable)
        etComments!!.isEnable(enable)
        btnImagen!!.isEnable(enable)
    }

    fun ifConected(): Boolean{
        return if(activity!!.hasInternet())
            true
        else {
            Toast.makeText( activity!!, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

}
