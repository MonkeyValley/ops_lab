package com.amplemind.vivasmart.vo_features.services.firebase

import android.os.Build
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import androidx.annotation.RequiresApi
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.ACTIVITY_CONTEXT
import com.amplemind.vivasmart.features.notification.viewModel.InboxViewModel
import com.amplemind.vivasmart.features.sync_forced.viewModel.SyncForcedViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UpdateNotificationModel
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.alert_message_dialog.view.*
import javax.inject.Inject


class NotificationService : FirebaseMessagingService() {

    companion object {
        private val TAG = NotificationService::class.java.simpleName
    }

    @Inject
    lateinit var mGson: Gson

    @Inject
    lateinit var viewModel: InboxViewModel

    @Inject
    lateinit var forcedViewModel: SyncForcedViewModel

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.d(TAG, "remoteMessage: ${remoteMessage.from}")

        if (remoteMessage.data.isNotEmpty()) {

            val data = remoteMessage.data.values.firstOrNull()

            if (data != null && ACTIVITY_CONTEXT != null) {
                try {
                    val notification = mGson.fromJson(data, UpdateNotificationModel::class.java)
                    viewModel.setContext(ACTIVITY_CONTEXT)
                    when (notification.type) {
                        "message" -> {
                            viewModel.getMessage(notification.msgDate!!.messageId ?: 0)
                        }
                        "collaborator" -> {
                            /*if(notification.isDelete){

                        } else*/
                            viewModel.UpdateCollaborator(notification.uuid!!, notification.isWorking)
                        }
                        "activity" -> {
                            if (notification.isDelete) {
                                forcedViewModel.deleteActivity(notification.uuid)
                            } else forcedViewModel.getActivitiesBackground()
                        }
                        "lot" -> {
                            forcedViewModel.getStageBackground()
                        }
                        "dailypay" -> {
                            forcedViewModel.getDailyPay()
                        }
                        else -> Log.d(TAG, "Message data payload: ${remoteMessage.data}")
                    }
                } catch (e: Exception){
                    Log.e("NotificationService", e.toString())
                }
            }
        }

        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")

        }
    }

}
