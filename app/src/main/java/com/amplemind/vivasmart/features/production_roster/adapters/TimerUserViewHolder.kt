package com.amplemind.vivasmart.features.production_roster.adapters


import android.app.Activity
import android.graphics.drawable.Animatable
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.core.content.ContextCompat
import android.util.Log.e
import android.view.View
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isTablet
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.features.production_roster.dialogs.PauseTimerDialog
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import de.hdodenhof.circleimageview.CircleImageView
import io.reactivex.subjects.BehaviorSubject

class TimerUserViewHolder(private val view: View, val supportFragmentManager: FragmentManager) : ChildViewHolder(view) {


    val clickProfileImage = BehaviorSubject.create<TimerUserViewModel>()
    var viewForeground: ConstraintLayout? = null
    var viewBackground: RelativeLayout? = null
    var viewFinish: RelativeLayout? = null

    private val subscriptions = AndroidDisposable()

    var registerId : Int? = null
    var packing_id : Int? = null
    var idLineCollaboratorOnline : Boolean = false

    fun bind(viewModel: TimerUserViewModel, position: Int, name_unit: String?, cost: Double?) {
        //Tag view to use draw row and show delete and finish options
        viewForeground = view.findViewById(R.id.viewForeground)
        viewBackground = view.findViewById(R.id.view_background)
        viewFinish = view.findViewById(R.id.view_finish)

        viewModel.name_unit = name_unit
        viewModel.cost = cost

        registerId = viewModel.registerId
        packing_id = viewModel.packing_id
        idLineCollaboratorOnline = viewModel.isOnLine

        val play = view.findViewById<ImageButton>(R.id.start_chronometer)
        val iv_clock = view.findViewById<ImageView>(R.id.iv_clock)
        val play_time = view.findViewById<LinearLayout>(R.id.ln_time)
        val tv_time_chronometer = view.findViewById<TextView>(R.id.tv_time_chronometer)
        val name = view.findViewById<TextView>(R.id.tv_user_name)
        val tv_unit = view.findViewById<TextView>(R.id.tv_unit)
        val userImage = view.findViewById<ImageView>(R.id.profileImage)

        tv_unit.text = viewModel.units.toString()
        if (name_unit == "isSection"){
            tv_unit.visibility = View.INVISIBLE
        }

        view.findViewById<CircleImageView>(R.id.profileImage).setOnClickListener {
            if (viewModel.canAssignUnits()) {
                viewModel.mediatorNavigation?.sendClickColleague(viewModel, position)
            }
        }

        name.text = viewModel.name
        tv_time_chronometer.text = viewModel.getElapsedTime()

        Glide.with(view.context)
                .load(viewModel.getUserImage())
                .apply(RequestOptions()
                        .placeholder(R.drawable.ic_user))
                .into(userImage)

        viewModel.getClockAnimationObservable().subscribe { isAnimating ->
            if (isAnimating) {
                startAnimation(viewModel)
            } else {
                stopAnimation(viewModel)
            }
        }.addTo(subscriptions)

        viewModel.getClockObservable().subscribe { time ->
            (view.context as Activity).runOnUiThread {
                tv_time_chronometer.text = time
            }
        }.addTo(subscriptions)


        //Start, stop timer listeners
        play_time.setOnClickListener {
            if (play.visibility == View.VISIBLE) {
                viewModel.startTimer()
                viewModel.mediatorNavigation?.sendStartTimer(viewModel, position)
                (iv_clock.drawable as Animatable).start()
                play.visibility = View.GONE
                iv_clock.visibility = View.VISIBLE
            } else {
                val dialog = PauseTimerDialog().newInstance()
                dialog.show(supportFragmentManager, PauseTimerDialog.TAG)
                dialog.setListener(object : PauseTimerDialog.OnPauseTimerListenner {
                    override fun pauseTimer(type: Int) {
                        dialog.dismiss()
                        viewModel.stopTimer()
                        viewModel.mediatorNavigation?.pauseTimer(viewModel, position)
                        (iv_clock.drawable as Animatable).stop()
                        play.visibility = View.VISIBLE
                        iv_clock.visibility = View.GONE
                    }
                })
            }
        }

        if (viewModel.showErrorMissingUnits) {
            tv_time_chronometer.setTextColor(ContextCompat.getColor(view.context, R.color.redAlert))
            tv_unit.setTextColor(ContextCompat.getColor(view.context, R.color.redAlert))
            iv_clock.setImageResource(R.drawable.ic_clock_red)
            (iv_clock.drawable as Animatable).start()
            play.setImageResource(R.drawable.ic_start_red)
            if (viewModel.timeIsRunning) {
                play.visibility = View.GONE
                iv_clock.visibility = View.VISIBLE
            } else {
                play.visibility = View.VISIBLE
                iv_clock.visibility = View.GONE
            }
        } else {
            play.setImageResource(R.drawable.ic_start)
            if (viewModel.timeIsRunning) {
                startAnimation(viewModel)
            } else {
                stopAnimation(viewModel)
            }
        }

    }

    private fun startAnimation(viewModel: TimerUserViewModel) {
        val play = view.findViewById<ImageButton>(R.id.start_chronometer)
        val tv_time_chronometer = view.findViewById<TextView>(R.id.tv_time_chronometer)
        val tv_unit = view.findViewById<TextView>(R.id.tv_unit)
        val iv_clock = view.findViewById<ImageView>(R.id.iv_clock)
        play.visibility = View.GONE
        iv_clock.visibility = View.VISIBLE
        (iv_clock.drawable as Animatable).start()
        tv_time_chronometer.setTextColor(ContextCompat.getColor(view.context, R.color.blue))
        tv_unit.setTextColor(ContextCompat.getColor(view.context, R.color.blue))
        iv_clock.setImageResource(R.drawable.ic_clock)
        (iv_clock.drawable as Animatable).start()
    }

    private fun stopAnimation(viewModel: TimerUserViewModel) {
        val play = view.findViewById<ImageButton>(R.id.start_chronometer)
        val tv_time_chronometer = view.findViewById<TextView>(R.id.tv_time_chronometer)
        val tv_unit = view.findViewById<TextView>(R.id.tv_unit)
        val iv_clock = view.findViewById<ImageView>(R.id.iv_clock)

        play.visibility = View.VISIBLE
        iv_clock.visibility = View.GONE
        play.setImageResource(R.drawable.ic_start)
        tv_time_chronometer.setTextColor(ContextCompat.getColor(view.context, R.color.dark_gray))
        tv_unit.setTextColor(ContextCompat.getColor(view.context, R.color.dark_gray))
        tv_time_chronometer.text = viewModel.getElapsedTime()
    }

    fun onRecycle() {
        subscriptions.dispose()
    }

}