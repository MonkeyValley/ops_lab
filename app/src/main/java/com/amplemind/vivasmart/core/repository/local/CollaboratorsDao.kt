package com.amplemind.vivasmart.core.repository.local

import androidx.room.*
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.model.CollaboratorsListReponse
import com.amplemind.vivasmart.core.repository.model.CollaboratosRecent
import io.reactivex.Single

@Dao
interface CollaboratorsDao {

    /*
     * Collaborators
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollaborator(recent: CollaboratorsListReponse): Long

    @Update
    fun updateCollaborator(vararg users: CollaboratorsListReponse)

    @Query("DELETE FROM CollaboratorsListReponse WHERE id = :id")
    fun deleteCollaborator(id: Long)

    @Query("SELECT * FROM DeleteCollaboratorsOffline")
    fun getCollaboratorsByDeleteSync() : List<DeleteCollaboratorsOffline>

    @Query("DELETE FROM DeleteCollaboratorsOffline WHERE idRegister = :idRegister")
    fun deleteByDeleteSync(idRegister: Int)

    @Query("SELECT * FROM CollaboratorsListReponse WHERE activityCodeId = :activityCode AND isDone = 0")
    fun getCollaborators(activityCode: Int) : Single<List<CollaboratorsListReponse>>

    @Query("SELECT COUNT(*) > 0 FROM CollaboratorsListReponse WHERE activityCodeId = :activityCode AND isDone = 0")
    fun collaboratorsRunningInCode(activityCode: Int) : Boolean

    @Query("SELECT COUNT(*) > 0 FROM CollaboratorsListReponse WHERE activityId = :activityId AND stageId = :stageId AND isDone = 0")
    fun collaboratorsIsRunning(activityId: Int, stageId : Int) : Boolean

    @Query("SELECT COUNT(*) > 0 FROM CollaboratorsListReponse WHERE stageId = :stageId AND isDone = 0")
    fun collaboratorsIsRunningStage(stageId: Int) : Boolean



    @Query("SELECT * FROM CollaboratorsListReponse WHERE isDone = 0")
    fun getCollaboratorsList() : List<CollaboratorsListReponse>

    @Query("SELECT * FROM CollaboratorsListReponse WHERE id = :id")
    fun getCollaboratorById(id: Int) : CollaboratorsListReponse?

    @Query("SELECT * FROM CollaboratorsListReponse WHERE collaboratorId = :collaboratorId AND activityCodeId = :activityCodeId AND isDone = :isDone")
    fun getCollaboratorByIdAndCode(collaboratorId: Int, activityCodeId: Int, isDone: Boolean = false) : CollaboratorsListReponse?

    @Query("SELECT * FROM CollaboratorsListReponse")
    fun getCollaboratorByIdAndCodeTest() : List<CollaboratorsListReponse>

    @Query("SELECT * FROM CollaboratorsListReponse WHERE collaboratorId = :collaboratorId AND activityCodeId = :activityCodeId AND isDone = 1")
    fun getCollaboratorDone(collaboratorId: Int, activityCodeId: Int) : List<CollaboratorsListReponse>

    @Query("UPDATE CollaboratorsListReponse SET isPaused = :isPaused, localTime = :time, date = 0 WHERE ID = :collaboratorId")
    fun updateCollaboratorPause(collaboratorId: Int, time: Long, isPaused: Boolean = true)

    @Query("UPDATE CollaboratorsListReponse SET isPaused = :isPaused, date = :date WHERE ID = :collaboratorId")
    fun restartCollaboratorTime(collaboratorId: Int, date: Long, isPaused: Boolean = false)

    @Query("UPDATE CollaboratorsListReponse SET isDone = :isDone WHERE ID = :collaboratorId")
    fun finishCollaborator(collaboratorId: Int, isDone: Boolean = true)

    /*
     * Collaborators Ready to Send
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollaboratorReadyToReview(recent: DetailtRosterResponse)

    @Query("SELECT * FROM DetailtRosterResponse WHERE stageId = :stageId GROUP BY collaboratorId")
    fun getCollaboratorsReadyToReview(stageId: Int) : List<DetailtRosterResponse>


    @Query("SELECT * FROM DetailtRosterLinesResponse WHERE packingline_collaborator_id =:idLine GROUP BY collaborator_id")
    fun getCollaboratorsReadyToReviewInLines(idLine: Int) : List<DetailtRosterLinesResponse>


    @Query("SELECT * FROM DetailtRosterResponse WHERE deleted = 0")
    fun getCollaboratorsReadyToSync() : List<DetailtRosterResponse>

    @Query("SELECT * FROM DetailtRosterResponse WHERE collaboratorId = :collaboratorId AND stageId = :stageId AND deleted = 0")
    fun getCollaboratorsActivityLogs(collaboratorId: Int, stageId: Int) : List<DetailtRosterResponse>

    @Query("SELECT * FROM DetailtRosterResponse WHERE id = :id")
    fun getCollaboratorsActivityLogsById(id: Int) : List<DetailtRosterResponse>

    @Query("SELECT * FROM DetailtRosterLinesResponse WHERE collaborator_id = :collaboratorId AND packingline_collaborator_id=:idLine")
    fun getCollaboratorsActivityLogsInLines(collaboratorId: Int?, idLine: Int) : List<DetailtRosterLinesResponse>

    @Query("SELECT * FROM DetailtRosterLinesResponse WHERE id_collaborator_db = :collaboratorId")
    fun getReportSyncByCollaborator(collaboratorId: Int?) : List<DetailtRosterLinesResponse>

    @Query("SELECT * FROM DetailtRosterLinesResponse")
    fun getCollaboratorsActivityLogsInLinesAll() : List<DetailtRosterLinesResponse>

    @Query("SELECT * FROM DetailtRosterResponse WHERE id = :id")
    fun getCollaboratorsActivityLog(id: Int) : DetailtRosterResponse

    @Query("UPDATE DetailtRosterResponse SET deleted = 1, statusSync = 1 WHERE id = :id")
    fun deleteActivityLog(id: Int)

    @Query("UPDATE DetailtRosterResponse SET statusSync = 0 WHERE id = :id")
    fun updateActivityLogError(id: Int)

    @Query("UPDATE DetailtRosterResponse SET quantity = :quantity, time = :time WHERE id = :id")
    fun updateCollaboratorTimeQuantity(quantity: Double, time: Long, id: Int)

    @Query("UPDATE DetailtRosterLinesResponse SET quantity = :quantity, time = :time WHERE id_detail = :id")
    fun updateCollaboratorTimeQuantityLines(quantity: Double, time: Long, id: Int)

    @Query("UPDATE DetailtRosterLinesResponse SET is_training = :isTraining, sign = :sign WHERE id_detail = :id")
    fun signOfflineCollaboratorInLines(isTraining: Boolean, sign: String, id: Int)

    @Query("UPDATE DetailtRosterResponse SET isTraining = :isTraining, sign = :sign, is_done = 1 WHERE id = :id")
    fun signCollaborator(isTraining: Boolean, sign: String, id: Int)

    /*
     * Recent Collaborators
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecentCollaborator(recent: CollaboratosRecent)

    @Query("DELETE FROM CollaboratosRecent WHERE id NOT IN (SELECT id FROM CollaboratosRecent ORDER BY id DESC LIMIT 10)")
    fun deleteOldCollaborators()

    @Query("SELECT * FROM COLLABORATOSRECENT ORDER BY id DESC")
    fun getRecentCollaborators() : Single<List<CollaboratosRecent>>



}
