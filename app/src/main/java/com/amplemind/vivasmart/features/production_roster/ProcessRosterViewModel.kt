package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.vo_core.repository.ProcessRosterRepository
import javax.inject.Inject

class ProcessRosterViewModel @Inject constructor(
        private val repository: ProcessRosterRepository
) {

    lateinit var stageUuid: String
    var stageTypeId: Int = 0

    fun cleanUp() {
        repository.cleanUp()
    }

    fun getPendingLogs() =
            repository.getPendingLogs(stageUuid, stageTypeId)

}
