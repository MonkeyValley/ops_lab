package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class VarietyModel (

        @Index
        @SerializedName("id")           var id: Long = 0,

        @Index
        @SerializedName("crop_id")      var cropId: Long = 0,
        @SerializedName("graft_name")   var graftName: String? = null,
        @SerializedName("is_commercial")var isCommercial: Boolean = false,
        @SerializedName("is_graft")     var isGraft: Boolean = false,
        @SerializedName("supplier")     var supplier: String? = null,
        @SerializedName("name")         var name: String = "",
        @SerializedName("stage_id")     var stageId: Int? = 0,

        @PrimaryKey
        @SerializedName("uuid")         var uuid: String = getUUID(),

        @Index
        @SerializedName("version")      var version: Long? = null
) : RealmObject(), Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readLong(),
                parcel.readLong(),
                parcel.readString(),
                parcel.readByte() != 0.toByte(),
                parcel.readByte() != 0.toByte(),
                parcel.readString(),
                parcel.readString()!!,
                parcel.readInt(),
                parcel.readString()!!,
                parcel.readValue(Long::class.java.classLoader) as? Long)

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeLong(id)
                parcel.writeLong(cropId)
                parcel.writeString(graftName)
                parcel.writeByte(if (isCommercial) 1 else 0)
                parcel.writeByte(if (isGraft) 1 else 0)
                parcel.writeString(supplier)
                parcel.writeString(name)
                parcel.writeInt(stageId!!)
                parcel.writeString(uuid)
                parcel.writeValue(version)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<VarietyModel> {
                override fun createFromParcel(parcel: Parcel): VarietyModel {
                        return VarietyModel(parcel)
                }

                override fun newArray(size: Int): Array<VarietyModel?> {
                        return arrayOfNulls(size)
                }
        }
}