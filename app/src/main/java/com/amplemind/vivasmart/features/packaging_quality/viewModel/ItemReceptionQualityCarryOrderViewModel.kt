package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityCarryOrderModel

class ItemReceptionQualityCarryOrderViewModel (
        val carryOrder: ReceptionQualityCarryOrderModel
){

    val code: String
        get() = carryOrder.folio

    val lotName: String
        get() = carryOrder.lotName

    val tables: String
        get() = carryOrder.tables

    val qualityPercentage: String
        get() = if (carryOrder.quality > 0) "%.2f".format(carryOrder.quality) else "-"

}