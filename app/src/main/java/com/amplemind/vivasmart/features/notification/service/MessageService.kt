package com.amplemind.vivasmart.features.notification.service

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.Gson
import io.reactivex.Single
import io.realm.Realm
import io.realm.Sort
import org.json.JSONObject
import javax.inject.Inject

class MessageService @Inject constructor() {
    var context: Context? = null

    lateinit var preferences: UserAppPreferences

    fun getMessages(): Single<List<MessageModel>> =
            Realm.getDefaultInstance().use {
                Single.create<List<MessageModel>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->
                        val results = realm
                                .where(MessageModel::class.java)
                                .equalTo("type", "MESSAGE")
                                .sort("id", Sort.DESCENDING)
                                .findAll()
                        emitter.onSuccess(realm.copyFromRealm(results))
                    }
                }
            }

    fun updateMessageStatuss(messageId: Int?) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction {
                var result = it
                        .where(MessageModel::class.java)
                        .equalTo("id", messageId)
                        .findFirst()

                if (result != null) {
                    if (result.status == "new") {
                        result.status = "read"

                        val rootObject = JSONObject()
                        rootObject.put("status", result.status)

                        val prefer = context!!.getSharedPreferences(
                                context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

                        val url = preferences.selectedServerUrl
                        val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                        uploadObject.verb = "PUT"
                        uploadObject.url = url + "v2/message/" + result.id
                        uploadObject.contentType = "application/json"
                        uploadObject.body = rootObject.toString()
                        uploadObject.token = prefer.getString("token", "") ?: ""
                        uploadObject.order = 99

                        it.insertOrUpdate(uploadObject)
                        it.insertOrUpdate(result)
                    }
                }
            }
        }
    }

    fun getNotificationType(notificationTypeId: Int?): String =
            when(notificationTypeId){
                1 -> "Calidad produción control"
                2 -> "Calidad validación cosecha"
                3 -> "Calidad reciba"
                else -> "Sin asunto"
            }

    fun getLotName(lotId: Int?): String =
            Realm.getDefaultInstance().use {
                it.where(LotModel::class.java)
                        .equalTo("id", lotId)
                        .findFirst()
                        ?.name ?: ""
            }

    fun updateCollanboratorAndGetname(uuid: String, working: Boolean): String {
        Realm.getDefaultInstance().use {
            var name = ""
            it.executeTransaction { realm ->
                var collaborator = realm
                        .where(CollaboratorModel::class.java)
                        .equalTo("uuid", uuid)
                        .findFirst()

                if (collaborator != null) {
                    collaborator.isWorking = working
                    realm.insertOrUpdate(collaborator)
                    name =  collaborator.name
                }
            }
            return name
        }
    }

}