package com.amplemind.vivasmart

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.features.collaborators.AddCollaboratorsActivity
import com.amplemind.vivasmart.features.collaborators.AssignGroovesActivity

import com.amplemind.vivasmart.features.packaging_quality.PackagingQualityActivity
import com.amplemind.vivasmart.features.production_quality.ProductionQualityActivity
import com.amplemind.vivasmart.features.quality_control.QualityControlActivity

@SuppressLint("Registered")
class MenuActivitiesFragment : BaseFragment() {

    private var mView : View? = null

    fun newInstance() : MenuActivitiesFragment{
        return MenuActivitiesFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.content_menu_activities_fragment, container, false)

        mView!!.findViewById<Button>(R.id.quality_btn).setOnClickListener {
            startActivity(Intent(activity, PackagingQualityActivity::class.java))
        }
        mView!!.findViewById<Button>(R.id.production_btn).setOnClickListener {
            (activity as AppCompatActivity).startActivity(Intent(context, ProductionQualityActivity::class.java))
        }

        mView!!.findViewById<Button>(R.id.quality_control_btn).setOnClickListener {
            (activity as AppCompatActivity).startActivity(Intent(context, QualityControlActivity::class.java))
        }

        mView!!.findViewById<Button>(R.id.collaborators_btn).setOnClickListener {
            (activity as AppCompatActivity).startActivity(Intent(context, AddCollaboratorsActivity::class.java))
        }

        mView!!.findViewById<Button>(R.id.assing_grooves_btn).setOnClickListener {
            (activity as AppCompatActivity).startActivity(Intent(context, AssignGroovesActivity::class.java))
        }

        return mView
    }


}
