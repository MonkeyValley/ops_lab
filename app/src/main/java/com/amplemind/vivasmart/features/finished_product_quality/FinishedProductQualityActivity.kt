package com.amplemind.vivasmart.features.finished_product_quality

import android.os.Bundle
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.SharedPreferencesUtils
import com.amplemind.vivasmart.features.finished_product_quality.fragment.ProductQualityReviewDetailFragment
import com.amplemind.vivasmart.features.finished_product_quality.fragment.ProductQualityReviewFragment
import com.amplemind.vivasmart.features.finished_product_quality.viewModel.FinishedProductQualityViewModel
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject


class FinishedProductQualityActivity : BaseActivityWithFragment() {

    @Inject
    lateinit var viewModel: FinishedProductQualityViewModel

    @Inject
    lateinit var preferences: UserAppPreferences

    companion object {
        lateinit var sharedPref: SharedPreferencesUtils
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finished_product_quality)
        sharedPref = SharedPreferencesUtils(this)
        val newReport =  intent.getBooleanExtra("newReport", true)
        var idReport = ""
        var lotName = ""
        var packingName = ""

         if(intent.getStringExtra("idReport") != null) {
             idReport = intent.getStringExtra("idReport")
             lotName = intent.getStringExtra("lotName")
             packingName = intent.getStringExtra("packingName")
         }

        initToolbar()
        callFragmentMenuActivities(newReport, idReport, lotName, packingName)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.clean_finished_report_title)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        finish()
    }

    protected fun getFragmentCount(): Int {
        return supportFragmentManager.backStackEntryCount
    }

    private fun callFragmentMenuActivities(newReport : Boolean, idReport: String, lotName: String, packingName : String ) {
        Log.d("newReport", newReport.toString())
        if(newReport) {
            val support = supportFragmentManager
            val commit = support.beginTransaction()
                    .replace(R.id.container_payroll, ProductQualityReviewFragment().newInstance("ProductQualityReviewFragment"))
                    .addToBackStack("ProductQualityReviewFragment")
                    .commit()
        }
        else{
            val support = supportFragmentManager
            val commit = support?.beginTransaction()
                    ?.replace(R.id.container_payroll, ProductQualityReviewDetailFragment().newInstance("ProductQualityReviewDetailFragment", false, idReport, lotName, packingName ))
                    ?.addToBackStack("ProductQualityReviewDetailFragment")
                    ?.commit()
        }
    }

}

