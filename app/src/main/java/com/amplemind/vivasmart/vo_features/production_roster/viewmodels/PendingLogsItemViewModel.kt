package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import com.amplemind.vivasmart.vo_core.repository.models.realm.PendingLogsModel
import com.amplemind.vivasmart.vo_core.utils.formatPayment
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import io.reactivex.disposables.Disposable

class PendingLogsItemViewModel (item: PendingLogsModel): BaseAdapter.ViewModel<PendingLogsModel>(item) {

    private var mPaymentDisposable: Disposable? = null

    val name = item.collaborator.name
    var total = formatPayment(item.payment)//"${pendingLogs.total - pendingLogs.unsigned}/${pendingLogs.total}"
    val code = item.collaborator.employeeCode
    //val showStatus = ObservableInt(View.VISIBLE)
    val score: Int = 0

    override fun bind(item: PendingLogsModel) {
    }

    fun clear() {
        mPaymentDisposable?.dispose()
    }


}