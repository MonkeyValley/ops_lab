package com.amplemind.vivasmart.features.packaging_quality.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemReceptionQualityCarryOrderBinding
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualityCarryOrderListFragment
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ItemReceptionQualityCarryOrderViewModel
import com.jakewharton.rxbinding2.view.RxView
import java.util.concurrent.TimeUnit

class ReceptionQualityCarryOrderAdapter(
        private val mEventBus: EventBus
) : RecyclerView.Adapter<ReceptionQualityCarryOrderAdapter.ReceptionQualityCarryOrderViewHolder>() {

    private var mItems = listOf<ItemReceptionQualityCarryOrderViewModel>()

    override fun onViewRecycled(holder: ReceptionQualityCarryOrderViewHolder) {
        super.onViewRecycled(holder)
        holder.cleanUp()
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ReceptionQualityCarryOrderViewHolder {
        val binding = ItemReceptionQualityCarryOrderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReceptionQualityCarryOrderViewHolder(binding)
    }

    override fun getItemCount(): Int = mItems.size

    override fun onBindViewHolder(viewHolder: ReceptionQualityCarryOrderViewHolder, position: Int) {
        viewHolder.bind(mItems[position])
    }

    fun setItems(items: List<ItemReceptionQualityCarryOrderViewModel>) {
        mItems = items
        notifyDataSetChanged()
    }

    inner class ReceptionQualityCarryOrderViewHolder(
            private val binding: ItemReceptionQualityCarryOrderBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        private val subscriptions = AndroidDisposable()

        fun cleanUp() {
            subscriptions.dispose()
        }

        fun bind(viewModel: ItemReceptionQualityCarryOrderViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()

            RxView.clicks(binding.root)
                    .throttleFirst(2, TimeUnit.SECONDS)
                    .subscribe {
                        mEventBus.send(ReceptionQualityCarryOrderListFragment.OnCarryOrderClickedEvent(viewModel.carryOrder))
                    }
                    .addTo(subscriptions)
        }
    }

}
