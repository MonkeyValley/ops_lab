package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.google.gson.annotations.SerializedName

data class QualityCarryOrderDetailResponse(
        @SerializedName("data") val data: List<QualityCarryOrderDetailResult>,
        @SerializedName("total_count") val count: Int
)


data class QualityCarryOrderDetailResult(
        @SerializedName("carry_order") val carry_order: CarryOrderModel,
        @SerializedName("carry_order_id") val carry_order_id: Int,
        @SerializedName("defect_condition") val defect_condition: Int,
        @SerializedName("export") val export: Int,
        @SerializedName("has_evidence") val has_evidence: Boolean,
        @SerializedName("id") val id: Int,
        @SerializedName("image1") val image1: String?,
        @SerializedName("image2") val image2: String?,
        @SerializedName("image3") val image3: String?,
        @SerializedName("image4") val image4: String?,
        @SerializedName("evaluation") val evaluation: Boolean?,
        @SerializedName("comment") val comment: String?,
        @SerializedName("is_active") val is_active: Boolean,
        @SerializedName("issues") val issues: List<IssuesModel>?,
        @SerializedName("permanent_defect") val permanent_defect: Int,
        @SerializedName("sample_size") val sample_size: Int,
        @SerializedName("unit_id") val unit_id: Int,
        @SerializedName("user_id") val user_id: Int
)


data class IssuesModel(
        @SerializedName("count") val count: Int,
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("percent_sample") val percent_sample: Double
)