package com.amplemind.vivasmart.features.crop_validation.viewmodel

import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.CropValidationRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.crop_validation.models.CarryOrderHarvest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class CropValidationActivityViewModel @Inject constructor(private val repository: CropValidationRepository, private val apiErrors: HandleApiErrors) : BaseViewModel() {
    private val list = mutableListOf<ItemCarryOrderHarvestViewModel>()
    private val requestSuccess = BehaviorSubject.create<List<ItemCarryOrderHarvestViewModel>>()

    fun getCarryOrderHarvest(day: String): Disposable {
        return repository.getCarryOrderHarvest(day)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate {
                    progressStatus.onNext(false)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelListToViewModel)
                .subscribe({
                    list.clear()
                    list.addAll(it)
                    requestSuccess.onNext(list)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelListToViewModel(result: List<CarryOrderHarvest>): List<ItemCarryOrderHarvestViewModel> {
        val viewModels = mutableListOf<ItemCarryOrderHarvestViewModel>()
        result.forEach { obj ->
            Log.e("crop_id: ", obj.crop_id.toString())
            viewModels.add(ItemCarryOrderHarvestViewModel(obj))
        }
        viewModels.sortBy { it.carry_order_id }
        viewModels.reverse()
        return viewModels
    }

    fun onSuccessRequest(): BehaviorSubject<List<ItemCarryOrderHarvestViewModel>> {
        return requestSuccess
    }

}