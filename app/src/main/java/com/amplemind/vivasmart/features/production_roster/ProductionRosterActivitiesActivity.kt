package com.amplemind.vivasmart.features.production_roster

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.haulage.HaulageActivity
import com.amplemind.vivasmart.features.lots.fragment.SelectionOfLotsFragment
import com.amplemind.vivasmart.features.planning.PlanningActivity
import com.amplemind.vivasmart.features.planning.viewmodels.PlanningViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.getbase.floatingactionbutton.FloatingActionsMenu
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.content_payroll_activity.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject

@SuppressLint("Registered")
open class ProductionRosterActivitiesActivity : BaseActivityWithFragment(), FragmentManager.OnBackStackChangedListener {

    companion object {
        private val TAG = ProductionRosterActivitiesActivity::class.java
    }

    @Inject
    lateinit var viewModel: PayrollViewModel

    @Inject
    lateinit var preferences: UserAppPreferences

    @Inject
    lateinit var mEventBus: EventBus

    private var mDisposable: Disposable? = null

    private var mStageId: Long = 0
    private var lotName = ""
    private lateinit var mStageUuid: String
    private lateinit var productionStageName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.content_payroll_activity)
        initToolbar()

        Log.e("----------","ProductionRosterActivitiesActivity")
        setUpCallbacks()
        callFragmentMenuActivitiesPayroll()



        btn_planning.addThrottle().subscribe {
            val activityIntent = Intent(this, PlanningActivity::class.java)
            activityIntent.putExtra("flow", PlanningViewModel.PlanningFlow.PRODUCTION.type)
            activityIntent.putExtra("id", mStageId)
            startActivity(activityIntent)
            fab_menu.collapse()
        }.addTo(subscriptions)

        fab_orden_haulage.addThrottle().subscribe {
            val intent = Intent(this, HaulageActivity::class.java).apply {
                putExtra(HaulageActivity.PARAM_STAGE_UUID, mStageUuid)
                putExtra(HaulageActivity.PRAM_STAGE_ID, mStageId.toString())
                putExtra(HaulageActivity.PARAM_LOT_NAME, lotName)
            }
            startActivity(intent)
            fab_menu.collapse()
        }.addTo(subscriptions)

        btn_quantity_report.addThrottle().subscribe {
            startActivity(Intent(this, QualifiedCarryOrderActivity::class.java))
            fab_menu.collapse()
        }.addTo(subscriptions)

        btn_close_day.addThrottle().subscribe {
            val intent = Intent( this, DailyReportActivity::class.java).apply {
                putExtra(DailyReportActivity.PARAM_STAGE_UUID, mStageUuid)
                putExtra("StageName", productionStageName)
            }
            startActivity(intent)
            fab_menu.collapse()
        }.addTo(subscriptions)

        fab_menu.setOnFloatingActionsMenuUpdateListener(object: FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
            override fun onMenuCollapsed() {
                root_content_payroll.visibility = View.GONE
            }

            override fun onMenuExpanded() {
                root_content_payroll.visibility = View.VISIBLE
            }

        })
    }

    private fun setUpCallbacks() {
        mEventBus.observe(this::onSetToolbarTitle).addTo(subscriptions)
        mEventBus.observe<ReportRosterActivity.CloseLotEvent>()
                .filter {event ->
                    event.stageUuid == mStageUuid
                }
                .subscribe(this::onStageCloseEvent)
                .addTo(subscriptions)

    }
    
    private fun onSetToolbarTitle(event: BaseFragment.SetToolbarTitleEvent) {
        if (event.context == this) {
            event.title?.apply {
                supportActionBar?.title = event.title
                lotName = event.title
            }
            event.subtitle?.apply {
                supportActionBar?.subtitle = event.subtitle
            }
        }
    }

    private fun onStageSelected(event: SelectionOfLotsFragment.StageSelectedEvent) {
        mStageId = event.stage.id.toLong()
        mStageUuid = event.stage.uuid
        productionStageName = event.stageType
        callChooseActivityFragment(event.stage)
        if(productionStageName != "Labores Culturales"){
            btn_planning.visibility = View.GONE
            fab_orden_haulage.visibility = View.GONE
        }else{
            btn_planning.visibility = View.VISIBLE
            fab_orden_haulage.visibility = View.VISIBLE
        }
    }

    private fun onActivitySelected(event: ChooseActivityFragment.ActivitySelectedEvent) {
        callTimerActivity(event.stageUuid, event.activityUuid)
    }

    private fun onStageCloseEvent(event: ReportRosterActivity.CloseLotEvent) {
    }


    private fun callFragmentMenuActivitiesPayroll() {
        mDisposable?.dispose()
        val support = supportFragmentManager
        support.addOnBackStackChangedListener(this)
        support.beginTransaction()
                .replace(R.id.container_payroll, MenuActivitiesPayrollFragment().newInstance(intent.getStringExtra("Title")))
                .commit()
    }

    private fun agreePlanningTutorial() {
        preferences.agreedPlanningTutorial = true
        ll_tutorial.visibility = View.GONE
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.subtitle = ""
    }

    private fun callTimerActivity(stageUuid: String, activityUuid: String) {
        startActivity(
                Intent(this, TimerActivity::class.java).apply {
                    putExtra(TimerActivity.PARAM_STAGE_UUID, stageUuid)
                    putExtra(TimerActivity.PARAM_ACTIVITY_UUID, activityUuid)
                }
        )
    }

    private fun callChooseActivityFragment(stage: StageModel) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
            .add(R.id.container_payroll, ChooseActivityFragment.newInstance(stage.uuid,
                    stage.cropId.toLong(), stage, productionStageName))
            .addToBackStack(ChooseActivityFragment.TAG)
            .commit()
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            val fm = supportFragmentManager
            if (fm.backStackEntryCount > 0) {
                fm.popBackStack()
            } else {
                finish()
                overridePendingTransition(R.anim.enter_right, R.anim.exit_right)
            }
            return false
        }
        return false
    }

    override fun onBackStackChanged() {
        val fragment = this.supportFragmentManager.findFragmentById(R.id.container_payroll)
        fab_menu.collapse()

        if (fragment is SelectionOfLotsFragment) {
            mDisposable?.dispose()
            mDisposable = mEventBus.observe(this::onStageSelected).addTo(subscriptions)
        }

        if (fragment is ChooseActivityFragment) {
            mDisposable?.dispose()
            mDisposable = mEventBus.observe(this::onActivitySelected).addTo(subscriptions)
            fab_menu.visibility = View.VISIBLE
        } else {
            fab_menu.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val fragment = this.supportFragmentManager.findFragmentById(R.id.container_payroll)
        if (fragment is SelectionOfLotsFragment) {
            agreePlanningTutorial()
        }
    }



}
