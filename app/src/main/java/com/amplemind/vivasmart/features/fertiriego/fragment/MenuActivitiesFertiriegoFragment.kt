package com.amplemind.vivasmart.features.fertiriego.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.fertiriego.FertiriegoActivitiesActivity
import com.amplemind.vivasmart.features.fertiriego.FertirriegoLotActivity
import com.amplemind.vivasmart.features.fertiriego.viewModel.MenuFertiriegoActivitiesViewModel
import com.amplemind.vivasmart.features.phenology.adapters.ActivitiesPhenologyAdapter
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModel
import kotlinx.android.synthetic.main.content_payroll_activities_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

@SuppressLint("Registered")
class MenuActivitiesFertiriegoFragment : BaseFragment() {

    @Inject
    lateinit var viewModelMenu: MenuFertiriegoActivitiesViewModel

    @Inject
    lateinit var prefer: UserAppPreferences

    private var section: String? = null

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    fun newInstance(title: String): MenuActivitiesFertiriegoFragment {
        val args = Bundle()
        args.putString("Title", title)

        val fragment = MenuActivitiesFertiriegoFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_payroll_activities_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lbl_track_title.text = "/ Fertirriego /"

        initRecycler()
        viewModelMenu.setupData(arguments!!)
        viewModelMenu.getToolbarTitle().subscribe {
            section = it
            (activity as FertiriegoActivitiesActivity).supportActionBar!!.title = it
            viewModelMenu.loadMenuActivities().addTo(subscriptions)
            viewModelMenu.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)

        }.addTo(subscriptions)
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_menu_activities_payroll.adapter as ActivitiesPhenologyAdapter).addElements(activities)
        (rv_menu_activities_payroll.adapter as ActivitiesPhenologyAdapter).onClickItem().subscribe(this::clickActivityPhenology).addTo(subscriptions)
    }

    private fun clickActivityPhenology(item: ItemActivitiesPayrollViewModel) {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now
        val intent = Intent(context, FertirriegoLotActivity::class.java)
        when(item.id){
            "0" -> intent.apply { putExtra(FertirriegoLotActivity.TAG_NAME, "Monitoreo") }
            "1" -> intent.apply { putExtra(FertirriegoLotActivity.TAG_NAME, "CE - pH") }
            else -> intent.apply { putExtra(FertirriegoLotActivity.TAG_NAME, "Reporte") }
        }
        startActivity(intent)
    }

    private fun initRecycler() {
        rv_menu_activities_payroll.hasFixedSize()
        rv_menu_activities_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_activities_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_activities_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_activities_payroll.adapter = ActivitiesPhenologyAdapter()
    }

    companion object {
        val TAG = MenuActivitiesFertiriegoFragment::class.java.simpleName
    }

}