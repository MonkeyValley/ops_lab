package com.amplemind.vivasmart.features.fertiriego.fragment


import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.features.fertiriego.adapter.CustomeSpinnerAdapter
import com.amplemind.vivasmart.features.fertiriego.viewModel.MonitoringViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveFertirriego
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveForTable
import com.google.gson.Gson
import javax.inject.Inject
import kotlin.collections.ArrayList


class MonitoringFragment : BaseFragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    @Inject
    lateinit var viewModel: MonitoringViewModel

    lateinit var fragView: View

    var et6Am: TextView? = null
    var et12Am: TextView? = null
    var et18Am: TextView? = null
    var et24Am: TextView? = null
    var spinnerBarrenaAm: Spinner? = null

    var et6Pm: TextView? = null
    var et12Pm: TextView? = null
    var et18Pm: TextView? = null
    var et24Pm: TextView? = null
    var backgroundSpinner:LinearLayout? = null
    var spinnerBarrenaPm: Spinner? = null
    var spinnerValve: Spinner? = null
    var btnMonitoring: Button? = null
    var arrayValidateSpinner = ArrayList<ValveFertirriego>()

    var itemSelected = -1
    val valveList = ArrayList<MonitoringViewModel.ValveItem>()
    var counterValvles = 1
    var type = 1

    fun newInstance(date: String, lotId: Int, table: String): MonitoringFragment {
        val args = Bundle()
        args.putString("DATE", date)
        args.putInt("LOT_ID", lotId)
        args.putString("TABLE", table)

        val fragment = MonitoringFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragView = inflater.inflate(R.layout.fragment_monitoring, container, false)

        viewModel.setServiceContect(activity!!)
        viewModel.fragment = this

        setUpUiAndArguments(fragView!!)
        addSubscribers()
        viewModel.getValves()
        initSpinner(6)

        return fragView
    }

    fun setUpUiAndArguments(view: View?) {
        viewModel.table = arguments!!.getString("TABLE") ?: ""
        viewModel.lotId = arguments!!.getInt("LOT_ID") ?: 0

        val validate: Boolean = viewModel.validateDate()

        spinnerValve = view!!.findViewById(R.id.spinner_valve)
        spinnerValve!!.onItemSelectedListener = this

        et6Am = view!!.findViewById(R.id.eT_6_am)
        et12Am = view!!.findViewById(R.id.eT_12_am)
        et18Am = view!!.findViewById(R.id.eT_18_am)
        et24Am = view!!.findViewById(R.id.eT_24_am)
        backgroundSpinner = view!!.findViewById(R.id.backgroundSpinner)
        spinnerBarrenaAm = view!!.findViewById(R.id.spinner_barrena_am)
        setEnableAM(validate)

        et6Pm = view!!.findViewById(R.id.eT_6_pm)
        et12Pm = view!!.findViewById(R.id.eT_12_pm)
        et18Pm = view!!.findViewById(R.id.eT_18_pm)
        et24Pm = view!!.findViewById(R.id.eT_24_pm)
        spinnerBarrenaPm = view!!.findViewById(R.id.spinner_barrena_pm)
        btnMonitoring = view!!.findViewById(R.id.btn_check_monitoring)
        btnMonitoring!!.setOnClickListener(this)
        setEnablePM(false)

        if (!validate) btnMonitoring!!.visibility = View.GONE
    }

    private fun setEnableAM(enable: Boolean) {
        et6Am!!.isEnable(enable)
        et12Am!!.isEnable(enable)
        et18Am!!.isEnable(enable)
        et24Am!!.isEnable(enable)
        spinnerBarrenaAm!!.isEnable(enable)
    }

    private fun setEnablePM(enable: Boolean) {
        et6Pm!!.isEnable(enable)
        et12Pm!!.isEnable(enable)
        et18Pm!!.isEnable(enable)
        et24Pm!!.isEnable(enable)
        spinnerBarrenaPm!!.isEnable(enable)
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessValvesDataPerDay().subscribe(this::setDataForVavle).addTo(subscriptions)
        viewModel.onSuccessRequestValves().subscribe(this::updateValveList).addTo(subscriptions)
        viewModel.onSuccessValvesDataPerDayExt().subscribe(this::getinfoforValves).addTo(subscriptions)
    }
    private fun getinfoforValves(list: List<ValveForTable>) {
        if (list.size > 0) {
            valveList.forEach{
                if(it.valveId ==  list.get(list.size-1).valveId){
                    it.status = list.get(list.size-1).meditionType
                }
            }
        } else {
            valveList.get(counterValvles).status = valveList.get(counterValvles).status
        }

        counterValvles += 1

        if(counterValvles  == valveList.size  ) {
            setDataSpinner()
        }

    }
    private fun setDataSpinner(){
        Log.e("....","TERMINO")
        spinnerValve?.adapter = CustomeSpinnerAdapter(this.context!!, valveList)
        //CustomeSpinnerAdapter(this.context!!, valveList)
                //ArrayAdapter<MonitoringViewModel.ValveItem>(activity?.applicationContext, R.layout.spinner_item_textview, valveList)
    }
    private fun setDataForVavle(list: List<ValveForTable>) {
        setValuesToZero()
        list.forEach {
            if (it.meditionType == 1) {
                et6Am!!.text = if(it.medition6 != null) it.medition6 else ""
                et12Am!!.text = if(it.medition12 != null) it.medition12 else ""
                et18Am!!.text = if(it.medition18 != null) it.medition18 else ""
                et24Am!!.text = if(it.medition24 != null) it.medition24 else ""

                val list = ArrayList<String>()
                list.add(it.barrena.toString())
                spinnerBarrenaAm!!.adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item_textview, list)

                setEnableAM(false)
                setEnablePM(true)

                type = 2
            } else {
                et6Pm!!.text = if(it.medition6 != null) it.medition6 else ""
                et12Pm!!.text = if(it.medition12 != null) it.medition12 else ""
                et18Pm!!.text = if(it.medition18 != null) it.medition18 else ""
                et24Pm!!.text = if(it.medition24 != null) it.medition24 else ""

                val list = ArrayList<String>()
                list.add(it.barrena.toString())
                spinnerBarrenaPm!!.adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item_textview, list)
                setEnablePM(false)

                btnMonitoring!!.visibility = View.GONE
            }
        }

        if (viewModel.validateDate() && list.isEmpty()) {
            initValue()
            setEnableAM(true)
            setEnablePM(false)
        }

    }

    private fun updateValveList(list: List<ValveFertirriego>) {
        valveList.clear()
        valveList.add(MonitoringViewModel.ValveItem(0, "SELECCIONAR VALVULA", -1 ))
        for (item in list) {
            valveList.add(MonitoringViewModel.ValveItem(item.valveId, item.valveName, 0))
            viewModel.getValvesDataForTable(item.valveId,"ext")
        }
        arrayValidateSpinner = ArrayList(list)
        spinnerValve?.adapter = CustomeSpinnerAdapter(this.context!!, valveList)

    }

    fun initValue(){
        type = 1
        initSpinner(6)
    }

    fun setValuesToZero() {
        et6Am!!.text = ""
        et12Am!!.text = ""
        et18Am!!.text = ""
        et24Am!!.text = ""
        et6Pm!!.text = ""
        et12Pm!!.text = ""
        et18Pm!!.text = ""
        et24Pm!!.text = ""
    }

    fun initSpinner(size: Int) {
        val list = ArrayList<String>()
        //list.add("SELECCIONAR")
        for (i in 1 until size) {
            list.add(i.toString())
        }
        spinnerBarrenaAm!!.adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item_textview, list)
        spinnerBarrenaPm!!.adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item_textview, list)
    }

    override fun onItemSelected(adapter: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (adapter!!.id) {
            R.id.spinner_valve -> {
                Log.e("--->", Gson().toJson(valveList[position]))
                Log.e("valveLogId", valveList[position].valveId!!.toString())
                viewModel.valveId = valveList[position].valveId!!
                itemSelected = position
                getDataFromDate()
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    fun getDataFromDate() {
        setEnableAM(false)
        setEnablePM(false)
        viewModel.getValvesDataForTable(viewModel.valveId,"")
        if (viewModel.validateDate()) {
            btnMonitoring!!.visibility = View.VISIBLE
            setEnableAM(true)
        }
        else btnMonitoring!!.visibility = View.GONE
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btn_check_monitoring -> {

                if(validateTextView()) {
                    Log.e("clickMonitoring", "click")
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("Fertirriego")
                    builder.setMessage("¿Desea guardar la información de esta valvula?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                        saveValve()
                        dialog.dismiss()
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
                    builder.show()
                }

            }
        }
    }

    private fun validateTextView(): Boolean {
        var validate = false
        if(viewModel.valveId == 0){
            showSnackBar("Se debe de seleccionar valvula")
            return false
        }
        if (type == 1) {
            when {
                et6Am!!.text.isNotEmpty() -> validate = true
                et12Am!!.text.isNotEmpty() -> validate = true
                et18Am!!.text.isNotEmpty() -> validate = true
                et24Am!!.text.isNotEmpty() -> validate = true
                else -> showSnackBar("Al menos un campo debe ser llenado")
            }
        } else {
            when {
                et6Pm!!.text.isNotEmpty() -> validate = true
                et12Pm!!.text.isNotEmpty() -> validate = true
                et18Pm!!.text.isNotEmpty() -> validate = true
                et24Pm!!.text.isNotEmpty() -> validate = true
                else -> showSnackBar("Al menos un campo debe ser llenado")
            }
        }

        return validate
    }

    fun showSnackBar(msj: String){
        val snackbar = Snackbar.make(fragView, msj, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    private fun saveValve() {
        if (type == 1) {
            viewModel.saveValveData(type, et6Am!!.text.toString(), et12Am!!.text.toString(),
                    et18Am!!.text.toString(), et24Am!!.text.toString(), spinnerBarrenaAm!!.selectedItemId.toInt() + 1)
            setEnableAM(false)
            setEnablePM(true)
            type = 2
            showSaveDialog()
        } else {
            viewModel.saveValveData(type, et6Pm!!.text.toString(), et12Pm!!.text.toString(),
                    et18Pm!!.text.toString(), et24Pm!!.text.toString(), spinnerBarrenaPm!!.selectedItemId.toInt() + 1)
            btnMonitoring!!.visibility = View.GONE
            setEnablePM(false)
            type = 1
            showSaveDialog()
        }
    }

    fun showSaveDialog(){
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Fertirriego")
        builder.setMessage("Revisión guardada con éxito")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            dialog.dismiss()
            valveList.get(itemSelected).status = valveList.get(itemSelected).status + 1
            spinnerValve?.adapter = CustomeSpinnerAdapter(this.context!!, valveList)
        }
        builder.show()
    }

}
