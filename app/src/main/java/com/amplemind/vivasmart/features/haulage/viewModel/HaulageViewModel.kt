package com.amplemind.vivasmart.features.haulage.viewModel

import android.util.Log
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.HaulageRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.TableGrooves
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.amplemind.vivasmart.core.utils.*
import com.amplemind.vivasmart.features.production_quality.SpinnerTablesViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject

class HaulageViewModel @Inject constructor(private val repository : HaulageRepository,
                                           private val uploadRepository: ProfileRepository,
                                           private val apiErrors: HandleApiErrors,
                                           private val preferences: UserAppPreferences,
                                           val mediator: Mediator){

    private var listOperators = BehaviorSubject.create<List<SpinnerCollaboratorViewModel>>()
    private var listVehicles = BehaviorSubject.create<List<SpinnerVehicleViewModel>>()
    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val successUpload = BehaviorSubject.create<MutableList<File>>()
    private val createCarryOrder = BehaviorSubject.create<Int>()
    private val updateSigns = BehaviorSubject.create<String>()

    lateinit var stageUuid: String
    var stageId: Int = 0
    var cropId: Int = 0
    var lotId: Int = 0
    /*
    val lotName = mediator.bundleControlQualityItemViewModel.getLotName()
    val cropName = mediator.bundleControlQualityItemViewModel.cropName
    val variety = mediator.bundleControlQualityItemViewModel.cropVariety
    val businessName = mediator.bundleControlQualityItemViewModel.businessName
     */
    private var carryCode = ""

    private val collaborators: MutableList<SpinnerCollaboratorViewModel> = mutableListOf()
    private val vehicles = mutableListOf<SpinnerVehicleViewModel>()
    private val signs = mutableListOf<String>()

    var boxTotal: Double = 0.0
    var boxCounts: List<VarietyBoxCount> = listOf()
    var secondQualityBoxCount: Double = 0.0

    private var selectTables = mutableListOf<Int>()

    fun getStage() =
            repository.getStage(stageId)

    fun setSelectTables(tables : List<Int>){
        selectTables.clear()
        selectTables = tables.toMutableList()
    }

    fun getSelectTables() : MutableList<Int> {
        return selectTables
    }

    fun getTables(): List<SpinnerTablesViewModel> {
        return tablesGrooves.map { SpinnerTablesViewModel(it) }
    }

    fun getTablesModel(): ArrayList<TableGrooves> {
        return tablesGrooves
    }

    fun getCollaborators(): List<SpinnerCollaboratorViewModel> {
        return collaborators
    }

    fun uploadSign(files: MutableList<File>): Disposable {
        return uploadRepository.uploadImage(files.first(), "user")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    if (files.isNotEmpty()) {
                        files.removeAt(0)
                    }
                    signs.add(it.relative_url)
                    successUpload.onNext(files)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun validateForm(table: List<Int>, boxNo: String, temperature: String, vehicle: Int): Boolean {
        if (table.isEmpty()) {
            requestFail.onNext("Seleccione una tabla")
            return false
        }
        /*
        if (boxNo.toIntOrNull() == null || boxNo.toInt() <= 0) {
            requestFail.onNext("Ingrese un número valido de cajas")
            return false
        }
        */
        if (temperature.toDoubleOrNull() == null) {
            requestFail.onNext("Ingrese una temperatura valida")
            return false
        }
        /*if (collaboratorId <= 0) {
            requestFail.onNext("Seleccione a un colaborador")
            return false
        }*/
        if (vehicle < 0) {
            requestFail.onNext("Seleccione una unidad")
            return false
        }
        return true
    }

    fun createOrder(table: List<Int>, temperature: String, collaboratorId: Int?, selectedVehicle: Int, comments : String): Disposable {
        val vehicleId = vehicles[selectedVehicle].id

        return repository.createCarryOrder(lotId, cropId, table, boxCounts, secondQualityBoxCount,
                boxTotal, temperature.toDouble(), collaboratorId, vehicleId, comments)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    carryCode = it.folio
                    createCarryOrder.onNext(it.id)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getVehicles(): Disposable {
        return repository.getVehicles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate() { progressStatus.onNext(false) }
                .map { it.data.map { SpinnerVehicleViewModel(it) } }
                .subscribe({
                    vehicles.clear()
                    vehicles.addAll(it)
                    listVehicles.onNext(vehicles)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun assignOrders(carryId: Int): Disposable {
        return repository.assignSigns(signs[0], signs[1], carryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    updateSigns.onNext(carryCode)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getOperators(name: String) : Disposable? {
        if (collaborators.filter { it.collaboratorName.equals(name) }.size > 0) {
            return null
        }
        return repository.getOperators(name, stageId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .observeOn(AndroidSchedulers.mainThread())
                .map { list ->
                    val results = list.data.map { SpinnerCollaboratorViewModel(it) }
                    collaborators.clear()
                    collaborators.addAll(results)
                    }
                .subscribe({
                    progressStatus.onNext(false)
                    listOperators.onNext(collaborators)
                }, {
                    progressStatus.onNext(false)
                    Log.e("Error", "Es ${it.localizedMessage}")
                })
    }

    fun getTableId(position: Int): Int {
        val tables = getTables()
        if (position >= tables.size) return 0
        return tables[position].getTableId()
    }

    fun getCollaboratorId(name: String): Int? {
        val filtered = collaborators.filter { it.collaboratorName == name }
        if (filtered.isEmpty()) return null
        return filtered.first().collaboratorId
    }

    fun getListOperator(): BehaviorSubject<List<SpinnerCollaboratorViewModel>> {
        return listOperators
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onSuccessUpload(): BehaviorSubject<MutableList<File>> {
        return successUpload
    }

    fun onCreateCarryOrder(): BehaviorSubject<Int> {
        return createCarryOrder
    }

    fun onAssignSigns(): BehaviorSubject<String> {
        return updateSigns
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onGetVehicles(): BehaviorSubject<List<SpinnerVehicleViewModel>> {
        return listVehicles
    }

    val tablesGrooves = createTablesList()

    private fun createTablesList(): ArrayList<TableGrooves> {
        val tables = arrayListOf<TableGrooves>()
        if (LOT!!.table1 > 0) tables.add(TableGrooves(1,LOT!!.table1))
        if (LOT!!.table2 > 0) tables.add(TableGrooves(2,LOT!!.table2))
        if (LOT!!.table3 > 0) tables.add(TableGrooves(3,LOT!!.table3))
        if (LOT!!.table4 > 0) tables.add(TableGrooves(4,LOT!!.table4))
        if (LOT!!.table5 > 0) tables.add(TableGrooves(5,LOT!!.table5))
        if (LOT!!.table6 > 0) tables.add(TableGrooves(6,LOT!!.table6))
        return tables
    }
}