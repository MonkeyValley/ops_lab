package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.model.ControlQualityActivitiesListModel
import com.amplemind.vivasmart.core.repository.model.ControlQualityListModel
import com.amplemind.vivasmart.core.repository.request.ReviewIssueRequest
import com.amplemind.vivasmart.core.repository.request.SignQualityRequest
import com.amplemind.vivasmart.core.repository.request.SyncControlQualityRequest
import com.amplemind.vivasmart.core.repository.request.SyncControlQualityResponse
import com.amplemind.vivasmart.core.repository.response.IssuesResponse
import io.reactivex.Observable
import retrofit2.http.*

interface QualityControlApi {

    @GET("/v2/quality_control/collaborator/condensed/{stageId}")
    @Headers("Content-Type: application/json")
    fun getCondensedCollaborators(
            @Header("Authorization") authentication: String,
            @Path("stageId") stageId: Int
    ): Observable<List<ControlQualityListModel>>

    @GET("/v2/quality_control/groove/{collaboratorId}/{stageId}")
    @Headers("Content-Type: application/json")
    fun getControlListActivities(
            @Header("Authorization") authentication: String,
            @Path("collaboratorId") collaboratorId: Int,
            @Path("stageId") stageId: Int
    ): Observable<List<ControlQualityActivitiesListModel>>

    @PUT("/v2/quality_control/{stageId}/done")
    @Headers("Content-Type: application/json")
    fun finalizeControlActivity(
            @Header("Authorization") authentication: String,
            @Path("stageId") stageId: Int,
            @Body body: SignQualityRequest
    ): Observable<Any>

    @GET("/v2/issue")
    @Headers("Content-Type: application/json")
    fun getListOfIssues(
            @Header("Authorization") authentication: String,
            @Query("activity_id") activity_id: Int,
            @Query("area_id") area_id: Int,
            @Query("category_id") category_id: Int,
            @Query("sub_category_id") sub_category_id: Int,
            @Query("__logic") logic: String = "AND",
            @Query("is_active") is_active: Boolean = true
    ): Observable<IssuesResponse>

    @POST("/v2/quality_control")
    @Headers("Content-Type: application/json")
    fun createIssue(
            @Header("Authorization") authentication: String,
            @Body body: ReviewIssueRequest
    ): Observable<Any>

    @POST("/v2/offline/sync/quality_control")
    @Headers("Content-Type: application/json")
    fun syncQualityControlIssues(
            @Header("Authorization") authentication: String,
            @Body body: SyncControlQualityRequest
    ): Observable<SyncControlQualityResponse>

}

