package com.amplemind.vivasmart.core.repository.response

import com.google.gson.annotations.SerializedName

data class FinishedProductResponse(
        @SerializedName("data") val data: List<FinishedProductResult>,
        @SerializedName("total_count") val total_count: Int
)

data class FinishedProductResult(
        @SerializedName("id") val id: Int,
        @SerializedName("lot_name") val lot_name: String? = null,
        @SerializedName("packing_name") val packing_name: String? = null,
        @SerializedName("percent") val percent: Double? = null
)
