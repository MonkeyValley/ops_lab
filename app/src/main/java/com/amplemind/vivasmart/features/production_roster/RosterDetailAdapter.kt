package com.amplemind.vivasmart.features.production_roster

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemRosterDetailBinding
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getPayment
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.utils.formatPayment
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.RosterDetailActivityLogViewModel
import androidx.databinding.library.baseAdapters.BR
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.Disposable

class RosterDetailAdapter(
        private val mEventBus: EventBus
) : BaseAdapter<ActivityLogModel, RosterDetailActivityLogViewModel, RosterDetailAdapter.RosterDetailViewHolder>() {

    companion object {
        private val TAG = RosterDetailAdapter::class.java.simpleName
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RosterDetailViewHolder {
        val binding = ItemRosterDetailBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)

        return RosterDetailViewHolder(binding)
    }

    override fun buildViewModel(item: ActivityLogModel): RosterDetailActivityLogViewModel
            = RosterDetailActivityLogViewModel(item)

    override fun getItemKey(item: ActivityLogModel): String = item.uuid

    private fun sendActivityLogClickedEvent(activityLog: ActivityLogModel) {
        mEventBus.send(ActivityLogClickedEvent( this, activityLog ))
    }

    private fun sendActivityLogLongClickedEvent(activityLog: ActivityLogModel) {
        mEventBus.send(ActivityLogLongClickedEvent( this, activityLog ))
    }

    fun calculateTotalPayment(isTraining: Boolean, isPractice: Boolean, isControl: Boolean): String {

        var total = 0.0

        for ((i: Int, item: ActivityLogModel) in items.withIndex()) {

            Log.e(TAG, "viewModel: ${getViewModel(i, false)?.calculatePayment(isTraining, isPractice, isControl)}")
            Log.e(TAG, "viewModel: ${item.getPayment(isTraining, isPractice, isControl)}")

            total += if (isTraining || isPractice || isControl) {
                getViewModel(i, false)?.calculatePayment(isPractice = isPractice, isTraining = isTraining, isControl = isControl)
                        ?: item.getPayment(isTraining = isTraining, isPractice = isPractice, isControl = isControl)
            }
            else {
                getViewModel(i, false)?.calculatePayment() ?: item.getPayment()
            }
        }

        return formatPayment(total)
    }


    inner class RosterDetailViewHolder(val binding: ItemRosterDetailBinding) : BaseAdapter.ViewHolder<RosterDetailActivityLogViewModel>(binding.root) {

        private var mDisposable: Disposable? = null

        override fun cleanUp() {
            super.cleanUp()
            mDisposable?.dispose()
        }

        override fun bind(viewModel: RosterDetailActivityLogViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
            try {
                binding.executePendingBindings()
            } catch (e: Exception){
                Log.e("RosterDetailAdapter", e.toString())
            }

            mDisposable = RxView.clicks(binding.root)
                    .subscribe {
                        sendActivityLogClickedEvent(viewModel.item)
                    }

            mDisposable = RxView.longClicks(binding.root)
                    .subscribe {
                        sendActivityLogLongClickedEvent(viewModel.item)
                    }
        }
    }

    data class ActivityLogClickedEvent (
            val adapter: RosterDetailAdapter,
            val activityLog: ActivityLogModel
    )

    data class ActivityLogLongClickedEvent (
            val adapter: RosterDetailAdapter,
            val activityLog: ActivityLogModel
    )


}
