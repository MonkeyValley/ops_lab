package com.amplemind.vivasmart.features.login.viewModel

import androidx.databinding.ObservableBoolean
import com.amplemind.vivasmart.core.extensions.isValidPassword
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.core.utils.Validations
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ChangePasswordViewModel @Inject constructor(val validation: Validations, private val api: AccountRepository,
                                                  private val apiErrors: HandleApiErrors) {

    private val validPassword: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val newPasswordValidations: BehaviorSubject<Validations.PasswordValidation> = BehaviorSubject.create()
    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val successChangePassword: BehaviorSubject<AccountRepository.ChangePasswordResponse> = BehaviorSubject.create()
    private val failChangePassword: BehaviorSubject<String> = BehaviorSubject.create()


    fun changePassword(request: AccountRepository.ChangePasswordRequest): Disposable {
        return api.changePassword(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({ successChangePassword.onNext(it) }, { failChangePassword.onNext(apiErrors.handleLoginError(it)) } )
    }


    fun validateUserPassword(password: String): Disposable {
        return api.validateCurrentPassword(AccountRepository.ValidatePasswordRequest(password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({ validPassword.onNext(true) }, { validPassword.onNext(false) } )
    }

    fun validatePassword(password: String, repeatPassword: String) {
        val passwordValidation = validation.validatePassword(password, repeatPassword)
        newPasswordValidations.onNext(passwordValidation)
    }

    fun onValidCurrentPassword() : BehaviorSubject<Boolean> {
        return validPassword
    }

    fun onValidNewPassword(): BehaviorSubject<Validations.PasswordValidation> {
        return newPasswordValidations
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onSuccessChangePassword(): BehaviorSubject<AccountRepository.ChangePasswordResponse> {
        return successChangePassword
    }

    fun onFailChangePassword(): BehaviorSubject<String> {
        return failChangePassword
    }

}