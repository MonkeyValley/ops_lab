package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.features.phenology.models.local.*
import io.reactivex.Observable
import retrofit2.http.*

interface PhenologyApi {
    @GET("/v2/stage_phenology")
    @Headers("Content-Type: application/json")
    fun getLots(
            @Header("Authorization") authentication: String,
            @Query("user_id") user_id: Int,
            @Query("is_commercial") is_commercial: Int
    ): Observable<List<PhenologyLot>>

    @GET("/v2/phenology_medition/{stage_id}")
    @Headers("Content-Type: application/json")
    fun getLotsVarieties(
            @Header("Authorization") authentication: String,
            @Path("stage_id") stage_id: Int,
            @Query("is_commercial") is_commercial: Int
    ): Observable<PhenologyResponse>


    @POST("v2/phenology_revision")
    @Headers("Content-Type: application/json")
    fun postPhenologyRevision(
            @Header("Authorization") authentication: String,
            @Body body: PhenologyPost
    ): Observable<PhenologyPostRevisionResponse>

    @POST("v2/phenology_revision/add_plant/{id_revision}")
    @Headers("Content-Type: application/json")
    fun postPhenologyPlant(
            @Header("Authorization") authentication: String,
            @Path("id_revision") id_revision: String,
            @Body body: PhenologyPlantItem
    ): Observable<PhenologyPostPlantResponse>


    @POST("v2/phenology_revision/complete/{id_revision}")
    @Headers("Content-Type: application/json")
    fun postPhenologySignAndComment(
            @Header("Authorization") authentication: String,
            @Path("id_revision") id_revision: String,
            @Body body: PhenologySignComment
    ): Observable<PhenologyPostRevisionResponse>

    @GET("/v2/phenology/var_revision_detail/{stage}/{variety}/{table_no}")
    fun getPhenologyDetailData(
            @Header("Authorization") authentication: String,
            @Path("stage") stage: Int,
            @Path("variety") varietyId: Int,
            @Path("table_no") tableNo: Int
    ): Observable<PhenologyDetailResponse>

    @GET("/v2/phenology/var_last_medition")
    fun getVariableLastRecord(
            @Header("Authorization") authentication: String,
            @Query("stage_id") stage_id: Int,
            @Query("table_no") table_no: Int,
            @Query("groove_from") groove_from: Int,
            @Query("groove_to") groove_to: Int,
            @Query("var_id") var_id: Int,
            @Query("plant_number") plant_number: Int
    ): Observable<VariableLastRecordResponse>


}