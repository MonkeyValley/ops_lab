package com.amplemind.vivasmart.feature.services

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.IntentServiceController
import kotlin.test.assertNotNull


@RunWith(RobolectricTestRunner::class)
class DownloadServiceTest {

    private var service: DownloadService? = null
    private var controller: IntentServiceController<DownloadService>? = null

    @Before
    fun setup() {
        controller = Robolectric.buildIntentService(DownloadService::class.java).create()
        service = controller!!.create().get()
    }

    @Test
    fun alarmServiceShouldBeNotNull() {
        assertNotNull(service)
    }
}