package com.amplemind.vivasmart.vo_core.repository.models.realm

import androidx.room.ColumnInfo
import com.amplemind.vivasmart.core.repository.model.CropStage
import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.google.gson.annotations.SerializedName

data class CarryOrderModel(@SerializedName("box_no") val boxNo: Double,
                           @SerializedName("variety_box") val varietyBoxCounts: List<VarietyBoxCount>,
                           @SerializedName("second_quality_box") val secondQualityBox: Double,
                           @SerializedName("business_unit_id") val businessUnitId: Long,
                           @SerializedName("collaborator_id") val collaboratorId: Long,
                           @SerializedName("crop_id") val cropId: Long,
                           @SerializedName("folio") val folio: String,
                           @SerializedName("is_active") @ColumnInfo(name = "carry_order_is_active") val isActive: Boolean,
                           @SerializedName("id") val id: Long,
                           @SerializedName("lot_id") val lotId: Long,
                           @SerializedName("revision") val reception: Boolean,
                           @SerializedName("sign_collaborator") val signCollaborator: String,
                           @SerializedName("sign_super") val signSuper: String,
                           @SerializedName("status") val status: String,
                           @SerializedName("table") val table: List<Int>?,
                           @SerializedName("temperature") val temperature: Double,
                           @SerializedName("userId") val userId: Long,
                           @SerializedName("vehicle_unit_id") val vehicleUnitId: Long,
                           @SerializedName("created_at") val date: String,
                           @SerializedName("comment") val comment: String?,
                           @SerializedName("lot") val lot: LotModel?,
                           @SerializedName("crop") val crop: CropStage?,
                           @SerializedName("user") val user: UserModel?,
                           @SerializedName("valid_box_no") val validBoxNo: Double?,
                           @SerializedName("average_weight") val averageWeight: Double?
)

