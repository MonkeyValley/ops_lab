package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityRevisionModel

class ItemReceptionQualityRevisionViewModel (val revision: ReceptionQualityRevisionModel){

    val total: String
        get() = revision.sampleSize.toString()

    val exportTotal: String
        get() = revision.export.toString()

    val exportPercentage: String
        get() = "%.2f %%".format(revision.export.toFloat() / revision.sampleSize * 100)

    val damagedTotal: String
        get() = (revision.sampleSize - revision.export).toString()

    val damagedPercentage: String
        get() = "%.2f %%".format((revision.sampleSize - revision.export).toFloat() / revision.sampleSize * 100)

}