package com.amplemind.vivasmart.core.repository.response

import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.core.repository.request.IssueRequest
import com.google.gson.annotations.SerializedName

data class QualityCarryOrderResponse(@SerializedName("carry_order_id") val carryOrderId: Int,
                               @SerializedName("sample_size") val sampleSize: Int,
                               @SerializedName("unit_id") val unitId: Int,
                               @SerializedName("user_id") val userId: Int,
                               @SerializedName("issues") val issues: List<IssueRequest>?,
                               @SerializedName("export") val export: Int,
                               @SerializedName("permanent_defect") val permanentDefect: Int,
                               @SerializedName("defect_condition") val defectCondition: Int): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.createTypedArrayList(IssueRequest),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(carryOrderId)
        parcel.writeInt(sampleSize)
        parcel.writeInt(unitId)
        parcel.writeInt(userId)
        parcel.writeTypedList(issues)
        parcel.writeInt(export)
        parcel.writeInt(permanentDefect)
        parcel.writeInt(defectCondition)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QualityCarryOrderResponse> {
        override fun createFromParcel(parcel: Parcel): QualityCarryOrderResponse {
            return QualityCarryOrderResponse(parcel)
        }

        override fun newArray(size: Int): Array<QualityCarryOrderResponse?> {
            return arrayOfNulls(size)
        }
    }

}