package com.amplemind.vivasmart.features.waste_control

import android.os.Bundle
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.SharedPreferencesUtils
import com.amplemind.vivasmart.features.waste_control.fragment.WasteControlTestFragment
import com.amplemind.vivasmart.features.waste_control.viewModel.WasteControlTestViewModel
import kotlinx.android.synthetic.main.activity_waste_control_test.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject

class WasteControlTestActivity : BaseActivityWithFragment() {

    @Inject
    lateinit var viewModel: WasteControlTestViewModel

    @Inject
    lateinit var preferences: UserAppPreferences

    companion object {
        lateinit var sharedPref: SharedPreferencesUtils
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waste_control_test)
        sharedPref = SharedPreferencesUtils(this)
        val crop = intent.getStringExtra("crop") ?: ""

        setupToolbar()
        callFragmentMenuActivities(crop)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_waste_control_test
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.waste_control_title)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        finish()
    }

    protected fun getFragmentCount(): Int {
        return supportFragmentManager.backStackEntryCount
    }

    private fun callFragmentMenuActivities(crop: String) {
        val support = supportFragmentManager
        val commit = support.beginTransaction()
                .replace(R.id.container, WasteControlTestFragment().newInstance("WasteControlTestFragment", crop))
                .addToBackStack("WasteControlTestFragment")
                .commit()
    }
}

