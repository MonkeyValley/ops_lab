package com.amplemind.vivasmart.core.base

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.utils.ACTIVITY_CONTEXT
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.crashlytics.android.Crashlytics
import dagger.android.support.DaggerAppCompatActivity

open class BaseActivityWithFragment : DaggerAppCompatActivity() {

    val subscriptions = AndroidDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ACTIVITY_CONTEXT = this
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    override fun onResume() {
        super.onResume()
        ACTIVITY_CONTEXT = this
        Log.e("ACTIVITY-CONTEXT", ACTIVITY_CONTEXT!!.toString())
    }

    override fun onPause() {
        hideKeyboard(this)
        super.onPause()
        //ACTIVITY_CONTEXT = null
        Log.e("ACTIVITY-CONTEXT", "null")
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.dispose()
    }

    protected fun setUpCrashlytics(userName: String, email: String) {
        Crashlytics.setUserName(userName)
        Crashlytics.setUserEmail(email)
    }

}