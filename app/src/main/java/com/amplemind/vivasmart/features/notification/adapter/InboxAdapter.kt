package com.amplemind.vivasmart.features.notification.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemInboxBinding
import com.amplemind.vivasmart.features.notification.service.MessageService
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


class InboxAdapter : RecyclerView.Adapter<InboxAdapter.InboxViewHolder>() {
    private var list = mutableListOf<MessageModel>()

    var listener: Listener? = null

    lateinit var service: MessageService

    private val clickSubject = PublishSubject.create<MessageModel>()

    private var mRecyclerView: RecyclerView? = null

    fun addElements(data: List<MessageModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InboxViewHolder {
        val binding = ItemInboxBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InboxViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: InboxViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem() = clickSubject

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mRecyclerView = null
    }

    inner class InboxViewHolder(private val binding: ItemInboxBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: MessageModel

        @RequiresApi(Build.VERSION_CODES.O)
        fun bind(item: MessageModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            if(item.status == "new") binding.btnShow.text = "Nuevo"

            binding.tvDate.text = getDateFormat(item.createdAt)

            setPreviewMessage(binding.tvPreviewMessage, item)

            binding.tvDisplayMessage.text = item.message

            binding.btnShow.setOnClickListener { view ->
                if(binding.llDisplayMessage.visibility == View.VISIBLE) {
                    binding.llDisplayMessage.visibility = View.GONE
                    binding.btnShow.background = (binding.root.resources.getDrawable(R.drawable.ic_button_green))
                    binding.btnShow.text = ">>"
                } else {
                    binding.llDisplayMessage.visibility = View.VISIBLE
                    binding.btnShow.background = (binding.root.resources.getDrawable(R.drawable.ic_button_red))
                    binding.btnShow.text = "<<"

                    service.updateMessageStatuss(item.id)
                }
            }
        }


        @RequiresApi(Build.VERSION_CODES.O)
        private fun getDateFormat(dateString: String): String {
            val inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS", Locale.ENGLISH)
            val outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH)
            val outputFormatterHour = DateTimeFormatter.ofPattern("HH:mm:ss", Locale.ENGLISH)
            val date =LocalDate.parse(dateString, inputFormatter)
            val hour = LocalDateTime.parse(dateString, inputFormatter)
            return outputFormatter.format(date) + "\n" + outputFormatterHour.format(hour)
        }

        private fun setPreviewMessage(textView: TextView, item: MessageModel) {
            if(item.data!!.lot_id == 0){
                textView.text = service.getNotificationType(item.data!!.notification_type_id)
            } else {
                textView.text = service.getLotName(item.data!!.lot_id) +
                        "\n" + service.getNotificationType(item.data!!.notification_type_id)
            }
        }


    }

    interface Listener {
        fun onGetCounterValue(osition: Int): Observable<Int>?
    }

}

