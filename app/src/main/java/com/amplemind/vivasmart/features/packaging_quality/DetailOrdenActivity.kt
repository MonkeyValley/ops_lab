package com.amplemind.vivasmart.features.packaging_quality

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.paintTables
import com.amplemind.vivasmart.core.extensions.snack
import com.amplemind.vivasmart.features.haulage.viewModel.CarryOrderViewModel
import com.amplemind.vivasmart.features.packaging_quality.clean_report.CleanReportActivity
import com.amplemind.vivasmart.features.packaging_quality.clean_report.CleaningReportTotalActivity
import com.amplemind.vivasmart.features.packaging_quality.fragment.StartReportDialogFragment
import com.amplemind.vivasmart.features.packaging_quality.viewModel.PackagingQualityDetailViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.SpinnerUnitsViewModel
import kotlinx.android.synthetic.main.content_detail_orden.*
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/9/18.
 */
@SuppressLint("Registered")
open class DetailOrdenActivity : BaseActivity() {

    private val TAG = DetailOrdenActivity::class.java.simpleName

    @Inject
    lateinit var viewModel: PackagingQualityDetailViewModel

    private val CLEANING_REPORT_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_detail_orden)

        setupToolbar()
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestSuccess().subscribe(this::onRequestSuccess).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onRequestGetUnits().subscribe(this::onGetUnits).addTo(subscriptions)
        viewModel.getDataQualityPackages(intent.getIntExtra("id", 0)).addTo(subscriptions)
    }

    private fun isSyncFlow(): Boolean {
        return intent.getBooleanExtra("SyncFlow", false)
    }

    private fun onRequestSuccess(vm: CarryOrderViewModel) {
        viewModel.getUnits().addTo(subscriptions)

        tv_lot_name.text = vm.lotName
        tv_crop_name.text = vm.cropName
        tv_box_size.text = vm.boxSize
        tv_type.text = vm.type
        tv_supervisor.text = vm.supervisorName
        tv_variety.text = vm.variety
        tv_size.text = vm.size
        tv_date.text = vm.getDate()
        tv_temp.text = vm.temperature
        tv_comments.text = vm.comment

        ln_tables.paintTables(vm.table)
    }

    private fun onGetUnits(list: List<SpinnerUnitsViewModel>) {
        val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item,
                list)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_units.adapter = spinnerAdapter
    }

    /**
     *  change title in toolbar
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_detail
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "R. Orden de acarreo"
    }

    /**
     *  menu search code
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (!isSyncFlow()) {
            menuInflater.inflate(R.menu.menu_quality_detail, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.detail_start -> {
                if (!ed_size_sample.text.isEmpty() && (ed_size_sample.text.toString().toInt() > 0)) {
                    val dialog = StartReportDialogFragment().newInstance()
                    dialog.show(supportFragmentManager, StartReportDialogFragment.TAG)

                    dialog.setListener(object : StartReportDialogFragment.OnStartReportListenner {
                        override fun startReportTotal() {
                            val intent = Intent(dialog.context!!, CleaningReportTotalActivity::class.java)
                            intent.putExtra("cropId", viewModel.getCropId())
                            intent.putExtra("total", ed_size_sample.text.toString().toInt())
                            intent.putExtra("carryOrderId", viewModel.getCarryOrderId())
                            intent.putExtra("unitId", viewModel.getUnitId(spinner_units.selectedItemPosition))
                            intent.putExtra("crop", viewModel.getCropName())
                            startActivityForResult(intent, CLEANING_REPORT_CODE)
                            dialog.dismiss()
                        }

                        override fun startReportPiece() {
                            val intent = Intent(dialog.context!!, CleanReportActivity::class.java)
                            intent.putExtra("cropId", viewModel.getCropId())
                            intent.putExtra("total", ed_size_sample.text.toString().toInt())
                            intent.putExtra("carryOrderId", viewModel.getCarryOrderId())
                            intent.putExtra("unitId", viewModel.getUnitId(spinner_units.selectedItemPosition))
                            intent.putExtra("crop", viewModel.getCropName())
                            startActivityForResult(intent, CLEANING_REPORT_CODE)
                            dialog.dismiss()
                        }
                    })
                } else {
                    main_view_order.snack("Tamaño debe ser mayor a cero")
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CLEANING_REPORT_CODE && resultCode == Activity.RESULT_OK) {
            val result = Intent()
            result.putExtra("carryOrderId", data!!.getIntExtra("carryOrderId", 0))
            setResult(Activity.RESULT_OK, result)
            finish()
        }
    }

}