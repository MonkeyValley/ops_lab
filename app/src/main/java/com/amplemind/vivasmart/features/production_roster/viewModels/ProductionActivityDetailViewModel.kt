package com.amplemind.vivasmart.features.production_roster.viewModels

import android.util.Log
import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ActivityCodeModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import io.reactivex.Observable
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class ProductionActivityDetailViewModel @Inject constructor(private val repository: ActivityCodeRepository,
                                                            private val preferences: UserAppPreferences,
                                                            private val apiErrors: HandleApiErrors) {

    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val requestFail: BehaviorSubject<String> = BehaviorSubject.create()
    private val requestSuccess: BehaviorSubject<List<ProductionActivityDetailItemViewModel>> = BehaviorSubject.create()
    private val newCodeCreate: BehaviorSubject<ProductionActivityDetailItemViewModel> = BehaviorSubject.create()
    private val activitiesCodes = mutableListOf<ProductionActivityDetailItemViewModel>()
    private var rangeFrom: Int? = null
    private var rangeTo: Int? = null
    private var cost: Double? = null
    private var activityId: Int? = null
    private var stageId: Int? = null
    private var unitLimit = 0
    private var activityName: String? = null

    fun getActivityCodes() : Disposable {
        activitiesCodes.clear()
        return repository.getCodes(preferences.token, activityId!!, stageId!!)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    repository.saveLocalCodes(it, activityId!!, stageId!!)
                    mapActivityCodes(it.sortedBy { it.code })}
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    createNextCode()
                    requestSuccess.onNext(it)
                }
                .subscribe({
                    if (it.isEmpty()) {
                        createActivityCode(false)
                    }
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLocalCodes(): Disposable {
        return repository.getLocalCodes(activityId!!, stageId!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapActivityCodes(it.sortedBy { it.code }) }
                .subscribe({
                    createNextCode()
                    requestSuccess.onNext(it)
                    if (it.isEmpty()) {
                        createLocalActivityCode(false)
                    }
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun createActivityCode(notifyIsNewCodeCreated: Boolean = true): Disposable {
        return repository.createCode(preferences.token, activityId!!, stageId!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    doAsync { repository.saveLocalCode(it) }
                    assignNewCode(it, notifyIsNewCodeCreated)
                }, {
                    createLocalActivityCode(notifyIsNewCodeCreated)
                })
    }

    private fun createLocalActivityCode(notifyIsNewCodeCreated: Boolean = true) {
        val code = if (activitiesCodes.size > 0) activitiesCodes[activitiesCodes.lastIndex].code else rangeFrom ?: 0
        val activityCode = ActivityCodeModel(null, activityId!!, code, unitLimit, "0/${unitLimit}", stageId!!, false)
        repository.savePendingRequestActivityCode(activityCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    activityCode.id = it
                    assignNewCode(activityCode, notifyIsNewCodeCreated)
                }, {
                    Log.e("Error","Es ${it.message}")
                })
    }

    private fun assignNewCode(code: ActivityCodeModel, notifyIsNewCodeCreated: Boolean) {
        if (activitiesCodes.size > 0 && activitiesCodes[activitiesCodes.lastIndex].isNewCode()) {
            activitiesCodes.removeAt(activitiesCodes.lastIndex)
        }
        activitiesCodes.add(ProductionActivityDetailItemViewModel(code, code.code, "$ ${cost}"))
        createNextCode()
        requestSuccess.onNext(activitiesCodes)
        if (notifyIsNewCodeCreated) {
            val removePositions = if (activitiesCodes[activitiesCodes.lastIndex].isNewCode()) 2 else 1
            newCodeCreate.onNext(activitiesCodes[activitiesCodes.size - removePositions])
        }
    }

    fun setInitialValues(rangeFrom: Int, rangeTo: Int, cost: Double, activityId: Int, stageId: Int, activityName: String, unitLimit: Int) {
        this.rangeFrom = rangeFrom
        this.rangeTo = rangeTo
        this.cost = cost
        this.activityId = activityId
        this.stageId = stageId
        this.activityName = activityName
        this.unitLimit = if (unitLimit == 0) 1 else unitLimit
    }

    private fun mapActivityCodes(models: List<ActivityCodeModel>): List<ProductionActivityDetailItemViewModel> {
        activitiesCodes.clear()
        for (model in models) {
            activitiesCodes.add(ProductionActivityDetailItemViewModel(model, model.code, "$ ${cost}"))
        }
        return activitiesCodes
    }

    private fun createNextCode() {
        if (activitiesCodes.size == 0) return
        val nextCode = activitiesCodes[activitiesCodes.lastIndex].code
        if (rangeTo!! > nextCode || canCreateUnlimitedCodes()) {
            activitiesCodes.add(ProductionActivityDetailItemViewModel(null, nextCode + 1, "$ ${cost}"))
        }
    }

    fun canCreateUnlimitedCodes(): Boolean {
        return rangeFrom == rangeTo
    }

    fun getAvaibleCodes(): List<ActivityCodeModel> {
        val models = ArrayList<ActivityCodeModel>()
        for (code in activitiesCodes) {
            if (!code.isDone && code.model != null) {
                models.add(code.model)
            }
        }
        return models
    }

    fun getMaxRange(): Int {
        return rangeTo ?: 0
    }

    fun getActivityName(): String {
        return activityName ?: ""
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onRequestSuccess(): BehaviorSubject<List<ProductionActivityDetailItemViewModel>> {
        return requestSuccess
    }

    fun onNewCodeCreated(): BehaviorSubject<ProductionActivityDetailItemViewModel> {
        return newCodeCreate
    }

}