package com.amplemind.vivasmart.vo_core.repository.dao

interface IDao<Model> {
    fun findByUuid(uuid: String): Model?
    fun findById(id: Long): Model?
    fun isEmpty(): Boolean
    fun saveServerResponse(response: Any?): Boolean
    fun upsert(obj: Model): Model?
    fun upsert(obj: List<Model>): List<Model>?
    fun delete(uuid: String): Model?
    fun <Type> buildCompatibleList(): List<Type>
    fun getLastVersion(versionField: String = "version"): Long
}