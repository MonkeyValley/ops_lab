package com.amplemind.vivasmart.features.lots.viewModel

import com.amplemind.vivasmart.vo_core.repository.LotChooserRepository
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import io.reactivex.Observable
import javax.inject.Inject

class SelectionLotsViewModel @Inject constructor(
        private val repository: LotChooserRepository
) {

    var stageTypeId: Int = 0

    fun setStageType(productionStageName: String) {
        stageTypeId = when (productionStageName) {
            "Cosecha" -> 1
            "Mantenimiento" -> 2
            "Fertiriego" -> 3
            "MIPE" -> 4
            "Labores Culturales" -> 5
            "Inocuidad" -> 6
            else -> 0
        }
    }

    fun cleanUp() {
        repository.cleanUp()
    }

    fun loadLots(): Observable<Changes<StageModel>> =
            repository.findAll()


    fun reOpenLot(stage: StageModel) =
            repository.isClosed(stage, false, stageTypeId)

    fun isClosed(lotId: Int) =
            repository.isClose(stageTypeId, lotId)

    fun getLotActivityCouter(lotId: Int) =
            repository.getLotActivityCouter(lotId, stageTypeId)


}
