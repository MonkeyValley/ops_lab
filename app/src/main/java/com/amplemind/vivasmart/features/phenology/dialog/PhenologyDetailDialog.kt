package com.amplemind.vivasmart.features.phenology.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SwitchCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.phenology.viewmodel.PhenologyDetailViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesPhenologyItemViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesTablesPhenologyItemViewModel
import com.google.gson.Gson
import com.jakewharton.rxbinding2.view.visibility
import javax.inject.Inject


class PhenologyDetailDialog : BaseDialogFragment(), CompoundButton.OnCheckedChangeListener {

    @Inject
    lateinit var viewModel: PhenologyDetailViewModel

    fun newInstance(stage_id: Int, variety : String, table : String ): PhenologyDetailDialog {
        val args = Bundle()
        args.putInt(STAGEID, stage_id)
        args.putString(VARIETY, variety)
        args.putString(TABLE, table)

        val dialog = PhenologyDetailDialog()
        dialog.arguments = args
        return dialog
    }

    companion object {
        val TAG = PhenologyDetailDialog::class.java.simpleName
        const val STAGEID = "stage_id"
        const val VARIETY = "variety"
        const val TABLE = "table"
    }

    var btnClose: Button? = null
    var layout: HorizontalScrollView? = null
    var layout2: HorizontalScrollView? = null
    var progressBar: ProgressBar? = null
    var tvDate1: TextView? = null
    var tvDate2: TextView? = null
    var table: TableLayout? = null
    var table2: TableLayout? = null
    var table3: TableLayout? = null
    var table4: TableLayout? = null
    var tittle: TextView ?=  null
    var swTable: SwitchCompat ?= null
    var tableData1: TableLayout ?= null
    var tableData2: TableLayout ?= null

    var tvDate1_2: TextView? = null
    var tvDate2_2: TextView? = null
    var table_2: TableLayout? = null
    var table2_2: TableLayout? = null
    var table3_2: TableLayout? = null
    var table4_2: TableLayout? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.activity_phenology_detail_dialog, null, false)
        builder.setCancelable(false)
        builder.setView(mView)

        val variety = Gson().fromJson(arguments?.getString(VARIETY), VarietiesPhenologyItemViewModel::class.java )
        val table = Gson().fromJson(arguments?.getString(TABLE), VarietiesTablesPhenologyItemViewModel::class.java )

        viewModel.stage_id = arguments?.getInt(STAGEID) ?: 0
        viewModel.variety_id = variety.variety_id!!.toInt()
        viewModel.table_no = table.table_no.toInt()

        setupUI(mView)
        tittle!!.text = "${variety.variety_name} /Tabla:${table.table_no} /Surco: ${table.groove_from}-${table.groove_to}"
        addSubscribers()
        loadData()
        return builder.create()
    }

    private fun addSubscribers() {
        viewModel.onReportLoaded(this::onReportLoaded).addTo(subscriptions)
        viewModel.onReportLoadingError(this::onReportLoadingError).addTo(subscriptions)
    }

    private fun showProgressBar(show: Boolean) { progressBar!!.visibility = if (show) View.VISIBLE else View.GONE }

    private fun setupUI(view: View) {
        btnClose = view.findViewById(R.id.btn_close)
        btnClose!!.setOnClickListener { v -> onClick(v) }
        layout = view.findViewById(R.id.ln_count2)
        layout2 = view.findViewById(R.id.ln_count3)
        progressBar = view.findViewById(R.id.progress_bar_phenology)
        tvDate1 = view.findViewById(R.id.tv_date_week1)
        tvDate2 = view.findViewById(R.id.tv_date_week2)
        table = view.findViewById(R.id.tb_report)
        table2 = view.findViewById(R.id.tb_report2)
        table3 = view.findViewById(R.id.tb_report3)
        table4 = view.findViewById(R.id.tb_report4)
        tittle = view.findViewById(R.id.txt_Tittle)
        swTable = view.findViewById(R.id.sw_phenology)
        swTable!!.setOnCheckedChangeListener(this)
        tableData1 = view.findViewById(R.id.tb_data1)
        tableData2 = view.findViewById(R.id.tb_data2)

        tvDate1_2 = view.findViewById(R.id.tv_date_week1_2)
        tvDate2_2 = view.findViewById(R.id.tv_date_week2_2)
        table_2 = view.findViewById(R.id.tb_report_2)
        table2_2 = view.findViewById(R.id.tb_report2_2)
        table3_2 = view.findViewById(R.id.tb_report3_2)
        table4_2 = view.findViewById(R.id.tb_report4_2)
    }

    private fun onClick(view: View) {
        when (view.id) {
            R.id.btn_close -> {
                dismiss()
            }
        }
    }

    private fun setDateTable(data: List<ItemPhenologyDetailViewModel>) {
        if (data.isNotEmpty()) {
            fillFooterTable(data)
            fillBodyTable(data)
            fillBodyTableAverage(data)
            fillHeaderTable(data)
        }
    }

    private fun fillHeaderTable(data: List<ItemPhenologyDetailViewModel>){
        val variableName = ArrayList<String>()
        var valvuleList1 = ArrayList<String>()
        var valvuleList2 = ArrayList<String>()
        val unit = ArrayList<String>()
        variableName.add("")

        if(data[0].valves.size > 0) {
            tvDate1!!.text = " " + data[0].valves[0].date + " "
            tvDate1_2!!.text = " " + data[0].valves[0].date + " "
            for (valve in data[0].valves[0].valvesDetail) {
                valvuleList1.add(" V" + valve.valve.toString() + " ")
            }
            val a = valvuleList1.distinct()
            valvuleList1.clear()
            valvuleList1.addAll(a)
        }

        if(data[0].valves.size > 1) {
            tvDate2!!.text = " " + data[0].valves[1].date + " "
            tvDate2_2!!.text = " " + data[0].valves[1].date + " "
            for (valve in data[0].valves[1].valvesDetail) {
                valvuleList2.add(" V" + valve.valve.toString() + " ")
            }
            val b = valvuleList2.distinct()
            valvuleList2.clear()
            valvuleList2.addAll(b)
        }

        unit.add("")

        if(valvuleList1.size == 0) valvuleList1.add("")
        if(valvuleList2.size == 0) valvuleList2.add("")

        initTable(variableName, valvuleList1, valvuleList2, unit)
        showProgressBar(false)
    }

    private fun fillBodyTable(data: List<ItemPhenologyDetailViewModel>) {
        val variableName = ArrayList<String>()
        val valvuleList1 = ArrayList<String>()
        val valvuleList2 = ArrayList<String>()
        val unit = ArrayList<String>()

        for (variable in data[0].variables) {
            variableName.clear()
            valvuleList1.clear()
            valvuleList2.clear()
            unit.clear()

            variableName.add(" " + variable.varName + " ")

            if(data[0].valves.size > 0) {
                for (data in data[0].valves[0].valvesDetail) {
                        if (data.varId == variable.varId) {
                            valvuleList1.add(" " + "%.2f".format(data.medition) + " ")
                        }
                }
                if( valvuleList1.size == 0){
                    valvuleList1.add(" " + 0 + " ")
                }
            } else {
                valvuleList1.add(" " + 0 + " ")
            }

            if(data[0].valves.size > 1) {
                for (data in data[0].valves[1].valvesDetail) {
                        if (data.varId == variable.varId) {
                            valvuleList2.add(" " + "%.2f".format(data.medition) + " ")
                        }
                    }
                if( valvuleList2.size == 0){
                    valvuleList2.add(" " + 0 + " ")
                }
            } else {
                valvuleList2.add(" " + 0 + " ")
            }

            unit.add(" " + variable.varUnitName + " ")
            initTable(variableName, valvuleList1, valvuleList2, unit)
        }
    }

    private fun fillBodyTableAverage(data: List<ItemPhenologyDetailViewModel>) {
        val variableName = ArrayList<String>()
        val valvuleList1 = ArrayList<String>()
        val valvuleList2 = ArrayList<String>()
        val unit = ArrayList<String>()

        for (variable in data[0].variables) {
            variableName.clear()
            valvuleList1.clear()
            valvuleList2.clear()
            var variableAverage = 0.0
            var count = 0
            unit.clear()

            variableName.add(" " + variable.varName + " ")

            if(data[0].valves.size > 0) {
                for (data in data[0].valves[0].valvesDetail) {
                    if (data.varId == variable.varId) {
                        variableAverage += data.medition
                        count++
                    }
                }
                if(variableAverage == 0.0){
                    valvuleList1.add("0")
                } else {
                    var result = variableAverage / count
                    valvuleList1.add("%.2f".format(result))
                }
            } else {
                valvuleList1.add("0")
            }

            count = 0
            variableAverage = 0.0
            if(data[0].valves.size > 1) {
                for (data in data[0].valves[1].valvesDetail) {
                    if (data.varId == variable.varId) {
                        variableAverage += data.medition
                        count++
                    }
                }
                if(variableAverage == 0.0){
                    valvuleList2.add(" " + 0 + " ")
                } else {
                    var result = variableAverage / count
                    valvuleList2.add("%.2f".format(result))
                }
            } else {
                valvuleList2.add("0")
            }

            unit.add(" " + variable.varUnitName + " ")
            initTableAverage(variableName, valvuleList1, valvuleList2, unit)
        }
    }

    private fun fillFooterTable(data: List<ItemPhenologyDetailViewModel>) {
        val variableName = ArrayList<String>()
        val valvuleList1 = ArrayList<String>()
        val valvuleList2 = ArrayList<String>()
        val unit = ArrayList<String>()
        variableName.add(" Frutos con racimo capado ")

        if(data[0].valves.size > 0) {
            var valve1 = 0
            for (data in data[0].valves[0].valvesDetail) {
                if (valve1 != data.valve) {
                    if(data.hasCluster) {
                        if (data.cluster != null) {
                            valvuleList1.add(" " + data.cluster + " ")
                        } else {
                            valvuleList1.add(" " + 0 + " ")
                        }
                    } else {
                        valvuleList1.add(" " + 0 + " ")
                    }
                    valve1 = data.valve
                }
            }
        } else {
            valvuleList1.add(" " + 0 + " ")
        }

        if(data[0].valves.size > 1) {
            var valve2 = 0
            for (data in data[0].valves[1].valvesDetail) {
                if (valve2 != data.valve) {
                    if(data.hasCluster) {
                        if (data.cluster != null) {
                            valvuleList2.add(" " + data.cluster + " ")
                        } else {
                            valvuleList2.add(" " + 0 + " ")
                        }
                    } else {
                        valvuleList2.add(" " + 0 + " ")
                    }
                    valve2 = data.valve
                }
            }
        } else {
            valvuleList2.add(" " + 0 + " ")
        }

        unit.add("")

        if(!valvuleList1.contains(" 0 ")){
            initTable(variableName, valvuleList1, valvuleList2, unit)
        } else if(!valvuleList2.contains(" 0 ")){
            initTable(variableName, valvuleList1, valvuleList2, unit)
        }
    }

    private fun initTable(variable: ArrayList<String>, valvuleList1: ArrayList<String>, valvuleList2: ArrayList<String>, unit: ArrayList<String>) {
        table!!.addView(createRow(variable, Gravity.LEFT), 0)
        table2!!.addView(createRow(valvuleList1, Gravity.CENTER), 0)
        table3!!.addView(createRow(valvuleList2, Gravity.CENTER), 0)
        table4!!.addView(createRow(unit, Gravity.LEFT), 0)
    }

    private fun initTableAverage(variable: ArrayList<String>, valvuleList1: ArrayList<String>, valvuleList2: ArrayList<String>, unit: ArrayList<String>) {
        table_2!!.addView(createRow(variable, Gravity.LEFT), 0)
        table2_2!!.addView(createRow(valvuleList1, Gravity.CENTER), 0)
        table3_2!!.addView(createRow(valvuleList2, Gravity.CENTER), 0)
        table4_2!!.addView(createRow(unit, Gravity.LEFT), 0)
    }

    private fun createRow(data: ArrayList<String>, gravity: Int): TableRow {
        val row = TableRow(this.context)
        row.setBackgroundResource(R.drawable.row_border)
        row.gravity = gravity
        val lp: TableRow.LayoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        row.layoutParams = lp
        for (i in 1..data.size) {
            val tv = TextView(this.context)
            tv.gravity = gravity
            tv.text = data[i - 1]
            row.addView(tv)
        }
        return row
    }

    override fun loadData() {
        showProgressBar(true)
        viewModel.lodPhenologyDetail()
    }

    private fun showNoRecordsLayout(show: Boolean) { layout2!!.visibility = if (show) View.GONE else View.VISIBLE }

    private fun onReportLoaded(data: List<ItemPhenologyDetailViewModel>) {
        showNoRecordsLayout(data.isEmpty())
        setDateTable(data)
    }

    private fun onReportLoadingError(error: Throwable) = showProgressBar(false)


    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if(isChecked){
            layout!!.visibility = View.VISIBLE
            layout2!!.visibility = View.GONE
        } else {
            layout!!.visibility = View.GONE
            layout2!!.visibility = View.VISIBLE
        }
    }
}
