package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_3_1_0 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectDailyPayModelrealm(realm)
        migrateUploadObjectStageTypeDataModelrealm(realm)
    }

    private fun migrateUploadObjectDailyPayModelrealm(realm: DynamicRealm){
        realm.schema.create("DailyPayModel")?.apply {
            addField("collaboratorEmployeeCode", String::class.javaObjectType)
            addField("collaboratorId", Int::class.javaObjectType)
            addField("collaboratorUuid", String::class.javaObjectType)
            addField("dailypayUuid", String::class.javaObjectType)
            addField("dailypayVersion", Long::class.javaObjectType)
            addField("date", String::class.javaObjectType)
        }
    }

    private fun migrateUploadObjectStageTypeDataModelrealm(realm: DynamicRealm){
        realm.schema.create("StageTypeModel")?.apply {
            addField("idStageType", Int::class.javaObjectType)
            addField("date", String::class.javaObjectType)
            addField("active", Boolean::class.javaObjectType)
        }
    }

    override fun drop(realm: DynamicRealm) {

    }
}