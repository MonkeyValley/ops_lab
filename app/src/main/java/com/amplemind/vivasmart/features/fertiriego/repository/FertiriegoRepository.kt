package com.amplemind.vivasmart.features.fertiriego.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.features.menus.getActivitiesFertirriego
import com.google.gson.Gson
import io.reactivex.Observable
import javax.inject.Inject

class FertiriegoRepository @Inject constructor(
        private val authorization: ApiAuthorization,
        private val preferences: UserAppPreferences) {

    val userInfo: UserModel = Gson().fromJson(preferences.userInfo, UserModel::class.java)
    var businessUnit = userInfo.businessUnitId

    fun getFertirriegoActivitiesMenu(): Observable<List<PayrollActivitiesModel>> {
        return Observable.just(getActivitiesFertirriego())
    }

}