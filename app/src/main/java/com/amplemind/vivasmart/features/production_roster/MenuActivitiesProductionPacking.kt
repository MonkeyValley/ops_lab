package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.crop_validation.CropValidationActivity
import com.amplemind.vivasmart.features.mipe.MipeMonitoringLotsActivity
import com.amplemind.vivasmart.features.packaging_quality.PackagingQualityActivity
import com.amplemind.vivasmart.features.packaging_quality.RecibaActivity
import com.amplemind.vivasmart.features.production_quality.ReportQualityFinishedProductActivity
import com.amplemind.vivasmart.features.reciba_quality.ReportRecibaQualityActivity
import com.amplemind.vivasmart.features.waste_control.WasteControlActivity
import kotlinx.android.synthetic.main.content_payroll_production_menu_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject


class MenuActivitiesProductionPacking : BaseFragment() {

    companion object {
        val TAG = MenuActivitiesProductionFragment::class.java.simpleName!!

        fun newInstance(): MenuActivitiesProductionPacking {
            return MenuActivitiesProductionPacking()
        }
    }

    @Inject
    lateinit var viewModel: PayrollActivitiesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_payroll_production_menu_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "Calidad en empaque"
        lbl_track_title.text = "/ Calidad / Empaque / "
        initRecycler()

        viewModel.loadMenuPackingProduction().addTo(subscriptions)
        viewModel.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_menu_production_payroll.adapter as ActivitiesPayrollAdapter).addElements(activities)
        (rv_menu_production_payroll.adapter as ActivitiesPayrollAdapter).onClickItem().subscribe(this::clickActivityPayroll).addTo(subscriptions)
    }

    private fun clickActivityPayroll(item: String) {
        if (item == "Reciba") {
            startActivity(Intent(context!!, RecibaActivity::class.java))
        } else if(item == "Validación de cosecha"){
            if (context!!.hasInternet()) {
                startActivity(Intent(context!!, CropValidationActivity::class.java))
            }
            else{
                noInternet()
            }
        } else if(item == "Calidad Reciba") {
            if (context!!.hasInternet()) {
                startActivity(Intent(context!!, ReportRecibaQualityActivity::class.java))
            }
            else{
                noInternet()
            }
        } else if (item == "Calidad en empaque") {
            startActivity(Intent(context!!, PackagingQualityActivity::class.java))
        } else if  (item == "Reporte de calidad") {
            startActivity(Intent(context!!, MipeMonitoringLotsActivity::class.java).apply {
                putExtra(MipeMonitoringLotsActivity.PARAM_TYPE, "1")
                putExtra(MipeMonitoringLotsActivity.PARAM_TITTLE, "MIPE")
            })
        } else if (item == "Calidad de producto terminado") {
            if (context!!.hasInternet()) {
                startActivity(Intent(context!!, ReportQualityFinishedProductActivity::class.java))
            }
            else{
                noInternet()
            }
        } else if (item == "Control de desperdicio") {
            if (context!!.hasInternet()) {
                startActivity(Intent(context!!, WasteControlActivity::class.java))
            }
            else{
                noInternet()
            }
        }
    }

    private fun noInternet() {
        Toast.makeText(context, "Éste módulo requiere conexión a internet", Toast.LENGTH_SHORT).show()
    }

    private fun initRecycler() {
        rv_menu_production_payroll.hasFixedSize()
        rv_menu_production_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_production_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_production_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_production_payroll.adapter = ActivitiesPayrollAdapter()
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "Calidad"
    }
}