package com.amplemind.vivasmart.core.repository.request

import com.amplemind.vivasmart.core.repository.model.ProductionCategoriesReportModel
import com.google.gson.annotations.SerializedName

class ProductionCategoriesReportResponse(@SerializedName("data") val data: ProductionCategoriesReportModel)