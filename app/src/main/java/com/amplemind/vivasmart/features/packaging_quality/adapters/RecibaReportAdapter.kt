package com.amplemind.vivasmart.features.packaging_quality.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.utils.CROP_ID_RECIBA
import com.amplemind.vivasmart.databinding.ItemRecibaReportBinding
import com.amplemind.vivasmart.features.adapters.BaseRecyclerViewAdapter
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ItemRecibaReportViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.Crop
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.Disposable
import io.realm.Realm

class RecibaReportAdapter : BaseRecyclerViewAdapter<ItemRecibaReportViewModel, RecibaReportAdapter.RecibaReportViewHolder>() {

    private var mOnClickListener: ((RecibaReportAdapter, ItemRecibaReportViewModel, Int) -> Unit)? = null
    var data = ArrayList<ItemRecibaReportViewModel>()
    var scroll = false

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): RecibaReportViewHolder {
        val view = ItemRecibaReportBinding.inflate(LayoutInflater.from(parent.context!!), parent, false)
        return RecibaReportViewHolder(view)
    }

    fun setOnClickListener(callback: (RecibaReportAdapter, ItemRecibaReportViewModel, Int) -> Unit) {
        mOnClickListener = callback
    }

    inner class RecibaReportViewHolder(val binding: ItemRecibaReportBinding): BaseViewHolder<ItemRecibaReportViewModel>(binding.root) {

        private var clickEvent: Disposable? = null

        override fun bind(item: ItemRecibaReportViewModel) {
            binding.apply{
                setVariable(BR.viewModel, item)
                executePendingBindings()
            }

            if(item.cropId != 0){
                if(!scroll) {
                    setCropNameAndBoxes(binding, item.cropId)
                }
            }

            clickEvent?.dispose()

            clickEvent = RxView.clicks(binding.root)
                    .subscribe{
                        mOnClickListener?.invoke(this@RecibaReportAdapter, mData[adapterPosition], adapterPosition)
                    }
        }

    }

    private fun setCropNameAndBoxes(binding: ItemRecibaReportBinding, cropId: Int?) {
        if (CROP_ID_RECIBA != cropId) {
            CROP_ID_RECIBA = cropId!!
            var cropName = Realm.getDefaultInstance()
                    .where(Crop::class.java)
                    .equalTo("id", cropId)
                    .findFirst()!!.name ?: ""

            binding.llCrop!!.visibility = View.VISIBLE
            binding.tvCrop!!.text = cropName

            var boxes: Double = 0.0
            data.forEach {
                if (it.cropId == cropId){
                    boxes+= it.boxes.toDouble()
                }
            }

            binding.tvBoxReceived!!.text = "$boxes Cajas"

        }
    }


}