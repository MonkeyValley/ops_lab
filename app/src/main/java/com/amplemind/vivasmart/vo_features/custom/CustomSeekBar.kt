package com.amplemind.vivasmart.vo_features.custom

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.res.ResourcesCompat
import androidx.appcompat.widget.AppCompatSeekBar
import android.util.AttributeSet
import android.view.MotionEvent
import com.amplemind.vivasmart.R
import io.reactivex.subjects.BehaviorSubject

class CustomSeekBar : AppCompatSeekBar {

    // - Mark Properties
    private var mThumb: Drawable? = null

    enum class State {
        PAUSED,
        CONTINUE,
        FINISH
    }

    var seekState = State.PAUSED
        private set

    /*
    * Behavior Observables
    * they are used to notify the view about any change
    * */
    private val seekStateControl = BehaviorSubject.create<State>()

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun setThumb(thumb: Drawable) {
        super.setThumb(thumb)
        mThumb = thumb
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {

            if (event.x >= mThumb!!.getBounds().left
                    && event.x <= mThumb!!.getBounds().right
                    && event.y <= mThumb!!.getBounds().bottom
                    && event.y >= mThumb!!.getBounds().top) {
                changeDownStatus()
                super.onTouchEvent(event)
            } else {
                return false
            }
        } else if (event.action == MotionEvent.ACTION_UP) {
                setSeekControlState(this.progress)
                changeUpStatus()
            return false
        } else {
            super.onTouchEvent(event)
        }

        return true
    }

    fun setSeekControlState(position: Int) {
        when (position) {
            0 -> {
                seekStateControl.onNext(seekState)
            }
            2 -> {
                seekStateControl.onNext(State.FINISH)
            }
        }
    }

    fun pauseTimers() {
        seekState = State.CONTINUE
    }

    fun restartTimers() {
        seekState = State.PAUSED
    }

    fun getSeekStateControl(): BehaviorSubject<State> {
        return seekStateControl
    }

    fun changeDownStatus() {
        val seekProgressDrawable = if (seekState == State.PAUSED) R.drawable.seek_bar_progress else R.drawable.seek_bar_progress_paused
        this.thumb = ResourcesCompat.getDrawable(resources, R.drawable.ic_unlock, null)
        this.progressDrawable = ResourcesCompat.getDrawable(resources, seekProgressDrawable, null)
    }

    fun changeUpStatus() {
        this.thumb = ResourcesCompat.getDrawable(resources, R.drawable.ic_lock, null)
        this.progressDrawable = ResourcesCompat.getDrawable(resources, R.drawable.seek_bar_progress_disabled, null)
        this.progress = 1
    }

}
