package com.amplemind.vivasmart.features.haulage.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.repository.model.TableGrooves
import com.amplemind.vivasmart.features.haulage.adapter.HaulageSelectTablesAdapter

class HaulageSelectTablesDialog : BaseDialogFragment() {

    private var listener: OnHaulageSelecteTables? = null
    private var mView : View? = null

    companion object {
        val TAG = HaulageSelectTablesDialog::class.java.simpleName

        fun newInstance(tables: ArrayList<TableGrooves>, selectTables : ArrayList<Int>): HaulageSelectTablesDialog {
            val dialog = HaulageSelectTablesDialog()
            val args = Bundle()
            args.putParcelableArrayList("TABLES", tables)
            args.putIntegerArrayList("SELECTED", selectTables)
            dialog.arguments = args
            return dialog
        }
    }

    private fun getTables(): ArrayList<TableGrooves>? {
        return arguments?.getParcelableArrayList<TableGrooves>("TABLES")
    }

    private fun getSelectedTables(): ArrayList<Int>? {
        return arguments?.getIntegerArrayList("SELECTED")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mView = LayoutInflater.from(context).inflate(R.layout.content_haulage_select_tables_dialog, null, false)
        builder.setView(mView)

        setupRecycler(mView!!.findViewById(R.id.rv_select_tables))

        builder.setPositiveButton("Aceptar", null)
        builder.setNegativeButton("Cancelar") { _, _ ->
            dismiss()
        }
        builder.setCancelable(false)
        return builder.create()
    }

    private fun setupRecycler(rv_tables : RecyclerView) {
        val adapter = HaulageSelectTablesAdapter(getSelectedTables())
        rv_tables.hasFixedSize()
        rv_tables.layoutManager = LinearLayoutManager(context)
        rv_tables.itemAnimator = DefaultItemAnimator()
        rv_tables.adapter = adapter
        adapter.addElements(getTables())
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            val tables = (mView!!.findViewById<RecyclerView>(R.id.rv_select_tables).adapter as HaulageSelectTablesAdapter).getTablesSelected()
            listener?.onSelectTables(tables)
            dialog!!.dismiss()
        }
    }

    fun setOnHaulageSelected(listener : OnHaulageSelecteTables){
        this.listener = listener
    }

}

interface OnHaulageSelecteTables {
    fun onSelectTables(list: List<Int>)
}