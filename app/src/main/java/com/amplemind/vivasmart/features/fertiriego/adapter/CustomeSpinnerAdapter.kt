package com.amplemind.vivasmart.features.fertiriego.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.fertiriego.viewModel.MonitoringViewModel
import kotlinx.android.synthetic.main.custo_spinner.view.*

class CustomeSpinnerAdapter (ctx: Context, moods: List<MonitoringViewModel.ValveItem>) : ArrayAdapter<MonitoringViewModel.ValveItem>(ctx, 0, moods) {
        override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
            return this.createView(position, recycledView, parent)
        }
        override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
            return this.createView(position, recycledView, parent)
        }
        private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
            val mood = getItem(position)
            val view = recycledView ?: LayoutInflater.from(context).inflate(
                    R.layout.custo_spinner,
                    parent,
                    false
            )
            view.lblTitleSpinner.text = mood!!.valveName

            when(mood!!.status){
                0->view.setBackgroundColor( view.resources.getColor(R.color.mililightgary) )
                1->view.setBackgroundColor( view.resources.getColor(R.color.yellow_light) )
                2->view.setBackgroundColor( view.resources.getColor(R.color.blue_light) )
                else ->view.setBackgroundColor( view.resources.getColor(R.color.mililightgary) )
            }

            return view
        }
}