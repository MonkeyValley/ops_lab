package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.core.repository.PayrollRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import io.reactivex.Observable
import io.realm.Realm
import javax.inject.Inject

class PayrollViewModel @Inject constructor(private val repository : PayrollRepository){

    fun countActiveLogsInStage(stage: StageModel): Observable<Int> {
        return repository.countActiveLogsInStage(stage)
    }

    fun countActiveLogsInActivity(stage: StageModel, activity: ActivityModel): Observable<Int> {
        return repository.countActiveLogsInActivity(stage, activity)
    }

}
