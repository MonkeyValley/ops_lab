package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SyncControlQualityRequest(@SerializedName("bulk") val bulk: List<SyncControlQuality>)
data class SyncControlQualityResponse(@SerializedName("data") val data: List<SyncControlQuality>)

data class SyncControlQuality(@SerializedName("issue_id") val issueId: Int,
                              @SerializedName("is_ok") val isOk: Boolean,
                              @SerializedName("quality_comment") val comment: String,
                              @SerializedName("activity_log_groove_id") val activityLogGrooveId: Int,
                              @SerializedName("offline_id") val id: Long,
                              @SerializedName("status") val status: Boolean = false,
                              @Expose(serialize = false, deserialize = false) val collaboratorId: Int)

