package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.model.BoxCountResponse
import com.amplemind.vivasmart.core.repository.model.CountBoxRequest
import io.reactivex.Observable
import retrofit2.http.*

interface CountBoxApi {

    @GET("v2/packingline_paysheet/packing")
    @Headers("Content-Type: application/json")
    fun getCountBox(
            @Header("Authorization") authentication: String,
            @Query("embed") embed : String = "packing",
            @Query("packingline_id") packingline_id : Int
    ): Observable<BoxCountResponse>

    @POST("v2/packingline_paysheet/boxcount/{line_packing}")
    @Headers("Content-Type: application/json")
    fun sendCountBox(
            @Header("Authorization") authentication: String,
            @Path("line_packing") line_packing: Int,
            @Body body: CountBoxRequest
    ): Observable<Any>

}
