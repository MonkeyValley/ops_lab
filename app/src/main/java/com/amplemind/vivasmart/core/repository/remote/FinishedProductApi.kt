package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.model.FinishedProductReportModel
import com.amplemind.vivasmart.core.repository.response.*
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ReacibaQualityReviewViewModel
import io.reactivex.Observable
import retrofit2.http.*

interface FinishedProductApi {

    @GET("/v2/finished_product_day")
    @Headers("Content-Type: application/json")
    fun getReportList(
            @Header("Authorization") authentication: String,
            @Query("day") day: String,
            @Query("business_unit_id") businessId: String
    ): Observable<List<FinishedProductResult>>

    @GET("/v2/packing")
    @Headers("Content-Type: application/json")
    fun getUnitCropList(
            @Header("Authorization") authentication: String,
            @Query("crop_id") crop_id: Int,
            @Query("__logic") __logic: String
    ): Observable<CropUnitResponse>

    @GET("/v2/client")
    @Headers("Content-Type: application/json")
    fun getClients(
            @Header("Authorization") authentication: String
    ): Observable<ClientResponse>

    @GET("/v2/stage")
    @Headers("Content-Type: application/json")
    fun getLots(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String,
            @Query("business_unit_id") business_unit_id: String,
            @Query("crop_id") crop_id: Int,
            @Query("is_active") is_active: Boolean,
            @Query("__logic") __logic: String
    ): Observable<LotsResponse>

    //https://qa-ops.vivasmart.com.mx/v2/unit_list?business_unit_id=2
    @GET("/v2/unit")
    @Headers("Content-Type: application/json")
    fun getUnits(
            @Header("Authorization") authentication: String,
            @Query("business_unit_id") business_unit_id: String
    ): Observable<UnitResponse>

    @GET("/v2/issue")
    @Headers("Content-Type: application/json")
    fun getIssues(
            @Header("Authorization") authentication: String,
            @Query("category_id") category_id: Int,
            @Query("crops_id__contains") crops_id__contains: Int,
            @Query("__logic") __logic: String
    ): Observable<IssuesResponse>

    @POST("v2/quality_finished_product")
    @Headers("Content-Type: application/json")
    fun postFinishedProduct(
            @Header("Authorization") authentication: String,
            @Body body: FinishedProductReportModel
    ): Observable<FinishedProductReportModel>

    @POST("v2/quality_carry_order")
    @Headers("Content-Type: application/json")
    fun postRecibaQualityReview(
            @Header("Authorization") authentication: String,
            @Body body: ReacibaQualityReviewViewModel
    ): Observable<ReacibaQualityReviewViewModel>

    @GET("/v2/quality_finished_product/{idReport}")
    @Headers("Content-Type: application/json")
    fun getReportDetail(
            @Header("Authorization") authentication: String,
            @Path("idReport") idReport: Int,
            @Query("embed") embed: String
    ): Observable<FinishedProductReportModel>

}
