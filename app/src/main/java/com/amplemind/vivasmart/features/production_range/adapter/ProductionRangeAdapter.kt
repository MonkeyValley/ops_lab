package com.amplemind.vivasmart.features.production_range.adapter

import android.app.TimePickerDialog
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.utils.CATEGORY_ID
import com.amplemind.vivasmart.core.utils.DecimalDigitsInputFilter
import com.amplemind.vivasmart.databinding.ItemProductionRageBinding
import com.amplemind.vivasmart.features.reciba_quality.model.VariatyItem
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.PublishSubject
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_production_range.*
import kotlinx.android.synthetic.main.activity_production_range.view.*
import kotlinx.android.synthetic.main.item_production_rage.view.*
import java.util.*
import kotlin.collections.ArrayList


class ProductionRangeAdapter : (RecyclerView.Adapter<ProductionRangeAdapter.ProductionRangeViewHolder>)() {

    var copy = false
    var disale = false
    var lotId = 0
    var scroll = false

    private var list = listOf<ProductionRangeVariatiesModel>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    val listVarieties = ArrayList<VariatyItem>()

    fun addElements(data: List<ProductionRangeVariatiesModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductionRangeViewHolder {
        val binding = ItemProductionRageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductionRangeViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ProductionRangeViewHolder, position: Int) {
        holder.bind(list[position])

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class ProductionRangeViewHolder(private val binding: ItemProductionRageBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ProductionRangeVariatiesModel

        fun bind(item: ProductionRangeVariatiesModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            if(!scroll) setCategoryName(binding, item.activitySubCategoryId)
            if(disale) {
                binding.btnCopy.isEnable(false)
                binding.etW2DataSpinner3.isEnable(false)
                binding.etData11.isEnable(false)
                binding.etData1.isEnable(false)
                binding.etData2.isEnable(false)
            }

            binding.tvVariety.text = item.name
            item.productionVarId = item.id.toString()

            if (item.options!!.size != 0) {
                setUpSpinner(binding, item.options!!)
            } else {
                when (item.productionVarType!!.id) {
                    1 -> {
                        setUpUnicValue(binding, item.productionVarDataType)
                    }
                    2 -> {
                        setUpMultiValue(binding, item.productionVarDataType)
                    }
                    else -> {
                    }
                }
            }

        }

        private fun setCategoryName(binding: ItemProductionRageBinding, activitySubCategoryId: Int?) {
            if (CATEGORY_ID != activitySubCategoryId) {
                CATEGORY_ID = activitySubCategoryId!!
                Realm.getDefaultInstance().use {
                    val categoryName = it
                            .where(ActivitySubCategory::class.java)
                            .equalTo("id", activitySubCategoryId)
                            .findFirst()?.name ?: ""

                    binding.tvCategory.text = categoryName
                    binding.linearLayout0.visibility = View.VISIBLE
                }
            }
        }

        private fun setUpSpinner(binding: ItemProductionRageBinding, options: RealmList<ProductionRangesOption>) {
            binding.linearLayout3.visibility = View.VISIBLE
            listVarieties.clear()
            listVarieties.add(VariatyItem(0, "SELECCIONAR"))
            for (item in options) {
                var obj = VariatyItem(item.id, item.name)
                listVarieties.add(obj)
            }
            binding.etW2DataSpinner1.adapter = ArrayAdapter<VariatyItem>(binding.root.context, R.layout.spinner_item_textview, listVarieties)
            binding.etW2DataSpinner1.isEnable(false)
            binding.etW2DataSpinner2.adapter = ArrayAdapter<VariatyItem>(binding.root.context, R.layout.spinner_item_textview, listVarieties)
            binding.etW2DataSpinner2.isEnable(false)
            binding.etW2DataSpinner3.adapter = ArrayAdapter<VariatyItem>(binding.root.context, R.layout.spinner_item_textview, listVarieties)

            setLastWeekData(binding, 1, lotId, item.id!!)

            binding.btnCopy.setOnClickListener {
                binding.etW2DataSpinner3.setSelection(binding.etW2DataSpinner2.selectedItemPosition)
                item.productionVarOptionId = binding.etW2DataSpinner3.selectedItemPosition.toString()
            }

            if (copy) {
                binding.etW2DataSpinner3.setSelection(binding.etW2DataSpinner2.selectedItemPosition)
                item.productionVarOptionId = listVarieties[binding.etW2DataSpinner2.selectedItemPosition].id.toString()
            }

            binding.etW2DataSpinner3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    item.productionVarOptionId = listVarieties[position].id.toString()
                }
            }


            item.mode = "1"
        }

        private fun setUpMultiValue(binding: ItemProductionRageBinding, type: ProductionVarDataType?) {
            binding.linearLayout1.visibility = View.VISIBLE
            when (type!!.id) {
                1 -> {
                    setUpHourEvent(binding.etData1)
                    setUpHourEvent(binding.etData2)
                }
                2 -> {
                    setUpNumberOnly(binding.etData1)
                    setUpNumberOnly(binding.etData2)
                }
                3 -> {
                    setUpNumberWithDecimal(binding.etData1)
                    setUpNumberWithDecimal(binding.etData2)
                }
                4 -> {
                    setUpHourEvent(binding.etData1)
                    setUpHourEvent(binding.etData2)
                }
                5 -> {
                    setUpNumberOnly(binding.etData1)
                    setUpNumberOnly(binding.etData2)
                }
                6 -> {
                    setUpNumberWithDecimal(binding.etData1)
                    setUpNumberWithDecimal(binding.etData2)
                }
                7 -> {
                }
            }

            setLastWeekData(binding, 2, lotId, item.id!!)

            binding.btnCopy.setOnClickListener {
                binding.etData1.setText(binding.etW1Data1.text.toString())
                binding.etData2.setText(binding.etW1Data2.text.toString())
                item.minVal = binding.etW1Data1.text.toString()
                item.maxVal = binding.etW1Data2.text.toString()
            }

            if (copy) {
                binding.etData1.setText(binding.etW1Data1.text.toString())
                binding.etData2.setText(binding.etW1Data2.text.toString())
                item.minVal = binding.etW1Data1.text.toString()
                item.maxVal = binding.etW1Data2.text.toString()
            }

            binding.etData1.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (binding.etData1.text.isNotEmpty()) item.minVal = binding.etData1.text.toString()
                    else item.minVal = ""
                }
            })

            binding.etData2.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (binding.etData2.text.isNotEmpty()) item.maxVal = binding.etData2.text.toString()
                    else item.maxVal = ""
                }
            })

            item.mode = "2"
        }

        private fun setUpUnicValue(binding: ItemProductionRageBinding, type: ProductionVarDataType?) {
            binding.linearLayout2.visibility = View.VISIBLE
            when (type!!.id) {
                1 -> {
                    setUpHourEvent(binding.etData11)
                }
                2 -> {
                    setUpNumberOnly(binding.etData11)
                }
                3 -> {
                    setUpNumberWithDecimal(binding.etData11)
                }
                4 -> {
                    setUpHourEvent(binding.etData11)
                }
                5 -> {
                    setUpNumberOnly(binding.etData11)
                }
                6 -> {
                    setUpNumberWithDecimal(binding.etData11)
                }
                7 -> {
                }
            }

            setLastWeekData(binding, 3, lotId, item.id!!)

            binding.btnCopy.setOnClickListener {
                binding.etData11.setText(binding.etW1Data11.text.toString())
                item.value = binding.etW1Data11.text.toString()
            }

            if (copy) {
                binding.etData11.setText(binding.etW1Data11.text.toString())
                item.value = binding.etW1Data11.text.toString()
            }

            binding.etData11.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (binding.etData11.text.isNotEmpty()) item.value = binding.etData11.text.toString()
                    else item.value = ""
                }
            })

            item.mode = "3"
        }

        private fun setUpHourEvent(editText: EditText?) {
            editText!!.keyListener = null
            editText.setOnClickListener {
                val c = Calendar.getInstance()
                val hour = c.get(Calendar.HOUR_OF_DAY)
                val minute = c.get(Calendar.MINUTE)

                TimePickerDialog(binding.root.context, TimePickerDialog.OnTimeSetListener(function = { view, hour, minute ->
                    var hour = hour
                    var am_pm = ""
                    when {
                        hour == 0 -> {
                            hour += 12
                            am_pm = "am"
                        }
                        hour == 12 -> am_pm = "pm"
                        hour > 12 -> {
                            hour -= 12
                            am_pm = "pm"
                        }
                        else -> am_pm = "am"
                    }

                    val timeHour = if (hour < 10) "0" + hour else hour
                    val tiemMin = if (minute < 10) "0" + minute else minute
                    editText.setText("$timeHour:$tiemMin$am_pm")
                    item.value = ("$timeHour:$tiemMin$am_pm")

                }), hour, minute, false).show()
            }
        }

        private fun setUpNumberOnly(editText: EditText?) {
            editText!!.inputType = InputType.TYPE_CLASS_NUMBER
            editText.filters += InputFilter.LengthFilter(4)
        }

        private fun setUpNumberWithDecimal(editText: EditText?) {
            //editText!!.inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL
            editText!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        }

    }

    private fun setLastWeekData(binding: ItemProductionRageBinding, mode: Int, lotId: Int, productionVarId: Int) {
        Realm.getDefaultInstance().use {
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
            val week = calendar.get(Calendar.WEEK_OF_YEAR)

            if (mode == 1) {
                val result = it
                        .where(ProductionVar::class.java)
                        .equalTo("productionVarId", productionVarId)
                        .and()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("weekNumer", week)
                        .and()
                        .isNotNull("productionVarOptionId")
                        .distinct("productionVarId")
                        .findFirst()

                if (result != null) {
                    setSpinnerData(result.productionVarOptionId!!, binding.etW2DataSpinner3, binding.root.context)
                    binding.btnCopy.isEnable(false)
                }

            } else {
                val result = it
                        .where(ProductionVar::class.java)
                        .equalTo("productionVarId", productionVarId)
                        .and()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("weekNumer", week)
                        .distinct("productionVarId")
                        .findFirst()

                if (result != null) {
                    if (mode == 2) {
                        if(result.maxVal != null) {
                            Log.e("minVal", result.minVal)
                            Log.e("maxVal", result.maxVal)
                            binding.etData1.setText(result.minVal!!.replace("\\s".toRegex(), ""))
                            binding.etData2.setText(result.maxVal!!.replace("\\s".toRegex(), ""))
                            binding.etData1.isEnable(false)
                            binding.etData2.isEnable(false)
                            binding.btnCopy.isEnable(false)
                        }
                    } else {
                        if (result.value != null) {
                            binding.etData11.setText(result.value!!.replace("\\s".toRegex(), ""))
                            binding.etData11.isEnable(false)
                            binding.btnCopy.isEnable(false)
                        }
                    }
                }
            }

            if (mode == 1) {
                val result = it
                        .where(ProductionVar::class.java)
                        .equalTo("productionVarId", productionVarId)
                        .and()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("weekNumer", week - 1)
                        .and()
                        .isNotNull("productionVarOptionId")
                        .distinct("productionVarId")
                        .findFirst()

                if (result != null) {
                    setSpinnerData(result.productionVarOptionId!!, binding.etW2DataSpinner2, binding.root.context)
                }

            } else {
                val result = it
                        .where(ProductionVar::class.java)
                        .equalTo("productionVarId", productionVarId)
                        .and()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("weekNumer", week - 1)
                        .distinct("productionVarId")
                        .findFirst()

                if (result != null) {
                    if (mode == 2) {
                        if(result.maxVal != null) {
                            binding.etW1Data1.setText(result.minVal!!.replace("\\s".toRegex(), ""))
                            binding.etW1Data2.setText(result.maxVal!!.replace("\\s".toRegex(), ""))
                        }
                    } else {
                        if (result.value != null) {
                            binding.etW1Data11.setText(result.value!!.replace("\\s".toRegex(), ""))
                        }
                    }
                }
            }

            if (mode == 1) {
                val result = it
                        .where(ProductionVar::class.java)
                        .equalTo("productionVarId", productionVarId)
                        .and()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("weekNumer", week - 2)
                        .and()
                        .isNotNull("productionVarOptionId")
                        .distinct("productionVarId")
                        .findFirst()

                if (result != null) {
                    setSpinnerData(result.productionVarOptionId!!, binding.etW2DataSpinner1, binding.root.context)
                }

            } else {
                val result = it
                        .where(ProductionVar::class.java)
                        .equalTo("productionVarId", productionVarId)
                        .and()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("weekNumer", week - 2)
                        .distinct("productionVarId")
                        .findFirst()

                if (result != null) {
                    if (mode == 2) {
                        if(result.maxVal != null) {
                            binding.etW2Data1.setText(result.minVal!!.replace("\\s".toRegex(), ""))
                            binding.etW2Data2.setText(result.maxVal!!.replace("\\s".toRegex(), ""))
                        }
                    } else {
                        if (result.value != null) {
                            binding.etW2Data11.setText(result.value!!.replace("\\s".toRegex(), ""))
                        }
                    }
                }
            }
        }
    }

    fun setSpinnerData(productionVarOptionId: Int, spinner: Spinner, context: Context){
        val listVarietiesFind = ArrayList<VariatyItem>()
        listVarietiesFind.add(VariatyItem(productionVarOptionId, getProductionVarOpcionName(productionVarOptionId)))
        spinner.adapter = ArrayAdapter<VariatyItem>(context, R.layout.spinner_item_textview, listVarietiesFind)
        spinner.isEnable(false)
    }

    private fun getProductionVarOpcionName(productionVarOptionId: Int?) =
            Realm.getDefaultInstance().use {
                it.where(ProductionRangesOption::class.java)
                        .equalTo("id", productionVarOptionId)
                        .findFirst()
                        ?.name ?: "SELECCIONAR"
            }


}