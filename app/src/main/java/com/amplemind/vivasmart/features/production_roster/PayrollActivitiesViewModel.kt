package com.amplemind.vivasmart.features.production_roster

import android.os.Bundle
import com.amplemind.vivasmart.core.repository.PayrollActivitiesRepository
import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel
import com.amplemind.vivasmart.core.repository.model.PayrollMenuModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PayrollActivitiesViewModel @Inject constructor(private val repository: PayrollActivitiesRepository) {

    private var listMenu = mutableListOf<ItemActivitiesPayrollViewModel>()

    private var activitiesSubject = BehaviorSubject.create<List<ItemActivitiesPayrollViewModel>>()

    private var titleToolbar = BehaviorSubject.create<String>()

    private var isEven = BehaviorSubject.create<Boolean>()

    fun loadMenuActivitiesPayroll(all: Boolean): Disposable {
        return repository.getActivities(all)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelToViewModels(it) }
                .doOnNext {
                    listMenu.addAll(it)
                }.doOnComplete {
                    activitiesSubject.onNext(listMenu)
                }.subscribe()
    }

    private fun mapModelToViewModels(models: List<PayrollActivitiesModel>): List<ItemActivitiesPayrollViewModel> {
        val list = arrayListOf<ItemActivitiesPayrollViewModel>()
        for (item in models) {
            list.add(ItemActivitiesPayrollViewModel(item))
        }
        return list
    }

    fun getActivities(): BehaviorSubject<List<ItemActivitiesPayrollViewModel>> {
        return activitiesSubject
    }

    fun loadMenuActivitiesPayrollProduction(): Disposable {
        return repository.getActivitiesProduction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelToViewModels(it) }
                .doOnNext {
                    listMenu.addAll(it)
                }.doOnComplete {
                    activitiesSubject.onNext(listMenu)
                }.subscribe()
    }

    fun loadMenuPackingProduction(): Disposable {
        return repository.getActivitiesPackingProduction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelToViewModels(it) }
                .doOnNext {
                    listMenu.addAll(it)
                }.doOnComplete {
                    activitiesSubject.onNext(listMenu)
                }.subscribe()
    }


    fun loadChooseLinesActivities(): Disposable {
        return repository.getChooseActivitiesLines()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelToViewModels(it) }
                .doOnNext {
                    listMenu.addAll(it)
                }.doOnComplete {
                    isEven.onNext(searchEvenNumber(listMenu.size))
                    activitiesSubject.onNext(listMenu)
                }.subscribe()
    }

    fun loadMenuQualityProduction(): Disposable {
        return repository.getProductionQualityMenu()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelToViewModels(it) }
                .doOnNext {
                    listMenu.addAll(it)
                }.doOnComplete {
                    activitiesSubject.onNext(listMenu)
                }.subscribe()
    }

    fun searchEvenNumber(size: Int): Boolean {
        return (size % 2) == 0
    }

    fun setupData(arguments: Bundle) {
        titleToolbar.onNext(arguments.getString("Title", ""))
    }

    fun getToolbarTitle(): BehaviorSubject<String> {
        return titleToolbar
    }


}
