package com.amplemind.vivasmart.features.mipe

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.mipe.adapter.MipeTableTab
import com.amplemind.vivasmart.features.mipe.fragment.SelectGrooveFragment
import kotlinx.android.synthetic.main.activity_mipe_select_table.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.*

class MipeSelectTableActivity : BaseActivityWithFragment() {

    companion object {
        val TAG = MipeSelectTableActivity::class.simpleName
        val TAG_NAME = "$TAG.TagName"
        val LOT_ID = "$TAG.LotId"
        val LOT_NAME = "$TAG.LotName"
    }

    var lotId: String = "0"
    var lotName: String = ""
    var tagName: String = ""
    var date: String = ""
    var week: Int = 0
    lateinit var fragmentAdapter: MipeTableTab

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mipe_select_table)

        getArtguments()
        setupToolbar()
        getDateAndWeek()
        setupButton()

        fragmentAdapter = MipeTableTab(supportFragmentManager, week, lotId.toInt(), lotName)
        callTabFragment()
        tabs_mipe.setupWithViewPager(viewpager_mipe)
        tv_date_week.text = "Semana"+ week + " - " + date
    }

    fun getArtguments(){
        lotId = intent?.getStringExtra(LOT_ID) ?: "0"
        lotName = intent?.getStringExtra(LOT_NAME) ?: ""
        tagName = intent?.getStringExtra(TAG_NAME) ?: ""
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_mipe
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = lotName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        lbl_track_title.text = "/ MIPE / Monitoreo / " + lotName +" /"
    }

    fun getDateAndWeek(){
        val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        val month = if((calendar.get(Calendar.MONTH) + 1) < 10) "0" +(calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        val day  = if((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" +(calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        date = (day + "-" + month + "-" + (calendar.get(Calendar.YEAR)).toString())
        week = calendar.get(Calendar.WEEK_OF_YEAR)
    }

    fun setupButton(){
        btn_report_mipe.setOnClickListener {
                intent = Intent(this, MipeReportActivity::class.java).apply {
                    putExtra(MipeReportActivity.LOT_ID, lotId)
                    putExtra(MipeReportActivity.TAG_NAME, "MIPE")
                    putExtra(MipeReportActivity.LOT_NAME, lotName)
                    putExtra(MipeReportActivity.WEEK, week)
                }
            fab_menu.collapse()
            startActivity(intent)
        }
    }

    fun callTabFragment() {
        viewpager_mipe.adapter = fragmentAdapter
        viewpager_mipe.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                val fragment: Fragment = fragmentAdapter.getRegisteredFragment(position)!!
                (fragment as SelectGrooveFragment)
            }
        })
        tabs_mipe.setupWithViewPager(viewpager_mipe)
    }

    override fun onBackPressed() {
        finish()
    }

}
