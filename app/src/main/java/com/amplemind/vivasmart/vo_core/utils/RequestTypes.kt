package com.amplemind.vivasmart.vo_core.utils

//Types
const val TYPE_LOTS                         = "stage"
const val TYPE_COLLABORATORS                = "collaborator"
const val TYPE_ACTIVITIES                   = "activity"
const val TYPE_ACTIVITY_CODES               = "activity_code"
const val TYPE_ACTIVITY_CODE_COLLABORATORS  = "activity_code_collaborator"
const val TYPE_ACTIVITY_LOGS                = "activity_log"
const val TYPE_MESSAGE                      = "message"
const val TYPE_EXTRA_TIME                   = "EXTRA_TIME_REQUEST"


// Status
const val STATUS_NEW                = "new"
const val STATUS_OPEN               = "open"
const val STATUS_AUTH               = "authorized"
const val STATUS_DENIED             = "denied"

