package com.amplemind.vivasmart.feature

import android.content.Intent
import android.widget.VideoView
import com.amplemind.vivasmart.AndroidTest
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.login.LoginActivity
import com.amplemind.vivasmart.features.login.SetPasswordActivity
import com.amplemind.vivasmart.features.main.MainActivity
import com.amplemind.vivasmart.features.splash.SplashActivity
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.robolectric.Robolectric
import org.robolectric.Shadows

class SplashActivityTest: AndroidTest() {

    // TODO test manageDeepLinkStuff
    // TODO test video_splash.setOnCompletionListener
    // TODO Test Labels

    private var splashActivity: SplashActivity? = null

    @Before
    fun setup() {
        splashActivity = Robolectric.buildActivity(SplashActivity::class.java)
                .create()
                .resume()
                .get()
    }

    @Test
    fun `Activity should not be null`() {
        assertNotNull(splashActivity)
    }

    @Test
    fun `setContentView() inflates the correct layout`() {
        assertEquals(R.layout.content_splash_activity, splashActivity?.getLayout())
    }

    @Test
    fun `SetPasswordActivity must be called`() {
        // given
        val expectedIntent = Intent(splashActivity, SetPasswordActivity::class.java)

        // when
        splashActivity?.showPasswordActivity(anyString())
        val actualIntent = Shadows.shadowOf(splashActivity).peekNextStartedActivity()

        // then
        assertTrue(expectedIntent.filterEquals(actualIntent))
    }

    @Test
    fun `MainActivity must be called`() {
        // given
        val expectedIntent = Intent(splashActivity, MainActivity::class.java)

        // when
        splashActivity?.showMainActivity()
        val actualIntent = Shadows.shadowOf(splashActivity).peekNextStartedActivity()

        // then
        assertTrue(expectedIntent.filterEquals(actualIntent))
    }

    @Test
    fun `LoginActivity must be called`() {
        // given
        val expectedIntent = Intent(splashActivity, LoginActivity::class.java)

        // when
        splashActivity?.showLoginActivity()

        val actualIntent = Shadows.shadowOf(splashActivity).peekNextStartedActivity()

        // then
        assertTrue(expectedIntent.filterEquals(actualIntent))
    }

    @Test
    fun `video must be playing after startVideo() method`() {
        // given
        val video = splashActivity?.findViewById<VideoView>(R.id.video_splash)

        // when
        splashActivity?.startVideo()

        // then
        assert(video?.isPlaying!!)
    }

    @Test
    fun `video should be paused after pauseVideo() method`() {
        // given
        val video = splashActivity?.findViewById<VideoView>(R.id.video_splash)

        // when
        splashActivity?.pauseVideo()

        // then
        assert(!video?.isPlaying!!)
    }

}