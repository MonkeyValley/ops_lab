package com.amplemind.vivasmart.features.quality_control.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.repository.model.ControlQualityGroovesModel
import com.amplemind.vivasmart.databinding.ContentGrooveActivitiesBinding
import com.amplemind.vivasmart.databinding.ItemActivitiesControlBinding
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlActivitiesItemViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlGrooveViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.content_groove_activities.view.*
import kotlinx.android.synthetic.main.item_activities_control.view.*

class ControlActivitiesAdapter constructor(val context: Context) : RecyclerView.Adapter<ControlActivitiesAdapter.ControlActivitiesViewHolder>() {

    private var list = mutableListOf<ControlActivitiesItemViewModel>()
    private var expandState = SparseBooleanArray()

    private val clickSubject = PublishSubject.create<Pair<ControlGrooveViewModel, Int>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ControlActivitiesViewHolder {
        val binding = ItemActivitiesControlBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ControlActivitiesViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ControlActivitiesViewHolder, position: Int) {
        val itemModel = list[position]
        itemModel.setExpansable(expandState.get(position))
        holder.bind(itemModel, position)
    }

    fun addElements(data: List<ControlActivitiesItemViewModel>) {
        list.addAll(data)
        list.forEachIndexed { index, _ ->
            expandState.append(index, false)
        }
        notifyDataSetChanged()
    }

    fun clickItem(): PublishSubject<Pair<ControlGrooveViewModel, Int>> {
        return clickSubject
    }

    inner class ControlActivitiesViewHolder(private val binding: ItemActivitiesControlBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ControlActivitiesItemViewModel

        fun bind(item: ControlActivitiesItemViewModel, position: Int) {
            binding.setVariable(BR.itemView, item)
            binding.executePendingBindings()
            this.item = item
            closeGrooves()

            binding.root.item_activities.setOnClickListener { it ->
                binding.root.pb_groover.visibility = View.VISIBLE
                if (binding.root.gd_groove.childCount > 0) {
                    closeGrooves()
                } else {
                    this.item.showGrooves()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .doOnTerminate {
                                binding.root.pb_groover.visibility = View.GONE
                                binding.root.gd_groove.visibility = View.VISIBLE
                            }
                            .map {
                                mapGrooveViews(it, position)
                            }.subscribe {
                                createGroovesBinding(it)
                            }
                }
            }
        }

        private fun closeGrooves() {
            binding.root.gd_groove.removeAllViews()
            binding.root.pb_groover.visibility = View.GONE
            binding.root.gd_groove.visibility = View.GONE
        }

        private fun createGroovesBinding(views: List<View>) {
            views.forEach {
                binding.root.gd_groove.addView(it)
            }
        }

        private fun mapGrooveViews(item: List<ControlQualityGroovesModel>, position: Int): List<View> {
            val grooves = mutableListOf<View>()
            val params = LinearLayout.LayoutParams(50.toPx(), 50.toPx())
            params.setMargins(1.toPx(), 1.toPx(), 1.toPx(), 1.toPx())
            var status = 0
            item.forEach { groovesModel ->
                val binding = ContentGrooveActivitiesBinding.inflate(LayoutInflater.from(context), null, false)
                val viewModel = ControlGrooveViewModel(groovesModel)
                status++
                binding.viewGroove = viewModel
                binding.root.layoutParams = params
                binding.root.ln_groove.setOnClickListener {
                    clickSubject.onNext(Pair(viewModel, position))
                }
                grooves.add(binding.root)
            }
            return grooves
        }

    }

}
