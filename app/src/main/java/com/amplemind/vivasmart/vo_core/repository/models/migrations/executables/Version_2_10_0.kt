package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_2_10_0 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectFindingsModelrealm(realm)
        migrateUpdateObjectPlagueDetailrealm(realm)
        migrateUploadObjectPulseForTablerealm(realm)
    }

    private fun migrateUploadObjectFindingsModelrealm(realm: DynamicRealm){
        realm.schema.get("FindingsModel")?.apply {
            addField("level", Int::class.javaObjectType)
        }
    }

    private fun migrateUpdateObjectPlagueDetailrealm(realm: DynamicRealm){
        val plagueImageModelSchema = realm.schema
                .create("PlagueImageModel")
                .addField("id", Int::class.javaObjectType)
                .addField("image", String::class.javaObjectType)
                .addField("level", Int::class.javaObjectType)
                .addField("plagueId", Int::class.javaObjectType)

        realm.schema.get("PlagueDetail")?.apply {
            addRealmListField("images", plagueImageModelSchema)
            addField("version", Long::class.javaObjectType)
        }

    }

    private fun migrateUploadObjectPulseForTablerealm(realm: DynamicRealm){
        realm.schema.get("PulseForTable")?.apply {
            addField("valveId", Int::class.javaObjectType)
        }
    }

    override fun drop(realm: DynamicRealm) {

    }
}