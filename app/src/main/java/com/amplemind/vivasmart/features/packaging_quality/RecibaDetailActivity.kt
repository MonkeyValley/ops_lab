package com.amplemind.vivasmart.features.packaging_quality

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.ArrayAdapter
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.paintTables
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.haulage.viewModel.CarryOrderViewModel
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import com.amplemind.vivasmart.features.packaging_quality.dialog.RecibaAverageWeightDialog
import com.amplemind.vivasmart.features.packaging_quality.viewModel.RecibaDetailViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.SpinnerUnitsViewModel
import com.amplemind.vivasmart.features.production_quality.SignatureDialogFragment
import com.amplemind.vivasmart.features.production_roster.fragments.HarvestBoxCountByVarietyDialog
import kotlinx.android.synthetic.main.activity_reciba_detail.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.io.File
import javax.inject.Inject

class RecibaDetailActivity : BaseActivity() {

    companion object {

        const val ID_KG = 3

        val TAG = DetailOrdenActivity::class.java.simpleName

    }

    @Inject
    lateinit var viewModel: RecibaDetailViewModel

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var preferences: UserAppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reciba_detail)
        setupToolbar()

        subcribers()

        ed_size_sample.inputType = InputType.TYPE_NULL

        ed_size_sample.setOnTouchListener{ view: View, event: MotionEvent ->

            if (event.action == MotionEvent.ACTION_DOWN) {

                val dialog =
                        RecibaAverageWeightDialog.newInstance(
                                1, viewModel.cropId)

                dialog.setListener(object : RecibaAverageWeightDialog.Listener {
                    override fun onAcceptClicked(dialog: RecibaAverageWeightDialog) {
                        ed_size_sample.setText("%.1f".format(dialog.getAverageWeight()))
                        dialog.dismiss()
                    }

                    override fun onOmitClicked(dialog: RecibaAverageWeightDialog) {
                        dialog.dismiss()
                    }

                    override fun onDismissed(dialog: RecibaAverageWeightDialog) {
                    }

                })

                dialog.show(supportFragmentManager, "avgWeight")
            }

            return@setOnTouchListener false
        }
    }

    fun subcribers(){
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestSuccess().subscribe(this::onRequestSuccess).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        //viewModel.onRequestGetUnits().subscribe(this::onGetUnits).addTo(subscriptions)
        viewModel.onSuccessUpdateSign().subscribe(this::onSuccessUpdateImage).addTo(subscriptions)
        viewModel.onUpdateCarryOrder().subscribe(this::onSuccessUpdate).addTo(subscriptions)
        viewModel.onUpdateSigns().subscribe(this::onUpdateSigns).addTo(subscriptions)

        viewModel.getCarryOrder(intent.getIntExtra("id", 0))
        viewModel.getUnits().addTo(subscriptions)
        viewModel.getWeightRestriction().addTo(subscriptions)
    }

    private fun onRequestSuccess(data: CarryOrderViewModel) {
        tv_lot_name.text = String.format("%s %s", data.folio, data.lotName)
//        lbl_track_title.text = "/ Operaciones / Reciba / " +  vm.folio + " - " + vm.lotName
        tv_crop_name.text = data.cropName
        tv_box_size.text = data.boxSize
        tv_type.text = data.type
        tv_supervisor.text = data.supervisorName
        tv_variety.text = data.variety
        tv_size.text = data.size
        tv_date.text = data.getDate()
        tv_temp.text = data.temperature
        tv_comment.text = data.comment

        viewModel.cropId = data.cropId

        ln_tables.paintTables(data.table)
        //ed_size_sample.isEnabled = false

        tv_box_size.apply {
            isFocusable = false
            isClickable = true
            isLongClickable = false

            addThrottle()
                    .subscribe {
                        HarvestBoxCountByVarietyDialog.newInstance(viewModel.varieties, viewModel.newBoxCounts, viewModel.newSecondQuality, false, true)
                                .onBoxCountResult { list, secondQuality, total ->
                                    findViewById<TextView>(R.id.tv_box_size).setText("%.1f".format(total))
                                    viewModel.newBoxCounts = list
                                    viewModel.newBoxCounts.forEach {
                                        Log.e("BOX COUNT", "${it.varietyId}, ${it.boxCount}")
                                    }
                                    viewModel.newSecondQuality = secondQuality
                                    viewModel.newTotalBoxes = total
                                }
                                .show(supportFragmentManager, HarvestBoxCountByVarietyDialog.TAG)
                    }
        }

        /*RxView.clicks(tv_box_size)
                .throttleFirst(2L, TimeUnit.SECONDS)
                .subscribe{
                    HarvestBoxCountByVarietyDialog.newInstance(viewModel.varieties, vm.boxCounts, vm.secondQualityCount, true)
                            .show(supportFragmentManager, HarvestBoxCountByVarietyDialog.TAG)
                }
                .addTo(subscriptions)*/
    }



    private fun onGetUnits(list: List<SpinnerUnitsViewModel>) {
        val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item,
                list)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_units.adapter = spinnerAdapter
    }

    private fun onSuccessUpdate(success: Boolean) {
        viewModel.updateSigns(intent.getIntExtra("id", 0)).addTo(subscriptions)
    }

    private fun getBoxNo(): Double {
        //return if (switch_size.isChecked) {
          return  tv_box_size.text.toString().toDouble() //+ (if (switch_halfbox.isChecked) 0.5 else 0.0)
        //}
        //else viewModel.carryOrderBoxSize
    }

    private fun onSuccessUpdateImage(files: MutableList<File>) {
        if (files.isNotEmpty()) {
            viewModel.uploadSign(files).addTo(subscriptions)
        } else {
            val boxNo = getBoxNo()
            val weight = ed_size_sample.text.toString().toDoubleOrNull() ?: 0.0
            val unitId = ID_KG//viewModel.getUnitId(spinner_units.selectedItemPosition)
            val temperature = ed_temperature.text.toString().toDouble()
            viewModel.updateCarryOrder(viewModel.newTotalBoxes, viewModel.newBoxCounts, viewModel.newSecondQuality, weight, temperature, unitId, intent.getIntExtra("id", 0)).addTo(subscriptions)
        }
    }

    private fun onUpdateSigns(success: Boolean) {
        val i = Intent()
        i.putExtra("id", intent.getIntExtra("id", 0))
        setResult(Activity.RESULT_OK, i)
        finish()
    }

    /**
     *  change title in toolbar
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_detail
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Reciba"

        tv_box_size.isEnabled = true
        switch_halfbox.isEnabled = false

        switch_halfbox.setOnCheckedChangeListener{ _, checked ->
            tv_halfbox.visibility = if (checked) View.VISIBLE else View.GONE
        }

//        switch_size.setOnCheckedChangeListener { _, checked ->
//            ed_size.isEnabled = checked
//            switch_halfbox.isEnabled = checked
//        }

        btn_finish.addThrottle().subscribe {
            if (viewModel.isValidForm(tv_box_size.text.toString(), true, ed_size_sample.text.toString(), spinner_units.selectedItemPosition, ed_temperature.text.toString())) {
                showSignature()
            }
        }.addTo(subscriptions)
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(this)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(this, bitmap), image, 400f, 400f)
        return image
    }

    private fun showSignature() {
        val dialog = SignatureDialogFragment().newInstance(
                "Verificar reciba",
                "¿Está seguro de querer enviar este reporte? Por favor firme para confirmar.",
                false,
                arrayOf("Firma 1 (Operador):", "Firma 2 (Jefe reciba):"),
                arrayOf("Cancelar", "Cancelar"),
                arrayOf("Siguiente", "Enviar"),
                mode = 0)
        dialog.show(supportFragmentManager, SignatureDialogFragment.TAG)

        dialog.setOnCompleteListener(object : OnSignatureCompleteListener {
            override fun onDismiss() {
            }

            override fun completeSign(sign1: Bitmap, sign2: Bitmap?) {
                val signs = mutableListOf<File>()
                signs.add(compressImage(sign1))
                if (sign2 != null) {
                    signs.add(compressImage(sign2))
                }
                viewModel.uploadSign(signs).addTo(subscriptions)
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
