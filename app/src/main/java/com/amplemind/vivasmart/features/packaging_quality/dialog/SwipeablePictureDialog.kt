package com.amplemind.vivasmart.features.packaging_quality.dialog


import android.net.Uri
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.vo_features.custom.listeners.SwipeDismissTouchListener
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_swipable_picture_dialog.*
import javax.inject.Inject

class SwipeablePictureDialog : BaseDialogFragment() {

    companion object {

        private val TAG = SwipeablePictureDialog::class.java.simpleName

        private val PARAM_PICTURE_URL: String = "$TAG.PictureUrl"
        private val PARAM_TITLE: String = "$TAG.Title"
        private val PARAM_OPTIONS: String = "$TAG.Options"

        fun newInstance(pictureUrl: Uri, title: String? = null, options: Bundle? = null) =
                SwipeablePictureDialog().apply {
                    arguments = Bundle().apply {
                        putParcelable(PARAM_PICTURE_URL, pictureUrl)
                        putString(PARAM_TITLE, title)
                        putBundle(PARAM_OPTIONS, options)
                    }
                }
    }

    @Inject
    lateinit var mEventBus: EventBus

    private lateinit var mPictureUrl: Uri
    private var mTitle: String? = null
    var mOptions: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        readArguments()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_swipable_picture_dialog, container, false)
    }

    fun readArguments() {
        requireNotNull(arguments){ "$TAG: Arguments are required" }.apply {
            mPictureUrl = requireNotNull( getParcelable(PARAM_PICTURE_URL) ) { "$TAG: Argument PARAM_PICTURE_URL is required"}
            mTitle = getString(PARAM_TITLE)
            mOptions = getBundle(PARAM_OPTIONS)
        }
    }

    override fun setUpUICallbacks() {
        super.setUpCallbacks()
        dialog!!.window?.apply {
            SwipeDismissTouchListener(decorView, Unit, object : SwipeDismissTouchListener.DismissCallbacks {
                override fun canDismiss(token: Any): Boolean {
                    return true
                }

                override fun onDismiss(view: View, direction: SwipeDismissTouchListener.Direction, token: Any) {
                    mEventBus.send(OnSwipedEvent(this@SwipeablePictureDialog, direction, mOptions))
                    dismiss()
                }

            })
        }
    }

    override fun setUpUI() {
        super.setUpUI()
        context?.apply {
            Glide.with(this)
                    .load(mPictureUrl)
                    .into(iv_picture)
                    .waitForLayout()
        }
    }

    override fun onResume() {
        super.onResume()
        mTitle?.apply {
            Snackbar
                    .make(iv_picture, this, Snackbar.LENGTH_LONG)
                    .show()
        }
    }

    data class OnSwipedEvent (
            val dialog: SwipeablePictureDialog,
            val direction: SwipeDismissTouchListener.Direction,
            val options: Bundle?
    )


}
