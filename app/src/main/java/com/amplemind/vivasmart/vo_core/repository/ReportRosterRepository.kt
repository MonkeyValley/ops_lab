package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.StagesDao
import javax.inject.Inject

class ReportRosterRepository @Inject constructor(
        private val mActivityLogsDao: ActivityLogsDao,
        private val mStagesDao: StagesDao
): Repository() {

    override fun cleanUp() {
        mActivityLogsDao.cleanUp()
        mStagesDao.cleanUp()
    }

    fun hasPeopleWorking(stageUuid: String, stageTypeId: Int) =
            mActivityLogsDao.hasPeopleWorking(stageUuid, stageTypeId)

    fun hasPeopleWorkingDailyPay(stageUuid: String, stageTypeId: Int) =
            mActivityLogsDao.hasPeopleWorkingDailyPay(stageUuid, stageTypeId)

    fun hasUnsignedLogs(stageUuid: String, stageTypeId: Int) =
            mActivityLogsDao.hasUnsignedLogs(stageUuid, stageTypeId)

    fun hasDailyPayCollaboratorsPending(stageUuid: String, stageTypeId: Int) =
            mActivityLogsDao.hasDailyPayCollaboratorsPending(stageUuid, stageTypeId)

    fun hasUnsignedLogsDailyPay(stageUuid: String, stageTypeId: Int) =
            mActivityLogsDao.hasUnsignedLogsDailyPay(stageUuid, stageTypeId)

    fun setAsDone(stageUuid: String, stageTypeId: Int) =
            mActivityLogsDao.setAsDone(stageUuid, stageTypeId)
                    .flatMap {
                        closeStage(stageUuid, stageTypeId)
                    }

    fun setAsDoneDailyPay(stageUuid: String, stageTypeId: Int) =
            mActivityLogsDao.setAsDoneDailyPay(stageUuid, stageTypeId)
                    /*.flatMap {
                        closeStage(stageUuid)
                    }*/

    fun closeStage(stageUuid: String, stageTypeId: Int) =
            mStagesDao.isClosed(stageUuid, true, stageTypeId)
}