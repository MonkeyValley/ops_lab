package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UnitModel
import com.amplemind.vivasmart.core.repository.remote.PackagingQualityApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReceptionQualitySampleRepository @Inject constructor(
        private val mPreferences: UserAppPreferences,
        private val mPackagingQualityApi: PackagingQualityApi
) : Repository() {

    override fun cleanUp() {
    }

    fun loadUnits(): Observable<List<UnitModel>> =
            mPackagingQualityApi.getUnits(mPreferences.authorizationToken, businessUnitId = mPreferences.user.businessUnitId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map {response ->
                        response.data
                    }

    fun loadCarryOrder(carryOrderId: Long) =
            mPackagingQualityApi.getItemCarryOrder(mPreferences.authorizationToken, id = carryOrderId.toInt(), businessUnitId = mPreferences.user.businessUnitId.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map {response ->
                        response.data.first()
                    }


}