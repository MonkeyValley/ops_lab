package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.extensions.showTrainingMesssage
import com.amplemind.vivasmart.core.utils.COLLABORATORS
import com.amplemind.vivasmart.core.utils.LOCAL_BROADCAST_SIGN_USER
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import com.amplemind.vivasmart.features.production_quality.SignatureDialogFragment
import com.amplemind.vivasmart.features.production_roster.adapters.package_roster.DetailRosterPackageAdapter
import com.amplemind.vivasmart.features.production_roster.adapters.package_roster.RosterDetailPackageViewModel
import kotlinx.android.synthetic.main.content_roster_detail_package_activity.*
import javax.inject.Inject

class RosterDetailPackageActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: DetailPackageViewModel

    @Inject
    lateinit var picturesHelper: PicturesHelper

    private var status = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_roster_detail_package_activity)

        setupToolbar()
        initRecycler()

        viewModel.intentData(intent)

        addListenerEvents()

        loadReportDetail()

        if (viewModel.isReview()) {
            sw_training_pa.isEnabled = false
        }

        sw_training_pa.setOnCheckedChangeListener { switch, isChecked ->
            if (!viewModel.isReview()){
                root_roster_detail_pa.showTrainingMesssage(isChecked)
            }
            changeMoney(isChecked)
        }
    }

    private fun changeMoney(isChecked: Boolean) {
        viewModel.updateIsTrainingSalary(isChecked)
        viewModel.calculateSalary()
        (rv_detail_package.adapter as DetailRosterPackageAdapter).notifyDataSetChanged()
        if (isChecked && status) {
            showSignature((rv_detail_package.adapter as DetailRosterPackageAdapter).getActivitiesId(), isChecked)
        }
    }


    private fun loadReportDetail() {
        if (viewModel.isSyncFlow()) {
            viewModel.loadLocalRosterDetailActivitiesLines(true).addTo(subscriptions)
            return
        }

        if (true) {
            viewModel.loadRosterDetail().addTo(subscriptions)
        } else {
            viewModel.loadLocalRosterDetailActivitiesLines().addTo(subscriptions)
        }
    }

    private fun addListenerEvents() {
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showSign().subscribe(this::showSignButton).addTo(subscriptions)
        viewModel.getName().subscribe(this::setName).addTo(subscriptions)
        viewModel.getRosterDetail().subscribe(this::setData).addTo(subscriptions)
        //viewModel.updateItem().subscribe(this::updateItem).addTo(subscriptions)
        viewModel.getTotalSalary().subscribe(this::setTotal).addTo(subscriptions)
        viewModel.sendSignSuccess().subscribe(this::onSignCollaborator).addTo(subscriptions)
        viewModel.isTraining().subscribe(this::isTraining).addTo(subscriptions)
        viewModel.onRefressData().subscribe {
            viewModel.loadRosterDetail().addTo(subscriptions)
        }.addTo(subscriptions)
    }

    private fun setName(name: String) {
        tv_name_pa.text = name
    }

    private fun setTotal(total: String) {
        tv_total_salary_pa.text = total
    }

    private fun onSignCollaborator(success: Boolean) {
        if (success) {
            val id = intent!!.getIntExtra("id_collaborator", 0)
            val broadcast = Intent(LOCAL_BROADCAST_SIGN_USER)
            broadcast.putExtra(COLLABORATORS, id)
            finish()
            LocalBroadcastManager.getInstance(this@RosterDetailPackageActivity).sendBroadcast(broadcast)
        }
    }

    /*
    private fun updateItem(item: UpdateResponse) {
        (rv_detail_package.adapter as DetailRosterPackageAdapter).onChangeItem(item)
        viewModel.calculateSalary()
        hideKeyboard()
    }

     */

    private fun showSignButton(show: Boolean) {
        status = show
        if (show) {
            tv_sing_pa.visibility = View.VISIBLE
            tv_sing_pa.setOnClickListener {
                showSignature((rv_detail_package.adapter as DetailRosterPackageAdapter).getActivitiesId())
            }
        }
    }

    private fun setData(reports: MutableList<RosterDetailPackageViewModel>) {
        rv_detail_package.adapter = null

        val adapter = DetailRosterPackageAdapter(reports)
        rv_detail_package.adapter = adapter
        adapter.onClickReport().subscribe(this::editElement).addTo(subscriptions)

        if (!viewModel.isTrainingEnabled() || viewModel.isSyncFlow()) {
            rl_training_container_pa.visibility = View.GONE
        }

        viewModel.isAllTraining().addTo(subscriptions)

        viewModel.getCollasePositions().forEach { position ->
            adapter.onGroupClick(position)
        }
    }

    fun isTraining(value: Boolean) {
        sw_training_pa.isChecked = value
        sw_training_pa.isEnabled = status
        viewModel.calculateSalary()
    }

    private fun editElement(item: ItemRosterDetailViewModel) {
        showDialogEditUnity(item)
    }

    private fun showDialogEditUnity(item: ItemRosterDetailViewModel) {
        /*
        val dialog = EditQuantityFragment.newInstance(item, intent.getIntExtra("type", 0), false)
        dialog.show(supportFragmentManager, EditQuantityFragment.TAG)
        dialog.setOnEditQuantity(object : OnEditQuantity {
            override fun onCancel() {
                hideKeyboard()
                dialog.dismiss()
            }

            override fun onEditItem(cant: Double, time: Int) {
                if (isOnlineMode(this@RosterDetailPackageActivity)) {
                    mViewModel.editUnitOnline(cant, time, item.id_log!!, item.position, dialog).addTo(subscriptions)
                } else {
                    mViewModel.editUnit(cant, time, item.id_log!!, item.position, dialog).addTo(subscriptions)
                }
            }
        })
        */
    }

    private fun showSignature(id_activities: List<Int>, isChecked: Boolean = false) {
        val mjs = if (!isChecked) "¿Está seguro de querer enviar este reporte? Por favor firme para confirmar." else "¿Está seguro de querer enviar este reporte como capacitación? Por favor firme para confirmar."
        val dialog = SignatureDialogFragment().newInstance("Enviar reporte de nómina", mjs, true, total = tv_total_salary_pa.text.toString(), name = tv_name_pa.text.toString(), mode = 0)
        dialog.isCancelable = false
        dialog.show(supportFragmentManager, SignatureDialogFragment.TAG)
        dialog.setOnCompleteListener(object : OnSignatureCompleteListener {
            override fun onDismiss() {
                if (isChecked) {
                    sw_training_pa.isChecked = false
                    changeMoney(false)
                }
            }

            override fun completeSign(sign1: Bitmap, sign2: Bitmap?) {
                val image = picturesHelper.createImageFile(dialog.context!!)
                picturesHelper.compressImage(picturesHelper.createBitmapToFile(dialog.context!!, sign1), image, 400f, 400f)
                if (true) {
                    viewModel.uploadSign(image, id_activities, sw_training_pa.isChecked)
                } else {
                    viewModel.signLocalCollaborator(image, id_activities, sw_training_pa.isChecked).addTo(subscriptions)
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initRecycler() {
        rv_detail_package.hasFixedSize()
        rv_detail_package.layoutManager = LinearLayoutManager(this)
        rv_detail_package.itemAnimator = DefaultItemAnimator()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_report_detail_package
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.title_roster_detail)
    }

    private fun hideKeyboard() {
        Handler().postDelayed({
            hideKeyboard(this)
        }, 500)
    }

}