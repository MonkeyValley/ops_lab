package com.amplemind.vivasmart.features.sync_forced.repository.remote

import com.amplemind.vivasmart.features.pollination.repository.responses.PollinationObjectResponse
import com.amplemind.vivasmart.features.pollination.repository.responses.Pollinationrevisions
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.responses.ActivityLogsResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectSingleResponse
import io.reactivex.Single
import retrofit2.http.*

interface SyncForcedApi {

    @GET("/v2/collaborator")
    @Headers("Content-Type: application/json")
    fun getCollaborators(
            @Header("Authorization")                authentication: String,
            @Query("limit")                         limit: Int = 100000,
            @Query("business_unit_id")              businessUnitId: Long,
            @Query("version__gt")                   version: Long? = null,
            @Query("__logic")                       logic: String = "AND"
    ): Single<ObjectResponse<CollaboratorModel>>

    @GET("/v2/activity/sync")
    @Headers("Content-Type: application/json")
    fun getActivities(
            @Header("Authorization")    authentication: String,
            @Query("embed")             embed: String = "crops,unit",
            @Query("activity_area_id")  activityCategoryId: Long = 1,
            @Query("__logic")           logic: String = "AND",
            @Query("business_unit_id")  businessUnitId: Long,
            @Query("version__gt")       version: Long? = null
            //@Query("is_active")         isEnabled: Boolean = true,
            //@Query("limit")             limit: Int = 100000
    ): Single<ObjectResponse<ActivityModel>>

    @GET("/v2/stage")
    @Headers("Content-Type: application/json")
    fun getStages(
            @Header("Authorization")        authentication: String,
            @Query("embed")                 embed: String = "crop,lot,lot.business_unit,varieties," +
                    "varieties.variety,crop.plagues,crop.plagues.plague,crop.plagues.plague.images",
            @Query("lot.business_unit_id")  businessUnitId: Long,
            @Query("lot.is_active")         isEnabled: Boolean = true,
            @Query("is_active")             stageIsEnabled: Boolean = true,
            @Query("stage_activities")      stageActivities: Boolean = true,
            @Query("order_by")              order: String = "lot:name.asc",
            @Query("__logic")               logic: String = "AND",
            @Query("version__gt")           version: Long? = null,
            @Query("limit")                 limit: Int = 100000
    ): Single<ObjectResponse<StageModel>>

    //https://dev.vivasmart.com.mx/v2/plague?embed=images,crops&version__gte=5552053
    @GET("/v2/plague")
    @Headers("Content-Type: application/json")
    fun getPlagues(
            @Header("Authorization")        authentication: String,
            @Query("embed")                 embed: String = "images,crops",
            @Query("version__gte")          version: Long? = null
    ): Single<ObjectResponse<PlagueDetail>>

    @GET("v2/activity_code_totals/{stage_id}")
    @Headers("Content-Type: application/json")
    fun getCodes(
            @Header("Authorization")    authentication: String,
            @Path("stage_id")           stageId: Long,
            @Query("closed_from")       closedFrom: String,
            @Query("version__gt")       version: Long? = null,
            @Query("limit")             limit: Int = 100000,
            @Query("__logic")           logic: String = "AND"
    ): Single<List<ActivityCodeModel>>

    @GET("/v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun getActivityLogsForCode(
            @Header("Authorization")            authentication: String,
            @Query("activity_code_id")          activityCodeId: Long,
            @Query("date__gte")                 sinceDate: String? = null,
            @Query("__logic")                   logic: String = "AND",
            @Query("limit")                     limit: Int = 100000,
            @Query("version_gt")                version: Long? = null
    ): Single<ActivityLogsResponse>

    @GET("/v2/activity_code/grooves/{activity_code_id}")
    @Headers("Content-Type: application/json")
    fun getGrooves(
            @Header("Authorization")            authentication: String,
            @Path("activity_code_id")           activityCodeId: Long
    ): Single<List<GrooveModel>>

    //https://dev.vivasmart.com.mx/v2/production_var?embed=production_var_type,production_var_data_type,soil_type,options&business_unit_id=1
    @GET("/v2/production_var")
    @Headers("Content-Type: application/json")
    fun getProductionRangeVarieties(
            @Header("Authorization")    authentication: String,
            @Query("embed") embed : String = "production_var_type,production_var_data_type,soil_type,crops.crop,options,activity_sub_category",
            @Query("business_unit_id") businessId: Long
    ): Single<ObjectResponse<ProductionRangeVariatiesModel>>

    //https://dev.vivasmart.com.mx/v2/pollination/lastweek/11
    @GET("/v2/pollination/lastweek/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getPollinationLastWeeks(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<PollinationObjectResponse<Pollinationrevisions>>

    //https://dev.vivasmart.com.mx/v2/collaborator/dailypay?business_unit_id=1&from_date=2020-10-30&version=8217787
    @GET("/v2/collaborator/dailypay")
    @Headers("Content-Type: application/json")
    fun getDailyPay(
            @Header("Authorization")    authentication: String,
            @Query("from_date") date: String,
            @Query("business_unit_id") businessId: Long,
            @Query("version") version: Long?
    ): Single<ObjectResponse<DailyPayModel>>

    //https://dev.vivasmart.com.mx/v2/collaborator/dailypay?business_unit_id=1&from_date=2020-10-30
    @GET("/v2/collaborator/dailypay")
    @Headers("Content-Type: application/json")
    fun getDailyPaySync(
            @Header("Authorization")    authentication: String,
            @Query("from_date") date: String,
            @Query("business_unit_id") businessId: Long
    ): Single<ObjectResponse<DailyPayModel>>

    //embed=lot,lot.business_unit,varieties,varieties.variety
    @GET("/v2/stage")
    @Headers("Content-Type: application/json")
    fun getStageForProductionRange(
            @Header("Authorization")        authentication: String,
            @Query("embed")                 embed: String = "lot,lot.business_unit,varieties," +
                    "varieties.variety",
            @Query("lot.business_unit_id")  businessUnitId: Long,
            @Query("lot.is_active")         isEnabled: Boolean = true,
            @Query("is_active")             stageIsEnabled: Boolean = true,
            @Query("stage_activities")      stageActivities: Boolean = true,
            @Query("order_by")              order: String = "lot:name.asc",
            @Query("__logic")               logic: String = "AND",
            @Query("version__gte")          version: Long? = null,
            @Query("limit")                 limit: Int = 100000
    ): Single<ObjectResponse<StageModel>>

}