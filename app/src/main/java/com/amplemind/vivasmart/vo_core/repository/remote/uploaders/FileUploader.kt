package com.amplemind.vivasmart.vo_core.repository.remote.uploaders

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.dao.UploadObjectDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.annotations.Uploadable
import com.google.gson.annotations.SerializedName
import java.io.File
import javax.inject.Inject

class FileUploader @Inject constructor(mPref: UserAppPreferences, mDao: UploadObjectDao): Uploader(mPref, mDao) {

    fun uploadSignature(file: File, customName: String? = null): UploadObjectModel {

        val body = FileBody(
                file = file.path,
                name = customName ?: file.nameWithoutExtension,
                type = "sign"
        )

        return buildRequest(body, POST)

    }

    @Uploadable(
            path = "upload",
            contentType = "image/jpeg"
    )
    private data class FileBody (
        @SerializedName("file") val file: String,
        @SerializedName("name") val name: String,
        @SerializedName("type") val type: String
    )

}