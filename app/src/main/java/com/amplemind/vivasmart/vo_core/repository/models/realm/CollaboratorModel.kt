    package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

    open class CollaboratorModel (
    @Index
    @Expose(serialize = false, deserialize = true)
    @SerializedName("id")              var id : Int = 0,
    @Index
    @SerializedName("name")            var name : String = "",
    @SerializedName("image")           var image : String? = null,
    @Index
    @SerializedName("employee_code")   var employeeCode: String = "",
    @SerializedName("birthdate")       var birthdate: String? = null,
    @SerializedName("is_working")      var isWorking: Boolean? = null,
    @PrimaryKey
    @SerializedName("uuid")
    override var uuid: String = getUUID(),
    @Index
    @SerializedName("version")
    var version: Long = 0,
    @Index
    var isActive: Boolean = true,
    var activityDate: String? = null,
    var activityId: Int? = null
) : RealmObject(), Identifiable

