package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.remote.PackagingQualityApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.vo_core.repository.models.realm.HarvestValidationModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.IssueModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UserModel
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HarvestValidationRepository @Inject constructor(
        mApiAuth: ApiAuthorization,
        mPreferences: UserAppPreferences,
        mGson: Gson,
        private val mPackagingQualityApi: PackagingQualityApi,
        private val mEventBus: EventBus
): Repository() {

    private val mUser = mGson.fromJson(mPreferences.userInfo, UserModel::class.java)
    private val mAuth = mApiAuth.getAuthToken(mPreferences.token)

    private var mCreateRevisionDisposable: Disposable? = null
        set(value) {
            mCreateRevisionDisposable?.dispose()
            field = value
        }

    override fun cleanUp() {
        mCreateRevisionDisposable?.dispose()
    }

    fun loadCriteria(): Observable<List<IssueModel>> =
            mPackagingQualityApi.getHarvestValidationCriteria(
                    authentication = mAuth,
                    businessUnitId = mUser.businessUnitId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map { response ->
                        response.data
                    }

    fun getHarvestValidation(carryOrderId: Long): Observable<HarvestValidationModel> =
            mPackagingQualityApi.getHarvestValidation(mAuth, carryOrderId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun createHarvestValidation(validation: HarvestValidationModel) =
        mPackagingQualityApi.createHarvestValidation(mAuth, validation)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

}