package com.amplemind.vivasmart.vo_core.repository.dao.realm

import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageTypeModel
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.Realm
import io.realm.Sort
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class StagesDao @Inject constructor(realmHolder: RealmHolder) : RealmDao<StageModel>(realmHolder, StageModel::class.java) {

    fun findAllActive(): Observable<Changes<StageModel>> =
            findAllLiveAsync { realm ->
                query(realm).sort("lot.name", Sort.DESCENDING)
                        .equalTo("isActive", true)
            }

    fun isPending(stage: StageModel, isPending: Boolean) =
            update(stage, func = { _, item ->
                item.stageActivities = isPending
            }, after = null)


    fun isClose(stage: StageModel, isClose: Boolean, stageTypeId: Int) =
            isClosed(stage.uuid, isClose, stageTypeId)

    fun isClosed(stageUuid: String, isClose: Boolean, stageTypeId: Int): Single<StageModel> {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->

                var stageResult = it.where(StageModel::class.java)
                        .equalTo("uuid", stageUuid)
                        .findFirst()

                var stageType = it.where(StageTypeModel::class.java)
                        .equalTo("idStageType", stageTypeId)
                        .and()
                        .equalTo("lotId", stageResult!!.lotId)
                        .findFirst()

                if (stageType == null) {
                    var newStageType = realm.createObject(StageTypeModel::class.java)
                            .apply {
                                lotId = stageResult!!.lotId
                                idStageType = stageTypeId
                                date = getTodayDate()
                                this.isClosed = isClose
                            }
                    realm.insert(newStageType)
                } else {
                    stageType.isClosed = isClose
                    stageType.date = getTodayDate()
                    realm.insertOrUpdate(stageType)
                }
            }
        }

        return updateByUuid(stageUuid, func = { _, item ->
            item.lot?.lastClose =
                    SimpleDateFormat("yyyy-MM-dd", Locale.US)
                            .format(Calendar.getInstance().time)
        }, after = null)
    }

    fun addLogCountAux(realm: Realm, stage: StageModel, count: Int, stageTypeId: Int) {
        stage.activityLogCount += count

        var stageType = realm.where(StageTypeModel::class.java)
                .beginGroup()
                .equalTo("lotId", stage.lotId)
                .and()
                .equalTo("idStageType", stageTypeId)
                .endGroup()
                .findFirst()

        if (stageType != null) {
            stageType.activityLogCount += count
            realm.insertOrUpdate(stageType)
        } else {
            var newStageType = realm.createObject(StageTypeModel::class.java)
                    .apply {
                        lotId = stage.lotId
                        idStageType = stageTypeId
                        date = getTodayDate()
                        isClosed = stage.isClosed
                        activityLogCount += count
                    }
            realm.insert(newStageType)
        }

        if (!stage.isManaged) {
            realm.copyToRealmOrUpdate(stage)
        }
    }

    fun isClose(stageTypeId: Int, lotId: Int) =
            Realm.getDefaultInstance().use {
                it.where(StageTypeModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("idStageType", stageTypeId)
                        .and()
                        .equalTo("date", getTodayDate())
                        .endGroup()
                        .findFirst()?.isClosed ?: false
            }

    fun getTodayDate(): String {
        val calendar = Calendar.getInstance()
        val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        return ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
    }

    fun getLotActivityCouter(lotId: Int, stageTypeId: Int) =
            Realm.getDefaultInstance().use {
                it.where(StageTypeModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("idStageType", stageTypeId)
                        .endGroup()
                        .findFirst()?.activityLogCount ?: 0
            }

}