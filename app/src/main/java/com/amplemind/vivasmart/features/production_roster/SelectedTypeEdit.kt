package com.amplemind.vivasmart.features.production_roster

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.widget.CompoundButton
import android.widget.ToggleButton
import com.amplemind.vivasmart.R

class SelectedTypeEdit : DialogFragment(), CompoundButton.OnCheckedChangeListener {

    companion object {
        val TAG = SelectedTypeEdit::class.java.simpleName
    }

    private var toggle_groove: ToggleButton? = null
    private var toggle_time: ToggleButton? = null

    private var evenListener : OnSelectedTypeEdit? = null

    fun newInstance() = SelectedTypeEdit()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_select_type_edit_dialog, null, false)
        builder.setView(mView)

        toggle_groove = mView.findViewById(R.id.btn_grooves)
        toggle_time = mView.findViewById(R.id.btn_time)

        toggle_groove!!.setOnCheckedChangeListener(this)
        toggle_time!!.setOnCheckedChangeListener(this)

        builder.setPositiveButton("Aceptar", null)
        builder.setNegativeButton("Cancelar") { _, _ ->
            dismiss()
        }

        val create = builder.create()

        create.setOnShowListener { _ ->
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if (toggle_groove!!.isChecked) {
                            evenListener!!.onEditGroove()
                        }
                        if (toggle_time!!.isChecked) {
                            evenListener!!.onEditTime()
                        }
                        dismiss()
                    }
        }

        return create
    }

    fun setListener(listener : OnSelectedTypeEdit){
        this.evenListener = listener
    }

    override fun onCheckedChanged(view: CompoundButton?, isChecked: Boolean) {
        when (view!!.id) {
            R.id.btn_grooves -> {
                if (isChecked) {
                    toggle_time!!.isChecked = false
                }
            }

            R.id.btn_time -> {
                if (isChecked) {
                    toggle_groove!!.isChecked = false
                }
            }
        }
    }

    interface OnSelectedTypeEdit{
        fun onEditGroove()
        fun onEditTime()
    }

}
