package com.amplemind.vivasmart.features.production_range.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.production_range.repository.remote.ProductionRangeApi
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangeVariatiesModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangesDataModel
import com.google.gson.Gson
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class ProductionRangeRepository @Inject constructor(private val api: ProductionRangeApi,
                                                         private val preferences: UserAppPreferences) {

    val businessUnitId = Gson().fromJson(preferences.userInfo, UserModel::class.java).businessUnitId
            ?: ""

    fun getDataForProductionRanges(lotId: Int): Single<ProductionRangesDataModel> =
            api.getProductionRangeWeeksData(
                    preferences.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

    fun getVarietiesForProductionRanges(): Single<List<ProductionRangeVariatiesModel>> =
            api.getProductionRangeVarieties(
                    preferences.authorizationToken,
                    businessId = businessUnitId.toLong())
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

}