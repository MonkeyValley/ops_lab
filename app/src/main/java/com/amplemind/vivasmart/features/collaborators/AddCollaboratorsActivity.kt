package com.amplemind.vivasmart.features.collaborators

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.SearchView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.extensions.forceFocus
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.extensions.searchViewPadding
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.HeaderModel
import com.amplemind.vivasmart.features.collaborators.fragment.CollaboratorsDataBaseFragment
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.content_add_collaborators_activity.*
import javax.inject.Inject


@SuppressLint("Registered")
open class AddCollaboratorsActivity : BaseActivityWithFragment() {

    companion object {
        val TAG = AddCollaboratorsActivity::class.simpleName

        val PARAM_ACTIVITY_CODE = "activityCode"
    }

    private val onSearchChange: BehaviorSubject<Pair<String, Boolean>> = BehaviorSubject.create()

    @Inject
    lateinit var prefer: UserAppPreferences

    lateinit var mActivityCode: ActivityCodeModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_add_collaborators_activity)

        //mActivityCode = intent.getParcelableExtra(PARAM_ACTIVITY_CODE)

        setupToolbar()

        //viewPager_collaborators.adapter = CollaboratorsViewPager(supportFragmentManager, onlySelected(), getSection())
        //tabs.setupWithViewPager(viewPager_collaborators)
        val collaboratorsFragment = CollaboratorsDataBaseFragment.newInstance(mActivityCode, onlySelected(), section = getSection())
        supportFragmentManager.beginTransaction()
                .add(R.id.frameLayout, collaboratorsFragment)
                .commit()
    }

    private fun getSection(): HeaderModel? {
        return intent.getParcelableExtra("section")
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_collaborators
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.add_users)

        search_view.findViewById<EditText>(androidx.appcompat.R.id.search_src_text).searchViewPadding()
        search_view.setOnQueryTextListener(object: SearchView.OnQueryTextListener, androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                onSearchChange.onNext(Pair(query ?: "",true))
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                onSearchChange.onNext(Pair(newText ?: "", false))
                return true
            }
        })
        forceFocus(toolbar_collaborators)
        toolbar_collaborators.postDelayed({
            hideKeyboard(this)
        }, 500)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_scanner_barcode, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun onlySelected(): Boolean {
        return intent.getBooleanExtra("onlySelected", false)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.menu_scanner -> {
                /*
                startActivity(Intent(this, ReadBarcodeActivity::class.java)
                        .putExtra(ReadBarcodeActivity.PARAM_ACTIVITY_CODE, mActivityCode)
                        .putExtra("showActivities", onlySelected())
                        .putExtra("section", getSection()))
                finish()
                */
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200){
            finish()
        }
    }

    fun onSearchQueryChange(): BehaviorSubject<Pair<String, Boolean>> {
        return onSearchChange
    }

}