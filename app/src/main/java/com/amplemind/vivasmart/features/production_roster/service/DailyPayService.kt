package com.amplemind.vivasmart.features.production_roster.service

import com.amplemind.vivasmart.vo_core.repository.models.realm.DailyPayModel
import io.realm.Realm
import java.util.*

class DailyPayService {

    fun validateCollaboratorDailyPayment(collaboratorUuid: String): Boolean{
        Realm.getDefaultInstance().use {

            val calendar = Calendar.getInstance()
            val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
            val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
            var date = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

            return it.where(DailyPayModel::class.java)
                    .equalTo("collaboratorUuid", collaboratorUuid)
                    .and()
                    .equalTo("date", date)
                    .findFirst() != null
        }
    }

}