package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.core.repository.PayrollProductionRepository
import com.amplemind.vivasmart.core.repository.model.PayrollMenuModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PayrollProductionViewModel @Inject constructor(private val repository : PayrollProductionRepository) {

    private var listMenu = mutableListOf<ItemMenuPayrollViewModel>()

    private var menuSubject = BehaviorSubject.create<List<ItemMenuPayrollViewModel>>()

    fun loadMenuActivities() : Disposable {
        return repository.getMenus()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModels)
                .doOnNext {
                    listMenu.addAll(it)
                }.doOnComplete {
                    menuSubject.onNext(listMenu)
                }.subscribe()
    }

    private fun mapModelToViewModels(models : List<PayrollMenuModel>): List<ItemMenuPayrollViewModel> {
        val list = arrayListOf<ItemMenuPayrollViewModel>()
        for (item in models){
            list.add(ItemMenuPayrollViewModel(item))
        }
        return list
    }

    fun getMenu(): BehaviorSubject<List<ItemMenuPayrollViewModel>> {
        return menuSubject
    }

}