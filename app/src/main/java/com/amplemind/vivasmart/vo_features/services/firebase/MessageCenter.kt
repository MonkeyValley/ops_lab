package com.amplemind.vivasmart.vo_features.services.firebase

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.RemoteInput
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.production_roster.ExtraHoursMessagesActivity
import com.amplemind.vivasmart.vo_core.repository.models.realm.UpdateNotificationModel
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import java.util.*

open class MessageCenter {
    private val NEW_NOTIFICATION = "nueva notificación a futuro"
    private val KEY_TEXT_REPLY = "key_text_reply"

    /**
     * Params:
     *          $remoteMessage:         Objeto que se recupera desde la propiedad Data de la notificación de Firebase
     *          $context:               Contexto del servicio o aplicación donde se manda a llamar el metodo
     *          $notificationManager:   Genera el builder del evento de la notificación en el UI thread
     * Annotations:
     */
    fun getFunctionType(remoteMessage: RemoteMessage, context: Context, notificationManager: NotificationManager) {
        //From here this fun does the addressing for each case when receiving a notification from our cloud messaging service
        val dataFire = remoteMessage.data.values
        val col: UpdateNotificationModel = Gson().fromJson(dataFire.firstOrNull(), UpdateNotificationModel::class.java)
        Log.d("NotifNotif", dataFire.firstOrNull().toString())
        //Case: Notification Message
    }

    fun ExtraTimeNotifications(context: Context, col: UpdateNotificationModel, notificationManager: NotificationManager ){
        //DownloadService.placeUpdate(context, col)
        val intent = Intent(context, ExtraHoursMessagesActivity::class.java)
        showNotificationIntent(col.type!!, col.uuid!!, context, notificationManager, intent)
        // notificationWithReply(context, intent, ExtraHoursMessagesActivity::class.java, notificationManager)
    }

    /**
     * Params:
     *          $title:                 Titulo de la notificación
     *          $body:                  Cuerpo del mensaje de la notificación
     *          $context:               Contexto del servicio o aplicación donde se manda a llamar el metodo
     *          $notificationManager:   Genera el builder del evento de la notificación en el UI thread
     * Annotations:
     */
    fun showNotification(title: String, body: String, context: Context, notificationManager: NotificationManager) {
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val uniqueNotId = Random().nextInt(9999 - 1000) + 1000

        val notificationBuilder = NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setDefaults(Notification.DEFAULT_LIGHTS)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelID = title// The id of the channel.
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(channelID, title, importance)
            // Create a notification and set the notification channel.
            val notification = notificationBuilder
                    .setChannelId(channelID)
                    .build()
            notificationManager.createNotificationChannel(mChannel)
            notificationManager.notify(uniqueNotId, notification)
        } else {
            notificationManager.notify(uniqueNotId, notificationBuilder.build())
        }
    }

    /**
     * Params:
     *          $title:                 Titulo de la notificación
     *          $body:                  Cuerpo del mensaje de la notificación
     *          $context:               Contexto del servicio o aplicación donde se manda a llamar el metodo
     *          $notificationManager:   Genera el builder del evento de la notificación en el UI thread
     *          $intent:                Actividad destino el evento touch sobre la notificación
     * Annotations:
     */
    fun showNotificationIntent(title: String, body: String, context: Context, notificationManager: NotificationManager, intent: Intent) {
        Log.d("NotifTitle", title)
        Log.d("NotifBody", body)


        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val uniqueNotId = Random().nextInt(9999 - 1000) + 1000

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(context, uniqueNotId /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val notificationBuilder = NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setContentIntent(pendingIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelID = title// The id of the channel.
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(channelID, title, importance)
            // Create a notification and set the notification channel.
            val notification = notificationBuilder
                    .setChannelId(channelID)
                    .build()
            notificationManager.createNotificationChannel(mChannel)
            notificationManager.notify(uniqueNotId, notification)
        } else {
            notificationManager.notify(uniqueNotId, notificationBuilder.build())
        }
    }
    /**
     * Params:
     *          $context:               Contexto del servicio o aplicación donde se manda a llamar el metodo
     *          $intent:                Actividad destino el evento touch sobre la notificación
     * Annotations:
     */
    fun notificationOpenActivity(context: Context, intent: Intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        // intent.putExtra("message", remoteMessage.notification.body!!)
        context.startActivity(intent)
    }

    /**
     * Params:
     *          $context:               Contexto del servicio o aplicación donde se manda a llamar el metodo
     *          $notif:                 Objeto que se recupera desde la propiedad data de una notificación
     * Annotations:
     */
    fun notificationToSyncLocal(context: Context, notif: UpdateNotificationModel) {
        //DownloadService.placeUpdate(context, notif)
    }

    /**
     * Params:
     *          $context:               Contexto del servicio o aplicación donde se manda a llamar el metodo
     *          $intent:                Actividad destino el evento touch sobre la notificación
     *          $clazz:                 Clase conjunta arraigada al intent, sirve para hacer el stackBuilder
     *          $notificationManager:   Genera el builder del evento de la notificación en el UI thread
     * Annotations:
     * Todo: Se requiere que la actividad destino tenga un evento GetSystemService, para detectar el texto y que termine la ejecución del Thread de la Notificacacion
     */
    fun <T> notificationWithReply(context: Context, intent: Intent, clazz: Class<T>, notificationManager: NotificationManager) {
        // Create the RemoteInput specifying this key
        val replyLabel = "Respuesta:"
        val remoteInput: RemoteInput = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            RemoteInput.Builder(KEY_TEXT_REPLY)
                    .setLabel(replyLabel)
                    .build()
        } else {
            return
        }

        val stackBuilder = TaskStackBuilder.create(context)
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(clazz)
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(intent)
        val uniqueNotId = 1000
// This uniqueID must to be the same as you receive in the activity
        val resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        // Add to your action, enabling Direct Reply for it
        val action = NotificationCompat.Action.Builder(R.drawable.ic_box_tomate, replyLabel, resultPendingIntent)
                .addRemoteInput(remoteInput!!)
                .setAllowGeneratedReplies(true)
                .build()

        val mBuilder = NotificationCompat.Builder(context)
                .addAction(action)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_send)
                .setContentTitle("Viva Smart dice")
                .setContentText("Esto funciona bien?")

        mBuilder.setContentIntent(resultPendingIntent)

        //Show it
        notificationManager!!.notify(uniqueNotId, mBuilder.build())
    }


}