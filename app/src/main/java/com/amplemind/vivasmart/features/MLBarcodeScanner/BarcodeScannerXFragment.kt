package com.amplemind.vivasmart.features.MLBarcodeScanner

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.amplemind.vivasmart.R
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.MLBarcodeScanner.adapter.CollaboratorSacanAdapter
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.BarcodeScannerViewModel
import javax.inject.Inject


class BarcodeScannerXFragment : BaseFragment() {

    @Inject
    lateinit var mViewModel: BarcodeScannerViewModel

    var mAdapter = CollaboratorSacanAdapter()
    var collaboratorList = ArrayList<CollaboratorModel>()
    var rvCollaboratorsScaner : RecyclerView? = null
    var llListCcan: LinearLayout? = null
    var tvScannerCount: TextView? = null
    var tvScannerName: TextView? = null

    private var mView: View? = null
    /*private var previewView: PreviewView? = null
    private var cameraProvider: ProcessCameraProvider? = null
    private var cameraSelector: CameraSelector? = null
    private var lensFacing = CameraSelector.LENS_FACING_BACK
    private var previewUseCase: Preview? = null
    private var analysisUseCase: ImageAnalysis? = null
    private var barcodeScanner: BarcodeScanner? = null*/


    /*private val screenAspectRatio: Int
        get() {
            // Get screen metrics used to setup camera for full screen resolution
            //val metrics = DisplayMetrics().also { previewView?.display?.getRealMetrics(it) }
            //return aspectRatio(metrics.widthPixels, metrics.heightPixels)
        }*/

    companion object {

        val TAG: String = BarcodeScannerXFragment::class.java.simpleName
        private const val PERMISSION_CAMERA_REQUEST = 1

        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0

        @JvmStatic
        fun newInstance() = BarcodeScannerXFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_barcode_scanner_x, container, false)
        setUpUi()
        initRecycler()
        setUpCamera()
        return mView
    }

    fun setUpUi(){
        rvCollaboratorsScaner = mView!!.findViewById(R.id.rv_collaborators_scaner)
        llListCcan = mView!!.findViewById(R.id.ll_list_scan)
        tvScannerCount = mView!!.findViewById(R.id.tv_scanner_count)
        tvScannerName = mView!!.findViewById(R.id.tv_scanner_name)
    }

    override fun onResume() {
        super.onResume()
        setUpCamera()
    }

    override fun onPause() {
        super.onPause()
        releaseCamera()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mViewModel.cleanUp()
    }

    private fun setUpCamera() {
        //previewView = mView!!.findViewById(R.id.preview_view)
        /*if(cameraSelector == null) {
            cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()
            ViewModelProvider(
                    this, ViewModelProvider.AndroidViewModelFactory.getInstance(activity!!.application)
            ).get(CameraXViewModel::class.java)
                    .processCameraProvider
                    .observe(this, Observer { provider: ProcessCameraProvider? ->
                        cameraProvider = provider
                        if (isCameraPermissionGranted()) {
                            bindCameraUseCases()
                        } else {
                            ActivityCompat.requestPermissions(
                                    activity!!,
                                    arrayOf(Manifest.permission.CAMERA),
                                    PERMISSION_CAMERA_REQUEST
                            )
                        }
                    }
                    )
        } else {
            bindCameraUseCases()
        }*/
    }

    private fun releaseCamera() {

    }

    private fun bindCameraUseCases() {
        bindPreviewUseCase()
        bindAnalyseUseCase()
    }

    private fun bindPreviewUseCase() {
        /*if (cameraProvider == null) {
            return
        }
        if (previewUseCase != null) {
            cameraProvider!!.unbind(previewUseCase)
        }

        previewUseCase = Preview.Builder()
                .setTargetAspectRatio(screenAspectRatio)
                .setTargetRotation(previewView!!.display.rotation)
                .build()
        previewUseCase!!.setSurfaceProvider(previewView!!.createSurfaceProvider())

        try {
            cameraProvider!!.bindToLifecycle(/* lifecycleOwner= */this,
                    cameraSelector!!,
                    previewUseCase
            )
        } catch (illegalStateException: IllegalStateException) {
            Log.e("BarcodeScannerX", illegalStateException.message)
        } catch (illegalArgumentException: IllegalArgumentException) {
            Log.e("BarcodeScannerX", illegalArgumentException.message)
        }*/
    }

    private fun bindAnalyseUseCase() {
        /*if(barcodeScanner == null) barcodeScanner = BarcodeScanning.getClient()

        if (cameraProvider == null) {
            return
        }
        if (analysisUseCase != null) {
            cameraProvider!!.unbind(analysisUseCase)
        }

        analysisUseCase = ImageAnalysis.Builder()
                .setTargetAspectRatio(screenAspectRatio)
                .setTargetRotation(previewView!!.display.rotation)
                .build()

        val cameraExecutor = Executors.newSingleThreadExecutor()

        analysisUseCase?.setAnalyzer(cameraExecutor, ImageAnalysis.Analyzer { imageProxy ->
            processImageProxy(imageProxy)
        })

        try {
            cameraProvider!!.bindToLifecycle(/* lifecycleOwner= */this,
                    cameraSelector!!,
                    analysisUseCase
            )
        } catch (illegalStateException: IllegalStateException) {
            Log.e("BarcodeScannerX", illegalStateException.message)
        } catch (illegalArgumentException: IllegalArgumentException) {
            Log.e("BarcodeScannerX", illegalArgumentException.message)
        }*/

    }

    /*@SuppressLint("UnsafeExperimentalUsageError")
    private fun processImageProxy(imageProxy: ImageProxy) {
        val inputImage =
                InputImage.fromMediaImage(imageProxy.image!!, imageProxy.imageInfo.rotationDegrees)

        barcodeScanner!!.process(inputImage)
                .addOnSuccessListener { barcodes ->
                    barcodes.forEach {
                        //barcode encontrado
                        Log.d("BarcodeScannerX-scan", it.rawValue)
                        mViewModel.findByCodeX(it.rawValue.toString())
                    }
                }
                .addOnFailureListener {
                    Log.e("BarcodeScannerX-error", it.message)
                }.addOnCompleteListener {
                    imageProxy.close()
                }
    }*/

    /*private fun aspectRatio(width: Int, height: Int): Int {
        val previewRatio = max(width, height).toDouble() / min(width, height)
        if (abs(previewRatio - RATIO_4_3_VALUE) <= abs(previewRatio - RATIO_16_9_VALUE)) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }*/

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_CAMERA_REQUEST) {
            if (isCameraPermissionGranted()) {
                bindCameraUseCases()
            } else {
                Log.e("BarcodeScannerX", "no camera permission")
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun isCameraPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
                activity!!.baseContext,
                Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun setUpUICallbacks() {
        mViewModel.onSuccessRequest_Scan().subscribe(this::sendScannedEvent).addTo(subscriptions)
    }

    @SuppressLint("RestrictedApi")
    private fun sendScannedEvent(collaborator: CollaboratorModel) {
        try {
            if (collaborator.isWorking == true) {
                showErrorDialog(getString(R.string.bcsf_already_working))
            } else {

                addElementsPlague(collaborator)
                mEventBus.send(OnCodeScannedEvent(
                        collaborator.uuid,
                        collaborator.employeeCode,
                        collaborator.name
                ))
            }
        } catch (e: Exception){
            Log.e("sendScannedEvent", e.toString())
        }
    }

    data class OnCodeScannedEvent(
            val uuid: String,
            val code: String,
            val name: String
    )

    private fun initRecycler() {
        rvCollaboratorsScaner!!.hasFixedSize()
        rvCollaboratorsScaner!!.layoutManager = LinearLayoutManager(activity)
        rvCollaboratorsScaner!!.itemAnimator = DefaultItemAnimator()
        rvCollaboratorsScaner!!.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        rvCollaboratorsScaner!!.adapter = mAdapter
    }

    fun addElementsPlague(data: CollaboratorModel) {
        llListCcan!!.visibility = View.VISIBLE
        tvScannerName!!.text = data.name

        collaboratorList.add(data)

        tvScannerCount!!.text = "Colaboradores escaneados - " + collaboratorList.distinctBy { it.id }.size

        Toast.makeText(activity, data.name + " se ha escaneado.", Toast.LENGTH_SHORT).show()

        mAdapter!!.addElements(collaboratorList.distinctBy { it.id })
    }

}