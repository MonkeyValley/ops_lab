package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.sync_forced.repository.remote.SyncForcedApi
import com.amplemind.vivasmart.vo_core.repository.models.realm.DailyPayModel
import io.reactivex.Single
import io.realm.RealmObject
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DailyPayDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val api: SyncForcedApi,
        private val mPref: UserAppPreferences
) : RealmDownloader<RealmObject?, DailyPayModel>(realmHolder, DailyPayModel::class.java) {


    override fun buildServerRequest(parent: RealmObject?, lastVersion: Long): Single<List<DailyPayModel>> =
            api.getDailyPaySync(
                    mPref.authorizationToken,
                    date = getTodayDate(),
                    businessId = mPref.user.businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version-DailyPay: ", lastVersion.toString())
                        Log.e("Data-DailyPay: ", response.data.toString())
                        response.data
                    }

    fun getTodayDate(): String {
        val calendar = Calendar.getInstance()
        val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        return ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
    }
}