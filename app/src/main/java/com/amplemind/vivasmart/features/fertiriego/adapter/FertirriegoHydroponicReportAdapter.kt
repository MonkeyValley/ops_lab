package com.amplemind.vivasmart.features.fertiriego.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemHydroponicReportBinding
import com.amplemind.vivasmart.features.mipe.viewModel.ItemPlagueViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PulseForTable
import io.reactivex.subjects.PublishSubject

class FertirriegoHydroponicReportAdapter : (RecyclerView.Adapter<FertirriegoHydroponicReportAdapter.FertirriegoHydroponicReportViewHolder>)() {

    var lotId = 0
    var groove = 0
    var week = 0
    var table = "0"
    var plant = 0

    fun setData(lotId: Int, groove: Int, week: Int, table: String, plant: Int) {
        this.lotId = lotId
        this.groove = groove
        this.week = week
        this.table = table
        this.plant = plant
    }

    private var list = listOf<PulseForTable>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    fun addElements(data: List<PulseForTable>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FertirriegoHydroponicReportViewHolder {
        val binding = ItemHydroponicReportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FertirriegoHydroponicReportViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: FertirriegoHydroponicReportViewHolder, position: Int) {
        holder.bind(list[position])

    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class FertirriegoHydroponicReportViewHolder(private val binding: ItemHydroponicReportBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: PulseForTable

        fun bind(item: PulseForTable) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.tvPulse.text = item.pulse.toString() + if(item.isCalculated!!) "*" else ""
            binding.tvHour.text = item.hour
            binding.tvMin.text = item.minutes.toString()
            binding.tvPorcent.text = item.drainPercent.toString()
            binding.tvCe.text = if(item.ceDrain != null) item.ceDrain.toString() else "-"
            binding.tvPh.text = if(item.phDrain != null) item.phDrain.toString() else "-"
        }
    }


}
