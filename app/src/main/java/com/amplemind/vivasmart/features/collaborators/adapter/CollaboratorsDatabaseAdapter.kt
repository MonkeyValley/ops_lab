package com.amplemind.vivasmart.features.collaborators.adapter

import android.net.Uri
import androidx.recyclerview.widget.RecyclerView
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.ItemCollaboratorsDatabaseBinding
import com.amplemind.vivasmart.databinding.ItemProgressCollaboratorsBinding
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemCollaboratorsViewModel
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemProgressMore
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import androidx.databinding.library.baseAdapters.BR
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_collaborators_database.view.*

class CollaboratorsDatabaseAdapter(val justSelectOne: Boolean, var limit_selected: Int? = null) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    enum class STATE {
        SELECTED,
        NORMAL
    }

    enum class TYPE_HOLDER(val type: Int) {
        TYPE_COLLABORATOR(1),
        TYPE_PROGRESS(2);

        companion object {
            fun from(value: Int): TYPE_HOLDER = TYPE_HOLDER.values().first { it.type == value }
        }
    }

    val list = mutableListOf<Any>()

    //listenner reactive item selected
    private val clickSubject = PublishSubject.create<ItemCollaboratorsViewModel>()

    // Detect change in scroll item current
    private var itemSubject = BehaviorSubject.create<String>()

    // saveServerResponse position in list and flag seleted
    val seletedItems = SparseBooleanArray()
    val selectedItems = mutableListOf<CollaboratorModel>()

    // saveServerResponse position in list and flag animation index
    private val animationItemsIndex = SparseBooleanArray()

    // animation flag all elements
    private var reverseAllAnimations = false

    private var isExceeded = BehaviorSubject.create<Boolean>()

    // index is used to animate only the selected row
    // dirty fix, find a better solution
    private var currentSelectedIndex = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder = when (viewType) {
            TYPE_HOLDER.TYPE_COLLABORATOR.type -> {
                val binding = ItemCollaboratorsDatabaseBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                CollaboratorsViewHolder(binding)
            }
            TYPE_HOLDER.TYPE_PROGRESS.type -> {
                val binding = ItemProgressCollaboratorsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                CollaboratorsProgressViewHolder(binding)
            }
            else -> {
                null
            }
        }
        return viewHolder!!
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewType = holder.itemViewType
        when (viewType) {
            TYPE_HOLDER.TYPE_COLLABORATOR.type -> {

                (holder as CollaboratorsViewHolder).bind(list[position] as ItemCollaboratorsViewModel)

                changeStatePreviusItem(holder)

                applyIconAnimation(holder)

                //nextLetter((list[position] as ItemCollaboratorsViewModel).name)
            }
        }
    }

    private fun changeStatePreviusItem(holder: CollaboratorsViewHolder) {
        if (seletedItems.get(holder.item!!.id, false)) {
            holder.item!!.state = STATE.SELECTED
        }
    }

    private fun applyIconAnimation(holder: CollaboratorsViewHolder) {
        when (holder.item!!.state) {
            STATE.SELECTED -> {
                holder.itemView.image_profile.visibility = View.GONE
                resetIconYAxis(holder.itemView.icon_back)
                holder.itemView.icon_back.visibility = View.VISIBLE
                holder.itemView.icon_back.alpha = 1F
            }
            STATE.NORMAL -> {
                holder.itemView.icon_back.visibility = View.GONE
                resetIconYAxis(holder.itemView.image_profile)
                holder.itemView.image_profile.visibility = View.VISIBLE
                holder.itemView.image_profile.alpha = 1F
            }
        }
    }

    private fun resetIconYAxis(view: View) {
        if (view.rotationY != 0F) {
            view.rotationY = 0F
        }
    }

    fun nextLetter(position: Int) {
        val item = list.getOrNull(position)
        if (item is ItemCollaboratorsViewModel) {
            itemSubject.onNext(item.name!!.substring(0, 1).toUpperCase())
        }
        else {
            itemSubject.onNext("?")
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (list[position] is ItemCollaboratorsViewModel) {
            return TYPE_HOLDER.TYPE_COLLABORATOR.type
        } else if (list[position] is ItemProgressMore) {
            return TYPE_HOLDER.TYPE_PROGRESS.type
        }
        return -1
    }

    fun addElements(data: List<ItemCollaboratorsViewModel>, search: Boolean = false) {
        if (search) {
            list.clear()
            list.addAll(data)
            notifyDataSetChanged()
        } else {
            list.addAll(data)
            notifyItemRangeInserted(list.size, data.size)
        }
    }

    fun currentLetter(): BehaviorSubject<String> {
        return itemSubject
    }

    fun onClick(): PublishSubject<ItemCollaboratorsViewModel> {
        return clickSubject
    }


    /**
     * Select all elements
     */
    fun selectAllElements() {
        clearSelections()
        list.forEach {
            if (it is ItemCollaboratorsViewModel) {
                it.state = STATE.SELECTED
                seletedItems.put(it.id, true)
                selectedItems.add(it.model)
            }
        }
        notifyDataSetChanged()
    }


    /**
     *  delete all elements selected
     */
    fun clearSelections() {
        reverseAllAnimations = true

        for (i in 0 until seletedItems.size()) {
            val item = list.asSequence().filter { it is ItemCollaboratorsViewModel && it.id == seletedItems.keyAt(i) }.first()
            (item as ItemCollaboratorsViewModel).changeState()
        }

        animationItemsIndex.clear()
        seletedItems.clear()
        selectedItems.clear()
        notifyDataSetChanged()
    }

    fun getCollaboratorName(): String {
        return (list.first { it is ItemCollaboratorsViewModel && (it.id == seletedItems.keyAt(0)) } as ItemCollaboratorsViewModel).name!!
    }

    fun getSelectedIDs(): MutableList<Int> {
        val ids = mutableListOf<Int>()
        for (i in 0 until seletedItems.size()) {
            ids.add(seletedItems.keyAt(i))
        }
        return ids
    }

    fun getSelected(): List<CollaboratorModel> {
        return selectedItems
    }

    /**
     *  return normal state all elements
     */
    fun resetAnimationIndex() {
        reverseAllAnimations = false
        animationItemsIndex.clear()
    }

    fun getSelectedItemCount() = seletedItems.size()


    fun addProgressItem(position: Int) {
        list.add(position, ItemProgressMore("Cargando mas colaboradores"))
        notifyItemChanged(position)
    }

    fun removeProgressItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }


    /**
     * create the animation for the selected item either to return to the normal state or to be selected
     */
    fun toogleSelection(position: Int, id_collaborator: Int) {
        val model = (list[position] as ItemCollaboratorsViewModel).model
        if (seletedItems.get(id_collaborator, false)) {
            seletedItems.delete(id_collaborator)
            selectedItems.remove(model)
            animationItemsIndex.delete(id_collaborator)
        } else {
            seletedItems.put(id_collaborator, true)
            selectedItems.add(model)
            animationItemsIndex.put(id_collaborator, true)
        }
        notifyItemChanged(position)
    }

    fun exceededLimitCollaborators(): BehaviorSubject<Boolean> {
        return isExceeded
    }

    inner class CollaboratorsViewHolder(private val binding: ItemCollaboratorsDatabaseBinding) : RecyclerView.ViewHolder(binding.root) {

        var item: ItemCollaboratorsViewModel? = null

        fun bind(item: ItemCollaboratorsViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()

            this.item = item
            val imageProfile = binding.root.findViewById<ImageView>(R.id.image_profile)

            Glide.with(binding.root.context)
                    .load(Uri.parse("https://s3-us-west-1.amazonaws.com/viva-organica-profile-pictures/fotosvo/${item.employeeCode}.JPG"))
                    .apply(RequestOptions()
                            .placeholder(R.drawable.ic_user))
                    .into(imageProfile)

            binding.root.setOnClickListener {
                if (adapterPosition > -1) {
                    if (limit_selected != null && (getSelectedItemCount() + 1) > limit_selected!! && !item.isSelected()) {
                        isExceeded.onNext(true)
                        return@setOnClickListener
                    }

                    if (justSelectOne) {
                        clearSelections()
                    }

                    item.position = adapterPosition
                    item.changeState()
                    toogleSelection(adapterPosition, item.id)
                    clickSubject.onNext(item)
                }
            }
        }

    }

    inner class CollaboratorsProgressViewHolder(private val binding: ItemProgressCollaboratorsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ItemProgressCollaboratorsBinding) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
        }
    }

}
