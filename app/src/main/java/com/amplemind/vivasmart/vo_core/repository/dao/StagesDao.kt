package com.amplemind.vivasmart.vo_core.repository.dao

import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.responses.StageResponse
import com.amplemind.vivasmart.vo_core.utils.currentISODate
import java.text.SimpleDateFormat
import java.util.*


class StagesDao : RealmDao<StageModel>(StageModel::class.java) {

    override fun retrieveServerData(response: Any?): List<StageModel>? {
        return when(response) {
            is StageResponse -> response.data
            else -> null
        }
    }

    fun getStagesForToday(): List<StageModel>? {
        updateLotStatus()
        return findAll()
    }

    fun setLotStatus(stage: StageModel, isClosed: Boolean): StageModel? {
        val obj = copy(stage)

        if (isClosed) {
            obj?.lot!!.lastClose = currentISODate()
        }

        obj?.stageActivities = false
        obj?.lot!!.isClosed = isClosed

        return upsert(obj)
    }

    fun setLotAsActive(stage: StageModel, isActive: Boolean) : StageModel? {

        val obj = copy(stage)

        obj?.stageActivities = isActive
        obj?.isClosed = false

        return upsert(obj!!)
    }

    private fun updateLotStatus(): List<StageModel>? {
        val stages = findAll()

        val today = Calendar.getInstance()
        val lastClosed = Calendar.getInstance()

        val parser = SimpleDateFormat("yyyy-MM-dd")

        val updatableStages = mutableListOf<StageModel>()

        stages?.forEach{stage ->
            if (!stage.lot!!.lastClose.isNullOrEmpty()) {
                lastClosed.time = parser.parse(stage.lot!!.lastClose)

                val dayDiff = today.get(Calendar.DAY_OF_YEAR) - lastClosed.get(Calendar.DAY_OF_YEAR)
                val yearDiff = today.get(Calendar.YEAR) - lastClosed.get(Calendar.YEAR)

                if (dayDiff > 0 || yearDiff > 0) {
                    stage.stageActivities = false
                    stage.lot!!.isClosed = false
                    updatableStages.add(stage)
                }
            }
        }

        return upsert(updatableStages)
    }



}
