package com.amplemind.vivasmart.core.repository.model

data class QuantityOrderModelTemp (
    val issues : List<IssuesModelTemp>
)


data class IssuesModelTemp(
        val name : String,
        val numberIssue : Int,
        val percentage : Int,
        val photos : List<String>
)
