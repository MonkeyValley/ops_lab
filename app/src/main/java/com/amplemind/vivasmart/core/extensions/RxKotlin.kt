package com.amplemind.vivasmart.core.extensions

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.View
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.features.production_roster.adapters.RecyclerItemTouchHelper
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit

/**
 * Created by
 *      amplemind on 6/29/18.
 */
fun Disposable.addTo(androidDisposable: AndroidDisposable): Disposable = apply { androidDisposable.add(this) }


fun View.addThrottle(duration : Long = 2L): Observable<Any> {
    return RxView.clicks(this).throttleFirst(duration, TimeUnit.SECONDS)
}


fun RecyclerView.touchHelper(listenner: RecyclerItemTouchHelper.RecyclerItemTouchHelperListener) {
    val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT, listenner)
    ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(this)
}

fun <T> Observable<T>.registerObserver(progress: BehaviorSubject<Boolean>): Observable<T> {
    return this.subscribeOn(Schedulers.io())
            .doOnSubscribe { progress.onNext(true) }
            .doOnTerminate { progress.onNext(false) }
            .doOnError { progress.onNext(false) }
            .observeOn(Schedulers.io())
}


fun <T> Single<T>.registerSingle(progress: BehaviorSubject<Boolean>): Single<T> {
    return this.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { progress.onNext(true) }
            .doOnSuccess { progress.onNext(false) }
            .doOnError { progress.onNext(false) }
}

fun Context.getImage(imageName: String): Int {
    return this.resources.getIdentifier(imageName, "drawable", this.packageName)
}

