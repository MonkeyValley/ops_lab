package com.amplemind.vivasmart.features.production_roster.viewModels

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.custom_views.SpanningLinearLayoutManager
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.production_quality.ProductionQualityActivity
import com.amplemind.vivasmart.features.production_roster.*
import com.amplemind.vivasmart.features.quality_control.QualityControlActivity
import kotlinx.android.synthetic.main.content_payroll_activities_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject


@SuppressLint("Registered")
class MenuaActivitiesQualityProductionFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: PayrollActivitiesViewModel


    fun newInstance(title: String): MenuaActivitiesQualityProductionFragment {
        val args = Bundle()
        args.putString("Title", title)

        val fragment = MenuaActivitiesQualityProductionFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_payroll_activities_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()

        viewModel.setupData(arguments!!)

        viewModel.getToolbarTitle().subscribe {

            (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = it
            lbl_track_title.text = "/ Calidad / " + it +" /"
            viewModel.loadMenuQualityProduction().addTo(subscriptions)

            viewModel.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)

        }.addTo(subscriptions)
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_menu_activities_payroll.adapter as ActivitiesPayrollAdapter).addElements(activities)

        (rv_menu_activities_payroll.adapter as ActivitiesPayrollAdapter).onClickItem().subscribe(this::clickActivityPayroll).addTo(subscriptions)
    }

    /**
     *  @param item click item activity
     *
     *  start of the different flows
     */
    private fun clickActivityPayroll(item : String) {
        Log.d(TAG, item)
        if (item == "Labores") {
            startActivity(Intent(context, QualityControlActivity::class.java))
        } else {
            startActivity(Intent(context!!, ProductionQualityActivity::class.java))
        }
    }


    private fun initRecycler() {
        rv_menu_activities_payroll.hasFixedSize()
        rv_menu_activities_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_activities_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_activities_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_activities_payroll.adapter = ActivitiesPayrollAdapter()
    }

    override fun onDestroy() {
        //TODO: Search a better way to do this
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "Calidad"
        super.onDestroy()
    }

    companion object {
        val TAG = MenuActivitiesPayrollFragment::class.java.simpleName!!
    }

}
