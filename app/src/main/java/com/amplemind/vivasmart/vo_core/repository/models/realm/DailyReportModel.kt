package com.amplemind.vivasmart.vo_core.repository.models.realm

data class DailyReportModel(
        val code: String,
        val name: String,
        val total: Double
)