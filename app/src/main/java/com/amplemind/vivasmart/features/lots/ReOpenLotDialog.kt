package com.amplemind.vivasmart.features.lots

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.widget.CompoundButton
import android.widget.ToggleButton
import com.amplemind.vivasmart.R

class ReOpenLotDialog : DialogFragment(), CompoundButton.OnCheckedChangeListener {

    private lateinit var listener: OnSelectedOption

    private var toggle_reopen: ToggleButton? = null
    private var toggle_cancel: ToggleButton? = null

    companion object {
        val TAG = ReOpenLotDialog::class.java.simpleName

        fun newInstance() = ReOpenLotDialog()
    }

    fun setListener(listener: OnSelectedOption) {
        this.listener = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_reopen_lots_dialog, null, false)
        builder.setView(mView)


        toggle_reopen = mView.findViewById(R.id.btn_reopen_lot)
        toggle_cancel = mView.findViewById(R.id.btn_cancel_lot)

        toggle_reopen!!.setOnCheckedChangeListener(this)
        toggle_cancel!!.setOnCheckedChangeListener(this)


        builder.setPositiveButton("Aceptar", null)
        builder.setNegativeButton("Cancelar") { _, i -> dismiss() }

        val create = builder.create()

        create.setOnShowListener { _ ->
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if (toggle_reopen!!.isChecked) {
                            listener.onReOpenLot()
                        }
                        if (toggle_cancel!!.isChecked) {
                            dismiss()
                        }
                    }
        }

        return create
    }

    override fun onCheckedChanged(view: CompoundButton?, isChecked: Boolean) {
        when (view!!.id) {
            R.id.btn_reopen_lot -> {
                if (isChecked) {
                    toggle_cancel!!.isChecked = false
                }
            }

            R.id.btn_cancel_lot -> {
                if (isChecked) {
                    toggle_reopen!!.isChecked = false
                }
            }
        }
    }

    interface OnSelectedOption {
        fun onReOpenLot()
    }

}
