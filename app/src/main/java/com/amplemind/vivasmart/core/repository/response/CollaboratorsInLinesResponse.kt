package com.amplemind.vivasmart.core.repository.response

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.core.repository.model.PackingLineCollaboratorsModel
import com.google.gson.annotations.SerializedName

data class CollaboratorsInLinesResult(@SerializedName("data") val data : List<CollaboratorsInLinesResponse>, @SerializedName("has_boxcount") val has_boxcount : Boolean)

@Entity
data class CollaboratorsInLinesResponse(
        @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) var id: Int? = null,
        @ColumnInfo(name = "id_activity") var id_activity: Int? = null,
        @ColumnInfo(name = "id_line") var id_line  : Int? = null,
        @SerializedName("activity") var activity: String,
        @SerializedName("collaborator_left") var collaborator_left: Int,
        @SerializedName("activity_info") @Ignore var activity_info : ActivitiesResponse? = null,
        @SerializedName("packingline_collaborator") @Ignore var packingline: List<PackingLineCollaboratorsModel>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readParcelable(ActivitiesResponse::class.java.classLoader),
            parcel.createTypedArrayList(PackingLineCollaboratorsModel))

    constructor() : this(0,null,null,"",0,null,null)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeValue(id_activity)
        parcel.writeValue(id_line)
        parcel.writeString(activity)
        parcel.writeInt(collaborator_left)
        parcel.writeParcelable(activity_info, flags)
        parcel.writeTypedList(packingline)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CollaboratorsInLinesResponse> {
        override fun createFromParcel(parcel: Parcel): CollaboratorsInLinesResponse {
            return CollaboratorsInLinesResponse(parcel)
        }

        override fun newArray(size: Int): Array<CollaboratorsInLinesResponse?> {
            return arrayOfNulls(size)
        }
    }
}