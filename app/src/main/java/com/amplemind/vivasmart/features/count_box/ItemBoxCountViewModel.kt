package com.amplemind.vivasmart.features.count_box

import com.amplemind.vivasmart.core.repository.model.BoxCountResultResponse

class ItemBoxCountViewModel(val item: BoxCountResultResponse) {

    val name = item.packing.name
    val last_count = item.last_count

    val id_box = item.packing_id
    var current_count : Int? = null

    var isError = true


}
