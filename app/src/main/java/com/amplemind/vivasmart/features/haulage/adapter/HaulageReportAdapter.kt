package com.amplemind.vivasmart.features.haulage.adapter

import android.os.Build
import androidx.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.setBackground
import com.amplemind.vivasmart.databinding.ItemReportHaulageBinding
import com.amplemind.vivasmart.features.adapters.BaseRecyclerViewAdapter
import com.amplemind.vivasmart.features.haulage.viewModel.ItemHaulageReportViewModel
import androidx.databinding.library.baseAdapters.BR
import java.text.SimpleDateFormat
import java.util.*


class HaulageReportAdapter : BaseRecyclerViewAdapter<ItemHaulageReportViewModel, HaulageReportAdapter.HaulageReportViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): HaulageReportViewHolder {
        val view = ItemReportHaulageBinding.inflate(LayoutInflater.from(parent.context!!), parent, false)
        return HaulageReportViewHolder(view)
    }


    inner class HaulageReportViewHolder(val binding: ItemReportHaulageBinding) : BaseRecyclerViewAdapter.BaseViewHolder<ItemHaulageReportViewModel>(binding.root) {

        @RequiresApi(Build.VERSION_CODES.O)
        override fun bind(item: ItemHaulageReportViewModel) {
            binding.apply {
                setVariable(BR.viewModel, item)
                executePendingBindings()

                val date = Date(item.createDate)
                val format = SimpleDateFormat("dd/MM/yy")
                format.timeZone = TimeZone.getTimeZone("-5GMT")
                binding.tvDateHaulage.text = format.format(date)

                if (item.reception) {
                    binding.ivStatus.setImageResource(R.drawable.done)
                } else {
                    binding.ivStatus.setImageResource(R.drawable.clock)
                }

            }
        }
    }

}