package com.amplemind.vivasmart.features.production_roster.fragments


import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.production_roster.ActivitiesPayrollAdapter
import com.amplemind.vivasmart.features.production_roster.GeneralActivitiesActivity
import com.amplemind.vivasmart.features.production_roster.ProductionRosterActivitiesActivity
import com.amplemind.vivasmart.features.production_roster.viewModels.GeneralRosterViewModel
import kotlinx.android.synthetic.main.fragment_general_roster.*
import javax.inject.Inject

class GeneralRosterFragment : BaseFragment() {

    companion object {
        fun newInstance(): GeneralRosterFragment {
            return GeneralRosterFragment()
        }
    }

    @Inject
    lateinit var viewModel: GeneralRosterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_general_roster, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "General"
        initRecycler()
        setMenu()
    }

    private fun initRecycler() {
        rv_general.hasFixedSize()
        rv_general.layoutManager = GridLayoutManager(context, 2)
        rv_general.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_general.itemAnimator = DefaultItemAnimator()
        rv_general.adapter = ActivitiesPayrollAdapter()
    }

    private fun setMenu() {
        (rv_general.adapter as ActivitiesPayrollAdapter).addElements(viewModel.getMenu())
        (rv_general.adapter as ActivitiesPayrollAdapter).onClickItem().subscribe(this::itemClicked).addTo(subscriptions)
    }

    private fun itemClicked(item : String) {
        startActivity(Intent(context!!, GeneralActivitiesActivity::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "Nómina"
    }

}
