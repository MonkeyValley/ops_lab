package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.response.CropUnitResponseResult

class ItemCropUnitModelViewModel(cropUnitModel : CropUnitResponseResult) {
    val id = cropUnitModel.id
    val crop_id = cropUnitModel.crop_id
    val material = cropUnitModel.material
    val name =  cropUnitModel.name
    val code =  cropUnitModel.code
    val is_premium =  cropUnitModel.is_premium
}
