package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CountingBox (
        @PrimaryKey(autoGenerate = true) val id : Int? = null,
        val registerId : Int,
        val units : Double,
        val activity_id : Int,
        val idLine : Int,
        val lot_id : Int,
        val sign : Boolean = false,
        val packing_id : Int? = null,
        val time : Double,
        val generateReport : Boolean = false
)