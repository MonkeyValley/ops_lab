package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.repository.request.VarietyBoxCount
import com.amplemind.vivasmart.vo_core.repository.HarvestBoxCountByVarietyRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.VarietyModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class HarvestBoxCountByVarietyViewModel @Inject constructor (private val repository: HarvestBoxCountByVarietyRepository){

    var stageUuid: String? = null
    var boxCounts: List<VarietyBoxCount>? = null
    var boxTotal: Double = 0.0
    var secondQuality: Double = 0.0

    var varieties: List<VarietyModel>? = null
    private val mBoxCounts = HashMap<Long, Double>()

    private val mOnVarietiesLoaded = PublishSubject.create<List<VarietyModel>>()
    private val mOnVarietiesLoadingError = PublishSubject.create<Throwable>()
    private val mOnTotalChanged = PublishSubject.create<Double>()

    fun loadVarieties() =
                when {
                    stageUuid != null -> repository.loadVarieties(stageUuid!!)
                            ?.map(this::groupVarieties)
                            ?.doOnNext(this::createBoxCountRegisters)
                            ?.subscribe(
                                    mOnVarietiesLoaded::onNext,
                                    mOnVarietiesLoadingError::onNext
                            )
                    varieties == null -> {
                        mOnVarietiesLoadingError.onNext(IllegalArgumentException("Error loading varieties"))
                        null
                    }
                    else -> Observable.just(varieties!!)
                            ?.map(this::groupVarieties)
                            ?.doOnNext(this::createBoxCountRegisters)
                            ?.subscribe({
                                mOnVarietiesLoaded.onNext(it)
                            }, {
                                mOnVarietiesLoadingError.onNext(it)
                            })
                }

    private fun createBoxCountRegisters(varieties: List<VarietyModel>?) {
        this.varieties = varieties

        varieties?.forEach { variety ->
            if (boxCounts == null) {
                setBoxCount(variety.id, 0.0, false)
            }
            else {
                val countValid = boxCounts?.find { it.varietyId == variety.id }?.boxCountValid ?: 0.0
                if(countValid > 0){
                    setBoxCount(variety.id, countValid, false)
                }else{
                    val count = boxCounts?.find { it.varietyId == variety.id }?.boxCount ?: 0.0
                    setBoxCount(variety.id, count, false)
                }

            }
        }
        calculateTotal()
    }

    private fun groupVarieties(varieties: List<VarietyModel>): List<VarietyModel> =
        varieties.distinctBy { variety -> variety.id }.sortedBy { variety -> variety.name }

    private fun calculateTotal() {
        boxTotal = mBoxCounts.entries.sumByDouble { entry ->
            entry.value
        }
        boxTotal += secondQuality

        mOnTotalChanged.onNext(boxTotal)
    }

    fun setBoxCount(varietyId: Long, boxCount: Double, calculateTotal: Boolean = true) {
        if (varietyId == -1L) {
            secondQuality = boxCount
        }
        else {
            mBoxCounts[varietyId] = boxCount
        }

        if (calculateTotal) {
            calculateTotal()
        }
    }

    fun getFullCountFor(varietyId: Long) = if (varietyId == -1L) secondQuality else (mBoxCounts[varietyId] ?: 0.0)

    fun getCountFor(varietyId: Long) = getFullCountFor(varietyId).toInt()

    fun hasHalfBox(varietyId: Long) = getFullCountFor(varietyId) > getCountFor(varietyId).toDouble()

    fun buildVarietyBoxCounts() =
            varieties?.map {variety ->
                VarietyBoxCount(variety.id, mBoxCounts[variety.id] ?: 0.0,  mBoxCounts[variety.id] ?: 0.0)
            } ?: listOf()

    fun onVarietiesLoaded(callback: (List<VarietyModel>) -> Unit): Disposable =
            mOnVarietiesLoaded.subscribe(callback)

    fun onVarietiesLoadingError(callback: (Throwable) -> Unit): Disposable =
            mOnVarietiesLoadingError.subscribe(callback)

    fun onTotalChanged(callback: (Double) -> Unit): Disposable =
            mOnTotalChanged.subscribe(callback)


}