package com.amplemind.vivasmart.features.production_lines.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.model.BoxCountResultResponse
import com.amplemind.vivasmart.core.repository.model.CountBox
import com.amplemind.vivasmart.core.repository.model.CountBoxModel
import com.amplemind.vivasmart.core.repository.model.CountBoxRequest
import com.amplemind.vivasmart.core.repository.remote.BoxCountLinesRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.count_box.ItemBoxCountViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class BoxCountLinesViewModel @Inject constructor(private val repository: BoxCountLinesRepository,
                                                 private val apiErrors: HandleApiErrors,
                                                 private val mediator: Mediator) : BaseViewModel(){

    private val finishCountBoxSubject = BehaviorSubject.create<Boolean>()

    private val listBoxSubject = BehaviorSubject.create<MutableList<ItemBoxCountViewModel>>()

    private val listBox = mutableListOf<ItemBoxCountViewModel>()

    private val newListBox = BehaviorSubject.create<CountBox>()

    fun loadCountBox(): Disposable {
        return repository.getBoxCount(mediator.bundleItemLinesPackage!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    doAsync {
                        repository.updateCountingBox(it.packings, mediator.bundleItemLinesPackage!!)
                    }
                    mapModelToViewModel(it.packings)
                }
                .subscribe({
                    listBox.addAll(it)
                    listBoxSubject.onNext(listBox)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getCountBoxOffline(): Disposable {
        return repository.getBoxCountOffline(mediator.bundleItemLinesPackage!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map(this::mapModelToViewModel)
                .subscribe({
                    listBox.addAll(it)
                    listBoxSubject.onNext(listBox)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModel(packings: List<BoxCountResultResponse>): MutableList<ItemBoxCountViewModel> {
        val models = mutableListOf<ItemBoxCountViewModel>()

        for (item in packings) {
            models.add(ItemBoxCountViewModel(item))
        }

        return models
    }

    fun getCountBox(): BehaviorSubject<MutableList<ItemBoxCountViewModel>> {
        return listBoxSubject
    }

    fun isCorrectCountBox(): Boolean {
        return listBox.none { it.isError }
    }

    private fun mapListBox(): MutableList<CountBoxModel> {
        val new_box = mutableListOf<CountBoxModel>()

        listBox.forEach { box ->
            new_box.add(CountBoxModel(box.id_box, box.current_count!!))
        }
        return new_box
    }

    fun getNewCountBox(): Disposable {
        return Observable.just(mapListBox()).map {
            return@map CountBox(it, mediator.timesCollaborator)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    newListBox.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getListBoxComplete(): BehaviorSubject<CountBox> {
        return newListBox
    }

    fun sendCountBox(hasInternet: Boolean, data: CountBoxRequest): Disposable {
        val observer = if (hasInternet) {
            repository.sendCountBox(data, mediator.bundleItemLinesPackage!!)
        } else {
            repository.saveCountBoxLocal(data, mediator.bundleItemLinesPackage!!)
        }

        return observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    finishCountBoxSubject.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun finishCountBox(): BehaviorSubject<Boolean> {
        return finishCountBoxSubject
    }

}
