package com.amplemind.vivasmart.core.base

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.utils.ACTIVITY_CONTEXT
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.vo_features.services.sync.SyncDownloadModule
import com.amplemind.vivasmart.vo_features.sync.SyncActivity
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Created by
 *           amplemind on 6/29/18.
 */
open class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var mEventBus: EventBus

    val subscriptions = AndroidDisposable()
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        mEventBus.observe(this::onSetToolbarTitleEvent).addTo(subscriptions)
        mEventBus.observe<SyncDownloadModule.DownloadEvent>()
                .filter { event ->
                    event.eventType == SyncDownloadModule.EventType.FIRST_SYNC_NEEDED
                }
                .subscribe(this::onFirstSyncNeeded)
                .addTo(subscriptions)

        initProgressDialog()
    }

    override fun onResume() {
        super.onResume()
        ACTIVITY_CONTEXT = this
        Log.e("ACTIVITY-CONTEXT", ACTIVITY_CONTEXT!!.toString())
    }

    override fun onPause() {
        hideKeyboard(this)
        super.onPause()
        //ACTIVITY_CONTEXT = null
        Log.e("ACTIVITY-CONTEXT", "null")
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.dispose()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(this)
        progressDialog.setTitle(getString(R.string.app_name))
        progressDialog.setMessage(getString(R.string.server_connection))
        progressDialog.setCancelable(false)
    }

    protected open fun onSetToolbarTitleEvent(event: BaseFragment.SetToolbarTitleEvent) {
        if (event.context == this) {
            event.title?.apply {
                supportActionBar?.title = event.title
            }
            event.subtitle?.apply {
                supportActionBar?.subtitle = event.subtitle
            }
        }
    }

    protected fun onError(error: String) {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.app_name))
                .setMessage(error)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, i ->
                    dialogInterface.dismiss()
                }.show()

    }

    protected fun showProgressDialog(show: Boolean) {
        if (show) progressDialog.show() else progressDialog.dismiss()
    }

    protected open fun onFirstSyncNeeded(event: SyncDownloadModule.DownloadEvent) {
        Log.e("BaseActivity", "FirstSyncPending!")
        if (this !is SyncActivity) {
            SyncActivity.startForFirstSync(this)
        }
    }

}