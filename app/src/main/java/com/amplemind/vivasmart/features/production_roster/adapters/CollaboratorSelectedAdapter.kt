package com.amplemind.vivasmart.features.production_roster.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.RadioGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_collaborator_checked.view.*

class CollaboratorSelectedAdapter  (ctx: Context, items:List<ActivityLogModel>, click:SelectedItem): RecyclerView.Adapter<CollaboratorSelectedAdapter.ViewHolder>() {

    var list:List<ActivityLogModel>
    var delegate:SelectedItem
    var ctx:Context

    init {
        this.list = items
        this.ctx = ctx
        this.delegate = click
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_collaborator_checked, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list.get(position)

        holder.lblName.text = item.collaborator!!.name

        holder.btnSelectedItem.setOnCheckedChangeListener { _, isChecked ->
            delegate.clickItem(list.get(position), position, isChecked)
        }
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val btnSelectedItem = vista.cb_collaborator
        val lblName = vista.lbl_name_collaborator
    }
}
interface SelectedItem{
    fun clickItem(item:ActivityLogModel, position:Int, isChecked:Boolean)
}