package com.amplemind.vivasmart.features.packaging_quality.clean_report

import com.amplemind.vivasmart.core.repository.CleanReportTotalRepository
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.request.IssueRequest
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class CleanReportViewModel @Inject constructor(private val repository: CleanReportTotalRepository,
                                               private val apiErrors: HandleApiErrors) {

    private val isAgreed = BehaviorSubject.create<Boolean>()
    private val listCards = BehaviorSubject.create<List<ItemCleanViewModel>>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val requestFail = BehaviorSubject.create<String>()
    private val issuesUpdated = BehaviorSubject.create<QualityCarryOrderResponse>()

    private val issues = mutableListOf<ItemCleanViewModel>()
    private val auxIssuesList = mutableListOf<IssueRequest>()
    private val issuesList = mutableListOf<IssueRequest>()
    private var numberReviews = 1
    private var sampleSize = 1
    private var numberExported = 0
    private var numberPermanentDamage = 0
    private var numberDefects = 0
    private var isFormRestarted = false
    private var isRestoreCard = false

    fun setNumberReviews(size: Int) {
        numberReviews = size
        sampleSize = size
    }

    fun uploadIssues(unitId: Int, carryOrderId: Int, isOnline: Boolean): Disposable {
        numberExported = 0
        numberPermanentDamage = 0
        numberDefects = 0
        val listIssues = mergeIssues()

        return repository.createIssues(listIssues, carryOrderId, sampleSize, unitId, numberExported, numberPermanentDamage, numberDefects, isOnline)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    issuesUpdated.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    // TODO: Improve the algorithm
    private fun mergeIssues(): List<IssueRequest> {
        val issuesRequest = mutableListOf<IssueRequest>()
        var cycles = 0
        var count = 0
        for (item in issuesList) {
            val filtered = issuesRequest.filter { it.id == item.id }
            if (filtered.isNotEmpty()) {
                filtered.first().count += item.count
            } else {
                issuesRequest.add(IssueRequest(item.id, item.count, item.name, item.isPermanent))
            }
            count++
            if (count >= issues.size) {
                var issuesFound = false
                var issuePermanent = false
                for (i in (cycles * issues.size) until ((cycles * issues.size) + issues.size)) {
                    if (i < issuesList.size && issuesList[i].count > 0) {
                        issuesFound = true
                        if (issuesList[i].isPermanent) {
                            issuePermanent = true
                            break
                        }
                    }
                }
                if (issuesFound) {
                    if (issuePermanent) {
                        numberPermanentDamage++
                    } else {
                        numberDefects++
                    }
                } else {
                    numberExported++
                }
                count = 0
                cycles++
            }
        }
        return issuesRequest
    }

    fun loadCards(cropId: Int): Disposable {
        return repository.getIssues(cropId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    repository.saveLocalListOfIssues(cropId, it.data)
                    mapModelToViewModels(it.data)
                }
                .subscribe({
                    issues.clear()
                    issues.addAll(it)
                    listCards.onNext(issues)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun loadLocalCards(cropId: Int): Disposable {
        return repository.getLocalListOfIssues(cropId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { mapModelToViewModels(it.data) }
                .subscribe({
                    issues.clear()
                    issues.addAll(it)
                    listCards.onNext(issues)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getListCards(): BehaviorSubject<List<ItemCleanViewModel>> {
        return listCards
    }

    private fun mapModelToViewModels(models: List<IssueModel>): List<ItemCleanViewModel> {
        val list = arrayListOf<ItemCleanViewModel>()
        auxIssuesList.clear()
        for (item in models) {
            list.add(ItemCleanViewModel(item))
        }
        return list
    }

    fun saveAgreed(): Disposable {
        return repository.saveAgreedTutorial()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    repository.saveUser(Gson().toJson(it))
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getAgreedTutorial(): Disposable {
        return repository.isAgreedTutorial()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    isAgreed.onNext(it)
                }.subscribe()
    }

    fun validIssue(adapterPosition: Int) {
        if (isFormRestarted && isRestoreCard) {
            auxIssuesList.add(IssueRequest(issues[issues.size - 1].id, 1, issues[issues.size - 1].name, issues[issues.size - 1].isPermanent))
            completeReview()
        } else {
            val position = adapterPosition + 1
            if (position >= issues.size) return
            auxIssuesList.add(IssueRequest(issues[position].id, 1, issues[position].name, issues[position].isPermanent))
            isFormRestarted = false
        }
        isRestoreCard = false
    }

    fun invalidIssue(adapterPosition: Int) {
        if (isFormRestarted && isRestoreCard) {
            auxIssuesList.add(IssueRequest(issues[issues.size - 1].id, 0, issues[issues.size - 1].name, issues[issues.size - 1].isPermanent))
            completeReview()
        } else {
            val position = adapterPosition + 1
            if (position >= issues.size) return
            auxIssuesList.add(IssueRequest(issues[position].id, 0, issues[position].name, issues[position].isPermanent))
            isFormRestarted = false
        }
        isRestoreCard = false
    }

    fun onRestoreCard() {
        if (numberReviews < sampleSize) {
            isRestoreCard = true
            if (!isFormRestarted) {
                if (auxIssuesList.isNotEmpty()) {
                    auxIssuesList.removeAt(auxIssuesList.size - 1)
                }
            } else {
                // Restore last state
                auxIssuesList.clear()
                numberReviews++
                for (i in issuesList.size - issues.size until issuesList.size) {
                    auxIssuesList.add(issuesList[i])
                }
                auxIssuesList.map { issuesList.remove(it) }
                auxIssuesList.removeAt(auxIssuesList.size - 1)
            }
        }
    }

    fun notIssuesFound(adapterPosition: Int) {
        if (isFormRestarted && isRestoreCard) {
            auxIssuesList.add(IssueRequest(issues[issues.size - 1].id, 0, issues[issues.size - 1].name, issues[issues.size - 1].isPermanent))
        } else {
            val position = adapterPosition + 1
            if (position >= issues.size) return
            for (i in position until issues.size) {
                auxIssuesList.add(IssueRequest(issues[i].id, 0, issues[i].name, issues[i].isPermanent))
            }
            isFormRestarted = false
        }
        isRestoreCard = false
    }

    fun completeReview() {
        if (numberReviews > 0) {
            numberReviews--
            isFormRestarted = true
            issuesList.addAll(auxIssuesList)
            auxIssuesList.clear()
        }
    }

    fun isLastCard(adapterPosition: Int): Boolean {
        val position = adapterPosition + 1
        if (position >= issues.size - 1) return true
        return false
    }

    fun isTherePendingReview(): Boolean {
        return numberReviews > 0
    }

    fun isAgreed(): BehaviorSubject<Boolean> {
        return isAgreed
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onIssuesUpdated(): BehaviorSubject<QualityCarryOrderResponse> {
        return issuesUpdated
    }

}
