package com.amplemind.vivasmart.features.production_range.service

import android.content.Context
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangeVariatiesModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionVar
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.reactivex.Single
import io.realm.Realm
import io.realm.Sort
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class ProductionRangeService @Inject constructor() {

    var context: Context? = null

    lateinit var preferences: UserAppPreferences

    fun getActivities(lotId: Int, soilId: Int): Single<List<ProductionRangeVariatiesModel>> =
            Realm.getDefaultInstance().use {
                Single.create<List<ProductionRangeVariatiesModel>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->

                        val cropId = realm
                                .where(StageModel::class.java)
                                .equalTo("lotId", lotId)
                                .findFirst()?.cropId ?: 0

                        //Se invierten debido a los tipos de suelos
                        val soil = when(soilId){
                            1 -> {  2 }
                            2 -> {  1 }
                            else -> 0
                        }

                        val results = realm
                                .where(ProductionRangeVariatiesModel::class.java)
                                .beginGroup()
                                .equalTo("crops.cropId", cropId)
                                .and()
                                .equalTo("soilTypeId", soil)
                                .or()
                                .isNull("soilType")
                                .endGroup()
                                .sort("activitySubCategoryId", Sort.DESCENDING)
                                .findAll()

                        emitter.onSuccess(realm.copyFromRealm(results))
                    }
                }
            }

    fun saveProductionRangeVarieties(list: ArrayList<ProductionRangeVariatiesModel>, lotId: String) : Boolean {
        val calendar = Calendar.getInstance()
        val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val todayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
        val date = SimpleDateFormat("yyyy-MM-dd").parse(todayDate)
        calendar.time = date

        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                list.forEach { item ->
                    val newObject = realm.createObject(ProductionVar::class.java)
                    newObject.lotId = lotId.toInt()
                    newObject.weekNumer = calendar.get(Calendar.WEEK_OF_YEAR)
                    newObject.value = item.value
                    newObject.maxVal = item.maxVal
                    newObject.minVal = item.minVal
                    newObject.productionVarId = item.id
                    newObject.productionVarOptionId = if (item.productionVarOptionId == "") null else item.productionVarOptionId!!.toInt()
                    newObject.uuid = getUUID()
                    realm.insert(newObject)
                }
            }
        }

        converProductionRangeVarietiesObjectToJson(list, lotId, todayDate)

        return true
    }


    private fun converProductionRangeVarietiesObjectToJson(list: ArrayList<ProductionRangeVariatiesModel>, lotId: String, date: String) {
        val prefer = context!!.getSharedPreferences(
                context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

        val rootObject = JSONObject()
        val arrayObject = JSONArray()

        list.forEach { item ->
            if(item.productionVarId != "") {
                val newJsonObject = JSONObject()
                newJsonObject.put("value", item.value)
                newJsonObject.put("min_val", item.minVal)
                newJsonObject.put("max_val", item.maxVal)
                newJsonObject.put("production_var_id", item.productionVarId)
                newJsonObject.put("production_var_option_id", item.productionVarOptionId)
                arrayObject.put(newJsonObject)
            }
        }

        rootObject.put("lot_id", lotId)
        rootObject.put("date", date)
        rootObject.put("production_vars", arrayObject)

        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                val url = preferences.selectedServerUrl
                val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                uploadObject.verb = "POST"
                uploadObject.url = url + "v2/production_var/log"
                uploadObject.contentType = "application/json"
                uploadObject.body = rootObject.toString()
                uploadObject.token = prefer.getString("token", "") ?: ""
                uploadObject.order = 99
                realm.insertOrUpdate(uploadObject)
            }
        }

    }

    fun validateWeekData(lotId: String, week: Int): Boolean {
        Realm.getDefaultInstance().use {
            val result = it
                    .where(ProductionVar::class.java)
                    .equalTo("lotId", lotId.toInt())
                    .and()
                    .equalTo("weekNumer", week)
                    .findAll()
            return result.size != 0
        }

    }

}