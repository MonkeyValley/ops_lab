package com.amplemind.vivasmart.core.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.amplemind.vivasmart.core.repository.model.CarryOrderCondensedModel
import com.amplemind.vivasmart.core.repository.model.UnitModel
import com.amplemind.vivasmart.core.repository.model.UpdateCarryOrderSync

@Dao
interface RecibaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecibaCondensedList(list: List<CarryOrderCondensedModel>)

    @Query("SELECT * FROM CarryOrderCondensedModel WHERE showInPackage = :showPackage ORDER BY folio ASC")
    fun getRecibaCondensedList(showPackage : Boolean = false): List<CarryOrderCondensedModel>

    @Query("DELETE FROM CarryOrderCondensedModel WHERE id = :id")
    fun deleteRecibaCondensedList(id: Int)

    @Query("UPDATE CarryOrderCondensedModel SET showInPackage = :showPackage WHERE id = :id")
    fun updateRecibaToPackage(id: Int, showPackage : Boolean = true)

    /*
     * Units
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUnit(order: UnitModel)

    @Query("SELECT * FROM UnitModel")
    fun getUnits(): List<UnitModel>

    /*
     * Reciba Sync
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCarryOrderReadyToSync(order: UpdateCarryOrderSync)

    @Query("SELECT * FROM UpdateCarryOrderSync")
    fun getCarryOrderReadyToSync(): List<UpdateCarryOrderSync>

    @Query("DELETE FROM UpdateCarryOrderSync")
    fun deleteAllCarryOrderReadyToSync()

}