package com.amplemind.vivasmart.features.CustomCamera

import android.content.Context
import android.content.res.Configuration
import android.hardware.Camera
import android.view.SurfaceHolder
import android.view.SurfaceView
import java.io.IOException

class showCamera(cam: Camera, context: Context): SurfaceView(context), SurfaceHolder.Callback {

    var ctx: Context
    var camera: Camera
    var holdsdjncsncjserr: SurfaceHolder

    init {
        this.ctx = context
        this.camera = cam
        this.holdsdjncsncjserr = holder
        this.holder.addCallback(this)
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        camera.stopPreview()
        camera.release()
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        var params = camera.parameters
        var sizes = params.supportedPictureSizes
        lateinit var mSizes: Camera.Size

        sizes.forEach {
            mSizes = it
        }

        if(this.resources.configuration.orientation != Configuration.ORIENTATION_LANDSCAPE ){
            params.set("orientation","portrait")
            camera.setDisplayOrientation(90)
            params.setRotation(90)
        }else{
            params.set("orientation","landscape")
            camera.setDisplayOrientation(0)
            params.setRotation(0)
        }

//        params.setPictureSize(600, 600)
        camera.parameters = params
        try {
            camera.setPreviewDisplay(this.holdsdjncsncjserr)
            camera.startPreview()
        }catch (e: IOException){
            e.printStackTrace()
        }
    }


}