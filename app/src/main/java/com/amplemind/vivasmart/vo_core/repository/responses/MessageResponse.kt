package com.amplemind.vivasmart.vo_core.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import com.google.gson.annotations.SerializedName

data class MessageResponse(@SerializedName("data") val data: ArrayList<MessageModel>)