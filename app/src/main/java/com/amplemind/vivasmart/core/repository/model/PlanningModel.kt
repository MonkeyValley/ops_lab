package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class PlanningModel(
        @SerializedName("activity") val activity: ActivityModel,
        @SerializedName("planning_id") val planningId: Int,
        @SerializedName("id") val id: Int,
        @SerializedName("priority") val priority: Int,
        @SerializedName("days") val days: List<PlanningDayModel>
)