package com.amplemind.vivasmart.core.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.response.ActivitiesResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResponse
import io.reactivex.Single

@Dao
interface CollaboratorsLineDao {

    @Query("SELECT * FROM ActivitiesResponse WHERE businessUnitId = :bu ORDER BY name ASC")
    fun getLocalActivities(bu : String): List<ActivitiesResponse>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertActivities(activity: List<ActivitiesResponse>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPresentation(presentations: List<PresentationResultResponse>)

    @Query("SELECT * FROM PresentationResultResponse WHERE business_unit_id = :bu ORDER BY presentation_name ASC")
    fun getLocalPresentation(bu : String): List<PresentationResultResponse>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollaboratorInLine(collaborators: PackingLineCollaboratorsModel) : Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCollaboratorInLine(collaborators: List<PackingLineCollaboratorsModel>)

    @Query("SELECT COUNT(*) >= 1 FROM PackingLineCollaboratorsModel WHERE employeeCode =:userCode AND is_done = :isDone")
    fun existCollaborator(userCode : String, isDone: Boolean = false) : Boolean

    @Query("UPDATE PackingLineCollaboratorsModel SET packing_id = :presentation, presentation_name =:name WHERE id_collaborator_line = :registerId AND packingline_id=:id_line")
    fun updatePresentation(registerId: Int, presentation: Int, name: String, id_line: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLine(line: CollaboratorsInLinesResponse): Long

    @Query("SELECT * FROM CollaboratorsInLinesResponse WHERE id_activity = :idActivity AND id_line = :idLine")
    fun existLine(idLine: Int, idActivity: Int): CollaboratorsInLinesResponse?


    @Query("SELECT * FROM CollaboratorsInLinesResponse")
    fun existLineAll(): List<CollaboratorsInLinesResponse>


    @Query("SELECT * FROM CollaboratorsInLinesResponse WHERE id_line = :idLine")
    fun getLocalLines(idLine: Int): List<CollaboratorsInLinesResponse>

    @Query("SELECT * FROM RosterLineModel WHERE line_id = :idLine")
    fun getLine(idLine: Int): RosterLineModel

    @Query("SELECT * FROM PackingLineCollaboratorsModel WHERE packingline_id = :idLine AND is_done = :isDone")
    fun getLocalCollaboratorsInLine(idLine: Int, isDone: Boolean = false): List<PackingLineCollaboratorsModel>

    @Query("SELECT * FROM PackingLineCollaboratorsModel WHERE packingline_id = :idLine AND is_done = :isDone AND id_collaborator_line = :id ")
    fun getLocalCollaboratorsInLineByCode(idLine: Int, id :Int,isDone: Boolean = false): List<PackingLineCollaboratorsModel>

    @Query("SELECT * FROM PackingLineCollaboratorsModel")
    fun getCollaboratorsSyncPackage(): List<PackingLineCollaboratorsModel>

    @Query("SELECT * FROM DetailtRosterLinesResponse")
    fun getDetailsReportsSync() : List<DetailtRosterLinesResponse>

    @Query("SELECT COUNT(*) FROM PackingLineCollaboratorsModel WHERE packingline_id = :idLine AND is_done = :isDone AND is_time = :isTime")
    fun getCountCollaboratorsInLine(idLine: Int, isDone: Boolean = false, isTime : Boolean = false): Int

    @Query("UPDATE CollaboratorsInLinesResponse SET collaborator_left = (collaborator_left - :collaborators) WHERE id = :idLineDb")
    fun updateCollaboratorLeft(idLineDb: Int, collaborators: Int)

    @Query("SELECT collaborator_left FROM CollaboratorsInLinesResponse WHERE id = :idLineDb")
    fun getCollaboratorsLeft(idLineDb: Int): Int

    @Query("SELECT * FROM BoxCountResultResponse WHERE packingline_id = :id_line")
    fun getLocalCountBox(id_line: Int): List<BoxCountResultResponse>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveCountBox(boxCountResultResponse: BoxCountResultResponse)

    @Query("SELECT * FROM BoxCountResultResponse WHERE packingline_id = :idLine")
    fun getBoxCountOffline(idLine: Int): List<BoxCountResultResponse>

    @Query("UPDATE PackingLineCollaboratorsModel SET temp_time = :time, is_paused = :pause, date = 0 WHERE id_collaborator_line = :id AND packingline_id = :idLine")
    fun pauseCollaboratorsOffline(pause: Boolean, time: Long, id: Int, idLine: Int)

    @Query("UPDATE PackingLineCollaboratorsModel SET is_paused = :pause, date = :date WHERE id_collaborator_line = :id AND packingline_id = :idLine")
    fun startCollaboratorOffline(id: Int, pause: Boolean = false, date: Long, idLine: Int)

    @Query("UPDATE PackingLineCollaboratorsModel SET temp_time = :time, date = :date WHERE id_collaborator_line = :id AND packingline_id = :idLine")
    fun saveTempTime(time: Long,date : Long,id: Int, idLine: Int)

    @Query("UPDATE PackingLineCollaboratorsModel SET temp_time = :time WHERE id_collaborator_line = :id AND packingline_id = :idLine")
    fun updateTimesCollaboratorsOffline(time: Long, id: Int, idLine: Int)

    @Query("DELETE FROM PackingLineCollaboratorsModel WHERE id_collaborator_line = :registerId AND packingline_id = :idLine")
    fun deleteCollaboratorOffline(registerId: Int, idLine: Int)

    @Query("UPDATE BoxCountResultResponse SET last_count = :total WHERE packing_id = :id AND packingline_id = :idLine")
    fun updateCountingBox(id: Int, total: Int, idLine: Int)

    @Query("DELETE FROM CollaboratorsInLinesResponse WHERE id = :id_lineOffline")
    fun deleteLineOffline(id_lineOffline: Int?)

    @Query("SELECT EXISTS(SELECT 1 FROM BoxCountResultResponse WHERE packing_id=:id_package AND packingline_id=:id_line)")
    fun existPresentation(id_package: Int, id_line: Int): Boolean?

    @Query("UPDATE PackingLineCollaboratorsModel SET temp_time=:timer WHERE id_collaborator_line = :userId AND packingline_id = :idLine")
    fun updateCollaboratorTime(userId: Int, timer: Long, idLine: Int)

    @Query("SELECT * FROM ActivitiesResponse WHERE id_activities = :idActivity")
    fun getActivity(idActivity: Int?): ActivitiesResponse

    @Query("SELECT * FROM PresentationResultResponse WHERE id_presentation = :packing_id")
    fun getPresentation(packing_id: Int?): PresentationResultResponse

    @Query("UPDATE PackingLineCollaboratorsModel SET temp_time=:timer, training = :isTraining, practice =:is_practice,is_done = :isDone, sign = :sign WHERE id_collaborator_line = :userId AND packingline_id = :idLine")
    fun finishOffLineCollaborator(userId: Int, timer: Long?, idLine: Int, isTraining : Boolean, is_practice : Boolean, sign : String?, isDone :Boolean = true)

    @Query("UPDATE PackingLineCollaboratorsModel SET temp_time=:timer,is_done = :isDone WHERE id_collaborator_line = :userId AND packingline_id = :idLine")
    fun finishOffLineAllCollaborator(userId: Int, timer: Long?, idLine: Int,isDone :Boolean = true)

    @Query("UPDATE PackingLineCollaboratorsModel SET units=:units, units_olds = :old_total_count WHERE is_done = :isDone AND packingline_id = :idLine AND id_collaborator_line = :userId")
    fun updateUnits(units: Double?,old_total_count : Float,idLine : Int,userId : Int?,isDone: Boolean = false)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createCountBoxLog(countingBox: CountingBox) : Long

    @Query("SELECT * FROM CountingBox WHERE registerId = :registerId")
    fun getAsyncCountBox(registerId: Int) : List<CountingBox>

    @Query("UPDATE CountingBox SET sign = :hasSign WHERE registerId = :registerId")
    fun finishCountBoxLogs(registerId: Int, hasSign : Boolean = true)

    @Query("SELECT * FROM CountingBox WHERE idLine = :idLine AND registerId = :idRegister")
    fun getTotalQuantityLogs(idLine: Int, idRegister : Int) : List<CountingBox>

    @Query("SELECT * FROM CountingBox WHERE idLine = :idLine AND registerId = :idRegister AND generateReport = :generate")
    fun getTotalQuantityLogsGenerateReport(idLine: Int, idRegister : Int, generate : Boolean = false) : List<CountingBox>

    @Query("UPDATE CountingBox SET generateReport = :generate WHERE id = :id_box")
    fun updateBoxGenerateReportSuccess(id_box : Int,generate : Boolean = true)

    @Query("SELECT * FROM CountingBox")
    fun getTotalQuantityLogsAll() : List<CountingBox>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCollaboratorReadyToReview(recent: DetailtRosterResponse)

    @Query("SELECT * FROM PackingLineCollaboratorsModel WHERE packingline_id = :idLine AND id_collaborator_line =:userId")
    fun getCollaboratorByIdAndLine(userId: Int?, idLine: Int) : PackingLineCollaboratorsModel

    @Query("SELECT * FROM PackingLineCollaboratorsModel")
    fun getAllCollaboratorInPackage() : List<PackingLineCollaboratorsModel>

    @Query("SELECT * FROM PackingLineCollaboratorsModel WHERE is_done = :isDone")
    fun getCollaboratorIsDoneTrue(isDone: Boolean = true) : List<PackingLineCollaboratorsModel>

    @Insert
    fun insertDetailRoster(detail : DetailtRosterLinesResponse)

    @Query("UPDATE DetailtRosterLinesResponse SET sign = :sign WHERE id_collaborator_db = :id")
    fun signAllDetailt(id: Int, sign: String)

    @Query("SELECT * FROM DetailtRosterLinesResponse WHERE id_collaborator_db = :id AND packingline_collaborator_id = :idLine")
    fun getReportDetail(id: Int, idLine: Int) : List<DetailtRosterLinesResponse>

    @Query("SELECT * FROM DetailtRosterLinesResponse")
    fun getReportDetailAll() : List<DetailtRosterLinesResponse>

    @Query("UPDATE DetailtRosterLinesResponse SET status_sync = :status WHERE id_collaborator_db = :id AND packingline_collaborator_id = :idLine")
    fun updateReportDetail(id: Int, idLine: Int, status : Boolean)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun deleteCollaboratorBySync(deleteCollaboratorsOffline: DeleteCollaboratorsOffline)

    @Query("SELECT * FROM DeleteCollaboratorsOffline")
    fun getCollaboratorsByDeleteSync() : List<DeleteCollaboratorsOffline>

    @Query("DELETE FROM CountingBox WHERE registerId = :registerId")
    fun deleteAsyncCountBox(registerId: Int)

    @Query("DELETE FROM BoxCountResultResponse WHERE packingline_id = :id_packing_line")
    fun deleteAsyncCountBoxByLine(id_packing_line : Int)

    @Query("DELETE FROM DetailtRosterLinesResponse WHERE id_collaborator_db = :registedId")
    fun deleteDetailsReportsSync(registedId : Int)

    @Query("SELECT SUM(time) FROM CountingBox WHERE idLine = :idLine AND registerId = :idRegister")
    fun getTimeInCountByCollaborator(idLine: Int, idRegister : Int) : Double


}