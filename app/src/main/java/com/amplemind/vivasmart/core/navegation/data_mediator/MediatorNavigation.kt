package com.amplemind.vivasmart.core.navegation.data_mediator

import android.content.Context
import com.amplemind.vivasmart.features.production_roster.fragments.TimerFragment
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class MediatorNavigation @Inject constructor() : IMediatorNavigation {

    private var array = mutableListOf<Colleague>()

    fun addColleague(item: Colleague) {
        array.add(item)
    }

    fun removeColleague(item: Colleague) {
        array.remove(item)
    }

    fun sendClickColleague(viewModel: TimerUserViewModel, position: Int) {
        for (fragment in array) {
            if (fragment is TimerFragment) {
                if (fragment.getActivityCode() == viewModel.getActivityCode()) {
                    fragment.onClick(viewModel, position)
                }
            } else {
                fragment.onClick(viewModel, position)
            }
        }
    }

    fun sendStartTimer(viewModel: TimerUserViewModel, position: Int) {
        for (fragment in array) {
            if (fragment is TimerFragment) {
                if (fragment.getActivityCode() == viewModel.getActivityCode()) {
                    fragment.startTimer(viewModel, position)
                }
            } else {
                fragment.startTimer(viewModel, position)
            }
        }
    }

    fun pauseTimer(viewModel: TimerUserViewModel, position: Int) {
        for (fragment in array) {
            if (fragment is TimerFragment){
                if (fragment.getActivityCode() == viewModel.getActivityCode()) {
                    fragment.pauseTimer(viewModel, position)
                }
            }else{
                fragment.pauseTimer(viewModel, position)
            }
        }
    }

    fun clear() {
        array.clear()
    }

}

interface Colleague {
    fun onClick(item: TimerUserViewModel, position: Int)
    fun startTimer(item: TimerUserViewModel, position: Int)
    fun pauseTimer(item: TimerUserViewModel, position: Int)
}