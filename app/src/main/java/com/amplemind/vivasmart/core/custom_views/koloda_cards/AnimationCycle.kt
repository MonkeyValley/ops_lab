package com.amplemind.vivasmart.core.custom_views.koloda_cards

/**
 * Created by anna on 11/16/17.
 */
enum class AnimationCycle {
    NO_ANIMATION, ANIMATION_START, ANIMATION_IN_PROGRESS
}