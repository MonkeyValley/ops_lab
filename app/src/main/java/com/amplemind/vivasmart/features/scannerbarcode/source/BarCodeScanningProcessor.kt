package com.amplemind.vivasmart.features.scannerbarcode.source

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import io.reactivex.subjects.PublishSubject

class BarCodeScanningProcessor : VisionProcessorBase<List<FirebaseVisionBarcode>>() {

    private var detector: FirebaseVisionBarcodeDetector? = null

    private val clickSubject = PublishSubject.create<String>()

    private var code = ""

    init {
        detector = FirebaseVision.getInstance().visionBarcodeDetector
    }

    override fun detectInImage(image: FirebaseVisionImage): Task<List<FirebaseVisionBarcode>> {
        return detector!!.detectInImage(image)
    }

    fun getBarcode(): PublishSubject<String> {
        return clickSubject
    }

    override fun onSuccess(results: List<FirebaseVisionBarcode>, frameMetadata: FrameMetadata) {
        try {
            for (i in results.indices) {
                val barcode = results[i]
                if (barcode.rawValue != null) {
                    if (code != barcode.rawValue!!) {
                        code = barcode.rawValue!!
                        clickSubject.onNext(barcode.rawValue!!)
                    }
                    return
                }
            }
        } catch (e: Exception){
            Log.e("barCodeProcessor", e.toString())
            return
        }
    }

    override fun onFailure(e: Exception) {
        Log.e("onFailure", "Barcode detection failed ${e.message}")
    }

    override fun stop() {
        detector!!.close()
    }
}
