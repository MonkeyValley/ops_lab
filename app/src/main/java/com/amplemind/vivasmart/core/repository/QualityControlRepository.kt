package com.amplemind.vivasmart.core.repository

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.ControlQualityDao
import com.amplemind.vivasmart.core.repository.model.ControlQualityActivitiesListModel
import com.amplemind.vivasmart.core.repository.model.ControlQualityListModel
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.remote.QualityControlApi
import com.amplemind.vivasmart.core.repository.request.*
import com.amplemind.vivasmart.core.repository.response.IssuesResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.google.gson.Gson
import io.reactivex.Observable
import org.jetbrains.anko.doAsync
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/17/18.
 */
open class QualityControlRepository @Inject constructor(private val qualityControlApi: QualityControlApi,
                                                        private val apiAuthorization: ApiAuthorization,
                                                        private val database: ControlQualityDao) {

    private val TAG = "---> ${this.javaClass.simpleName}"

    // ------------------------------ CREATE ------------------------------

    fun saveLocalControlListActivities(collaboratorId: Int, list: List<ControlQualityActivitiesListModel>) {
        Log.d(TAG, "saveLocalControlListActivities")
        Observable.fromCallable {
            list.forEach {
                it.collaboratorId = collaboratorId.toLong()
                database.insertControlQualityActivity(it)
                it.grooves.map { groove ->
                    groove.controlQualityId = it.activityLogId
                }
                database.insertControlQualityGrooves(it.grooves)
            }
        }.subscribe()
    }

    fun saveControlQualityList(list: List<ControlQualityListModel>, stageId: Int) {
        Log.d(TAG, "saveControlQualityList")
        Observable.fromCallable {
            list.map { it.stageId = stageId }
            database.insertControlQuality(list)
        }.subscribe()
    }

    fun saveLocalListOfIssues(activityId: Int, areaId: Int, category: Int, subCategory: Int, issues: List<IssueModel>) {
        Observable.fromCallable {
            issues.map {
                it.activityId = activityId
                it.areaId = areaId
                it.category = category
                it.subCategory = subCategory
            }
            database.insertIssues(issues)
        }.subscribe()
    }

    fun createIssue(token: String, issues: List<ListIssueRequest>, comment: String, grooveId: Int, activityLogId: Int, collaboratorId: Int): Observable<Any> {
        doAsync {
            createLocalIssue(issues, comment, grooveId, activityLogId, collaboratorId).subscribe()
        }
        val request = ReviewIssueRequest(issues, GrooveIssueRequest(comment, grooveId), collaboratorId)
        return qualityControlApi.createIssue(apiAuthorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun createLocalIssue(issues: List<ListIssueRequest>, comment: String, grooveId: Int, activityLog: Int, collaboratorId: Int): Observable<Any> {
        val request = ReviewIssueRequest(issues, GrooveIssueRequest(comment, grooveId), collaboratorId)
        Log.d(TAG, "createLocalIssue: ${Gson().toJson(request)}")
        return Observable.fromCallable {
            database.incrementTotalControlQualityActivity(activityLog)
            database.updateControlQualityGrooves(true, grooveId)
            database.insertReviewIssue(request)
        }
    }


    // ------------------------------ READ ------------------------------

    fun getControlList(token: String, stageId: Int): Observable<List<ControlQualityListModel>> {
        return qualityControlApi.getCondensedCollaborators(apiAuthorization.getAuthToken(token), stageId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalControlList(stageId: Int): Observable<List<ControlQualityListModel>> {
        Log.d(TAG, "getLocalControlList")
        return Observable.fromCallable {
            database.getListControlQuality(stageId)
        }
    }

    fun getControlListActivities(token: String, collaboratorId: Int, stageId: Int): Observable<List<ControlQualityActivitiesListModel>> {
        Log.d(TAG, "getControlListActivities")
        return qualityControlApi.getControlListActivities(apiAuthorization.getAuthToken(token), collaboratorId, stageId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalControlListActivities(collaboratorId: Int): Observable<List<ControlQualityActivitiesListModel>> {
        Log.d(TAG, "getLocalControlListActivities")
        return Observable.fromCallable {
            val activities = database.getControlQualityActivities(collaboratorId)
            activities.map { activity ->
                activity.grooves = database.getControlQualityGrooves(activity.activityLogId)
            }
            activities
        }
    }

    fun getListOfIssues(token: String, activityId: Int, areaId: Int, category: Int, subCategory: Int): Observable<IssuesResponse> {
        return qualityControlApi.getListOfIssues(apiAuthorization.getAuthToken(token), activityId, areaId, category, subCategory)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalListOfIssues(activityId: Int, areaId: Int, category: Int, subCategory: Int): Observable<IssuesResponse> {
        return Observable.fromCallable {
            IssuesResponse(database.getIssues(activityId, areaId, category, subCategory))
        }
    }

    // ------------------------------ UPDATE ------------------------------

    fun finalizeControlActivity(token: String, stageId: Int, qualitySign: String, supervisorSign: String): Observable<Any> {
        Log.d(TAG, "finalizeControlActivity")
        val request = SignQualityRequest(qualitySign, supervisorSign)
        return qualityControlApi.finalizeControlActivity(apiAuthorization.getAuthToken(token), stageId, request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun syncControlQualityIssues(token: String, request: SyncControlQualityRequest): Observable<SyncControlQualityResponse> {
        Log.d(TAG, "syncControlQualityIssues")
        return qualityControlApi.syncQualityControlIssues(apiAuthorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }


}