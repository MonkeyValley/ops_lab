package com.amplemind.vivasmart.features.haulage.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment

class HaulageNumberDialog: BaseDialogFragment() {

    companion object {
        val TAG = HaulageNumberDialog::class.java.simpleName

        fun newInstance(number: String): HaulageNumberDialog {
            val dialog = HaulageNumberDialog()
            val args = Bundle()
            args.putString("number", number)
            dialog.arguments = args
            return dialog
        }
    }

    private var listener: OnHaulageComplete? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_haulage_number, null, false)
        builder.setView(mView)

        mView.findViewById<TextView>(R.id.tv_haulage_number).text = arguments!!.getString("number")
        builder.setPositiveButton("Cerrar", null)
        builder.setCancelable(false)
        return builder.create()
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED)
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            listener?.onDismissDialog()
            dialog!!.dismiss()
        }
    }

    fun setOnHaulageComplete(listener: OnHaulageComplete) {
        this.listener = listener
    }


}

interface OnHaulageComplete {
    fun onDismissDialog()
}