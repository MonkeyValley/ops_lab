package com.amplemind.vivasmart.core.repository.remote.models

import com.google.gson.annotations.SerializedName

data class HarvestValidationCarryOrderModel (
        @SerializedName("carry_order_id")       val carryOrderId: Long,
        @SerializedName("box_no")               val boxNo: Double,
        @SerializedName("lot_id")               val lotId: Long,
        @SerializedName("lot_name")             val lotName: String,
        @SerializedName("has_harvest_quality")  val hasHarvestQuality: Boolean?,
        @SerializedName("harvest_quality_ok")   val harvestQualityOk: Boolean?
)
