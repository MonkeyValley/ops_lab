package com.amplemind.vivasmart.vo_core.repository.remote

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface PayrollActivitiesApi {

    @GET("/v2/activity/sync")
    @Headers("Content-Type: application/json")
    fun getActivities(
            @Header("Authorization")    authentication: String,
            @Query("embed")             embed: String = "crops,unit",
            @Query("activity_area_id")  activityCategoryId: Long = 1,
            @Query("__logic")           logic: String = "AND",
            @Query("business_unit_id")  businessUnitId: Long,
            @Query("version__gte")       version: Long? = null
            //@Query("is_active")         isEnabled: Boolean = true,
            //@Query("order_by")              orderBy: String = "name.asc"
            //@Query("crop_activity.crop_id") crop_id: Int,
            //@Query("stage_activities")      stageActivities: Int,
            //@Query("limit")             limit: Int = 100000
    ) : Single<ObjectResponse<ActivityModel>>
}