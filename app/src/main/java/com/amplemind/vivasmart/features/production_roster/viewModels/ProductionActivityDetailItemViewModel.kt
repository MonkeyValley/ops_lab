package com.amplemind.vivasmart.features.production_roster.viewModels

import androidx.databinding.ObservableBoolean
import com.amplemind.vivasmart.core.repository.model.ActivityCodeModel

class ProductionActivityDetailItemViewModel constructor(val model: ActivityCodeModel?, val code: Int, val price: String) {

    val id = model?.id
    var isDone = false
    val totalActivity = model?.total ?: "0"
        get() = if (isNewCode()) "+" else if (field == "None") "0" else field
    val isActive: Boolean
    val isActiveRow = ObservableBoolean(false)

    val remaining = model?.remaining

    init {
        val values = model?.total?.split("/")
        if (values != null && values.size == 2) {
            val initalValue = values[0].toIntOrNull()
            val finalValue = values[1].toIntOrNull()
            isDone = (initalValue ?: 0 >= finalValue ?: 0) && !model!!.activeCollaborators
        }
        isActive = model != null
        isActiveRow.set(isActive)
    }

    fun isNewCode(): Boolean {
        return model == null
    }

    fun getStringCode(): String {
        return code.toString()
    }
}