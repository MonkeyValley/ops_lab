package com.amplemind.vivasmart.core.repository.model

import android.os.Parcel
import android.os.Parcelable

data class TimesCollaborator(val id_collaborator: Int, val time: Long) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readLong()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id_collaborator)
        parcel.writeLong(time)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TimesCollaborator> {
        override fun createFromParcel(parcel: Parcel): TimesCollaborator {
            return TimesCollaborator(parcel)
        }

        override fun newArray(size: Int): Array<TimesCollaborator?> {
            return arrayOfNulls(size)
        }
    }
}
