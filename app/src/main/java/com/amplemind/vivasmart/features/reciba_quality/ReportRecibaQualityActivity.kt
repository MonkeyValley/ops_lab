package com.amplemind.vivasmart.features.reciba_quality

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.features.reciba_quality.adapter.ReportRecibaQualityAdapter
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ItemReportRecibaQualityViewModel
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ReportRecibaQualityViewModel
import com.github.badoualy.datepicker.DatePickerTimeline
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_report_quality_finished_product.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.*
import javax.inject.Inject

class ReportRecibaQualityActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ReportRecibaQualityViewModel
    var fecha : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_reciba_quality)

        setupToolbar()
        addSubscribers()

        loadDateTime()

        loadReportList()
    }

    fun ifConected(): Boolean{
        return if(this.hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

    private fun loadReportList() {
        viewModel.getReportList(fecha!!)
    }

    private fun loadDateTime(){
        val date = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        var cal_begin = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        cal_begin.add(Calendar.DATE, -15)

        Log.e("date", "" + date.get(Calendar.DST_OFFSET))
        Log.e("cal_begin", "" + date.get(Calendar.DST_OFFSET))

        if(date.get(Calendar.DST_OFFSET) != 0){
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) +"-"+ cal_begin.get(Calendar.MONTH)+"-"+cal_begin.get(Calendar.DAY_OF_MONTH)+1)
            timeline.setLastVisibleDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH)+1)
            timeline.setSelectedDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH)+1)
        } else {
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH))
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) +"-"+ cal_begin.get(Calendar.MONTH)+"-"+cal_begin.get(Calendar.DAY_OF_MONTH))
            timeline.setLastVisibleDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH))
            timeline.setSelectedDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH))
        }

        var sti =  date.get(Calendar.YEAR).toString()
        fecha = ( ( date.get(Calendar.YEAR)).toString() + "-" +(date.get(Calendar.MONTH) + 1).toString() + "-" + (date.get(Calendar.DAY_OF_MONTH)).toString() )

        val stringYear = sti.subSequence(2, 4)
        timeline.setDateLabelAdapter {
            calendar,
            index -> Integer.toString(calendar[Calendar.MONTH] + 1) + "/" + stringYear
        }
        timeline.onDateSelectedListener = DatePickerTimeline.OnDateSelectedListener { year, month, day, index ->
            Log.d("dateSelected", "year:$year month:$month day:$day index: $index")
            fecha = ((year).toString() + "-" + (month + 1).toString() + "-" + (day).toString())
            if (ifConected()) {
                loadReportList()
            }
        }
    }


    private fun setupToolbar() {
        val mToolbar = toolbar_report_quality_finished
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.reciba_quality)
        lbl_track_title.text = "/ Calidad / Empaque / Calidad reciba /"
        lbl_track_title.setTextColor(this.resources.getColor(R.color.white))
        icon_track_title.setColorFilter(this.resources.getColor(R.color.white))
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest().subscribe(this::updateList).addTo(subscriptions)
    }

    private fun updateList(list: List<ItemReportRecibaQualityViewModel>) {
        Log.d("lista", Gson().toJson(list))
        val adapter = ReportRecibaQualityAdapter(list.toMutableList())
        rv_quality_finished.hasFixedSize()
        rv_quality_finished.layoutManager = LinearLayoutManager(this)
        rv_quality_finished.itemAnimator = DefaultItemAnimator()
        rv_quality_finished.adapter = adapter

        adapter.onClick().subscribe(this::onClickItem).addTo(subscriptions)
    }


    private fun onClickItem(item: ItemReportRecibaQualityViewModel) {
        if(ifConected()) {

            if (item.percentInt == null) intent = Intent(this, RecibaQualityReviewActivity::class.java)
            else intent = Intent(this, RecibaQualityDetailActivity::class.java)

            intent.putExtra("id", item.id)
            intent.putExtra("lotName", item.lot_name)
            intent.putExtra("table", item.table)

            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==200) {
            if (resultCode == Activity.RESULT_OK) {
                loadReportList()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val TAG = "RepQualFinishProd"
    }

    override fun onResume() {
        super.onResume()
        loadReportList()
    }
}
