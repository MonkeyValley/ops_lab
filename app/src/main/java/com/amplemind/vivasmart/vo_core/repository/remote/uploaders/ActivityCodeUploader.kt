package com.amplemind.vivasmart.vo_core.repository.remote.uploaders

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.dao.UploadObjectDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.annotations.Uploadable
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

class ActivityCodeUploader @Inject constructor(
        mPref: UserAppPreferences,
        mDao: UploadObjectDao
) : Uploader(mPref, mDao){

    fun create(obj: ActivityCodeModel): UploadObjectModel {
        val body = CreateCodeBody(
                obj.uuid,
                obj.activityId.toLong(),
                obj.stageId.toLong(),
                obj.code
        )
        return buildRequest(body, POST)
    }

    @Uploadable(
            path =          "activity_code",
            contentType =   "application/json"
    )
    private data class CreateCodeBody (
            @SerializedName("uuid")         val uuid: String,
            @SerializedName("activity_id")  val activityId: Long,
            @SerializedName("stage_id")     val stageId: Long,
            @SerializedName("code")         val code: Int
    )

}