package com.amplemind.vivasmart.features.reciba_quality.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.response.*
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.reciba_quality.repository.remote.RecibaQualityApi
import com.amplemind.vivasmart.features.reciba_quality.repository.response.ReportRecibaQualityResponse
import com.amplemind.vivasmart.features.reciba_quality.repository.response.RewviewDetailRecibaQualityResponse
import com.amplemind.vivasmart.features.reciba_quality.repository.response.VariatiesRecibaQualityResponse
import com.google.gson.Gson
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RecibaQualityRepository @Inject constructor(private val api: RecibaQualityApi,
                                                  private val authorization: ApiAuthorization,
                                                  private val preferences: UserAppPreferences) {

    data class ReportFinishedResponse(val token: String)

    val businessUnitId = Gson().fromJson(preferences.userInfo, UserModel::class.java).businessUnitId
            ?: ""

    fun getReportList(day: String): Observable<List<ReportRecibaQualityResponse>> {
        return api.getReportList(authentication = authorization.getAuthToken(preferences.token), day = day, businessId = businessUnitId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getVarieties(carryOrderId: String): Observable<VariatiesRecibaQualityResponse> {
        return api.getVarieties(authentication = authorization.getAuthToken(preferences.token), id = carryOrderId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getUnits(): Observable<UnitResponse> {
        return api.getUnits(authentication = authorization.getAuthToken(preferences.token),
                business_unit_id = businessUnitId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getDetailList(carryOrderId: String): Observable<List<RewviewDetailRecibaQualityResponse>> {
        return api.getDetail(authentication = authorization.getAuthToken(preferences.token), id = carryOrderId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

}
