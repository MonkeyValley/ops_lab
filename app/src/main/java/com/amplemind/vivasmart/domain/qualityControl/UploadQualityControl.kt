package com.amplemind.vivasmart.domain.qualityControl

import android.util.Log
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.platform.SyncNotification
import com.amplemind.vivasmart.core.platform.SynchronizeDataBaseService
import com.amplemind.vivasmart.core.repository.ProductionQualityRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.ProductionQualityDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ProductionQualityOfflineRequest
import com.amplemind.vivasmart.core.repository.model.SyncReportModel
import com.amplemind.vivasmart.core.repository.request.CreateIssueRequest
import com.amplemind.vivasmart.core.repository.request.ReportIssuesRequest
import com.amplemind.vivasmart.core.repository.request.SignModel
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.google.gson.annotations.Until
import org.jetbrains.anko.doAsync
import java.io.File
import javax.inject.Inject
import kotlin.math.sign

class UploadQualityControl @Inject constructor(private val picturesHelper: PicturesHelper,
                                               private val uploadRepository: ProfileRepository,
                                               private val preferences: UserAppPreferences,
                                               private val database: ProductionQualityDao,
                                               private val repository: ProductionQualityRepository) {

    private var delegate: SyncNotification? = null
    private val type = SynchronizeDataBaseService.SynchronizeRequest.CONTROL_QUALITY
    private var subscriptions: AndroidDisposable? = null

    fun syncIssues(delegate: SyncNotification, subscriptions: AndroidDisposable) {
        this.delegate = delegate
        this.subscriptions = subscriptions

        doAsync({
            Log.e("Error", "syncIssues ${it.message}")
            delegate.onTerminate(mutableListOf(),type,"syncIssues ${it.message}")
        }, {
            uploadImageReports(database.getIssuesPendingToSend(),
                    database.getProductionQualityOfflineRequest(),
                    0)
        })
    }

    private fun uploadImageReports(reports: List<CreateIssueRequest>, signs: List<ProductionQualityOfflineRequest>, position: Int) {
        if (position < reports.size) {
            if (!File(reports[position].image).exists()) {
                uploadImageReports(reports, signs, position + 1)
                return
            }
            uploadImage(reports[position].image,
                    { path ->
                        reports[position].image = path
                        uploadImageReports(reports, signs, position + 1)
                    },
                    { error -> uploadImageReports(reports, signs, position + 1) })
        } else {
            uploadSigns(reports, signs, 0, true)
        }
    }

    private fun uploadSigns(reports: List<CreateIssueRequest>, signs: List<ProductionQualityOfflineRequest>, position: Int, signCollaborator: Boolean) {
        if (position < signs.size) {
            val sign = if (signCollaborator) signs[position].signCollaborator else signs[position].signSuper
            val signPosition = if (signCollaborator) position else position + 1
            if (!File(sign).exists()) {
                uploadSigns(reports, signs, signPosition, !signCollaborator)
                return
            }
            uploadImage(sign,
                    { path ->
                        if (signCollaborator) {
                            signs[position].signCollaborator = path
                        } else {
                            signs[position].signSuper = path
                        }
                        uploadSigns(reports, signs, signPosition, !signCollaborator)
                    },
                    { error -> uploadSigns(reports, signs, signPosition, !signCollaborator) })
        } else {
            syncReports(reports, signs)
        }
    }

    private fun uploadImage(image: String, complete: (String) -> Unit, error: (String) -> Unit) {
        val file = File(image)
        picturesHelper.compressImage(file, file, 400f, 400f)
        uploadRepository.uploadImage(file, "user")
                .subscribe({
                    complete(it.relative_url)
                }, {
                    error(it.message ?: "")
                }).addTo(subscriptions!!)
    }

    private fun deleteIssues(issues: List<CreateIssueRequest>) {
        database.deleteAllProductionQualityOfflineRequest()
        issues.map { issue ->
            if (issue.status) {
                database.incrementCountIssues(issue.issueId, -1)
                database.incrementCountInCategory(issue.categoryId, -1)
                database.deleteIssuePendingToSend(issue.dbPrimaryKey ?: 0)
            }
        }
    }

    private fun syncReports(issues: List<CreateIssueRequest>, signs: List<ProductionQualityOfflineRequest>) {
        repository.syncIssuesReports(preferences.token, ReportIssuesRequest(issues,
                signs.map { SignModel(it.lotId, it.signCollaborator, it.signSuper, it.id ?: 0) }))
                .subscribe({ reportsSent ->
                    val reports = mutableListOf<SyncReportModel>()
                    reportsSent.data.map { issue ->
                        if (reports.filter { it.offline_id == issue.stageId }.isEmpty()) {
                            val total = reportsSent.data.filter { it.stageId == issue.stageId }.size
                            val complete = if (issue.status) total else 0

                            reports.add(SyncReportModel("Invernadero ${issue.stageId}", total, complete, issue.stageId))
                        }
                    }

//                    reports.addAll(reportsSent.signs.map {
//                        val total = 1
//                        val complete = if (it.status) total else 0
//                        SyncReportModel("Lote ${it.stageId}", total, complete)
//                    })
                    delegate?.onTerminate(reports, type)
                    deleteIssues(reportsSent.data)
                }, {
                    Log.e("Fail upload", "")
                    val reports = issues.map {
                        SyncReportModel(it.description, 1, 0)
                    }
                    delegate?.onTerminate(reports, type)
                }).addTo(subscriptions!!)
    }

}