package com.amplemind.vivasmart.features.packaging_quality

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualityCarryOrderListFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualityFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualityRevisionListFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.ReceptionQualitySampleDialogFragment
import kotlinx.android.synthetic.main.activity_reception_quality.*

class ReceptionQualityActivity : BaseActivity() {

    companion object {

        private val TAG = ReceptionQualityActivity::class.java.simpleName

        private val TAG_SAMPLE_DIALOG_FRAGMENT = "$TAG.${ReceptionQualitySampleDialogFragment::class.java.simpleName}"
        private val TAG_QUALITY_FRAGMENT = "$TAG.${ReceptionQualityFragment::class.java.simpleName}"
        private val TAG_REVISION_LIST_FRAGMENT = "$TAG.${ReceptionQualityRevisionListFragment::class.java.simpleName}"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reception_quality)


        setUpUI()
        setUpUICallbacks()
    }

    private fun setUpUI() {
        setUpToolbar()
        showCarryOrderListFragment()
    }

    private fun setUpUICallbacks() {
        mEventBus.observe(this::onSetToolbarTitleEvent).addTo(subscriptions)
        mEventBus.observe(this::onCarryOrderClicked).addTo(subscriptions)
        mEventBus.observe(this::onSampleSizeCaptured).addTo(subscriptions)
        mEventBus.observe(this::onRevisionFinished).addTo(subscriptions)
        mEventBus.observe(this::onCreateRevisionClicked).addTo(subscriptions)
    }

    private fun setUpToolbar() {
        val toolbar = toolbar_reception_quality.findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.title_activity_reception_quality)
    }

    private fun showCarryOrderListFragment() {
        supportFragmentManager.beginTransaction()
                .add(R.id.container, ReceptionQualityCarryOrderListFragment.newInstance())
                .commit()
    }

    private fun onCarryOrderClicked(event: ReceptionQualityCarryOrderListFragment.OnCarryOrderClickedEvent) {
        if (event.carryOrder.quality > 0) {
            supportFragmentManager.beginTransaction()
                    .addToBackStack(TAG_REVISION_LIST_FRAGMENT)
                    .replace(R.id.container,
                            ReceptionQualityRevisionListFragment.newInstance(event.carryOrder))
                    .commit()
        }
        else {
            ReceptionQualitySampleDialogFragment.newInstance(event.carryOrder)
                    .show(supportFragmentManager, TAG_SAMPLE_DIALOG_FRAGMENT)
        }
    }

    private fun onCreateRevisionClicked(event: ReceptionQualityRevisionListFragment.OnCreateRevisionClickedEvent) {
        if (event.carryOrder != null) {
            ReceptionQualitySampleDialogFragment.newInstance(event.carryOrder)
                    .show(supportFragmentManager, TAG_SAMPLE_DIALOG_FRAGMENT)
        }
        else {
            ReceptionQualitySampleDialogFragment.newInstance(event.carryOrderId)
                    .show(supportFragmentManager, TAG_SAMPLE_DIALOG_FRAGMENT)
        }
    }

    private fun onSampleSizeCaptured(event: ReceptionQualitySampleDialogFragment.OnOkClickedEvent) {
        supportFragmentManager.beginTransaction()
                .addToBackStack(TAG_QUALITY_FRAGMENT)
                .replace(R.id.container,
                        ReceptionQualityFragment.newInstance(event.carryOrderId, event.sampleSize, event.unit))
                .commit()
    }

    private fun onRevisionFinished(event: ReceptionQualityFragment.OnRevisionFinishedEvent) {
        onBackPressed()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            super.onBackPressed()
        }
        else {
            supportFragmentManager.popBackStack()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
