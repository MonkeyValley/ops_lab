package com.amplemind.vivasmart.core.repository.local

import androidx.room.*
import com.amplemind.vivasmart.core.repository.model.LotsPackageResultResponse
import com.amplemind.vivasmart.core.repository.model.RosterLineModel
import io.reactivex.Single

@Dao
interface PackingLineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLines(line: RosterLineModel)

    @Query("SELECT * FROM RosterLineModel ORDER BY name ASC")
    fun getLocalLines(): Single<List<RosterLineModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLots(lots : LotsPackageResultResponse)

    @Query("SELECT * FROM LotsPackageResultResponse ORDER BY name ASC")
    fun getLocalLots(): Single<List<LotsPackageResultResponse>>

    @Query("UPDATE RosterLineModel SET actual_stage = :stage_id WHERE line_id = :linesPackage")
    fun assignLotLocal(stage_id: Int, linesPackage: Int)

    @Query("SELECT actual_stage FROM RosterLineModel WHERE line_id = :linesPackage")
    fun getLotByLine(linesPackage: Int) : Int

}
