package com.amplemind.vivasmart.core.repository.model

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class CropLine(
        @PrimaryKey(autoGenerate = true) val id : Int,
        @SerializedName("crop_id") val crop_id : Int,
        @SerializedName("packingline_id") val packingline_id : Int
)
