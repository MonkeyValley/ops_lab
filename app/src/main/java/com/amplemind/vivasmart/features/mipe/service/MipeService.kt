package com.amplemind.vivasmart.features.mipe.service

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.Gson
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmResults
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class MipeService @Inject constructor() {

    var context: Context? = null

    lateinit var preferences: UserAppPreferences

    fun getLots(): Single<List<LotModel>> =
            Realm.getDefaultInstance().use {
                Single.create<List<LotModel>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->
                        val results = realm
                                .where(LotModel::class.java)
                                .isNotNull("soilId")
                                .distinct("id")
                                .findAll()
                        emitter.onSuccess(realm.copyFromRealm(results))
                    }
                }
            }

    fun getGroove(lotId: Int, table: String): Int {
        return Realm.getDefaultInstance().use {
            when (table) {
                "1" -> {
                    it.where(LotModel::class.java)
                            .equalTo("id", lotId)
                            .findFirst()?.table1 ?: 0
                }
                "2" -> {
                    it.where(LotModel::class.java)
                            .equalTo("id", lotId)
                            .findFirst()?.table2 ?: 0
                }
                "3" -> {
                    it.where(LotModel::class.java)
                            .equalTo("id", lotId)
                            .findFirst()?.table3 ?: 0
                }
                "4" -> {
                    it.where(LotModel::class.java)
                            .equalTo("id", lotId)
                            .findFirst()?.table4 ?: 0
                }
                "5" -> {
                    it.where(LotModel::class.java)
                            .equalTo("id", lotId)
                            .findFirst()?.table5 ?: 0
                }
                "6" -> {
                    it.where(LotModel::class.java)
                            .equalTo("id", lotId)
                            .findFirst()?.table6 ?: 0
                }
                else -> 0
            }
        }
    }

    fun getPlagueLotCrop(cropId: Int): Single<List<PlagueDetail>> =
            Realm.getDefaultInstance().use {
                Single.create<List<PlagueDetail>> { emitter ->
                    Realm.getDefaultInstance().use { realm ->

                        var result = realm
                                .where(PlaguesModel::class.java)
                                .equalTo("cropId", cropId)
                                .findAll()

                        var plagueDetailList = ArrayList<PlagueDetail>()

                        result.forEach { plague ->
                            if (!plagueDetailList.contains(plague.plagueDetail)) {
                                plagueDetailList.add(plague.plagueDetail!!)
                            }
                        }

                        var list = plagueDetailList.distinctBy { it.id }

                        emitter.onSuccess(realm.copyFromRealm(list))
                    }
                }
            }

    fun savePlantData(lotId: Int, table: String, groove: Int, plant: Int, quadrant: Int, id: Int?, medition: Int?, week: Int, lvl: Int?) {
        Realm.getDefaultInstance().use { realm ->

            val calendar = Calendar.getInstance()
            val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
            val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
            val todayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

            var newPlantsMonitoring: PlantsMonitoringModel?
            var result: MipeMonitoringModel?

            realm.executeTransaction {

                result = realm
                        .where(MipeMonitoringModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .equalTo("grooveNo", groove)
                        .and()
                        .equalTo("week", week)
                        .endGroup()
                        .findFirst()

                if (result == null) {
                    val newObj = it.createObject(MipeMonitoringModel::class.java)
                    newObj.lotId = lotId
                    newObj.table = table.toInt()
                    newObj.grooveNo = groove
                    newObj.date = todayDate
                    newObj.week = week
                    result = newObj
                }

                val newFindings = it.createObject(FindingsModel::class.java)
                if (medition != null) {
                    newFindings.plagueId = id
                    newFindings.finding = medition
                    newFindings.level = lvl
                }

                if (result!!.plants!!.where().equalTo("plantNo", plant).findFirst() == null) {
                    val newObj = it.createObject(PlantsMonitoringModel::class.java)
                    newObj.plantNo = plant
                    newObj.quadrant = quadrant
                    if (medition != null) newObj.findings!!.add(newFindings)
                    result!!.plants!!.add(newObj)
                } else {
                    if (medition != null) result!!.plants!!.where().equalTo("plantNo", plant).findFirst()!!.findings!!.add(newFindings)
                }

                it.insertOrUpdate(result)
            }

        }

    }

    fun savePlantGroove(lotId: Int, table: String, groove: Int, week: Int) {
        Realm.getDefaultInstance().use { realm ->
            val result = realm
                    .where(MipeMonitoringModel::class.java)
                    .beginGroup()
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("table", table.toInt())
                    .and()
                    .equalTo("grooveNo", groove)
                    .and()
                    .equalTo("week", week)
                    .endGroup()
                    .findFirst()

            if(result != null) {

                val gson = Gson()
                val rootObject = gson.toJson(realm.copyFromRealm(result))
                Log.e("MipeJson", rootObject)

                val prefer = context!!.getSharedPreferences(
                        context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

                Realm.getDefaultInstance().use {
                    it.executeTransaction { realm ->
                        val url = preferences.selectedServerUrl
                        val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                        uploadObject.verb = "POST"
                        uploadObject.url = url + "v2/mipe/monitoring"
                        uploadObject.contentType = "application/json"
                        uploadObject.body = rootObject.toString()
                        uploadObject.token = prefer.getString("token", "") ?: ""
                        uploadObject.order = 99
                        realm.insertOrUpdate(uploadObject)

                        result.finished = true
                        realm.insertOrUpdate(result)
                    }
                }
            }
        }
    }

    fun getCropId(lotId: Int) =
            Realm.getDefaultInstance().use {
                it.where(StageModel::class.java)
                        .equalTo("lotId", lotId)
                        .findFirst()
                        ?.cropId ?: 0
            }

}
