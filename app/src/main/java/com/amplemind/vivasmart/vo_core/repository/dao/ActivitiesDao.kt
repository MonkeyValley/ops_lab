package com.amplemind.vivasmart.vo_core.repository.dao

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.responses.ActivitiesResponse
import io.realm.Realm
import io.realm.Sort

class ActivitiesDao : RealmDao<ActivityModel>(ActivityModel::class.java) {

    override fun retrieveServerData(response: Any?): List<ActivityModel>? {
        return when (response) {
            is ActivitiesResponse -> return response.data
            else -> null
        }
    }

    fun findByStage(stage: StageModel): List<ActivityModel> {

        val realm = Realm.getDefaultInstance()

        val result = realm.where(mModelClazz)
                .equalTo("crops.cropId", stage.cropId)
                .sort("name", Sort.ASCENDING)
                .findAll()

        val ret = realm.copyFromRealm(result)

        ret.forEach { activity ->

            val count = realm.where(ActivityLogModel::class.java)
                    .equalTo("activityCode.activityUuid", activity.uuid)
                    .and()
                    .equalTo("activityCode.stageId", stage.id)
                    .and()
                    .equalTo("isDone", false)
                    .and()
                    .equalTo("isPending", false)
                    .count()

            activity.stageActivities = count > 0

        }

        realm.close()

        return ret
    }

    fun findByCrop(cropId: Long): List<ActivityModel> {

        val realm = Realm.getDefaultInstance()

        val result = realm.where(mModelClazz)
                .equalTo("crops.cropId", cropId)
                .findAll()

        val ret = realm.copyFromRealm(result)

        ret.forEach { activity ->

            val count = realm.where(ActivityLogModel::class.java)
                    .equalTo("activityCode.activityUuid", activity.uuid)
                    .and()
                    .equalTo("isDone", false)
                    .and()
                    .equalTo("isPending", false)
                    .count()

            activity.stageActivities = count > 0

        }

        realm.close()

        return ret
    }

    fun findByCrop(cropUuid: String): List<ActivityModel> {

        val realm = Realm.getDefaultInstance()

        val result = realm.where(mModelClazz)
                .equalTo("cropUuid", cropUuid)
                .findAll()

        val ret = realm.copyFromRealm(result)

        realm.close()

        return ret
    }


}