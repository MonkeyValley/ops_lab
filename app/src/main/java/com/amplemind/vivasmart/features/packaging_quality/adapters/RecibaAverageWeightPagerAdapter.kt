package com.amplemind.vivasmart.features.packaging_quality.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.amplemind.vivasmart.features.finished_product_quality.fragment.ProductQualityAverageWeightFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.BoxAverageWeightFragment
import com.amplemind.vivasmart.features.packaging_quality.fragment.PalleteAverageWeightFragment

class RecibaAverageWeightPagerAdapter(fm: FragmentManager, mode: Int, cropId: Long) : FragmentPagerAdapter(fm) {

    private val mList =
            if (mode == 1) {
                listOf(
                        FragmentContainer("Cajas", BoxAverageWeightFragment.newInstance(cropId)),
                        FragmentContainer("Pallets", PalleteAverageWeightFragment.newInstance())
                )
            } else {
                listOf(
                        FragmentContainer("Muestra", ProductQualityAverageWeightFragment.newInstance())
                )
            }

    override fun getCount(): Int {
        return mList.size
    }

    override fun getItem(position: Int): Fragment {
        return mList[position].fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mList[position].title
    }

    private data class FragmentContainer(
            val title: String,
            val fragment: Fragment
    )

    fun getAverageWeight(position: Int): Double {
        return (mList[position].fragment as AverageWeightFragment).calculateAverage()
    }

    interface AverageWeightFragment {
        fun calculateAverage(): Double
    }

}