package com.amplemind.vivasmart.features.production_roster

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.amplemind.vivasmart.R

class ExtraHoursRequestsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extra_hours_requests)
    }
}
