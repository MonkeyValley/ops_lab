package com.amplemind.vivasmart.features.haulage.viewModel

import android.util.Log
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import java.text.SimpleDateFormat
import java.util.*

class CarryOrderViewModel(private val model: CarryOrderModel) {

    val folio = model.folio
    val name = model.lot?.name ?: ""
    val id = model.id
    val lotName = "Lote: ${model.lot?.name ?: ""}"
    val cropId = model.cropId
    val cropName = model.crop?.name ?: ""
    val boxSize = "${model.boxNo}"
    val boxSizeNumber = model.boxNo
    val table = model.table
    val type = model.crop?.cropType?.name ?: ""
    val supervisorName = model.user?.name ?: ""
    val variety = model.crop?.variety ?: ""
    val comment = model.comment
    //TODO: It's not in the server right now
    val size = ""
    val temperature = "${model.temperature} ºC"
    val boxCounts =  model.varietyBoxCounts
    val secondQualityCount = model.secondQualityBox


    //This may change in the future
    fun getDate(): String {

        val origParser = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH)
        origParser.timeZone = TimeZone.getTimeZone("GMT")
        val parsedDate = origParser.parse(model.date)

        val destParser = SimpleDateFormat("dd/MM/yyyy HH:mm a", Locale.ENGLISH)
        destParser.timeZone = TimeZone.getTimeZone("GMT")
        Log.i("TIMEZONE", destParser.timeZone.displayName)

        return destParser.format(parsedDate)
    }
}