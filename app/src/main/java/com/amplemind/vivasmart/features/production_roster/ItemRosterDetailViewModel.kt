package com.amplemind.vivasmart.features.production_roster

import androidx.databinding.ObservableInt
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import com.amplemind.vivasmart.core.extensions.formatTime
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.model.DetailtRosterResponse
import java.util.*
import javax.inject.Inject

class ItemRosterDetailViewModel @Inject constructor(val section: String?, val model: DetailtRosterResponse?,
                                                    private val mediator: Mediator?) : Parcelable {

    var headerPosition = -1

    val activity = model?.activity_code?.activity?.name
    var isTraining = model?.isTraining ?: false
        private set
    val isPractice = model?.isPractice ?: false
    val isHead = model?.activity_code?.activity?.isHead ?: false
    val lote = model?.activity_code?.stageId
    var cant = model?.quantity

    val packing_name = String.format("• %s", if (model?.isTimeLine == true) "Tiempo" else model?.packing_name)

    var cost = calculateCost()
        private set
    val costPerHour = model?.activity_code?.activity?.costPerHour

    var timeMinutes = (model?.time ?: 0) / 60

    var timeExtra = (model?.extra_time ?: 0) / 60

    var timeQuantityExtra = (model?.extra_quantity ?: 0.0) / 60

    var extraQuantity = if ((model?.extra_quantity ?: 0.0) > 0) ((model?.extra_quantity ?: 0.0) / 3600) else 0.0

    var money = moneyCalc(cost)
    var moneyTraining = moneyCalc(costPerHour ?: cost ?: 0.0, true)
    var moneyPractice = moneyCalc(model?.activity_code?.activity?.practiceCost ?: cost ?: 0.0, true)
    var moneyHrsExtra = moneyCalcHrExtra(model?.activity_code?.activity?.extra_time_cost ?: 0.0)

    var status = isFinish()

    var statusSync = model?.statusSync

    var hrs = formatTime(getTimeInSeconds())

    var id_log = model?.id
    val activityLogIdLocalDB = model?.activityLogIdLocalDB

    val collaborator_id = model?.collaboratorId

    val activity_code = model?.activityCodeId
    val activity_id = model?.activity_code?.activityId

    var showSection: ObservableInt
    var showItem: ObservableInt

    var position: Int? = null

    val isTimeLine = model?.isTimeLine ?: false

    init {
        if (section == null) {
            showSection = ObservableInt(View.GONE)
            showItem = ObservableInt(View.VISIBLE)
        } else {
            showSection = ObservableInt(View.VISIBLE)
            showItem = ObservableInt(View.GONE)
        }
    }

    fun updateIsTraining(training: Boolean) {
        isTraining = training
        cost = calculateCost()
        money = moneyCalc(cost)
    }

    fun cantFormat(): String {
        if (model?.isLine == true) {
            return "%.2f".format(cant)
        }
        return "${cant?.toInt() ?: 0}"
    }

    private fun calculateCost(): Double {
        if (model == null) return 0.0
        if (isTraining) {
            return model.activity_code?.activity?.costPerHour
                    ?: model.activity_code?.activity?.cost
                    ?: model.activity_code?.activity?.unit?.cost ?: 0.0
        }
        if (isPractice) {
            return model.activity_code?.activity?.practiceCost
                    ?: model.activity_code?.activity?.cost
                    ?: model.activity_code?.activity?.unit?.cost ?: 0.0
        }
        if (model.isLine && !model.isTimeLine) {
            return model.presentationCost
        }
        if (model.isLine && model.isTimeLine) {
            return model.activity_code?.activity?.cost
                    ?: model.activity_code?.activity?.unit?.cost ?: 0.0
        }
        return model.activity_code?.activity?.cost ?: model.activity_code?.activity?.unit?.cost
        ?: 0.0
    }

    fun isTrainingEnabled(): Boolean {
        if (model?.activity_code?.activity?.code == null || model.activity_code.activity!!.code == 0) {
            return false
        }
        return true
    }

    fun getTimeInSeconds(): Long {
        return timeMinutes * 60L
    }

    fun isGroove(): Boolean? {
        return model?.activity_code?.activity?.isGrooves ?: false
    }

    fun isCostByHrs(): Boolean {
        val type = model?.activity_code?.activity?.activityTypeId
                ?: ItemActivitiesPayrollViewModelNew.PAYMENTTYPE.PIECE.type
        return type == ItemActivitiesPayrollViewModelNew.PAYMENTTYPE.HRS.type
    }

    private fun moneyCalcHrExtra(costAcivity: Double): Double {
        return calculatePaymentByHrs(timeQuantityExtra.toFloat() / 60f, costAcivity) + calculatePaymentByHrs(timeExtra.toFloat() / 60f, costAcivity)
    }

    private fun moneyCalc(costAcivity: Double, isTraining: Boolean = false): Double {
        val isSunday = model?.activity_code?.activity?.sunday_rate ?: false
        return if (!model!!.isTimeLine && !isTraining && !isPractice && model.isLine) {
            val result = (cant ?: 0.0) * model.presentationCost
            if (isSunday) isCostSunday(result) else result
        } else if (isCostByHrs() || isTraining || isPractice || model.isTimeLine) {
            val result = calculatePaymentByHrs(timeMinutes.toFloat() / 60f, costAcivity)
            if (isSunday) isCostSunday(result) else result
        } else {
            var result = (cant ?: 0.0) * costAcivity
            if (isHead) {
                result *= mediator?.bundleControlQualityItemViewModel!!.plantPerGroove
            }
            if (isSunday) isCostSunday(result) else result
        }
    }

    private fun isCostSunday(cost: Double): Double {
        return cost * 1.5
    }

    private fun calculatePaymentByHrs(time: Float, cost: Double): Double {
        val hrs = time.toInt()
        val half_hours = time - hrs

        var payment = hrs * cost
        if (half_hours >= 0.75) {
            payment += cost
        }
        return payment
    }

    private fun isFinish(): Boolean {
        if (model?.sign == null) return false
        return !model.sign.contains("No Sign")
    }

    fun setPosition(pos: Int) {
        this.position = pos
    }

    fun changeValues(cant: Double, time: Int) {
        this.cant = cant
        this.timeMinutes = time
        this.hrs = formatTime(getTimeInSeconds())
        money = moneyCalc(cost)
        moneyTraining = moneyCalc(costPerHour ?: cost ?: 0.0, true)
        moneyPractice = moneyCalc(model?.activity_code?.activity?.practiceCost ?: cost ?: 0.0, true)
        moneyHrsExtra = moneyCalcHrExtra(model?.activity_code?.activity?.extra_time_cost ?: 0.0)
    }

    fun formatMoney(): String {
        if (isTraining) {
            return "$%.2f".format(Locale.US, moneyTraining)
        }
        if (isPractice) {
            return "$%.2f".format(Locale.US, moneyPractice)
        }
        return "$%.2f".format(Locale.US, money)
    }

    fun formatMoneyPackage(): String {
        if (isTraining) {
            return "%.2f".format(Locale.US, (moneyTraining + moneyHrsExtra))
        }
        if (isPractice) {
            return "%.2f".format(Locale.US, (moneyPractice + moneyHrsExtra))
        }
        return "%.2f".format(Locale.US, (money + moneyHrsExtra))
    }

    fun changeGrooves(cant: Int) {
        this.cant = cant.toDouble()
        money = moneyCalc(cost)
    }


    //region code Parcelable
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readParcelable(DetailtRosterResponse::class.java.classLoader),
            parcel.readParcelable(Mediator::class.java.classLoader)) {
        cant = parcel.readValue(Double::class.java.classLoader) as? Double
        timeMinutes = parcel.readInt()
        money = parcel.readDouble()
        moneyTraining = parcel.readDouble()
        moneyPractice = parcel.readDouble()
        status = parcel.readByte() != 0.toByte()
        statusSync = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        hrs = parcel.readString()!!
        id_log = parcel.readValue(Int::class.java.classLoader) as? Int
        showSection = parcel.readParcelable(ObservableInt::class.java.classLoader)!!
        showItem = parcel.readParcelable(ObservableInt::class.java.classLoader)!!
        position = parcel.readValue(Int::class.java.classLoader) as? Int
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(section)
        parcel.writeParcelable(model, flags)
        parcel.writeParcelable(mediator, flags)
        parcel.writeValue(cant)
        parcel.writeInt(timeMinutes)
        parcel.writeDouble(money)
        parcel.writeDouble(moneyTraining)
        parcel.writeDouble(moneyPractice)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeValue(statusSync)
        parcel.writeString(hrs)
        parcel.writeValue(id_log)
        parcel.writeParcelable(showSection, flags)
        parcel.writeParcelable(showItem, flags)
        parcel.writeValue(position)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ItemRosterDetailViewModel> {
        override fun createFromParcel(parcel: Parcel): ItemRosterDetailViewModel {
            return ItemRosterDetailViewModel(parcel)
        }

        override fun newArray(size: Int): Array<ItemRosterDetailViewModel?> {
            return arrayOfNulls(size)
        }
    }
    //endregion


}
