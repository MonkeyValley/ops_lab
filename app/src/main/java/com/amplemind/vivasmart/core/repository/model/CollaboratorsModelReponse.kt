package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CollaboratorsReponseActivities(
        @SerializedName("data") val data: ArrayList<CollaboratorsListReponse>,
        @SerializedName("total_count") val totalCount: Int)

@Entity
data class CollaboratorsListReponse(
        @SerializedName("activity_code_id") val activityCodeId: Int,
        @Embedded @SerializedName("collaborator") val collaborator: CollaboratorsReponse,
        @SerializedName("collaborator_id") val collaboratorId: Int,
        @SerializedName("id") @PrimaryKey(autoGenerate = true) var id: Int?,
        @SerializedName("is_active") val isActive: Boolean,
        @SerializedName("is_done") val isDone: Boolean,
        @SerializedName("time_counter") var timerCount: Double,
        @SerializedName("is_paused") val isPaused: Boolean,
        @SerializedName("is_time") val is_time: Boolean?,
        @SerializedName("temp_time") val localTime: Long,
        @SerializedName("packing_id") val packing_id : Int? = -1,
        @SerializedName("packingline_id") val packingline_id : Int? = -1,
        @SerializedName("name_presentation") val name_presentation : String? = null,
        @Expose(serialize = false, deserialize = false) var date : Long,
        @Expose(serialize = false, deserialize = false) var stageId : Int = 0,
        @Expose(serialize = false, deserialize = false) var activityId : Int = 0,
        @Expose(serialize = false, deserialize = false) var activityCode : Int = 0
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readParcelable(CollaboratorsReponse::class.java.classLoader)!!,
            parcel.readInt(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readDouble(),
            parcel.readByte() != 0.toByte(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readLong(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readLong(),
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(activityCodeId)
        parcel.writeParcelable(collaborator, flags)
        parcel.writeInt(collaboratorId)
        parcel.writeValue(id)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeByte(if (isDone) 1 else 0)
        parcel.writeDouble(timerCount)
        parcel.writeByte(if (isPaused) 1 else 0)
        parcel.writeValue(is_time)
        parcel.writeLong(localTime)
        parcel.writeValue(packing_id)
        parcel.writeValue(packingline_id)
        parcel.writeString(name_presentation)
        parcel.writeLong(date)
        parcel.writeInt(stageId)
        parcel.writeInt(activityId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CollaboratorsListReponse> {
        override fun createFromParcel(parcel: Parcel): CollaboratorsListReponse {
            return CollaboratorsListReponse(parcel)
        }

        override fun newArray(size: Int): Array<CollaboratorsListReponse?> {
            return arrayOfNulls(size)
        }
    }


}

@Entity
data class CollaboratorsReponse(
        @SerializedName("birthdate") val birthdate: String?,
        @SerializedName("employee_code") val employeeCode: String,
        @SerializedName("id") @ColumnInfo(name = "collaborator_id_") @PrimaryKey(autoGenerate = true) val id: Int?,
        @SerializedName("image") val image: String?,
        @SerializedName("name") val name: String,
        @SerializedName("activity_logs") var activityLogs: List<GroovesResultReponse>? = null
) :Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()!!,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString()!!,
            parcel.createTypedArrayList(GroovesResultReponse)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(birthdate)
        parcel.writeString(employeeCode)
        parcel.writeValue(id)
        parcel.writeString(image)
        parcel.writeString(name)
        parcel.writeTypedList(activityLogs)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CollaboratorsReponse> {
        override fun createFromParcel(parcel: Parcel): CollaboratorsReponse {
            return CollaboratorsReponse(parcel)
        }

        override fun newArray(size: Int): Array<CollaboratorsReponse?> {
            return arrayOfNulls(size)
        }
    }
}