package com.amplemind.vivasmart.features.phenology.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.LotPhenologyItemBinding
import com.amplemind.vivasmart.features.phenology.viewmodel.LotPhenologyItemViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class LotsInLinesPhenologyAdapter : RecyclerView.Adapter<LotsInLinesPhenologyAdapter.LotsPhenologyViewHolder>() {
    private var list = mutableListOf<LotPhenologyItemViewModel>()

    var listener: Listener? = null

    private val clickSubject = PublishSubject.create<LotPhenologyItemViewModel>()

    private var mRecyclerView: RecyclerView? = null

    fun addElements(data: List<LotPhenologyItemViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LotsPhenologyViewHolder {
        val binding = LotPhenologyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LotsPhenologyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: LotsPhenologyViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem() = clickSubject

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        mRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mRecyclerView = null
    }

    inner class LotsPhenologyViewHolder(private val binding: LotPhenologyItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: LotPhenologyItemViewModel

        fun bind(item: LotPhenologyItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.root.setOnClickListener {
                clickSubject.onNext(this.item)
            }
        }

    }

    interface Listener {
        fun onGetCounterValue( osition: Int): Observable<Int>?
    }

}
