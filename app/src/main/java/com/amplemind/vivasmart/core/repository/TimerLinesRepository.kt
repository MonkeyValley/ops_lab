package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.extensions.calculatePaymentByHrs
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.local.CollaboratorsLineDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.remote.BoxCountLinesRepository
import com.amplemind.vivasmart.core.repository.remote.TimerLinesApi
import com.amplemind.vivasmart.core.repository.request.*
import com.amplemind.vivasmart.core.repository.response.ActivitiesResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorFinishCostResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.core.utils.TIME_OUT_SYNC
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserSectionViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import io.reactivex.Observable
import org.jetbrains.anko.doAsync
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class TimerLinesRepository @Inject constructor(private val api: TimerLinesApi,
                                                    private val authorization: ApiAuthorization,
                                                    private val userAppPreferences: UserAppPreferences,
                                                    private val mediator: Mediator,
                                                    private val database: CollaboratorsLineDao,
                                                    private val countBoxRepository: BoxCountLinesRepository) {


    fun getCollaboratorsByLine(): Observable<CollaboratorsInLinesResult> {
        return api.getCollaboratorsByLine(authorization.getAuthToken(userAppPreferences.token), packingline_id = mediator.bundleItemLinesPackage!!)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalCollaborators(): Observable<List<CollaboratorsInLinesResponse>> {
        val idLine = mediator.bundleItemLinesPackage!!
        return Observable.fromCallable {
            return@fromCallable mapCollaborators(database.getLocalLines(idLine) as MutableList<CollaboratorsInLinesResponse>, database.getLocalCollaboratorsInLine(idLine))
        }
    }

    private fun mapCollaborators(line: MutableList<CollaboratorsInLinesResponse>, collaborators: List<PackingLineCollaboratorsModel>): List<CollaboratorsInLinesResponse> {
        line.forEach { item ->
            val users = collaborators.filter { item.id == it.id_line && item.id_activity == it.activity_id }

            item.activity_info = database.getActivity(item.id_activity)

            if (users.isNotEmpty()) {
                item.packingline = users
            } else {
                line.remove(item)
            }
        }
        return line
    }


    fun deleteCollaborator(registerId: Int, idLine: Int): Observable<Any> {
        doAsync {
            database.deleteCollaboratorOffline(registerId, idLine)
        }
        return api.deleteCollaborator(authorization.getAuthToken(userAppPreferences.token), registerId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun pauseCollaborators(request: PauseTimerCollaboratorRequest): Observable<Any> {
        return api.pauseTimerCollaborators(authorization.getAuthToken(userAppPreferences.token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun pauseCollaboratorOfflineObserver(collaborators: List<PauseTimerCollaborator>, idLine: Int): Observable<Any> {
        return Observable.fromCallable {
            pauseCollaboratorOffline(collaborators, idLine)
        }
    }

    fun pauseCollaboratorOffline(collaborators: List<PauseTimerCollaborator>, idLine: Int) {
        collaborators.forEach { item ->
            database.pauseCollaboratorsOffline(item.isPused, item.time, item.id, idLine = idLine)
        }
    }

    fun startCollaborators(restartTimerCollaboratorRequest: RestarTimerCollaboratorLines): Observable<Any> {
        return api.restartTimerCollaborators(authorization.getAuthToken(userAppPreferences.token), restartTimerCollaboratorRequest)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun startCollaboratorsOfflineObservable(collaborators: List<RestartTimerCollaboratorLines>, idLine: Int): Observable<Any> {
        return Observable.fromCallable {
            startCollaboratorOffline(collaborators, idLine)
        }
    }

    fun startCollaboratorOffline(collaborators: List<RestartTimerCollaboratorLines>, idLine: Int) {
        collaborators.forEach { item ->
            database.startCollaboratorOffline(item.id, date = System.currentTimeMillis(), idLine = idLine)
        }
    }


    fun finishCollaboratorSkipSignature(request: List<FinishCollaboratorSignature>): Observable<Any> {
        return api.finishCollaboratorSkipSignature(authorization.getAuthToken(userAppPreferences.token), FinishCollaboratorSkipSignatureRequest(request))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getTotalsForFinishCollaborator(time: Long?, count_box: List<CountBoxModel>?, id_collaborator: Int): Observable<CollaboratorFinishCostResponse> {
        return api.getTotalsFinishCollaborator(authorization.getAuthToken(userAppPreferences.token), id_collaborator, CountBoxTotals(time, count_box, mediator.mapTimesToHashMap()))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getTotalsForFinishCollaboratorOffline(isBoxCount: Boolean, user: TimerUserViewModel,
                                              count_box: List<CountBoxModel>?, activity: ActivitiesResponse?): Observable<CollaboratorFinishCostResponse> {
        return Observable.fromCallable {

            val total: Float
            var unit_total = 0.0

            if (isBoxCount) {
                val result = countBoxRepository.calculateUnitsTotalByCollaborator(user.packing_line!!, CountBox(presentation = count_box!!), user.registerId!!)

                unit_total = result.first
                total = result.second
            } else {
                total = calculatePaymentByHrs(user.getMinutesTime().toFloat() / 60f, activity?.cost?.toFloat()
                        ?: 0.0f)
            }

            return@fromCallable CollaboratorFinishCostResponse(practice_cost = activity?.practice_cost, total = total.toDouble(), training_cost = activity?.costPerHour, unit_total = unit_total, hours = user.time.toDouble())
        }
    }


    fun finishCollaboratorWithBox(finish_request: FinishAllCollaboratorWithBox): Observable<Any> {
        return api.finishCollaboratorWithBox(authorization.getAuthToken(userAppPreferences.token), finish_request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun deleteCollaboratorOffline(registerId: Int, fromOnline: Boolean, idLine: Int): Observable<Any> {
        return Observable.fromCallable {
            database.deleteCollaboratorOffline(registerId, idLine)
            if (fromOnline) {
                database.deleteCollaboratorBySync(DeleteCollaboratorsOffline(idRegister = registerId))
            }
        }
    }

    fun deleteLineOffline(id_lineOffline: Int?): Observable<Any> {
        return Observable.fromCallable {
            database.deleteLineOffline(id_lineOffline)
        }
    }

    private fun saveCountLocal(count_box: CountBox?, id_line: Int) {
        count_box?.presentation?.forEach { box ->
            database.updateCountingBox(box.id, box.total, id_line)
        }
    }

    fun finishOfflineCollaborator(registerId: Int, idLine: Int, sign: String?, training: Boolean, isPractice: Boolean, time: Long?, isBox: Boolean): Observable<Any> {
        return Observable.fromCallable {
            countBoxRepository.distributeCountingBox(idLine, mediator.getMapCountBoxRequest(), sign, training, isPractice, registerId)

            if (isBox) {
                saveCountLocal(mediator.count_box, idLine)
            } else {
                countBoxRepository.createDetailRosterInLinesTime(idLine, registerId, sign, training, isPractice)
            }

            database.finishCountBoxLogs(registerId)
            database.finishOffLineCollaborator(registerId, time, idLine, training, isPractice, sign)
        }
    }

    fun updateFinishCollaborator(registerId: Int, time: Long?, idLine: Int, training: Boolean, isPractice: Boolean, sign: String?) {
        doAsync {
            database.finishOffLineCollaborator(registerId, time, idLine, training, isPractice, sign)
        }
    }

    fun updateAllFinishCollaborators(collaborators: List<FinishCollaboratorSignature>?) {
        doAsync {
            collaborators?.forEach { item ->
                database.finishOffLineAllCollaborator(item.id, item.time, mediator.bundleItemLinesPackage!!)
            }
        }
    }

    fun finishCollaboratorSkipSignatureOffline(allCollaborators: MutableList<FinishCollaboratorSignature>, idLine: Int): Observable<Any> {
        return Observable.fromCallable {

            allCollaborators.forEach { user ->
                database.finishOffLineCollaborator(user.id, user.time, idLine, false, false, null)
            }

        }
    }

    fun finishCollaboratorWithBoxOffline(sections: MutableList<TimerUserSectionViewModel>, idLine: Int, count_box: CountBoxRequest?): Observable<Any> {
        return Observable.fromCallable {
            countBoxRepository.distributeCountingBox(idLine, count_box)

            sections.forEach { section ->
                section.users.forEach { user ->
                    database.finishOffLineCollaborator(user.registerId!!, user.time, idLine, false, false, null)
                }
            }
        }
    }

    fun insertCollaborators(sections: List<CollaboratorsInLinesResponse>) {
        doAsync {
            sections.forEach { activity ->
                activity.id_line = mediator.bundleItemLinesPackage
                activity.id_activity = activity.packingline?.first()?.activity_id


                val idLineDb = insertLine(activity)

                if (activity.activity_info != null) {
                    activity.activity_info?.collaborator_left = activity.collaborator_left
                    database.insertActivities(arrayListOf(activity.activity_info!!))
                }

                if (activity.packingline != null) {
                    activity.packingline?.forEach { packing ->
                        packing.id_line = idLineDb
                        packing.isOnlineCollaborator = true

                        if (packing.is_paused == false) {
                            packing.temp_time = packing.temp_time + (packing.time_counter?.toLong()
                                    ?: 0L)
                            packing.date = System.currentTimeMillis()
                        }

                        savePresentationLocal(packing.packing)
                    }


                    database.insertAllCollaboratorInLine(activity.packingline!!)
                }
            }
        }
    }

    private fun insertLine(activity: CollaboratorsInLinesResponse): Int {
        val idLine: Int

        val idLineDb = database.existLine(activity.id_line!!, activity.id_activity!!)

        idLine = if (idLineDb != null) {
            database.updateCollaboratorLeft(idLineDb.id!!, activity.packingline?.size ?: 0)
            idLineDb.id!!
        } else {
            activity.id = activity.id_line!! + activity.id_activity!!
            database.insertLine(activity).toInt()
        }
        return idLine
    }

    private fun savePresentationLocal(packing: PresentationResultResponse?) {
        val id_line = mediator.bundleItemLinesPackage!!
        if (packing != null) {
            database.insertPresentation(arrayListOf(packing))

            val exist = database.existPresentation(packing.id, id_line) ?: false

            if (!exist) {
                database.saveCountBox(BoxCountResultResponse(null, 0, packing.id, id_line, packing))
            }
        }
    }

    fun syncRosterPackageCollaborators(token: String, request: SyncRosterPackageRequest): Observable<SyncRosterPackageResponse> {
        return api.syncCollaboratorsPackage(authorization.getAuthToken(token), request)
                .timeout(TIME_OUT_SYNC, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun syncRosterDeleteCollaborators(token: String): Observable<Any> {
        val deletesUsers = database.getCollaboratorsByDeleteSync()

        val list = mutableListOf<Int>()

        deletesUsers.forEach { collaborator ->
            if (collaborator.activityLog == null) {
                list.add(collaborator.idRegister)
            }
        }

        return api.syncDeleteCollaboratorsPackage(authorization.getAuthToken(token), SyncRosterDeleteCollaborators(list))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun deleteInfoSync(): Observable<Any> {
        return Observable.fromCallable {
            val collaborators = database.getCollaboratorIsDoneTrue()
            collaborators.forEach { item ->
                database.deleteCollaboratorOffline(item.id!!, item.packingline_id!!)
                database.deleteAsyncCountBox(item.id!!)
                database.deleteDetailsReportsSync(item.id!!)
                database.deleteAsyncCountBoxByLine(item.packingline_id)
            }
        }
    }

    fun haveCount(packing_line: Int): Boolean {
        return database.getBoxCountOffline(packing_line).sumBy { it.last_count } > 0
    }

}
