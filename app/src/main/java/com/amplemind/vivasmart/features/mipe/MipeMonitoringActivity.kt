package com.amplemind.vivasmart.features.mipe

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.features.mipe.adapter.DiseaseAdapter
import com.amplemind.vivasmart.features.mipe.adapter.PlagueAdapter
import com.amplemind.vivasmart.features.mipe.viewModel.ItemPlagueViewModel
import com.amplemind.vivasmart.features.mipe.viewModel.MipeMonitoringViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.MipeMonitoringModel
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_mipe_monitoring.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject

class MipeMonitoringActivity : BaseActivity() {

    companion object {
        val TAG = MipeMonitoringActivity::class.simpleName
        val LOT_ID = "$TAG.LotId"
        val GROOVE = "$TAG.Groove"
        val WEEK = "$TAG.Week"
        val TABLE = "$TAG.Table"
        val TAG_NAME = "$TAG.TagName"
        var PLANT = "$TAG.Plant"
    }

    private var plagueAdapter = PlagueAdapter()
    private var diseaseAdapter = DiseaseAdapter()

    val plagueList = ArrayList<ItemPlagueViewModel>()
    val diseaseList = ArrayList<ItemPlagueViewModel>()

    var lotId = 0
    var groove = 0
    var week = 0
    var table = ""
    var tagName = ""
    var plant = 0

    @Inject
    lateinit var viewModel: MipeMonitoringViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mipe_monitoring)

        plagueAdapter.activity = this
        diseaseAdapter.activity = this

        arguments()
        setupToolbar()
        setupRecycler()
        subscribers()
        setuUi()
    }

    fun arguments() {
        lotId = intent?.getIntExtra(LOT_ID, 0) ?: 0
        groove = intent?.getIntExtra(GROOVE, 0) ?: 0
        week = intent?.getIntExtra(WEEK, 0) ?: 0
        table = intent?.getStringExtra(TABLE) ?: ""
        tagName = intent?.getStringExtra(TAG_NAME) ?: ""
        plant = intent?.getIntExtra(PLANT, 0) ?: 0

        viewModel.getCropId(lotId)

        plagueAdapter.setData(lotId,groove,week,table,plant)
        diseaseAdapter.setData(lotId,groove,week,table,plant)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_mipe_monitoring
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = tagName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    fun subscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getPlagueLotCrop()
        viewModel.onSuccessRequest_lot_plague().subscribe(this::addElementsPlague).addTo(subscriptions)
        viewModel.getDiseaseLotCrop()
        viewModel.onSuccessRequest_lot_disease().subscribe(this::addElementsDisease).addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun getData() {
        viewModel.getPlagueLotCrop()
    }

    fun addElementsPlague(data: List<ItemPlagueViewModel>) {
        plagueList.clear()
        plagueList.addAll(data)
        plagueAdapter.addElements(data)
    }

    fun addElementsDisease(data: List<ItemPlagueViewModel>) {
        diseaseList.clear()
        diseaseList.addAll(data)
        diseaseAdapter.addElements(data)
        initSpinner(35)
    }

    private fun setupRecycler() {
        rv_plague.hasFixedSize()
        rv_plague.layoutManager = LinearLayoutManager(this)
        rv_plague.itemAnimator = DefaultItemAnimator()
        rv_plague.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        plagueAdapter.setHasStableIds(true)
        rv_plague.adapter = plagueAdapter

        rv_disease.hasFixedSize()
        rv_disease.layoutManager = LinearLayoutManager(this)
        rv_disease.itemAnimator = DefaultItemAnimator()
        rv_disease.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        diseaseAdapter.setHasStableIds(true)
        rv_disease.adapter = diseaseAdapter
    }

    fun setuUi(){
        tv_mipe_monitoring.setText("Tabla " + table + " - Surco " + groove + " - Planta " + plant)

        btn_plague.setOnClickListener { view ->
            rv_plague.visibility = View.VISIBLE
            rv_disease.visibility = View.GONE
            btn_plague.setBackgroundColor(Color.parseColor("#9B9B9B"))
            btn_disease.setBackgroundColor(Color.parseColor("#E3E3E3"))
        }

        btn_disease.setOnClickListener { view ->
            rv_plague.visibility = View.GONE
            rv_disease.visibility = View.VISIBLE
            btn_plague.setBackgroundColor(Color.parseColor("#E3E3E3"))
            btn_disease.setBackgroundColor(Color.parseColor("#9B9B9B"))
        }

        btn_save_monitoring_groove.setOnClickListener { view ->
            //saveMonitoringMipe()
            if(validateField()) {
                showProgressDialog(true)
                saveData()
            }
        }
    }

    fun initSpinner(size: Int) {
        val list = ArrayList<String>()
        val quadrant = getQuadrant()
        if(quadrant == 0){
            list.add("SELECCIONAR")
            for (i in 1 ..size) {
                list.add(i.toString())
            }
        } else {
            list.add(quadrant.toString())
            sp_quadrant.isEnable(false)
            btn_save_monitoring_groove.visibility = View.GONE
        }

        sp_quadrant!!.adapter = ArrayAdapter<String>(this, R.layout.spinner_item_textview, list)
        sp_quadrant!!.adapter = ArrayAdapter<String>(this, R.layout.spinner_item_textview, list)
    }


    private fun getQuadrant(): Int {
        Realm.getDefaultInstance().use {
            var quadrant = 0
            val result = it
                    .where(MipeMonitoringModel::class.java)
                    .beginGroup()
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("table", table.toInt())
                    .and()
                    .equalTo("grooveNo", groove)
                    .and()
                    .equalTo("week", week)
                    .endGroup()
                    .findFirst()

            if (result != null) {
                val plantResult = result.plants!!.where().equalTo("plantNo", plant).findFirst()
                quadrant = if (plantResult != null) plantResult.quadrant!! else 0
            }
            return quadrant
        }
    }

    fun saveMonitoringMipe(){
        if(validateField()){
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Planta - " + plant)
            builder.setMessage("¿Desea guardar la información de la planta?")
            builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                saveData()
                dialog.dismiss()
            }
            builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
            builder.show()
        }
    }

    fun saveData(){
            for (item in plagueList) {
                viewModel.savePlantData(lotId, table, groove, plant, sp_quadrant.selectedItemId.toInt(), item.id, item.medition, week, item.lvl)
            }

            for (item in diseaseList) {
                viewModel.savePlantData(lotId, table, groove, plant, sp_quadrant.selectedItemId.toInt(), item.id, item.medition, week, item.lvl)
            }
            /* val builder = AlertDialog.Builder(this)
        builder.setTitle("Planta - " + plant)
        builder.setMessage("Se ha guardado la informacion de la planta " + plant + " con exito.")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            dialog.dismiss()*/
            showProgressDialog(false)
            finish()
            /*}
        builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
        builder.show()*/
    }

    private fun validateField(): Boolean{
        if(sp_quadrant.selectedItemId.toInt() == 0){
            showSnackBar("Se debe de seleccionar el cuadrante al que pertenece")
            return false
        }
        return true
    }

    fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    fun ifConected(): Boolean {
        return if (this.hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

}
