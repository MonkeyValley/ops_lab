package com.amplemind.vivasmart.features.production_roster.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemRosterLinesBinding
import com.amplemind.vivasmart.features.production_roster.viewModels.ItemRosterLinesViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RosterLinesAdapter : RecyclerView.Adapter<RosterLinesAdapter.RosterLinesViewHolder>() {

    private var list  = mutableListOf<ItemRosterLinesViewModel>()

    private val clickSubject = PublishSubject.create<ItemRosterLinesViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RosterLinesViewHolder {
        val binding = ItemRosterLinesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RosterLinesViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RosterLinesViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(data : List<ItemRosterLinesViewModel>){
        list = data.toMutableList()
        notifyDataSetChanged()
    }

    fun onClickItem(): PublishSubject<ItemRosterLinesViewModel> {
        return clickSubject
    }

    inner class RosterLinesViewHolder(private val binding: ItemRosterLinesBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ItemRosterLinesViewModel

        fun bind(item: ItemRosterLinesViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item
            if (adapterPosition > -1) {
                binding.root.setOnClickListener {
                    clickSubject.onNext(item)
                }
            }
        }
    }

}
