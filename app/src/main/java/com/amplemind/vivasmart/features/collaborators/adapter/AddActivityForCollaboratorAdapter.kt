package com.amplemind.vivasmart.features.collaborators.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemAddActivitiesCollaboratorBinding
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemAddActivitiesViewModel
import io.reactivex.subjects.BehaviorSubject

class AddActivityForCollaboratorAdapter : RecyclerView.Adapter<AddActivityForCollaboratorAdapter.AddActivityViewHolder>() {

    val list = mutableListOf<ItemAddActivitiesViewModel>()

    var selected : Int? = null

    private var clickSubject = BehaviorSubject.create<ItemAddActivitiesViewModel>()

    fun addElements(data: List<ItemAddActivitiesViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddActivityViewHolder {
        val binding = ItemAddActivitiesCollaboratorBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddActivityViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: AddActivityViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem(): BehaviorSubject<ItemAddActivitiesViewModel> {
        return clickSubject
    }

    fun changeStatus(item: ItemAddActivitiesViewModel) {
        if (selected != null){
            list[selected!!].selected(false)
            notifyItemChanged(selected!!)
            if (selected == item.position){
                selected = null
                return
            }
        }
        selected = item.position
        item.selected(true)
        notifyItemChanged(item.position!!)
    }

    inner class AddActivityViewHolder(val binding: ItemAddActivitiesCollaboratorBinding) : RecyclerView.ViewHolder(binding.root) {

        var item: ItemAddActivitiesViewModel? = null

        fun bind(item: ItemAddActivitiesViewModel) {
            binding.setVariable(BR.itemModel, item)
            binding.executePendingBindings()

            this.item = item
            if (adapterPosition > -1) {
                binding.root.setOnClickListener {
                        item.position = adapterPosition
                        changeStatus(item)
                        clickSubject.onNext(item)
                }
            }
        }

    }

}
