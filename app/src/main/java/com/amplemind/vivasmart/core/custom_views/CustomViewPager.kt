package com.amplemind.vivasmart.core.custom_views

import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class CustomViewPager(context: Context, attrs: AttributeSet?) : ViewPager(context, attrs) {

    var swipeEnabled = true

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        if (swipeEnabled)  return super.onTouchEvent(ev)
        return false
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (swipeEnabled) return super.onInterceptTouchEvent(ev)
        return false
    }

}