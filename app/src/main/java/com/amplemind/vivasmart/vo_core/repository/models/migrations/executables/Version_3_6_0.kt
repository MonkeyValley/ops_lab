package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_3_6_0 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectWeightRestrictionModelrealm(realm)
    }

    private fun migrateUploadObjectWeightRestrictionModelrealm(realm: DynamicRealm){
        realm.schema.create("WeightRestriction")?.apply {
            addField("id", Long::class.javaObjectType)
            addField("businessUnitId", Long::class.javaObjectType)
            addField("cropId", Long::class.javaObjectType)
            addField("maxWeight", Double::class.javaObjectType)
            addField("minWeight", Double::class.javaObjectType)
        }
    }


    override fun drop(realm: DynamicRealm) {

    }
}