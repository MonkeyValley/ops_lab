package com.amplemind.vivasmart.features.production_roster.adapters

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.setImageUrl
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ExtraTimeModel
import com.amplemind.vivasmart.vo_core.trumps.HorizontalPicker
import io.reactivex.subjects.BehaviorSubject


class ExtraHoursCreateRequestAdapter(val list: List<ActivityModel>) : RecyclerView.Adapter<ExtraHoursCreateRequestAdapter.ExtraHourRequestViewHolder>() {

    private var mlist = mutableListOf<ActivityModel>()
    private val clickSubject = BehaviorSubject.create<ExtraTimeModel>()
    private var selectedHour: Int = 0
    private var selectedColaborator: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExtraHourRequestViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_extra_hour_request, parent, false)

        return ExtraHourRequestViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ExtraHourRequestViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onSelectedItem(): BehaviorSubject<ExtraTimeModel> {
        return clickSubject
    }

    fun setSelectedOptions(selectedColaborators: Int, selectedHours: Int){
        selectedHour = selectedHours
        selectedColaborator =  selectedColaborators
        notifyDataSetChanged()
    }


    inner class ExtraHourRequestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), HorizontalPicker.OnItemClicked, HorizontalPicker.OnItemSelected {


        private lateinit var pickerColaborators: HorizontalPicker
        private lateinit var pickerHours: HorizontalPicker

        private lateinit var underLayoutHour: ConstraintLayout
        private lateinit var underLayoutCollaborator: ConstraintLayout

        private lateinit var txtActivity: TextView
        private lateinit var imgActivity: ImageView

        private val TagCollaborator = "picker_collaborator"
        private val TagHours = "picker_hours"

        private lateinit var mActivity: ActivityModel
        private var mHours: Int = 0
        private var mCollaborators = 0


        fun bind(item: ActivityModel) {
            // TODO: Futura programacion del bind de este adapater
            pickerColaborators = itemView.findViewById(R.id.picker) as HorizontalPicker
            pickerHours = itemView.findViewById(R.id.picker2) as HorizontalPicker
            underLayoutHour = itemView.findViewById(R.id.under_selecter_hour_blue) as ConstraintLayout
            underLayoutCollaborator = itemView.findViewById(R.id.under_selecter_collaborator_blue) as ConstraintLayout
            txtActivity = itemView.findViewById(R.id.ica_extrahour_txv_activity) as TextView
            imgActivity = itemView.findViewById(R.id.ica_extrahour_imgv_accept) as ImageView

            mActivity = item

            pickerColaborators.setIndentifier(TagCollaborator)
            pickerHours.setIndentifier(TagHours)

            val icon = "${BuildConfig.SERVER_URL}/uploads/icons/${item.icon}"
            txtActivity.text = item.name
            imgActivity.setImageUrl(icon)

            if(selectedColaborator > 0 && selectedHour > 0){
                pickerColaborators.selectedItem = selectedColaborator
                pickerHours.selectedItem = selectedHour

                underLayoutCollaborator.visibility = View.VISIBLE
                underLayoutHour.visibility = View.VISIBLE
            }


            pickerColaborators.setOnItemClickedListener(this, TagCollaborator)
            pickerColaborators.setOnItemSelectedListener(this, TagCollaborator)
            pickerHours.setOnItemClickedListener(this, TagHours)
            pickerHours.setOnItemSelectedListener(this, TagHours)

        }

        override fun onItemSelected(index: Int, identifier: String?) {
            if (identifier.equals(TagCollaborator)) {
                if (index > 0) {
                    underLayoutCollaborator.visibility = View.VISIBLE
                } else if (index == 0) {
                    underLayoutCollaborator.visibility = View.INVISIBLE
                }
                mCollaborators = index
            }


            if (identifier.equals(TagHours)) {
                if (index > 0) {
                    underLayoutHour.visibility = View.VISIBLE
                } else if (index == 0) {
                    underLayoutHour.visibility = View.INVISIBLE
                }
                mHours = index
            }
            val extra = ExtraTimeModel(extra_hours = mHours, collaborators_no = mCollaborators, activity_id = mActivity.id)  // TODO
            clickSubject.onNext(extra)   // TODO

        }

        override fun onItemClicked(index: Int, identifier: String?) {
            // Toast.makeText(itemView.context, index.toString() + identifier, Toast.LENGTH_SHORT).show()
        }


    }
}