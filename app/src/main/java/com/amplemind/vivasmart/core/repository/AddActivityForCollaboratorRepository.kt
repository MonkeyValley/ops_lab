package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.extensions.dummyAddActivities
import com.amplemind.vivasmart.core.repository.model.AddActivitiesListModel
import io.reactivex.Observable
import javax.inject.Inject

class AddActivityForCollaboratorRepository @Inject constructor() {

    fun getActivities(): Observable<List<AddActivitiesListModel>> {
        return Observable.just(dummyAddActivities())
    }

}