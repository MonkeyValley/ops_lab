package com.amplemind.vivasmart.features.collaborators.fragment

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.model.AssingGroovesModel
import com.amplemind.vivasmart.features.collaborators.IAssingGroovesManager
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemAssingGroovesViewModel
import com.amplemind.vivasmart.features.collaborators.adapter.AssingGroovesAdapter
import com.amplemind.vivasmart.features.collaborators.viewModel.GroovesViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import javax.inject.Inject

class GroovesFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: GroovesViewModel

    private var mAdapter: AssingGroovesAdapter? = null

    private lateinit var mView: View

    private var listener: IAssingGroovesManager? = null

    private var mTable: Int = 0
    private var mPosition: Int = 0
    private lateinit var mActivityLog: ActivityLogModel
    private lateinit var mGrooves: ArrayList<AssingGroovesModel>

    companion object {

        val TAG                 = GroovesFragment::class.java.simpleName
        val PARAM_TABLE         = "$TAG.Table"
        val PARAM_ACTIVITY_LOG  = "$TAG.ActivityLog"
        val PARAM_POSITION      = "$TAG.Position"
        val PARAM_GROOVES       = "$TAG.Grooves"

        fun newInstance(table: Int, grooves: ArrayList<AssingGroovesModel>, activityLog: ActivityLogModel, position: Int): GroovesFragment {
            val fragment = GroovesFragment()
            val args = Bundle()
            args.putInt(PARAM_TABLE, table)
            //args.putParcelableArrayList(PARAM_GROOVES, grooves)
            //args.putParcelable(PARAM_ACTIVITY_LOG, activityLog)
            args.putInt(PARAM_POSITION, position)
            //fragment.setIAssingGroove(listener)
            fragment.arguments = args
            return fragment
        }

        fun newInstance(table: Int, grooves: ArrayList<AssingGroovesModel>, id_collaborator: Int, listener: IAssingGroovesManager, position: Int, idActivityLog : Long): GroovesFragment {
            val fragment = GroovesFragment()
            val args = Bundle()
            args.putInt("TABLA", table)
            args.putInt("id_collaborator", id_collaborator)
            args.putInt("position", position)
            args.putLong("idActivityLog", idActivityLog)
            //args.putParcelableArrayList("grooves", grooves)
            fragment.setIAssingGroove(listener)
            fragment.arguments = args
            return fragment
        }
    }

    /*
    private fun getidActivityLog() : Long {
        return arguments!!.getLong("idActivityLog",-1)
    }
    */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mTable = arguments!!.getInt(PARAM_TABLE)
        mPosition = arguments!!.getInt(PARAM_POSITION)
        //mActivityLog = arguments!!.getParcelable(PARAM_ACTIVITY_LOG)!!
        //mGrooves = arguments!!.getParcelableArrayList(PARAM_GROOVES)!!

    }

    private fun setIAssingGroove(listener: IAssingGroovesManager) {
        this.listener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.content_assing_grooves_fragment, container, false)

        initRecycler(mView.findViewById(R.id.rv_grooves))
        getData()

        //(context as AssignGroovesActivity).getSelectedSubject().subscribe(this::resetView).addTo(subscriptions)

        return mView
    }

    fun resetView(clean: Boolean) {
        mAdapter!!.clearSelections()
        mView.findViewById<RecyclerView>(R.id.rv_grooves).post {
            Runnable {
                mAdapter!!.resetAnimationIndex()
            }.run()
        }
    }



    /**
     *  obtains the information of the grooves that had already been assigned previously
     */
    private fun getData() {
        viewModel.createGrooves(mGrooves, mActivityLog)
        viewModel.getGroovesData().subscribe(this::setData).addTo(subscriptions)
    }

    /**
     *  fill the information in the adapter
     */
    private fun setData(data: List<ItemAssingGroovesViewModel>) {
        mAdapter!!.addElements(data)
    }


    private fun initRecycler(recycler : RecyclerView) {
        mAdapter = AssingGroovesAdapter(object : OnGrooveListener {
            override fun onClickGroove(groove: ItemAssingGroovesViewModel) {
                listener!!.addGroove(groove.item.groove, mPosition, groove.item.animation_status!!)
            }

        })
        recycler.hasFixedSize()
        recycler.layoutManager = GridLayoutManager(context, 7)
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.adapter = mAdapter
    }

    /*
    private fun getTable(): Int {
        return arguments!!.getInt("TABLA", -1)
    }

    private fun getIDCollaborator(): Int {
        return arguments!!.getInt("id_collaborator", -1)
    }

    private fun getPosition(): Int {
        return arguments!!.getInt("position", -1)
    }

    private fun getGrooves(): ArrayList<AssingGroovesModel> {
        return arguments!!.getParcelableArrayList<AssingGroovesModel>("grooves")
    }
    */

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as IAssingGroovesManager
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}


interface OnGrooveListener {
    fun onClickGroove(groove: ItemAssingGroovesViewModel)
}