package com.amplemind.vivasmart.feature

import com.google.android.material.textfield.TextInputLayout
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.amplemind.vivasmart.AndroidTest
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.RecoverPasswordModel
import com.amplemind.vivasmart.features.login.RecoverPasswordActivity
import org.amshove.kluent.`should equal`
import org.amshove.kluent.shouldEqualTo
import org.junit.Before
import org.junit.Test
import org.robolectric.Robolectric
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class RecoverPasswordActivityTest : AndroidTest() {

    private var activity: RecoverPasswordActivity? = null

    private var btnRecovery: Button? = null
    private var tvLogin: TextView? = null
    private var etUsername: EditText? = null
    private var tiUsername: TextInputLayout? = null
    private var tvRecoveryPass: TextView? = null

    @Before
    fun setup() {
        activity = Robolectric.buildActivity(RecoverPasswordActivity::class.java)
                .create()
                .get()

        btnRecovery = activity?.findViewById(R.id.btn_recover)
        tvLogin = activity?.findViewById(R.id.tv_log_in)
        etUsername = activity?.findViewById(R.id.ed_username_recover)
        tiUsername = activity?.findViewById(R.id.textInputLayout_recover)
        tvRecoveryPass = activity?.findViewById(R.id.tvRecoveryPass)
    }

    @Test
    fun `Activity should not be null`() {
        assertNotNull(activity)
    }

    @Test
    fun `views should have its correct text`() {
        assertEquals(getString(R.string.send), btnRecovery?.text)
        assertEquals(getString(R.string.log_in_return_underline), tvLogin?.text)
        assertEquals(getString(R.string.recover_password), tvRecoveryPass?.text)
    }

    @Test
    fun `setContentView() inflates the correct layout`() {
        assertEquals(R.layout.content_recover_activity, activity?.getLayout())
    }

    @Test
    fun `Finish activity on tv_log_in press`() {
        tvLogin?.performClick()

        assertTrue(activity?.isFinishing!!)
    }

    @Test
    fun `Error should be shown when validation fails`() {
        etUsername?.setText("invalid email")

        btnRecovery?.performClick()

        assertEquals(context().getString(R.string.incorrect_user), tiUsername?.error)
    }

    @Test
    fun `Error should be null when validation success`() {
        etUsername?.setText("aaron.jags@gmail.com")

        btnRecovery?.performClick()

        assertEquals(null, tiUsername?.error)
    }

    @Test
    fun `sucessMessage must be visible on success request`() {
        val llForm = activity?.findViewById<LinearLayout>(R.id.ln_form)
        val tvSucessMessage = activity?.findViewById<TextView>(R.id.success_message)

        activity?.onSuccessRecoveryPassword(RecoverPasswordModel.RecoverPasswordResponse(""))

        assertEquals(View.INVISIBLE, llForm?.visibility)
        assertEquals(View.VISIBLE, tvSucessMessage?.visibility)
        assertEquals(context().getString(R.string.recover_password_sucess), tvSucessMessage?.text)
    }
}