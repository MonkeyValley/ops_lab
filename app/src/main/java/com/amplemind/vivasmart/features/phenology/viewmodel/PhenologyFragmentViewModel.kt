package com.amplemind.vivasmart.features.phenology.viewmodel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.PhenologyRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyLot
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PhenologyFragmentViewModel @Inject constructor(
        private val repository: PhenologyRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val requestSuccess_lots = BehaviorSubject.create<List<LotPhenologyItemViewModel>>()
    private val listLots = mutableListOf<LotPhenologyItemViewModel>()

    fun getLots(is_commercial: Int): Disposable {
        return repository.getLots(is_commercial)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModels)
                .subscribe({
                    listLots.clear()
                    listLots.addAll(it)
                    requestSuccess_lots.onNext(listLots)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModels(models: List<PhenologyLot>): List<LotPhenologyItemViewModel> {
        val list = arrayListOf<LotPhenologyItemViewModel>()
        for (item in models) {
            val lotmodel = LotPhenologyItemViewModel(item)
            list.add(lotmodel)
        }
        return list
    }

    fun onSuccessRequest_lots(): BehaviorSubject<List<LotPhenologyItemViewModel>> {
        return requestSuccess_lots
    }
}