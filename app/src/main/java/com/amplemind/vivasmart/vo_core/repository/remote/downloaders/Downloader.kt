package com.amplemind.vivasmart.vo_core.repository.remote.downloaders

import io.reactivex.Maybe
import io.reactivex.Single

interface Downloader<ParentType: Any?, ChildType> {

    fun getLastVersion(): Single<Long>
    fun buildServerRequest(parent: ParentType, lastVersion: Long): Single<List<ChildType>>
    fun beforeSaving(parent: ParentType, child: ChildType): Single<ChildType>
    fun saveServerResponse(parent: ParentType, response: List<ChildType>): Single<List<ChildType>>
    fun findAll(): Single<List<ChildType>>
    fun findByUuid(uuid: String): Maybe<ChildType>
    fun delete(uuid: String): Single<ChildType>
    fun clear()
    fun clearCache()

}