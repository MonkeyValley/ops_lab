package com.amplemind.vivasmart.vo_core.repository.dao.realm

import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.models.realm.DownloadObjectModel
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.RequestType
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.Role
import com.amplemind.vivasmart.vo_core.utils.currentDateInMillis
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.realm.Realm
import java.util.*
import javax.inject.Inject

class DownloadObjectDao @Inject constructor(
        realmHolder: RealmHolder
): RealmDao<DownloadObjectModel>(realmHolder, DownloadObjectModel::class.java) {

    fun createSyncRegistersIfNotExists(syncTree: Role.Node): Completable =
            executeTransaction { realm ->
                treeNavigator(realm, syncTree)
            }.ignoreElement()

    private fun treeNavigator(realm: Realm, node: Role.Node) {

        if (node.requestType != RequestType.UNKNOWN) {
            val row = query(realm)
                    .equalTo("type", node.requestType.remoteName)
                    .findFirst()

            if (row == null) {
                realm.createObject(modelClass, node.requestType.remoteName)
            }
        }

        node.children.forEach { child ->
            treeNavigator(realm, child)
        }

    }

    fun addRequest(requestType: RequestType): Completable =
            executeTransaction { realm ->
                val obj = query(realm)
                        .equalTo("type", requestType.remoteName)
                        .findFirst()

                obj?.apply {
                    timesRequested ++
                    requestDate = Date(currentDateInMillis())
                }
            }.ignoreElement()

    fun resetRequests(requestType: RequestType): Completable =
            executeTransaction { realm ->
                val objs = query(realm)
                        .run {
                            if (requestType == RequestType.UNKNOWN) {
                                findAll()
                            }
                            else {
                                equalTo("type", requestType.remoteName)
                                listOf(findFirst())
                            }
                        }

                objs.forEach { obj ->
                    obj?.apply {
                        firstSyncDone = true
                        timesRequested = 0
                        lastUpdated = Date(currentDateInMillis())
                    }
                }
            }.ignoreElement()

    fun needsFirstSync(): Single<Boolean> =
            findAll(false)
            .map { objs: List<DownloadObjectModel> ->
                objs.isEmpty() || objs.any{ obj -> !obj.firstSyncDone }
            }

    fun getForRequestType(requestType: RequestType): Maybe<DownloadObjectModel> =
            findFirst({ realm ->
                query(realm)
                        .equalTo("type", requestType.remoteName)
            })


}