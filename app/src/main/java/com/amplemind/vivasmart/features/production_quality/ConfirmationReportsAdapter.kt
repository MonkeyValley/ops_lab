package com.amplemind.vivasmart.features.production_quality

import android.content.Context
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.util.Log.e
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.databinding.ItemConfirmationReportsBinding
import io.reactivex.subjects.BehaviorSubject

class ConfirmationReportsAdapter constructor(val context: Context, val pref: UserAppPreferences) : RecyclerView.Adapter<ConfirmationReportsAdapter.ConfirmationReportsViewHolder>() {

    private var list = mutableListOf<ConfirmationReportsItemViewModel>()

    private val onClick = BehaviorSubject.create<ConfirmationReportsItemViewModel>()

    fun addElements(data: MutableList<ConfirmationReportsItemViewModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConfirmationReportsViewHolder {
        val binding = ItemConfirmationReportsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ConfirmationReportsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ConfirmationReportsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClickItem(): BehaviorSubject<ConfirmationReportsItemViewModel> {
        return onClick
    }

    fun removeItem(issue: Pair<Int, Int>) {
        val listFilter = list.filter { it.issueId == issue.first }
        if (listFilter.isNotEmpty()) {
            list.remove(listFilter.first())
            verifyIsEmptySection(listFilter.first().model?.tableNo ?: -1)
        }
        notifyPositionChange(issue.second)
    }

    private fun verifyIsEmptySection(tableNo: Int) {
        val listFilter = list.filter { it.header == "Tabla ${tableNo}" }
        if (listFilter.isNotEmpty() && list.filter{ it.model?.tableNo == tableNo }.isEmpty()) {
            list.remove(listFilter.first())
        }
    }

    private fun notifyPositionChange(tableNo: Int) {
        val listFilter = list.filter { it.model?.tableNo == tableNo }
        for (item in 0 until listFilter.size){
            e("Anterior position", listFilter[item].position)
            e("Anterior nueva", item.toString())
            listFilter[item].position = (item + 1).toString()
        }
        notifyDataSetChanged()
    }

    fun isHeader(position: Int): Boolean {
        return list[position].isHeader()
    }

    inner class ConfirmationReportsViewHolder(private val binding: ItemConfirmationReportsBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ConfirmationReportsItemViewModel
        var viewForeground: CardView? = null
        var viewBackground: RelativeLayout? = null

        fun bind(item: ConfirmationReportsItemViewModel) {

            item.serverUrl = pref.selectedServerUrl

            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item
            this.viewForeground = binding.root.findViewById(R.id.viewForeground)
            this.viewBackground = binding.root.findViewById(R.id.view_background)
            binding.root.setOnClickListener {
                if (!item.isHeader()) {
                    onClick.onNext(item)
                }
            }
//            binding.root.btn_delete.setOnClickListener {
//                clickRemove.onNext(item)
//            }
        }
    }

}