package com.amplemind.vivasmart.features.reciba_quality.repository.response

import com.google.gson.annotations.SerializedName

data class RewviewDetailRecibaQualityResponse(
        @SerializedName("crop_id") var cropId: Int,
        @SerializedName("crop_name") var cropName: String,
        @SerializedName("damaged") var damaged: Int,
        @SerializedName("export") var export: Int,
        @SerializedName("id") var folio: Int,
        @SerializedName("issues_condition") var issuesCondition: List<issuesCondition>,
        @SerializedName("issues_permanent") var issuesPermanent: List<issuesPermanent>,
        @SerializedName("lot_id") var lotId: Int,
        @SerializedName("lot_name") var lotName: String,
        @SerializedName("total") var total: Int,
        @SerializedName("variety_id") var varietyId: Int,
        @SerializedName("variety_name") var varietyName: String
)

data class issuesCondition(
        @SerializedName("frequency_no") var frequencyNo: Int,
        @SerializedName("id") var issuesConditionId: Int,
        @SerializedName("name") var name: String,
        @SerializedName("percentage") var percentage: Double
)

data class issuesPermanent(
        @SerializedName("frequency_no") var frequencyNo: Int,
        @SerializedName("id") var issuesPermanentId: Int,
        @SerializedName("name") var name: String,
        @SerializedName("percentage") var percentage: Double
)