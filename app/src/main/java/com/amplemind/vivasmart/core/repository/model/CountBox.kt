package com.amplemind.vivasmart.core.repository.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CountBox(@SerializedName("presentations") val presentation: List<CountBoxModel>? = null, @SerializedName("times") val times: List<TimesCollaborator>? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(CountBoxModel),
            parcel.createTypedArrayList(TimesCollaborator)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(presentation)
        parcel.writeTypedList(times)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CountBox> {
        override fun createFromParcel(parcel: Parcel): CountBox {
            return CountBox(parcel)
        }

        override fun newArray(size: Int): Array<CountBox?> {
            return arrayOfNulls(size)
        }
    }
}

data class CountBoxRequest(@SerializedName("presentations") val presentation: List<CountBoxModel>?, @SerializedName("times") val times: HashMap<Int, Long>)


data class CountBoxModel(@SerializedName("id") val id: Int, @SerializedName("total") val total: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(total)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CountBoxModel> {
        override fun createFromParcel(parcel: Parcel): CountBoxModel {
            return CountBoxModel(parcel)
        }

        override fun newArray(size: Int): Array<CountBoxModel?> {
            return arrayOfNulls(size)
        }
    }
}

data class CountBoxTotals(@SerializedName("temp_time") val temp_time: Long?, @SerializedName("presentations") val presentation: List<CountBoxModel>?, @SerializedName("times") val times: HashMap<Int, Long>)