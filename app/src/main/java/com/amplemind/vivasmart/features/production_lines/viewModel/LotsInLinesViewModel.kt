package com.amplemind.vivasmart.features.production_lines.viewModel

import com.amplemind.vivasmart.core.extensions.registerObserver
import com.amplemind.vivasmart.core.extensions.registerSingle
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.RosterRepository
import com.amplemind.vivasmart.core.repository.model.LotsPackageResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class LotsInLinesViewModel @Inject constructor(private val repository: RosterRepository,
                                               private val apiErrors: HandleApiErrors,
                                               private val mediator: Mediator) {

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val errorSelected = BehaviorSubject.create<String>()
    private val searchLocalData = BehaviorSubject.create<Boolean>()
    private val changeLotSubject = BehaviorSubject.create<Boolean>()
    private val assingLotSubject = BehaviorSubject.create<Int>()
    private val requestFail = BehaviorSubject.create<String>()
    private val lots = BehaviorSubject.create<List<ItemLotPackageViewModel>>()

    private val lotList = mutableListOf<ItemLotPackageViewModel>()

    fun loadLots(): Disposable {
        return repository.getLots(mediator.bundleItemLinesPackage!!)
                .registerObserver(progressStatus)
                .map {
                    repository.insertLots(it.lots)
                    mapModelToViewModels(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    lotList.clear()
                    lotList.addAll(it)
                }
                .subscribe({
                    lots.onNext(lotList)
                }, {
                    searchLocalData.onNext(true)
                })
    }

    fun onLoadLocalData(): BehaviorSubject<Boolean> {
        return searchLocalData
    }

    private fun mapModelToViewModels(response: LotsPackageResponse): List<ItemLotPackageViewModel> {
        val list = arrayListOf<ItemLotPackageViewModel>()
        for (item in response.lots) {
            list.add(ItemLotPackageViewModel(item))
        }
        if (list.size == 1){
            list.first().changeStatus()
        }
        return list
    }

    fun getLots(): BehaviorSubject<List<ItemLotPackageViewModel>> {
        return lots
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onErrorSelected(): BehaviorSubject<String> {
        return errorSelected
    }

    fun changeLot(hasInternet: Boolean): Disposable? {
        val lot = lotList.filter { it.status }
        if (lot.isEmpty()){
            errorSelected.onNext("Es necesario seleccionar un lote")
            return null
        }
        val observer = if (hasInternet) {
            repository.changeLot(RosterRepository.ChangeLotRequest(lot.first().stage_id,mediator.mapTimesToHashMap(),mediator.count_box?.presentation), mediator.bundleItemLinesPackage!!)
        } else {
            repository.changeLotOffline(lot.first().stage_id, mediator.getMapCountBoxRequest(), mediator.bundleItemLinesPackage!!)
        }
        return observer.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    changeLotSubject.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSucessChangeLot(): BehaviorSubject<Boolean> {
        return changeLotSubject
    }

    fun onSuccessAssingLot(): BehaviorSubject<Int> {
        return assingLotSubject
    }

    fun assignLot(hasInternet: Boolean, stage_id: Int): Disposable {
        return if (hasInternet) {
            assignLotOnline(stage_id)
        } else {
            assignLotLocal(stage_id)
        }
    }

    private fun assignLotLocal(stage_id: Int): Disposable {
        return repository.assignLotLocal(stage_id, mediator.bundleItemLinesPackage!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    assingLotSubject.onNext(stage_id)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun assignLotOnline(stage_id: Int): Disposable {
        return repository.assignLot(stage_id, mediator.bundleItemLinesPackage!!)
                .registerObserver(progressStatus)
                .map {
                    repository.assignLotLocal(stage_id, mediator.bundleItemLinesPackage!!)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    assingLotSubject.onNext(stage_id)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLocalLots(): Disposable {
        return repository.getLocalLots()
                .registerSingle(progressStatus)
                .map { mapModelToViewModels(LotsPackageResponse(it)) }
                .subscribe({
                    lotList.clear()
                    lotList.addAll(it)
                    lots.onNext(lotList)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

}
