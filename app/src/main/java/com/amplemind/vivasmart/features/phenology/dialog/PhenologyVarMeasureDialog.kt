package com.amplemind.vivasmart.features.phenology.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.features.phenology.viewmodel.VarMeasurePhenologyItemViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*


class PhenologyVarMeasureDialog : BaseDialogFragment() {

    companion object {
        val TAG = PhenologyVarMeasureDialog::class.java.simpleName
        lateinit var item: VarMeasurePhenologyItemViewModel

        fun newInstance(item: String): PhenologyVarMeasureDialog {
            val dialog = PhenologyVarMeasureDialog()
            val args = Bundle()
            args.putString("item", item)
            dialog.arguments = args
            return dialog
        }
    }

    lateinit var mView: View
    lateinit var name: TextView
    lateinit var desc: TextView
    lateinit var last: TextView
    lateinit var photo: ImageView

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mView = LayoutInflater.from(context).inflate(R.layout.phenology_varieties_var_measure_dialog, null, false)
        builder.setView(mView)
        val gson = Gson()
        val itemString: String? = arguments?.getString("item")
        val item: VarMeasurePhenologyItemViewModel = gson.fromJson(itemString, VarMeasurePhenologyItemViewModel::class.java)

        name = mView.findViewById(R.id.TV_Var_Name_Dialog)
        desc = mView.findViewById(R.id.TV_Var_Desc_Dialog)
        photo = mView.findViewById(R.id.IV_Photo_Dialog)
        last = mView.findViewById(R.id.TV_Last_Review_Dialog)

        name.text = item.phenology_var_name
        desc.text = item.phenology_var_description
        val url_host = getString(R.string.url_img_root)
        val url = url_host + item.phenology_var_image
        Glide.with(this).load(url).apply(RequestOptions().placeholder(R.drawable.ic_user)).into(photo)
        last.text = if(item.date.toString().equals("null")) "Sin registro" else getDate(item.date!!)

        builder.setPositiveButton ("Aceptar"){ _, _ ->
            dismiss()
        }
        builder.setCancelable(true)
        return builder.create()
    }

    fun getDate(dateString: String) : String{
        val date = SimpleDateFormat("E, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH).parse(dateString)
        val df2 = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        return  df2.format(date)
    }
}