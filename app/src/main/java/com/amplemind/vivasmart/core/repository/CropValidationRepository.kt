package com.amplemind.vivasmart.core.repository

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.CropValidationApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.crop_validation.models.*
import com.google.gson.Gson
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CropValidationRepository @Inject constructor(private val api: CropValidationApi, private val authorization: ApiAuthorization, private val preferences: UserAppPreferences) {
    val businessUnitId = Gson().fromJson(preferences.userInfo, UserModel::class.java).businessUnitId
            ?: ""

    fun getCarryOrderHarvest(day: String): Observable<List<CarryOrderHarvest>> {

        Log.e("BussinesId", authorization.getAuthToken(preferences.token.toString()))

        return api.getCarryOrderHarvest(authentication = authorization.getAuthToken(preferences.token),
                business_unit_id = businessUnitId, day = day).timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)
    }

    fun getIssues(crop_id: Int): Observable<CropIssuesResponse> =
            api.getIssues(authentication = authorization.getAuthToken(preferences.token),
                    crop_id = crop_id ).timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)

    fun getDetail(carry_order_id: Int): Observable<CropValidationDetailResponse> =
            api.getDetail(authentication = authorization.getAuthToken(preferences.token),
                    carry_order_id = carry_order_id ).timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)

    fun sendDataCropValidation(body: CropValidationPost): Observable<CropValidationPostResponse> =
            api.sendDataCropValidation(authentication = authorization.getAuthToken(preferences.token), body = body ).timeout(TIME_OUT, TimeUnit.SECONDS).retry(RETRY)

}
