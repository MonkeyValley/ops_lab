package com.amplemind.vivasmart.features.mipe.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.mipe.service.MipeService
import com.amplemind.vivasmart.vo_core.repository.models.realm.PlagueDetail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MipeMonitoringViewModel @Inject constructor(
        private val apiErrors: HandleApiErrors,
        private val service: MipeService) : BaseViewModel() {

    var cropId = 0

    private val requestSuccess_lot_plague = BehaviorSubject.create<List<ItemPlagueViewModel>>()
    private val listLotPlague = mutableListOf<ItemPlagueViewModel>()

    private val requestSuccess_lot_disease = BehaviorSubject.create<List<ItemPlagueViewModel>>()
    private val listLotDisease = mutableListOf<ItemPlagueViewModel>()

    fun getPlagueLotCrop(): Disposable {
        return service.getPlagueLotCrop(cropId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModelsPlague)
                .subscribe({
                    listLotPlague.clear()
                    listLotPlague.addAll(it)
                    requestSuccess_lot_plague.onNext(listLotPlague)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModelsPlague(models: List<PlagueDetail>): List<ItemPlagueViewModel> {
        val list = arrayListOf<ItemPlagueViewModel>()
        for (item in models) {
            if(item.isDesease == null || item.isDesease == false) {
                val lotmodel = ItemPlagueViewModel(item)
                list.add(lotmodel)
            }
        }
        return list
    }

    fun onSuccessRequest_lot_plague(): BehaviorSubject<List<ItemPlagueViewModel>> {
        return requestSuccess_lot_plague
    }

    fun getDiseaseLotCrop(): Disposable {
        return service.getPlagueLotCrop(cropId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModelsDisease)
                .subscribe({
                    listLotDisease.clear()
                    listLotDisease.addAll(it)
                    requestSuccess_lot_disease.onNext(listLotDisease)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModelsDisease(models: List<PlagueDetail>): List<ItemPlagueViewModel> {
        val list = arrayListOf<ItemPlagueViewModel>()
        for (item in models) {
            if(item.isDesease == true) {
                val lotmodel = ItemPlagueViewModel(item)
                list.add(lotmodel)
            }
        }
        return list
    }

    fun onSuccessRequest_lot_disease(): BehaviorSubject<List<ItemPlagueViewModel>> {
        return requestSuccess_lot_disease
    }

    fun savePlantData(lotId: Int, table: String, groove: Int, plant: Int, quadrant: Int, id: Int?, medition: Int?, week: Int, lvl: Int?) {
        service.savePlantData(lotId, table, groove, plant, quadrant, id, medition, week, lvl)
    }

    fun getCropId(lotId: Int) {
        cropId = service.getCropId(lotId)
    }


}