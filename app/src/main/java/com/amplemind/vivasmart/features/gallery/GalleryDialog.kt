package com.amplemind.vivasmart.features.gallery


import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.custom_views.Signature
import com.amplemind.vivasmart.core.custom_views.SignatureListener
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.CustomCamera.CameraActivity
import com.amplemind.vivasmart.features.finished_product_quality.fragment.ProductQualityReviewDetailFragment
import com.amplemind.vivasmart.features.finished_product_quality.fragment.ProductQualityReviewDetailFragment.Companion.ifConected
import com.amplemind.vivasmart.features.gallery.viewModel.GalleryViewModel
import com.google.gson.Gson
import java.io.File
import java.io.IOException
import javax.inject.Inject


class GalleryDialog : BaseDialogFragment() {

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mPermission: AndroidPermission

    @Inject
    lateinit var viewModel: GalleryViewModel

    private val CAMERA_REQUEST_CODE = 1

    private lateinit var viewButton: View
    private lateinit var path: String
    private lateinit var imagenBitmap: Bitmap
    val imageList = mutableListOf<File>()
    var imageListSize: Int = 0
    val signList = mutableListOf<File>()
    var skipSignature: Boolean = true
    var status = 0

    companion object {
        val TAG = GalleryDialog::class.java.simpleName
        var totalDamaged = 0.0
        var mode = 0
    }

    var imgButton1: ImageButton? = null
    var imgButton2: ImageButton? = null
    var imgButton3: ImageButton? = null
    var imgButton4: ImageButton? = null
    var imgButton5: ImageButton? = null
    var imgButton6: ImageButton? = null

    var image1: File? = null
    var image2: File? = null
    var image3: File? = null
    var image4: File? = null
    var image5: File? = null
    var image6: File? = null

    var table: TableLayout? = null
    var signature: Signature? = null
    var reload: ImageView? = null
    var headerTable: LinearLayout? = null

    var spinnerGallery: Spinner? = null

    fun newInstance(damage: Double, modeType: Int): GalleryDialog {
        totalDamaged = damage
        mode = modeType
        return GalleryDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.fragment_gallery, null, false)
        addSubscribers()
        builder.setCancelable(false)
        builder.setView(mView)

        table = mView.findViewById(R.id.table)
        headerTable = mView.findViewById(R.id.ln_orden)
        signature = mView.findViewById(R.id.signature_gallery)
        reload = mView.findViewById(R.id.btn_reload_signature_gallery)

        imgButton1 = mView.findViewById(R.id.img_btn1)
        imgButton2 = mView.findViewById(R.id.img_btn2)
        imgButton3 = mView.findViewById(R.id.img_btn3)
        imgButton4 = mView.findViewById(R.id.img_btn4)
        imgButton5 = mView.findViewById(R.id.img_btn5)
        imgButton6 = mView.findViewById(R.id.img_btn6)

        imgButton1!!.setOnClickListener { v -> onClick(v) }
        imgButton2!!.setOnClickListener { v -> onClick(v) }
        imgButton3!!.setOnClickListener { v -> onClick(v) }
        imgButton4!!.setOnClickListener { v -> onClick(v) }
        imgButton5!!.setOnClickListener { v -> onClick(v) }
        imgButton6!!.setOnClickListener { v -> onClick(v) }
        reload!!.setOnClickListener { v -> onClick(v) }

        spinnerGallery = mView.findViewById(R.id.spinner_gallery)
        initSpinner()
        spinnerGallery!!.visibility = if (mode == 0) View.GONE else View.VISIBLE

        signature!!.setOnSignatureListener(object : SignatureListener {
            override fun onDraw() {
                skipSignature = false
            }
        })
        if (!isDamaged()) {
            table!!.visibility = View.GONE
            headerTable!!.visibility = View.GONE
        }

        builder.setPositiveButton("ENVIAR", null)
        builder.setNegativeButton("CANCELAR") { _, _ -> dismiss() }
        val create = builder.create()

        create.setOnShowListener {
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if (validateMode()) {
                            status = spinnerGallery!!.selectedItemId.toInt()
                            if (!skipSignature) {
                                if (!isDamaged()) {
                                    uploadSign()
                                } else if (image1 != null && isDamaged()) {
                                    uploadSign()
                                    uploadImage()
                                } else {
                                    showSnackBar(mView, "Agregue por lo menos 1 foto")
                                }
                            } else {
                                showSnackBar(mView, "La firma es requerida")
                            }
                        } else {
                            showSnackBar(mView, "Seleccione el estatus de revisión de producto terminado")
                        }
                    }
        }
        return create
    }

    private fun validateMode(): Boolean = if (mode == 1) {
        spinnerGallery!!.selectedItemId.toInt() != 0
    } else true

    fun showSnackBar(view: View, msj: String) {
        val snackbar = Snackbar.make(view, msj, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    private fun isDamaged(): Boolean = totalDamaged != 0.0


    fun onClick(view: View) {
        when (view.id) {
            R.id.btn_reload_signature_gallery -> {
                skipSignature = true
                signature!!.clear()
            }
            else -> {
                if (mPermission.cameraPermission(this.context!!)) {
                    viewButton = view
                    initCamera()
                } else {
                    mPermission.verifyHavePermissions(this.activity!!)
                }
            }
        }
    }

    private fun initCamera() {
        /*try {
            val imageFile = this.context?.let { picturesHelper.createImageFile(it) }
            path = imageFile?.absolutePath.toString()
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (callCameraIntent.resolveActivity(activity?.packageManager!!) != null) {
                val authorities = activity?.packageName + ".fileprovider"
                val imageUri = this.context?.let { imageFile?.let { it1 -> FileProvider.getUriForFile(it, authorities, it1) } }
                callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
            }
        } catch (e: IOException) {
            val toast = Toast.makeText(this.context, "Could not create file!", Toast.LENGTH_LONG)
            toast.show()
        }*/
        val iny = Intent(this.context, CameraActivity::class.java)
        this.startActivityForResult(iny, CAMERA_REQUEST_CODE)
    }

    fun initSpinner() {
        val list = ArrayList<String>()
        list.add("SELECCIONAR")
        list.add("Retenida")
        list.add("Liberada")
        spinnerGallery!!.adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item_textview, list)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    when (viewButton.id) {
                        R.id.img_btn1 -> {

                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)

                            imgButton1!!.setImageBitmap(imagenBitmap)
                            image1 = compressImage(imagenBitmap)

                            table?.setColumnStretchable(1, true)
                            table?.setColumnStretchable(2, true)
                            imgButton2?.visibility = View.VISIBLE
                        }
                        R.id.img_btn2 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton2!!.setImageBitmap(imagenBitmap)
                            image2 = compressImage(imagenBitmap)

                            table?.setColumnStretchable(3, true)
                            table?.isStretchAllColumns = true
                            imgButton3?.visibility = View.VISIBLE
                        }
                        R.id.img_btn3 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton3!!.setImageBitmap(imagenBitmap)
                            image3 = compressImage(imagenBitmap)
                            imgButton4?.visibility = View.VISIBLE
                        }
                        R.id.img_btn4 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton4!!.setImageBitmap(imagenBitmap)
                            image4 = compressImage(imagenBitmap)
                            imgButton5?.visibility = View.VISIBLE
                        }
                        R.id.img_btn5 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton5!!.setImageBitmap(imagenBitmap)
                            image5 = compressImage(imagenBitmap)
                            imgButton6?.visibility = View.VISIBLE
                        }
                        R.id.img_btn6 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton6!!.setImageBitmap(imagenBitmap)
                            image6 = compressImage(imagenBitmap)
                        }
                    }
                }
            }
        }
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(this.context!!)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(this.context!!, bitmap), image, 400f, 400f)
        return image
    }

    private fun uploadSign() {
        if (ifConected()) {
            val sign = signature!!.save()
            if (signature != null) {
                signList.add(compressImage(sign))
                viewModel.uploadImage(signList, "sign")
            } else {
                Toast.makeText(context, "La firma es requerida para hacer el envio", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun uploadImage() {
        if (ifConected()) {
            if (image1 != null) imageList.add(image1!!)
            if (image2 != null) imageList.add(image2!!)
            if (image3 != null) imageList.add(image3!!)
            if (image4 != null) imageList.add(image4!!)
            if (image5 != null) imageList.add(image5!!)
            if (image6 != null) imageList.add(image6!!)
            // Se agrega la cantidad de fotos a el 1 (de la firma )
            imageListSize = imageList.size
            viewModel.uploadImage(imageList, "issue")
        }
    }

    fun addSubscribers() {
        viewModel.onSuccessUpdateImageUrl().subscribe(this::getImagesList).addTo(subscriptions)
    }

    fun getImagesList(list: List<String>) {
        //esperar hasta que se envie toda la lista de imagenes de defectos
        if (isDamaged()) {
            if (viewModel.imageList.size == imageListSize + 1) {
                sendResult(Gson().toJson(list), status)
            }
        }
        //Solo la imagen de la firma
        else {
            sendResult(Gson().toJson(list), status)
        }
    }

    private fun sendResult(message: String, status: Int) {
        if (targetFragment == null) {
            return
        }
        val intent: Intent? = ProductQualityReviewDetailFragment.newIntent(message, status)
        targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
        dismiss()
    }

}
