package com.amplemind.vivasmart.features.crop_validation.viewmodel

import com.amplemind.vivasmart.features.crop_validation.models.CarryOrderHarvest

class ItemCarryOrderHarvestViewModel(carryOrder: CarryOrderHarvest){
    var box_no = carryOrder.box_no.toString()
    var carry_order_id = carryOrder.carry_order_id.toString()
    var crop_id =  carryOrder.crop_id
    var crop_name = carryOrder.crop_name
    var harvest_quality_ok = carryOrder.harvest_quality_ok
    var has_harvest_quality = carryOrder.has_harvest_quality
    var lot_id = carryOrder.lot_id
    var lot_name = carryOrder.lot_name
}