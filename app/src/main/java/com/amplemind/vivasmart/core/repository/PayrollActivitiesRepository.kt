package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel
import com.amplemind.vivasmart.features.menus.*
import io.reactivex.Observable
import javax.inject.Inject

class PayrollActivitiesRepository @Inject constructor() {

    fun getActivities(all: Boolean): Observable<List<PayrollActivitiesModel>> {
        return Observable.just(getActivitiesPayroll(all))
    }

    fun getActivitiesProduction(): Observable<List<PayrollActivitiesModel>> {
        return Observable.just(getActivitiesPayrollProduction())
    }

    fun getActivitiesPackingProduction(): Observable<List<PayrollActivitiesModel>> {
        return Observable.just(getActivitiesPackingQuality())
    }

    fun getChooseActivitiesLines(): Observable<List<PayrollActivitiesModel>> {
        return Observable.just(getActivitiesPayrollChooseLines())
    }

    fun getProductionQualityMenu(): Observable<List<PayrollActivitiesModel>> {
        return Observable.just(getProductionFromQualityMenu())
    }

}