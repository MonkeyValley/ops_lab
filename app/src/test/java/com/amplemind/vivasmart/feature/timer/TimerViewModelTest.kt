package com.amplemind.vivasmart.feature.timer

import com.amplemind.vivasmart.core.navegation.data_mediator.MediatorNavigation
import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.CollaboratorsListReponse
import com.amplemind.vivasmart.core.repository.model.CollaboratorsReponse
import com.amplemind.vivasmart.core.repository.remote.ActivityCodeApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerViewModel
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class TimerViewModelTest {

    private val activityCode: Int = 1
    private val collaboratorId: Int = 1
    private val id: Int = 1
    private val isActive: Boolean = true
    private val isDone: Boolean = false
    private val timeCounter: Double = 90.0
    private val isPause: Boolean = false
    private val temTime: Long = 0L
    private val collaboratorName = "Juan Perez"
    private val userCollaborator = CollaboratorsReponse("2018-02-02", 3333, 1, null, collaboratorName, null)

    private lateinit var repository: ActivityCodeRepository
    private lateinit var viewModel: TimerViewModel
    private var testObserver: TestObserver<List<TimerUserViewModel>> = TestObserver.create()

    private val list = listOf(TimerUserViewModel())

    private val api: ActivityCodeApi = mock()
    private val apiAuthorization: ApiAuthorization = mock()
    private val userAppPreferences: UserAppPreferences = mock()
    private val apiErrors: HandleApiErrors = mock()
    private val uploadRepository: ProfileRepository = mock()
    private val mediatorNavigation: MediatorNavigation = mock()
    private val collaborator = CollaboratorsListReponse(activityCode,
            userCollaborator, collaboratorId, id, isActive, isDone, timeCounter, isPause, temTime)

    @Before
    fun setup() {
        repository = ActivityCodeRepository(api, apiAuthorization, userAppPreferences)
        viewModel = TimerViewModel(repository, apiErrors, uploadRepository, mediatorNavigation)
        list[0].setActivities(collaborator)
        list[0].name_unit = "Nombre unidad"
        list[0].units = 25
        list[0].cost = 45.50
        viewModel.addListCollaborators(list)
    }

    @Test
    fun getCollaboratorName() {
        val name = viewModel.getUserName(0)
        assertNotEquals(name, "")
        assertEquals(name, collaboratorName)
    }

    @Test
    fun setSeekState() {
        viewModel.pauseTimers()
        assertEquals(viewModel.seekState, TimerViewModel.SeekControlState.CONTINUE)
        viewModel.restartTimers()
        assertEquals(viewModel.seekState, TimerViewModel.SeekControlState.PAUSED)
    }

    @Test
    fun listSize() {
        assertEquals(viewModel.listSize(), 1)
    }

    @Test
    fun getUserTime() {
        var time = viewModel.getUserTime(0)
        assertEquals(time, "01:30")
        time = viewModel.getUserTime(1)
        assertEquals(time, "00:00")
    }

    @Test
    fun getUserWork() {
        var work = viewModel.getUserWork(0)
        assertEquals(work, "25")
        work = viewModel.getUserWork(1)
        assertEquals(work, "")
    }

    @Test
    fun getUnitName() {
        var name = viewModel.getUnitName(0)
        assertEquals(name, "Nombre unidad")
        name = viewModel.getUnitName(1)
        assertEquals(name, "")
    }

    @Test
    fun getCost() {
        var cost = viewModel.getCost(0)
        assertEquals(cost, 45.50)
        cost = viewModel.getCost(1)
        assertEquals(cost, 0.0)
    }

    @Test
    fun getUserPayment() {
        var cost = viewModel.getUserPayment(25.0, 0, mediator.bundleItemActivitiesPayroll?.payment!!, mediator.bundleItemActivitiesPayroll?.sunday_rate!!)
        assertEquals(cost, "$625.00")
        cost = viewModel.getUserPayment(25.0, 1, mediator.bundleItemActivitiesPayroll?.payment!!, mediator.bundleItemActivitiesPayroll?.sunday_rate!!)
        assertEquals(cost, "")
    }

    @Test
    fun getPaymentByCode() {
        val cost = viewModel.getPaymentByCode(50.56)
        assertEquals(cost, "$ 50.56 por unidad")
    }

}