package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class UserModel(
        @SerializedName("id")               var id: Long = 0,
        @SerializedName("name")             var name: String = "",
        @SerializedName("role_id")          var roleId: Long = 0,
        @SerializedName("business_unit_id") var businessUnitId: Long = 0,
        @SerializedName("birthdate")        var birthdate: String = "",
        @SerializedName("email")            var email: String = "",
        @SerializedName("image")            var image: String = "",
        @SerializedName("tutorial")         var tutorial: Boolean = true,

        @SerializedName("role")             var role: RoleModel? = null,

        @PrimaryKey
        @SerializedName("uuid")             var uuid: String = getUUID()
): RealmObject(), Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readLong(),
                parcel.readString()!!,
                parcel.readLong(),
                parcel.readLong(),
                parcel.readString()!!,
                parcel.readString()!!,
                parcel.readString()!!,
                parcel.readByte() != 0.toByte(),
                parcel.readParcelable(RoleModel::class.java.classLoader),
                parcel.readString()!!) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeLong(id)
                parcel.writeString(name)
                parcel.writeLong(roleId)
                parcel.writeLong(businessUnitId)
                parcel.writeString(birthdate)
                parcel.writeString(email)
                parcel.writeString(image)
                parcel.writeByte(if (tutorial) 1 else 0)
                parcel.writeParcelable(role, flags)
                parcel.writeString(uuid)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<UserModel> {
                override fun createFromParcel(parcel: Parcel): UserModel {
                        return UserModel(parcel)
                }

                override fun newArray(size: Int): Array<UserModel?> {
                        return arrayOfNulls(size)
                }
        }
}