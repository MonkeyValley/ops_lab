package com.amplemind.vivasmart.features.mipe

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.mipe.fragment.MenuActivitiesMipeFragment
import com.amplemind.vivasmart.features.operations.OperationsActivitiesActivity
import com.amplemind.vivasmart.features.operations.viewModel.OperationsActivitiesViewModel
import com.amplemind.vivasmart.features.production_roster.ChooseActivityFragment
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import io.reactivex.Observable
import kotlinx.android.synthetic.main.content_payroll_activity.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject

@SuppressLint("Registered")
class MipeActivitiesActivity :  BaseActivityWithFragment(), FragmentManager.OnBackStackChangedListener, ChooseActivityFragment.Listener {

    companion object {
        val TAG = OperationsActivitiesActivity::class.java
        val PARAM_STAGE = "$TAG.Stage"
    }

    @Inject
    lateinit var activitiesViewModel: OperationsActivitiesViewModel

    @Inject
    lateinit var preferences: UserAppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mipe_activities2)
        Log.e("---", "OperationsActivity")
        initToolbar()
        callFragmentMenuActivities()
    }

    private fun callFragmentMenuActivities() {
        val support = supportFragmentManager
        support.addOnBackStackChangedListener(this)
        support.beginTransaction()
                .replace(R.id.container_payroll, MenuActivitiesMipeFragment().newInstance(intent.getStringExtra("Title")))
                .commit()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.subtitle = ""
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            val fm = supportFragmentManager
            if (fm.backStackEntryCount > 0) {
                fm.popBackStack()
            } else {
                finish()
                overridePendingTransition(R.anim.enter_right, R.anim.exit_right)
            }
            return false
        }
        return false
    }

    override fun onBackStackChanged() {
        val fragment = this.supportFragmentManager.findFragmentById(R.id.container_payroll)
        fab_menu.collapse()
        if (fragment is ChooseActivityFragment) {
            intent.putExtra("stage_id", fragment.getStageId())
            fab_menu.visibility = View.VISIBLE
        } else {
            fab_menu.visibility = View.GONE
        }
    }

    override fun onGetCounterValue(stage: StageModel, activity: ActivityModel, position: Int): Observable<Int> {
        return activitiesViewModel.countActiveLogsInActivity(stage, activity)
    }

}
