package com.amplemind.vivasmart.core.navegation.data_mediator

import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import io.reactivex.subjects.ReplaySubject

interface IMediator {

    fun observerBundleTimerUserViewModel() : ReplaySubject<TimerUserViewModel>
    fun sendBundleTimerUserViewModel(item : TimerUserViewModel)

    /*fun send(message: String)
    fun observerBundleData(): AsyncSubject<String>?
    fun setItemActivitiesPayrollViewModelNew(mViewModel: ItemActivitiesPayrollViewModelNew)
    fun observeItemActivitiesPayrollViewModelNew(): AsyncSubject<ItemActivitiesPayrollViewModelNew>*/
}