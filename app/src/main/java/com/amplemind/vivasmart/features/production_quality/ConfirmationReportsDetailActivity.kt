package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.titleBold
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_confirmation_reports_detail.*
import javax.inject.Inject

class ConfirmationReportsDetailActivity : BaseActivity() {

    private val TAG = ProductionQualityActivity::class.java.simpleName

    lateinit var viewModel: ConfirmationReportsItemViewModel

    @Inject
    lateinit var mPref: UserAppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation_reports_detail)
        viewModel = ConfirmationReportsItemViewModel(mPref, null, intent.getParcelableExtra("report"), intent.getStringExtra("position"))
        setupToolbar()
        setupListeners()
        setupView()
    }

    private fun setupView() {
        Glide.with(this).load(viewModel.url).into(card_image)
        tv_report_position.text = viewModel.position
        tv_category.text = viewModel.category
        tv_groves.text = viewModel.grooves

        tv_description.titleBold(viewModel.description)
    }

    private fun setupListeners() {
        btn_delete_report.setOnClickListener {
            val i = Intent()
            i.putExtra("id", viewModel.issueId)
            setResult(Activity.RESULT_OK, i)
            finish()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_reports_detail.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = viewModel.getTableName()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
