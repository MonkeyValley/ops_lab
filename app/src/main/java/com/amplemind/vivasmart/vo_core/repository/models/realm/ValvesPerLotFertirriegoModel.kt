package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class ValvesPerLotFertirriegoModel(
        @SerializedName("T1") var t1List: RealmList<ValveFertirriego>? = RealmList(),
        @SerializedName("T2") var t2List: RealmList<ValveFertirriego>? = RealmList(),
        @SerializedName("T3") var t3List: RealmList<ValveFertirriego>? = RealmList(),
        @SerializedName("T4") var t4List: RealmList<ValveFertirriego>? = RealmList(),
        @SerializedName("T5") var t5List: RealmList<ValveFertirriego>? = RealmList(),
        @SerializedName("T6") var t6List: RealmList<ValveFertirriego>? = RealmList(),
        @SerializedName("lot_id") var lotId: Int = 0,
        @SerializedName("type") var type: String = "",
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class ValveFertirriego(
        @SerializedName("valve") var valveName: String = "",
        @SerializedName("valve_expense") var valveExpense: Double? = 0.0,
        @SerializedName("valve_head") var valveHead: Int = 0,
        @SerializedName("valve_id") var valveId: Int = 0,
        @SerializedName("valve_irrigation") var valveIrrigation: String = "",
        @SerializedName("valve_production_location") var valeProductionLocation: String? = null,
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable