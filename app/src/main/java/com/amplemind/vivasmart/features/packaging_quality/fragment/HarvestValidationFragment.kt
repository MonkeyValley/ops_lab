package com.amplemind.vivasmart.features.packaging_quality.fragment


import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.remote.models.HarvestValidationCarryOrderModel
import com.amplemind.vivasmart.features.packaging_quality.adapters.HarvestValidationAdapter
import com.amplemind.vivasmart.features.packaging_quality.viewModel.HarvestQualitySettingViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.HarvestValidationViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.HarvestValidationModel
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.fragment_harvest_validation.*
import retrofit2.HttpException
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class HarvestValidationFragment : BaseFragment() {

    companion object {

        private val TAG = HarvestValidationFragment::class.java.simpleName

        private val PARAM_CARRY_ORDER_ID = "$TAG.CarryOrderId"
        private val PARAM_CARRY_ORDER_CODE = "$TAG.CarryOrderCode"
        private val PARAM_LOT_NAME = "$TAG.LotName"
        private val PARAM_BOX_COUNT = "$TAG.BoxCount"
        private val PARAM_HAS_HARVEST_QUALITY = "$TAG.HasHarvestQuality"

        fun newInstance(carryOrder: HarvestValidationCarryOrderModel) =
                HarvestValidationFragment().apply {
                    arguments = Bundle().apply {
                        putLong(PARAM_CARRY_ORDER_ID, carryOrder.carryOrderId)
                        putString(PARAM_CARRY_ORDER_CODE, carryOrder.carryOrderId.toString())
                        putString(PARAM_LOT_NAME, carryOrder.lotName)
                        putDouble(PARAM_BOX_COUNT, carryOrder.boxNo)
                        putBoolean(PARAM_HAS_HARVEST_QUALITY, carryOrder.hasHarvestQuality ?: false)
                    }
                }
    }

    @Inject
    lateinit var mViewModel: HarvestValidationViewModel

    lateinit var mAdapter: HarvestValidationAdapter

    private lateinit var mCarryOrderCode: String
    private lateinit var mLotName: String
    private var mBoxCount: Double = -1.0

    init {
        argumentsNeeded = true
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_harvest_validation, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mViewModel.cleanUp()
    }

    override fun readArguments(args: Bundle) {
        args.apply {
            mCarryOrderCode =
                    requireNotNull(getString(PARAM_CARRY_ORDER_CODE)) { "$TAG: $PARAM_CARRY_ORDER_CODE is required!" }

            mLotName =
                    requireNotNull(getString(PARAM_LOT_NAME)) { "$TAG: $PARAM_LOT_NAME is required!" }

            mBoxCount = getDouble(PARAM_BOX_COUNT, -1.0)
            require(mBoxCount > -1.0) { "$TAG: $PARAM_BOX_COUNT is required!" }

            mViewModel.carryOrderId = getLong(PARAM_CARRY_ORDER_ID, -1)
            require(mViewModel.carryOrderId > -1) { "$TAG: $PARAM_CARRY_ORDER_ID is required!" }

            mViewModel.hasHarvestQuality = getBoolean(PARAM_HAS_HARVEST_QUALITY, false)
        }
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()

        btn_send.clicks()
                .subscribe {
                    showSendConfirmationDialog()
                }
                .addTo(subscriptions)
    }

    override fun setUpCallbacks() {
        super.setUpCallbacks()

        mViewModel.onValidationLoaded.subscribe(this::onValidationLoaded).addTo(subscriptions)
    }

    override fun setUpUI() {
        super.setUpUI()
        setUpCarryOrderInfo()
        setUpRecyclerView()
        setReadOnlyIfHasValidation()
    }

    override fun loadData() {
        super.loadData()
        mViewModel.loadHarvestQuality().subscribe(this::onHarvestQualityLoaded).addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        sendSetToolbarTitleEvent(getString(R.string.hvf_subtitle))
    }

    private fun onHarvestQualityLoaded(harvestQuality: List<HarvestQualitySettingViewModel>) {
        showProgressLayout(false)
        mAdapter.setItems(harvestQuality)
    }

    private fun onValidationLoaded(validation: HarvestValidationModel) {
        et_comments.setText(validation.qualityComment)
    }

    private fun onValidationCreated(validation: HarvestValidationModel) {
        mEventBus.send(OnHarvestValidationFinished(this, validation))
    }

    private fun onValidationCreationError(error: Throwable) {
        showProgressLayout(false)
        val msg = when (error) {
            is IOException -> getString(R.string.network_error)
            is HttpException -> getString(R.string.server_error).format(error.response())
            is TimeoutException -> getString(R.string.timeout_error)
            else -> getString(R.string.default_error)
        }

        AlertDialog.Builder(context!!)
                .setTitle(R.string.title_network_error)
                .setMessage(msg)
                .setPositiveButton(R.string.accept, null)
                .show()
    }

    private fun createValidation() {
        showProgressLayout(true)
        mViewModel.createHarvestValidation(mAdapter.getSettings(), et_comments.text.toString())
                .subscribe(this::onValidationCreated, this::onValidationCreationError)
                .addTo(subscriptions)
    }

    private fun setUpCarryOrderInfo() {
        tv_code.text = mCarryOrderCode
        tv_lot_name.text = mLotName
        tv_box_count.text = "$mBoxCount"
    }

    private fun setUpRecyclerView() {
        mAdapter = HarvestValidationAdapter()

        rv_criteria.adapter = mAdapter
        rv_criteria.setHasFixedSize(true)
        rv_criteria.layoutManager = LinearLayoutManager(context!!)
    }

    private fun setReadOnlyIfHasValidation() {
        if (mViewModel.hasHarvestQuality) {
            btn_send.visibility = View.GONE
            et_comments.isEnabled = false
            mAdapter.readOnly = true
        }
    }

    private fun showSendConfirmationDialog() {
        AlertDialog.Builder(context!!)
                .setMessage(R.string.hvf_send_confirmation_msg)
                .setPositiveButton(R.string.accept) { _, _ ->
                    createValidation()
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun showProgressLayout(show: Boolean) {
        layout_progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    data class OnHarvestValidationFinished(
            val fragment: HarvestValidationFragment,
            val validation: HarvestValidationModel
    )
}
