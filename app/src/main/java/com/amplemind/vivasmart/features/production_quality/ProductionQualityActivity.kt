package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.getSpanCount
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlQualityItemViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.QualityControlViewModel
import kotlinx.android.synthetic.main.activity_production_quality.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

class ProductionQualityActivity : BaseActivity() {

    private val TAG = ProductionQualityActivity::class.java.simpleName
    private val LOTS_REQUEST = 1

    @Inject
    lateinit var viewModel: QualityControlViewModel

    private fun getLayout() = R.layout.activity_production_quality

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        setupToolbar()
        initRecycler()
        getData()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getLots()?.addTo(subscriptions)
        /*
        if (isOnlineMode(this)) {
            viewModel.getLots()?.addTo(subscriptions)
        } else {
            viewModel.getLocalLots().addTo(subscriptions)
        }
        */
    }

    private fun getData() {
        viewModel.flow = QualityControlViewModel.LOT_FLOW.QUALITY_PRODUCTION
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onGetLots().subscribe(this::onGetLots).addTo(subscriptions)
    }

    private fun onGetLots(lots: List<ControlQualityItemViewModel>) {
        (rv_production_quality.adapter as ProductionQualityAdapter).addElements(lots)
        /*
        if (!isOnlineMode(this)) {
            root_production_quality.snackWithoutInternetMessage()
        }
        */
    }

    private fun initRecycler() {
        rv_production_quality.hasFixedSize()
        rv_production_quality.layoutManager = GridLayoutManager(this, getSpanCount(this))
        rv_production_quality.addItemDecoration(ItemOffsetDecoration(4.toPx()))
        rv_production_quality.adapter = ProductionQualityAdapter(this)
        (rv_production_quality.adapter as ProductionQualityAdapter).onItemClicked().subscribe(this::itemClicked).addTo(subscriptions)
    }

    private fun itemClicked(item: ControlQualityItemViewModel) {
        val intent = Intent(this, ProductionCategoriesActivity::class.java)

        intent.putExtra("lot_name", item.getLotName())
        intent.putExtra("lot_id", item.lotId)
        intent.putExtra("stage_id", item.stageId)
        intent.putExtra("is_closed", item.isComplete())
        intent.putParcelableArrayListExtra("tables", item.tablesGrooves as java.util.ArrayList<out Parcelable>)
        startActivityForResult(intent, LOTS_REQUEST)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_quality
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.production_quality)
        lbl_track_title.text = "/ Calidad / Producción / Control /"

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LOTS_REQUEST && resultCode == Activity.RESULT_OK) {
            viewModel.getLots()?.addTo(subscriptions)
            /*
            if (isOnlineMode(this)) {
                viewModel.getLots()?.addTo(subscriptions)
            } else {
                viewModel.getLocalLots().addTo(subscriptions)
            }
            */
        }
    }
}
