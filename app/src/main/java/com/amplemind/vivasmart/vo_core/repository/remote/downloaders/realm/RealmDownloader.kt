package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.dao.realm.RealmDao
import com.amplemind.vivasmart.vo_core.repository.remote.downloaders.Downloader
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.RealmObject

abstract class RealmDownloader<ParentType: RealmObject?, ChildType: RealmObject> (
        realmHolder: RealmHolder,
        modelClass: Class<ChildType>
) : RealmDao<ChildType>(realmHolder, modelClass), Downloader<ParentType, ChildType> {

    override fun findAll() =
            findAll(false)

    override fun findByUuid(uuid: String): Maybe<ChildType> =
            findByUuid(uuid, false)

    override fun getLastVersion(): Single<Long> =
            executeTransaction { realm ->
                if (realm.schema.get(modelClass.simpleName)?.hasField("version") == true) {
                    query(realm).max("version")?.toLong() ?: 0
                } else {
                    0
                }
            }

    override fun beforeSaving(parent: ParentType, child: ChildType): Single<ChildType> {
        return Single.just(child)
    }

    override fun saveServerResponse(parent: ParentType, response: List<ChildType>): Single<List<ChildType>> =
            Observable.fromIterable(response)
                    .concatMapSingle { child ->
                        beforeSaving(parent, child)
                    }
                    .toList()
                    .flatMap{ preparedChildren ->
                        Log.e(TAG, "Children to save: ${preparedChildren.size}")
                        executeListTransaction { realm ->
                            realm.copyToRealmOrUpdate(preparedChildren)
                        }
                    }

    override fun delete(uuid: String): Single<ChildType> =
            deleteByUuid(uuid,null,null)

    override fun clear() = cleanUp()

    override fun clearCache() {
    }

}