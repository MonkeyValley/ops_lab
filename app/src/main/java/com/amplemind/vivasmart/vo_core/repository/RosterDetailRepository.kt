package com.amplemind.vivasmart.vo_core.repository

import android.util.Log
import android.util.SparseArray
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.CollaboratorsDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.google.gson.Gson
import io.reactivex.Maybe
import io.reactivex.Single
import java.io.File
import javax.inject.Inject

class RosterDetailRepository @Inject constructor(
        private val collaboratorsDao: CollaboratorsDao,
        private val activityLogsDao: ActivityLogsDao
) : Repository() {

    enum class Type { SIGNED, UNSIGNED }

    override fun cleanUp() = activityLogsDao.cleanUp()

    fun loadCollaborator(collaboratorUuid: String): Maybe<CollaboratorModel> = collaboratorsDao.findByUuid(collaboratorUuid)

    fun loadActivityLogs(stageUuid: String, collaboratorUuid: String, type: Type): Single<List<ActivityLogModel>> =
            if (type == Type.UNSIGNED) {
                activityLogsDao.findCollaboratorUnsignedLogs(stageUuid, collaboratorUuid)
            } else {
                activityLogsDao.findCollaboratorSignedLogs(stageUuid, collaboratorUuid)
            }

    fun setQuantityAndTime(activityLog: ActivityLogModel, quantity: Int, timeInMinutes: Int) =
            activityLogsDao.setQuantityAndTime(activityLog, quantity, timeInMinutes)

    fun assignGrooves(activityLog: ActivityLogModel, grooves: SparseArray<List<Int>>) =
            activityLogsDao.assignGrooves(activityLog.uuid, grooves)

    fun finishActivityLogs(activityLogs: List<ActivityLogModel>, isTraining: Boolean, isPractice: Boolean, isControl: Boolean, signature: File?) =
            activityLogsDao.sign(activityLogs, signature, isTraining, isPractice, isControl)

}