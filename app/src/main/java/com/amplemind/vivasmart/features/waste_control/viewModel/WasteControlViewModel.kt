package com.amplemind.vivasmart.features.waste_control.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.WasteControlRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.waste_control.models.WasteControlItemViewModel
import com.amplemind.vivasmart.features.waste_control.models.WasteControlResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class WasteControlViewModel @Inject constructor(private val repository: WasteControlRepository, private val apiErrors: HandleApiErrors) : BaseViewModel() {
    private val list = mutableListOf<WasteControlItemViewModel>()
    private val requestSuccess = BehaviorSubject.create<List<WasteControlItemViewModel>>()

    fun getList(day: String): Disposable {
        return repository.getList(day)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate {
                    progressStatus.onNext(false)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelListToViewModel)
                .subscribe({
                    list.clear()
                    list.addAll(it)
                    requestSuccess.onNext(list)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelListToViewModel(result: WasteControlResponse): List<WasteControlItemViewModel> {
        val viewModels = mutableListOf<WasteControlItemViewModel>()
        result.data.forEach { obj ->
            viewModels.add(WasteControlItemViewModel(obj))
        }
        viewModels.sortBy { it.id }
        viewModels.reverse()
        return viewModels
    }

    fun onSuccessRequest(): BehaviorSubject<List<WasteControlItemViewModel>> {
        return requestSuccess
    }

}