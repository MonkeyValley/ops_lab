package com.amplemind.vivasmart.features.collaborators.viewModel

import android.util.SparseArray
import collections.forEach
import com.amplemind.vivasmart.vo_core.repository.AssignGroovesRepository
import com.amplemind.vivasmart.vo_core.repository.models.extensions.tablesAndGrooves
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject


class AssingGroovesViewModel @Inject constructor(
        private val repository: AssignGroovesRepository
) {

    lateinit var activityLogUuid: String
    lateinit var activityCodeUuid: String

    fun cleanUp() {
        repository.cleanUp()
    }

    fun loadActivityLog() =
            repository.loadActivityLog(activityLogUuid)

    fun loadGrooves(): Maybe<List<TableGrooves>> =
            repository.loadActivityCode(activityCodeUuid)
                    .map { activityCode ->
                        val result = mutableListOf<TableGrooves>()

                        getTablesAndGrooves(activityCode)?.forEach { tableNo, grooveTotal ->
                            result.add(
                                    TableGrooves(
                                            activityCodeUuid,
                                            activityLogUuid,
                                            tableNo,
                                            grooveTotal,
                                            repository.loadGrooves(activityCodeUuid, tableNo)
                                    )
                            )
                        }

                        return@map result
                    }

    fun assignGrooves(grooves: SparseArray<List<Int>>) =
            repository.assignGrooves(activityLogUuid, grooves)

    private fun getTablesAndGrooves(activityCode: ActivityCodeModel) =
            activityCode.stage?.lot?.tablesAndGrooves()

    data class TableGrooves (
            val activityCodeUuid: String,
            val activityLogUuid: String,
            val tableNo: Int,
            val grooveTotal: Int,
            val changes: Single<List<GrooveModel>>
    )

}
