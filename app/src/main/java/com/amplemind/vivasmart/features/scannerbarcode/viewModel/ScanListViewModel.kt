package com.amplemind.vivasmart.features.scannerbarcode.viewModel

import android.content.Intent
import com.amplemind.vivasmart.core.repository.CollaboratorsRepository
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemCollaboratorsViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ScanListViewModel @Inject constructor(val repository : CollaboratorsRepository) {

    private val newCode = BehaviorProcessor.create<ItemCollaboratorsViewModel>()

    private var collaborators = mutableListOf<ItemCollaboratorsViewModel>()

    private var collaboratorsSubject = BehaviorSubject.create<List<ItemCollaboratorsViewModel>>()

    fun setupData(intent : Intent) {
        val item = ItemCollaboratorsViewModel(CollaboratorModel(-1, intent.getStringExtra("CODE"), "https://www.lainformacion.com/files/article_default_content/uploads/2018/03/20/5ab13436cbab7.jpeg", "1", ""))
        newCode.onNext(item)
    }

    fun loadData(): BehaviorProcessor<ItemCollaboratorsViewModel> {
        return newCode
    }

    fun getCollaborators() : BehaviorSubject<List<ItemCollaboratorsViewModel>> {
        return collaboratorsSubject
    }

    private fun mapModelToViewModels(models: List<CollaboratorModel>): List<ItemCollaboratorsViewModel>  {
        val list = arrayListOf<ItemCollaboratorsViewModel>()
        for (item in models){
            list.add(ItemCollaboratorsViewModel(item))
        }
        return list
    }

}
