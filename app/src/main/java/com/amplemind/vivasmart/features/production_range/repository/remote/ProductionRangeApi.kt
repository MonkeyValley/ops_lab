package com.amplemind.vivasmart.features.production_range.repository.remote

import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangeVariatiesModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangesDataModel
import com.amplemind.vivasmart.vo_core.repository.responses.MessageResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectResponse
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectSingleResponse
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.*

interface ProductionRangeApi {

    //https://dev.vivasmart.com.mx/v2/production_var/lastweek/11
    @GET("/v2/production_var/lastweek/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getProductionRangeWeeksData(
            @Header("Authorization")    authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<ObjectSingleResponse<ProductionRangesDataModel>>


    //https://dev.vivasmart.com.mx/v2/production_var?embed=production_var_type,production_var_data_type,soil_type,options&business_unit_id=1
    @GET("/v2/production_var")
    @Headers("Content-Type: application/json")
    fun getProductionRangeVarieties(
            @Header("Authorization")    authentication: String,
            @Query("embed") embed : String = "production_var_type,production_var_data_type,soil_type,crops.crop,options,activity_sub_category",
            @Query("business_unit_id") businessId: Long
    ): Single<ObjectResponse<ProductionRangeVariatiesModel>>

    //https://dev.vivasmart.com.mx/v2/message?receiver_id=5
    @GET("/v2/message")
    @Headers("Content-Type: application/json")
    fun getMessagesByUserID(
            @Header("Authorization")        authentication: String,
            @Query("receiver_id")               userId: Long = 0
    ): Single<ObjectResponse<MessageModel>>

}