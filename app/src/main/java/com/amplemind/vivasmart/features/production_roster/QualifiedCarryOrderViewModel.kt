package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.QuantityOrderRepository
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderDetailResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class QualifiedCarryOrderViewModel @Inject constructor(private val repository: QuantityOrderRepository,
                                                       private val mediator: Mediator,
                                                       private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val qualified_list = mutableListOf<ItemQualifiedViewModel>()

    private val requestSuccess = BehaviorSubject.create<List<ItemQualifiedViewModel>>()

    /*
    fun getLoadQualifiedCarry(): Disposable {
        return repository.loadQualifiedCarry(mediator.bundleControlQualityItemViewModel.lotId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel)
                .doOnError { progressStatus.onNext(false) }
                .subscribe({
                    qualified_list.addAll(it)
                    requestSuccess.onNext(qualified_list)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }
    */

    fun getLotName() : String? {
        return mediator.bundleControlQualityItemViewModel.lot!!.name
    }

    fun getQualifiedOrders(): BehaviorSubject<List<ItemQualifiedViewModel>> {
        return requestSuccess
    }

    private fun mapModelToViewModel(result: QualityCarryOrderDetailResponse): List<ItemQualifiedViewModel> {
        val viewModels = mutableListOf<ItemQualifiedViewModel>()

        result.data.forEach { order ->
            viewModels.add(ItemQualifiedViewModel(order))
        }
        return viewModels
    }

}
