package com.amplemind.vivasmart.core.dagger.module.fragment

import com.amplemind.vivasmart.core.base.dagger.scope.FragmentScope
import com.amplemind.vivasmart.features.production_roster.*
import com.amplemind.vivasmart.features.production_roster.dialogs.FinalizeUserTimerDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.PauseTimerDialog
import com.amplemind.vivasmart.features.production_roster.fragments.AssignUnitHarvestDialogFragment
import com.amplemind.vivasmart.features.production_roster.fragments.DailyReportFragment
import com.amplemind.vivasmart.vo_features.production_roster.TimerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProductionRosterFragmentProvider {

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindTimerFragment(): TimerFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindFinalizeUserTimerDialog(): FinalizeUserTimerDialog

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindEditQuantityFragment(): EditQuantityFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindPauseTimerDialog(): PauseTimerDialog

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindAssingUnitDialogFragment(): AssignUnitDialogFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindAssignUnitHarvestDialogFragment(): AssignUnitHarvestDialogFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindRevisedRosterFragment(): RevisedRosterFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindProcessRosterFragment(): ProcessRosterFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindTimerLinesFragment(): TimerLinesFragment

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindDailyReportFragment(): DailyReportFragment

}