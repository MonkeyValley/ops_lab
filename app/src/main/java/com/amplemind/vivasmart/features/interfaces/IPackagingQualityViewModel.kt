package com.amplemind.vivasmart.features.interfaces

import io.reactivex.disposables.Disposable

interface IPackagingQualityViewModel {

    fun loadDummyData() : Disposable

}
