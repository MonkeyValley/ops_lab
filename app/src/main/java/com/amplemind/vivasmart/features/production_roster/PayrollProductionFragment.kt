package com.amplemind.vivasmart.features.production_roster

import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.mipe.MipeMonitoringLotsActivity
import com.amplemind.vivasmart.features.fertiriego.FertiriegoActivitiesActivity
import com.amplemind.vivasmart.features.main.MainActivity
import com.amplemind.vivasmart.features.mipe.MipeActivitiesActivity
import com.amplemind.vivasmart.features.operations.OperationsActivitiesActivity
import com.amplemind.vivasmart.features.phenology.PhenologyActivitiesActivity
import kotlinx.android.synthetic.main.content_payroll_menu_fragment.*
import javax.inject.Inject


@SuppressLint("Registered")
class PayrollProductionFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: PayrollProductionViewModel

    fun newInstance(): PayrollProductionFragment {
        return PayrollProductionFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_payroll_menu_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).supportActionBar!!.title = "Inicio"

        initRecycler()

        viewModel.loadMenuActivities().addTo(subscriptions)
        viewModel.getMenu().subscribe(this::setDataAdapter).addTo(subscriptions)
    }

    fun setDataAdapter(menus: List<ItemMenuPayrollViewModel>) {
        (rv_menu_payroll.adapter as MenuListPayrollAdapter).addElements(menus)
        (rv_menu_payroll.adapter as MenuListPayrollAdapter).onClickItem().subscribe(this::clickActivityPayroll).addTo(subscriptions)
    }


    private fun clickActivityPayroll(title: String) {
        val bndlanimation = ActivityOptions.makeCustomAnimation(context, R.anim.enter, R.anim.exit).toBundle()
        if (title == "Nómina" || title == "Calidad") {
            startActivity(Intent(context, ProductionRosterActivitiesActivity::class.java).putExtra("Title", title), bndlanimation)
        }
        else if(title == "MIPE"){
            startActivity(Intent(context, MipeActivitiesActivity::class.java).putExtra("Title", title), bndlanimation)
        }
        else if(title == "Fertirriego"){
            startActivity(Intent(context, FertiriegoActivitiesActivity::class.java).putExtra("Title", title), bndlanimation)
        }
        else if(title == "Fenología"){
            startActivity(Intent(context, PhenologyActivitiesActivity::class.java).putExtra("Title", title), bndlanimation)
        }
        else if(title == "Operaciones"){
            startActivity(Intent(context, OperationsActivitiesActivity::class.java).putExtra("Title", title), bndlanimation)
        }
        else {
            Toast.makeText(context,"En construcción", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initRecycler() {
        rv_menu_payroll.hasFixedSize()
        rv_menu_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_payroll.adapter = MenuListPayrollAdapter()
    }

}
