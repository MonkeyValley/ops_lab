package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.CollaboratorsDao
import com.amplemind.vivasmart.core.repository.local.CollaboratorsLineDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.remote.BoxCountLinesRepository
import com.amplemind.vivasmart.core.repository.remote.CollaboratorsApi
import com.amplemind.vivasmart.core.repository.request.SyncDeleteCollaborators
import com.amplemind.vivasmart.core.repository.request.UpdatePresentationRequest
import com.amplemind.vivasmart.core.repository.response.ActivitiesResponse
import com.amplemind.vivasmart.core.repository.response.ActivityLinesResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.core.utils.TIME_OUT_SYNC
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import io.reactivex.Single
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class CollaboratorsRepository @Inject constructor(
        private var countBoxRepository: BoxCountLinesRepository,
        private val api: CollaboratorsApi,
        private val database: CollaboratorsDao,
        preferences: UserAppPreferences,
        private val databaseLine: CollaboratorsLineDao) {

    data class CollaboratorsResponse(val data: ArrayList<CollaboratorsListModel>, @SerializedName("total_count") val total_count: Int)

    data class SendCollaboratorsReponse(@SerializedName("bulk") val data: MutableList<SendCollaboratorsModel>)

    data class AssingCollaborators(@SerializedName("bulk") val data: List<AssignCollaboratorModel>, @SerializedName("count") val count: CountBoxRequest?)

    @Inject
    lateinit var authorization: ApiAuthorization


    val businessUnitId = Gson().fromJson(preferences.userInfo, UserModel::class.java).businessUnitId
            ?: ""

    private fun getCurrentDate(): String {
        val sdf = SimpleDateFormat("dd-MMM-yyyy hh:mm:ss", Locale.getDefault())
        return sdf.format(Date())
    }

    fun getCollaborators(token: String, offset: Int = 0, stage_or_line: Int, lines_flow: Boolean = false): Observable<CollaboratorsResponse> {
        return if (lines_flow) {
            api.getCollaborators(authentication = authorization.getAuthToken(token), offset = offset, stage = stage_or_line, package_path = "collaborator", paysheet = "paysheet/", packingline = "packingline/", business_unit_id = businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        } else {
            api.getCollaborators(authentication = authorization.getAuthToken(token), offset = offset, stage = stage_or_line, business_unit_id = businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        }
    }

    fun searchCollaboratorByCode(token: String, code: String, stage: Int, lines_flow: Boolean): Observable<CollaboratorsResponse> {
        return if (lines_flow) {
            api.searchCollaboratorByCode(authentication = authorization.getAuthToken(token), code = code, stage = stage, package_path = "collaborator", paysheet = "paysheet/", packingline = "packingline/", business_unit_id = businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        } else {
            api.searchCollaboratorByCode(authentication = authorization.getAuthToken(token), code = code, stage = stage, business_unit_id = businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        }
    }

    fun getCollaboratorsRecent(): Single<List<CollaboratosRecent>> {
        return database.getRecentCollaborators()
    }

    fun sendCollaborators(token: String, collaborators: SendCollaboratorsReponse): Observable<CollaboratorsReponseActivities> {
        return api.sendCollaborators(authorization.getAuthToken(token), collaborators)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLineActivities(token: String, packline: String): Observable<ActivityLinesResponse> {
        return api.getLinesActivities(authorization.getAuthToken(token), packingline = packline, packingline_id = packline, business_unit_id = businessUnitId, packingline_id_embed = packline)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLineActivitiesLocal(idLine: Int): Observable<List<ActivitiesResponse>> {
        return Observable.fromCallable {
            val line = databaseLine.getLine(idLine)
            val crops = line.crops
            val activities = databaseLine.getLocalActivities(businessUnitId)

            val filtersActivities = mutableListOf<ActivitiesResponse>()

            activities.forEach { item ->
                if (compareCrops(item.crops, crops)){
                    filtersActivities.add(item)
                }
            }

            return@fromCallable filtersActivities
        }
    }

    private fun compareCrops(cropsActivity: List<Crop>?, cropsLine: List<CropLine>): Boolean {
        cropsActivity?.forEach { activity ->
            val result = cropsLine.any { it.crop_id == activity.cropId }
            if (result) {
                return true
            }
        }
        return false
    }

    fun getCollaboratorsSearch(token: String, offset: Int = 0, query: String, stage: Int, lines_flow: Boolean): Observable<CollaboratorsResponse> {
        return if (lines_flow) {
            api.searchCollaboratorByName(authentication = authorization.getAuthToken(token), offset = offset, code = query, stage = stage, package_path = "collaborator", paysheet = "paysheet/", packingline = "packingline/", business_unit_id = businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        } else {
            api.searchCollaboratorByName(authentication = authorization.getAuthToken(token), offset = offset, code = query, stage = stage, business_unit_id = businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        }
    }

    fun saveCollaboratorsRecent(data: List<CollaboratorsListReponse>) {
        for (it in data) {
            database.insertRecentCollaborator(CollaboratosRecent(null,
                    it.collaborator.id ?: it.collaborator.employeeCode.toInt(),
                    it.collaborator.name,
                    it.collaborator.image ?: "",
                    it.collaborator.birthdate,
                    it.collaborator.employeeCode,
                    getCurrentDate()))
        }
        database.deleteOldCollaborators()
    }

    fun getPresentations(token: String, line_package: String): Observable<PresentationResponse> {
        return api.getPresentation(authorization.getAuthToken(token), line_package, businessUnitId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun assingCollaborator(token: String, assing: AssingCollaborators): Observable<CollaboratorsInLinesResult> {
        return api.assignCollaborator(authorization.getAuthToken(token), assing)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun updatePresentation(token: String, request: UpdatePresentationRequest): Observable<Any> {
        return api.updatePresentation(authorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun syncRosterCollaborators(token: String, request: SyncRosterProductionRequest): Observable<SyncRosterProductionResponse> {
        return api.syncCollaborators(authorization.getAuthToken(token), request)
                .timeout(TIME_OUT_SYNC, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun syncRosterCollaboratorsDeleted(token: String, request: SyncDeleteCollaborators): Observable<Any> {
        return api.syncDeleteCollaborators(authorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun saveActivities(activities: List<ActivitiesResponse>) {
        return databaseLine.insertActivities(activities)
    }

    fun savePresentation(presentations: List<PresentationResultResponse>) {
        return databaseLine.insertPresentation(presentations)
    }

    fun getPresentationsLocal(idLine: Int): Observable<List<PresentationResultResponse>> {
        return Observable.fromCallable {
            val lines = databaseLine.getLine(idLine)
            val presentations = databaseLine.getLocalPresentation(businessUnitId)

            val filterPresentation = mutableListOf<PresentationResultResponse>()

            presentations.forEach { item ->
                if (lines.crops.any { it.crop_id == item.crop_id }){
                    filterPresentation.add(item)
                }
            }

            return@fromCallable filterPresentation
        }
    }


    fun saveCollaboratorsAndLineLocal(list: MutableList<AssignCollaboratorModel>, idCollaborators: List<CollaboratorModel>,
                                      name: String?, collaborator_left: Int?, activity_id: Int?, id_line: Int, packing: PresentationResultResponse? = null, count_box: CountBoxRequest?): Observable<CollaboratorsInLinesResult> {
        return Observable.fromCallable {
            val collaborators = mutableListOf<PackingLineCollaboratorsModel>()

            countBoxRepository.distributeCountingBox(id_line, count_box)

            saveCountLocal(count_box, id_line)

            list.forEach { user ->
                val user_item = idCollaborators.first { it.id == user.collaborator_id }
                collaborators.add(createCollaboratorLocal(user, user_item, packing))
            }

            val line = insertCollaboratorAndLinesInDb(activity_id, id_line, name, collaborator_left, collaborators)

            savePresentationLocal(packing, id_line)

            return@fromCallable CollaboratorsInLinesResult(arrayListOf(line), false)
        }
    }


    private fun insertCollaboratorAndLinesInDb(activity_id: Int?, id_line: Int, name: String?, collaborator_left: Int?, collaborators: MutableList<PackingLineCollaboratorsModel>): CollaboratorsInLinesResponse {
        val line = CollaboratorsInLinesResponse(null, activity_id, id_line, name
                ?: "", collaborator_left ?: 0, packingline = collaborators)

        val idLine: Int

        val idLineDb = databaseLine.existLine(id_line, activity_id!!)

        idLine = if (idLineDb != null) {
            updateLine(idLineDb.id!!, collaborators.size)
            idLineDb.id!!
        } else {
            line.id = line.id_line!! + line.id_activity!!
            insertLine(line, collaborator_left ?: 0, collaborators.size)
        }

        collaborators.map { it.id_line = idLine }

        collaborators.forEach { item ->
            val id = databaseLine.insertCollaboratorInLine(item)
            item.id = id.toInt()
        }

        return line
    }

    private fun savePresentationLocal(packing: PresentationResultResponse?, id_line: Int) {
        if (packing != null) {
            val exist = databaseLine.existPresentation(packing.id, id_line) ?: false

            if (!exist) {
                databaseLine.saveCountBox(BoxCountResultResponse(null, 0, packing.id, id_line, packing))
            }
        }
    }

    private fun saveCountLocal(count_box: CountBoxRequest?, id_line: Int) {
        count_box?.presentation?.forEach { box ->
            databaseLine.updateCountingBox(box.id, box.total, id_line)
        }
    }

    private fun createCollaboratorLocal(it: AssignCollaboratorModel, first: CollaboratorModel, packing: PresentationResultResponse? = null): PackingLineCollaboratorsModel {
        return PackingLineCollaboratorsModel(it.activity_id, it.collaborator_id, null, false, true, it.isTime,
                it.packing_id, it.packingline_id, 0L, 0.0, units = 0f, units_olds = 0f, date = 0, packing = packing, collaborator = CollaboratorsReponse("", first.employeeCode!!, first.id, "", first.name!!))
    }

    /**
     * the information of the line is inserted, and the variable collaborator_left is updated,
     * subtracting the collaborators that will be inserted later
     */
    private fun insertLine(line: CollaboratorsInLinesResponse, collaborator_left: Int, lessColaborators: Int): Int {
        line.collaborator_left = (collaborator_left - lessColaborators)
        return databaseLine.insertLine(line).toInt()
    }

    /**
     *  the line is not inserted,
     *  only the counting of colaborators_left is updated
     */
    private fun updateLine(idLineDb: Int, collaborators: Int) {
        databaseLine.updateCollaboratorLeft(idLineDb, collaborators)
    }


    fun updatePresentationOffline(registerId: Int, presentation: PresentationResultResponse,id_line: Int): Observable<Any> {
        return Observable.fromCallable {
            savePresentationLocal(presentation,id_line)
            databaseLine.updatePresentation(registerId, presentation.id, presentation.name, id_line)
        }
    }

    fun existCollaboratorInLines(code: String): Observable<Boolean> {
        return Observable.fromCallable {
            return@fromCallable databaseLine.existCollaborator(code)
        }
    }

}

