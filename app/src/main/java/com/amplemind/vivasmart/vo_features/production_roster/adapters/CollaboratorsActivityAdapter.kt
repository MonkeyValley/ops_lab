package com.amplemind.vivasmart.vo_features.production_roster.adapters

import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.CATEGORY_ID
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemCollaboratorBinding
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_core.repository.models.utils.profileImageUrl
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.StatefulAdapter
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ItemCollaboratorViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jakewharton.rxbinding2.view.RxView
import io.realm.Realm

class CollaboratorsActivityAdapter constructor(
        private val mEventBus: EventBus,
        selectAll: Boolean = false
) : StatefulAdapter<CollaboratorModel,
        ItemCollaboratorViewModel,
        CollaboratorsActivityAdapter.CollaboratorActivityViewHolder>() {

    companion object {
        private val TAG = CollaboratorsActivityAdapter::class.java.simpleName
    }

    var listCollaborator = ArrayList<String>()

    private var mTotalSelected: Int = 0

    var scroll = false
    var turnOnActivity = false

    var selectAll: Boolean = selectAll
        set(value) {
            field = value

            mTotalSelected = if (value) {
                itemCount
            }
            else {
                0
            }

            clearViewModels()
        }

    override fun getItemId(position: Int): Long = items[position].uuid.hashCode().toLong()

    override fun buildViewModel(item: CollaboratorModel): ItemCollaboratorViewModel =
            ItemCollaboratorViewModel(item)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollaboratorActivityViewHolder {
        val binding = ItemCollaboratorBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CollaboratorActivityViewHolder(binding)
    }


    override fun setItems(changes: Changes<CollaboratorModel>?) {
        if (selectAll && changes != null && items.isEmpty()) {
            mTotalSelected = changes.data.size

            if(mTotalSelected > 0) {
                for(data in changes.data)
                    listCollaborator.add(data.uuid)
            }

            mEventBus.send(OnSelectionChangedEvent(this, mTotalSelected))
        }

        super.setItems(changes)
    }

    override fun getItemKey(item: CollaboratorModel): String = item.uuid

    override fun setDefaultValues(viewModel: ItemCollaboratorViewModel) {
        super.setDefaultValues(viewModel)

        viewModel.isSelected = selectAll
    }

    fun getSelected(): List<String> = listCollaborator.distinct()

    inner class CollaboratorActivityViewHolder(private val binding: ItemCollaboratorBinding) : StatefulAdapter.ViewHolder<ItemCollaboratorViewModel>(binding.root) {

        private var subscriptions = AndroidDisposable()

        // If mSelectAll is true, there is no need to save states
        // otherwise, when mSelectAll is false we save the state of view models
        // when its isSelected is true
        override fun shouldSave(): Boolean = !selectAll && viewModel.isSelected

        override fun bind(viewModel: ItemCollaboratorViewModel) {

            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()

            showSelection()

            if(turnOnActivity) if(viewModel.item.activityId != null) if(!scroll) setCategoryName(binding, viewModel.item.activityId)

            RxView.clicks(binding.icon)
                    .subscribe{
                        toggleSelection()
                        mEventBus.send (
                                OnItemSelectionChangedEvent(
                                        this@CollaboratorsActivityAdapter,
                                        viewModel.item,
                                        adapterPosition,
                                        viewModel.isSelected,
                                        mTotalSelected
                                )
                        )
                    }.addTo(subscriptions)

            Glide.with(binding.root.context)
                    .load(Uri.parse(viewModel.item.profileImageUrl))
                    .apply(RequestOptions()
                            .placeholder(R.drawable.ic_user))
                    .into(binding.imageProfile)

        }

        private fun toggleSelection() {
            viewModel.isSelected = !viewModel.isSelected

            if (viewModel.isSelected) {
                mTotalSelected ++

                listCollaborator.add(viewModel.item.uuid)
                Log.e("------>", listCollaborator.toString())
            }
            else if (mTotalSelected > 0) {
                mTotalSelected --
                for(i in 0..listCollaborator.size){
                    if(viewModel.item.uuid == listCollaborator.get(i)) {
                        //listCollaborator.drop(i)
                        listCollaborator.removeAt(i)
                        Log.e("------>", listCollaborator.toString())

                        break
                    }
                }
            }

            checkIfShouldSaveState()
            showSelection()
        }

        private fun showSelection() {
            binding.selected.visibility = if (viewModel.isSelected) View.VISIBLE else View.GONE
        }

        override fun cleanUp() {
            subscriptions.dispose()
        }

    }

    private fun setCategoryName(binding: ItemCollaboratorBinding, activityId: Int?) {
        if (CATEGORY_ID != activityId) {
            CATEGORY_ID = activityId!!
            Realm.getDefaultInstance().use {
                val activityName = it
                        .where(ActivityModel::class.java)
                        .equalTo("id", activityId)
                        .findFirst()?.name ?: ""

                binding.linearLayout0.visibility = View.VISIBLE
                binding.tvActivity.text = activityName
            }
        }
    }

    data class OnItemSelectionChangedEvent(
            val adapter: CollaboratorsActivityAdapter,
            val collaborator: CollaboratorModel,
            val position: Int,
            val isSelected: Boolean,
            val totalSelected: Int
    )

    data class OnSelectionChangedEvent (
            val adapter: CollaboratorsActivityAdapter,
            val totalSelected: Int
    )
}