package com.amplemind.vivasmart.features.production_roster.dialogs

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import androidx.core.content.res.ResourcesCompat
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import com.amplemind.vivasmart.R

class CollaboratorsActionsDialog {

    companion object {
        fun showAlertAction(context: Context, title: String, message: String, acceptAction: () -> Unit, cancelAction: () -> Unit?) {
            val ssBuilder = SpannableStringBuilder(title)
            ssBuilder.setSpan(ForegroundColorSpan(ResourcesCompat.getColor(context.resources, R.color.textBlue, null))
                    , 0, title.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            AlertDialog.Builder(context)
                    .setTitle(ssBuilder)
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(context.getString(R.string.accept), object : DialogInterface.OnClickListener {

                        lateinit var acceptAction: () -> Unit

                        override fun onClick(dialog: DialogInterface?, p1: Int) {
                            acceptAction()
                            dialog!!.dismiss()
                        }

                        fun setAcceptAction(acceptAction: () -> Unit): DialogInterface.OnClickListener {
                            this.acceptAction = acceptAction
                            return this
                        }

                    }.setAcceptAction(acceptAction))
                    .setNegativeButton(context.getString(R.string.cancel), object : DialogInterface.OnClickListener {

                        lateinit var cancelAction: () -> Unit?

                        override fun onClick(dialog: DialogInterface?, p1: Int) {
                            cancelAction()
                            dialog!!.dismiss()
                        }

                        fun setCancelAction(cancelAction: () -> Unit?): DialogInterface.OnClickListener {
                            this.cancelAction = cancelAction
                            return this
                        }
                    }.setCancelAction(cancelAction)).show()
        }
    }

}