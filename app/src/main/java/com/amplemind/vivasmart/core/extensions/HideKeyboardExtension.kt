package com.amplemind.vivasmart.core.extensions

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager


/**
 * Created by
 *      amplemind on 7/4/18.
 */

fun hideKeyboard(context: Context) {
    val view = (context as AppCompatActivity).currentFocus
    view?.postDelayed({
        val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    },200)
}

fun Context.hideSoftKeyboard() =
        hideKeyboard(this)


fun forceFocus(editText: View){
    editText.requestFocus()
    val imm = editText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
}