package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.model.IssueModel

class ItemReportQualityReviewDetailViewModel(issue : IssueModel, frequency_no_init: String, percent_init: String) {
    val id = issue.id
    val name = issue.name
    val image = issue.image
    val description = issue.description
    val activityId = issue.activityId
    val category = issue.category
    val subCategory = issue.subCategory
    val cropId = issue.cropId
    var frequency_no = frequency_no_init
    var percent = percent_init
}


