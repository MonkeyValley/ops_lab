package com.amplemind.vivasmart.features.sync_forced

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.main.MainActivity
import com.amplemind.vivasmart.features.sync_forced.viewModel.SyncForcedViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import kotlinx.android.synthetic.main.activity_sync_forced.*
import javax.inject.Inject

class SyncForcedActivity : BaseActivity() {

    companion object {

        fun startForForcedDownload(context: Context) {
            val intent = Intent(context, SyncForcedActivity::class.java).apply {
                //putExtra(EXTRA_FIRST_SYNC, false)
                //putExtra(EXTRA_SYNC, "syncForce")
            }
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var forcedViewModel: SyncForcedViewModel

    lateinit var preferences: UserAppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sync_forced)

        preferences = UserAppPreferences(this, null, null)
        preferences.syncStatus = "NOT_READY"
        forcedViewModel.activity = this
        callsubscriptions()

        if (forcedViewModel.moduleHasAccess(1)
                || forcedViewModel.moduleHasAccess(2)
                || forcedViewModel.moduleHasAccess(3)
                || forcedViewModel.moduleHasAccess(26)
                || forcedViewModel.moduleHasAccess(27)
                || forcedViewModel.moduleHasAccess(28)
        ) {
            forcedViewModel.getColaboratorForced()
        }
        if (forcedViewModel.moduleHasAccess(17)
                || forcedViewModel.moduleHasAccess(18)
                || forcedViewModel.moduleHasAccess(19)
        ) {
            forcedViewModel.getFertirriegoForced()
        }
        if (forcedViewModel.moduleHasAccess(25)) {
            forcedViewModel.getProductionRangesForced()
        }
        if (forcedViewModel.moduleHasAccess(1)
                || forcedViewModel.moduleHasAccess(2)
                || forcedViewModel.moduleHasAccess(3)
                || forcedViewModel.moduleHasAccess(26)
                || forcedViewModel.moduleHasAccess(27)
                || forcedViewModel.moduleHasAccess(28)
        ) {
            forcedViewModel.getActivitiesForced()
        }
        if (forcedViewModel.moduleHasAccess(1)
                || forcedViewModel.moduleHasAccess(2)
                || forcedViewModel.moduleHasAccess(3)
                || forcedViewModel.moduleHasAccess(26)
                || forcedViewModel.moduleHasAccess(27)
                || forcedViewModel.moduleHasAccess(28)
        ) {
            forcedViewModel.validateStageDownload = true
            forcedViewModel.getStageForced()
        }

        if (!forcedViewModel.validateStageDownload) {
            if (forcedViewModel.moduleHasAccess(4)
                    || forcedViewModel.moduleHasAccess(5)
                    || forcedViewModel.moduleHasAccess(13)
                    || forcedViewModel.moduleHasAccess(14)
                    || forcedViewModel.moduleHasAccess(15)
                    || forcedViewModel.moduleHasAccess(16)
                    || forcedViewModel.moduleHasAccess(17)
                    || forcedViewModel.moduleHasAccess(18)
                    || forcedViewModel.moduleHasAccess(19)
                    || forcedViewModel.moduleHasAccess(20)
                    || forcedViewModel.moduleHasAccess(21)
                    || forcedViewModel.moduleHasAccess(22)
            ) {
                forcedViewModel.getStageForced()
            } else {
                forcedViewModel.validateStageDownload = false
            }
        }

        if (!forcedViewModel.validateStageDownload) {
            if (forcedViewModel.moduleHasAccess(25)){
                forcedViewModel.getStageForProductionRangeForced()
            }
        }

        if (forcedViewModel.moduleHasAccess(22)){
            forcedViewModel.deletePollination()
            forcedViewModel.getLots().forEach {
                forcedViewModel.getPollinationForced(it.id)
            }
        }

        forcedViewModel.getPlagueForced()
        forcedViewModel.getDailyPay()
    }

    private fun callsubscriptions() {
        forcedViewModel.onSuccessCollaboratorRequest().subscribe(this::uploadCollaborator).addTo(subscriptions)
        forcedViewModel.onSuccessFertirriegoRequest().subscribe(this::uploadFertirriego).addTo(subscriptions)
        forcedViewModel.onSuccessProductionRangesRequest().subscribe(this::uploadProductionRanges).addTo(subscriptions)
        forcedViewModel.onSuccessActivituesRequest().subscribe(this::uploadActivity).addTo(subscriptions)
        forcedViewModel.onSuccessSatgeRequest().subscribe(this::uploadStage).addTo(subscriptions)
        forcedViewModel.onSuccessPlagueRequest().subscribe(this::uploadPlague).addTo(subscriptions)
    }

    private fun uploadCollaborator(list: List<CollaboratorModel>) {
        forcedViewModel.uploadCollaborator(list)
    }

    private fun uploadFertirriego(list: List<LotFertirriegoModel>) {
        forcedViewModel.uploadFertirriego(list)
    }

    private fun uploadProductionRanges(list: List<ProductionRangeVariatiesModel>) {
        forcedViewModel.uploadProductionRanges(list)
    }

    private fun uploadActivity(list: List<ActivityModel>) {
        forcedViewModel.uploadActivity(list)
    }

    private fun uploadStage(list: List<StageModel>) {
        forcedViewModel.uploadStage(list)
    }

    private fun uploadPlague(list: List<PlagueDetail>) {
        forcedViewModel.uploadPlague(list)
    }

    fun finishSyncForced() {
        startMainActivity()
    }

    fun setStatus(msj: String) {
        runOnUiThread { tv_status.text = msj }
    }

    private fun startMainActivity() {
        Handler().postDelayed(
                {
                    setStatus(getString(R.string.sync_activity_finished))
                    preferences.syncStatus = "COMPLETE"
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                },
                5000
        )
    }

}
