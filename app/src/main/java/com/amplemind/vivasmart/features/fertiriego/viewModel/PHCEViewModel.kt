package com.amplemind.vivasmart.features.fertiriego.viewModel

import android.content.Context
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_DATE
import com.amplemind.vivasmart.features.fertiriego.fragment.PHCEFragment
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService
import com.amplemind.vivasmart.vo_core.repository.models.realm.PHCEForTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject

class PHCEViewModel @Inject constructor(
        private val service: FertirriegoService) : BaseViewModel() {

    var table: String = ""
    var lotId: Int = 0
    lateinit var fragment: PHCEFragment

    fun setServiceContect(context: Context){
        service.context = context
        service.preferences  = UserAppPreferences(context, null, null)
    }

    private val requestSuccessPHCEDataPerDay = BehaviorSubject.create<List<PHCEForTable>>()


    fun getPHCEDataForTable(): Disposable {
        return service.getPHCETableDataPerDay(lotId, FERTIRRIEGO_DATE, table)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccessPHCEDataPerDay.onNext(it)
                }, {
                    fragment.setDataToZero()
                })
    }
    fun onSuccesPHCEDataPerDay(): BehaviorSubject<List<PHCEForTable>> = requestSuccessPHCEDataPerDay

    fun validateDate(): Boolean {
        val calendar = Calendar.getInstance()

        var month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        var day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val todayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        calendar.add(Calendar.DATE, -1)
        month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val yesterdayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        return FERTIRRIEGO_DATE == todayDate || FERTIRRIEGO_DATE == yesterdayDate
    }

    fun saveValveData(etPH: String, etCE: String, type: String){
        service.savePHCEData(lotId, etPH, etCE, table, FERTIRRIEGO_DATE, type)
    }

}