package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.ControlQualityDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.AccountApi
import com.amplemind.vivasmart.core.repository.remote.CleanReportTotalApi
import com.amplemind.vivasmart.core.repository.request.IssueRequest
import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderRequest
import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderSyncRequest
import com.amplemind.vivasmart.core.repository.request.QualityCarryOrderSyncResponse
import com.amplemind.vivasmart.core.repository.response.IssuesResponse
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.google.gson.Gson
import io.reactivex.Observable
import org.jetbrains.anko.doAsync
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/13/18.
 */
open class CleanReportTotalRepository @Inject constructor(private val api: CleanReportTotalApi,
                                                          private val accountApi: AccountApi,
                                                          private val authorization: ApiAuthorization,
                                                          private val preferences: UserAppPreferences,
                                                          private val database: ControlQualityDao) {

    fun getIssues(cropId: Int): Observable<IssuesResponse> {
        return api.getIssues(authorization.getAuthToken(preferences.token), cropId = cropId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalListOfIssues(cropId: Int): Observable<IssuesResponse> {
        return Observable.fromCallable {
            IssuesResponse(database.getIssuesByCrop(cropId))
        }
    }

    fun saveLocalListOfIssues(cropId: Int, issues: List<IssueModel>) {
        doAsync {
            issues.map {
                it.cropId = cropId
            }
            database.insertIssues(issues)
        }
    }

    private fun onlinePhotos(request: QualityCarryOrderRequest, photos: List<String>?) {
        if (photos != null) {
            for (image in photos) {
                if (request.image_one == null) {
                    request.image_one = image
                    continue
                }
                if (request.image_two == null) {
                    request.image_two = image
                    continue
                }
                if (request.image_three == null) {
                    request.image_three = image
                    continue
                }
                if (request.image_four == null) {
                    request.image_four = image
                    continue
                }
            }
        }
    }

    private fun offlinePhotos(request: QualityCarryOrderRequest, photosOffline: List<File>?) {
        if (photosOffline != null) {
            for (image in photosOffline) {
                if (request.image_one == null) {
                    request.image_one = image.absolutePath
                    continue
                }
                if (request.image_two == null) {
                    request.image_two = image.absolutePath
                    continue
                }
                if (request.image_three == null) {
                    request.image_three = image.absolutePath
                    continue
                }
                if (request.image_four == null) {
                    request.image_four = image.absolutePath
                    continue
                }
            }
        }
    }

    fun createIssues(issues: List<IssueRequest>, carryOrderId: Int, sampleSize: Int, unitId: Int,
                     export: Int, permanentDefect: Int, defectCondition: Int, isOnline: Boolean, photos: List<String>? = null, photosOffline: List<File>? = null): Observable<QualityCarryOrderResponse> {

        val user = Gson().fromJson(preferences.userInfo, UserModel::class.java)

        val request = QualityCarryOrderRequest(carryOrderId, sampleSize, unitId, user.id, issues, export, permanentDefect, defectCondition)

        if (isOnline) {
            onlinePhotos(request, photos)
        } else {
            offlinePhotos(request, photosOffline)
        }

        return if (isOnline) {
            api.createIssues(authorization.getAuthToken(preferences.token), request)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        } else {
            Observable.fromCallable {
                database.insertQualityCarryOrderRequest(request)
                val list = arrayListOf<IssueRequest>()
                request.issues.forEach {
                    list.add(it)
                }
                QualityCarryOrderResponse(request.carryOrderId, request.sampleSize, request.unitId, request.userId, list,
                        request.export, request.permanentDefect, request.defectCondition)
            }
        }
    }

    fun syncQualityCarryOrder(request: QualityCarryOrderSyncRequest): Observable<QualityCarryOrderSyncResponse> {
        return api.syncQualityCarryOrder(authorization.getAuthToken(preferences.token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun saveAgreedTutorial(): Observable<UserModel> {
        return accountApi.setAgreedTutorial(authorization.getAuthToken(preferences.token), AccountRepository.UpdateAgreedTutorial(false), getIdCollaborator())
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun isAgreedTutorial(): Observable<Boolean> {
        return Observable.just(Gson().fromJson(preferences.userInfo, UserModel::class.java).tutorial)
    }

    private fun getIdCollaborator(): Int {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java).id
    }

    fun saveUser(user: String) {
        preferences.userInfo = user
    }

}