package com.amplemind.vivasmart.core.repository.request

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ReviewIssueRequest(@SerializedName("bulk") val bulk: List<ListIssueRequest>,
                              @Embedded @SerializedName("groove") val groove: GrooveIssueRequest,
                              val collaboratorId: Int,
                              @PrimaryKey val id: Long? = null,
                              val deleted: Boolean = false)

data class ListIssueRequest(@SerializedName("issue_id") val issueId: Int,
                            @SerializedName("is_ok") val isOk: Boolean)

@Entity
data class GrooveIssueRequest(@SerializedName("quality_comment") val comment: String,
                              @SerializedName("activity_log_groove_id") @PrimaryKey val grooveId: Int)