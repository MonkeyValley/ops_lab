package com.amplemind.vivasmart.features.packaging_quality.clean_report

import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.model.TinderCardModelTemp
import javax.inject.Inject

class ItemCleanViewModel @Inject constructor(item: IssueModel) {

    //Condition is subCategory 8
    //Permanent is subCategory 9

    val id = item.id
    val name = item.name
    val isPermanent = item.subCategory == 9
    val url = "${BuildConfig.SERVER_URL}uploads/${item.image}"

}
