package com.amplemind.vivasmart.features.scannerbarcode

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.HeaderModel
import com.amplemind.vivasmart.core.utils.COLLABORATORS
import com.amplemind.vivasmart.features.collaborators.fragment.CollaboratorsDataBaseFragment
import com.amplemind.vivasmart.features.scannerbarcode.dialog.ScanFragmentDialog
import com.amplemind.vivasmart.features.scannerbarcode.viewModel.ScanListViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import kotlinx.android.synthetic.main.content_scan_list_activity.*
import javax.inject.Inject

@SuppressLint("Registered")
open class ScanListActivity : BaseActivityWithFragment() {

    companion object {

        val TAG = ScanListActivity::class.simpleName

        val PARAM_ACTIVITY_CODE = "$TAG.ActivityCode"
    }

    @Inject
    lateinit var viewModel: ScanListViewModel

    @Inject
    lateinit var prefer: UserAppPreferences

    lateinit var mActivityCode: ActivityCodeModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_scan_list_activity)

        //mActivityCode = intent.getParcelableExtra(PARAM_ACTIVITY_CODE)

        initToolbar()

        camera.setOnClickListener{
            finish()
        }

        if (getSection() != null || isProductionFlow()) {
            continueScan()
        }

        this.supportFragmentManager
                .beginTransaction()
                .replace(R.id.collaborators_frame,
                        CollaboratorsDataBaseFragment.newInstance(mActivityCode, showActivities(), models = intent.getParcelableArrayListExtra(COLLABORATORS), section = getSection()))
                .commit()
    }

    private fun isProductionFlow(): Boolean {
        return prefer.flow == "Producción"
    }

    private fun getSection(): HeaderModel? {
        return intent.getParcelableExtra("section")
    }

    fun showActivities(): Boolean {
        return intent.getBooleanExtra("showActivities", false)
    }

    private fun continueScan() {
        val dialog = ScanFragmentDialog().newInstance()
        //dialog.show(supportFragmentManager, AddProblemDialogFragment.TAG)
        dialog.isCancelable = false

        dialog.setListener(object : ScanFragmentDialog.OnScanListEvents {
            override fun onContinueScan() {
                finish()
            }

            override fun onStopScan() {
                dialog.dismiss()
            }

        })
    }

    override fun onBackPressed() {
        setResult(200)
        super.onBackPressed()
    }

    private fun initToolbar() {
        val mToolbar = toolbar_scan_list
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Agregar colaboradores"
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                setResult(200)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200) {
            finish()
        }
    }

}
