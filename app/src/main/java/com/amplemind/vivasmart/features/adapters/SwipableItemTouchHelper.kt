package com.amplemind.vivasmart.features.adapters

import android.graphics.Canvas
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.View


class SwipableItemTouchHelper (swipeDirs: Int, private val listener: SwipableItemTouchHelper.Listener) : ItemTouchHelper.SimpleCallback(0, swipeDirs) {

    companion object {
        const val DELETABLE = ItemTouchHelper.LEFT
        const val FINISHABLE = ItemTouchHelper.RIGHT
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return true
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (viewHolder is SwipableViewHolder) {
            getDefaultUIUtil().onSelected(viewHolder.viewForeground)
        }
    }

    override fun onChildDrawOver(c: Canvas, recyclerView: RecyclerView,
                                 viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float,
                                 actionState: Int, isCurrentlyActive: Boolean) {
        if (viewHolder is SwipableViewHolder) {
            getDefaultUIUtil().onDrawOver(c, recyclerView, viewHolder.viewForeground, dX, dY,
                    actionState, isCurrentlyActive)
        }
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        if (viewHolder is SwipableViewHolder){
            getDefaultUIUtil().clearView(viewHolder.viewForeground)
        }
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView,
                             viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float,
                             actionState: Int, isCurrentlyActive: Boolean) {
        if (viewHolder is SwipableViewHolder) {
            if (dX > 0) {
                viewHolder.viewDelete?.visibility = View.GONE
                viewHolder.viewFinish?.visibility = View.VISIBLE
            } else {
                viewHolder.viewDelete?.visibility = View.VISIBLE
                viewHolder.viewFinish?.visibility = View.GONE
            }
            if (viewHolder.viewForeground != null) {
                getDefaultUIUtil().onDraw(c, recyclerView, viewHolder.viewForeground, dX, dY,
                        actionState, isCurrentlyActive)
            }
        }
    }


    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        listener.onSwiped(viewHolder, direction, viewHolder.adapterPosition)
    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    interface Listener {
        fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int)
    }
}
