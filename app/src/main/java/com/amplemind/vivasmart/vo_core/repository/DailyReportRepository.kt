package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.StagesDao
import javax.inject.Inject

class DailyReportRepository @Inject constructor(
        private val activityLogsDao: ActivityLogsDao,
        private val stagesDao: StagesDao
) : Repository() {

    override fun cleanUp() {
        activityLogsDao.cleanUp()
        stagesDao.cleanUp()
    }

    fun loadStage(stageUuid: String) =
            stagesDao.findByUuid(stageUuid)

    fun loadDailyReport(stageUuid: String, date: String, mode: Int) =
            activityLogsDao.retrieveDailyReportForDate(stageUuid, date, mode)

}