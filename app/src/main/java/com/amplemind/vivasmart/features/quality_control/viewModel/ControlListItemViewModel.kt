package com.amplemind.vivasmart.features.quality_control.viewModel

import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.core.repository.model.ControlQualityListModel

class ControlListItemViewModel(item: ControlQualityListModel) {

    enum class ControlListItemStatus(val value: Int) {
        PENDING(0),
        DONE(1),
        NONE(2);

        companion object {
            fun from(value: Int): ControlListItemStatus = ControlListItemStatus.values().first { it.value == value }
        }
    }

    private var controlStatus: ControlListItemStatus

    val name = item.name
    val total = if (item.completed) "${item.signed}/${item.total}" else "${item.total - item.noSigned}/${item.total}"
    val code = item.employeeCode
    val score = item.score
    val id_collaborator = item.collaboratorId

    val employee_code = item.employeeCode

    val complete = item.completed

    val showStatus = ObservableInt(View.VISIBLE)

    init {
        showStatus.set(if (item.completed) View.GONE else View.VISIBLE)
        if (item.isDone == null) {
            controlStatus = ControlListItemStatus.NONE
        } else  if(item.isDone!!) {
            controlStatus = ControlListItemStatus.DONE
        } else {
            controlStatus = ControlListItemStatus.PENDING
        }
    }

    fun getStatus(): Int {
        return controlStatus.value
    }

}
