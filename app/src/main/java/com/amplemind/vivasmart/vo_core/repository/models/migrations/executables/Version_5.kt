package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_5 : MigrationExecutable {
    override fun run(realm: DynamicRealm) {
        alterVariety(realm)
    }

    override fun drop(realm: DynamicRealm) {
        revertVariety(realm)
    }

    private fun alterVariety(realm: DynamicRealm) {
        realm.schema.get("VarietyModel")
                ?.setRequired("graftName", false)
                ?.setRequired("supplier", false)
    }

    private fun revertVariety(realm: DynamicRealm) {
        realm.schema.get("VarietyModel")
                ?.setRequired("graftName", true)
                ?.setRequired("supplier", true)
    }

}