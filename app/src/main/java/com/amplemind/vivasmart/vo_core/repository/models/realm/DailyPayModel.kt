package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class DailyPayModel(
        @SerializedName("collaborator_employee_code")        var collaboratorEmployeeCode: String? = "",
        @SerializedName("collaborator_id")                   var collaboratorId: Int? = 0,
        @SerializedName("collaborator_uuid")                 var collaboratorUuid: String? = "",
        @SerializedName("dailypay_uuid")                     var dailypayUuid: String? = "",
        @SerializedName("dailypay_version")                  var dailypayVersion: Long? = 0,
        @SerializedName("date")                              var date: String? = ""
) : RealmObject()