package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_7 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        alterActivityCodeModel(realm)
        alterActivityModel(realm)
        alterCropModel(realm)
    }

    override fun drop(realm: DynamicRealm) {
        revertActivityCodeModel(realm)
        revertActivityModel(realm)
        revertCropModel(realm)
    }

    private fun alterActivityCodeModel(realm: DynamicRealm) {
        realm.schema.get("ActivityCodeModel")
                ?.addIndex("id")
                ?.addIndex("activityId")
                ?.addIndex("activityUuid")
                ?.addIndex("stageId")
                ?.addIndex("stageUuid")
                ?.addIndex("remaining")
                ?.addIndex("code")
    }

    private fun alterActivityModel(realm: DynamicRealm) {
        realm.schema.get("ActivityModel")
                ?.addIndex("id")
                ?.addIndex("businessUnitId")
                ?.addIndex("name")
    }

    private fun alterCropModel(realm: DynamicRealm) {
        realm.schema.get("CropModel")
                ?.addIndex("activityId")
                ?.addIndex("cropId")
    }

    private fun revertActivityCodeModel(realm: DynamicRealm) {
        realm.schema.get("ActivityCodeModel")
                ?.removeIndex("id")
                ?.removeIndex("activityId")
                ?.removeIndex("activityUuid")
                ?.removeIndex("stageId")
                ?.removeIndex("stageUuid")
                ?.removeIndex("remaining")
                ?.removeIndex("code")
    }

    private fun revertActivityModel(realm: DynamicRealm) {
        realm.schema.get("ActivityModel")
                ?.removeIndex("id")
                ?.removeIndex("business_unit_id")
                ?.removeIndex("name")
    }

    private fun revertCropModel(realm: DynamicRealm) {
        realm.schema.get("CropModel")
                ?.removeIndex("activityId")
                ?.removeIndex("cropId")
    }

}