package com.amplemind.vivasmart.features.phenology.adapters

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.toDp
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.databinding.VarietiesPhenologyItemBinding
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyVarietiesFragment
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVars
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesPhenologyItemViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesTablesPhenologyItemViewModel
import androidx.databinding.library.baseAdapters.BR

class VarietiesPhenologyAdapter constructor(val fragment : PhenologyVarietiesFragment, val list: MutableList<VarietiesPhenologyItemViewModel>, val phenology_vars: MutableList<PhenologyVars>) : RecyclerView.Adapter<VarietiesPhenologyAdapter.VarietiesPhenologyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VarietiesPhenologyViewHolder {
        val binding = VarietiesPhenologyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VarietiesPhenologyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: VarietiesPhenologyViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun isEmpty() = list.isEmpty()

    inner class VarietiesPhenologyViewHolder(private val binding: VarietiesPhenologyItemBinding ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: VarietiesPhenologyItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.RVPhenologyVarietiesTable.layoutManager = LinearLayoutManager(fragment.context, LinearLayoutManager.VERTICAL, false)
                binding.RVPhenologyVarietiesTable.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

                val tables = item.tables
                val listaVar = ArrayList<VarietiesTablesPhenologyItemViewModel>()
                if (tables != null) {
                    for (item in tables){
                        val obj = VarietiesTablesPhenologyItemViewModel(item)
                        listaVar.add(obj)
                    }
                }

                val mAdapter = VarietiesTablesPhenologyAdapter(item, fragment, listaVar, phenology_vars )
                binding.RVPhenologyVarietiesTable.adapter = mAdapter

            }
        }
    }
}
