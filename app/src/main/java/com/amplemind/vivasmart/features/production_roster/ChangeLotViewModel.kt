package com.amplemind.vivasmart.features.production_roster

import com.amplemind.vivasmart.core.repository.ChangeLotRepository
import com.amplemind.vivasmart.core.repository.model.ChangeLotModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ChangeLotViewModel @Inject constructor(private var repository : ChangeLotRepository){

    private var listLotsSubject = BehaviorSubject.create<List<ItemChangeLotViewModel>>()

    fun loadData() : Disposable {
        return repository.getLots()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelToViewModels(it) }
                .doOnNext {
                    listLotsSubject.onNext(it)
                }
                .subscribe()
    }

    private fun mapModelToViewModels(it: List<ChangeLotModel>): List<ItemChangeLotViewModel> {
        val list = arrayListOf<ItemChangeLotViewModel>()
        for (item in it){
            list.add(ItemChangeLotViewModel(item))
        }
        return list
    }

    fun getLots(): BehaviorSubject<List<ItemChangeLotViewModel>> {
        return listLotsSubject
    }

}
