package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.PayrollActivitiesModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.features.menus.*
import com.google.gson.Gson
import io.reactivex.Observable
import javax.inject.Inject

class OperationRepository @Inject constructor( private val authorization: ApiAuthorization, private val preferences: UserAppPreferences) {
    val userInfo: UserModel = Gson().fromJson(preferences.userInfo, UserModel::class.java)
    var businessUnit = userInfo.businessUnitId

    fun getOperationActivitiesMenu(): Observable<List<PayrollActivitiesModel>> {
        return Observable.just(getActivitiesOperation())
    }

}