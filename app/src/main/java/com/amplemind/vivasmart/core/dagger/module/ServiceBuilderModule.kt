package com.amplemind.vivasmart.core.dagger.module

import com.amplemind.vivasmart.core.platform.SynchronizeDataBaseService
import com.amplemind.vivasmart.vo_features.services.firebase.NotificationService
import com.amplemind.vivasmart.vo_features.services.sync.SyncService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuilderModule {

    @ContributesAndroidInjector
    internal abstract fun provideSynchronizeDataBaseService(): SynchronizeDataBaseService

    @ContributesAndroidInjector
    internal abstract fun provideSyncService(): SyncService

    @ContributesAndroidInjector
    internal abstract fun provideNotificationService(): NotificationService
}