package com.amplemind.vivasmart.vo_core.repository.remote

import com.amplemind.vivasmart.vo_core.repository.responses.MessageResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface MessageApi {
    @GET("/v2/message")
    @Headers("Content-Type: application/json")
    fun getMessages(
            @Header("Authorization")        authentication: String,
            @Query("version__gte")          version: Long? = null
    ): Call<MessageResponse>

    @GET("/v2/message")
    @Headers("Content-Type: application/json")
    fun getMessagesByUserID(
            @Header("Authorization")        authentication: String,
            @Query("embed")                 embed: String = "user_id",
            @Query("user_id")               userId: Int = 0,
            @Query("version__gte")          version: Long? = null
    ): Call<MessageResponse>
}