package com.amplemind.vivasmart.vo_features.production_roster.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_features.adapters.LiveFragmentPagerAdapter
import com.amplemind.vivasmart.vo_features.production_roster.TimerFragment

class TimerFragmentViewPagerAdapter(fragmentManager: FragmentManager) : LiveFragmentPagerAdapter<ActivityCodeModel>(fragmentManager) {

    override fun buildFragment(position: Int): Fragment = TimerFragment.newInstance(get(position).uuid)

    override fun getFragmentTag(position: Int): String = get(position).uuid

    override fun getPageTitle(position: Int): CharSequence? = get(position).code.toString()

    override fun onFragmentInsert(position: Int) {
        super.onFragmentInsert(position)
        currentPosition = position
    }

}