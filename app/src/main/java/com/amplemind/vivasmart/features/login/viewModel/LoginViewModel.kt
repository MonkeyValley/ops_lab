package com.amplemind.vivasmart.features.login.viewModel

import android.util.Log
import com.amplemind.vivasmart.core.extensions.isValidEmail
import com.amplemind.vivasmart.core.extensions.isValidPassword
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.model.LoginModel
import com.amplemind.vivasmart.features.login.LoginActivity.Companion.LOGIN_ERROR_MESSAGE
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UserRolModel
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import javax.inject.Inject

open class LoginViewModel @Inject constructor(private val repository: AccountRepository) {

    private val validation: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val onSucess: BehaviorSubject<LoginModel.LoginResponse> = BehaviorSubject.create()
    private val onSucessGetUser: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val onFail: BehaviorSubject<String> = BehaviorSubject.create()
    private lateinit var token: String

    val selectedUrl: String
        get() = repository.getServerUrl()

    var serverUrl: String
        get() = repository.getServerUrl()
        set(value) = repository.saveServerUrl(value)

    var customServerUrl: String
        get() = repository.getCustomServerUrl()
        set(value) = repository.saveCustomServerUrl(value)

    var serverUrlIsPreset: Boolean
        get() = repository.getServerUrlIsPreset()
        set(value) = repository.saveServerUrlIsPreset(value)


    fun validateAccessLoginTextFields(user: String, password: String) {
        validation.onNext(user.isValidEmail() && password.isValidPassword())
    }

    fun getUser(): Disposable {
        return repository.getUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    repository.saveUser(Gson().toJson(it))
                    onSucessGetUser.onNext(true)
                }, {
                    onFail.onNext(LOGIN_ERROR_MESSAGE)
                })
    }

    fun getUserRol(): Disposable {
        return repository.getUserRol()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    Log.e("Rol", Gson().toJson(it))
                    saveRol(it)
                }, {

                })
    }

    private fun saveRol(rolModel: UserRolModel) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                realm.insertOrUpdate(rolModel)
            }
        }
    }

    fun doLogin(request: AccountRepository.LoginRequest): Disposable {
        return repository.doLogin(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    onSucess.onNext(it)
                }, {
                    progressStatus.onNext(false)
                    onFail.onNext(LOGIN_ERROR_MESSAGE)
                })
    }

    fun observeValidationChanges(): BehaviorSubject<Boolean> {
        return validation
    }

    fun observeProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun observeSuccessLogin(): BehaviorSubject<LoginModel.LoginResponse> {
        return onSucess
    }

    fun observeFailLogin(): BehaviorSubject<String> {
        return onFail
    }

    fun observeSuccessGetUser(): BehaviorSubject<Boolean> {
        return onSucessGetUser
    }

    fun saveToken(token: String) {
        repository.saveTokenUser(token)
        this.token = token
    }

    fun updateTokenToDataUpload() {
        Log.e("TOKEN", this.token)
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction {

                val dataObjects = realm.where(UploadObjectModel::class.java)
                        .findAll()

                if (dataObjects.size > 0) {
                    for (data in dataObjects) {
                        data.errorCode = ""
                        data.errorServerResponse = ""
                        data.token = this.token
                        realm.insertOrUpdate(data)
                    }
                }
            }
        }
    }


}
