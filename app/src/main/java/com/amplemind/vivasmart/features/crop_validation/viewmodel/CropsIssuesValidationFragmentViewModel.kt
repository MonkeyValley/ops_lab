package com.amplemind.vivasmart.features.crop_validation.viewmodel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.CropValidationRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.crop_validation.models.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class CropsIssuesValidationFragmentViewModel @Inject constructor(private val repository: CropValidationRepository, private val apiErrors: HandleApiErrors) : BaseViewModel() {
    private val list = mutableListOf<ItemCropIssuesViewModel>()
    private val requestSuccess = BehaviorSubject.create<List<ItemCropIssuesViewModel>>()
    private val requestSuccessPost = BehaviorSubject.create<CropValidationPostResponse>()
    private val requestSuccessDetail = BehaviorSubject.create<CropValidationDetailResponse>()

    fun getIssues(crop_id: Int): Disposable {
        return repository.getIssues(crop_id)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelListToViewModel)
                .subscribe({
                    list.clear()
                    list.addAll(it)
                    requestSuccess.onNext(list)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }
    private fun mapModelListToViewModel(result: CropIssuesResponse): List<ItemCropIssuesViewModel> {
        val viewModels = mutableListOf<ItemCropIssuesViewModel>()
        result.data.forEach { obj ->
            viewModels.add(ItemCropIssuesViewModel(obj))
        }
        viewModels.sortBy { it.issueName }
        return viewModels
    }
    fun onSuccessRequest(): BehaviorSubject<List<ItemCropIssuesViewModel>> {
        return requestSuccess
    }

    fun getDetail(carry_order_id: Int): Disposable {
        return repository.getDetail(carry_order_id)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ requestSuccessDetail.onNext(it) }, { requestFail.onNext(apiErrors.handleLoginError(it)) })
    }
    fun onSuccessRequestDetail(): BehaviorSubject<CropValidationDetailResponse> {
        return requestSuccessDetail
    }

    fun sendDataCropValidation(objData : CropValidationPost): Disposable {
        return repository.sendDataCropValidation(objData).subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ requestSuccessPost.onNext(it) }, { requestFail.onNext(apiErrors.handleLoginError(it)) })
    }
    fun onSuccessRequestPost(): BehaviorSubject<CropValidationPostResponse> {
        return requestSuccessPost
    }

}