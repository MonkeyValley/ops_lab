package com.amplemind.vivasmart.features.mipe.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.features.mipe.service.MipeService
import javax.inject.Inject

class MipeGrooveViewModel  @Inject constructor(
        private val service: MipeService
): BaseViewModel() {

    var lotId: Int = 0
    var week: Int = 0
    var table: String = ""
    var lotName: String = ""

    private val grooveListPair = ArrayList<Int>()
    private val grooveListOdd = ArrayList<Int>()

    fun getDataPairGroove(): ArrayList<Int>{
        return onSuccessRequestGroovePair()
    }

    fun onSuccessRequestGroovePair(): ArrayList<Int> {
        grooveListPair.clear()
        for(i in 1..service.getGroove(lotId, table)) {
            if (i % 2 == 0) grooveListPair.add(i)
        }
        return grooveListPair
    }

    fun getDataOddGroove(): ArrayList<Int>{
        return onSuccessRequestGrooveOdd()
    }

    fun onSuccessRequestGrooveOdd(): ArrayList<Int> {
        grooveListOdd.clear()
        for(i in 1..service.getGroove(lotId, table)) {
            if (i % 2 != 0) grooveListOdd.add(i)
        }
        return grooveListOdd
    }

}