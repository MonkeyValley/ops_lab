package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.features.production_range.repository.remote.ProductionRangeApi
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import io.reactivex.Single
import io.realm.RealmObject
import javax.inject.Inject

class MegasseDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mProductionRangeApi: ProductionRangeApi,
        private val mPref: UserAppPreferences
) : RealmDownloader<RealmObject?, MessageModel>(realmHolder, MessageModel::class.java) {


    override fun buildServerRequest(parent: RealmObject?, lastVersion: Long): Single<List<MessageModel>> =
            mProductionRangeApi.getMessagesByUserID(
                    mPref.authorizationToken, userId = mPref.user.id
            ).map { response ->
                Log.e("Data-Message-userId: ", mPref.user.id.toString())
                Log.e("Data-Message: ", response.data.toString())
                response.data
            }
}