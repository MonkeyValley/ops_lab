package com.amplemind.vivasmart.features.production_quality

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.widget.CompoundButton
import android.widget.ToggleButton
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment

class AssingTypeActivity : BaseDialogFragment(), CompoundButton.OnCheckedChangeListener {

    private var toggle_hours: ToggleButton? = null
    private var toggle_piece: ToggleButton? = null

    private lateinit var listener : OnTypeActivity

    companion object {
        val TAG = AssingTypeActivity::class.java.simpleName
        fun newInstance() = AssingTypeActivity()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_type_activity_alert_dialog, null, false)
        builder.setView(mView)

        isCancelable = false

        toggle_hours = mView.findViewById(R.id.btn_hours)
        toggle_piece = mView.findViewById(R.id.btn_piece)

        toggle_hours!!.setOnCheckedChangeListener(this)
        toggle_piece!!.setOnCheckedChangeListener(this)

        builder.setPositiveButton("Listo", null)
        builder.setNegativeButton("Cancelar") { _, _ ->
            listener.onCancel()
        }

        val create = builder.create()
        create.setOnShowListener { _ ->
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if (toggle_hours!!.isChecked){
                            listener.onHours()
                        }
                        if (toggle_piece!!.isChecked){
                            listener.onPiece()
                        }
                    }
        }


        return create
    }

    override fun onCheckedChanged(view : CompoundButton?, isChecked: Boolean) {
        when (view!!.id) {
            R.id.btn_hours -> {
                if (isChecked) {
                    toggle_piece!!.isChecked = false
                }
            }

            R.id.btn_piece -> {
                if (isChecked) {
                    toggle_hours!!.isChecked = false
                }
            }
        }
    }

    fun setTypeActivityListener(onType : OnTypeActivity){
        this.listener = onType
    }


    interface OnTypeActivity{
        fun onHours()
        fun onPiece()
        fun onCancel()
    }

}