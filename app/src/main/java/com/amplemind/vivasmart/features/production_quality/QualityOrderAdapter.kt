package com.amplemind.vivasmart.features.production_quality

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.onChange
import com.amplemind.vivasmart.databinding.ItemContainerPhotosIssuesBinding
import com.amplemind.vivasmart.databinding.ItemIssuesEvaluationBinding
import com.amplemind.vivasmart.databinding.ItemPhotoIssueBinding
import com.amplemind.vivasmart.databinding.ItemQuantityOrderIssuesBinding
import androidx.databinding.library.baseAdapters.BR

class QualityOrderAdapter(val isRead : Boolean = false) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    enum class QualityTypeHolder(val type: Int) {
        ISSUES(0),
        PHOTOS(1),
        EVUALUATION(2),
        COMMENTS(3);

        companion object {
            fun from(value: Int): QualityTypeHolder = QualityTypeHolder.values().first { it.type == value }
        }
    }

    private var issues = mutableListOf<IssuesItemViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder = when (viewType) {
            QualityTypeHolder.ISSUES.type -> {
                val binding = ItemQuantityOrderIssuesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                IssuesViewHolder(binding)
            }
            QualityTypeHolder.PHOTOS.type -> {
                val binding = ItemContainerPhotosIssuesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                IssuesPhotosViewHolder(binding)
            }
            QualityTypeHolder.EVUALUATION.type -> {
                val binding = ItemIssuesEvaluationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                IssuesEvaluationViewHolder(binding)
            }
            else -> {
                null
            }
        }
        return viewHolder!!
    }

    override fun getItemCount() = issues.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val typeHolder = holder.itemViewType
        when (typeHolder) {
            QualityTypeHolder.ISSUES.type -> {
                (holder as IssuesViewHolder).bind(issues[position])
            }
            QualityTypeHolder.EVUALUATION.type -> {
                (holder as IssuesEvaluationViewHolder).bind(issues[position])
            }
            QualityTypeHolder.PHOTOS.type -> {
                (holder as IssuesPhotosViewHolder).bind(issues[position])
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return issues[position].typeHolder
    }

    fun setData(issues: List<IssuesItemViewModel>) {
        this.issues = issues.toMutableList()
        notifyDataSetChanged()
    }

    inner class IssuesViewHolder(private val binding: ItemQuantityOrderIssuesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: IssuesItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
        }
    }

    inner class IssuesPhotosViewHolder(private val binding: ItemContainerPhotosIssuesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: IssuesItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            val container = binding.containerPhotos
            container.removeAllViews()
            item.photosIssues?.forEach {
                val photoBinding = ItemPhotoIssueBinding.inflate(LayoutInflater.from(binding.root.context), container, false)
                photoBinding.setVariable(BR.photo, it)
                photoBinding.executePendingBindings()
                container.addView(photoBinding.root)
            }
        }
    }

    inner class IssuesEvaluationViewHolder(private val binding: ItemIssuesEvaluationBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: IssuesItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()

            if (isRead){
                binding.tvCommentLabel.visibility = if (item.evaluationServer) View.GONE else View.VISIBLE
                binding.commentServer.visibility = View.VISIBLE
                binding.containerEvaluation.visibility = View.VISIBLE
                binding.tvCommentsIssue.visibility = View.GONE
                binding.containerGroup.visibility = View.GONE
                return
            }

            binding.rgButton.setOnCheckedChangeListener { radioGroup, checkedId ->
                if (checkedId == R.id.bad) {
                    item.evaluationResult = false
                    binding.tvComments.visibility = View.VISIBLE
                } else {
                    item.commentsIssue = ""
                    item.evaluationResult = true
                    binding.tvComments.visibility = View.GONE
                }
            }
            binding.tvCommentsIssue.onChange {
                item.commentsIssue = it
            }
        }
    }


}
