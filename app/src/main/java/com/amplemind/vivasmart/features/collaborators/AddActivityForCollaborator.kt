package com.amplemind.vivasmart.features.collaborators

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.SearchView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.*
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import com.amplemind.vivasmart.core.utils.COLLABORATORS
import com.amplemind.vivasmart.core.utils.HAS_BOXCOUNT
import com.amplemind.vivasmart.core.utils.LOCAL_BROADCAST_USER_PACKING_RECEIVED
import com.amplemind.vivasmart.features.collaborators.adapter.AddActivityForCollaboratorAdapter
import com.amplemind.vivasmart.features.collaborators.viewModel.AddActivityForCollaboratorViewModel
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemAddActivitiesViewModel
import com.amplemind.vivasmart.features.production_quality.AssingTypeActivity
import kotlinx.android.synthetic.main.content_add_activity_for_collaborator_activity.*
import javax.inject.Inject

class AddActivityForCollaborator : BaseActivity() {

    @Inject
    lateinit var viewModel: AddActivityForCollaboratorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_add_activity_for_collaborator_activity)

        setupToolbar()

        initRecycler()

        viewModel.onSearchLocalActivities().subscribe(this::onLoadActivitiesLocal).addTo(subscriptions)
        viewModel.getData().subscribe(this::setAdapter).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onAssingCollaboratorSucess().subscribe(this::successAsing).addTo(subscriptions)
        getActivities()

        (rv_add_activities.adapter as AddActivityForCollaboratorAdapter).onClickItem().subscribe(this::selectCollaborator).addTo(subscriptions)
    }

    private fun getActivities() {
        if (true) {
            viewModel.getActivities()?.addTo(subscriptions)
        } else {
            onLoadActivitiesLocal(true)
        }
    }

    private fun onLoadActivitiesLocal(result: Boolean) {
        if (result) {
            main_view_activity.snackWithoutInternetMessage()
            viewModel.getActivitiesLocal().addTo(subscriptions)
        }
    }

    private fun successAsing(result: CollaboratorsInLinesResult) {
        val intent = Intent(LOCAL_BROADCAST_USER_PACKING_RECEIVED)

        intent.putExtra(COLLABORATORS, result.data as java.util.ArrayList<out Parcelable>)
        intent.putExtra(HAS_BOXCOUNT, result.has_boxcount)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        setResult(200)
        finish()
    }

    private fun initRecycler() {
        rv_add_activities.hasFixedSize()
        rv_add_activities.layoutManager = LinearLayoutManager(this)
        rv_add_activities.itemAnimator = DefaultItemAnimator()
        rv_add_activities.adapter = AddActivityForCollaboratorAdapter()
    }

    private fun setAdapter(data: List<ItemAddActivitiesViewModel>) {
        if (data.isEmpty()){
            ll_list_empty_activities.visibility = View.VISIBLE
        }
        (rv_add_activities.adapter as AddActivityForCollaboratorAdapter).addElements(data)
    }

    private fun selectCollaborator(item: ItemAddActivitiesViewModel) {
        if ((item.collaborator_left ?: 0) <= 0) {
            main_view_activity.snack("La actividad esta llena")
            (rv_add_activities.adapter as AddActivityForCollaboratorAdapter).changeStatus(item)
            return
        }
        val dialog = AssingTypeActivity.newInstance()
        dialog.show(supportFragmentManager, AssingTypeActivity.TAG)
        dialog.setTypeActivityListener(object : AssingTypeActivity.OnTypeActivity {
            override fun onHours() {
                //viewModel.assignCollaborator(isOnlineMode(this@AddActivityForCollaborator), getIdsCollaborator(), item.activity_id, item.name, item.collaborator_left).addTo(subscriptions)
                dialog.dismiss()
            }

            override fun onPiece() {
                startActivityForResult(Intent(dialog.context, PresentationForCollaboratorActivity::class.java)
                        .putExtra("name", getName())
                        .putExtra("name_activity", item.name)
                        .putExtra("collaborator_left", item.collaborator_left)
                        //.putParcelableArrayListExtra("list_collaborators", getIdsCollaborator())
                        .putExtra("activity_id", item.activity_id), 200)
                dialog.dismiss()
            }

            override fun onCancel() {
                (rv_add_activities.adapter as AddActivityForCollaboratorAdapter).changeStatus(item)
                dialog.dismiss()
            }
        })
    }

    /**
     *  create toolbar in view
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_add_activity
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Elegir actividad"

        //Set SearView EditText padding to match the title padding
        search_view.findViewById<EditText>(androidx.appcompat.R.id.search_src_text).searchViewPadding()
        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener, androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.searchActivities(query ?: "")
                hideKeyboard(this@AddActivityForCollaborator)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.searchActivities(newText ?: "")
                return true
            }
        })
    }

    //private fun getIdsCollaborator() = intent.getParcelableArrayListExtra<CollaboratorModel>("list_collaborators")

    private fun getName(): String {
        return intent.getStringExtra("name") ?: ""
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200) {
            setResult(200)
            finish()
        }
    }
}
