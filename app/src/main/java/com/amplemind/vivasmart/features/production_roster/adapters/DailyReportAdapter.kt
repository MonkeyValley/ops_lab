package com.amplemind.vivasmart.features.production_roster.adapters

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.utils.STAGE_TYPE_ID
import com.amplemind.vivasmart.databinding.ItemDailyReportBinding
import com.amplemind.vivasmart.features.production_roster.viewModels.ItemDailyReportViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.DailyPayModel
import io.realm.Realm
import java.util.*

class DailyReportAdapter : RecyclerView.Adapter<DailyReportAdapter.DailyReportViewHolder>() {

    private var mItems: List<ItemDailyReportViewModel>? = null
    var date = ""

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): DailyReportViewHolder {
        val binding = ItemDailyReportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DailyReportViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return mItems?.size ?: 0
    }

    override fun onBindViewHolder(viewHolder: DailyReportViewHolder, index: Int) {
        viewHolder.bind(mItems!![index])
    }

    fun setItems(items: List<ItemDailyReportViewModel>?) {
        mItems = items
        notifyDataSetChanged()
    }

    inner class DailyReportViewHolder(val binding: ItemDailyReportBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ItemDailyReportViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()

            if(STAGE_TYPE_ID == 5) {
                validateDailyPay(binding.idCardView, item.code)
            }
        }
    }

    fun validateDailyPay(cardView: CardView, code: String) {
        Realm.getDefaultInstance().use {

            if(date == "") {
                val calendar = Calendar.getInstance()
                val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
                val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
                date = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
            }

            var result = it
                    .where(DailyPayModel::class.java)
                    .equalTo("collaboratorEmployeeCode", code)
                    .and()
                    .equalTo("date", date)
                    .findFirst()

            if(result != null) {
                cardView.setBackgroundColor(Color.parseColor("#9FC693"))
            } else {
                cardView.setBackgroundColor(Color.parseColor("#FFEFF2F5"))
            }
        }
    }


}