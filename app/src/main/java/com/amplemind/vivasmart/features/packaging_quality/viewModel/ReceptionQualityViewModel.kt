package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.vo_core.repository.ReceptionQualityRepository
import com.amplemind.vivasmart.vo_core.repository.models.realm.IssueModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityRevisionModel
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ReceptionQualityViewModel @Inject constructor (
        private val mPreferences: UserAppPreferences,
        private val mRepository: ReceptionQualityRepository
) {

    var carryOrderId: Long = -1
    var unitId: Long = -1

    var sampleSize: Int = 0

    var totalDamaged: Int = 0
    var totalExport: Int = 0
        set(value) {
            if (value <= sampleSize) {
                totalDamaged = sampleSize - value
                field = value
            }
        }

    lateinit var carryOrder: CarryOrderModel
    lateinit var issues: List<IssueModel>

    fun cleanUp() {
        mRepository.cleanUp()
    }

    fun loadReceptionQualityData(): Observable<ReceptionQualityData> =
            mRepository.loadCarryOrder(carryOrderId.toInt())
                    .doOnNext{ carryOrder ->
                        this.carryOrder = carryOrder
                    }
                    .switchMap { carryOrder ->
                        loadIssues(carryOrder)
                    }
                    .map { issues ->
                        this.issues = issues
                        val issueViewModels = issues.map {issue ->
                            ItemReceptionQualityCriteriaViewModel(issue)
                        }
                        ReceptionQualityData(carryOrder, issueViewModels)
                    }

    private fun loadIssues(carryOrder: CarryOrderModel) =
            mRepository.loadIssues(carryOrder.cropId)

    fun createReceptionQualityRevision(
            issues: List<ItemReceptionQualityCriteriaViewModel>,
            exportableCount: Int,
            totalPermanentlyDamaged: Int,
            totalConditionallyDamaged: Int
    ): Single<ReceptionQualityRevisionModel> {

        val issueCounts = issues.map {issue ->
            ReceptionQualityRevisionModel.IssueCountModel(
                    issue.issueId,
                    issue.count,
                    issue.isPermanent
            )
        }

        val revision =
                ReceptionQualityRevisionModel(
                        carryOrderId,
                        unitId,
                        mPreferences.user.id,
                        issueCounts,
                        sampleSize,
                        exportableCount,
                        totalPermanentlyDamaged,
                        totalConditionallyDamaged
                )

        return mRepository.createReceptionQualityRevision(revision)
    }

    data class ReceptionQualityData (
            val carryOrder: CarryOrderModel,
            val issues: List<ItemReceptionQualityCriteriaViewModel>
    )

}