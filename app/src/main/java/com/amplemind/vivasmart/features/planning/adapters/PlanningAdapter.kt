package com.amplemind.vivasmart.features.planning.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemPlanningBinding
import com.amplemind.vivasmart.features.planning.viewmodels.PlanningItemViewModel

class PlanningAdapter constructor() : RecyclerView.Adapter<PlanningAdapter.PlanningViewHolder>() {

    private var list = listOf<PlanningItemViewModel>()

    fun addElements(data : List<PlanningItemViewModel>){
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlanningViewHolder {
        val binding = ItemPlanningBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PlanningViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: PlanningViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class PlanningViewHolder(private val binding: ItemPlanningBinding) : RecyclerView.ViewHolder(binding.root) {


        fun bind(item: PlanningItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
        }
    }
}
