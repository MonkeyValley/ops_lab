package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ProductionCategoriesReportModel(@SerializedName("table_1") val table1: List<ProductionCategoriesTable>?,
                                           @SerializedName("table_2") val table2: List<ProductionCategoriesTable>?,
                                           @SerializedName("table_3") val table3: List<ProductionCategoriesTable>?,
                                           @SerializedName("table_4") val table4: List<ProductionCategoriesTable>?,
                                           @SerializedName("table_5") val table5: List<ProductionCategoriesTable>?,
                                           @SerializedName("table_6") val table6: List<ProductionCategoriesTable>?)

@Entity
data class ProductionCategoriesTable(@SerializedName("description") val description: String,
                                     @SerializedName("groove_no") val grooveNo: Int,
                                     @SerializedName("grooves") val grooves: Int,
                                     @SerializedName("id") @PrimaryKey val id: Int,
                                     @SerializedName("image") val image: String,
                                     @SerializedName("is_active") val isActive: Boolean,
                                     @SerializedName("issue_id") val issueId: Int,
                                     @SerializedName("sign_super") val signSuper: String,
                                     @SerializedName("sign_super_qa") val signSuperQa: String,
                                     @SerializedName("stage_id") val stageId: Int,
                                     @SerializedName("sub_category") val subCategory: String,
                                     @SerializedName("table_no") val tableNo: Int,
                                     val isLocalModel: Boolean = false,
                                     val readOnly: Boolean): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(description)
        parcel.writeInt(grooveNo)
        parcel.writeInt(grooves)
        parcel.writeInt(id)
        parcel.writeString(image)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeInt(issueId)
        parcel.writeString(signSuper)
        parcel.writeString(signSuperQa)
        parcel.writeInt(stageId)
        parcel.writeString(subCategory)
        parcel.writeInt(tableNo)
        parcel.writeByte(if (isLocalModel) 1 else 0)
        parcel.writeByte(if (readOnly) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductionCategoriesTable> {
        override fun createFromParcel(parcel: Parcel): ProductionCategoriesTable {
            return ProductionCategoriesTable(parcel)
        }

        override fun newArray(size: Int): Array<ProductionCategoriesTable?> {
            return arrayOfNulls(size)
        }
    }


}