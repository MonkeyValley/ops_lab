package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class AssignCollaboratorModel (
        @SerializedName("packingline_id") val packingline_id: Int,
        @SerializedName("activity_id") val activity_id: Int,
        @SerializedName("collaborator_id") val collaborator_id: Int,
        @SerializedName("packing_id") val packing_id: Int?,
        @SerializedName("is_time") val isTime: Boolean
)

