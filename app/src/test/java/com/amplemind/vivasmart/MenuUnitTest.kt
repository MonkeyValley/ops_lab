package com.amplemind.vivasmart

import com.amplemind.vivasmart.core.extensions.onlyDigits
import com.amplemind.vivasmart.core.repository.PayrollActivitiesRepository
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class MenuUnitTest {

    lateinit var repository: PayrollActivitiesRepository

    @Before
    fun setup() {
        repository = PayrollActivitiesRepository()
    }



    @Test
    fun testLoadActivitiesAll() {
        repository.getActivities(true)
                .test()
                .assertValueCount(1)
                .assertValue {
                    it.size == 3
                }
    }

    @Test
    fun testLoadActivities() {
        repository.getActivities(false)
                .test()
                .assertValueCount(1)
                .assertValue {
                    it.isNotEmpty()
                }
    }

}