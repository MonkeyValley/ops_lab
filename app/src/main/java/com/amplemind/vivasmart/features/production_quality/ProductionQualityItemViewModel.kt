package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.features.interfaces.IProductionQualityItemViewModel
import com.amplemind.vivasmart.core.repository.model.ProductionQualityModel
import java.util.*
import javax.inject.Inject

class ProductionQualityItemViewModel @Inject constructor(model: ProductionQualityModel) : IProductionQualityItemViewModel, Observable() {

    val name: String
    val complete: Boolean

    init {
        complete = model.complete
        if (complete) name = "" else name = model.name
    }

}