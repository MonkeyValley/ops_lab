package com.amplemind.vivasmart.vo_features.production_roster.adapters


import android.graphics.Color
import android.graphics.drawable.Animatable
import android.net.Uri
import androidx.core.content.ContextCompat
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemUsersTimerBinding
import com.amplemind.vivasmart.features.adapters.SwipableViewHolder
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.utils.profileImageUrl
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ActivityLogRecyclerViewModel
import androidx.databinding.library.baseAdapters.BR
import com.amplemind.vivasmart.core.utils.STAGE_TYPE_ID
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jakewharton.rxbinding2.view.RxView
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.item_users_timer.view.*
import java.util.concurrent.TimeUnit

class TimerUserViewHolder(val binding: ItemUsersTimerBinding, private val mEventBus: EventBus) :
        BaseAdapter.ViewHolder<ActivityLogRecyclerViewModel>(binding.root),
        SwipableViewHolder
{

    override val viewForeground: View?
        get() = binding.root.findViewById(R.id.view_foreground)

    override val viewDelete: View?
        get() = binding.root.findViewById(R.id.view_delete)

    override val viewFinish: View?
        get() = binding.root.findViewById(R.id.view_finish)

    lateinit var play: ImageButton
    lateinit var ivClock: ImageView
    lateinit var playTime: LinearLayout
    lateinit var tvUnit: TextView
    lateinit var tvTimeChronometer: TextView
    lateinit var ivUserImage: CircleImageView

    lateinit var mViewModel: ActivityLogRecyclerViewModel

    val subscriptions = AndroidDisposable()

    override fun cleanUp() {
        subscriptions.dispose()
    }

    override fun bind(viewModel: ActivityLogRecyclerViewModel) {

        viewModel.updateMissingUnitsError()

        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()

        mViewModel = viewModel

        val activityLog = viewModel.item

        val view = binding.root

        play                = view.findViewById(R.id.start_chronometer)
        ivClock             = view.findViewById(R.id.iv_clock)
        playTime            = view.findViewById(R.id.ln_time)
        tvUnit              = view.findViewById(R.id.tv_unit)
        tvTimeChronometer   = view.findViewById(R.id.tv_time_chronometer)
        ivUserImage         = view.findViewById(R.id.profileImage)


        if(STAGE_TYPE_ID == 5) {
            if (mViewModel.validateCollaboratorDailyPayment(activityLog.collaboratorUuid)) {
                ivUserImage.apply {
                    borderWidth = 5
                    borderColor = Color.GREEN
                }
            } else {
                ivUserImage.apply {
                    borderWidth = 5
                    borderColor = Color.WHITE
                }
            }
        }

        RxView.clicks(ivUserImage)
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe{
                    mEventBus.send(OnProfileImageClickEvent(activityLog))
                }.addTo(subscriptions)

        //tvTimeChronometer.text = formatTime(activityLog.time)

        if (activityLog.isPaused) {
            stopAnimation()
            play.visibility = View.VISIBLE
        }
        else {
            startAnimation()
            play.visibility = View.GONE
        }

        Glide.with(view.context)
                .load(Uri.parse(activityLog.collaborator?.profileImageUrl))
                .apply(RequestOptions().placeholder(R.drawable.ic_user))
                .into(ivUserImage)

        RxView.clicks(play)
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe{
                    mEventBus.send(OnTimerClickEvent(activityLog, true))
                }
                .addTo(subscriptions)

        RxView.clicks(ivClock)
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe{
                    mEventBus.send(OnTimerClickEvent(activityLog, false))
                }
                .addTo(subscriptions)

        if (mViewModel.showMissingUnitsError) {
            tvTimeChronometer.setTextColor(ContextCompat.getColor(view.context, R.color.redAlert))
            tvUnit.setTextColor(ContextCompat.getColor(view.context, R.color.redAlert))
            ivClock.setImageResource(R.drawable.ic_clock_red)
            (ivClock.drawable as Animatable).start()
            play.setImageResource(R.drawable.ic_start_red)
        } else {
            play.setImageResource(R.drawable.ic_start)
        }
    }

    private fun startAnimation() {
        val view = binding.root

        view.apply {
            start_chronometer.visibility = View.GONE
            iv_clock.visibility = View.VISIBLE
            (iv_clock.drawable as Animatable).start()
            tv_time_chronometer.setTextColor(ContextCompat.getColor(context, R.color.blue))
            tv_unit.setTextColor(ContextCompat.getColor(context, R.color.blue))
            iv_clock.setImageResource(R.drawable.ic_clock)
            (iv_clock.drawable as Animatable).start()
        }
    }

    private fun stopAnimation() {
        val view = binding.root

        view.apply {
            start_chronometer.visibility = View.VISIBLE
            iv_clock.visibility = View.GONE
            start_chronometer.setImageResource(R.drawable.ic_start)
            tv_time_chronometer.setTextColor(ContextCompat.getColor(view.context, R.color.dark_gray))
            tv_unit.setTextColor(ContextCompat.getColor(view.context, R.color.dark_gray))
        }
    }

    data class OnProfileImageClickEvent(
            val activityLog: ActivityLogModel
    )

    data class OnTimerClickEvent(
            val activityLog: ActivityLogModel,
            val timerStarted: Boolean
    )


}