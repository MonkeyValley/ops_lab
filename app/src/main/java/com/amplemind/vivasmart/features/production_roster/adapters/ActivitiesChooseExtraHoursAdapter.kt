package com.amplemind.vivasmart.features.production_roster.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ItemActivitiesPayrollViewModel
import io.reactivex.subjects.BehaviorSubject


class ActivitiesChooseExtraHoursAdapter  () : RecyclerView.Adapter<ActivitiesChooseExtraHoursAdapter.ActivitiesExtraHourViewHolder>() {

    private var list = mutableListOf<ItemActivitiesPayrollViewModel>()
    private val clickSubject = BehaviorSubject.create<ItemActivitiesPayrollViewModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivitiesExtraHourViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_choose_activity_for_extrahour, parent, false)
        return ActivitiesExtraHourViewHolder(v)
    }
    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ActivitiesExtraHourViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(activities: List<ItemActivitiesPayrollViewModel>) {
        list.clear()
        list.addAll(activities)
        notifyDataSetChanged()
    }

    fun onClickItem(): BehaviorSubject<ItemActivitiesPayrollViewModel> {
        return clickSubject
    }

    inner class ActivitiesExtraHourViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var layout_state: LinearLayout
        lateinit var img_icon: ImageView
        lateinit var txt_name: TextView
        lateinit var layout_check: LinearLayout
        lateinit var img_checkAccept: ImageView

        fun bind(item: ItemActivitiesPayrollViewModel) {
            layout_state = itemView.findViewById(R.id.ica_extrahour_layout_state) as LinearLayout
            img_icon = itemView.findViewById(R.id.ica_extrahour_img_icon) as ImageView
            txt_name = itemView.findViewById(R.id.ica_extrahour_txt_name) as TextView
            layout_check = itemView.findViewById(R.id.ica_extrahour_check_accept) as LinearLayout
            img_checkAccept = itemView.findViewById(R.id.ica_extrahour_imgv_accept) as ImageView

            txt_name.text = item.name.get()
            //img_icon.setImageUrl(item.icon)

            layout_state.setOnClickListener{
                if (item.isExtraHour){
                    item.isExtraHour = false
                    layout_check.visibility = View.GONE
                }else{
                    item.isExtraHour = true
                    layout_check.visibility = View.VISIBLE
                }
                clickSubject.onNext(item)
            }

        }

    }
}