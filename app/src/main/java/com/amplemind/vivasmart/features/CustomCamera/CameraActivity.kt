package com.amplemind.vivasmart.features.CustomCamera

import android.app.Activity
import android.content.Intent
import android.hardware.Camera
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import com.amplemind.vivasmart.R
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class CameraActivity : AppCompatActivity(),  View.OnClickListener{

    lateinit var show:showCamera
    var camera = Camera.open()
    var lock = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        show = showCamera(camera , this);
        frameLayout.addView(show)
        btn_takePicture.setOnClickListener(this)

    }

    var takePictureCallback = object : Camera.PictureCallback{
        override fun onPictureTaken(data: ByteArray?, camera: Camera?) {

            val file_picture = getOutputMediaFile()

            if(file_picture.path == ""){
                return
            }else{
                try {
                    val fileoutput = FileOutputStream(file_picture)

                    fileoutput.write(data)
                    fileoutput.close()

                    val dataInt = Intent()
                    val uri = Uri.fromFile(file_picture);
                    dataInt.setData(uri)
                    setResult(Activity.RESULT_OK, dataInt)
                    finish()

                }catch (e: IOException)
                {
                    e.printStackTrace()
                }
            }
        }
    }
    private fun getOutputMediaFile(): File {

        var state = Environment.getExternalStorageState();
        if (!state.equals(Environment.MEDIA_MOUNTED)){
            return File("")
        }else{
            val folder = File( Environment.getExternalStorageDirectory().toString() + File.separator + "GUI")
            if(!folder.exists() ){
                folder.mkdir()
            }
            val outputFile = File(folder, "tep.jpg")
            return  outputFile
        }

    }
    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_takePicture ->{
                if(lock) {
                    lock = false
                    camera.takePicture(null, null, takePictureCallback)
                }
            }
        }

    }
}
