package com.amplemind.vivasmart.features.fertiriego.viewModel

import android.content.Context
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.fertiriego.fragment.SoilReportFragment
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService
import com.amplemind.vivasmart.features.mipe.viewModel.LotMipeItemViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PHCEForTable
import com.amplemind.vivasmart.vo_core.repository.models.realm.PulseForTable
import com.amplemind.vivasmart.vo_core.repository.models.realm.TablesPerDay
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveForTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.realm.RealmList
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class FertirriegoSoilMonitoringViewModel @Inject constructor(
        private val service: FertirriegoService
) : BaseViewModel() {

    var lotId = "0"
    var date = ""

    lateinit var fragment: SoilReportFragment

    fun setServiceContect(context: Context){
        service.context = context
        service.preferences  = UserAppPreferences(context, null, null)
    }

    fun validateDate(): Boolean {
        val calendar = Calendar.getInstance()

        var month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        var day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val todayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        calendar.add(Calendar.DATE, -1)
        month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val yesterdayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        return date == todayDate //|| date == yesterdayDate
    }

    private val requestSuccessPHCEDataPerDay = BehaviorSubject.create<List<PHCEForTable>>()

    fun getPHCEDataForTable(lotId: Int, date: String, table: String): Disposable {
        return service.getPHCETableDataPerDay(lotId, date, table)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccessPHCEDataPerDay.onNext(it)
                }, {
                    fragment.setDataToZero()
                })
    }
    fun onSuccesPHCEDataPerDay(): BehaviorSubject<List<PHCEForTable>> = requestSuccessPHCEDataPerDay

    private val requestSuccessPulseForTable = BehaviorSubject.create<List<SoilReportValveModel>>()

    fun getValveForTable(lotId: Int, date: String, table: String): Disposable {
        return service.getValveForTable(lotId, date, table)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map { mapModelToViewModels(it) }
                .subscribe({
                    requestSuccessPulseForTable.onNext(it)
                }, {
                    fragment.setDataToZero()
                })
    }
    private fun mapModelToViewModels(models: List<ValveForTable>): List<SoilReportValveModel> {
        val list = arrayListOf<SoilReportValveModel>()
        var obj = SoilReportValveModel()
        models.forEach {
            if(obj.valveId == 0) {
                obj.valveId = it.valveId
                obj.valveName = it.valveId.toString()
                obj.valveForTable!!.add(it)
                if(it.meditionType == 1) obj.valveUuId = it.uuid
            }

            if(obj.valveId == it.valveId) {
                obj.valveForTable!!.add(it)
            } else {
                list.add(obj)
                obj = SoilReportValveModel()
                obj.valveId = it.valveId
                obj.valveName = it.valveId.toString()
                obj.valveForTable!!.add(it)
                if(it.meditionType == 1) obj.valveUuId = it.uuid
            }
        }

        if(obj.valveId != 0) list.add(obj)

        return list
    }
    fun onSuccesPulseForTable(): BehaviorSubject<List<SoilReportValveModel>> = requestSuccessPulseForTable

    fun saveData(valveUuId: String, pulsesNo: Int, minutes: Double, m3ha: Int) {
        service.saveM3HA(valveUuId, pulsesNo, minutes, m3ha)
    }

}

open class SoilReportValveModel(
        var valveId : Int? = 0,
        var valveName: String = "",
        var m3ha: Int = 0,
        var pulsesNo: Int = 0,
        var minutes: Double = 0.0,
        var valveUuId: String = "",
        var active: Boolean = true,
        var valveForTable: RealmList<ValveForTable>? = RealmList()
)