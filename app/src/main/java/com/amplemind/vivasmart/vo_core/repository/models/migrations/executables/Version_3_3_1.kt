package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_3_3_1 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectStageTypeModelrealm(realm)
    }

    private fun migrateUploadObjectStageTypeModelrealm(realm: DynamicRealm){
        realm.schema.get("StageTypeModel")?.apply {
            addField("activityLogCount", Int::class.javaObjectType)
            renameField("active", "isClosed")
        }
    }


    override fun drop(realm: DynamicRealm) {

    }
}