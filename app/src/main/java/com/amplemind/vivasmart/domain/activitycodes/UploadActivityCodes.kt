package com.amplemind.vivasmart.domain.activitycodes

import android.util.Log
import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.core.repository.local.ActivitiesDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.PendingRequestModel
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class UploadActivityCodes @Inject constructor(private val repositoryTimer: ActivityCodeRepository,
                                              private val preferences: UserAppPreferences,
                                              private val database: ActivitiesDao) {

    fun uploadActivityCode(pendingRequest: PendingRequestModel) {
        doAsync {
            val code = database.getActivityCodeById(pendingRequest.tablePrimaryKey)
            repositoryTimer.createCode(preferences.token, code.activityId, code.stageId)
                    .subscribe({
                        database.deleteActivityCode(code.id!!)
                    }, {
                        Log.e("Fail upload","Error ${it.localizedMessage}")
                    })
        }
    }

}