package com.amplemind.vivasmart.features.phenology.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toDp
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.phenology.PhenologyActivity
import com.amplemind.vivasmart.features.phenology.PhenologyVarietiesPlantsActivity
import com.amplemind.vivasmart.features.phenology.adapters.VarietiesPhenologyAdapter
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyDetailDialog
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyPostRevisionResponse
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyResponse
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVarieties
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVars
import com.amplemind.vivasmart.features.phenology.viewmodel.PhenologyVarietiesFragmentViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesPhenologyItemViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesTablesPhenologyItemViewModel
import com.google.gson.Gson
import dagger.android.DispatchingAndroidInjector
import kotlinx.android.synthetic.main.fragment_varieties_phenology.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

@SuppressLint("Registered")
class PhenologyVarietiesFragment : BaseFragment() {
    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var viewModel: PhenologyVarietiesFragmentViewModel

    var phenology_vars = ArrayList<PhenologyVars>()
    var varieties = ArrayList<PhenologyVarieties>()

    companion object {
        const val resultRevision = "resultRevision"
        var lot_name: String? = ""
        var lot_id: Int? = 0
        var stage_id: Int? = 0
        var is_commercial: Int? = 0
        var isNew: Boolean = true

        fun ifConected(): Boolean {
            return if (PhenologyVarietiesFragment().hasInternet())
                true
            else {
                Toast.makeText(PhenologyVarietiesFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

    fun newInstance(title: String, lot_id: Int, lot_name: String, stage_id: Int, is_commercial: Int): PhenologyVarietiesFragment {
        val args = Bundle()
        args.putString("Title", title)
        args.putInt("lot_id", lot_id)
        args.putString("lot_name", lot_name)
        args.putInt("stage_id", stage_id)
        args.putInt("is_commercial", is_commercial)

        val fragment = PhenologyVarietiesFragment()
        fragment.arguments = args
        return fragment
    }

    fun showVarietiesRevisionActivity(varieties: VarietiesPhenologyItemViewModel, table: VarietiesTablesPhenologyItemViewModel, Vars: MutableList<PhenologyVars>, isNew : Boolean) {
        if(ifConected()) {
            showProgressDialog(true)
            val intent = Intent(activity, PhenologyVarietiesPlantsActivity::class.java)
            intent.putExtra("stage_id", stage_id)
            intent.putExtra("lot_id", lot_id)
            intent.putExtra("lot_name", lot_name)
            intent.putExtra("variety_id", varieties.variety_id)
            intent.putExtra("variety_name", varieties.variety_name)
            intent.putExtra("has_cluster", varieties.has_cluster)

            intent.putExtra("table", Gson().toJson(table))
            intent.putExtra("vars", Gson().toJson(Vars))
            intent.putExtra("isNew", isNew)

            startActivity(intent)
        }
    }

    fun showVarietiesRevisionDetail(varieties: VarietiesPhenologyItemViewModel, table: VarietiesTablesPhenologyItemViewModel, Vars: MutableList<PhenologyVars>) {
        if(ifConected()) {
            val dialog = PhenologyDetailDialog().newInstance(stage_id!!, Gson().toJson(varieties), Gson().toJson(table))
            dialog.show(fragmentManager!!, PhenologyDetailDialog.TAG)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_varieties_phenology, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lot_id = arguments?.getInt("lot_id")
        lot_name = arguments?.getString("lot_name")
        stage_id = arguments?.getInt("stage_id")
        is_commercial = arguments?.getInt("is_commercial")

        (activity as PhenologyActivity).supportActionBar!!.title = arguments!!.getString("lot_name")
        setupRecycler()

        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getLotsVarieties(stage_id!!, is_commercial!!)
        viewModel.onSuccessRequest_lots().subscribe(this::getPhenologyResponse).addTo(subscriptions)

        when(is_commercial){
            0-> lbl_track_title.text = "/ Fenología / Variedades de pruebas / "+ arguments!!.getString("lot_name")
            1-> lbl_track_title.text = "/ Fenología / Variedades comerciales / "+ arguments!!.getString("lot_name")
        }
    }

    private fun setupRecycler() {
        rv_phenology_varieties.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_phenology_varieties.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    fun getData() {
        viewModel.getLotsVarieties(stage_id!!, is_commercial!!)
    }

    fun getPhenologyResponse(data: PhenologyResponse) {
        phenology_vars = data.phenology_vars as ArrayList<PhenologyVars>
        varieties = data.varieties as ArrayList<PhenologyVarieties>

        val listaVar = ArrayList<VarietiesPhenologyItemViewModel>()
        for (item in varieties) {
            val obj = VarietiesPhenologyItemViewModel(item)
            listaVar.add(obj)
        }
        addElements(listaVar, phenology_vars)
    }

    fun addElements(data: ArrayList<VarietiesPhenologyItemViewModel>, phenology_vars: ArrayList<PhenologyVars>) {
        val mAdapter = VarietiesPhenologyAdapter(this, data, phenology_vars)
        rv_phenology_varieties.adapter = mAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == 250) {
            var response  = data!!.getStringExtra(resultRevision)
            var objectResponse = Gson().fromJson(response, PhenologyPostRevisionResponse::class.java)
            Log.d("objectResponse",Gson().toJson(objectResponse) )
        }
    }

}
