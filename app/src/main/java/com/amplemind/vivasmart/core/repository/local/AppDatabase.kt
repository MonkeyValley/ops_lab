package com.amplemind.vivasmart.core.repository.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.request.*
import com.amplemind.vivasmart.core.repository.response.ActivitiesResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResponse
import com.amplemind.vivasmart.core.repository.response.IsEditable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

@Database(entities = [
    (CollaboratosRecent::class),
    (LotModel::class),
    (Stage::class),
    (BusinessUnit::class),
    (CropStage::class),
    (CropType::class),
    (ActivitiesModel::class),
    (CropUnit::class),
    (Crop::class),
    (GroovesResultReponse::class),
    (Grooves::class),
    (PendingRequestModel::class),
    (CollaboratorsReponse::class),
    (CollaboratorsListReponse::class),
    (ActivityCodeModel::class),
    (RosterLineModel::class),
    (LotsPackageResultResponse::class),
    (ActivitiesResponse::class),
    (PresentationResultResponse::class),
    (PackingLineCollaboratorsModel::class),
    (DetailtRosterResponse::class),
    (ActivityCodeDetail::class),
    (CollaboratorsInLinesResponse::class),
    (BoxCountResultResponse::class),
    (ControlQualityListModel::class),
    (ControlQualityActivitiesListModel::class),
    (ControlQualityGroovesModel::class),
    (IssueModel::class),
    (ReviewIssueRequest::class),
    (GrooveIssueRequest::class),
    (ProductionCategoriesModel::class),
    (IssuesResultResponse::class),
    (CreateIssueRequest::class),
    (CountingBox::class),
    (DetailtRosterLinesResponse::class),
    (PackingLineCollaborator::class),
    (Packing::class),
    (ProductionCategoriesTable::class),
    (CarryOrderCondensedModel::class),
    (DeleteCollaboratorsOffline::class),
    (UnitModel::class),
    (QualityCarryOrderRequest::class),
    (UserModel::class),
    (ProductionQualityOfflineRequest::class),
    (UpdateCarryOrderSync::class),
    (RoleModel::class)
], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun splashDao(): SplashDao

    abstract fun collaboratorsDao(): CollaboratorsDao

    abstract fun collaboratorsLineDao() : CollaboratorsLineDao

    abstract fun lotsDao(): LotsDao

    abstract fun activitiesDao(): ActivitiesDao

    abstract fun groovesDao(): GroovesDao

    abstract fun packaginLineDao(): PackingLineDao

    abstract fun controlQualityDao(): ControlQualityDao

    abstract fun productionQualityDao(): ProductionQualityDao

    abstract fun recibaDao(): RecibaDao

}

class Converters {

    private val gson = Gson()

    @TypeConverter
    fun fromListCrop(value: String?): List<Crop>? {
        if (value == null) return listOf()
        val listType = object : TypeToken<List<Crop>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun listCropToString(crops: List<Crop>?): String? {
        return gson.toJson(crops)
    }

    @TypeConverter
    fun fromListTables(value: String?): List<Int>? {
        if (value == null) return listOf()
        val listType = object : TypeToken<List<Int>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun listTablesToString(crops: List<Int>?): String? {
        return gson.toJson(crops)
    }

    @TypeConverter
    fun fromListCropLine(value: String?): List<CropLine>? {
        if (value == null) return listOf()
        val listType = object : TypeToken<List<CropLine>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun listCropLineToString(crops: List<CropLine>?): String? {
        return gson.toJson(crops)
    }

    @TypeConverter
    fun fromListGrooves(value: String?): List<GroovesResultReponse>? {
        if (value == null) return listOf()
        val listType = object : TypeToken<List<GroovesResultReponse>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun listGroovesToString(grooves: List<GroovesResultReponse>?): String? {
        return gson.toJson(grooves)
    }

    @TypeConverter
    fun fromListIsEditable(value: String?): List<IsEditable> {
        if (value == null) return listOf()
        val listType = object : TypeToken<List<IsEditable>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun listIsEditableToString(isEditable: List<IsEditable>): String? {
        return gson.toJson(isEditable)
    }

    @TypeConverter
    fun fromListListIssueRequest(value: String?): List<ListIssueRequest> {
        if (value == null) return listOf()
        val listType = object : TypeToken<List<ListIssueRequest>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun listIssueRequestToString(packing: List<ListIssueRequest>): String? {
        return gson.toJson(packing)
    }

    @TypeConverter
    fun fromListIssueRequest(value: String?): List<IssueRequest> {
        if (value == null) return listOf()
        val listType = object : TypeToken<List<IssueRequest>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun issueRequestToString(packing: List<IssueRequest>): String? {
        return gson.toJson(packing)
    }

}