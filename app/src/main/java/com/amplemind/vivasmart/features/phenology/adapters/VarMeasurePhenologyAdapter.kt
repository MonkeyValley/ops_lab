package com.amplemind.vivasmart.features.phenology.adapters

import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.databinding.VarMeasurePhenologyItemBinding
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyVarietiesPlantsFragment
import com.amplemind.vivasmart.features.phenology.viewmodel.VarMeasurePhenologyItemViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangeVariatiesModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.PublishSubject

class VarMeasurePhenologyAdapter constructor(val fragment: PhenologyVarietiesPlantsFragment) : RecyclerView.Adapter<VarMeasurePhenologyAdapter.VarMeasurePhenologyViewHolder>() {
    private val clickSubject = PublishSubject.create<VarMeasurePhenologyItemViewModel>()

    var editable = false
    private var list = listOf<VarMeasurePhenologyItemViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VarMeasurePhenologyViewHolder {
        val binding = VarMeasurePhenologyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VarMeasurePhenologyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: VarMeasurePhenologyViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(data: List<VarMeasurePhenologyItemViewModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class VarMeasurePhenologyViewHolder(private val binding: VarMeasurePhenologyItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: VarMeasurePhenologyItemViewModel

        fun bind(item: VarMeasurePhenologyItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.TVVarName.setOnClickListener {
                fragment.showVarMeasureDetail(item)
            }

            var issues = ""

            binding.ETMedition.setOnFocusChangeListener { view, foscus ->
                if (foscus) issues = item.phenology_var_name!!
            }

            binding.ETMedition.isEnable(editable)
            binding.ETMedition.setText(if (item.medition.toString() == "-10.0") "" else item.medition.toString())

            binding.ETMedition.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (p0 != null)
                        if (p0.isNotBlank() && p0.toString() != ".")
                            if (issues.equals(item.phenology_var_name)) {
                                item.medition = (p0.toString()).toDouble()
                            }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (p0 != null)
                        if (p0.isNotBlank())
                            if (issues.equals(item.phenology_var_name)) {
                                item.medition = -10.0
                            }
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (p0 != null)
                        if (p0.isNotBlank() && p0.toString() != ".")
                            if (issues.equals(item.phenology_var_name)) {
                                item.medition = (p0.toString()).toDouble()
                            }
                }
            })

        }

    }

}


