package com.amplemind.vivasmart.core.repository.model

data class RosterDetailModelTemp(val activity: String, val lote : String, val hrs : String, val cant : Int, val money : Int, val status : Boolean)
