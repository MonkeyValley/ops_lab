package com.amplemind.vivasmart.features.crop_validation.dialogs

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.custom_views.Signature
import com.amplemind.vivasmart.core.custom_views.SignatureListener
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.CustomCamera.CameraActivity
import com.amplemind.vivasmart.features.crop_validation.fragment.CropIssuesValidationFragment.Companion.all_ok
import com.amplemind.vivasmart.features.crop_validation.viewmodel.SignPhotoCropValidationDialogViewModel
import com.amplemind.vivasmart.features.waste_control.fragment.WasteControlTestFragment.Companion.ifConected
import com.google.gson.Gson
import java.io.File
import java.io.IOException
import javax.inject.Inject


class SignPhotoCropValidationDialog : BaseDialogFragment() {

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mPermission: AndroidPermission

    @Inject
    lateinit var viewModel: SignPhotoCropValidationDialogViewModel

    private val CAMERA_REQUEST_CODE = 1

    private lateinit var viewButton: View
    private lateinit var path: String
    private lateinit var imagenBitmap: Bitmap
    val imageList = mutableListOf<File>()
    var imageListSize: Int = 0
    val signList = mutableListOf<File>()
    var skipSignature: Boolean = true

    companion object {
        val TAG = SignPhotoCropValidationDialog::class.java.simpleName
    }

    var imgButton1: ImageButton? = null
    var imgButton2: ImageButton? = null
    var imgButton3: ImageButton? = null
    var imgButton4: ImageButton? = null
    var imgButton5: ImageButton? = null
    var imgButton6: ImageButton? = null

    var image1: File? = null
    var image2: File? = null
    var image3: File? = null
    var image4: File? = null
    var image5: File? = null
    var image6: File? = null
    var table: TableLayout? = null
    var signature: Signature? = null
    var reload: ImageView? = null
    var headerTable: LinearLayout? = null

    fun newInstance( ): SignPhotoCropValidationDialog {
        return SignPhotoCropValidationDialog()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.fragment_gallery, null, false)
        addSubscribers()
        builder.setCancelable(false)
        builder.setView(mView)

        signature = mView.findViewById(R.id.signature_gallery)
        reload = mView.findViewById(R.id.btn_reload_signature_gallery)
        headerTable = mView.findViewById(R.id.ln_orden)
        table = mView.findViewById(R.id.table)

        reload!!.setOnClickListener { v -> onClick(v) }

        imgButton1 = mView.findViewById(R.id.img_btn1)
        imgButton2 = mView.findViewById(R.id.img_btn2)
        imgButton3 = mView.findViewById(R.id.img_btn3)
        imgButton4 = mView.findViewById(R.id.img_btn4)
        imgButton5 = mView.findViewById(R.id.img_btn5)
        imgButton6 = mView.findViewById(R.id.img_btn6)

        imgButton1!!.setOnClickListener { v -> onClick(v) }
        imgButton2!!.setOnClickListener { v -> onClick(v) }
        imgButton3!!.setOnClickListener { v -> onClick(v) }
        imgButton4!!.setOnClickListener { v -> onClick(v) }
        imgButton5!!.setOnClickListener { v -> onClick(v) }
        imgButton6!!.setOnClickListener { v -> onClick(v) }
        reload!!.setOnClickListener { v -> onClick(v) }

        if(all_ok) {
            table!!.visibility = View.GONE
            headerTable!!.visibility = View.GONE
        }

        signature!!.setOnSignatureListener(object : SignatureListener {
            override fun onDraw() {
                skipSignature = false
            }
        })

        builder.setPositiveButton("ENVIAR", null)
        builder.setNegativeButton("CANCELAR") { _, _ -> dismiss() }
        val create = builder.create()

        create.setOnShowListener {
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if(!skipSignature) {
                            if (all_ok) {
                                uploadSign()
                            } else if (image1 != null && !all_ok) {
                                uploadSign()
                                uploadImage()
                            } else {
                                showSnackbar(mView, "Agregue por lo menos 1 foto")
                            }
                        }
                        else{
                            showSnackbar(mView, "La firma es requerida")
                        }
                    }
        }
        return create
    }

    fun showSnackbar(mView: View, message: String){
        val snackbar = Snackbar.make(mView, message, Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.btn_reload_signature_gallery -> {
                skipSignature = true
                signature!!.clear()
            }
            else -> {
                if (mPermission.cameraPermission(this.context!!)) {
                    viewButton = view
                    initCamera()
                } else {
                    mPermission.verifyHavePermissions(this.activity!!)
                }
            }
        }
    }

    private fun initCamera() {
        /*try {
            val imageFile = this.context?.let { picturesHelper.createImageFile(it) }
            path = imageFile?.absolutePath.toString()
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (callCameraIntent.resolveActivity(activity?.packageManager!!) != null) {
                val authorities = activity?.packageName + ".fileprovider"
                val imageUri = this.context?.let { imageFile?.let { it1 -> FileProvider.getUriForFile(it, authorities, it1) } }
                callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
            }
        } catch (e: IOException) {
            val toast = Toast.makeText(this.context, "Could not create file!", Toast.LENGTH_LONG)
            toast.show()
        }*/
        val iny = Intent(this.context, CameraActivity::class.java)
        this.startActivityForResult(iny, CAMERA_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    when (viewButton.id) {
                        R.id.img_btn1 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton1!!.setImageBitmap(imagenBitmap)
                            image1 = compressImage(imagenBitmap)

                            table?.setColumnStretchable(1, true)
                            table?.setColumnStretchable(2, true)
                            imgButton2?.visibility = View.VISIBLE
                        }
                        R.id.img_btn2 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton2!!.setImageBitmap(imagenBitmap)
                            image2 = compressImage(imagenBitmap)

                            table?.setColumnStretchable(3, true)
                            table?.isStretchAllColumns = true
                            imgButton3?.visibility = View.VISIBLE
                        }
                        R.id.img_btn3 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton3!!.setImageBitmap(imagenBitmap)
                            image3 = compressImage(imagenBitmap)
                            imgButton4?.visibility = View.VISIBLE
                        }
                        R.id.img_btn4 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton4!!.setImageBitmap(imagenBitmap)
                            image4 = compressImage(imagenBitmap)
                            imgButton5?.visibility = View.VISIBLE
                        }
                        R.id.img_btn5 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton5!!.setImageBitmap(imagenBitmap)
                            image5 = compressImage(imagenBitmap)
                            imgButton6?.visibility = View.VISIBLE
                        }
                        R.id.img_btn6 -> {
                            imagenBitmap = MediaStore.Images.Media.getBitmap(this.context!!.contentResolver,
                                    data!!.data)
                            imgButton6!!.setImageBitmap(imagenBitmap)
                            image6 = compressImage(imagenBitmap)
                        }
                    }
                }
            }
        }
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(this.context!!)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(this.context!!, bitmap), image, 400f, 400f)
        return image
    }

    private fun uploadSign() {
        if (ifConected()) {
            val sign = signature!!.save()
            if (signature != null) {
                signList.add(compressImage(sign))
                viewModel.uploadImage(signList, "sign")
            } else {
                Toast.makeText(context, "La firma es requerida para hacer el envio", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun uploadImage() {
        if(ifConected()) {
            if (image1 != null) imageList.add(image1!!)
            if (image2 != null) imageList.add(image2!!)
            if (image3 != null) imageList.add(image3!!)
            if (image4 != null) imageList.add(image4!!)
            if (image5 != null) imageList.add(image5!!)
            if (image6 != null) imageList.add(image6!!)
            // Se agrega la cantidad de fotos a el 1 (de la firma )
            imageListSize = imageList.size
            viewModel.uploadImage(imageList, "issue")
        }
    }

    fun addSubscribers() {
        viewModel.onSuccessUpdateImageUrl().subscribe(this::getImagesList).addTo(subscriptions)
    }

    fun getImagesList(list: List<String>) {
        //esperar hasta que se envie toda la lista de imagenes de defectos
        if (!all_ok) {
            if (viewModel.imageList.size == imageListSize + 1) {
                sendResult(Gson().toJson(list))
            }
        }
        //Solo la imagen de la firma
        else {
            sendResult(Gson().toJson(list))
        }
    }

    private fun sendResult(message: String) {
        if (targetFragment == null) {
            return
        }

        val intent = activity?.intent
        intent?.putExtra("listImgUrlJSON", message)
        targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
        dismiss()
    }

}
