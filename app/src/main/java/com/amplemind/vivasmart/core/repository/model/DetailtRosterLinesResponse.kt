package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class DetailtRosterLinesResponse(
        @ColumnInfo(name = "id_activity") @SerializedName("activity_id") val activity_id : Int,
        @SerializedName("collaborator_id") val collaborator_id : Int,
        @ColumnInfo(name = "id_collaborator_db") @SerializedName("id") val id : Int,
        @SerializedName("is_practice") val is_practice : Boolean,
        @SerializedName("is_training") val is_training : Boolean,
        @SerializedName("extra_quantity") val extra_quantity : Double?,
        @SerializedName("total_day_time") val total_day_time : Double?,
        @SerializedName("packing_id") val packing_id : Int?,
        @SerializedName("packingline_collaborator_id") val packingline_collaborator_id : Int?,
        @SerializedName("quantity") val quantity : Double?,
        @SerializedName("stage_id") val stage_id : Int?,
        @SerializedName("time") val time : Double?,
        @SerializedName("extra_time") val extra_time : Double? = 0.0,
        @SerializedName("sign") val sign : String?,
        @Embedded @SerializedName("packingline_collaborator") val packinglineCollaborator : PackingLineCollaborator?,
        @Embedded @SerializedName("packing") val packing : Packing?,
        @Embedded @SerializedName("activity") val activity : ActivitiesModel?,
        @ColumnInfo(name = "id_lot") val id_lot : Int,
        @ColumnInfo(name = "status_sync") val status_sync : Boolean? = null,
        @PrimaryKey(autoGenerate = true) val id_detail : Long? = null
)

@Entity
data class PackingLineCollaborator(
        @PrimaryKey(autoGenerate = true) val id_packing_line: Int? = null,
        @SerializedName("is_time") val isTime: Boolean
)

@Entity
data class Packing(
        @PrimaryKey(autoGenerate = true) val id_packing : Int? = null,
        @ColumnInfo(name = "packing_cost") @SerializedName("cost") val cost: Double,
        @ColumnInfo(name = "packing_name") @SerializedName("name") val name: String
)
