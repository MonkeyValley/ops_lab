package com.amplemind.vivasmart.features.finished_product_quality.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.DecimalDigitsInputFilter
import com.amplemind.vivasmart.core.utils.WEIGHT_PROM_LIST
import com.amplemind.vivasmart.features.packaging_quality.adapters.RecibaAverageWeightPagerAdapter

class ProductQualityAverageWeightFragment : Fragment(), RecibaAverageWeightPagerAdapter.AverageWeightFragment {

    companion object {
        @JvmStatic
        fun newInstance() = ProductQualityAverageWeightFragment()
    }

    private val mBoxes = mutableListOf<EditText>()
    private lateinit var mAverage: TextView

    private var mAverageValue = 0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_average_weight, container, false)
        mBoxes.add(view.findViewById(R.id.box1))
        mBoxes.add(view.findViewById(R.id.box2))
        mBoxes.add(view.findViewById(R.id.box3))
        mBoxes.add(view.findViewById(R.id.box4))
        mBoxes.add(view.findViewById(R.id.box5))
        mBoxes.add(view.findViewById(R.id.box6))
        mBoxes.add(view.findViewById(R.id.box7))
        mBoxes.add(view.findViewById(R.id.box8))
        mBoxes.add(view.findViewById(R.id.box9))
        mBoxes.add(view.findViewById(R.id.box10))
        mBoxes.add(view.findViewById(R.id.box11))
        mBoxes.add(view.findViewById(R.id.box12))

        mBoxes.forEach { box ->
            box.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        }

        val textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                doAverageCalculations()
            }
        }

        mBoxes.forEach { box ->
            box.addTextChangedListener(textWatcher)
        }

        mAverage = view.findViewById(R.id.tv_avg_weight)

        loadData()

        doAverageCalculations()

        return view
    }

    private fun loadData(){
        if(WEIGHT_PROM_LIST.size != 0){
            var count = 0
            mBoxes.forEach { box ->
                if(WEIGHT_PROM_LIST[count] != "0") box.setText(WEIGHT_PROM_LIST[count])
                count++
            }
        }
    }

    private fun doAverageCalculations() {

        val sum = mBoxes.sumByDouble { box ->
            val value: Double = box.text.toString().toDoubleOrNull() ?: 0.0
            return@sumByDouble if (value > 0.0) (value) else 0.0
        }

        var x = 0.0
        for(etBox in mBoxes){
            if(etBox.text.toString() != ""){
                x++
            }
        }

        mAverageValue = sum / x

        mAverageValue = if(mAverageValue.isNaN()) 0.0 else mAverageValue

        mAverage.text = "%.2f Kg".format(mAverageValue)
    }

    override fun calculateAverage(): Double {
        WEIGHT_PROM_LIST.clear()
        mBoxes.forEach { box ->
            WEIGHT_PROM_LIST.add(box.text.toString())
        }
        return mAverageValue
    }



}
