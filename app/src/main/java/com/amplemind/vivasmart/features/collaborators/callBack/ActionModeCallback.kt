package com.amplemind.vivasmart.features.collaborators.callBack

import androidx.appcompat.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import com.amplemind.vivasmart.R
import io.reactivex.subjects.PublishSubject

class ActionModeCallback(private val showAddButton: Boolean = false) : ActionMode.Callback {

    private val actionModeSubject = PublishSubject.create<Boolean>()

    private val addClickSubject = PublishSubject.create<Boolean>()

    private val clearSelectionSubject = PublishSubject.create<Boolean>()

    private val resetAnimationSubject = PublishSubject.create<Boolean>()

    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        if (showAddButton){
            mode!!.menuInflater.inflate(R.menu.menu_clean, menu)
            return true
        }
        mode!!.menuInflater.inflate(R.menu.menu_seleted_collaborators, menu)
        return true
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.add_collaborators) {
            addClickSubject.onNext(true)
            return true
        }
        return false
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return false
    }

    fun onAddClick(): PublishSubject<Boolean> {
        return addClickSubject
    }

    fun getActionMode(): PublishSubject<Boolean> {
        return actionModeSubject
    }

    fun onClearSelection(): PublishSubject<Boolean> {
        return clearSelectionSubject
    }

    fun onResetAnimation(): PublishSubject<Boolean> {
        return resetAnimationSubject
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        clearSelectionSubject.onNext(true)

        actionModeSubject.onNext(false)

        resetAnimationSubject.onNext(true)
    }

}