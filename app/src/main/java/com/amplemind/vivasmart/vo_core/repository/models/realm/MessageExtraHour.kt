package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName

open class MessageExtraHour(
        @SerializedName("activity_id")              val activityId: Int = 0,
        @SerializedName("collaborator_no")          val collaboratorNo: Int = 0,
        @SerializedName("extra_hours")              val extraHOurs: Int = 0
){

}