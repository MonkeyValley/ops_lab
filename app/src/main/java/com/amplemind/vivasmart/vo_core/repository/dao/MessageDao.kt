package com.amplemind.vivasmart.vo_core.repository.dao

import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageDataModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import com.amplemind.vivasmart.vo_core.repository.responses.MessageResponse
import io.realm.Realm


open class MessageDao : RealmDao<MessageModel>(MessageModel::class.java) {

    override fun retrieveServerData(response: Any?): List<MessageModel>? {
        return when (response) {
            is MessageResponse ->
                response.data
            else -> null
        }
    }


    fun createMessage(data: MessageDataModel, _uuid: String, message: String, type: String, userId: Int, isForced: Boolean, isPrompt: Boolean): MessageModel {

        val realm = Realm.getDefaultInstance()

        val messageModel = MessageModel(
                id = 0,
                data = data,
                message = message,
                status = "new",
                type = type,
                userId = userId,
                isForced = isForced,
                isPrompt = isPrompt,
                uuid = _uuid
        )

        realm.beginTransaction()
        realm.insertOrUpdate(messageModel)
        realm.commitTransaction()

        realm.close()

        return messageModel
    }

    fun getCountMessages(): Int {
        var count = 0
        val realm = Realm.getDefaultInstance()

        val query = realm
                .where(MessageModel::class.java)
                .equalTo("status", "new")
                .and()
                .equalTo("type", "MESSAGE")

        count = query.findAll().count()

        realm.close()

        return count
    }

    fun getMessagesByType(type: String): List<MessageModel>? {
        val realm = Realm.getDefaultInstance()

        val query = realm
                .where(MessageModel::class.java)
                .equalTo("type", type)

        val count = query.findAllAsync()
        val results = realm.copyFromRealm(count)
        realm.close()
        return results
    }

    fun getMessagesByType_id(type: String, id:Int): List<MessageModel>? {
        val realm = Realm.getDefaultInstance()

        val query = realm
                .where(MessageModel::class.java)
                .equalTo("type", type)

        val count = query.findAllAsync()
        realm.close()
        return count.toList()
    }


}