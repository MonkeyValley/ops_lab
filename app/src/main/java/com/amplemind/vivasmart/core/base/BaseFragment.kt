package com.amplemind.vivasmart.core.base

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 6/29/18.
 */
abstract class BaseFragment : Fragment() {

    protected val subscriptions = AndroidDisposable()
    private var progressDialog: ProgressDialog? = null

    @Inject
    lateinit var mEventBus: EventBus

    private var mIsFragmentInitialized = false

    open var argumentsNeeded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (argumentsNeeded) {
            require( arguments != null) { "Arguments are required for ${this::class.java}" }

            arguments?.apply {
                readArguments(this)
            }
        }

    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context!!)
    }

    override fun onDetach() {

        if(this::class.simpleName.equals("ChooseActivityFragment")){
            sendSetToolbarTitleEvent("Seleccione un lote","Lotes")
        }else if(this::class.simpleName.equals("SelectionOfLotsFragment")){
            sendSetToolbarTitleEvent("","Producción")
        }

        super.onDetach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        subscriptions.dispose()
        mIsFragmentInitialized = false
    }

    fun hasInternet(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!mIsFragmentInitialized) {
            setUpCallbacks()
            setUpUI()
            setUpUICallbacks()
            loadData()
            mIsFragmentInitialized = true
        }
    }

    override fun onPause() {
        if (context != null) {
            hideKeyboard(context!!)
        }
        super.onPause()
    }

    protected fun showErrorDialog(error: Throwable) {
        showErrorDialog(error.localizedMessage)
    }

    protected fun showErrorDialog(msg: String) {
        onError(msg)
    }

    protected open fun onError(error: String) {
        AlertDialog.Builder(context!!)
                .setTitle(getString(R.string.app_name))
                .setMessage(error)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, i ->
                    dialogInterface.dismiss()
                }.show()

    }

    protected fun initProgressDialog() {
        if(progressDialog == null) {
            progressDialog = ProgressDialog(context)
            progressDialog!!.setTitle(getString(R.string.app_name))
            progressDialog!!.setMessage(getString(R.string.server_loading))
            progressDialog!!.setCancelable(false)
        }
    }

    protected fun showProgressDialog(show: Boolean) {
        initProgressDialog()
        if (show) progressDialog!!.show() else progressDialog!!.dismiss()
    }

    protected open fun readArguments(args: Bundle) {}
    protected open fun setUpUI() {}
    protected open fun setUpUICallbacks() {}
    protected open fun setUpCallbacks() {}
    protected open fun loadData() {}

    protected fun hideKeyboard() {

    }

    protected fun sendSetToolbarTitleEvent(subtitle: String, title: String? = null) {
        context?.apply {
            mEventBus.send(SetToolbarTitleEvent(this, title, subtitle))
        }
    }

    // Events
    class SetToolbarTitleEvent(
            val context: Context,
            val title: String? = null,
            val subtitle: String? = null
    )

}