package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.local.CollaboratorsLineDao
import com.amplemind.vivasmart.core.repository.local.PackingLineDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.response.ActivitiesResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class BoxCountLinesRepository @Inject constructor(val api: CountBoxApi, private val apiAuthorization: ApiAuthorization,
                                                  private val preferences: UserAppPreferences, val database: CollaboratorsLineDao,private val databaseLots: PackingLineDao) {

    fun getBoxCount(packing_line: Int): Observable<BoxCountResponse> {
        return api.getCountBox(apiAuthorization.getAuthToken(preferences.token), packingline_id = packing_line)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun sendCountBox(presentation: CountBoxRequest, line_packing: Int): Observable<Any> {
        return api.sendCountBox(apiAuthorization.getAuthToken(preferences.token), line_packing, presentation)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getBoxCountOffline(packing_line: Int): Observable<List<BoxCountResultResponse>> {
        return Observable.fromCallable {
            return@fromCallable database.getBoxCountOffline(packing_line)
        }
    }

    fun saveCountBoxLocal(data: CountBoxRequest, idLine: Int): Observable<Any> {
        return Observable.fromCallable {
            distributeCountingBox(idLine, data)

            data.presentation?.forEach { box ->
                database.updateCountingBox(box.id, box.total, idLine)
            }
        }
    }


    /**
     * For distribur the count of boxes it is necessary to take out the total count of presentations and the total count of the new presentations
     * if the previous count is equal to the new one, no logs are created
     * to create logs only collaborators are taken that are not by time and the new counting is divided among them
     */
    fun distributeCountingBox(id_line: Int, count_box: CountBoxRequest?, sign: String? = null, training: Boolean = false, practice: Boolean = false, registerId: Int = -1) {

        val last_count_box = database.getLocalCountBox(id_line)

        val collaborators = database.getLocalCollaboratorsInLine(id_line)

        val lotId = databaseLots.getLotByLine(id_line)

        count_box?.presentation?.forEach { presentation ->

            val last_total = last_count_box?.first { it.packing_id == presentation.id }

            val lastCount = last_total?.last_count ?: 0

            val totals = presentation.total

            if (lastCount != totals) {
                val totalCollaborators = collaborators.filter { it.is_time == false }.size

                val quantity = (totals - lastCount).toDouble() / totalCollaborators.toDouble()
                collaborators.forEach { item ->
                    if (item.is_time == false) {
                        var time = 0.0

                        if (item.packing_id == presentation.id) {
                            val time_total = database.getTimeInCountByCollaborator(id_line, item.id!!)

                            time = calculateTime(item) - time_total
                        }

                         database.createCountBoxLog(CountingBox(registerId = item.id!!, units = quantity, activity_id = item.activity_id!!, idLine = id_line, packing_id = presentation.id, lot_id = lotId, time = time))
                    }
                }
                createDetailRosterInLinesWithBox(id_line, sign, training, practice, registerId)
            } else {
                if (totals > 0 && registerId != -1) {
                    createDetailRosterInLinesWithBox(id_line, sign, training, practice, registerId, true)
                }
            }
        }
    }

    /**
     *  At the end of a collaborator,
     *  it is necessary to calculate the count of the box to search and find the records that already have registered
     */
    fun calculateUnitsTotalByCollaborator(id_line: Int, count_box: CountBox?, idRegister: Int): Pair<Double, Float> {

        var totalUnits = 0.0

        var totalMoney = 0f

        val logs = database.getTotalQuantityLogs(id_line, idRegister)

        logs.forEach {
            val packagin = database.getPresentation(it.packing_id).cost
            totalUnits += it.units
            totalMoney += (it.units * packagin).toFloat()
        }

        val last_count_box = database.getLocalCountBox(id_line)

        val totalCollaborators = database.getCountCollaboratorsInLine(id_line)

        count_box?.presentation?.forEach { presentation ->
            val last_total = last_count_box?.first { it.packing_id == presentation.id }

            val lastCount = last_total?.last_count ?: 0
            val totals = presentation.total

            if (lastCount != totals) {
                val quantity = (totals - lastCount).toDouble() / totalCollaborators.toDouble()

                val packagin = database.getPresentation(presentation.id).cost

                totalUnits += quantity
                totalMoney += (quantity * packagin).toFloat()
            }
        }

        return Pair(totalUnits, totalMoney)
    }

    fun updateCountingBox(packings: List<BoxCountResultResponse>, idLine: Int) {
        packings.forEach { box ->
            val exist = database.existPresentation(box.packing_id, idLine) ?: false

            if (!exist) {
                database.saveCountBox(BoxCountResultResponse(null, box.last_count, box.packing_id, box.packingline_id, box.packing))
            } else {
                database.updateCountingBox(box.packing_id, box.last_count, box.packingline_id)
            }
        }
    }

    fun createDetailRosterInLinesTime(idLine: Int, registerId: Int, sign: String?, training: Boolean, practice: Boolean) {

        val user = database.getCollaboratorByIdAndLine(registerId, idLine)

        var time = calculateTime(user)

        val training_user = if (user.id == registerId) training else user.isTraining
        val practice_user = if (user.id == registerId) practice else user.isPractice
        val sign_user = if (user.id == registerId) sign else null

        val lotId = databaseLots.getLotByLine(idLine)

        val packagin = if (user.packing_id != null) database.getPresentation(user.packing_id) else null

        if (packagin != null){
            val time_total = database.getTimeInCountByCollaborator(idLine, registerId)

            time -= time_total
        }

        val activity = mapActivityResponseByActivityModel(database.getActivity(user.activity_id))

        val detail = DetailtRosterLinesResponse(user.activity_id!!, user.collaborator_id!!, user.id!!, practice_user
                ?: false, training_user ?: false,0.0,0.0, user.packing_id,
                user.packingline_id, 0.0, 0, time, 0.0, sign_user, PackingLineCollaborator(isTime = user.is_time!!), Packing(cost = packagin?.cost
                ?: 0.0, name = packagin?.name ?: ""),
                activity, lotId)

        database.insertDetailRoster(detail)

        if (sign_user != null) {
            database.signAllDetailt(registerId, sign_user)
        }
    }

    private fun mapActivityResponseByActivityModel(activity: ActivitiesResponse): ActivitiesModel {
        val activityModel = ActivitiesModel(activity.id, activity.activityAreaId, activity.activityCategoryId,
                activity.activitySubCategoryId, activity.activityTypeId, activity.businessUnitId, activity.code, activity.cost,
                null, activity.costPerHour,activity.extraTimeCost,activity.practice_cost, false, activity.sunday_rate_value, true, "", "",
                activity.isActive, activity.isGrooves, activity.isRemovable, activity.name, activity.performance, activity.rangeFrom, activity.rangeTo,
                null, activity.unitId, activity.unitLimit, false)

        return activityModel
    }

    private fun calculateTime(collaborator: PackingLineCollaboratorsModel): Double {
        return if (collaborator.date > 0) {
            (collaborator.temp_time + ((System.currentTimeMillis() - collaborator.date) / 1000)).toDouble()
        } else {
            collaborator.temp_time.toDouble()
        }
    }


    private fun createDetailRosterInLinesWithBox(idLine: Int, sign: String?, training: Boolean, practice: Boolean, registerId: Int, onlyOne: Boolean = false) {

        if (onlyOne) {
            createDetailRosterInLinesTime(idLine, registerId, sign, training, practice)
            return
        }

        val collaborators = database.getLocalCollaboratorsInLine(idLine)

        collaborators.forEach { user ->
            if (user.is_time == false) {

                val countingBox = database.getTotalQuantityLogsGenerateReport(idLine, user.id!!)

                val activity = mapActivityResponseByActivityModel(database.getActivity(user.activity_id))

                countingBox.forEach {
                    val training_user = if (user.id == registerId) training else user.isTraining
                    val practice_user = if (user.id == registerId) practice else user.isPractice
                    val sign_user = if (user.id == registerId) sign else null

                    val packagin = database.getPresentation(it.packing_id)


                    val detail = DetailtRosterLinesResponse(user.activity_id!!, user.collaborator_id!!, user.id!!, practice_user
                            ?: false
                            , training_user
                            ?: false,0.0,0.0,it.packing_id, user.packingline_id, it.units, 0, it.time, 0.0, sign_user,
                            PackingLineCollaborator(isTime = user.is_time), Packing(cost = packagin.cost, name = packagin.name),
                            activity, it.lot_id)

                    database.insertDetailRoster(detail)
                    database.updateBoxGenerateReportSuccess(it.id!!)

                    if (sign != null) {
                        database.signAllDetailt(registerId, sign)
                    }
                }
            }
        }
    }


}
