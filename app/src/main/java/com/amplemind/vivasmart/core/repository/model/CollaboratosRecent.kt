package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
class CollaboratosRecent (
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long? = null,
    @ColumnInfo(name = "id_collaborator") var id_collaborator: Int,
    @ColumnInfo(name = "name") var name : String,
    @ColumnInfo(name = "image") var image : String,
    @ColumnInfo(name = "birthdate") var birthdate : String?,
    @ColumnInfo(name = "employee_code") var employee_code : String,
    @ColumnInfo(name = "date") var date : String
)
