package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.vo_core.repository.ReceptionQualityRevisionListRepository
import io.reactivex.Observable
import javax.inject.Inject

class ReceptionQualityRevisionListViewModel @Inject constructor(
        private val mRepository: ReceptionQualityRevisionListRepository
) {

    var carryOrderId: Long = -1

    var carryOrder: CarryOrderModel? = null

    fun loadCarryOrder(): Observable<CarryOrderModel> =
            mRepository.loadCarryOrder(carryOrderId)
                    .doOnNext {carryOrder ->
                        this.carryOrder = carryOrder
                    }

    fun loadRevisions(): Observable<List<ItemReceptionQualityRevisionViewModel>> =
            mRepository.loadRevisions(carryOrderId)
                    .map { revisions ->
                        revisions.map {revision ->
                            ItemReceptionQualityRevisionViewModel(revision)
                        }
                    }

}