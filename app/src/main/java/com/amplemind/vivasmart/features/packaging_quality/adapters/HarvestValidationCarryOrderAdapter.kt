package com.amplemind.vivasmart.features.packaging_quality.adapters

import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemValidationCarryOrderBinding
import com.amplemind.vivasmart.features.packaging_quality.fragment.HarvestValidationCarryOrderListFragment
import com.amplemind.vivasmart.features.packaging_quality.viewModel.HarvestValidationCarryOrderViewModel
import com.jakewharton.rxbinding2.view.RxView
import java.util.concurrent.TimeUnit

class HarvestValidationCarryOrderAdapter(
        private val mEventBus: EventBus
) : RecyclerView.Adapter<HarvestValidationCarryOrderAdapter.HarvestValidationCarryOrderViewHolder>() {

    private var mItems = listOf<HarvestValidationCarryOrderViewModel>()

    override fun onViewRecycled(holder: HarvestValidationCarryOrderViewHolder) {
        super.onViewRecycled(holder)
        holder.cleanUp()
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): HarvestValidationCarryOrderViewHolder {
        val binding = ItemValidationCarryOrderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HarvestValidationCarryOrderViewHolder(binding)
    }

    override fun getItemCount(): Int = mItems.size

    override fun onBindViewHolder(viewHolder: HarvestValidationCarryOrderViewHolder, position: Int) {
        viewHolder.bind(mItems[position])
    }

    fun setItems(items: List<HarvestValidationCarryOrderViewModel>) {
        mItems = items
        notifyDataSetChanged()
    }

    inner class HarvestValidationCarryOrderViewHolder(
            private val binding: ItemValidationCarryOrderBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        private val subscriptions = AndroidDisposable()

        fun cleanUp() {
            subscriptions.dispose()
        }

        fun bind(viewModel: HarvestValidationCarryOrderViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()

            ViewCompat.setElevation(binding.cvItem, 10.0f)
            ViewCompat.setElevation(binding.ivNotOkay, 15.0f)

            binding.ivNotOkay.setImageResource(
                    if (viewModel.isHarvestQualityOk)
                            R.drawable.ic_correct
                        else
                            R.drawable.ic_warning_red_24dp
            )

            RxView.clicks(binding.root)
                    .throttleFirst(2, TimeUnit.SECONDS)
                    .subscribe {
                        mEventBus.send(HarvestValidationCarryOrderListFragment.OnCarryOrderClickedEvent(viewModel.carryOrder))
                    }
                    .addTo(subscriptions)
        }
    }

}