package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.remote.FertirriegoApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmObject
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LotFertirriegoDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mLotFertirriegoApi: FertirriegoApi,
        private val mPref: UserAppPreferences
) : RealmDownloader<RealmObject?, LotFertirriegoModel>(realmHolder, LotFertirriegoModel::class.java) {

    override fun buildServerRequest(parent: RealmObject?, lastVersion: Long): Single<List<LotFertirriegoModel>> =
            mLotFertirriegoApi.getLotFertirriego(
                mPref.authorizationToken
        ).map { response ->
            Log.e("Data-LotFertirriego: ", response.data.toString())

        getLotFromFertirriego(response.data)

            response.data
        }

    fun getLotFromFertirriego(data: List<LotFertirriegoModel>) {
        for(item in data){

            for(soil in item.soilList!!){
                getLotFromFertirriego(soil.lotId, "Soil")
                getValvesPerWeek(soil.lotId)
                getPHCEPerWeek(soil.lotId)
            }

            for(hydroponic in item.hydroponicList!!){
                getLotFromFertirriego(hydroponic.lotId, "hydroponic")
                getPHCEPerWeek(hydroponic.lotId)
                getPulsePerWeek(hydroponic.lotId)
            }

        }
    }

    fun getLotFromFertirriego(lotId: Int, type: String): Disposable =
            getValvesForFertirriego(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        insertValves(it, lotId, type)
                    }, {
                        Log.e("ValvesPerFertirriego", "error", it)
                    })

    fun getValvesForFertirriego(lotId: Int): Single<ValvesPerLotFertirriegoModel> =
            mLotFertirriegoApi.getValveLotFertirriego(
                    mPref.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

    fun insertValves(item : ValvesPerLotFertirriegoModel?, lotId: Int, type: String) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.type = type
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }
    }

    fun getValvesPerWeek(lotId: Int): Disposable =
            getInfoMonitoringPerLot(lotId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    insertValvesMonitoringPerWeek(it, lotId)
                }, {
                    Log.e("ValvesPerWeekFer", "error", it)
                })

    fun getInfoMonitoringPerLot(lotId: Int): Single<ValvesPerWeekFertirriegoModel> =
            mLotFertirriegoApi.getValesPerWeekFertirriego(
                    mPref.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

    fun insertValvesMonitoringPerWeek(item : ValvesPerWeekFertirriegoModel?, lotId: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }
    }

    fun getPHCEPerWeek(lotId: Int): Disposable =
            getInfoPHCEPerLot(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        insertValvesMonitoringPerWeek(it, lotId)
                    }, {
                        Log.e("PHCEPerWeekFer", "error", it)
                    })

    fun getInfoPHCEPerLot(lotId: Int): Single<PHCEPerWeekFertirriegoModel> =
            mLotFertirriegoApi.getPHCEPerWeekFertirriego(
                    mPref.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

    fun insertValvesMonitoringPerWeek(item : PHCEPerWeekFertirriegoModel?, lotId: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }
    }


    fun getPulsePerWeek(lotId: Int): Disposable =
            getInfoMonitoringPulsePerLot(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        insertPulseMonitoringPerWeek(it, lotId)
                    }, {
                        Log.e("ValvesPerWeekFer", "error", it)
                    })

    fun getInfoMonitoringPulsePerLot(lotId: Int): Single<PulsesPerWeekFertirriegoModel> =
            mLotFertirriegoApi.getPulsePerWeekFertirriego(
                    mPref.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

    fun insertPulseMonitoringPerWeek(item : PulsesPerWeekFertirriegoModel?, lotId: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }
    }

}