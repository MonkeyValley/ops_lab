package com.amplemind.vivasmart.features.reciba_quality.model

import androidx.room.Entity
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.google.gson.annotations.SerializedName

@Entity
data class IssuesModel(
        @SerializedName("id") var issue_id: Int,
        @SerializedName("name") var name: String,
        @SerializedName("description") var description: String,
        @SerializedName("is_condition") var isCondition: Boolean? = true,
        @SerializedName("image") var image: String
)