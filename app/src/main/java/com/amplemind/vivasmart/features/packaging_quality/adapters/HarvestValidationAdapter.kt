package com.amplemind.vivasmart.features.packaging_quality.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.databinding.ItemHarvestValidationCriteriaBinding
import com.amplemind.vivasmart.features.packaging_quality.viewModel.HarvestQualitySettingViewModel
import com.jakewharton.rxbinding3.widget.checkedChanges

class HarvestValidationAdapter : RecyclerView.Adapter<HarvestValidationAdapter.HarvestValidationViewHolder>() {

    private var mItems = listOf<HarvestQualitySettingViewModel>()

    var readOnly: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): HarvestValidationViewHolder {
        val binding = ItemHarvestValidationCriteriaBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HarvestValidationViewHolder(binding)
    }

    override fun getItemCount(): Int = mItems.size

    override fun onViewRecycled(holder: HarvestValidationViewHolder) {
        super.onViewRecycled(holder)
        holder.cleanUp()
    }

    override fun onBindViewHolder(viewHolder: HarvestValidationViewHolder, position: Int) {
        viewHolder.bind(mItems[position])
    }

    fun setItems(items: List<HarvestQualitySettingViewModel>) {
        mItems = items
        notifyDataSetChanged()
    }

    fun getSettings() = mItems

    inner class HarvestValidationViewHolder(
            private val binding: ItemHarvestValidationCriteriaBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        private val subscriptions = AndroidDisposable()

        fun cleanUp() {
            subscriptions.dispose()
        }

        fun bind(viewModel: HarvestQualitySettingViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()

            binding.swCriteria.isClickable = !readOnly

            if (!readOnly) {
                binding.swCriteria.checkedChanges()
                        .skipInitialValue()
                        .subscribe{ isChecked ->
                            viewModel.isOk.set(isChecked)
                        }
                        .addTo(subscriptions)
            }
        }
    }
}