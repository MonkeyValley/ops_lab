package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import androidx.lifecycle.LiveData
import com.amplemind.vivasmart.core.repository.GroovesRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.vo_core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getActiveTime
import com.amplemind.vivasmart.vo_core.repository.models.extensions.realCost
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_features.custom.CustomSeekBar
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import java.util.*
import javax.inject.Inject

class TimerViewModel @Inject constructor(
        var repository: ActivityCodeRepository,
        var uploadRepository: ProfileRepository,
        var groovesRepository: GroovesRepository,
        var picturesHelper: PicturesHelper) {

    private val requestFail = BehaviorSubject.create<Pair<String, Int?>>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val isAllPause = BehaviorSubject.create<Boolean>()
    private val isAllRestart = BehaviorSubject.create<Boolean>()
    private val successUpload = BehaviorSubject.create<List<Any>>()
    private val successFinishCollaborator = BehaviorSubject.create<Int>()
    private val successFinishAllCollaborators = BehaviorSubject.create<Boolean>()
    private val deleteCollaborator = BehaviorSubject.create<Int>()
    private val missingUnits = BehaviorSubject.create<Boolean>()
    private val successSendUnit = BehaviorSubject.create<Int>()
    //private val activities = BehaviorSubject.create<List<TimerUserViewModel>>()

    var activityLogs: List<ActivityLogModel> = listOf()

    var seekState = CustomSeekBar.State.PAUSED
        private set

    /*
    * Behavior Observables
    * they are used to notify the view about any change
    * */
    private val seekStateControl = BehaviorSubject.create<CustomSeekBar.State>()

    private lateinit var mCollaborators: LiveData<Changes<ActivityLogModel>>

    fun setSeekContolState(position: Int) {
        when (position) {
            0 -> {
                seekStateControl.onNext(seekState)
            }
            2 -> {
                seekStateControl.onNext(CustomSeekBar.State.FINISH)
            }
        }
    }

    fun pauseTimers() {
        seekState = CustomSeekBar.State.CONTINUE
    }

    fun restartTimers() {
        seekState = CustomSeekBar.State.PAUSED
    }

    fun startTimer(activityLog: ActivityLogModel): Observable<ActivityLogModel?> {
        return repository.startTimer(activityLog)
    }

    fun startTimerForAll(activityLogs: List<ActivityLogModel> = this.activityLogs): Observable<List<ActivityLogModel>?> {
        return repository.startTimer(activityLogs)
    }

    fun pauseTimer(activityLog: ActivityLogModel): Observable<ActivityLogModel?> {
        return repository.pauseTimer(activityLog)
    }

    fun pauseTimerForAll(activityLogs: List<ActivityLogModel> = this.activityLogs): Observable<List<ActivityLogModel>?> {
        return repository.pauseTimer(activityLogs)
    }


    fun hasCollaborators(): Boolean {
        return activityLogs.isNotEmpty()
    }

    fun getActivityCost(activity: ActivityModel): String {
        return "$ %.2f por:".format(Locale.US, activity.realCost)
    }

    fun getSpacesCount(activityCode: ActivityCodeModel): Observable<String> {

        return repository.calculateUnitTotals(activityCode)
                .map {total ->
                    return@map "Estado: $total"
                }

    }

    fun thereAreLogsWithTime(): Boolean {
        return activityLogs.any{ it.getActiveTime() > 0 }
    }

    fun thereAreLogsWorking(): Boolean {
        return activityLogs.any{ !it.isPaused }
    }

    fun loadActiveCollaborators(activityCode: ActivityCodeModel, currentData: MutableList<ActivityLogRecyclerViewModel> = mutableListOf()): Observable<Changes<ActivityLogModel>>? {
        return repository.loadUndone(activityCode)
                .doOnNext{ changes ->
                    activityLogs = changes.data
                }
    }

    fun finish(activityLog: ActivityLogModel, sign: File? = null): Observable<ActivityLogModel?> {
        return repository.finish(activityLog, sign)
    }

    fun finishAll(activityLogs: List<ActivityLogModel> = this.activityLogs): Observable<List<ActivityLogModel>?> {
        return repository.finish(activityLogs)
    }

    fun deleteCollaborator(activityLog: ActivityLogModel): Observable<ActivityLogModel?> {
        return repository.deleteActivityLog(activityLog)
    }

    fun setUnits(activityLog: ActivityLogModel, quantity: Int): Observable<ActivityLogModel?> {
        return repository.setUnits(activityLog, quantity)
    }

    fun setUnits(activityLog: ActivityLogModel, goodQuantity: Int, badQuantity: Int): Observable<ActivityLogModel?> {
        return repository.setUnits(activityLog, goodQuantity, badQuantity)
    }

    fun canAssignUnits(activityLog: ActivityLogModel): Boolean {
        return !activityLog.isPaused
    }

    fun canFinish(activityLog: ActivityLogModel): Boolean {
        return activityLog.quantity > 0
    }


    //region BehaviorSubject
    fun getSeekStateControl(): BehaviorSubject<CustomSeekBar.State> {
        return seekStateControl
    }

    fun onSuccessUpload(): BehaviorSubject<List<Any>> {
        return successUpload
    }

    fun onSuccessFinishCollaborator(): BehaviorSubject<Int> {
        return successFinishCollaborator
    }

    fun onSuccessFinishAllCollaborators(): BehaviorSubject<Boolean> {
        return successFinishAllCollaborators
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<Pair<String, Int?>> {
        return requestFail
    }

    fun onDeleteCollaborator(): BehaviorSubject<Int> {
        return deleteCollaborator
    }

    fun onMissingUnits(): BehaviorSubject<Boolean> {
        return missingUnits
    }

}


