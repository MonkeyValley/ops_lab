package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class IssuesByCategoryResponse(@SerializedName("data") val data: List<IssuesResultResponse>)

@Entity
data class IssuesResultResponse(@PrimaryKey val id: Int?,
                                val is_active: Boolean?,
                                val name: String?,
                                val count : Int?,
                                var categoryId: Int = 0,
                                var stageId: Int = 0)