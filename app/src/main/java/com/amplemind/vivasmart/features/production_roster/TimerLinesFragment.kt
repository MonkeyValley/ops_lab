package com.amplemind.vivasmart.features.production_roster

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.*
import com.amplemind.vivasmart.core.navegation.data_mediator.Colleague
import com.amplemind.vivasmart.core.navegation.data_mediator.MediatorNavigation
import com.amplemind.vivasmart.core.repository.model.FinishCollaboratorResult
import com.amplemind.vivasmart.core.repository.model.HeaderModel
import com.amplemind.vivasmart.core.repository.response.CollaboratorFinishCostResponse
import com.amplemind.vivasmart.core.utils.*
import com.amplemind.vivasmart.features.collaborators.AddCollaboratorsActivity
import com.amplemind.vivasmart.features.collaborators.PresentationForCollaboratorActivity
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity.Companion.ADD_COLLABORATOR
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity.Companion.FINISH_COLLABORATOR
import com.amplemind.vivasmart.features.production_roster.adapters.RecyclerItemTouchHelper
import com.amplemind.vivasmart.features.production_roster.adapters.TimerManagerUsersSectionAdapter
import com.amplemind.vivasmart.features.production_roster.adapters.TimerUserViewHolder
import com.amplemind.vivasmart.features.production_roster.dialogs.CollaboratorsActionsDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.FinalizeUserTimerDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.PauseTimerDialog
import com.amplemind.vivasmart.features.production_roster.fragments.interfaces.ControlTimerManager
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserSectionViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerViewModel
import com.amplemind.vivasmart.features.scannerbarcode.ReadBarcodeActivity
import com.thoughtbot.expandablerecyclerview.listeners.GroupExpandCollapseListener
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import kotlinx.android.synthetic.main.fragment_timer.*
import javax.inject.Inject

class TimerLinesFragment : BaseFragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, Colleague {

    @Inject
    lateinit var viewModel: TimerLinesViewModel

    @Inject
    lateinit var mediatorNavigation: MediatorNavigation

    @Inject
    lateinit var picturesHelper: PicturesHelper

    private var listener : ICountHasBox? = null

    companion object {
        val TAG = TimerLinesFragment::class.java.simpleName
        fun newInstance(name_line: String, lot_id: Int, listener : ICountHasBox): TimerLinesFragment {
            val fragment = TimerLinesFragment()
            val bundle = Bundle()
            bundle.putString("name_line", name_line)
            bundle.putInt("lot_id", lot_id)
            fragment.arguments = bundle
            fragment.setListener(listener)
            return fragment
        }
    }

    fun setListener(listener: ICountHasBox) {
        this.listener = listener
    }

    private fun getNameLine(): String {
        return arguments?.getString("name_line", "") ?: ""
    }

    private fun getLotId(): Int {
        return arguments?.getInt("lot_id", -1) ?: -1
    }

    override fun onPause() {
        super.onPause()
        viewModel.getTimesByCollaborators()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mediatorNavigation.addColleague(this)

        viewModel.getInformationToFinishCollaborator().subscribe(this::showAlertFinishCollaborator).addTo(subscriptions)
        viewModel.onFinishAllCollaborator().subscribe(this::onFinishAllCollaborator).addTo(subscriptions)
        viewModel.onSuccessUpload().subscribe(this::finishCollaboratorSign).addTo(subscriptions)
        viewModel.finishCollaboratorSuccess().subscribe(this::finishCollaboratorResult).addTo(subscriptions)
        viewModel.getSectionsCollaborators().subscribe(this::setData).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.deleteResponse().subscribe(this::onDeleteCollaborator).addTo(subscriptions)
        viewModel.addMoreCollaboratorsSucess().subscribe(this::onRefreshAdapter).addTo(subscriptions)
        viewModel.getHaveCount().subscribe(this::showCountActivity).addTo(subscriptions)
        viewModel.getshowCountActivity().subscribe(this::AddCollaboratorsResult).addTo(subscriptions)

        LocalBroadcastManager.getInstance(context!!).registerReceiver(onRefreshCollaborators, IntentFilter(LOCAL_BROADCAST_USER_PACKING_RECEIVED))
        LocalBroadcastManager.getInstance(context!!).registerReceiver(userReceived, IntentFilter(LOCAL_BROADCAST_FINISH_ACTIVITY))
    }

    fun isAllPause(type : Int) {
        viewModel.showCountBoxActivity(hasInternet(), type).addTo(subscriptions)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_timer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupEvents()
        setupRecyclerView()

        getLoadCollaborators()
    }

    private fun getLoadCollaborators() {
        if (hasInternet()) {
            viewModel.loadCollaborators().addTo(subscriptions)
        } else {
            loadCollaboratorsOffline(true)
        }
    }

    private fun loadCollaboratorsOffline(show: Boolean) {
        if (show) {
            main_collaboratorview.snackWithoutInternetMessage()
            viewModel.getOfflineCollaborators().addTo(subscriptions)
        }
    }

    private fun onFinishAllCollaborator(result: Boolean) {
        viewModel.removeAllCollaborator()
        (rv_timer.adapter as ControlTimerManager).onNotifyDataSetChange()
        showButtonStart()
        viewModel.showCountBox()
    }

    private fun showCountActivity(result : Pair<Boolean, Int>){
        listener?.showCountBox(result)
    }

    private val onRefreshCollaborators = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                LOCAL_BROADCAST_USER_PACKING_RECEIVED -> {
                    viewModel.addCollaborators(
                            intent.getParcelableArrayListExtra(COLLABORATORS),
                            hasInternet(),
                            intent.getBooleanExtra(HAS_BOXCOUNT, false))
                }
            }
        }
    }

    private val userReceived = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                LOCAL_BROADCAST_FINISH_ACTIVITY -> finishLine()
            }
        }
    }

    fun finishLine() {
        if (context != null) {
            (context as AppCompatActivity).setResult(Activity.RESULT_OK)
            (context as AppCompatActivity).finish()
        }
    }

    private fun onRefreshAdapter(result: Boolean) {
        rv_timer.adapter = null
        setData(viewModel.getData())
    }

    /**
     *  in this method you can see only listener register
     */
    private fun setupEvents() {
        //seekBar.getSeekStateControl().subscribe(this::seekBarEvent).addTo(subscriptions)
        rv_timer.touchHelper(this)
        btn_start.addThrottle().subscribe(this::onStartTimer).addTo(subscriptions)
    }

    private fun setupRecyclerView() {
        rv_timer.hasFixedSize()
        rv_timer.layoutManager = LinearLayoutManager(context)
        rv_timer.itemAnimator = DefaultItemAnimator()
    }

    private fun setData(sections: MutableList<TimerUserSectionViewModel>) {
        if (activity != null && sections.isNotEmpty()) {
            val adapter = TimerManagerUsersSectionAdapter(activity!!, activity!!.supportFragmentManager, sections)
            rv_timer.adapter = adapter
            adapter.setOnGroupExpandCollapseListener(object : GroupExpandCollapseListener {
                override fun onGroupCollapsed(group: ExpandableGroup<*>?) {
                    (group as TimerUserSectionViewModel).groupCollapsed()
                }

                override fun onGroupExpanded(group: ExpandableGroup<*>?) {
                    (group as TimerUserSectionViewModel).groupExpanded()
                }
            })

            (activity as TimerLinesActivity).hideArrow()

            if (viewModel.allCollaboratorHaveTime()) {
                showTimerControl()
            }

            adapter.onClickAddCollaborators().subscribe(this::addCollaborators).addTo(subscriptions)
            adapter.isCleanListSubject().subscribe(this::cleanList).addTo(subscriptions)
            adapter.onDeleteLine().subscribe(this::deleteLineOffline).addTo(subscriptions)
        }
    }

    private fun cleanList(clean: Boolean) {
        if (clean) {
            showButtonStart()
        }
    }

    private fun deleteLineOffline(idLine: Int) {
        viewModel.deleteLineOffline(idLine).addTo(subscriptions)
    }

    private fun showButtonStart() {
        (activity as TimerLinesActivity).showArrow()
        btn_start.visibility = View.VISIBLE
        seekBarContainer.visibility = View.GONE
    }

    private fun showTimerControl() {
        btn_start.visibility = View.GONE
        seekBarContainer.visibility = View.VISIBLE
    }

    //region Result events viewmodel

    /**
     *  response delete collaborator event
     */
    private fun onDeleteCollaborator(position: Int) {
        (rv_timer.adapter as ControlTimerManager).onRemoveItem(position)
        viewModel.showCountBox()
    }

    //endregion

    //region events in collaborators
    private fun addCollaborators(section: HeaderModel) {
        if (section.collaborator_left == 0) {
            main_collaboratorview.snack("La actividad no permite agregar mas colaboradores")
            return
        }
        viewModel.showCountBoxActivity(hasInternet(),ADD_COLLABORATOR, section).addTo(subscriptions)
    }

    private fun AddCollaboratorsResult(result : Pair<Boolean, HeaderModel>){
        val show = result.first
        val section = result.second
        if (show) {
            startActivity(Intent(context!!, BoxCountLinesActivity::class.java)
                    .putExtra("type", ADD_COLLABORATOR)
                    .putExtra("name_line", getNameLine())
                    .putExtra("lot_id", getLotId())
                    .putExtra("section", section))
        } else {
            if (hasInternet()) {
                startActivity(Intent(context!!, AddCollaboratorsActivity::class.java)
                        .putExtra("onlySelected", true)
                        .putExtra("section", section))
            } else {
                startActivity(Intent(context!!, ReadBarcodeActivity::class.java)
                        .putExtra("showActivities", true)
                        .putExtra("section", section))
            }
        }
    }

    private fun onStartTimer(any: Any) {
        if (viewModel.isSectionVisible()) {
            (rv_timer.adapter as ControlTimerManager).onStartGeneralTimer()
            viewModel.startAllCollaborators(hasInternet()).addTo(subscriptions)
            showTimerControl()
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is TimerUserViewHolder) {
            when (direction) {
                ItemTouchHelper.LEFT -> {
                    CollaboratorsActionsDialog.showAlertAction(context!!, getString(R.string.delete), getString(R.string.delete_user_msg), {
                        viewModel.deleteCollaborator(hasInternet(), viewHolder.registerId!!, viewHolder.idLineCollaboratorOnline, position).addTo(subscriptions)
                    }, {
                        (rv_timer.adapter as ControlTimerManager).onNotifyItemChanged(position)
                    })
                }
                ItemTouchHelper.RIGHT -> {
                    if (viewHolder.packing_id != null) {
                        startActivityForResult(Intent(context, BoxCountLinesActivity::class.java)
                                .putExtra("type", FINISH_COLLABORATOR)
                                .putExtra("registerId", viewHolder.registerId)
                                .putExtra("position", position)
                                .putExtra("name_line", getNameLine())
                                .putExtra("lot_id", getLotId()),
                                COUNT_BOX_REQUEST)
                    } else {
                        viewModel.getTotalsBeforeFinishCollaborator(hasInternet(), viewHolder.registerId!!, position, false)
                    }
                    (rv_timer.adapter as ControlTimerManager).onNotifyItemChanged(position)
                }
            }
        }
    }

    private fun showAlertFinishCollaborator(result: CollaboratorFinishCostResponse) {
        val user = viewModel.getUser(result.registerID)
        val practiceCost = viewModel.getUserPaymentInPractice(result.practice_cost, user)
        val trainingEnabled = result.training_cost != null && result.training_cost > 0
        val practiceEnabled = result.practice_cost != null && result.practice_cost > 0


        val dialog = FinalizeUserTimerDialog.newInstance(getString(R.string.finish_activity_title), getString(R.string.finish_mjs),
                user.name!!, user.getElapsedTime(), viewModel.getUserPayment(result), viewModel.getUnits(result.unit_total), true, getNameLine(),
                viewModel.getUserPaymentInTraining(result.training_cost, user), practiceCost, trainingEnabled, name_line = viewModel.getNameUnit(user.registerId),
                isPractice = practiceEnabled)

        dialog.isCancelable = false
        dialog.show(activity?.supportFragmentManager!!, FinalizeUserTimerDialog.TAG)
        /*
        dialog.setOnCompleteListener(object : OnFinalizeUserListener {
            override fun completeSign(sign: Bitmap, isTraining: Boolean, isPractice: Boolean) {
                val image = picturesHelper.createImageFile(context!!)
                picturesHelper.compressImage(picturesHelper.createBitmapToFile(context!!, sign), image, 400f, 400f)
                if (hasInternet()) {
                    mViewModel.uploadSign(user.registerId!!, user.time, image, isTraining, result.position, isPractice, result.isCountBox).addTo(subscriptions)
                } else {
                    mViewModel.finishSignOfflineCollaborators(user.registerId!!, user.time, isTraining, result.position, image, isPractice, result.isCountBox)
                }
            }

            override fun skipSignature(isTraining: Boolean, isPractice: Boolean) {
                (rv_timer.adapter as ControlTimerManager).onNotifyItemChanged(result.position)
                if (hasInternet()) {
                    mViewModel.finishCollaboratorSignature(user.registerId!!, user.time, isTraining, result.position, null, isPractice, result.isCountBox).addTo(subscriptions)
                } else {
                    mViewModel.finishSignOfflineCollaborators(user.registerId!!, user.time, isTraining, result.position, null, isPractice, result.isCountBox)
                }
            }

            override fun cancel() {
                (rv_timer.adapter as ControlTimerManager).onNotifyItemChanged(result.position)
            }
        })
        */
    }

    override fun onClick(item: TimerUserViewModel, position: Int) {
        if (context != null) {
            if (item.packing_id != null) {
                startActivityForResult(Intent(context, PresentationForCollaboratorActivity::class.java)
                        .putExtra("registerId", item.registerId)
                        .putExtra("name", item.name)
                        .putExtra("list_collaborators", intArrayOf(item.getCollaboratorId()))
                        .putExtra("curren_presentation", item.name_presentation)
                        .putExtra("activity_id", item.getActivityCode()), COUNT_BOX_REQUEST)
            }
        }
    }

    private fun finishCollaboratorSign(results: FinishCollaboratorResult) {
        viewModel.finishCollaboratorSignature(results.registerId, results.time, results.training, results.position, results.relative_url, results.isPractice, results.isBox).addTo(subscriptions)
    }

    private fun finishCollaboratorResult(result: Pair<Int, Int>) {
        (rv_timer.adapter as ControlTimerManager).onRemoveItem(result.second)
        viewModel.showCountBox()
    }

    override fun startTimer(item: TimerUserViewModel, position: Int) {
        viewModel.startCollaborator(hasInternet(), item.registerId!!, position).addTo(subscriptions)
        if (viewModel.allTimerAreRunning()) {
            showTimerControl()
        }
    }

    override fun pauseTimer(item: TimerUserViewModel, position: Int) {
        viewModel.pauseCollaborator(hasInternet(), item.registerId!!, item.time, position).addTo(subscriptions)
    }

    private fun pauseAlertDialog() {
        val dialog = PauseTimerDialog().newInstance()
        dialog.isCancelable = false
        dialog.show(activity!!.supportFragmentManager, PauseTimerDialog.TAG)
        dialog.setListener(object : PauseTimerDialog.OnPauseTimerListenner {
            override fun pauseTimer(type: Int) {
                dialog.dismiss()

                viewModel.pauseAllCollaborators(hasInternet()).addTo(subscriptions)
                seekBar.pauseTimers()
                (rv_timer.adapter as ControlTimerManager).onPauseGeneralTimer()
                tv_left_seek_control.text = getString(R.string.mcontinue)
            }
        })
    }

    private fun continueTimerCollaborator() {
        viewModel.startAllCollaborators(hasInternet()).addTo(subscriptions)
        seekBar.restartTimers()
        tv_left_seek_control.text = getString(R.string.pause)
        (rv_timer.adapter as ControlTimerManager).onStartGeneralTimer()

    }

    private fun finishAllCollaborators() {
        CollaboratorsActionsDialog.showAlertAction(context!!, getString(R.string.finalize), getString(R.string.finalize_users_msg), acceptAction = {
            if (viewModel.getShowCountBox()) {
                startActivityForResult(Intent(context, BoxCountLinesActivity::class.java)
                        .putExtra("type", FINISH_COLLABORATOR)
                        .putExtra("name_line", getNameLine())
                        .putExtra("lot_id", getLotId())
                        .putExtra("position", -1)
                        , COUNT_BOX_REQUEST)
            } else {
                viewModel.finishAllCollaborators(hasInternet()).addTo(subscriptions)
            }
        }, cancelAction = {})
    }

    /**
     *  Events that are detonated through the seekbar
     */
    private fun seekBarEvent(state: TimerViewModel.SeekControlState) {
        when (state) {
            TimerViewModel.SeekControlState.PAUSED -> {
                pauseAlertDialog()
            }
            TimerViewModel.SeekControlState.CONTINUE -> {
                continueTimerCollaborator()
            }
            TimerViewModel.SeekControlState.FINISH -> {
                finishAllCollaborators()
            }
        }
    }
    //endregion

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == COUNT_BOX_REQUEST) {
            if (data != null) {
                when (resultCode) {
                    BoxCountLinesActivity.BOX_DONE -> {
                        viewModel.getTotalsBeforeFinishCollaborator(hasInternet(), data.getIntExtra("registerId", -1), data.getIntExtra("position", -1), true).addTo(subscriptions)
                    }
                    BoxCountLinesActivity.BOX_ALL_DONE -> {
                        viewModel.finishAllCollaboratorWithCountBox(hasInternet()).addTo(subscriptions)
                    }
                    BoxCountLinesActivity.CHANGE_PRESENTATION -> {
                        viewModel.changePresentation(data.getIntExtra("registerId", -1), data.getStringExtra("name_presentation"))
                    }
                    else -> {
                        (rv_timer.adapter as ControlTimerManager).onRemoveItem(data.getIntExtra("position", -1))
                        viewModel.showCountBox()
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.removeCountBox()
        mediatorNavigation.removeColleague(this)
    }
}



