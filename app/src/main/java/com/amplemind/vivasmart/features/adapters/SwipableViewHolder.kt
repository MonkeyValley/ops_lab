package com.amplemind.vivasmart.features.adapters

import android.view.View

//Abstract class for swipable/draggable items
interface SwipableViewHolder {

    val viewForeground: View?
    val viewDelete: View?
    val viewFinish: View?

}
