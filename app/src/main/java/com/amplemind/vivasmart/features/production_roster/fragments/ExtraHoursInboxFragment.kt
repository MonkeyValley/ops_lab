package com.amplemind.vivasmart.features.production_roster.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_roster.ExtraHoursRequestsActivity
import com.amplemind.vivasmart.features.production_roster.adapters.ExtraHoursMessagesAdapter
import com.amplemind.vivasmart.vo_core.repository.dao.MessageDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import com.amplemind.vivasmart.vo_core.utils.TYPE_EXTRA_TIME

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ExtraHoursInboxFragment : BaseFragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var recycler: RecyclerView
    private lateinit var noIncidents: TextView
    private lateinit var mMessages: List<MessageModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_extra_hours_inbox, container, false)
        recycler = view.findViewById(R.id.extraInbox_rcv_request) as RecyclerView
        noIncidents = view.findViewById(R.id.no_incidents) as TextView
        mMessages = MessageDao().getMessagesByType("EXTRA_TIME_REQUEST")!!
        if (mMessages.isNotEmpty()) {
            initRecycler()
            noIncidents.visibility = View.GONE
        } else {
            noIncidents.visibility = View.VISIBLE
        }
        return view
    }

    private fun initRecycler() {
        recycler.hasFixedSize()
        recycler.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.adapter = ExtraHoursMessagesAdapter(mMessages!!)
        (recycler.adapter as ExtraHoursMessagesAdapter).onclickItem().subscribe(this::clickItem).addTo(subscriptions)
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    fun clickItem(item: MessageModel) {
         when (item.type) {
            TYPE_EXTRA_TIME -> {
                val fragment: Fragment = ExtraHoursAuthRequest.newInstanceAuthorize(item.data!!)
                changeFragment(fragment)
            }

        }



    }

    private fun retrieveIntenForType(type: String): Intent? {
        return when (type) {
            TYPE_EXTRA_TIME -> Intent(activity, ExtraHoursRequestsActivity::class.java)
            else -> null
        }
    }

    private fun changeFragment(fragment: Fragment) {
        val manager = fragmentManager
        val transaction = manager!!.beginTransaction()
        transaction.replace(R.id.ExtraHourMessage_container, fragment)
        transaction.commit()
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ExtraHoursInboxFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
