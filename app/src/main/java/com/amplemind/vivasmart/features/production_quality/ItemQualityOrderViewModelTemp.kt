package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.model.IssuesModelTemp
import com.amplemind.vivasmart.core.repository.model.QuantityOrderModelTemp

class ItemQualityOrderViewModelTemp(item : IssuesModelTemp) {

    val name = item.name
    val numberIssue = item.numberIssue
    val percentage = item.percentage

}
