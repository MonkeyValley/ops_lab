package com.amplemind.vivasmart.vo_features.production_roster.helpers

import android.os.Handler
import android.os.Looper
import android.util.Log
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getActiveTime
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.remote.utils.ReactiveHandlerThread
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ActivityLogTimer @Inject constructor (
        private val mActivityLogsDao: ActivityLogsDao
){

    companion object {
        private val TAG = ActivityLogTimer::class.java.simpleName
        private val THREAD_NAME = "$TAG.Thread"
    }

    private lateinit var mThread: ReactiveHandlerThread
    private val mSubscriptions = AndroidDisposable()

    fun setUp() {
        cleanUp()
        mThread = ReactiveHandlerThread(THREAD_NAME)
        mThread.start()
    }

    fun cleanUp() {
        if (::mThread.isInitialized && mThread.isAlive) {
            Handler(mThread.looper).post {
                mSubscriptions.dispose()
                mActivityLogsDao.cleanUp()
            }
            mThread.quitSafely()
        }
    }

    fun forActivityCode(activityCodeUuid: String, onNext: (String) -> Unit, onError: ((Throwable) -> Unit)? = null) {
        mThread.onStatusChanged
                .filter { status ->
                    status == ReactiveHandlerThread.Status.READY
                }
                .switchMap {
                    Observable.defer {
                        updateTimers(activityCodeUuid)
                                .flatMapIterable { activityLogUuids ->
                                    activityLogUuids
                                }
                    }
                            .subscribeOn(mThread.scheduler)
                            .observeOn(AndroidSchedulers.mainThread())
                }
                .run {
                    if (onError == null) {
                        subscribe(onNext)
                    }
                    else {
                        subscribe(onNext, onError)
                    }
                }
                .addTo(mSubscriptions)
    }


    private fun updateTimers(activityCodeUuid: String): Observable<List<String>> =
            mActivityLogsDao.findActiveLogs(activityCodeUuid)
                    .map { changes ->
                        Log.e(TAG, "Is main thread: ${Looper.getMainLooper() == Looper.myLooper()}")
                        changes.data
                    }
                    .filter { activityLogs ->
                        activityLogs.isNotEmpty()
                    }
                    .switchMap { activityLogs ->
                        Observable.fromCallable {
                            calculateNextUpdates(activityLogs)
                        }
                        .delay {nextUpdates ->
                            Observable.just(1)
                                    .delay(nextUpdates.remainingTime, TimeUnit.SECONDS, mThread.scheduler)
                        }
                        .repeatUntil { activityLogs.isEmpty() }
                    }
                    .map { nextUpdate ->
                        nextUpdate.activityLogUuids
                    }

    private fun calculateNextUpdates(activityLogs: List<ActivityLogModel>): NextUpdate {
        Log.e("calculateNextUpdates", activityLogs.size.toString())
        var smallestRemainingSeconds = Long.MAX_VALUE

        val nextUpdates = mutableListOf<String>()

        activityLogs.forEach { activityLog ->
            if (activityLog.isValid && !activityLog.isPaused) {
                val currentTime = activityLog.getActiveTime()

                val remainingSeconds = 60 - (currentTime % 60)

                if (remainingSeconds < smallestRemainingSeconds) {
                    smallestRemainingSeconds = remainingSeconds
                    nextUpdates.clear()
                    nextUpdates.add(activityLog.uuid)
                } else if (remainingSeconds == smallestRemainingSeconds) {
                    nextUpdates.add(activityLog.uuid)
                }
            }
        }

        if (smallestRemainingSeconds != Long.MAX_VALUE) {
            Log.i(TimerThread.TAG, "Next timers update in $smallestRemainingSeconds seconds...")
        }

        return NextUpdate(nextUpdates, smallestRemainingSeconds)
    }

    private data class NextUpdate (
            val activityLogUuids: List<String>,
            val remainingTime: Long
    )

}