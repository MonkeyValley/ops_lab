package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import kotlinx.android.synthetic.main.content_quality_order_activity.*
import javax.inject.Inject

class QualityOrderActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: QualityOrderViewModel

    @Inject
    lateinit var picturesHelper: PicturesHelper

    enum class QualityOrderFlow(val type: Int) {
        READ(1),
        EVALUATION(2);

        companion object {
            fun from(value: Int): QualityOrderFlow = QualityOrderFlow.values().first { it.type == value }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_quality_order_activity)

        setupToolbar()
        setupRecycler()

        addSubscribers()

        name_lot.text = getNameLot().toUpperCase()
    }

    private fun getIdCarry() = intent.getIntExtra("id_carry", -1)
    private fun getPosition() = intent.getIntExtra("position", -1)

    private fun isReadFlow() : Boolean {
        return intent.getIntExtra("flow", -1) == QualityOrderActivity.QualityOrderFlow.READ.type
    }

    private fun getNameLot() : String {
        return intent.getStringExtra("name_lot") ?: ""
    }

    private fun setupRecycler() {
        rv_order.layoutManager = LinearLayoutManager(this)
        rv_order.itemAnimator = DefaultItemAnimator()
        rv_order.adapter = QualityOrderAdapter(isReadFlow())
    }

    private fun setData(issues: List<IssuesItemViewModel>) {
        (rv_order.adapter as QualityOrderAdapter).setData(issues)
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getIssuesList().subscribe(this::setData).addTo(subscriptions)
        viewModel.signSendSuccess().subscribe(this::sendDataReport).addTo(subscriptions)
        viewModel.sendReportSuccess().subscribe(this::sendReportComplete).addTo(subscriptions)
        btn_send.addThrottle().subscribe(this::sendReport).addTo(subscriptions)

        btn_send.visibility = if(isReadFlow()) View.GONE else View.VISIBLE

        if (isReadFlow()){
            viewModel.getCarryReadDetail(getIdCarry()).addTo(subscriptions)
        }else{
            viewModel.getCarryOrderDetail(getIdCarry()).addTo(subscriptions)
        }
    }

    private fun sendReport(any: Any) {
        val dialog = SignatureDialogFragment().newInstance("Verificar reporte", "Esta seguro de querer enviar éste reporte? Por favor firme para confirmar", true,
                roles = arrayOf("Supervisor sr de calidad"), mode = 0)
        dialog.show(supportFragmentManager, SignatureDialogFragment.TAG)
        dialog.setOnCompleteListener(object : OnSignatureCompleteListener {
            override fun onDismiss() {
                hideKeyboard(dialog.context!!)
            }

            override fun completeSign(sign1: Bitmap, sign2: Bitmap?) {
                val image = picturesHelper.createImageFile(dialog.context!!)
                picturesHelper.compressImage(picturesHelper.createBitmapToFile(dialog.context!!, sign1), image, 400f, 400f)
                viewModel.sendReportCarrySign(image).addTo(subscriptions)
            }
        })
    }

    private fun sendDataReport(sign: String) {
        viewModel.sendReportDataCarry(sign, getIdCarry()).addTo(subscriptions)
    }

    private fun sendReportComplete(result: Boolean) {
        if (result) {
            val intentData = Intent()
            intentData.putExtra("position", getPosition())
            setResult(Activity.RESULT_OK, intentData)
            finish()
        }
    }

    /**
     *  change title in toolbar
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_report_order
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "R. Orden de acarreo"
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
