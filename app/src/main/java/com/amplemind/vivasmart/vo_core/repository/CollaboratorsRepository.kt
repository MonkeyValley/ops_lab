package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.realm.CollaboratorsDao
import javax.inject.Inject

class CollaboratorsRepository @Inject constructor(
        private val collaboratorsDao: CollaboratorsDao
) : Repository() {

    override fun cleanUp() {
        collaboratorsDao.cleanUp()
    }

    fun find(query: String = "") =
            collaboratorsDao.findWithCodeQuery(query)

    fun findByActivity(query: String = "") =
            collaboratorsDao.findActivityWithCodeQuery(query)

    fun findByUuidAndCodeQuery(uuids: List<String>, query: String = "") =
            collaboratorsDao.findByUuidAndCodeQuery(uuids, query)

    fun findActivityByUuidAndCodeQuery(uuids: List<String>, query: String = "") =
            collaboratorsDao.findActivityByUuidAndCodeQuery(uuids, query)

}