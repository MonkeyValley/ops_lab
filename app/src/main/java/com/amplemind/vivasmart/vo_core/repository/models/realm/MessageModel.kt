package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class MessageModel(@PrimaryKey @SerializedName("id")         var id: Int? = 0,
                        @SerializedName("user_id")                var userId: Int? = 0,
                        @SerializedName("type")                   var type: String ="",
                        @SerializedName("status")                 var status: String? ="",
                        @SerializedName("message")                var message: String ="",
                        @SerializedName("data")                   var data: MessageDataModel?= null,
                        @SerializedName("uuid")                   var uuid: String = "",
                        @SerializedName("is_prompt")              var isPrompt: Boolean = false,
                        @SerializedName("is_forced")              var isForced: Boolean = false,
                        @SerializedName("created_at")             var createdAt: String = "",
                        @SerializedName("version")                var version: Int = 0,
                        @SerializedName("receiver_id")            var receiverId: Int = 0


) : RealmObject(), Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readParcelable(MessageDataModel::class.java.classLoader),
            parcel.readString()!!,
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeValue(userId)
        parcel.writeString(type)
        parcel.writeString(status)
        parcel.writeString(message)
        parcel.writeParcelable(data, flags)
        parcel.writeString(uuid)
        parcel.writeByte(if (isPrompt) 1 else 0)
        parcel.writeByte(if (isForced) 1 else 0)
        parcel.writeString(createdAt)
        parcel.writeInt(version)
        parcel.writeInt(receiverId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MessageModel> {
        override fun createFromParcel(parcel: Parcel): MessageModel {
            return MessageModel(parcel)
        }

        override fun newArray(size: Int): Array<MessageModel?> {
            return arrayOfNulls(size)
        }
    }
}