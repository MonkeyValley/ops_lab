package com.amplemind.vivasmart.features.reciba_quality

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.SharedPreferencesUtils
import com.amplemind.vivasmart.core.utils.USER_ID
import com.amplemind.vivasmart.features.production_quality.UnitsViewModel
import com.amplemind.vivasmart.features.reciba_quality.RecibaQualityReviewDetailActivity.Companion.sharedPref
import com.amplemind.vivasmart.features.reciba_quality.model.UnitItem
import com.amplemind.vivasmart.features.reciba_quality.model.VariatyItem
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ReacibaQualityReviewViewModel
import com.amplemind.vivasmart.features.reciba_quality.viewModel.RecibaQualityReviewViewModel
import com.amplemind.vivasmart.features.reciba_quality.viewModel.VarietiesViewModel
import com.google.gson.Gson
import com.jakewharton.rxbinding2.widget.text
import kotlinx.android.synthetic.main.activity_reciba_quality_review.*
import kotlinx.android.synthetic.main.activity_report_quality_finished_product.toolbar_report_quality_finished
import kotlinx.android.synthetic.main.categories_toolbar.*
import javax.inject.Inject


class RecibaQualityReviewActivity : BaseActivity(), AdapterView.OnItemSelectedListener, View.OnClickListener{

    @Inject
    lateinit var viewModel: RecibaQualityReviewViewModel
    @Inject
    lateinit var preferences: UserAppPreferences
    lateinit var progressDialogInner: ProgressDialog
    lateinit var sharedPref: SharedPreferencesUtils

    var spinnerUnit: Spinner? = null
    var spinnerVariety: Spinner? = null
    var tvLotName: TextView? = null
    var tvTable: TextView? = null
    var edCarrySize: EditText? = null
    var etSampleSize: EditText? = null
    var etCutWeek: EditText? =  null
    var btnCheck: Button? = null
    val listUnits = ArrayList<UnitItem>()
    val listVarieties = ArrayList<VariatyItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reciba_quality_review)
        sharedPref = SharedPreferencesUtils(this)

        setUpUi()
        getArgument()
        addSubscribers()
        setDialogLoadingInner()
        setupToolbar()
        getCatalogos()
    }

    private fun setUpUi(){
        spinnerUnit = findViewById(R.id.spinner_unit_reciba_quality)
        spinnerVariety = findViewById(R.id.spinner_variety_reciba_quality)
        tvLotName = findViewById(R.id.tv_lot_name)
        tvTable = findViewById(R.id.tv_table)
        edCarrySize = findViewById(R.id.ed_carry_size)
        etSampleSize = findViewById(R.id.et_sample_size)
        etCutWeek = findViewById(R.id.et_cut_week)

        btnCheck = findViewById(R.id.btn_check)

        spinnerUnit!!.onItemSelectedListener = this
        spinnerVariety!!.onItemSelectedListener = this

        btnCheck!!.setOnClickListener { view -> onClick(view) }
    }

    private fun getArgument(){
        val args = intent.extras
        viewModel.carryOrderId = args!!.getString("id", "0")
        tvLotName!!.text = args!!.getString("lotName", "")
        tvTable!!.text = "Tabla(s): " + args!!.getString("table", "")
    }

    fun ifConected(): Boolean{
        return if(hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

    fun getCatalogos() {
        if(ifConected()) {
            showProgressDialogInner(true)
            viewModel.getUnits()
        }
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_report_quality_finished
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.reciba_quality)
        toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_units().subscribe(this::updateList_units).addTo(subscriptions)
        viewModel.onSuccessRequest_varieties().subscribe(this::updateList_varieties).addTo(subscriptions)
    }

    private fun updateList_units(list: List<UnitsViewModel>) {
        listUnits.clear()

        //listUnits.add( UnitItem(0, 0,"SELECCIONAR", "SELECCIONAR"))

        var firtItem: UnitItem? = null

        for (item in list) {
            var obj  =  UnitItem(item.id, item.business_unit_id!!, item.name, item.business_unit_names)
            if(item.name == "Piezas") firtItem = obj
            listUnits.add(obj)
        }

        if(firtItem != null) {
            listUnits.remove(firtItem)
            listUnits.add(0, firtItem)
        }

        spinner_unit_reciba_quality?.adapter = ArrayAdapter<UnitItem>(this, R.layout.spinner_item_textview, listUnits )
    }

    private fun updateList_varieties(list: List<VarietiesViewModel>) {
        listVarieties.clear()

        edCarrySize!!.setText(viewModel.validBoxNo.toString())

        listVarieties.add(VariatyItem(0, "SELECCIONAR"))

        for (item in list) {
            var obj  =  VariatyItem(item.id, item.name)
            listVarieties.add(obj)
        }
        spinner_variety_reciba_quality?.adapter = ArrayAdapter<VariatyItem>(this, R.layout.spinner_item_textview, listVarieties )
        showProgressDialogInner(false)
    }

    fun setDialogLoadingInner(){
        progressDialogInner = ProgressDialog(this)
        progressDialogInner.setTitle(getString(R.string.app_name))
        progressDialogInner.setMessage(getString(R.string.server_loading))
        progressDialogInner.setCancelable(false)
    }

    private fun showProgressDialogInner(show: Boolean) {
        if (show) progressDialogInner.show() else progressDialogInner.dismiss()
    }


    override fun onItemSelected(adapter: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (adapter!!.id) {
            R.id.spinner_unit_reciba_quality ->{
                viewModel.unitId = listUnits[position].id!!
            }
            R.id.spinner_variety_reciba_quality ->{
                viewModel.varietyId = if (position != 0) listVarieties[position].id!! else 0
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        println("notSelected")
    }

    override fun onClick(view: View?) {
        if(validateData()){
            if(ifConected()) {

                var objFinishedProd = ReacibaQualityReviewViewModel(
                        viewModel.carryOrderId,
                        etSampleSize!!.text.toString().toInt(),
                        viewModel.unitId,
                        Gson().fromJson(preferences.userInfo, UserModel::class.java).id,
                        0,
                        viewModel.varietyId,
                        null,
                        etCutWeek!!.text.toString().toInt(),
                        null,
                        null)
                sharedPref.saveObjectToSharedPreference("recibaQualityReviewOjb", objFinishedProd)

                intent = Intent(this, RecibaQualityReviewDetailActivity::class.java)
                intent.putExtra("newReport", true)
                intent.putExtra("cropId", viewModel.cropId)
                startActivityForResult(intent, 200)

                finish()
            }
        }
    }

    private fun validateData(): Boolean {
        /*if(spinnerUnit!!.selectedItemId.toInt() >= 0 ) {
            showSnackBar("Se debe de selecccionar la unidad correspondiente")
            return false
        }*/

        if( spinnerVariety!!.selectedItemId.toInt() == 0) {
            showSnackBar("Se debe de seleccionar la variedad correspondiente")
            return false
        }

        if(etSampleSize!!.text.toString() != "" && etCutWeek!!.text.toString() != ""){
            if(etSampleSize!!.text.toString().toDouble() <= 0.0) {
                showSnackBar("El campo de tamaño de la muestra debe ser mayor que 0.")
                return false
            }
            if(etCutWeek!!.text.toString().toDouble() <= 0.0) {
                showSnackBar("El campo de semana de corte debe ser mayor que 0.")
                return false
            }
            return true
        } else {
            showSnackBar("Todos las variables son requeridas y no deben estar vacias.")
            return false
        }
    }

    fun showSnackBar(msj: String){
        val snackbar = Snackbar.make(findViewById(android.R.id.content), msj,
                Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text)
                .setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        snackbar.show()
    }

}
