package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

data class CarryOrderReportModel(
                                 @SerializedName("created_at") val date: String,
                                 @SerializedName("folio") val folio: String,
                                 @SerializedName("box_no") val boxNo: Float,
                                 @SerializedName("valid_box_no") val validBoxNo: Float,
                                 @SerializedName("reception") val reception: Boolean
)