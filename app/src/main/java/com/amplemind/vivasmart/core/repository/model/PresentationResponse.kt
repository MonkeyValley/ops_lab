package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class PresentationResponse(@SerializedName("data") val presentations: List<PresentationResultResponse>)

@Entity
data class PresentationResultResponse(
        @SerializedName("business_unit_id") val business_unit_id: Int,
        @SerializedName("code") val code: String,
        @SerializedName("complexity_level") val complexity_level: Int,
        @SerializedName("crop_id") val crop_id: Int,
        @SerializedName("cost") val cost: Double,
        @SerializedName("id") @ColumnInfo(name = "id_presentation") @PrimaryKey(autoGenerate = true) val id: Int,
        @ColumnInfo(name = "presentation_image") @SerializedName("image") val image: String? = null,
        @SerializedName("is_active") val is_active: Boolean,
        @SerializedName("is_premium") val is_premium: Boolean,
        @SerializedName("material") val material: String,
        @ColumnInfo(name = "presentation_name") @SerializedName("name") val name: String,
        @SerializedName("time") val time: Int,
        @SerializedName("weight") val weight: Double
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readDouble()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(business_unit_id)
        parcel.writeString(code)
        parcel.writeInt(complexity_level)
        parcel.writeInt(crop_id)
        parcel.writeDouble(cost)
        parcel.writeInt(id)
        parcel.writeString(image)
        parcel.writeByte(if (is_active) 1 else 0)
        parcel.writeByte(if (is_premium) 1 else 0)
        parcel.writeString(material)
        parcel.writeString(name)
        parcel.writeInt(time)
        parcel.writeDouble(weight)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PresentationResultResponse> {
        override fun createFromParcel(parcel: Parcel): PresentationResultResponse {
            return PresentationResultResponse(parcel)
        }

        override fun newArray(size: Int): Array<PresentationResultResponse?> {
            return arrayOfNulls(size)
        }
    }
}