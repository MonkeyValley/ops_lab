package com.amplemind.vivasmart.features.quality_control

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.onChange
import com.amplemind.vivasmart.databinding.ItemDetailControlQualityBinding
import com.amplemind.vivasmart.databinding.ItemIssueNoteBinding
import com.amplemind.vivasmart.databinding.ItemNoIssuesBinding
import com.amplemind.vivasmart.features.quality_control.viewModel.IssueViewModel

class DetailControlQualityAdapter constructor(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    private var list = listOf<IssueViewModel>()

    private val ISSUE = 1
    private val ISSUE_NOTE = 2
    private val NO_ISSUE = 3

    fun addElements(data: List<IssueViewModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ISSUE) {
            val binding = ItemDetailControlQualityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            CategoriesDetailViewHolder(binding)
        } else if(viewType == ISSUE_NOTE){
            val binding = ItemIssueNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            CategoriesDetailNoteViewHolder(binding)
        }else{
            val binding = ItemNoIssuesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            CategoriesDetailNoIssueViewHolder(binding)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            list[position].isNote -> ISSUE_NOTE
            list[position].image_no_found -> NO_ISSUE
            else -> ISSUE
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val typeView = holder.itemViewType
        when (typeView) {
            ISSUE -> {
                (holder as CategoriesDetailViewHolder).bind(list[position])
            }
            ISSUE_NOTE -> {
                (holder as CategoriesDetailNoteViewHolder).bind(list[position])
            }
            NO_ISSUE -> {
                (holder as CategoriesDetailNoIssueViewHolder).bind(list[position])
            }
        }
    }

    inner class CategoriesDetailViewHolder(private val binding: ItemDetailControlQualityBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: IssueViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            binding.switch1.setOnCheckedChangeListener { button, isChecked ->
                item.switchEnabled = isChecked
            }
        }
    }

    inner class CategoriesDetailNoteViewHolder(private val binding: ItemIssueNoteBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: IssueViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (item.isNote) {
                binding.edOptional.onChange { item.comment = it }
            }
        }
    }

    inner class CategoriesDetailNoIssueViewHolder(private val binding: ItemNoIssuesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: IssueViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
        }
    }


}