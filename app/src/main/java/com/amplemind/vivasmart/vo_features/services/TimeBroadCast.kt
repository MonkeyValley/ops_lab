package com.amplemind.vivasmart.vo_features.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.ACTIVITY_CONTEXT
import com.amplemind.vivasmart.features.notification.viewModel.InboxViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.DailyPayModel
import io.realm.Realm
import kotlinx.android.synthetic.main.alert_message_dialog.view.*
import java.util.*
import javax.inject.Inject

class TimeBroadCast : BroadcastReceiver() {

    @Inject
    lateinit var viewModel: InboxViewModel

    companion object {
        private val TAG = TimeBroadCast::class.java.simpleName
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context?, intent: Intent?) {
        var action = intent!!.action
        Log.e("action", action)

        when (action) {
            Intent.ACTION_TIME_TICK -> {

                val calendar: Calendar = Calendar.getInstance()
                val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
                val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
                var date = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

                if(validateDailyPay(date)) {
                    val hour: Int = calendar.get(Calendar.HOUR_OF_DAY)
                    val minutes: Int = calendar.get(Calendar.MINUTE)

                    Log.e("hour", hour.toString())
                    Log.e("minutes", minutes.toString())

                    if (hour == 14 && minutes == 30) {

                        Toast.makeText(context!!, "Mensaje entrante...", Toast.LENGTH_SHORT).show()

                        writeLine("Solo quedan 30 minutos para finalizar a los " +
                                "colaboradores de pago diario, recuerda finalizarlos antes de las 3:00 de la " +
                                "tarde. \n")
                    }
                }
            }

            Intent.ACTION_TIME_CHANGED -> {
                Log.e("Reloj", "No modifiques el hora")
            }

            Intent.ACTION_DATE_CHANGED -> {
                Log.e("Fecha", "No modifiques la fecha")
            }
        }

    }

    fun validateDailyPay(date: String): Boolean {
        Realm.getDefaultInstance().use {
            return  it.where(DailyPayModel::class.java)
                    .equalTo("date", date)
                    .findFirst() != null
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun writeLine(message: String) {
        val mDialogView = LayoutInflater.from(ACTIVITY_CONTEXT).inflate(R.layout.alert_message_dialog, null)

        mDialogView.extraInbox_layout_header.visibility = View.GONE
        mDialogView.tv_display_message.text = message

        android.app.AlertDialog.Builder(ACTIVITY_CONTEXT)
                .setView(mDialogView)
                .setCancelable(false)
                .setPositiveButton("Cerrar") { dialogInterface, i ->
                    dialogInterface.dismiss()
                }.show()
    }

}