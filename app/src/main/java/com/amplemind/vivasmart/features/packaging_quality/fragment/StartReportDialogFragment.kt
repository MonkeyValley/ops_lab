package com.amplemind.vivasmart.features.packaging_quality.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.ToggleButton
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment

/**
 * Created by
 *          amplemind on 7/9/18.
 */
class StartReportDialogFragment : BaseDialogFragment(), CompoundButton.OnCheckedChangeListener {

    companion object {
        val TAG = StartReportDialogFragment::class.java.simpleName!!
    }

    private var toggle_total : ToggleButton? = null
    private var toggle_piece : ToggleButton? = null

    fun newInstance(): StartReportDialogFragment {
        return StartReportDialogFragment()
    }

    private var eventListener : OnStartReportListenner? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_start_orden_dialog_fragment, null, false)
        builder.setView(mView)

        toggle_total = mView.findViewById(R.id.btn_total)

        toggle_piece = mView.findViewById(R.id.btn_piece)

        toggle_total!!.setOnCheckedChangeListener(this)
        toggle_piece!!.setOnCheckedChangeListener(this)


        builder.setPositiveButton("INICIAR", null)
        builder.setNegativeButton("Cancelar") { _, _ -> dismiss() }

        val create = builder.create()

        create.setOnShowListener { _ ->
            create.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setOnClickListener {
                        if (toggle_total!!.isChecked) {
                            eventListener!!.startReportTotal()
                        }
                        if (toggle_piece!!.isChecked) {
                            eventListener!!.startReportPiece()
                        }
                        val snackbar = Snackbar.make(mView, "Seleccione un elemento", Snackbar.LENGTH_SHORT)
                        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                        snackbar.show()
                    }

        }

        return create
    }


    override fun onCheckedChanged(view: CompoundButton?, isChecked: Boolean) {
        when (view!!.id) {
            R.id.btn_total -> {
                if (isChecked) {
                    toggle_piece!!.isChecked = false
                }
            }

            R.id.btn_piece -> {
                if (isChecked) {
                    toggle_total!!.isChecked = false
                }
            }
        }
    }

    fun setListener(listener : OnStartReportListenner){
        eventListener = listener
    }

    interface OnStartReportListenner {
        fun startReportTotal()
        fun startReportPiece()
    }



}