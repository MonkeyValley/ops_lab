package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PendingRequestModel(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long? = null,
        @ColumnInfo(name = "type") var type: Int,
        @ColumnInfo(name = "table_primary_key") var tablePrimaryKey: Long
)