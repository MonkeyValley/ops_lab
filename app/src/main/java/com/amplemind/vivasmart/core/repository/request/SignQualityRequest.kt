package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

data class SignQualityRequest(@SerializedName("quality_sign") val qualitySign: String,
                              @SerializedName("supervisor_quality_sign") val supervisorQuality: String)