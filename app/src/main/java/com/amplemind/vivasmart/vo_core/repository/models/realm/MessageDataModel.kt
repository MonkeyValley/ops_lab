package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class MessageDataModel (@SerializedName("id")                         var id: Int? = 0,
                             @SerializedName("activity_id")                var activityId: Int = 0,
                             @SerializedName("collaborator_no")            var collaboratorNo: Int = 0,
                             @SerializedName("extra_hours")                var extraHours: Int = 0,
                             @SerializedName("activity_name")              var activity_name: String = "",
                             @SerializedName("activity_icon")              var activity_icon: String = "",
                             @SerializedName("stage_id")                   var stage_id: Int = 0,
                             @SerializedName("stage_name")                 var stage_name: String ="",
                             @SerializedName("lot_id")                     var lot_id: Int? = 0,
                             @SerializedName("message_id")                 var message_id: Int? = 0,
                             @SerializedName("notification_type_id")       var notification_type_id: Int? = 0

) : RealmObject(), Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeInt(activityId)
        parcel.writeInt(collaboratorNo)
        parcel.writeInt(extraHours)
        parcel.writeString(activity_name)
        parcel.writeString(activity_icon)
        parcel.writeInt(stage_id)
        parcel.writeString(stage_name)
        parcel.writeInt(lot_id!!)
        parcel.writeInt(message_id!!)
        parcel.writeInt(notification_type_id!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MessageDataModel> {
        override fun createFromParcel(parcel: Parcel): MessageDataModel {
            return MessageDataModel(parcel)
        }

        override fun newArray(size: Int): Array<MessageDataModel?> {
            return arrayOfNulls(size)
        }
    }
}


