package com.amplemind.vivasmart.features.root_quality.viewModel

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.root_quality.remote.rootQualityApi
import com.amplemind.vivasmart.features.root_quality.sirvice.RootQualityService
import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RootQualityViewModel @Inject constructor(
        private val uploadRepository: ProfileRepository,
        private val service: RootQualityService,
        private val api: rootQualityApi,
        private val apiErrors: HandleApiErrors) {

    @Inject
    lateinit var authorization: ApiAuthorization

    private val progressStatus = BehaviorSubject.create<Boolean>()

    private val requestFail = BehaviorSubject.create<String>()

    private val successUploadImageUrl = BehaviorSubject.create<String>()

    private val successGetDataRootQuality = BehaviorSubject.create<List<rootQualityModel>>()

    fun setServiceContect(context: Context){
        service.context = context
        service.preferences  = UserAppPreferences(context, null, null)
    }

    fun getTableCount(lotId: Int): Int {
        return service.getTableCount(lotId)
    }

    @SuppressLint("CheckResult")
    fun uploadImage(file: File, type: String) {
        uploadRepository.uploadImage(file, type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    successUploadImageUrl.onNext(it.relative_url)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }
    fun onSuccessUpdateImageUrl(): BehaviorSubject<String> { return successUploadImageUrl }

    fun getRootQualityData(lotId: Int): Disposable =
            getRootQualityWeek(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        onSuccessGetDataRootQuality().onNext(it)
                    }, {
                        Log.e("getRootQualityData", "error", it)
                    })

    fun getRootQualityWeek(lotId: Int): Single<List<rootQualityModel>> =
            api.getRootQualityWeek(
                    authorization.getAuthToken(service.preferences.token),
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.tables
                    }
    fun onSuccessGetDataRootQuality(): BehaviorSubject<List<rootQualityModel>> { return successGetDataRootQuality }

    fun saveRootQuality(lotId: Int, table: Int, imageUrl: String, comment: String, quality: Int){
        service.saveRootQuality(lotId, table, imageUrl, comment, quality)
    }

}

data class ObjectResponseRootQuality<T: Any> (
        @SerializedName("tables")         val tables: List<rootQualityModel>
)

data class rootQualityModel(
        @SerializedName("comment") val comment: String? = null,
        @SerializedName("date") val date: String? = null,
        @SerializedName("image") val image: String? = null,
        @SerializedName("lot_id") val lotId: Int? = null,
        @SerializedName("quality") val quality: Int? = null,
        @SerializedName("table") val table: Int? = null
)