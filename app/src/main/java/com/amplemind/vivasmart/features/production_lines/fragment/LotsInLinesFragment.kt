package com.amplemind.vivasmart.features.production_lines.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.snackWithoutInternetMessage
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.utils.FRAGMENT_LINES
import com.amplemind.vivasmart.features.production_lines.adapter.LotsInLinesAdapter
import com.amplemind.vivasmart.features.production_lines.viewModel.ItemLotPackageViewModel
import com.amplemind.vivasmart.features.production_lines.viewModel.LotsInLinesViewModel
import com.amplemind.vivasmart.features.production_roster.ProductionRosterActivitiesActivity
import com.amplemind.vivasmart.features.production_roster.RosterLinesFragment
import com.amplemind.vivasmart.features.production_roster.TimerLinesActivity
import kotlinx.android.synthetic.main.lots_in_lines_fragment.*
import javax.inject.Inject

class LotsInLinesFragment : BaseFragment() {

    companion object {
        fun newInstance(item: String): LotsInLinesFragment {
            val fragment = LotsInLinesFragment()
            val args = Bundle()
            args.putString("name_line", item)
            fragment.arguments = args
            return fragment
        }

        val TAG = LotsInLinesFragment::class.java.simpleName
    }

    @Inject
    lateinit var mediator: Mediator

    private fun getNameLine(): String {
        return arguments?.getString("name_line", "") ?: ""
    }

    @Inject
    lateinit var viewModel: LotsInLinesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.lots_in_lines_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = getString(R.string.title_lot_fragment)

        setupRecycler()

        viewModel.getLots().subscribe(this::setData).addTo(subscriptions)

        loadLots()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onLoadLocalData().subscribe(this::loadLocalLots).addTo(subscriptions)
        viewModel.onSuccessAssingLot().subscribe(this::onAssingSuccess).addTo(subscriptions)
    }

    private fun loadLots() {
        if (hasInternet()) {
            viewModel.loadLots().addTo(subscriptions)
        } else {
            loadLocalLots(true)
        }
    }

    private fun loadLocalLots(show: Boolean) {
        if (show) {
            view_lots.snackWithoutInternetMessage()
            viewModel.getLocalLots().addTo(subscriptions)
        }
    }

    private fun setData(data: List<ItemLotPackageViewModel>) {
        if (data.isEmpty()){
            ll_list_empty.visibility = View.VISIBLE
        }
        (rv_lots_lines.adapter as LotsInLinesAdapter).addElements(data)
    }

    private fun setupRecycler() {
        val adapter = LotsInLinesAdapter()
        rv_lots_lines.itemAnimator = DefaultItemAnimator()
        rv_lots_lines.adapter = adapter

        adapter.setEvents(object : OnEvents {
            override fun onClickEvent(item: ItemLotPackageViewModel) {
                onClick(item)
            }
        })
    }

    private fun onClick(item: ItemLotPackageViewModel) {
        viewModel.assignLot(hasInternet(), item.stage_id).addTo(subscriptions)
    }

    private fun onAssingSuccess(stageID: Int) {
        mediator.bundleItemActivitiesPayroll = null
        startActivityForResult(Intent(activity, TimerLinesActivity::class.java)
                .putExtra("name_line", getNameLine())
                .putExtra("line_id", mediator.bundleItemLinesPackage)
                .putExtra("lot_id", stageID),
                FRAGMENT_LINES)

        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = getString(R.string.title_lines_fragment)
        fragmentManager?.beginTransaction()?.remove(this)?.commit()
        fragmentManager?.popBackStack()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            callLinesFragment()
        }
    }

    private fun callLinesFragment() {
        (activity as AppCompatActivity)
                .supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                .replace(R.id.container_payroll, RosterLinesFragment.newInstance())
                .addToBackStack(RosterLinesFragment.TAG)
                .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = getString(R.string.title_lines_fragment)
    }

    interface OnEvents {
        fun onClickEvent(item: ItemLotPackageViewModel)
    }

}
