package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import javax.inject.Inject

class ProcessRosterRepository @Inject constructor(
        private val activityLogsDao: ActivityLogsDao
): Repository() {

    override fun cleanUp() {
        activityLogsDao.cleanUp()
    }

    fun getPendingLogs(stageUuid: String, stageTypeId: Int) =
            activityLogsDao.groupUnsignedLogs(stageUuid, stageTypeId)

}