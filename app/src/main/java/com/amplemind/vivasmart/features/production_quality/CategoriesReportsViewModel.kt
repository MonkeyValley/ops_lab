package com.amplemind.vivasmart.features.production_quality

import androidx.databinding.ObservableInt
import android.graphics.Bitmap
import android.view.View
import com.amplemind.vivasmart.core.repository.ProductionQualityRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.TableGrooves
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import java.util.*
import javax.inject.Inject

class CategoriesReportsViewModel@Inject constructor(private val repository : ProductionQualityRepository,
                                                    private val uploadRepository: ProfileRepository,
                                                    private val preferences: UserAppPreferences,
                                                    private val apiErrors: HandleApiErrors) {

    private var imageFilePath: String? = null
    var image: Bitmap? = null
    private lateinit var tables: List<SpinnerTablesViewModel>

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val requestSuccess = BehaviorSubject.create<Boolean>()
    private val successUploadImage = BehaviorSubject.create<String>()
    private val requestFail = BehaviorSubject.create<String>()

    enum class REPORTS_VALIDATION(val msg: String) {
        ERROR_IMAGE("La fotografía es requerida"),
        ERROR_GROVES_NUM("Número de surcos es requerido"),
        ERROR_GROVES_MAX("Número invalido de surcos"),
        ERROR_TABLE("La tabla es requerida"),
        ERROR_DESCRIPTION("Comentarios requeridos"),
        SUCCESS("")
    }

    fun setupTables(tables: List<TableGrooves>) {
        this.tables = tables.map { SpinnerTablesViewModel(it) }
    }

    fun setImageReport(path: String?) {
        imageFilePath = path
    }

    fun setImageBitmap(bitmap: Bitmap?) {
        image = bitmap
    }

    fun validateForm(description: String, tableNo: Int, grooveNum: String): REPORTS_VALIDATION {
        if (tableNo <= 0) {
            return REPORTS_VALIDATION.ERROR_TABLE
        }
        if (grooveNum.isEmpty() || grooveNum.toIntOrNull() == null) {
            return REPORTS_VALIDATION.ERROR_GROVES_NUM
        }
        if (grooveNum.toInt() > tables[tableNo - 1].getMaxGroves() || grooveNum.toInt() <= 0) {
            return REPORTS_VALIDATION.ERROR_GROVES_MAX
        }
        if(image == null) {
            return REPORTS_VALIDATION.ERROR_IMAGE
        }
        if(description.isEmpty()) {
            return REPORTS_VALIDATION.ERROR_DESCRIPTION
        }
        return REPORTS_VALIDATION.SUCCESS
    }

    fun getImagePath(): String? {
        return imageFilePath
    }

    fun getTables(): List<SpinnerTablesViewModel> {
        return tables
    }

    fun getTableAtPositon(position: Int): Int {
        if (position > tables.size) return 0
        return tables[position].getTableId()
    }

    fun createReport(description: String, stageId: Int, issueId: Int, tableNo: Int, grooveNum: Int, image: String): Disposable {
        return repository.createIssue(preferences.token, description, stageId, issueId, tableNo, grooveNum, image)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestSuccess.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun createLocalReport(description: String, stageId: Int, issueId: Int, tableNo: Int, grooveNum: Int, image: File, categoryId: Int, lotId: Int): Disposable {
        return repository.createLocalIssue(description, stageId, issueId, tableNo, grooveNum, image.absolutePath, categoryId, lotId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestSuccess.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun uploadImage(file: File): Disposable {
        return uploadRepository.uploadImage(file, "user")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    successUploadImage.onNext(it.relative_url)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }


    fun onSuccessRequest(): BehaviorSubject<Boolean> {
        return requestSuccess
    }

    fun onFailRequest(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onSuccessUploadImage(): BehaviorSubject<String> {
        return successUploadImage
    }
}