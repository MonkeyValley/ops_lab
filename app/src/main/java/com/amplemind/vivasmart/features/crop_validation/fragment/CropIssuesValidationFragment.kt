package com.amplemind.vivasmart.features.crop_validation.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.repository.model.ImagesModel
import com.amplemind.vivasmart.features.crop_validation.adapter.CropImagesValidationAdapter
import com.amplemind.vivasmart.features.crop_validation.adapter.CropIssuesValidationAdapter
import com.amplemind.vivasmart.features.crop_validation.dialogs.SignPhotoCropValidationDialog
import com.amplemind.vivasmart.features.crop_validation.models.CropIssuesModel
import com.amplemind.vivasmart.features.crop_validation.models.CropValidationDetailResponse
import com.amplemind.vivasmart.features.crop_validation.models.CropValidationPost
import com.amplemind.vivasmart.features.crop_validation.models.CropValidationPostResponse
import com.amplemind.vivasmart.features.crop_validation.viewmodel.CropsIssuesValidationFragmentViewModel
import com.amplemind.vivasmart.features.crop_validation.viewmodel.ItemCarryOrderHarvestViewModel
import com.amplemind.vivasmart.features.crop_validation.viewmodel.ItemCropIssuesViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class CropIssuesValidationFragment : BaseFragment(), HasSupportFragmentInjector {
    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = childFragmentInjector

    @Inject
    lateinit var viewModel: CropsIssuesValidationFragmentViewModel

    lateinit var issuesList: List<ItemCropIssuesViewModel>
    lateinit var cropTitle: TextView
    lateinit var tvOrder: TextView
    lateinit var tvLot: TextView
    lateinit var tvBoxes: TextView
    lateinit var rvIssues: RecyclerView
    lateinit var rvImages: RecyclerView
    lateinit var etComment: EditText
    lateinit var btnSend: Button
    lateinit var llImages: LinearLayout
    lateinit var tvEmpty: TextView

    companion object {
        lateinit var objCrop: ItemCarryOrderHarvestViewModel
        const val LIST_IMG_URL_JSON = "listImgUrlJSON"
        var all_ok: Boolean = true
        fun ifConected(): Boolean {
            return if (CropIssuesValidationFragment().hasInternet())
                true
            else {
                Toast.makeText(CropIssuesValidationFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

    fun newInstance(title: String, obj: ItemCarryOrderHarvestViewModel): CropIssuesValidationFragment {
        val args = Bundle()
        args.putString("Title", title)

        objCrop = obj
        Log.e("Tercero: ", Gson().toJson(objCrop))
        val fragment = CropIssuesValidationFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_crop_issues_validation, container, false)

        addSubscribers()
        initElements(view)
        setElements()

        Log.e("Cuarto: ", Gson().toJson(objCrop))
        if (objCrop.has_harvest_quality) {
            getDetail(objCrop.carry_order_id.toInt())
            btnSend.visibility = View.GONE
        } else {
            getIssues(objCrop.crop_id)
        }

        return view
    }

    private fun addSubscribers() {
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.onSuccessRequest().subscribe(this::updateList).addTo(subscriptions)
        viewModel.onSuccessRequestPost().subscribe(this::returnToMain).addTo(subscriptions)
        viewModel.onSuccessRequestDetail().subscribe(this::updateListDetail).addTo(subscriptions)
    }

    fun getIssues(id_crop: Int) {
        viewModel.getIssues(id_crop)
    }

    fun getDetail(carry_order_id: Int) {
        viewModel.getDetail(carry_order_id)
    }

    fun initElements(view: View) {
        cropTitle = view.findViewById(R.id.txt_crop_name)
        tvOrder = view.findViewById(R.id.TV_order)
        tvLot = view.findViewById(R.id.TV_lotName)
        tvBoxes = view.findViewById(R.id.TV_boxes_no)
        rvIssues = view.findViewById(R.id.rv_crop_issues)
        etComment = view.findViewById(R.id.et_comment)
        btnSend = view.findViewById(R.id.btn_check)
        rvImages = view.findViewById(R.id.rv_crop_images)
        llImages = view.findViewById(R.id.LL_images)
        tvEmpty = view.findViewById(R.id.empty_view)
    }

    fun setElements() {
        cropTitle.text = objCrop.crop_name
        tvOrder.text = objCrop.carry_order_id
        tvLot.text = objCrop.lot_name
        tvBoxes.text = objCrop.box_no
        btnSend.setOnClickListener {
            Log.e("issuesList", Gson().toJson(issuesList))
            getSignature(isEverythingOk())
        }
    }

    fun isEverythingOk(): Boolean {
        all_ok = true
        for (item in issuesList)
            if (!item.isChecked!!) all_ok = false
        return all_ok
    }

    fun getSignature(isOk: Boolean) {
        val dialog = SignPhotoCropValidationDialog().newInstance()
        dialog.setTargetFragment(this, 100)
        dialog.show(fragmentManager!!, SignPhotoCropValidationDialog.TAG)
    }

    private fun updateList(list: List<ItemCropIssuesViewModel>) {
        Log.e("updateList", Gson().toJson(list))
        issuesList = list

        Log.e("crop_id: ", objCrop.crop_id.toString())

        val adapter = CropIssuesValidationAdapter(list.toMutableList(), objCrop.has_harvest_quality)
        rvIssues.hasFixedSize()
        rvIssues.layoutManager = LinearLayoutManager(context)
        rvIssues.itemAnimator = DefaultItemAnimator()
        rvIssues.adapter = adapter

        if (list.isEmpty()) {
            tvEmpty.visibility = View.VISIBLE
            btnSend.visibility = View.GONE
        }

    }

    private fun updateListDetail(data: CropValidationDetailResponse) {
        Log.e("updateListDetail", Gson().toJson(data))
        val listIssues = ArrayList<ItemCropIssuesViewModel>()
        for (issue in data.issues) {
            val item = CropIssuesModel(issue.issueId, issue.issue!!.name, issue.is_ok)
            listIssues.add(ItemCropIssuesViewModel(item))
        }
        updateList(listIssues)
        etComment.setText(data.quality_comment)
        etComment.isEnable(false)

        llImages.visibility = View.VISIBLE
        val adapter = CropImagesValidationAdapter(data.harvest_quality_images.toMutableList())
        rvImages.hasFixedSize()
        rvImages.layoutManager = LinearLayoutManager(context)
        rvImages.itemAnimator = DefaultItemAnimator()
        rvImages.adapter = adapter
    }

    fun prepareData(listUrls: ArrayList<String>) {
        // el primer elemento del array es la firma
        val sign: String = listUrls[0]
        val listImages = ArrayList<ImagesModel>()
        val listIssues = ArrayList<CropIssuesModel>()

        for (itemUrl in listUrls) {
            val imageModel = ImagesModel(itemUrl)
            listImages.add(imageModel)
        }

        for (item in issuesList) {
            val issueModel = CropIssuesModel(item.issueId, item.issueName, item.isChecked)
            listIssues.add(issueModel)
        }

        val postObject = CropValidationPost(objCrop.carry_order_id.toInt(), etComment.text.toString(), listIssues, sign, listImages)
        Log.e("postObject", Gson().toJson(postObject))
        sendDataReport(postObject)
    }

    fun sendDataReport(postObj: CropValidationPost) {
        if (ifConected()) {
            viewModel.sendDataCropValidation(postObj)
        }
    }

    fun returnToMain(cropValidationPostResp: CropValidationPostResponse) {
        AlertDialog.Builder(context)
                .setTitle(getString(R.string.crop_validation_title))
                .setMessage("Información enviada correctamente")
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, _ ->
                    val intent = Intent()
                    Log.e("cropValidationPostResp", Gson().toJson(cropValidationPostResp))
                    intent.putExtra("cropValidationPostResp", Gson().toJson(cropValidationPostResp))
                    activity!!.setResult(Activity.RESULT_OK, intent)
                    activity!!.finish()
                    dialogInterface.dismiss()
                }.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == 100) {
            val jsonList = data!!.getStringExtra(LIST_IMG_URL_JSON)
            val listUrls: ArrayList<String> = Gson().fromJson(jsonList, object : TypeToken<ArrayList<String?>?>() {}.type)
            prepareData(listUrls)
        }
    }


}
