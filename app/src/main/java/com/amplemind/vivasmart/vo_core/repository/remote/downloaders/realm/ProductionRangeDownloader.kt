package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangesDataModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionVar
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.remote.StagesApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmObject
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ProductionRangeDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mStagesApi: StagesApi,
        private val mPref: UserAppPreferences
) : RealmDownloader<RealmObject?, StageModel>(realmHolder, StageModel::class.java) {

    override fun buildServerRequest(parent: RealmObject?, lastVersion: Long): Single<List<StageModel>> =
            mStagesApi.getProductionRangeStage(
                    mPref.authorizationToken,
                    businessUnitId = mPref.user.businessUnitId,
                    version = lastVersion
            ).map { response ->
                Log.e("Version: ", lastVersion.toString())
                Log.e("Data-Stage: ", response.data.toString())

                response.data.forEach {
                    Log.e("DATA-LOTID: ", it.lot!!.id.toString())
                    getProductionRangeWeek(it.lot!!.id)
                }
                response.data
            }

    private fun getProductionRangeWeek(lotId: Int): Disposable =
            getInfoProductionRangeLot(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        insertProductionRangesPerWeek(it, lotId)
                    }, {
                        Log.e("ProductionRangesWeek", "error", it)
                    })

    private fun getInfoProductionRangeLot(lotId: Int): Single<ProductionRangesDataModel> =
            mStagesApi.getProductionRangeWeeksData(
                    mPref.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

    private fun insertProductionRangesPerWeek(item: ProductionRangesDataModel?, lotId: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }

        setWeekNumber(item!!.week0!!.productionVars, item!!.week0!!.startDate)
        setWeekNumber(item!!.week1!!.productionVars, item!!.week1!!.startDate)
        setWeekNumber(item!!.week2!!.productionVars, item!!.week2!!.startDate)
    }

    private fun setWeekNumber(item: List<ProductionVar>?, date: String?) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                item!!.forEach { productionVar ->
                    val cal: Calendar = Calendar.getInstance()
                    val date = SimpleDateFormat("yyyy-MM-dd").parse(date)
                    cal.time = date
                    productionVar.weekNumer = cal.get(Calendar.WEEK_OF_YEAR)
                    realm.insertOrUpdate(productionVar)
                }
            }
        }
    }

}