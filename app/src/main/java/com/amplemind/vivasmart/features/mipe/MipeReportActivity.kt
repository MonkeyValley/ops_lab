package com.amplemind.vivasmart.features.mipe

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.mipe.adapter.MipeReportTab
import com.amplemind.vivasmart.features.mipe.fragment.MipeReportFragment
import kotlinx.android.synthetic.main.activity_mipe_report.*
import kotlinx.android.synthetic.main.custom_toolbar.*

class MipeReportActivity : BaseActivityWithFragment() {

    companion object {
        val TAG = MipeReportActivity::class.simpleName
        val TAG_NAME = "$TAG.TagName"
        val LOT_ID = "$TAG.LotId"
        val LOT_NAME = "$TAG.LotName"
        val WEEK = "$TAG.Week"
    }

    var lotId: String = "0"
    var week: Int = 0
    lateinit var fragmentAdapter: MipeReportTab

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mipe_report)

        readArguments()
        setupToolbar()

        fragmentAdapter = MipeReportTab(supportFragmentManager, week, lotId.toInt(), "" + intent?.getStringExtra(LOT_NAME))
        callTabFragment()
        tabs_mipe_report.setupWithViewPager(viewpager_mipe_report)
    }

    fun readArguments(){
        lotId = intent?.getStringExtra(LOT_ID) ?: "0"
        week = intent?.getIntExtra(WEEK, 0) ?: 0
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_mipe_report
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = "" + intent?.getStringExtra(LOT_NAME) + " - " + intent?.getStringExtra(TAG_NAME)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    fun callTabFragment() {
        viewpager_mipe_report.adapter = fragmentAdapter
        viewpager_mipe_report.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                val fragment: Fragment = fragmentAdapter.getRegisteredFragment(position)!!
                (fragment as MipeReportFragment)
            }
        })
        tabs_mipe_report.setupWithViewPager(viewpager_mipe_report)
    }

    override fun onBackPressed() {
        finish()
    }
}
