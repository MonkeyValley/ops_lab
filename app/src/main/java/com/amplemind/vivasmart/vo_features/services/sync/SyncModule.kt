package com.amplemind.vivasmart.vo_features.services.sync

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject

abstract class SyncModule {

    val onReady = BehaviorSubject.createDefault(false)

    lateinit var subscriptionScheduler: Scheduler

    abstract fun cleanUp()

    protected fun <T> Observable<T>.onMainThread(): Observable<T> =
            observeOn(AndroidSchedulers.mainThread())

    protected fun <T> Flowable<T>.onMainThread(): Flowable<T> =
            observeOn(AndroidSchedulers.mainThread())

    protected fun <T> Single<T>.onMainThread(): Single<T> =
            observeOn(AndroidSchedulers.mainThread())

    protected fun <T> Maybe<T>.onMainThread(): Maybe<T> =
            observeOn(AndroidSchedulers.mainThread())

    protected fun Completable.onMainThread(): Completable =
            observeOn(AndroidSchedulers.mainThread())

    protected fun <T> Observable<T>.onCustomScheduler(): Observable<T> =
            observeOn(subscriptionScheduler)

    protected fun <T> Flowable<T>.onCustomScheduler(): Flowable<T> =
            observeOn(subscriptionScheduler)

    protected fun <T> Single<T>.onCustomScheduler(): Single<T> =
            observeOn(subscriptionScheduler)

    protected fun <T> Maybe<T>.onCustomScheduler(): Maybe<T> =
            observeOn(subscriptionScheduler)

    protected fun Completable.onCustomScheduler(): Completable =
            observeOn(subscriptionScheduler)
}