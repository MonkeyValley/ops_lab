package com.amplemind.vivasmart.vo_features.production_roster

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hideSoftKeyboard
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.adapters.SwipableItemTouchHelper
import com.amplemind.vivasmart.features.collaborators.AssignGroovesActivity
import com.amplemind.vivasmart.features.production_roster.AssignUnitDialogFragment
import com.amplemind.vivasmart.features.production_roster.Utils.CustomAlertDialog
import com.amplemind.vivasmart.features.production_roster.Utils.SendDataDelegate
import com.amplemind.vivasmart.features.production_roster.dialogs.CollaboratorsActionsDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.FinalizeUserTimerDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.PauseTimerDialog
import com.amplemind.vivasmart.features.production_roster.fragments.AssignUnitHarvestDialogFragment
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityCodesDao
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getCollaboratorName
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getUnitName
import com.amplemind.vivasmart.vo_core.repository.models.extensions.realCost
import com.amplemind.vivasmart.vo_core.repository.models.extensions.remainingSpaces
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.utils.formatPayment
import com.amplemind.vivasmart.vo_features.custom.CustomSeekBar
import com.amplemind.vivasmart.vo_features.production_roster.adapters.TimerManagerUsersAdapter
import com.amplemind.vivasmart.vo_features.production_roster.adapters.TimerUserViewHolder
import com.amplemind.vivasmart.vo_features.production_roster.helpers.ActivityLogTimer
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.TimerActivityViewModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.TimerFragmentViewModel
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_timer.*
import javax.inject.Inject


class TimerFragment : BaseFragment(), SwipableItemTouchHelper.Listener, SendDataDelegate {

    @Inject
    lateinit var mViewModel: TimerFragmentViewModel

    @Inject
    lateinit var mViewModelTA: TimerActivityViewModel

    @Inject
    lateinit var mPicturesHelper: PicturesHelper

    @Inject
    lateinit var preferences: UserAppPreferences

    @Inject
    lateinit var mActivityLogTimer: ActivityLogTimer


    var rvTimer: RecyclerView? = null
    var stateTab: LinearLayout? = null
    lateinit var mAdapter: TimerManagerUsersAdapter
    var arraySelected = ArrayList<ActivityLogModel>()

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    private var mEnableSeekControl = true
    val realm:Realm = Realm.getDefaultInstance()
    val isEmpty = BehaviorSubject.createDefault(false)

    companion object {

        private val TAG = TimerFragment::class.java.simpleName

        private val PARAM_ACTIVITY_CODE_UUID = "$TAG.ActivityCodeUuid"

        private const val REQUEST_GROOVES = 10

        fun newInstance(activityCodeUuid: String) = TimerFragment().apply {
            arguments = Bundle().apply {
                putString(PARAM_ACTIVITY_CODE_UUID, activityCodeUuid)
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        readArguments()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_timer, container, false)
        setUpUi(view)
        return view
    }

    /*override fun onStart() {
        super.onStart()
        rvTimer!!.adapter = mAdapter
    }*/

    /*override fun onPause() {
        super.onPause()
        rvTimer!!.adapter = null
    }

    override fun onResume() {
        super.onResume()
        reloadData()
    }*/

    override fun onDestroy() {
        super.onDestroy()
        mViewModel.cleanUp()
        mAdapter.cleanUp()
        rvTimer!!.adapter = null
    }

    private fun readArguments() {
        mViewModel.activityCodeUuid = arguments?.getString(PARAM_ACTIVITY_CODE_UUID)
                ?: throw IllegalStateException("TimerFragment with undefined activityCodeUuid")
    }

    fun setUpUi(v: View) {
        rvTimer = v.findViewById(R.id.rv_timer)
        stateTab = v.findViewById(R.id.state_tab)
        initRecycler()
    }

    override fun setUpCallbacks() {
        super.setUpCallbacks()
        mActivityLogTimer.forActivityCode(mViewModel.activityCodeUuid, this::onTimerUpdate)
    }

    override fun setUpUICallbacks() {

        seekBar.getSeekStateControl().subscribe(this::seekBarStateControl).addTo(subscriptions)

        mEventBus.observe<TimerUserViewHolder.OnTimerClickEvent>()
                .filter{ event ->
                    event.activityLog.activityCodeUuid == mViewModel.activityCodeUuid
                }
                .subscribe(this::onTimerClicked)
                .addTo(subscriptions)

        mEventBus.observe<TimerUserViewHolder.OnProfileImageClickEvent>()
                .filter {event ->
                    event.activityLog.activityCodeUuid == mViewModel.activityCodeUuid
                }
                .subscribe(this::onProfileImageClicked)
                .addTo(subscriptions)

        RxView.clicks(btn_start)
                .subscribe{
                    mViewModel.startTimerForAll(mAdapter.items)
                            .subscribe(this@TimerFragment::onStartTimerForAll, this::showErrorDialog)
                            .addTo(subscriptions)
                }
                .addTo(subscriptions)

    }

    override fun loadData() {
        if (mViewModel.canLoadData()) {
            mViewModel.loadActivityCode()
                    .subscribe(this::onActivityCodeLoaded, this::showErrorDialog)
                    ?.addTo(subscriptions)
            mViewModel.loadActivityLogs()
                    .subscribe(this::onActivityLogsLoaded, this::showErrorDialog)
                    ?.addTo(subscriptions)
        }
    }

    private fun onTimerUpdate(activityLogUuid: String) {
        if (::mAdapter.isInitialized) {
            mAdapter.updateTimer(activityLogUuid)
        }
    }

    private fun onActivityCodeLoaded(activityCode: ActivityCodeModel) {
        tv_count.text = getString(R.string.activity_unit_status)
                .format(activityCode.occupiedSpaces, activityCode.totalSpaces)
        cost_by_unit.text = formatPayment(activityCode.activity.realCost)
        tv_name_unit.text = activityCode.activity.getUnitName(context!!)
    }


    private fun onActivityLogsLoaded(activityLogs: Changes<ActivityLogModel>) {

        isEmpty.onNext(activityLogs.data.isEmpty())

        val hasCollaborators = mViewModel.hasCollaborators(activityLogs.data)

        mAdapter.setItems(activityLogs)
        if (hasCollaborators) {
            if (mViewModel.hasCollaboratorsWithTime(activityLogs.data)) {
                showTimerControl(true)
                if (mViewModel.hasCollaboratorsWorking(activityLogs.data)) {
                    resumeTimer()
                } else {
                    pauseTimer()
                }
            }
            else {
                showTimerControl(false)
            }
        }
        else {
            hideTimerButtons()
        }

    }

    private fun onFinishAll(activityLogs: List<ActivityLogModel>) {
        if (activityLogs.isNotEmpty()) {
            activity!!.setResult(200)
            activity!!.finish()
        }
    }

    private fun onStartTimerForAll(activityLogs: List<ActivityLogModel>) {
        if (activityLogs.isNotEmpty()) {
            resumeTimer()
        }
        mEnableSeekControl = true
    }

    private fun onPauseTimerForAll(activityLogs: List<ActivityLogModel>) {
        if (activityLogs.isNotEmpty()) {
            pauseTimer()
        }
        mEnableSeekControl = true
    }

    private fun seekBarStateControl(state: CustomSeekBar.State) {
        //if (mEnableSeekControl) {
            when (state) {
                CustomSeekBar.State.PAUSED -> {
                    showPauseTimerDialog()
                }
                CustomSeekBar.State.CONTINUE -> {
                    mViewModel.startTimerForAll(mAdapter.items)
                            .subscribe(this::onStartTimerForAll, this::showErrorDialog)
                            .addTo(subscriptions)
                    mEnableSeekControl = false
                }
                CustomSeekBar.State.FINISH -> {
                    var alert = CustomAlertDialog(this.activity!!)

                    var query = realm.where(LotModel::class.java)
                    query.distinct("id")

                    val result1 = query.findAll();
                    if (canFinish(mAdapter.items))
                    {

                        alert.alertNot(this, result1, mAdapter.items, mViewModel.stage.lot!!)
                    }
                }
            }
        //}
    }

    private fun showPauseTimerDialog() {
        PauseTimerDialog().newInstance().apply {
            isCancelable = false
            show(this@TimerFragment.fragmentManager!!, PauseTimerDialog.TAG)
            setListener(object : PauseTimerDialog.OnPauseTimerListenner {
                override fun pauseTimer(type: Int) {
                    dismiss()
                    mViewModel.stopTimerForAll(mAdapter.items)
                            .subscribe(this@TimerFragment::onPauseTimerForAll, this@TimerFragment::showErrorDialog)
                            .addTo(subscriptions)
                    mEnableSeekControl = false
                }
            })
        }
    }

    private fun pauseTimer() {
        tv_left_seek_control.text = getString(R.string.mcontinue)
        seekBar.pauseTimers()
    }

    private fun resumeTimer() {
        tv_left_seek_control.text = getString(R.string.pause)
        seekBar.restartTimers()
    }

    private fun showTimerControl(show: Boolean = true) {
        btn_start.visibility = if (show) View.GONE else View.VISIBLE
        seekBarContainer.visibility = if (show) View.VISIBLE else View.GONE
    }


    private fun hideTimerButtons() {
        btn_start.visibility = View.GONE
        seekBarContainer.visibility = View.GONE
    }

    fun initRecycler() {
        mAdapter = TimerManagerUsersAdapter(mEventBus)

        rvTimer!!.hasFixedSize()
        rvTimer!!.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvTimer!!.itemAnimator = DefaultItemAnimator()
        rvTimer!!.setPadding(0, 0, 0, 70.toPx())
        rvTimer!!.clipToPadding = false
        mAdapter.setHasStableIds(true)
        rvTimer!!.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        rvTimer!!.adapter = mAdapter
        stateTab!!.visibility = View.VISIBLE

        val itemTouchHelper = SwipableItemTouchHelper(SwipableItemTouchHelper.FINISHABLE or SwipableItemTouchHelper.DELETABLE, this)
        ItemTouchHelper(itemTouchHelper).attachToRecyclerView(rvTimer!!)
    }

    private fun onProfileImageClicked(event: TimerUserViewHolder.OnProfileImageClickEvent) {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        if (mViewModel.canAssignUnits(event.activityLog)) {
            try {
                if (mViewModel.isGrooveActivity()) {
                    startActivityForResult(Intent(context!!, AssignGroovesActivity::class.java).apply {
                        putExtra(AssignGroovesActivity.PARAM_ACTIVITY_LOG_UUID, event.activityLog.uuid)
                        putExtra(AssignGroovesActivity.PARAM_ACTIVITY_CODE_UUID, event.activityLog.activityCodeUuid)
                    }, REQUEST_GROOVES)
                } else {
                    showEditUnitsDialog(event.activityLog)
                }
            } catch (e: Exception){
                Toast.makeText(this.context,
                        "Error al abrir asignacion de surcos, vuelva a intentarlo.",
                        Toast.LENGTH_SHORT)
                        .show()
            }

        }
    }

    private fun onTimerClicked(event: TimerUserViewHolder.OnTimerClickEvent) {
        if (event.timerStarted) {
            mViewModel.startTimer(event.activityLog)
                    .subscribe({}, this::showErrorDialog)
                    .addTo(subscriptions)
        } else {
            val dialog = PauseTimerDialog().newInstance()
            dialog.show(fragmentManager!!, PauseTimerDialog.TAG)
            dialog.setListener(object : PauseTimerDialog.OnPauseTimerListenner {
                override fun pauseTimer(type: Int) {
                    dialog.dismiss()
                    mViewModel.pauseTimer(event.activityLog)
                            .doOnError(this@TimerFragment::showErrorDialog)
                            .subscribe()
                            .addTo(subscriptions)
                }
            })
        }
    }

    /**
     *  upload the information of Collaborators registered previously
     */

    private fun canFinish(activityLogs: List<ActivityLogModel>): Boolean {

        var lowestPosition: Int = -1

        mViewModel.checkMissingUnits(activityLogs) { position, _ ->
            if (lowestPosition == -1 || lowestPosition > position) {
                lowestPosition = position
            }
            showMissingUnitsError(position, false)
        }

        rvTimer!!.scrollToPosition(lowestPosition)

        if (lowestPosition > -1) {
            showMissingUnitsSnackbar()
        }

        return lowestPosition == -1
    }

    private fun canFinish(position: Int, activityLog: ActivityLogModel): Boolean {
        val canFinish = mViewModel.canFinish(activityLog)

        if (!canFinish) {
            showMissingUnitsError(position)
            rvTimer!!.scrollToPosition(position)
        }

        return canFinish
    }

    private fun showMissingUnitsSnackbar() {
        val snackbar = Snackbar.make(fragment_timer_root, getString(R.string.missing_units), Snackbar.LENGTH_LONG)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text)
                .setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    private fun showMissingUnitsError(position: Int, showSnackbar: Boolean = true) {
        if (showSnackbar) {
            showMissingUnitsSnackbar()
        }
        mAdapter.showMissingUnitsError(position)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is TimerUserViewHolder) {
            if (!mAdapter.isRemoving(position)) {
                val activityLog = mAdapter[position]
                when (direction) {
                    SwipableItemTouchHelper.DELETABLE -> delete(position, activityLog)
                    SwipableItemTouchHelper.FINISHABLE -> finish(position, activityLog)
                }
            }
        }
    }

    private fun delete(position: Int, activityLog: ActivityLogModel) {
        try {
            CollaboratorsActionsDialog.showAlertAction(context!!, getString(R.string.delete), getString(R.string.delete_user_msg), acceptAction = {

                try {


                    showProgressDialog(true)
                    mAdapter.setRemoving(position, true)
                    mViewModel.remove(activityLog)
                            .subscribe({
                                rvTimer!!.recycledViewPool.clear()
                                mAdapter.notifyDataSetChanged()
                                reloadData()
                                showProgressDialog(false)
                            }, { error ->
                                resetItem(position)
                                mAdapter.notifyDataSetChanged()
                                reloadData()
                                showErrorDialog(error)
                                showProgressDialog(false)
                            }).addTo(subscriptions)

                }catch (e:IndexOutOfBoundsException){
                    Log.e("deleteCollaborator", e.message)
                    Toast.makeText(
                            this.context, "Hubo un problema al momento de eliminar a "+
                            activityLog.getCollaboratorName(this.context!!) + ", intentelo de nuevo porfavor.",
                            Toast.LENGTH_SHORT).show()
                    resetItem(position)
                    mAdapter.notifyItemChanged(position)
                }
            }, cancelAction = {
                resetItem(position)
                mAdapter.notifyItemChanged(position)
                showProgressDialog(false)
            })
        } catch (e: Exception) {
            Log.e("deleteCollaborator", e.message)
            Toast.makeText(
                    this.context, "Hubo un problema al momento de eliminar a "+
                    activityLog.getCollaboratorName(this.context!!) + ", intentelo de nuevo porfavor.",
                    Toast.LENGTH_SHORT).show()
            resetItem(position)
            mAdapter.notifyItemChanged(position)
        }
    }

    private fun reloadData(){
        rvTimer!!.adapter = null
        loadData()
        rvTimer!!.adapter = mAdapter
        showProgressDialog(false)
    }

    private fun finish(position: Int, activityLog: ActivityLogModel) {
        if (canFinish(position, activityLog)) {
            FinalizeUserTimerDialog.newInstance(activityLog.uuid).apply {
                listener = object: FinalizeUserTimerDialog.OnFinalizeCollaborator {
                    override fun onFinalize(activityLog: ActivityLogModel, signature: Bitmap?) {
                        mAdapter.setRemoving(position, true)
                        mAdapter.notifyItemChanged(position)
                        Log.e("Signature: ", signature.toString())
                        if (signature != null) {
                            Log.e("Context: ", context!!.toString())
                            mPicturesHelper.createSignatureImageFile(context!!, signature)
                                    .flatMap { signatureFile ->
                                        Log.e("SignatureFile: ", signatureFile.toString())
                                        this@TimerFragment.mViewModel.finish(activityLog, signatureFile)
                                    }
                        }
                        else {
                            this@TimerFragment.mViewModel.finish(activityLog)
                        }
                        .subscribe({}, {error ->
                            resetItem(position)
                            showErrorDialog(error)
                        })
                        .addTo(subscriptions)
                    }
                    override fun onCancel() {
                        resetItem(position)
                    }

                }
                show(this@TimerFragment.childFragmentManager, FinalizeUserTimerDialog.TAG)
            }

        }
    }

    private fun resetItem(position: Int) {
        mAdapter.setRemoving(position, false)
        mAdapter.notifyItemChanged(position)
    }


    private fun showEditUnitsDialog(activityLog: ActivityLogModel) {
        if (mViewModel.isHarvestActivity()) {
            val dialog = AssignUnitHarvestDialogFragment.newInstance(
                    activityLog,
                    mViewModel.activity.getUnitName(context!!),
                    mViewModel.activityCode.remainingSpaces,
                    activityLog.quantity
            )

            dialog.listener = object: AssignUnitHarvestDialogFragment.Listener {
                override fun onOkClicked(dialog: AssignUnitHarvestDialogFragment, goodBoxes: Int, badBoxes: Int, valuesChanges: Boolean) {
                    dialog.dismiss()
                    if (valuesChanges) {
                        Log.i(TAG, "$goodBoxes $badBoxes")
                        mViewModel.setUnits(activityLog, goodBoxes, badBoxes)
                                .doOnError(this@TimerFragment::showErrorDialog)
                                .subscribe()
                                .addTo(subscriptions)
                    }
                }
                override fun onCancelClicked(dialog: AssignUnitHarvestDialogFragment) {
                    dialog.dismiss()
                }
            }

            dialog.show(childFragmentManager, AssignUnitHarvestDialogFragment.TAG)
        }
        else {
            val dialog = AssignUnitDialogFragment.newInstance(
                    mViewModel.activity.getUnitName(context!!),
                    mViewModel.activityCode.remainingSpaces,
                    activityLog.quantity
            )

            dialog.show((context as AppCompatActivity).supportFragmentManager, AssignUnitDialogFragment.TAG)
            dialog.setChangeListenner(object : AssignUnitDialogFragment.OnChangeUnit {

                override fun cancelEdit() {
                    dialog.context?.hideSoftKeyboard()
                    dialog.dismiss()
                }

                override fun changeUnit(newValue: Int, previousValue: Int) {
                    Log.i(TAG, "$newValue")

                    mViewModel.setUnits(activityLog, newValue)
                            .doOnError(this@TimerFragment::showErrorDialog)
                            .subscribe()
                            .addTo(subscriptions)

                    dialog.context?.hideSoftKeyboard()
                }
            })
        }
    }


    data class RefreshTimerEvent(
            val activityUuid: String,
            val stageUuid: String
    )

    override fun sendSing(lote: LotModel, actividad: ActivityModel, stage: StageModel, option_selected: Boolean, items_seleted:ArrayList<ActivityLogModel>) {

        Log.e("RES ---",lote.name + " - " + actividad.name + " - "+ option_selected)

        if(option_selected){
            mViewModel.finishAll(mAdapter.items)
                    .subscribe(this::onFinishAll, this::showErrorDialog)
                    .addTo(subscriptions)
            mEnableSeekControl = false
        }else{
            Log.e("DOS ---",items_seleted.size.toString())

            var query = realm.where(ActivityCodeModel::class.java)
            query.equalTo("stageUuid", stage.uuid )
                    .and()
                    .equalTo("activityUuid", actividad.uuid)
                    .and()
                    .equalTo("isOpen", true)
                    .count()
            val count =  query.findAll()!!


            val collaboratorUuidsAux = ArrayList<String>()

            items_seleted.forEach {
                Log.e("NAME ->",it.collaborator!!.uuid)
                collaboratorUuidsAux.add(it.collaborator!!.uuid)
            }
            val collaboratorUuids =  collaboratorUuidsAux.toTypedArray()

            Log.e("----->", count.size.toString())

            if(count.size > 0) {

                var flag = 0
                for (it in count){
                    val spacesRest =   it.totalSpaces - it.occupiedSpaces
                    if(it.isOpen && spacesRest >= items_seleted.size) {
                        Log.e("CODE UUID: ", it.uuid)
                        mViewModel.finishAll(items_seleted)
                                .subscribe(this::onFinishAll, this::showErrorDialog)
                                .addTo(subscriptions)
                        mEnableSeekControl = false
                        mViewModelTA.createActivityLogs(it.uuid, collaboratorUuids)
                                .subscribe()
                                .addTo(subscriptions)
                        break
                    }else{
                        flag++
                    }
                }

                if(flag == count.size){
                   Toast.makeText(this.context, "No se pueden tener mas de "+ActivityCodesDao.OPEN_LIMIT+" códigos o frecuencias abiertos", Toast.LENGTH_LONG).show()
                }
                Log.e("CODE UUID: "," END")

            }else{
                mViewModel.finishAll(items_seleted)
                        .subscribe(this::onFinishAll, this::showErrorDialog)
                        .addTo(subscriptions)
                mEnableSeekControl = false
                mViewModelTA.createActivityCodeWParams(stage.uuid,actividad.uuid)
                        .subscribe({

                            mViewModelTA.createActivityLogs(it.uuid, collaboratorUuids)
                                    .subscribe()
                                    .addTo(subscriptions)

                        }, { error ->
                            when (error) {
                                is ActivityCodesDao.OpenActivityCodesExceeded ->
                                    androidx.appcompat.app.AlertDialog.Builder(this.context!!)
                                            .setMessage(
                                                    getString(R.string.activity_codes_exceeded)
                                                            .format(ActivityCodesDao.OPEN_LIMIT)
                                            )
                                            .show()
                                else -> throw error
                            }
                        }).addTo(subscriptions)
            }
        }
    }

}
