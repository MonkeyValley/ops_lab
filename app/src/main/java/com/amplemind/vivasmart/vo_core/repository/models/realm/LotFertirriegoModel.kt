package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class LotFertirriegoModel(
        @SerializedName("hydroponic") var hydroponicList: RealmList<HydroponicModel>? = RealmList(),
        @SerializedName("soil") var soilList: RealmList<SoilModel>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class HydroponicModel(
        @SerializedName("lot_id") var lotId: Int = 0,
        @SerializedName("lot_name") var lotName: String = "",
        @SerializedName("lot_type") var lotType: Int = 0,
        @SerializedName("stage_id") var stageId: Int = 0,
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class SoilModel(
        @SerializedName("lot_id") var lotId: Int = 0,
        @SerializedName("lot_name") var lotName: String = "",
        @SerializedName("lot_type") var lotType: Int = 0,
        @SerializedName("stage_id") var stageId: Int = 0,
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable
