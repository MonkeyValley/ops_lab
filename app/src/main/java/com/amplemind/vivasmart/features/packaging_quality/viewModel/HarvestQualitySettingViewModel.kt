package com.amplemind.vivasmart.features.packaging_quality.viewModel

import androidx.databinding.ObservableBoolean
import com.amplemind.vivasmart.vo_core.repository.models.realm.HarvestValidationModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.IssueModel

class HarvestQualitySettingViewModel (
        private val criteria: IssueModel?
) {

    private var issue: HarvestValidationModel.IssueModel? = null

    constructor(issue: HarvestValidationModel.IssueModel): this(null) {
        this.issue = issue
        isOk.set(issue.isOk)
    }

    val id: Long
        get() = criteria?.id ?: issue?.issueId ?: -1

    val criteriaName: String
        get() = criteria?.name ?: issue?.issue?.name ?: ""

    val isOk = ObservableBoolean(issue?.isOk ?: true)
}
