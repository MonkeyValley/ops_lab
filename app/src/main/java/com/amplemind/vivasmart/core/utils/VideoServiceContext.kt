package com.amplemind.vivasmart.core.utils

import android.content.Context
import android.content.ContextWrapper

/**
 * LeakCanary recognizes { ViewVideo.context } as leak when the activity is closed
 * Because VideoView uses a system service : AudioManager.
 * In AudioManager, you will see that old code would use an activity context to create itself.
 * And it’s a strong reference type which means will cause a reference count on this context.
 *
 * @see - https://android-review.googlesource.com/c/platform/frameworks/base/+/140481/1
 * */
class VideoServiceContext(base: Context) : ContextWrapper(base) {

    override fun getSystemService(name: String): Any? {
        return if (Context.AUDIO_SERVICE == name) {
            applicationContext.getSystemService(name)
        } else super.getSystemService(name)
    }

    companion object {

        fun getContext(base: Context): ContextWrapper {
            return VideoServiceContext(base)
        }
    }
}