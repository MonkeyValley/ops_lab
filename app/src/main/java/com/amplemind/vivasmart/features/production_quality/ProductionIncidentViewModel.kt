package com.amplemind.vivasmart.features.production_quality

import java.util.*

class ProductionIncidentViewModel : Observable() {

    fun getComments(): String {
        return "El surco tira mucha agua, el tubo esta roto"
}

    fun getCategory(): String {
        return "Mantenimiento"
    }

    fun getTable(): String {
        return "Tabla 1"
    }

    fun getGroove(): String {
        return "5"
    }
}
