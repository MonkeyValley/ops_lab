package com.amplemind.vivasmart.features.collaborators.fragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.amplemind.vivasmart.features.collaborators.IAssingGroovesManager
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel

class AssingGroovesViewPager(supportFragmentManager: FragmentManager,
                             //val id_collaborator: Int,
                             //val idActivityLog : Long,
                             val activityLog: ActivityLogModel,
                             val listener : IAssingGroovesManager
) : FragmentPagerAdapter(supportFragmentManager) {

    private val titles = mutableListOf<String>()
    private val fragments = mutableListOf<Fragment>()

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = titles.size

    override fun getPageTitle(position: Int) = titles[position]

}
