package com.amplemind.vivasmart.features.production_roster

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.extensions.snack

class AssignUnitDialogFragment : BaseDialogFragment() {

    companion object {
        val TAG = AssignUnitDialogFragment::class.java.simpleName
        fun newInstance(name: String?, busy: Int, units : Int): AssignUnitDialogFragment{
            val fragment = AssignUnitDialogFragment()
            val args = Bundle()
            args.putString("name_unit", name)
            args.putInt("busy", busy)
            args.putInt("units", units)
            fragment.arguments = args
            return fragment
        }
    }

    private var mView : View? = null
    private var eventChange : OnChangeUnit?= null

    private var value : EditText? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mView = LayoutInflater.from(context).inflate(R.layout.content_assign_unit_dialog, null, false)
        builder.setView(mView)

        value = mView!!.findViewById(R.id.ed_unit)

        mView!!.findViewById<TextView>(R.id.tv_name_unit).text = arguments!!.getString("name_unit")

        builder.setPositiveButton("Aceptar", null)
        builder.setNegativeButton("Cancelar") { _, i ->
            hideKeyboard(context!!)
            dismiss()
        }

        return builder.create()
    }

    private fun getBusy(): Int {
        return arguments!!.getInt("busy",0)
    }

    private fun getUnits(): Int {
        return arguments!!.getInt("units",0)
    }


    fun setChangeListenner(listenner : OnChangeUnit){
        this.eventChange = listenner
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (!value!!.text.toString().isEmpty() && (value!!.text.toString().toInt() > 0)){
                if (value!!.text.toString().toInt() <= getBusy() + getUnits()){
                    eventChange!!.changeUnit(value!!.text.toString().toInt(), getUnits())
                    dismiss()
                }else{
                    mView!!.snack("Las unidades que desea asignar son mayores a las disponibles")
                }
            }else{
                mView!!.snack("No puede quedar vacío o en cero")
            }
        }
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
            eventChange!!.cancelEdit()
        }
    }

    interface OnChangeUnit{
        fun changeUnit(newValue : Int, previousValue : Int)
        fun cancelEdit()
    }

}
