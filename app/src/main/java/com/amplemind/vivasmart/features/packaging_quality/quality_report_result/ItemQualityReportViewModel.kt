package com.amplemind.vivasmart.features.packaging_quality.quality_report_result

import com.amplemind.vivasmart.core.repository.request.IssueRequest
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/12/18.
 */
class ItemQualityReportViewModel @Inject constructor(item : IssueRequest, private val sampleSize: Int) {

    val name = item.name
    val number = item.count.toString()
    val percentage = getPercentFromProblem(item.count)

    private fun validateProblems(numberProblems: Int): Int {
        return if (numberProblems > sampleSize) sampleSize else numberProblems
    }

    fun getPercentFromProblem(numberProblems: Int): String {
        val problems = validateProblems(numberProblems)
        return "${(problems * 100) / sampleSize} %"
    }

}