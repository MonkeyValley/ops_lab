package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.response.CarryOrdersResponse
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by
 *          amplemind on 7/6/18.
 */
interface QualitySearchCodeApi {

    @GET("/v2/carry_order/quality/condensed/item/{folio}")
    @Headers("Content-Type: application/json")
    fun searchCarryOrder(
            @Header("Authorization") authentication: String,
            @Path("folio") folio: String,
            @Query("embed") embed: String = "lot,crop,user"
    ): Observable<CarryOrdersResponse>

}