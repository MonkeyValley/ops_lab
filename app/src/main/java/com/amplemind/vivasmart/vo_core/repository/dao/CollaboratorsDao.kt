package com.amplemind.vivasmart.vo_core.repository.dao

import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_core.repository.responses.CollaboratorsResponse
import io.realm.Case
import io.realm.Realm
import io.realm.Sort

class CollaboratorsDao : RealmDao<CollaboratorModel>(CollaboratorModel::class.java) {

    override fun retrieveServerData(response: Any?): List<CollaboratorModel>? {
        return when (response) {
            is CollaboratorsResponse -> {
                response.data
            }
            else -> null
        }
    }

    fun getCollaborators(queryString: String? = null) : List<CollaboratorModel> {
        val realm = Realm.getDefaultInstance()

        val query = realm
                .where(CollaboratorModel::class.java)
                .sort("name", Sort.ASCENDING)

                //notEqualTo wasn't working properly here
                .beginGroup()
                    .equalTo("isWorking", false)
                    .or()
                    .isNull("isWorking")
                .endGroup()

        if (queryString != null) {
            query.and()
                 .beginGroup()
                    .contains("employeeCode", queryString, Case.INSENSITIVE)
                    .or()
                    .contains("name", queryString, Case.INSENSITIVE)
                 .endGroup()
        }

        val results = query.findAll()

        val ret = realm.copyFromRealm(results)

        realm.close()

        return ret
    }

    fun getCollaboratorWithCode(code: String?) : CollaboratorModel? {
        val realm = Realm.getDefaultInstance()

        var ret: CollaboratorModel? = null

        if (code != null) {
            val query = realm
                    .where(CollaboratorModel::class.java)
                    .equalTo("employeeCode", code, Case.INSENSITIVE)

            val results = query.findFirst()

            ret = if (results != null) realm.copyFromRealm(results) else null
        }

        realm.close()

        return ret
    }

    fun markAsWorking(collaborator: CollaboratorModel, isWorking: Boolean): CollaboratorModel? {
        return markAsWorking(listOf(collaborator), isWorking)?.first()
    }

    fun markAsWorking(collaborators: List<CollaboratorModel>, isWorking: Boolean): List<CollaboratorModel>? {

        val objs = copy(collaborators)

        objs.forEach { collab ->
            collab.isWorking = isWorking
        }

        return upsert(collaborators)
    }

    fun setActive(uuid: String, isActive: Boolean) {
        val realm = Realm.getDefaultInstance()

        realm.executeTransaction {
            val collab = realm.where(CollaboratorModel::class.java)
                    .equalTo("uuid", uuid)
                    .findFirst()

            collab?.isActive = isActive
        }

        realm.close()
    }
}