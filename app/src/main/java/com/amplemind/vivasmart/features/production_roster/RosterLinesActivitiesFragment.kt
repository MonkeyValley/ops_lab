package com.amplemind.vivasmart.features.production_roster

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import kotlinx.android.synthetic.main.content_roster_lines_activities_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

class RosterLinesActivitiesFragment : BaseFragment(){

    companion object {
        val TAG = RosterLinesActivitiesFragment::class.java.simpleName
        fun newInstance() = RosterLinesActivitiesFragment()
    }

    @Inject
    lateinit var viewModel: PayrollActivitiesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_roster_lines_activities_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "Empaque"

        initRecycler()
        lbl_track_title.text = "/ Nomina / Empaque /"
        viewModel.loadChooseLinesActivities().addTo(subscriptions)

        viewModel.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)

        btn_finish_lines.setOnClickListener {
            Toast.makeText(context,"En construcción",Toast.LENGTH_SHORT).show()
        }
    }

    private fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_lines_activities.adapter as ActivitiesChoosePackingAdapter).addElements(activities)
    }

    private fun initRecycler() {
        rv_lines_activities.hasFixedSize()
        rv_lines_activities.layoutManager = GridLayoutManager(context, 3)
        rv_lines_activities.itemAnimator = DefaultItemAnimator()
        rv_lines_activities.adapter = ActivitiesChoosePackingAdapter(context!!)

        (rv_lines_activities.adapter as ActivitiesChoosePackingAdapter).clickEvent
                .subscribe(this::onClickItem).addTo(subscriptions)

    }

    fun onClickItem(action : String){
        if (action == "Líneas"){
            (activity as AppCompatActivity)
                    .supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                    .add(R.id.container_payroll, RosterLinesFragment.newInstance())
                    .addToBackStack(RosterLinesFragment.TAG)
                    .commit()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "Nómina"
    }

}
