package com.amplemind.vivasmart.features.notification.viewModel

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.ACTIVITY_CONTEXT
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.notification.service.MessageService
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import com.amplemind.vivasmart.vo_core.repository.remote.FertirriegoApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import kotlinx.android.synthetic.main.alert_message_dialog.view.*
import org.jetbrains.anko.runOnUiThread
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class InboxViewModel @Inject constructor(
        private val apiErrors: HandleApiErrors,
        private val api: FertirriegoApi,
        private val mPref: UserAppPreferences,
        val service: MessageService) : BaseViewModel() {

    private val requestSuccess_message = BehaviorSubject.create<List<MessageModel>>()
    private val listLots = mutableListOf<MessageModel>()

    fun setContext(context: Context?) {
        service.context = context
        service.preferences = UserAppPreferences(context!!, null, null)
    }

    fun getInbox(): Disposable {
        return service.getMessages()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    listLots.clear()
                    listLots.addAll(it)
                    requestSuccess_message.onNext(listLots)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }


    fun onSuccessRequest_message(): BehaviorSubject<List<MessageModel>> {
        return requestSuccess_message
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun getMessage(messageId: Int): Disposable =
            getMessageById(messageId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        writeLine(it)
                        insertMsg(it)
                    }, {
                        Log.e("MESSAGE-NOTIFICATION", "error", it)
                    })

    fun getMessageById(messageId: Int): Single<List<MessageModel>> =
            api.getMessageByIdApi(
                    mPref.authorizationToken,
                    messageId = messageId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

    fun insertMsg(list: List<MessageModel>?) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                list!!.forEach { item ->
                    if (item != null) {
                        realm.insertOrUpdate(item)
                    }
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun writeLine(message: List<MessageModel>) {
        val mDialogView = LayoutInflater.from(ACTIVITY_CONTEXT).inflate(R.layout.alert_message_dialog, null)
        mDialogView.tv_date.text = getDateFormat(message[0].createdAt)
        setPreviewMessage(mDialogView.tv_preview_message, message[0])
        mDialogView.tv_display_message.text = message[0].message

        android.app.AlertDialog.Builder(ACTIVITY_CONTEXT)
                .setView(mDialogView)
                .setCancelable(false)
                .setPositiveButton("Cerrar") { dialogInterface, i ->
                    dialogInterface.dismiss()
                }.show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getDateFormat(dateString: String): String {
        val inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS", Locale.ENGLISH)
        val outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH)
        val outputFormatterHour = DateTimeFormatter.ofPattern("HH:mm:ss", Locale.ENGLISH)
        val date = LocalDate.parse(dateString, inputFormatter)
        val hour = LocalDateTime.parse(dateString, inputFormatter)
        return outputFormatter.format(date) + "\n" + outputFormatterHour.format(hour)
    }

    private fun setPreviewMessage(textView: TextView, item: MessageModel) {
        if (item.data!!.lot_id == 0) {
            textView.text = service.getNotificationType(item.data!!.notification_type_id)
        } else {
            textView.text = service.getLotName(item.data!!.lot_id) +
                    "\n" + service.getNotificationType(item.data!!.notification_type_id)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun UpdateCollaborator(uuid: String, working: Boolean) {
        try {
            var callaboratorName = service.updateCollanboratorAndGetname(uuid, working)
            if (callaboratorName != "") {
                Log.e("Collaborator", callaboratorName + " - " + working)
                //showToat("$callaboratorName-$working")
            }
        } catch (e: Exception){
            Log.e("UpdateCollaborator", e.toString())
        }
    }

    fun showToat(msj: String){
        ACTIVITY_CONTEXT!!.runOnUiThread {
            Toast.makeText(ACTIVITY_CONTEXT, msj, Toast.LENGTH_SHORT).show()
        }
    }

}