package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.CarryOrderCondensedModel
import com.google.gson.annotations.SerializedName

data class CarryOrderCondensedResponse(@SerializedName("data") val data: List<CarryOrderCondensedModel>)