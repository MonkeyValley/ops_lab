package com.amplemind.vivasmart.features.profile

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.getImage
import com.amplemind.vivasmart.core.utils.AndroidPermission
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.features.login.ChangePasswordActivity
import com.amplemind.vivasmart.features.login.LoginActivity
import com.amplemind.vivasmart.features.profile.viewModel.ProfileViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.content_profile_fragment.*
import java.io.File
import java.io.IOException
import javax.inject.Inject

class ProfileActivity : BaseActivity() {


    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mPermission: AndroidPermission

    @Inject
    lateinit var viewModel: ProfileViewModel

    private val CAMERA_REQUEST_CODE = 1
    private val GALLERY_REQUEST_CODE = 2
    private var cameraPermission = false
    private lateinit var dialog: ProgressDialog

    //TODO: mover a un repository, si en algun momento se crea
    @Inject
    lateinit var preferences: UserAppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_profile_fragment)
        initializeUI()
        setProgressDialog()

        viewModel.observeProgressStatus().subscribe(this::onProgressRequestChange).addTo(subscriptions)
        viewModel.observeSuccessUploadImage().subscribe(this::onUploadSuccess).addTo(subscriptions)
        viewModel.observeFailUploadImage().subscribe(this::onUploadFail).addTo(subscriptions)
        viewModel.observeOnSucessUpdateImage().subscribe(this::onSucessUpdateImage).addTo(subscriptions)
        viewModel.onSuccessLogOut().subscribe(this::onSuccessLogOut).addTo(subscriptions)
    }

    private fun initializeUI() {
        val mToolBar = profileToolbar.findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(mToolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = getString(R.string.profile)
        profileImage.setOnClickListener { showAlert() }
        change_password.setOnClickListener {
            startActivity(Intent(this, ChangePasswordActivity::class.java))
        }
        btnLogOut.setOnClickListener {
            viewModel.logOut().addTo(subscriptions)
        }
        userName.text = viewModel.getUserName()
        tv_email.text = viewModel.getUserEmail()

        Glide.with(this)
                .load(viewModel.getUserImage())
                .apply(RequestOptions()
                        .placeholder(R.drawable.ic_user))
                .into(profileImage)
    }

    private fun onSuccessLogOut(success: Boolean) {
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setProgressDialog() {
        dialog = ProgressDialog(this)
        dialog.setMessage(getString(R.string.server_connection))
        dialog.setCancelable(false)
    }

    private fun onProgressRequestChange(requestInProgress: Boolean) {
        if (requestInProgress) {
            dialog.show()
        } else {
            dialog.dismiss()
        }
    }

    private fun onUploadSuccess(response: ProfileRepository.UploadImageResponse) {
        viewModel.updateUserImage(preferences.token, response.relative_url).addTo(subscriptions)
    }

    private fun onSucessUpdateImage(success: Boolean) {

    }

    private fun onUploadFail(msg: String) {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.app_name))
                .setMessage(msg)
                .setPositiveButton("Ok") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }.show()
    }

    private fun initCamera() {
        try {
            viewModel.userImage = picturesHelper.createImageFile(this)
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (callCameraIntent.resolveActivity(packageManager) != null) {
                val authorities = "$packageName.fileprovider"
                val imageUri = FileProvider.getUriForFile(this, authorities, viewModel.userImage!!)
                callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
            }
        } catch (e: IOException) {
            Toast.makeText(this, "Could not create file!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE)
    }

    private fun showAlert() {
        val alert = AlertDialog.Builder(this)
        val dialog = alert.create()
        //Set custom view
        val view = layoutInflater.inflate(R.layout.dialog_user_profile, null, false)
        dialog.setView(view)
        //Buttons Actions
        view.findViewById<LinearLayout>(R.id.gallery_button).setOnClickListener {
            if (mPermission.galleryPermission(this)) {
                initGallery()
            } else {
                cameraPermission = false
                mPermission.verifyHavePermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE))
            }
            dialog.dismiss()
        }
        view.findViewById<LinearLayout>(R.id.camera_button).setOnClickListener {
            if (mPermission.cameraPermission(this)) {
                initCamera()
            } else {
                cameraPermission = true
                mPermission.verifyHavePermissions(this)
            }
            dialog.dismiss()
        }
        view.findViewById<LinearLayout>(R.id.delete_button).setOnClickListener {
            viewModel.deleteUserImage().addTo(subscriptions)
            Glide.with(this).load(this.getImage("ic_user")).into(profileImage)
            dialog.dismiss()
        }
        //Show Dialog
        dialog.show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == mPermission.REQUEST_CODE_PERMISSIONS) {
            if (cameraPermission && mPermission.verifyHavePermissions(this)) {
                initCamera()
            } else if (mPermission.verifyHavePermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE))) {
                initGallery()
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val bitmap = picturesHelper.setScaledBitmap(profileImage.width,
                            profileImage.height, viewModel.userImage!!.absolutePath)
                    Glide.with(this)
                            .load(bitmap)
                            .into(profileImage)
                    val image = picturesHelper.createImageFile(this)
                    picturesHelper.compressImage(viewModel.userImage!!, image, 400f, 400f)
                    viewModel.changeProfileImage(image, preferences.token).addTo(subscriptions)
                }
            }
            GALLERY_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val contentURI = data!!.data
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    Glide.with(this)
                            .load(bitmap)
                            .into(profileImage)

                    val image = picturesHelper.createImageFile(this)
                    picturesHelper.compressImage(File(picturesHelper.getRealPathFromURI(this, contentURI!!)), image, 400f, 400f)
                    viewModel.changeProfileImage(image, preferences.token).addTo(subscriptions)
                }
            }
        }
    }
}
