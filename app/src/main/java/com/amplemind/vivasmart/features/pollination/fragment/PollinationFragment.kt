package com.amplemind.vivasmart.features.pollination.fragment

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.features.pollination.viewModel.PollinationViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PollinationModel
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class PollinationFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: PollinationViewModel

    lateinit var fragView: View

    fun newInstance(weekNumber: Int, lotId: Int, table: Int): PollinationFragment {
        val args = Bundle()
        args.putInt("WEEK", weekNumber)
        args.putInt("LOT_ID", lotId)
        args.putInt("TABLE", table)

        val fragment = PollinationFragment()
        fragment.arguments = args
        return fragment
    }

    var lvl0 = 0
    var lvl1 = 0
    var lvl2 = 0
    var lvl3 = 0

    var etFlowers: EditText? = null
    var btnMinus0: Button? = null
    var etLvl0: EditText? = null
    var btnPlus0: Button? = null
    var tvPorcent0: TextView? = null
    var btnMinus1: Button? = null
    var etLvl1: EditText? = null
    var btnPlus1: Button? = null
    var tvPorcent1: TextView? = null
    var btnMinus2: Button? = null
    var etLvl2: EditText? = null
    var btnPlus2: Button? = null
    var tvPorcent2: TextView? = null
    var btnMinus3: Button? = null
    var etLvl3: EditText? = null
    var btnPlus3: Button? = null
    var tvPorcent3: TextView? = null
    var etPollinationPercent: EditText? = null
    var btnSendPollination: Button? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragView = inflater.inflate(R.layout.fragment_pollination, container, false)

        viewModel.setContext(activity!!)

        setUpUi()
        arguments()
        subscribers()

        return fragView
    }

    private fun subscribers(){
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_pollination().subscribe(this::addElementsPollination).addTo(subscriptions)
        viewModel.getData()
    }

    fun addElementsPollination(data: PollinationModel) {
        etFlowers!!.setText(data.flowers.toString())
        etLvl0!!.setText(data.lvl0.toString())
        etLvl1!!.setText(data.lvl1.toString())
        etLvl2!!.setText(data.lvl2.toString())
        etLvl3!!.setText(data.lvl3.toString())
        tvPorcent0!!.text = data.porcentLvl0.toString()
        tvPorcent1!!.text = data.porcentLvl1.toString()
        tvPorcent2!!.text = data.porcentLvl2.toString()
        tvPorcent3!!.text = data.porcentLvl3.toString()
        etPollinationPercent!!.setText(data.pollinationPorcent.toString() + "%")
        setDisableUi()
    }

    private fun arguments(){
        viewModel.lotId = arguments!!.getInt("LOT_ID") ?: 0
        viewModel.table = arguments!!.getInt("TABLE") ?: 0
        viewModel.week = arguments!!.getInt("WEEK") ?: 0
    }

    private fun setUpUi() {
        etFlowers = fragView.findViewById(R.id.et_flowers)
        calculatePorcentPerLvl(etFlowers)

        btnMinus0 = fragView.findViewById(R.id.btn_minus_0)
        etLvl0 = fragView.findViewById(R.id.et_lvl_0)
        btnPlus0 = fragView.findViewById(R.id.btn_plus_0)
        tvPorcent0 = fragView.findViewById(R.id.tv_porcent_0)
        btnMinus0!!.setOnClickListener { minusCall(it, etLvl0) }
        btnPlus0!!.setOnClickListener { plusCall(it, etLvl0) }
        textChanged(etLvl0, tvPorcent0)

        btnMinus1 = fragView.findViewById(R.id.btn_minus_1)
        etLvl1 = fragView.findViewById(R.id.et_lvl_1)
        btnPlus1 = fragView.findViewById(R.id.btn_plus_1)
        tvPorcent1 = fragView.findViewById(R.id.tv_porcent_1)
        btnMinus1!!.setOnClickListener { minusCall(it, etLvl1) }
        btnPlus1!!.setOnClickListener { plusCall(it, etLvl1) }
        textChanged(etLvl1, tvPorcent1)

        btnMinus2 = fragView.findViewById(R.id.btn_minus_2)
        etLvl2 = fragView.findViewById(R.id.et_lvl_2)
        btnPlus2 = fragView.findViewById(R.id.btn_plus_2)
        tvPorcent2 = fragView.findViewById(R.id.tv_porcent_2)
        btnMinus2!!.setOnClickListener { minusCall(it, etLvl2) }
        btnPlus2!!.setOnClickListener { plusCall(it, etLvl2) }
        textChanged(etLvl2, tvPorcent2)

        btnMinus3 = fragView.findViewById(R.id.btn_minus_3)
        etLvl3 = fragView.findViewById(R.id.et_lvl_3)
        btnPlus3 = fragView.findViewById(R.id.btn_plus_3)
        tvPorcent3 = fragView.findViewById(R.id.tv_porcent_3)
        btnMinus3!!.setOnClickListener { minusCall(it, etLvl3) }
        btnPlus3!!.setOnClickListener { plusCall(it, etLvl3) }
        textChanged(etLvl3, tvPorcent3)

        etPollinationPercent = fragView.findViewById(R.id.et_pollination_percent)
        btnSendPollination = fragView.findViewById(R.id.btn_send_pollination)
        btnSendPollination!!.setOnClickListener { sendPollination() }
    }

    private fun plusCall(it: View?, editText: EditText?) {
        if (editText!!.text.isEmpty()) {
            editText.setText("1")
        } else {
            if (editText.text.toString().toInt() < 999) {
                var result = (editText.text.toString().toInt() + 1)
                editText.setText(result.toString())
            }
        }
    }

    private fun minusCall(it: View?, editText: EditText?) {
        if (editText!!.text.isNotEmpty()) {
            if (editText.text.toString().toInt() > 1) {
                var result = (editText.text.toString().toInt() - 1)
                editText.setText(result.toString())
            } else {
                editText.setText("")
            }
        }
    }

    private fun textChanged(editText: EditText?, textView: TextView?) {
        editText!!.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                        if (p0 != null) {
                            lvl0 = if (etLvl0!!.text.isNotEmpty()) etLvl0!!.text.toString().toInt() else 0
                            lvl1 = if (etLvl1!!.text.isNotEmpty()) etLvl1!!.text.toString().toInt() else 0
                            lvl2 = if (etLvl2!!.text.isNotEmpty()) etLvl2!!.text.toString().toInt() else 0
                            lvl3 = if (etLvl3!!.text.isNotEmpty()) etLvl3!!.text.toString().toInt() else 0
                            etFlowers!!.setText((lvl0 + lvl1 + lvl2 + lvl3).toString())
                        }
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                })
    }

    private fun calculatePorcentPerLvl(etFlowers: EditText?) {
        etFlowers!!.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                        if (p0 != null) {
                            var lv0 = if (etLvl0!!.text.isNotEmpty()) etLvl0!!.text.toString().toDouble() else 0.0
                            var lv1 = if (etLvl1!!.text.isNotEmpty()) etLvl1!!.text.toString().toDouble() else 0.0
                            var lv2 = if (etLvl2!!.text.isNotEmpty()) etLvl2!!.text.toString().toDouble() else 0.0
                            var lv3 = if (etLvl3!!.text.isNotEmpty()) etLvl3!!.text.toString().toDouble() else 0.0
                            var total = etFlowers!!.text.toString().toDouble()

                            when {
                                lv0 == 0.0 && lv1 == 0.0 && lv2 == 0.0 && lv3 == 0.0 -> {
                                    tvPorcent0!!.text = "0.00"
                                    tvPorcent1!!.text = "0.00"
                                    tvPorcent2!!.text = "0.00"
                                    tvPorcent3!!.text = "0.00"
                                }
                                else -> {
                                    tvPorcent0!!.text = String.format("%.2f", ((lv0 / total) * 100.00))
                                    tvPorcent1!!.text = String.format("%.2f", ((lv1 / total) * 100.00))
                                    tvPorcent2!!.text = String.format("%.2f", ((lv2 / total) * 100.00))
                                    tvPorcent3!!.text = String.format("%.2f", ((lv3 / total) * 100.00))
                                }
                            }

                            etPollinationPercent!!.setText(String.format("%.2f", (
                                    tvPorcent1!!.text.toString().toDouble() +
                                            tvPorcent2!!.text.toString().toDouble() +
                                            tvPorcent3!!.text.toString().toDouble())) + "%")
                        }
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                })
    }

    private fun sendPollination(){
        AlertDialog.Builder(context!!)
                .setMessage("¿Desea enviar la informacion de polinización")
                .setPositiveButton(R.string.accept) { _, _ ->
                    if(validateData()) {
                        viewModel.sendPollination(etFlowers!!.text.toString(),
                                etLvl0!!.text.toString(), tvPorcent0!!.text.toString(),
                                etLvl1!!.text.toString(), tvPorcent1!!.text.toString(),
                                etLvl2!!.text.toString(), tvPorcent2!!.text.toString(),
                                etLvl3!!.text.toString(), tvPorcent3!!.text.toString(),
                                etPollinationPercent!!.text.toString().replace("%","")
                        )
                        setDisableUi()
                    } else {
                        val snackbar = Snackbar.make(fragView, "Todos los campos son requeridos", Snackbar.LENGTH_SHORT)
                        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
                        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
                        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                        snackbar.show()
                    }
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun validateData() : Boolean =
        when {
            etFlowers!!.text.isEmpty() -> false
            etLvl0!!.text.isEmpty() -> false
            tvPorcent0!!.text.isEmpty() -> false
            etLvl1!!.text.isEmpty() -> false
            tvPorcent1!!.text.isEmpty() -> false
            etLvl2!!.text.isEmpty() -> false
            tvPorcent2!!.text.isEmpty() -> false
            etLvl3!!.text.isEmpty() -> false
            tvPorcent3!!.text.isEmpty() -> false
            etPollinationPercent!!.text.isEmpty() -> false
            else -> true
        }

    private fun setDisableUi(){
        btnSendPollination!!.visibility = View.GONE
        btnMinus0!!.isEnable(false)
        btnMinus1!!.isEnable(false)
        btnMinus2!!.isEnable(false)
        btnMinus3!!.isEnable(false)
        btnPlus0!!.isEnable(false)
        btnPlus1!!.isEnable(false)
        btnPlus2!!.isEnable(false)
        btnPlus3!!.isEnable(false)
        etLvl0!!.isEnable(false)
        etLvl1!!.isEnable(false)
        etLvl2!!.isEnable(false)
        etLvl3!!.isEnable(false)
    }

}
