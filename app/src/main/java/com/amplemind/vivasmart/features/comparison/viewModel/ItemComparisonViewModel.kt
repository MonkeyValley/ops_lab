package com.amplemind.vivasmart.features.comparison.viewModel

import com.amplemind.vivasmart.core.repository.response.ComparisonResultResponse

class ItemComparisonViewModel(item: ComparisonResultResponse) {

    val name = item.activity_name
    val people_no = item.days.first().people_no
    val people_executed_no = item.days.first().people_executed_no
    val total_estimated_units = item.days.first().total_estimated_units
    val total_executed_units = item.days.first().total_executed_units

    val statusTotalExecuted = people_executed_no >= people_no
    val statusExecuted = total_executed_units >= total_estimated_units

}
