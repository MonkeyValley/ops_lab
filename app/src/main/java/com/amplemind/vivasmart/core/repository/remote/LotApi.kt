package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.LotRepository
import io.reactivex.Observable
import retrofit2.http.*

interface LotApi {

    @GET("/v2/stage")
    @Headers("Content-Type: application/json")
    fun getLots(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "crop,lot,lot.business_unit",
            @Query("lot.business_unit_id") businessUnitId: String,
            @Query("lot.is_active") isEnabled: Boolean = true,
            @Query("is_active") stageIsEnabled: Boolean = true,
            @Query("stage_activities") stage_activities: Boolean = true,
            @Query("order_by") order: String = "lot:name.asc",
            @Query("__logic") logic: String = "AND"
    ): Observable<LotRepository.LotResponse>

    @PUT("/v2/lot/{id_lot}/force_open")
    @Headers("Content-Type: application/json")
    fun reOpenLots(
            @Header("Authorization") authentication: String,
            @Path("id_lot") id_lot : Int
    ): Observable<Any>

}