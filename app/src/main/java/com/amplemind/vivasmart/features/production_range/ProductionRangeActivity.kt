package com.amplemind.vivasmart.features.production_range

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.utils.CATEGORY_ID
import com.amplemind.vivasmart.features.production_range.adapter.ProductionRangeAdapter
import com.amplemind.vivasmart.features.production_range.viewModel.ProductionRangeViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangeVariatiesModel
import kotlinx.android.synthetic.main.activity_production_range.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ProductionRangeActivity : BaseActivity(), View.OnClickListener {

    companion object {
        val TAG: String =  ProductionRangeActivity::class.java.simpleName
        val TAG_NAME = "$TAG.TagName"
        val LOT_ID = "$TAG.LotId"
        var SOIL_ID = "$TAG.SoilId"
        val LOT_NAME = "$TAG.LotName"
    }

    var tittle = ""
    var lotId = "0"
    var soilId = "0"
    var lotName = ""

    private var productionRangeAdapter = ProductionRangeAdapter()
    val productionRangeList = ArrayList<ProductionRangeVariatiesModel>()

    @Inject
    lateinit var viewModel: ProductionRangeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_production_range)
        CATEGORY_ID = 0
        viewModel.setServiceContect(this)
        readArguments()
        setupToolbar()
        setUpUi()
        addSubscribers()
        viewModel.getVariety(lotId, soilId)
    }

    private fun readArguments() {
        tittle = requireNotNull(intent.getStringExtra(TAG_NAME))
        lotId = intent.getStringExtra(LOT_ID) ?: "0"
        soilId = intent.getStringExtra(SOIL_ID) ?: "0"
        lotName = requireNotNull(intent.getStringExtra(LOT_NAME))
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_production_range
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text =  lotName
        lbl_track_title.text = "/ Operaciones / "+ tittle + " / " + lotName + " /"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun setUpUi(){
        showProgressDialog(true)
        btn_copy.setOnClickListener(this)
        btn_save_production_range.setOnClickListener(this)

        val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        val week = calendar.get(Calendar.WEEK_OF_YEAR)

        tv_week1.text = "Semana " + (week-2)
        tv_week2.text = "Semana " + (week-1)
        tv_week3.text = "Semana " + week

        if(viewModel.validateWeekData(lotId, week)){
            btn_copy.isEnable(false)
            btn_save_production_range.visibility = View.GONE
        }

        setupRecycler()
    }

    private fun setupRecycler() {
        rv_production_range.hasFixedSize()
        rv_production_range.layoutManager = LinearLayoutManager(this)
        rv_production_range.itemAnimator = DefaultItemAnimator()
        rv_production_range.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        productionRangeAdapter.setHasStableIds(true)
        productionRangeAdapter.lotId = lotId.toInt()
        rv_production_range.adapter = productionRangeAdapter
        rv_production_range.addOnScrollListener(rvScrollManager)
    }

    private val rvScrollManager = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            Log.e("dx", dx.toString())
            Log.e("dy", dy.toString())
            Log.e("CATEGORY_ID", CATEGORY_ID.toString())
            productionRangeAdapter.scroll = true
        }
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccesProductionRangesVarieties()
                .subscribe(this::addElementsVariety).addTo(subscriptions)
    }

    private fun addElementsVariety(data: List<ProductionRangeVariatiesModel>){
        productionRangeList.clear()
        productionRangeList.addAll(data)
        productionRangeAdapter.addElements(productionRangeList)
        showProgressDialog(false)
    }

    private fun reloadData(copy: Boolean, disale: Boolean){
        showProgressDialog(true)
        productionRangeAdapter.copy = copy
        productionRangeAdapter.disale = disale
        productionRangeAdapter.addElements(productionRangeList)
        showProgressDialog(false)
    }

    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.btn_copy -> {
                showProgressDialog(true)
                CATEGORY_ID = 0
                reloadData(copy = true, disale = false)
            }
            R.id.btn_save_production_range -> {
                if(validateData()){

                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Rangos de producción")
                    builder.setMessage("¿Desea guardar la información de estas variedades?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                       showProgressDialog(true)
                        //disableItemRecyclerView(productionRangeList.size)
                        if(viewModel.saveProductionRangeVarieties(productionRangeList, lotId)){
                            showProgressDialog(false)
                        }
                        btn_copy.isEnable(false)
                        btn_save_production_range!!.visibility = View.GONE
                        reloadData(copy = false, disale = true)
                        dialog.dismiss()
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
                    builder.show()
                } else {
                    showSnackBar("Todos los datos son requeridos.")
                }
            }
        }
    }

    private fun validateData() : Boolean {
        var validate = true
        productionRangeList.forEach {
            when(it.mode){
                "1" ->{ if(it.productionVarOptionId == "" || it.productionVarOptionId == "0") validate = false }
                "2" ->{ if(it.maxVal == "" || it.minVal == "" ) validate = false }
                "3" ->{ if(it.value == "") validate = false }
            }
        }
        return validate
    }

    fun showSnackBar(msj: String){
        val snackbar = Snackbar.make(findViewById(android.R.id.content), msj,
                Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text)
                .setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        snackbar.show()
    }

}
