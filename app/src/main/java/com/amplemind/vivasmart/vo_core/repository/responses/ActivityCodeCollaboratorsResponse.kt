package com.amplemind.vivasmart.vo_core.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList

data class ActivityCodeCollaboratorsResponse (
        @SerializedName("data")         val data: List<ActivityCodeCollaboratorItemResponse>,
        @SerializedName("total_count")  val totalCount: Int)

data class ActivityCodeCollaboratorItemResponse (
        @SerializedName("id")                       var id: Long = 0,
        @SerializedName("activity_code_id")         var activityCodeId: Long = 0,
        @SerializedName("activity_code_uuid")       var activityCodeUuid: String = "",
        @SerializedName("activity_code")            var activityCode: ActivityCodeModel = ActivityCodeModel(),
        @SerializedName("collaborator_id")          var collaboratorId: Int = 0,
        @SerializedName("collaborator_uuid")        var collaboratorUuid: String = "",
        @SerializedName("collaborator")             var collaborator: CollaboratorResponse = CollaboratorResponse(),
        @SerializedName("is_active")                var isActive: Boolean = false,
        @SerializedName("is_done")                  var isDone: Boolean = false,
        @SerializedName("time_counter")             var timerCount: Double = 0.0,
        @SerializedName("is_paused")                var isPaused: Boolean = false,
        @SerializedName("is_time")                  var isTime: Boolean? = false,
        @SerializedName("temp_time")                var localTime: Long = 0,
        @SerializedName("packing_id")               var packingId : Int? = -1,
        @SerializedName("packingline_id")           var packingLineId : Int? = -1,
        @SerializedName("name_presentation")        var namePresentation : String? = null,
        @SerializedName("uuid")                     var uuid: String = getUUID()
)

data class CollaboratorResponse(
        @SerializedName("id")              var id : Int = 0,
        @SerializedName("name")            var name : String = "",
        @SerializedName("image")           var image : String? = null,
        @SerializedName("employee_code")   var employeeCode: String = "",
        @SerializedName("birthdate")       var birthdate: String? = null,
        @SerializedName("activity_logs")   var activityLogs: RealmList<ActivityLogModel> = RealmList(),
        @SerializedName("uuid")            var uuid: String = getUUID(),
        @SerializedName("version")         var version: Long = 0
)
