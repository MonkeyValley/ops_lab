package com.amplemind.vivasmart.core.extensions

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.view.WindowManager
import com.amplemind.vivasmart.R

fun isTablet(context: Context): Boolean {
    return context.resources.getBoolean(R.bool.isTable)
}

fun getSpanCount(context: Context) : Int {
    return if(isTablet(context)) 6 else 3
}

fun getSpanCount2(context: Context) : Int {
    return if(isTablet(context)) 6 else 4
}

fun getSpanCount3(context: Context) : Int {
    return if(isTablet(context)) 7 else 7
}