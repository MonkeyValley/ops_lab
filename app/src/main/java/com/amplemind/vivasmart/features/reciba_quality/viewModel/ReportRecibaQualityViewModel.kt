package com.amplemind.vivasmart.features.reciba_quality.viewModel

import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.reciba_quality.repository.RecibaQualityRepository
import com.amplemind.vivasmart.features.reciba_quality.repository.response.ReportRecibaQualityResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ReportRecibaQualityViewModel @Inject constructor(
        private val repository: RecibaQualityRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val list = mutableListOf<ItemReportRecibaQualityViewModel>()
    private val requestSuccess = BehaviorSubject.create<List<ItemReportRecibaQualityViewModel>>()

    fun getReportList(day: String): Disposable {
        return repository.getReportList(day)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel)
                .subscribe({
                    list.clear()
                    list.addAll(it)
                    requestSuccess.onNext(list)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                    Log.e("getReportList", "error", it)
                })
    }


    fun mapModelToViewModel(result : List<ReportRecibaQualityResponse>) : List<ItemReportRecibaQualityViewModel>{
        val viewModels = mutableListOf<ItemReportRecibaQualityViewModel>()
        result.forEach { obj ->
            viewModels.add(ItemReportRecibaQualityViewModel(obj))
        }
        viewModels.sortBy { it.id }
        viewModels.reverse()
        return viewModels
    }

    fun onSuccessRequest(): BehaviorSubject<List<ItemReportRecibaQualityViewModel>> {
        return requestSuccess
    }

}