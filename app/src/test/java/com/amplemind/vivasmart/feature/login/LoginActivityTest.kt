package com.amplemind.vivasmart.feature.login

import android.content.Context
import android.content.Intent
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import androidx.core.content.ContextCompat
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import com.amplemind.vivasmart.AndroidTest
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.LoginModel
import com.amplemind.vivasmart.features.login.LoginActivity
import com.amplemind.vivasmart.features.login.LoginActivity.Companion.LOGIN_ERROR_MESSAGE
import com.amplemind.vivasmart.features.login.RecoverPasswordActivity
import com.amplemind.vivasmart.features.main.MainActivity
import io.reactivex.observers.TestObserver
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.robolectric.Robolectric
import org.robolectric.Shadows


class LoginActivityTest : AndroidTest() {

    // TODO Test Labels

    private var activity: LoginActivity? = null
    private lateinit var testObserver: TestObserver<LoginModel.LoginResponse>

    private val username: String = "user@test.com"
    private val password: String = "123456"

    private var tvForgotPassword: TextView? = null

    private var textInputPassword: TextInputLayout? = null

    private var btn_login: Button? = null
    private var ed_username: TextInputEditText? = null
    private var ed_password: TextInputEditText? = null

    @Before
    fun setup() {
        testObserver = TestObserver.create()

        activity = Robolectric.buildActivity(LoginActivity::class.java)
                .create()
                .resume()
                .get()

        tvForgotPassword = activity?.findViewById(R.id.tv_forgotPassword)
        textInputPassword = activity?.findViewById(R.id.txtInputPassword)
        btn_login = activity?.findViewById(R.id.btn_login)
        ed_username = activity?.findViewById(R.id.ed_username)
        ed_password = activity?.findViewById(R.id.ed_password)
        textInputPassword = activity?.findViewById(R.id.txtInputPassword)
    }

    @Test
    fun `Activity should not be null`() {
        assertNotNull(activity)
    }

    @Test
    fun `setContentView() inflates the correct layout`() {
        assertEquals(R.layout.content_login_activity, activity?.getLayout())
    }

    @Test
    fun `RecoverPasswordActivity must be called on forgot password click`() {
        // Given
        val expectedIntent = Intent(activity, RecoverPasswordActivity::class.java)

        // when
        tvForgotPassword?.performClick()
        val actualIntent = Shadows.shadowOf(activity).peekNextStartedActivity()

        // then
        assertTrue(expectedIntent.filterEquals(actualIntent))
    }

    @Test
    fun `InputPassword should change into default state`() {
        // when
        activity?.changeInputPasswordToDefaultStatus()

        // then
        assertEquals(false, textInputPassword?.isErrorEnabled)
        assertEquals(null, textInputPassword?.error)
    }

    @Test
    fun `Error message should be shown when not valid inputs`() {
        // given
        ed_username?.setText("not valid email")
        ed_password?.setText("12345678a")

        // when
        btn_login?.performClick()

        // then
        assertEquals(true, textInputPassword?.isErrorEnabled)
        assertEquals(LOGIN_ERROR_MESSAGE, textInputPassword?.error)
        assertEquals(textInputPassword?.hint, "Contraseña")
        assertEquals(ContextCompat.getDrawable(context(), R.drawable.edittext_selector), ed_username?.background)

        // textInputPassword is focused
        assert(activity?.currentFocus?.id != textInputPassword?.id)

        // checking if keyboard is hidden
        val inputMethod = context().getSystemService(
                Context.INPUT_METHOD_SERVICE) as InputMethodManager
        assert(!inputMethod.isAcceptingText)
    }

    @Test
    fun `MainActivity should be called when login success`() {
        // given
        val expectedIntent = Intent(activity, MainActivity::class.java)

        // when
        activity?.onLoginSuccess(LoginModel.LoginResponse("dummy Data"))
        val actualIntent = Shadows.shadowOf(activity).peekNextStartedActivity()

        // then
        assertTrue(expectedIntent.filterEquals(actualIntent))
        assertTrue(activity?.isFinishing!!)
    }


}