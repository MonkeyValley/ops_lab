package com.amplemind.vivasmart.features.comparison.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemComparisonBinding
import com.amplemind.vivasmart.features.comparison.viewModel.ItemComparisonViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.BehaviorSubject

class ComparisonAdapter : RecyclerView.Adapter<ComparisonAdapter.ComparisonViewHolder>() {

    private var list = mutableListOf<ItemComparisonViewModel>()

    private val clickSubject = BehaviorSubject.create<ItemComparisonViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComparisonViewHolder {
        val binding = ItemComparisonBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ComparisonViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ComparisonViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(items: MutableList<ItemComparisonViewModel>) {
        this.list.addAll(items)
        notifyDataSetChanged()
    }

    fun onClickItem(): BehaviorSubject<ItemComparisonViewModel> {
        return clickSubject
    }

    inner class ComparisonViewHolder(val binding: ItemComparisonBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemComparisonViewModel) {
            binding.setVariable(BR.itemModel, item)
            binding.executePendingBindings()

            if (adapterPosition > -1) {
                binding.root.setOnClickListener {
                    clickSubject.onNext(item)
                }
            }

        }

    }

}
