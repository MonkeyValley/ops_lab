package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_1 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectModel(realm)
    }

    override fun drop(realm: DynamicRealm) {
        dropUploadObjectModel(realm)
    }

    private fun migrateUploadObjectModel(realm: DynamicRealm) {
        realm.schema.get("UploadObjectModel")?.apply {
            addField("errorCode", Int::class.java)
            setNullable("errorCode", true)
            addField("errorMsg", String::class.java)
        }
    }

    private fun dropUploadObjectModel(realm: DynamicRealm) {
        realm.schema.get("UploadObjectModel")?.apply {
            removeField("errorCode")
            removeField("errorMsg")
        }
    }

}