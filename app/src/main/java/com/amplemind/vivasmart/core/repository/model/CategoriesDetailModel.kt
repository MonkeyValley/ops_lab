package com.amplemind.vivasmart.core.repository.model

data class CategoriesDetailModel(val id: Int, val name: String, val count: Int)