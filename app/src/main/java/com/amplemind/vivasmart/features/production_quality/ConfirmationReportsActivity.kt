package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.snackWithoutInternetMessage
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import kotlinx.android.synthetic.main.activity_confirmation_reports.*
import java.io.File
import javax.inject.Inject

class ConfirmationReportsActivity : BaseActivity(), ItemDeleteConfirmationReport.RecyclerItemTouchHelperListener {

    private val TAG = ProductionQualityActivity::class.java.simpleName

    @Inject
    lateinit var viewModel: ConfirmationReportsViewModel

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mPref: UserAppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation_reports)

        setupToolbar()

        setupListeners()

        initRecycler()

        getData()
    }

    private fun getData() {
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.getData().subscribe(this::addElements).addTo(subscriptions)
        viewModel.onDeleteIssue().subscribe(this::onDeleteIssue).addTo(subscriptions)
        viewModel.onSuccessFinishActivity().subscribe(this::onFinishActivity).addTo(subscriptions)
        viewModel.onSuccessUpdateSign().subscribe(this::onSuccessUpdateImage).addTo(subscriptions)
        if (!isSyncFlow()) {
            viewModel.loadStaticReports(intent.getIntExtra("lot_id", 0)).addTo(subscriptions)
        } else {
            if (isSyncFlow()) btn_send.visibility = View.INVISIBLE else root_confirmation_reports.snackWithoutInternetMessage()
            viewModel.loadLocalStaticReports(intent.getIntExtra("stage_id", 0), isSyncFlow()).addTo(subscriptions)
        }
    }

    private fun onSuccessUpdateImage(files: MutableList<File>) {
        if (files.isNotEmpty()) {
            viewModel.updateSign(files).addTo(subscriptions)
        } else {
            viewModel.finishActivity(intent.getIntExtra("lot_id", 0))?.addTo(subscriptions)
        }
    }

    private fun isSyncFlow(): Boolean {
        return intent.getBooleanExtra("SyncFlow", false)
    }

    private fun onFinishActivity(success: Boolean) {
        setResult(Activity.RESULT_OK)
        finish()
    }

    fun addElements(data: MutableList<ConfirmationReportsItemViewModel>) {
        (rv_confirmation_reports.adapter as ConfirmationReportsAdapter).addElements(data)
        (rv_confirmation_reports.adapter as ConfirmationReportsAdapter).onClickItem().subscribe(this::onClickItem).addTo(subscriptions)
    }

    fun onClickItem(item: ConfirmationReportsItemViewModel) {
        if(!isSyncFlow()) {
            val intent = Intent(this, ConfirmationReportsDetailActivity::class.java)
            intent.putExtra("report", item.model)
            intent.putExtra("position", item.position)
            startActivityForResult(intent, 200)
        }
    }

    private fun onClickDeleteItem(item: ConfirmationReportsItemViewModel) {
        if (true) {
            viewModel.deleteIssue(item.issueId, item.model!!.tableNo).addTo(subscriptions)
        } else {
            viewModel.deleteLocalIssue(item.issueId, item.model!!.tableNo, item.isLocalModel, item.category.toIntOrNull() ?: 0).addTo(subscriptions)
        }
    }

    private fun onDeleteIssue(issue: Pair<Int, Int>) {
        (rv_confirmation_reports.adapter as ConfirmationReportsAdapter).removeItem(issue)
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(this@ConfirmationReportsActivity)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(this@ConfirmationReportsActivity, bitmap), image, 400f, 400f)
        return image
    }

    private fun setupListeners() {
        btn_send.addThrottle().subscribe {
            val dialog = SignatureDialogFragment().newInstance(viewModel.getSignatureTitle(), viewModel.getSignatureMessage(), false,  mode = 0)
            dialog.show(supportFragmentManager, SignatureDialogFragment.TAG)

            dialog.setOnCompleteListener(object : OnSignatureCompleteListener {
                override fun onDismiss() {
                }

                override fun completeSign(sign1: Bitmap, sign2: Bitmap?) {
                    val signs = mutableListOf<File>()
                    signs.add(compressImage(sign1))
                    if (sign2 != null) {
                        signs.add(compressImage(sign2))
                    }
                    if (true) {
                        viewModel.updateSign(signs).addTo(subscriptions)
                    } else {
                        viewModel.finishLocalActivity(signs[0].absolutePath, signs[1].absolutePath, intent.getIntExtra("stage_id", 0)).addTo(subscriptions)
                    }
                }
            })
        }.addTo(subscriptions)
    }

    private fun initRecycler() {
        rv_confirmation_reports.hasFixedSize()
        rv_confirmation_reports.adapter = ConfirmationReportsAdapter(this, mPref)
        val itemTouchHelperCallback = ItemDeleteConfirmationReport(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rv_confirmation_reports)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_categories.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.confirmation)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            200 -> {
                if (resultCode == Activity.RESULT_OK) {
                    val report = viewModel.getReport(data!!.getIntExtra("id", 1))
                    if(true) {
                        viewModel.deleteIssue(report.issueId,report.model!!.tableNo).addTo(subscriptions)
                    } else {
                        viewModel.deleteLocalIssue(report.issueId, report.model!!.tableNo, report.isLocalModel, report.category.toIntOrNull() ?: 0).addTo(subscriptions)
                    }
                }
            }
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is ConfirmationReportsAdapter.ConfirmationReportsViewHolder) {
            when (direction) {
                ItemTouchHelper.LEFT -> {
                    val item = viewModel.getReportAtPosition(viewHolder.adapterPosition)
                    if (item != null && !item.isHeader()) {
                        onClickDeleteItem(item)
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
