package com.amplemind.vivasmart.features.main

import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.MenuItemCompat
import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.dagger.module.HostSelectionInterceptor
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.disable
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.features.main.viewModel.MainViewModel
import com.amplemind.vivasmart.features.notification.MessageActivity
import com.amplemind.vivasmart.features.production_roster.PayrollProductionFragment
import com.amplemind.vivasmart.features.profile.ProfileActivity
import com.amplemind.vivasmart.features.sync_forced.SyncForcedActivity
import com.amplemind.vivasmart.vo_core.repository.dao.MessageDao
import com.amplemind.vivasmart.vo_features.services.TimeBroadCast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import javax.inject.Inject


@SuppressLint("Registered")
open class MainActivity : BaseActivityWithFragment(),
        NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener{


    @Inject
    lateinit var viewModel: MainViewModel

    @Inject
    lateinit var preferences: UserAppPreferences

    @Inject
    lateinit var interceptor: HostSelectionInterceptor

    lateinit var MessagesNot: TextView

    private val myBroadcast: TimeBroadCast = TimeBroadCast()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        validateSyncronizaiton()

        setSupportActionBar(toolbar)

        setting_menu.disable(false) //TODO: Remove when button configuration is used
        setting_menu.setOnClickListener(this)

        setNavigationView()


        callFragmentPayroll()

        val TAG ="Topic subscribe"
        val userStored = getStoredUser()
        val topic :String = userStored.businessUnitId + userStored.role?.roleName?.trim()
        Log.e("firebase-topic", topic.replace("\\s".toRegex(), ""))
        FirebaseMessaging.getInstance().subscribeToTopic(topic.replace("\\s".toRegex(), ""))
                //FirebaseMessaging.getInstance().subscribeToTopic("TEST")
                .addOnCompleteListener { task ->
                    val msg: String
                    if (task.isSuccessful) {
                        msg = getString(R.string.it_was_subscribed)
                    }
                    else {
                        msg = getString(R.string.msg_subscribe_failed)
                    }
                    Log.d(TAG, msg)
                }

        val refreshedToken = FirebaseInstanceId.getInstance().token
        if (refreshedToken != null) {
            Log.d("FireBase-Toke", refreshedToken)
            putUserFirebaseNotificationToken(refreshedToken)
        }

        viewModel.observeSuccessPutFirebaseToken().subscribe(this::onPutFirebaseToken).addTo(subscriptions)
        viewModel.observeFailPutToken().subscribe(this::onPutFirebaseTokenError).addTo(subscriptions)

        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_TIME_TICK)
        filter.addAction(Intent.ACTION_TIME_CHANGED)
        filter.addAction(Intent.ACTION_DATE_CHANGED)
        try {
            registerReceiver(myBroadcast, filter)
        }
        catch (e: Exception) {
            Log.e("TimeBroadCast", e.message.toString())
        }
    }

    private fun validateSyncronizaiton() {
        if(viewModel.validateSync()){
            viewModel.initSync(this)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        setupUI()
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_TIME_TICK)
        filter.addAction(Intent.ACTION_TIME_CHANGED)
        filter.addAction(Intent.ACTION_DATE_CHANGED)
        try {
            registerReceiver(myBroadcast, filter)
        }
        catch (e: Exception) {
            Log.e("TimeBroadCast", e.message.toString())
        }
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(myBroadcast)
    }

    private fun setupUI() {
        val headerView = nav_view.getHeaderView(0)
        val userName = viewModel.getUserName()
        val userEmail = viewModel.getUserEmail()
        val role = viewModel.getRoleName()

        if (BuildConfig.FLAVOR == "local") {
            val serverUrl = viewModel.getServerUrl()
            interceptor.setHost(serverUrl)
            headerView.tvServer.text = serverUrl
        }
        else {
            headerView.tvServer.visibility = View.GONE
        }
        initializeCountDrawer()
        setUpCrashlytics(userName, userEmail)

        headerView.tv_email.text = userEmail
        headerView.tv_user_name.text = userName
        headerView.tv_rol.text = role.toLowerCase().capitalize()
        headerView.tv_version.setOnLongClickListener{
            Toast.makeText(this, viewModel.getServerUrl(), Toast.LENGTH_SHORT).show()
            true
        }


        Glide.with(this)
                .load(viewModel.getUserImage())
                .apply(RequestOptions()
                        .placeholder(R.drawable.ic_user))
                .into(headerView.im_user_picture)
    }

    @SuppressLint("NewApi")
    private fun setNavigationView() {
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        nav_view.getHeaderView(0).findViewById<ImageView>(R.id.im_user_picture).setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        MessagesNot = MenuItemCompat.getActionView(nav_view.menu.findItem(R.id.chat_menu)) as TextView
        initializeCountDrawer()

    }

    private fun callFragmentPayroll() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container_main, PayrollProductionFragment().newInstance())
                .commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.greenhouse_ship_menu -> {
                callFragmentPayroll()
            }
            R.id.ranking_menu -> {

            }
            R.id.chat_menu -> {
                // TODO_ Call the extra hours inbox
                startActivity(Intent(this, MessageActivity::class.java))
            }
            R.id.bug_menu -> {

            }
            R.id.alert_menu -> {

            }
            R.id.sync_menu -> {
                if (ifConected()) {
                    if (viewModel.validateUploadObject()) {
                        snackError("No se puede realizar la sincronización debido a que hay información pendiente por enviar.")
                    } else {
                        SyncForcedActivity.startForForcedDownload(this)
                        finish()
                    }
                }
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onClick(view: View?) {
        if (view!!.id == R.id.setting_menu) {

        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    @SuppressLint("NewApi")
    private fun initializeCountDrawer() {
        MessagesNot.setTypeface(null, Typeface.BOLD)
        val messages = MessageDao().getCountMessages()
        if(messages < 1){
            MessagesNot.visibility = View.GONE
        }else{
            MessagesNot.setText(messages.toString())
        }
    }

    private fun getStoredUser(): UserModel {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java)
    }

    private fun putUserFirebaseNotificationToken(token: String){
        val user = getStoredUser()
        val firebasetoken = AccountRepository.Firebasetoken(token)
        viewModel.putFirebasetoken(firebasetoken, user.id)

    }

    fun onPutFirebaseToken(response: UserModel){
        if(response.firebasetoken != null) {
            Log.d("tokenUpdate4", response.firebasetoken)
        }else{
            Log.d("tokenUpdate3", "Not updated")
        }
    }

    fun onPutFirebaseTokenError(err: String) {
        Log.d("tokenUpdate", err)
    }

    fun ifConected(): Boolean {
        return if (hasInternet())
            true
        else {
            snackError("Se requiere conexión a internet")
            false
        }
    }

    private fun snackError(msj: String){
        val snackbar = Snackbar.make(findViewById(android.R.id.content), msj,
                Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text)
                .setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT

        snackbar.show()
    }

}
