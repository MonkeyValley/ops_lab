package com.amplemind.vivasmart.features.mipe.fragment

import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.mipe.adapter.DiseaseAdapter
import com.amplemind.vivasmart.features.mipe.adapter.DiseaseReportAdapter
import com.amplemind.vivasmart.features.mipe.adapter.PlagueReportAdapter
import com.amplemind.vivasmart.features.mipe.viewModel.ItemPlagueViewModel
import com.amplemind.vivasmart.features.mipe.viewModel.MipeMonitoringViewModel
import kotlinx.android.synthetic.main.fragment_mipe_report.*
import javax.inject.Inject

class MipeReportFragment : BaseFragment() {

    fun newInstance(week: Int, lotId: Int, table: String, lotName: String): MipeReportFragment {
        val args = Bundle()
        args.putInt("WEEK", week)
        args.putInt("LOT_ID", lotId)
        args.putString("TABLE", table)
        args.putString("LOT_NAME", lotName)

        val fragment = MipeReportFragment()
        fragment.arguments = args
        return fragment
    }

    private var plagueAdapter = PlagueReportAdapter()
    private var diseaseAdapter = DiseaseReportAdapter()

    val plagueList = ArrayList<ItemPlagueViewModel>()
    val diseaseList = ArrayList<ItemPlagueViewModel>()

    var lotId = 0
    var groove = 0
    var week = 0
    var table = ""
    var tagName = ""
    var plant = 0

    var btnPlague: Button? = null
    var btnDisease: Button? = null
    var tvWeek1: TextView? = null
    var tvWeek2: TextView? = null
    var tvWeek3: TextView? = null
    var rvPlague: RecyclerView? = null
    var rvDisease: RecyclerView? = null

    @Inject
    lateinit var viewModel: MipeMonitoringViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_mipe_report, container, false)
        arguments()
        setuUi(view)
        setupRecycler()
        subscribers()
        return view
    }

    fun arguments() {
        lotId = arguments?.getInt("LOT_ID", 0) ?: 0
        groove = arguments?.getInt("GROOVE", 0) ?: 0
        week = arguments?.getInt("WEEK", 0) ?: 0
        table = arguments?.getString("TABLE") ?: ""
        tagName = arguments?.getString("TAG_NAME") ?: ""
        plant = arguments?.getInt("PLANT", 0) ?: 0

        plagueAdapter.setData(lotId,groove,week,table,plant)
        diseaseAdapter.setData(lotId,groove,week,table,plant)
    }

    fun subscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getPlagueLotCrop()
        viewModel.onSuccessRequest_lot_plague().subscribe(this::addElementsPlague).addTo(subscriptions)
        viewModel.getDiseaseLotCrop()
        viewModel.onSuccessRequest_lot_disease().subscribe(this::addElementsDisease).addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun getData() {
        viewModel.getPlagueLotCrop()
    }

    fun addElementsPlague(data: List<ItemPlagueViewModel>) {
        plagueList.clear()
        plagueList.addAll(data)
        plagueAdapter.addElements(data)
    }

    fun addElementsDisease(data: List<ItemPlagueViewModel>) {
        diseaseList.clear()
        diseaseList.addAll(data)
        diseaseAdapter.addElements(data)
    }

    private fun setupRecycler() {
        rvPlague!!.hasFixedSize()
        rvPlague!!.layoutManager = LinearLayoutManager(activity)
        rvPlague!!.itemAnimator = DefaultItemAnimator()
        rvPlague!!.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        rvPlague!!.adapter = plagueAdapter

        rvDisease!!.hasFixedSize()
        rvDisease!!.layoutManager = LinearLayoutManager(activity)
        rvDisease!!.itemAnimator = DefaultItemAnimator()
        rvDisease!!.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        rvDisease!!.adapter = diseaseAdapter
    }

    fun setuUi(view: View){
        btnPlague = view.findViewById(R.id.btn_plague_report)
        btnDisease = view.findViewById(R.id.btn_disease_report)
        tvWeek1 = view.findViewById(R.id.tv_week1)
        tvWeek1!!.text = "Semena"+week
        tvWeek2 = view.findViewById(R.id.tv_week2)
        tvWeek2!!.text = "Semena"+ (week-1)
        tvWeek3 = view.findViewById(R.id.tv_week3)
        tvWeek3!!.text = "Semena"+ (week-2)
        rvPlague = view.findViewById(R.id.rv_plague_report)
        rvDisease = view.findViewById(R.id.rv_disease_report)

        btnPlague!!.setOnClickListener { view ->
            rvPlague!!.visibility = View.VISIBLE
            rvDisease!!.visibility = View.GONE
            btnPlague!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
            btn_disease_report.setBackgroundColor(Color.parseColor("#E3E3E3"))
        }

        btnDisease!!.setOnClickListener { view ->
            rvPlague!!.visibility = View.GONE
            rvDisease!!.visibility = View.VISIBLE
            btnPlague!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
            btnDisease!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
        }

    }



}
