package com.amplemind.vivasmart.features.collaborators

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.view.ActionMode
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.CATEGORY_ID
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.MLBarcodeScanner.BarcodeScannerXFragment
import com.amplemind.vivasmart.features.collaborators.callBack.ActionModeCallback
import com.amplemind.vivasmart.features.collaborators.fragment.BarcodeScannerFragment
import com.amplemind.vivasmart.features.collaborators.fragment.CollaboratorsFragment
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.jakewharton.rxbinding2.view.RxView
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_search_collaborator.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class SearchCollaboratorActivity : BaseActivityWithFragment() {

    companion object {

        val TAG: String = SearchCollaboratorActivity::class.java.name

        val EXTRA_SELECTED = "$TAG.Selected"

        val ACTIVITY_ID = "$TAG.activityId"
    }

    @Inject
    lateinit var mEventBus: EventBus

    private var mMenuItem: MenuItem? = null

    private var mIsScannerShowing: Boolean = false
    private var scanReOpen: Boolean = false

    private var mScannedUuids = HashMap<String, BarcodeScannerFragment.OnCodeScannedEvent>()

    private var mActionMode: ActionMode? = null
    private val mActionModeCallback = ActionModeCallback()

    private var mActionModeFinishedManually = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_collaborator)

        setUpCallbacks()
        setUpUI()
        setUpUICallbacks()

        showScanner()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_collaborators, menu)

        mMenuItem = menu?.findItem(R.id.switchSearch)
        setUpMenuIcon()

        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == R.id.switchSearch) {
            if (mIsScannerShowing) {
                showCollaboratorList()
            } else {
                showScanner()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0 && !scanReOpen) {
            finish()
            /* fab_camera.hide()
            mMenuItem?.isVisible = true
            supportFragmentManager.popBackStack()*/
        } else if (scanReOpen) {
            //finish()
            showScannedCollaboratorsList()
        } else {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }

    override fun onSupportActionModeFinished(mode: ActionMode) {
        super.onSupportActionModeFinished(mode)

        if (mode == mActionMode) {
            if (mActionModeFinishedManually) {
                mActionModeFinishedManually = false
            }

            clearSelections()

            mActionMode = null
        }
    }

    private fun setUpCallbacks() {
        mEventBus.observe(this::onCollaboratorScannedEvent).addTo(subscriptions)
        mEventBus.observe(this::onActionModeUpdate).addTo(subscriptions)

        supportFragmentManager.addOnBackStackChangedListener {
            setUpMenuIcon()
        }
    }

    private fun setUpUICallbacks() {
        mActionModeCallback.onAddClick().subscribe(this::onAddClicked).addTo(subscriptions)

        RxView.clicks(fab_camera)
                .subscribe {
                    scanReOpen = true
                    showScanner()
                    mActionMode?.finish()
                }.addTo(subscriptions)

        RxView.clicks(fab_add)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe {
                    showScannedCollaboratorsList()
                }.addTo(subscriptions)
    }

    private fun setUpUI() {
        fab_camera.hide()
        setUpToolbar()
    }

    private fun setUpToolbar() {
        setSupportActionBar(toolbar_collaborators.findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.add_users)
    }

    private fun setUpMenuIcon() =
            mMenuItem?.setIcon(if (mIsScannerShowing) R.drawable.ic_lista_colab else R.drawable.ic_barcode)

    private fun clearSelections() {
        val fragment =
                supportFragmentManager.findFragmentByTag(CollaboratorsFragment.TAG) as? CollaboratorsFragment

        fragment?.selectAll(false)
    }

    private fun showScanner() {
        fab_camera.hide()
        mIsScannerShowing = true

        supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, BarcodeScannerFragment.newInstance())
                .commit()

        setUpMenuIcon()
    }

    private fun showCollaboratorList() {

        mIsScannerShowing = false

        supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, CollaboratorsFragment.newInstance(), CollaboratorsFragment.TAG)
                .commit()

        setUpMenuIcon()
    }

    private fun showScannedCollaboratorsList() {
        val selected = ArrayList<String>(mScannedUuids.keys)

        mMenuItem?.isVisible = false
        val fragment = CollaboratorsFragment.newInstance(selected, true)

        supportFragmentManager.beginTransaction()
                .addToBackStack(BarcodeScannerFragment.TAG)
                .replace(R.id.frameLayout, fragment, CollaboratorsFragment.TAG)
                .commit()

        scanReOpen = false

        fab_add.hide()
        fab_camera.show()
    }

    private fun onCollaboratorScannedEvent(event: BarcodeScannerFragment.OnCodeScannedEvent) {
        Log.e("COLLABORATOR", event.name)
        mScannedUuids[event.uuid] = event
        fab_add.show()
    }

    private fun onActionModeUpdate(event: CollaboratorsFragment.OnActionModeUpdateEvent) {
        if (event.count > 0) {
            if (mActionMode == null) {
                mActionMode = startSupportActionMode(mActionModeCallback)
            }

            mActionMode?.title = event.count.toString()
            mActionMode?.invalidate()
        }
        else {
            mActionModeFinishedManually = true
            mActionMode?.finish()
        }
    }

    private fun onAddClicked(add: Boolean) {
        if (add) {
            val fragment =
                    supportFragmentManager.findFragmentByTag(CollaboratorsFragment.TAG) as CollaboratorsFragment


            val collaborators = fragment.getSelectedCollaborators().distinct()
            /*val collaboratorsByActivity = fragment.getSelectedCollaboratorsByActivity()
            val selected = when {
                        collaborators.isNotEmpty()
                                && collaboratorsByActivity.isNotEmpty() -> {
                            concatenate(collaborators, collaboratorsByActivity).distinct()
                        }
                        collaborators.isNotEmpty() -> collaborators
                        else -> collaboratorsByActivity
                    }*/

            if(collaborators.isNotEmpty()) updateCollaboratorsData(collaborators)

            setResult(RESULT_OK, Intent().apply {
                putExtra(EXTRA_SELECTED, collaborators.toTypedArray())
            })

            CATEGORY_ID = 0
            finish()
        }
    }

    private fun updateCollaboratorsData(List: List<String>) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if(List.isNotEmpty()) {
                    List.forEach {
                        var result = realm
                                .where(CollaboratorModel::class.java)
                                .equalTo("uuid", it)
                                .findFirst()

                        if (result != null) {
                            val calendar = Calendar.getInstance()
                            val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
                            val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
                            result.activityDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
                            result.activityId = intent?.getIntExtra(ACTIVITY_ID, 0) ?: 0
                            realm.insertOrUpdate(result)
                        }
                    }
                }
            }
        }
    }

    private fun <T> concatenate(vararg lists: List<T>): List<T> {
        return listOf(*lists).flatten()
    }


}
