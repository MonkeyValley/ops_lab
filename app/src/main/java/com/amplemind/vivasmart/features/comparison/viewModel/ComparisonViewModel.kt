package com.amplemind.vivasmart.features.comparison.viewModel

import android.content.Intent
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.response.ComparisonResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.comparison.repository.ComparisonRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ComparisonViewModel @Inject constructor(private val repository: ComparisonRepository,
                                              private val apiErrors: HandleApiErrors) : BaseViewModel() {

    enum class ComparisonFlow(val type: Int) {
        PRODUCTION(1),
        LINES(2);

        companion object {
            fun from(value : Int) : ComparisonFlow = ComparisonFlow.values().first{ it.type == value}
        }
    }

    private var intent : Intent? = null

    private val listComparison = BehaviorSubject.create<List<ItemComparisonViewModel>>()

    fun getTitleDate(): String {
        val sdf = SimpleDateFormat("EEEE dd 'de' MMMM", Locale.getDefault())
        return sdf.format(Date())
    }

    fun loadComparison(flow : ComparisonFlow): Disposable {
        val observer = if (flow == ComparisonFlow.PRODUCTION){
            repository.getComparisonProduction(intent?.getIntExtra("stage",-1).toString())
        }else{
            repository.getComparisonPackage(intent?.getIntExtra("idLine",-1).toString())
        }
        return  observer
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .doOnError { progressStatus.onNext(false) }
                .map(this::mapModelToViewModel)
                .subscribe({ data ->
                    listComparison.onNext(data)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getDataComparison(): BehaviorSubject<List<ItemComparisonViewModel>> {
        return listComparison
    }

    private fun mapModelToViewModel(items: ComparisonResponse): List<ItemComparisonViewModel> {
        val models = mutableListOf<ItemComparisonViewModel>()

        items.data.forEach { item ->
            models.add(ItemComparisonViewModel(item))
        }

        return models
    }

    fun setIntent(intent: Intent?) {
        this.intent = intent
    }

}
