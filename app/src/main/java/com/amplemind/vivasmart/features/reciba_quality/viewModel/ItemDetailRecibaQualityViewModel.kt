package com.amplemind.vivasmart.features.reciba_quality.viewModel

import com.amplemind.vivasmart.features.reciba_quality.repository.response.RewviewDetailRecibaQualityResponse

class ItemDetailRecibaQualityViewModel (detailRecibaQuality: RewviewDetailRecibaQualityResponse){
   var cropId = detailRecibaQuality.cropId
    var cropName = detailRecibaQuality.cropName
   var damaged = detailRecibaQuality.damaged.toString()
    var export = detailRecibaQuality.export.toString()
    var folio = detailRecibaQuality.folio.toString()
    var issuesCondition = detailRecibaQuality.issuesCondition
   var issuesPermanent = detailRecibaQuality.issuesPermanent
   var lotId = detailRecibaQuality.lotId
    var lotName = detailRecibaQuality.lotName
   var total = detailRecibaQuality.total.toString()
    var varietyId = detailRecibaQuality.varietyId
    var varietyName = detailRecibaQuality.varietyName
}