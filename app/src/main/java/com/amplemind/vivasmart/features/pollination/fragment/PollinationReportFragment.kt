package com.amplemind.vivasmart.features.pollination.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.pollination.viewModel.PollinationViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PollinationModel
import javax.inject.Inject


class PollinationReportFragment : BaseFragment() {

    fun newInstance(week: Int, lotId: Int, table: Int, lotName: String): PollinationReportFragment {
        val args = Bundle()
        args.putInt("WEEK", week)
        args.putInt("LOT_ID", lotId)
        args.putInt("TABLE", table)
        args.putString("LOT_NAME", lotName)

        val fragment = PollinationReportFragment()
        fragment.arguments = args
        return fragment
    }

    @Inject
    lateinit var viewModel: PollinationViewModel

    lateinit var fragView: View

    var lotId = 0
    var week = 0
    var table = 0
    var tagName = ""

    var tvWeek3: TextView? = null
    var tvFlower3: TextView? = null
    var tvLvl03: TextView? = null
    var tvLvl13: TextView? = null
    var tvLvl23: TextView? = null
    var tvLvl33: TextView? = null
    var tvPorcentPollination3: TextView? = null

    var tvWeek2: TextView? = null
    var tvFlower2: TextView? = null
    var tvLvl02: TextView? = null
    var tvLvl12: TextView? = null
    var tvLvl22: TextView? = null
    var tvLvl32: TextView? = null
    var tvPorcentPollination2: TextView? = null

    var tvWeek1: TextView? = null
    var tvFlower1: TextView? = null
    var tvLvl01: TextView? = null
    var tvLvl11: TextView? = null
    var tvLvl21: TextView? = null
    var tvLvl31: TextView? = null
    var tvPorcentPollination1: TextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragView = inflater.inflate(R.layout.fragment_pollination_report, container, false)

        arguments()
        setUpUi()
        subscribers()

        return fragView
    }

    fun arguments() {
        lotId = arguments?.getInt("LOT_ID", 0) ?: 0
        week = arguments?.getInt("WEEK", 0) ?: 0
        table = arguments?.getInt("TABLE", 0) ?: 0
        tagName = arguments?.getString("TAG_NAME") ?: ""

        viewModel.lotId = lotId
        viewModel.table = table
        viewModel.week = week
    }

    fun setUpUi(){
        tvWeek3 = fragView.findViewById(R.id.tv_week3)
        tvFlower3 = fragView.findViewById(R.id.tv_flower3)
        tvLvl03 = fragView.findViewById(R.id.tv_lvl0_3)
        tvLvl13 = fragView.findViewById(R.id.tv_lvl1_3)
        tvLvl23 = fragView.findViewById(R.id.tv_lvl2_3)
        tvLvl33 = fragView.findViewById(R.id.tv_lvl3_3)
        tvPorcentPollination3 = fragView.findViewById(R.id.tv_porcent_pollination3)

        tvWeek2 = fragView.findViewById(R.id.tv_week2)
        tvFlower2 = fragView.findViewById(R.id.tv_flower2)
        tvLvl02 = fragView.findViewById(R.id.tv_lvl0_2)
        tvLvl12 = fragView.findViewById(R.id.tv_lvl1_2)
        tvLvl22 = fragView.findViewById(R.id.tv_lvl2_2)
        tvLvl32 = fragView.findViewById(R.id.tv_lvl3_2)
        tvPorcentPollination2 = fragView.findViewById(R.id.tv_porcent_pollination2)

        tvWeek1 = fragView.findViewById(R.id.tv_week1)
        tvFlower1 = fragView.findViewById(R.id.tv_flower1)
        tvLvl01 = fragView.findViewById(R.id.tv_lvl0_1)
        tvLvl11 = fragView.findViewById(R.id.tv_lvl1_1)
        tvLvl21 = fragView.findViewById(R.id.tv_lvl2_1)
        tvLvl31 = fragView.findViewById(R.id.tv_lvl3_1)
        tvPorcentPollination1 = fragView.findViewById(R.id.tv_porcent_pollination1)
    }

    private fun subscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_pollination_Report().subscribe(this::addElementsPollinationReport).addTo(subscriptions)
        viewModel.getDataReport()
    }

    fun addElementsPollinationReport(list: List<PollinationModel>) {

        tvWeek3!!.text = "Semana\n" + (week-2)
        tvWeek2!!.text = "Semana\n" + (week-1)
        tvWeek1!!.text = "Semana\n" + week

        list.forEach{
            if (it.week == week - 2) {
                tvFlower3!!.text = if (it.flowers == 0) "" else it.flowers.toString()
                tvLvl03!!.text = if (it.porcentLvl0 == 0.0) "" else it.porcentLvl0.toString() + "%"
                tvLvl13!!.text = if (it.porcentLvl1 == 0.0) "" else it.porcentLvl1.toString() + "%"
                tvLvl23!!.text = if (it.porcentLvl2 == 0.0) "" else it.porcentLvl2.toString() + "%"
                tvLvl33!!.text = if (it.porcentLvl3 == 0.0) "" else it.porcentLvl3.toString() + "%"
                tvPorcentPollination3!!.text = if (it.pollinationPorcent == 0.0) "" else it.pollinationPorcent.toString() + "%"
            }

            if (it.week == week - 1) {
                tvFlower2!!.text = if (it.flowers == 0) "" else it.flowers.toString()
                tvLvl02!!.text = if (it.porcentLvl0 == 0.0) "" else it.porcentLvl0.toString() + "%"
                tvLvl12!!.text = if (it.porcentLvl1 == 0.0) "" else it.porcentLvl1.toString() + "%"
                tvLvl22!!.text = if (it.porcentLvl2 == 0.0) "" else it.porcentLvl2.toString() + "%"
                tvLvl32!!.text = if (it.porcentLvl3 == 0.0) "" else it.porcentLvl3.toString() + "%"
                tvPorcentPollination2!!.text = if (it.pollinationPorcent == 0.0) "" else it.pollinationPorcent.toString() + "%"
            }

            if (it.week == week) {
                tvFlower1!!.text = if (it.flowers == 0) "" else it.flowers.toString()
                tvLvl01!!.text = if (it.porcentLvl0 == 0.0) "" else it.porcentLvl0.toString() + "%"
                tvLvl11!!.text = if (it.porcentLvl1 == 0.0) "" else it.porcentLvl1.toString() + "%"
                tvLvl21!!.text = if (it.porcentLvl2 == 0.0) "" else it.porcentLvl2.toString() + "%"
                tvLvl31!!.text = if (it.porcentLvl3 == 0.0) "" else it.porcentLvl3.toString() + "%"
                tvPorcentPollination1!!.text = if (it.pollinationPorcent == 0.0) "" else it.pollinationPorcent.toString() + "%"
            }
        }

    }

}