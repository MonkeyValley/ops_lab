package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.ActivitiesDao
import com.amplemind.vivasmart.core.repository.local.CollaboratorsDao
import com.amplemind.vivasmart.core.repository.local.CollaboratorsLineDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.remote.PayrollActivitiesApi
import com.amplemind.vivasmart.core.repository.remote.ReportsApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.vo_core.repository.dao.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.ActivityLogUploader
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.FileUploader
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class PayrollRepository @Inject constructor(private val api: ReportsApi,
                                                 private val apiActivities: PayrollActivitiesApi,
                                                 private val apiAuthorization: ApiAuthorization,
                                                 private val preferences: UserAppPreferences,
                                                 private val activityLogsDao: ActivityLogsDao,
                                                 private val database: ActivitiesDao,
                                                 private val databaseCollaborator: CollaboratorsDao,
                                                 private val databaseCollaboratorLine: CollaboratorsLineDao,
                                                 private val fileUploader: FileUploader,
                                                 private val logsUploader: ActivityLogUploader) {

    data class ActivitiesReponse(
            @SerializedName("data") val data: ArrayList<ActivitiesModel>,
            @SerializedName("total_count") val total_count: Int)

    data class StagesReponse(
            @SerializedName("data") val data: ArrayList<StagesModel>,
            @SerializedName("total_count") val total_count: Int)

    data class FinishDayLine(
            @SerializedName("sign") val sign: String? = null
    )

    @Inject
    lateinit var authorization: ApiAuthorization

    /**
     *  @param token authorization in sharedpreferences
     *
     */
    fun getActivities(token: String, lot_id: Int, stage: Int): Observable<ActivitiesReponse> {
        return apiActivities.getActivities(authentication = authorization.getAuthToken(token), activity_category_id = 1, crop_id = lot_id, stage_activities = stage)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    /**
     * Local Activities
     */

    fun saveLocalActivities(activities: List<ActivitiesModel>) {
        for (activity in activities) {
            database.insertActivities(activity)
        }
    }

    fun getLocalActivities(): Observable<List<ActivitiesModel>> {
        return Observable.fromCallable {
            return@fromCallable database.getLocalActivities()
        }
    }

    data class DetailtRosterActivitiesResponse(
            @SerializedName("data") val response: List<DetailtRosterResponse>,
            @SerializedName("total_count") val totalCount: Int
    )

    data class DetailtRosterLinesActivitiesResponse(
            @SerializedName("data") val response: List<DetailtRosterLinesResponse>,
            @SerializedName("total_count") val totalCount: Int
    )

    fun getReviewedList(stageId: Int): Observable<List<ControlQualityListModel>> {
        return api.getReviewedCollaborators(apiAuthorization.getAuthToken(preferences.token), stageId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun loadUnsignedPendingLogsInfo(stage: StageModel): Observable<Changes<PendingLogsModel>> {
        return activityLogsDao.loadUnsignedPendingLogsInfo(stage)
    }

    fun loadUnsignedPending(collaborator: CollaboratorModel, stage: StageModel): Observable<Changes<ActivityLogModel>> {
        return activityLogsDao.loadUnsignedPending(collaborator, stage)
    }

    fun loadSignedPendingLogsInfo(stage: StageModel): Observable<Changes<PendingLogsModel>> {
        return activityLogsDao.loadSignedPendingLogsInfo(stage)
    }

    fun loadSignedPending(collaborator: CollaboratorModel, stage: StageModel): Observable<Changes<ActivityLogModel>> {
        return activityLogsDao.loadSignedPending(collaborator, stage)
    }

    fun setQuantityAndTime(activityLog: ActivityLogModel, quantity: Int, timeInHours: Int): Observable<ActivityLogModel?> {
        return Observable.fromCallable {
            return@fromCallable activityLogsDao.setQuantityAndTime(activityLog, quantity, timeInHours)
        }
        .doOnNext{
            logsUploader.update(it!!)
        }
    }

    fun updatePaymentMode(activityLogs: List<ActivityLogModel>, isTraining: Boolean? = null, isControl: Boolean? = null, isPractice: Boolean? = null): Observable<List<ActivityLogModel>?> {
        return Observable.fromCallable {
            return@fromCallable activityLogsDao.updatePaymentMode(activityLogs, isTraining = isTraining, isControl = isControl, isPractice = isPractice)
        }
    }

    fun setGrooves(activityLog: ActivityLogModel, grooves: List<GrooveModel>): Observable<ActivityLogModel?> {
        return Observable.fromCallable {
            return@fromCallable activityLogsDao.assingGrooves(activityLog, grooves)
        }
        .doOnNext {
            logsUploader.update(activityLog, true)
        }
    }

    fun sign(activityLogs: List<ActivityLogModel>, signature: File?, isTraining: Boolean = false, isPractice: Boolean = false, isControl: Boolean = false): Observable<List<ActivityLogModel>> {
        return Observable.fromCallable {
            return@fromCallable activityLogsDao.sign(activityLogs, signature, isTraining = isTraining, isPractice = isPractice, isControl = isControl)
        }
        .doOnNext{
            if (signature != null) {
                fileUploader.uploadSignature(signature)
                logsUploader.update(it)
            }
        }
    }

    fun setAsDone(activityLogs: List<ActivityLogModel>): Observable<List<ActivityLogModel>> {
        return Observable.fromCallable {
            return@fromCallable activityLogsDao.setAsDone(activityLogs)
        }
        .doOnNext{
            logsUploader.update(it)
        }
    }

    fun setAsDone(stage: StageModel): Observable<List<ActivityLogModel>> {
        return Observable.fromCallable {
            return@fromCallable activityLogsDao.setAsDone(stage)
        }
        .doOnNext{
            logsUploader.update(it)
        }
    }

    fun hasActiveLogs(stage: StageModel): Observable<Boolean> {
        return Observable.fromCallable {
            return@fromCallable activityLogsDao.hasActiveLogs(stage)
        }
    }

    fun thereAreUnsigned(stage: StageModel): Observable<Boolean> {
        return Observable.fromCallable {
            return@fromCallable activityLogsDao.thereAreUnsigned(stage)
        }
    }

    fun countActiveLogsInStage(stage: StageModel): Observable<Int> {
        return activityLogsDao.countActiveLogsForStage(stage)
    }

    fun countActiveLogsInActivity(stage: StageModel, activity: ActivityModel): Observable<Int> {
        return activityLogsDao.countActiveLogsForActivity(stage, activity)
    }

    fun getProcessList(stageId: Int): Observable<List<ControlQualityListModel>> {
        return api.getInProcessCollaborators(apiAuthorization.getAuthToken(preferences.token), stageId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalProcessList(stageId: Int, inReview: Boolean): Observable<List<ControlQualityListModel>> {
        return Observable.fromCallable {
            val processList = mutableListOf<ControlQualityListModel>()
            val activityLogs = databaseCollaborator.getCollaboratorsReadyToReview(stageId)
            for (log in activityLogs) {
                val collaborator = databaseCollaborator.getCollaboratorByIdAndCode(log.collaboratorId!!, log.activityCodeId!!, true)
                val collaboratorActivityLog = databaseCollaborator.getCollaboratorsActivityLogs(log.collaboratorId, stageId)

                if (inReview && collaboratorActivityLog.all { it.sign != null }
                        || !inReview && collaboratorActivityLog.any { it.sign == null }) {
                    val model = ControlQualityListModel(collaborator!!.collaborator.name, collaborator.collaboratorId, collaborator.collaborator.employeeCode,
                            collaboratorActivityLog.filter { it.sign != null }.size, collaboratorActivityLog.filter { it.sign == null }.size,
                            collaboratorActivityLog.size, inReview)
                    processList.add(model)
                }
            }
            processList
        }
    }

    fun getLocalListPackingline(idLine: Int, isReview: Boolean): Observable<List<ControlQualityListModel>> {
        return Observable.fromCallable {
            val processList = mutableListOf<ControlQualityListModel>()

            val activityLogs = databaseCollaborator.getCollaboratorsReadyToReviewInLines(idLine)

            for (log in activityLogs) {
                val collaborator = databaseCollaboratorLine.getCollaboratorByIdAndLine(log.id, idLine)
                val collaboratorsLogs = databaseCollaborator.getCollaboratorsActivityLogsInLines(log.collaborator_id, idLine)

                if (isReview && collaboratorsLogs.all { it.sign != null }
                        || !isReview && collaboratorsLogs.any { it.sign == null }) {

                    if (collaborator.is_done == true){
                        val model = ControlQualityListModel(collaborator.collaborator?.name
                                ?: "", collaborator.collaborator_id!!, collaborator.collaborator?.employeeCode
                                ?: "",
                                collaboratorsLogs.filter { it.sign != null }.size, collaboratorsLogs.filter { it.sign == null }.size,
                                collaboratorsLogs.size, isReview)

                        processList.add(model)
                    }

                }
            }

            return@fromCallable processList
        }
    }

    fun getReviewedListPackingline(packing_line: Int): Observable<List<ControlQualityListModel>> {
        return api.getReviewedCollaboratorsPackingline(apiAuthorization.getAuthToken(preferences.token), packing_line)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getProcessListPackingline(packing_line: Int): Observable<List<ControlQualityListModel>> {
        return api.getInProcessCollaboratorsPackingline(apiAuthorization.getAuthToken(preferences.token), packing_line)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getDetailRosterActivities(id_collaborator: Int, stage: Int): Observable<DetailtRosterActivitiesResponse> {
        return api.getRosterDetailt(apiAuthorization.getAuthToken(preferences.token), id_collaborator = id_collaborator, stage = stage)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalDetailRosterActivities(id_collaborator: Int, stage: Int, sync: Boolean): Observable<DetailtRosterActivitiesResponse> {
        return Observable.fromCallable {
            val activityLogs = mutableListOf<DetailtRosterResponse>()
            if (sync) {
                activityLogs.addAll(databaseCollaborator.getCollaboratorsActivityLogsById(id_collaborator))
            } else {
                activityLogs.addAll(databaseCollaborator.getCollaboratorsActivityLogs(id_collaborator, stage))
            }
            activityLogs.map { activityLog ->
                activityLog.activity_code?.activity = database.getActivityById(activityLog.activity_code?.activityId
                        ?: 0)
            }
            DetailtRosterActivitiesResponse(activityLogs, 0)
        }
    }

    fun finishActivity(stageId: Int): Observable<Any> {
        return api.finishActivity(apiAuthorization.getAuthToken(preferences.token), stageId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getDetailRosterLinesActivities(idCollaborator: Int, idLine: Int): Observable<DetailtRosterLinesActivitiesResponse> {
        return api.getRosterDetailtLines(apiAuthorization.getAuthToken(preferences.token), id_collaborator = idCollaborator, idLine = idLine)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun finishActivityLines(linesPackage: Int): Observable<Any> {
        return api.finishActivityLines(apiAuthorization.getAuthToken(preferences.token), linesPackage, FinishDayLine())
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getDetailRosterOfflineLinesActivities(idCollaborator: Int, idLine: Int?, sync : Boolean): Observable<DetailtRosterLinesActivitiesResponse> {
        return Observable.fromCallable {
            val activityLogs = if (sync){
                databaseCollaborator.getReportSyncByCollaborator(idCollaborator)
            }else{
                databaseCollaborator.getCollaboratorsActivityLogsInLines(idCollaborator,idLine ?: -1)
            }
            return@fromCallable DetailtRosterLinesActivitiesResponse(activityLogs, activityLogs.size)
        }
    }

}