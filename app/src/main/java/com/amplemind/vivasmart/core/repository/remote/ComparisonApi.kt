package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.response.ComparisonResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path

interface ComparisonApi {

    @GET("/v2/planning/stage/executed/day/{stage}/{activityArea}/{activityCategory}/{date}")
    @Headers("Content-Type: application/json")
    fun getComparisonProduction(
            @Header("Authorization") authentication: String,
            @Path("stage") stage : String,
            @Path("activityArea") activityArea : String,
            @Path("activityCategory") activityCategory : String,
            @Path("date") date : String
    ) : Observable<ComparisonResponse>

    @GET("/v2/planning/packingline/executed/day/{packingline}/{activityArea}/{activityCategory}/{date}")
    @Headers("Content-Type: application/json")
    fun getComparisonPackage(
            @Header("Authorization") authentication: String,
            @Path("packingline") packingline : String,
            @Path("activityArea") activityArea : String,
            @Path("activityCategory") activityCategory : String,
            @Path("date") date : String
    ) : Observable<ComparisonResponse>

}