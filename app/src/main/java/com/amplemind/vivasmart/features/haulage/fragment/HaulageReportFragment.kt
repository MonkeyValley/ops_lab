package com.amplemind.vivasmart.features.haulage.fragment


import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.adapters.MarginItemDecoration
import com.amplemind.vivasmart.features.haulage.adapter.HaulageReportAdapter
import com.amplemind.vivasmart.features.haulage.viewModel.HaulageReportViewModel
import com.amplemind.vivasmart.features.haulage.viewModel.ItemHaulageReportViewModel
import com.amplemind.vivasmart.vo_core.utils.currentDate
import com.amplemind.vivasmart.vo_core.utils.currentDateInMillis
import kotlinx.android.synthetic.main.fragment_haulage_report.*
import java.util.*
import javax.inject.Inject


class HaulageReportFragment : BaseFragment() {

    companion object {
        val TAG = HaulageReportFragment::class.java.simpleName
        fun newInstance(): HaulageReportFragment = HaulageReportFragment()
    }

    @Inject
    lateinit var viewModel: HaulageReportViewModel

    private lateinit var mAdapter: HaulageReportAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_haulage_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addSubscribers()
        setUpUI()
        loadData()
    }

    private fun addSubscribers() {
        viewModel.onShowProgress(this::showProgressBar).addTo(subscriptions)
        viewModel.onReportLoaded(this::onReportLoaded).addTo(subscriptions)
        viewModel.onReportLoadingError(this::onReportLoadingError).addTo(subscriptions)
    }

    private fun showProgressBar(show: Boolean) {
        progress_bar2.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun onReportLoadingError(error: Throwable) {
        //onError(error.localizedMessage)
        showProgressBar(false)
    }

    override fun setUpUI() {
        setUpRecyclerView()

        btn_date1.apply {
            text = viewModel.humanFromDate
            setOnClickListener {
                val dialog = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
                    mAdapter.clearData()
                    countBoxes(true, null)
                    viewModel.setFromDate(selectedYear, selectedMonth, selectedDay)
                    displayHumanFromDate()
                    loadData()
                }, viewModel.fromYear, viewModel.fromMonth, viewModel.fromDay)

                dialog.apply {
                    datePicker.maxDate = currentDateInMillis()
                    datePicker.minDate = currentDateInMillis() - 14*24*60*60*1000
                    setTitle("Selecciona una fecha")
                    show()
                }
            }
        }

        btn_date2.apply {
            text = viewModel.humanToDate
            setOnClickListener {
                val dialog = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
                    mAdapter.clearData()
                    countBoxes(true, null)
                    viewModel.setToDate(selectedYear, selectedMonth, selectedDay)
                    displayHumanToDate()
                    loadData()
                }, viewModel.toYear, viewModel.toMonth, viewModel.toDay)

                dialog.apply {
                    datePicker.maxDate = currentDateInMillis()
                    datePicker.minDate = currentDateInMillis() - 14*24*60*60*1000
                    setTitle("Selecciona una fecha")
                    show()
                }
            }
        }
    }

    private fun setUpRecyclerView() {
        mAdapter = HaulageReportAdapter()
        mAdapter.setData(emptyList())
        rv_quality_finished.apply {
            layoutManager = LinearLayoutManager(context!!)
            adapter = mAdapter
            addItemDecoration(MarginItemDecoration(8))
        }
    }

    private fun displayHumanFromDate() {
        btn_date1.text = viewModel.humanFromDate
    }

    private fun displayHumanToDate() {
        btn_date2.text = viewModel.humanToDate
    }

    private fun onReportLoaded(data: List<ItemHaulageReportViewModel>) {
        showNoRecordsLayout(data.isEmpty())
        countBoxes(data.isEmpty(), data)
        mAdapter.setData(data)
    }

    private fun countBoxes(show: Boolean, data: List<ItemHaulageReportViewModel>?) {
        if (show) {
            tv_send_box.text = "0"
            tv_box_received.text = "0"
        } else {
            var box = 0.0
            var receivedBox = 0.0
            for (item in data!!) {
                box += item.box.toDouble()
                receivedBox += item.validBoxes.toDouble()
            }
//            if(box > receivedBox){
//                tv_send_box.setTextColor(Color.parseColor("#33FF3C"))
//                tv_box_received.setTextColor(Color.parseColor("#FF3333"))
//            } else if (box < receivedBox){
//                tv_send_box.setTextColor(Color.parseColor("#FF3333"))
//                tv_box_received.setTextColor(Color.parseColor("#33FF3C"))
//            } else {
//                tv_send_box.setTextColor(Color.parseColor("#33FF3C"))
//                tv_box_received.setTextColor(Color.parseColor("#33FF3C"))
//            }
            tv_send_box.text = box.toString()
            tv_box_received.text = receivedBox.toString()
        }
    }

    override fun loadData() {
        showNoRecordsLayout(false)
        viewModel.loadCarryOrdersReport()
    }

    private fun showNoRecordsLayout(show: Boolean) {
        ln_report.visibility = if (show) View.GONE else View.VISIBLE
    }

}
