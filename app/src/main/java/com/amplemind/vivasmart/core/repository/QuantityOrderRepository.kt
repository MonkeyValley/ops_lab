package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.extensions.dummyQuantityOrder
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.QuantityOrderModelTemp
import com.amplemind.vivasmart.core.repository.remote.CarryOrderApi
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderDetailResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class QuantityOrderRepository @Inject constructor(private val apiCarry: CarryOrderApi,
                                                       private val preferences: UserAppPreferences,
                                                       private val apiAuthorization: ApiAuthorization) {

    data class CarryEvaluationRequest(
            @SerializedName("evaluation") val evaluation: Boolean,
            @SerializedName("sign") val sign: String,
            @SerializedName("comment") val comment: String
    )

    fun getQualityCarryOrderDetail(id_carry: Int): Observable<QualityCarryOrderDetailResponse> {
        return apiCarry.getQualityCarryOrderDetail(apiAuthorization.getAuthToken(preferences.token), id_carry = id_carry)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun sendCarryOrderEvaluation(sign: String, evaluationResult: Boolean, commentsIssue: String, id_carry: Int): Observable<Any> {
        val request = CarryEvaluationRequest(evaluationResult, sign, commentsIssue)
        return apiCarry.sendCarryOrderEvaluation(apiAuthorization.getAuthToken(preferences.token), id_carry, request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun loadQualifiedCarry(id_lot: Int): Observable<QualityCarryOrderDetailResponse> {
        return apiCarry.getQualifiedCarryOrders(apiAuthorization.getAuthToken(preferences.token), id_lot = id_lot)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getCarryForReadEvaluation(idCarry: Int): Observable<QualityCarryOrderDetailResponse> {
        return apiCarry.getQualifiedEvaluation(apiAuthorization.getAuthToken(preferences.token), idCarry = idCarry)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }


}
