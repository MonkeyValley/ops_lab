package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class PHCEPerWeekFertirriegoModel (
        @SerializedName("lot_id") var lotId: Int = 0,
        @SerializedName("date0") var date0: RealmList<TablesPHCEPerDay>? = RealmList(),
        @SerializedName("date1") var date1: RealmList<TablesPHCEPerDay>? = RealmList(),
        @SerializedName("date2") var date2: RealmList<TablesPHCEPerDay>? = RealmList(),
        @SerializedName("date3") var date3: RealmList<TablesPHCEPerDay>? = RealmList(),
        @SerializedName("date4") var date4: RealmList<TablesPHCEPerDay>? = RealmList(),
        @SerializedName("date5") var date5: RealmList<TablesPHCEPerDay>? = RealmList(),
        @SerializedName("date6") var date6: RealmList<TablesPHCEPerDay>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()

) : RealmObject(), Identifiable

open class TablesPHCEPerDay(
        @SerializedName("lot_id") var lotId: Int = 0,
        @SerializedName("date") var date: String = "",
        @SerializedName("T1") var T1: RealmList<PHCEForTable>? = RealmList(),
        @SerializedName("T2") var T2: RealmList<PHCEForTable>? = RealmList(),
        @SerializedName("T3") var T3: RealmList<PHCEForTable>? = RealmList(),
        @SerializedName("T4") var T4: RealmList<PHCEForTable>? = RealmList(),
        @SerializedName("T5") var T5: RealmList<PHCEForTable>? = RealmList(),
        @SerializedName("T6") var T6: RealmList<PHCEForTable>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class PHCEForTable(
        @SerializedName("lot_id") var lotId: Int = 0,
        @SerializedName("ce") var ce: String ?= "",
        @SerializedName("date") var date: String = "",
        @SerializedName("ph") var ph: String ?= "",
        @SerializedName("table") var table: Int = 0,
        @SerializedName("type") var type: String? = "",
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable