package com.amplemind.vivasmart.vo_features.production_roster.viewmodels

import androidx.databinding.ObservableField
import com.amplemind.vivasmart.features.production_roster.service.DailyPayService
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getActiveTime
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.utils.formatTime
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.StatefulAdapter

class ActivityLogRecyclerViewModel(item: ActivityLogModel) : StatefulAdapter.ViewModel<ActivityLogModel>(item) {

    internal var isRemoving = false

    var collaboratorName = ObservableField<String>()
    var units = ObservableField<String>()
    var time = ObservableField<String>()
    var service =  DailyPayService()

    val code: String
        get() = item.collaborator?.employeeCode ?: "N/D"

    var isPaused = true
    var startTime = 0L
    var timeInSeconds = 0L

    var showMissingUnitsError: Boolean = false

    val hasUnits: Boolean
        get() = item.quantity > 0 || (item.badQuantity != null && item.badQuantity!! > 0)

    override fun bind(item: ActivityLogModel) {
        collaboratorName.set(item.collaborator?.name ?: "Nombre no disponible")
        units.set(item.quantity.toString())
        isPaused = item.isPaused
        startTime = item.startTime
        timeInSeconds = item.time

        updateMissingUnitsError()
    }


    fun updateMissingUnitsError() {
        if (hasUnits) {
            showMissingUnitsError = false
        }
    }

    @Synchronized
    fun updateTime() {
        time.set(formatTime(item.getActiveTime()))
    }

    fun validateCollaboratorDailyPayment(collaboratorUuid: String) =
            service.validateCollaboratorDailyPayment(collaboratorUuid)

}