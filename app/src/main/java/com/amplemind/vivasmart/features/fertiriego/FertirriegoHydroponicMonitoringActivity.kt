package com.amplemind.vivasmart.features.fertiriego

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.*
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_DATE
import com.amplemind.vivasmart.features.fertiriego.adapter.HydroponicPulseAdapter
import com.amplemind.vivasmart.features.fertiriego.viewModel.FertirriegoHydroponicMonitoringViewModel
import com.amplemind.vivasmart.features.fertiriego.dialog.EditMaxValvuleDialog
import com.amplemind.vivasmart.features.root_quality.RootQualityDialog
import com.github.badoualy.datepicker.DatePickerTimeline
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_fertirriego_hydroponic_monitoring.*
import kotlinx.android.synthetic.main.activity_fertirriego_hydroponic_monitoring.timeline
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList

class FertirriegoHydroponicMonitoringActivity : BaseActivity(), HydroponicPulseAdapter.Listener {

    companion object {
        val TAG = FertirriegoHydroponicMonitoringActivity::class.simpleName
        val TAG_NAME = "$TAG.TagName"
        val LOT_ID = "$TAG.LotId"
        var MODE = "$TAG.Mode"

    }

    private var mListener: HydroponicPulseAdapter.Listener? = null

    private var mAdapter = HydroponicPulseAdapter()

    @Inject
    lateinit var viewModel: FertirriegoHydroponicMonitoringViewModel

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300
    private var maxPulse = 0

    var etPorcentDrain: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fertirriego_hydroponic_monitoring)

        etPorcentDrain = findViewById(R.id.et_porcent_drain)

        arguments()
        setupToolbar()
        loadDateTime()
        setupRecycler()
        setupButton()
        setUpSubcribers()
    }

    private fun arguments() {
        viewModel.lotId = intent?.getStringExtra(LOT_ID) ?: "0"
        mAdapter.lotId = viewModel.lotId.toInt()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_pulsos
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = "" + intent?.getStringExtra(TAG_NAME) + " - Hidroponia"
        lbl_track_title.text = "/ Fertirriego / " + intent?.getStringExtra("lot_name")  +   "/\n" + intent?.getStringExtra(TAG_NAME)  +  " /"
        lbl_track_title.setTextColor(this.resources.getColor(R.color.white))
        icon_track_title.setColorFilter(this.resources.getColor(R.color.white))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun loadDateTime() {
        val date = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        var cal_begin = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        cal_begin.add(Calendar.DATE, -7)

        Log.e("date", "" + date.get(Calendar.DST_OFFSET))
        Log.e("cal_begin", "" + date.get(Calendar.DST_OFFSET))

        if (date.get(Calendar.DST_OFFSET) != 0) {
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH) + 1)
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) + "-" + cal_begin.get(Calendar.MONTH) + "-" + cal_begin.get(Calendar.DAY_OF_MONTH) + 1)
            timeline.setLastVisibleDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH) + 1)
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH) + 1)
            timeline.setSelectedDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH) + 1)
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH) + 1)
        } else {
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH))
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) + "-" + cal_begin.get(Calendar.MONTH) + "-" + cal_begin.get(Calendar.DAY_OF_MONTH))
            timeline.setLastVisibleDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH))
            timeline.setSelectedDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) + "-" + date.get(Calendar.MONTH) + "-" + date.get(Calendar.DAY_OF_MONTH))
        }

        val sti = date.get(Calendar.YEAR).toString()
        val month = if ((date.get(Calendar.MONTH) + 1) < 10) "0" + (date.get(Calendar.MONTH) + 1).toString() else (date.get(Calendar.MONTH) + 1).toString()
        val day = if ((date.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (date.get(Calendar.DAY_OF_MONTH)).toString() else (date.get(Calendar.DAY_OF_MONTH)).toString()
        viewModel.date = ((date.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
        mAdapter.date = viewModel.date
        getData()

        Log.e("todayData", FERTIRRIEGO_DATE)

        val stringYear = sti.subSequence(2, 4)
        timeline.setDateLabelAdapter { calendar,
                                       index ->
            Integer.toString(calendar[Calendar.MONTH] + 1) + "/" + stringYear
        }

        timeline.onDateSelectedListener = DatePickerTimeline.OnDateSelectedListener { year, month, day, index ->
            Log.d("dateSelected", "year:$year month:$month day:$day index: $index")
            val month = if ((month + 1) < 10) "0" + (month + 1).toString() else (month + 1).toString()
            val day = if ((day) < 10) "0" + (day).toString() else (day).toString()
            viewModel.date = ((year).toString() + "-" + month + "-" + day)
            mAdapter.date = viewModel.date
            getData()
        }
    }

    fun setUpSubcribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    fun getData() {
        addElements(viewModel.getData())
        etPorcentDrain!!.text = viewModel.getPorcentDrain(viewModel.lotId, mAdapter.date)
    }

    fun addElements(data: ArrayList<Int>) {
        mAdapter.addElements(data)
        maxPulse = data.size
    }


    private fun setupRecycler() {
        rv_pulsos.hasFixedSize()
        rv_pulsos.layoutManager = GridLayoutManager(this, getSpanCount2(this))
        rv_pulsos.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

        mAdapter.listener = this
        rv_pulsos.adapter = mAdapter
        clickItemPulse()
    }

    private fun clickItemPulse() {
        mAdapter.onClickItem()
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe(this::showLotDetail).addTo(subscriptions)
    }

    private fun showLotDetail(pulse: Int) {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now

        val intent = Intent(this, FertirriegoPulseActivity::class.java).apply {
            putExtra(FertirriegoPulseActivity.LOT_ID, viewModel.lotId)
            putExtra(FertirriegoPulseActivity.PULSE, pulse.toString())
            putExtra(FertirriegoPulseActivity.DATE, mAdapter.date)
            putExtra(FertirriegoPulseActivity.TAG_NAME, "" + intent?.getStringExtra(TAG_NAME) + " - Hidroponia")
            putExtra(FertirriegoPulseActivity.LOT_NAME, "" + intent?.getStringExtra(TAG_NAME))
        }
        startActivity(intent)
    }

    override fun onGetCounterValue(position: Int): Observable<Int>? {
        return mListener?.onGetCounterValue(position)
    }

    fun setupButton() {
        btn_root_quality.setOnClickListener {
            if (ifConected()) {
                val dialog = RootQualityDialog().newInstance(viewModel.lotId.toInt())
                dialog.show(supportFragmentManager, "Calidad raiz")
                fab_menu.collapse()
            }
        }
        btn_root_quality_add.setOnClickListener {
            if (viewModel.validateDate()) {
                val dialog = EditMaxValvuleDialog().newInstance(viewModel.lotId.toInt(), maxPulse, viewModel.date)
                dialog.show(supportFragmentManager, "Modificar max. de valvulas")
                fab_menu.collapse()
            } else {
                Toast.makeText(this, "No se pueden agregar pulsos en esta fecha", Toast.LENGTH_SHORT).show()
                fab_menu.collapse()
            }
        }
    }

    fun ifConected(): Boolean {
        return if (this.hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

}
