package com.amplemind.vivasmart.features.finished_product_quality.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.FinishedProductRepository
import com.amplemind.vivasmart.core.repository.model.FinishedProductReportModel
import com.amplemind.vivasmart.core.repository.response.IssuesResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.finished_product_quality.services.FinishedProductQualityService
import com.amplemind.vivasmart.features.production_quality.ItemCropUnitModelViewModel
import com.amplemind.vivasmart.features.production_quality.ItemReportQualityReviewDetailViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ProductQualityReviewDetailViewModel @Inject constructor(
        private val repository: FinishedProductRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val listaIssues = mutableListOf<ItemReportQualityReviewDetailViewModel>()
    private lateinit var finishedProductDetail: FinishedProductReportModel
    private val requestSuccess_issues = BehaviorSubject.create<List<ItemReportQualityReviewDetailViewModel>>()
    private val requestSuccess_report = BehaviorSubject.create<FinishedProductReportModel>()
    private val get_report_detail = BehaviorSubject.create<FinishedProductReportModel>()
    private val requestSuccess_unitCrop = BehaviorSubject.create<List<ItemCropUnitModelViewModel>>()
    private val finishedProductQualityService: FinishedProductQualityService = FinishedProductQualityService()

    fun getIssuesList(id_crop: Int): Disposable {
        return repository.getIssues(id_crop)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_issuesUnits)
                .subscribe({
                    listaIssues.clear()
                    listaIssues.addAll(it)
                    requestSuccess_issues.onNext(listaIssues)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModel_issuesUnits(result: IssuesResponse): List<ItemReportQualityReviewDetailViewModel> {
        val list = arrayListOf<ItemReportQualityReviewDetailViewModel>()

        for (item in result.data) {
            list.add(ItemReportQualityReviewDetailViewModel(item, "0", "0"))
        }
        list.sortBy { it.name }
        return list
    }

    fun onSuccessRequest_issues(): BehaviorSubject<List<ItemReportQualityReviewDetailViewModel>> {
        return requestSuccess_issues
    }

    fun onSuccessRequest_report(): BehaviorSubject<FinishedProductReportModel> {
        return requestSuccess_report
    }

    fun onSuccessGet_report(): BehaviorSubject<FinishedProductReportModel> {
        return get_report_detail
    }

    fun onSuccessRequest_unitCrop(): BehaviorSubject<List<ItemCropUnitModelViewModel>> {
        return requestSuccess_unitCrop
    }

    fun postFinishedProduct(ObjFinishedProdReport: FinishedProductReportModel): Disposable {
        return repository.postFinishedProduct(ObjFinishedProdReport)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestSuccess_report.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getReportDetail(idReport: Int): Disposable {
        return repository.getReportDetail(idReport)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate {
                    get_report_detail.onNext(finishedProductDetail)
                }
                .subscribe({
                    finishedProductDetail = it
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getCrop(id_crop: Int): ProductQualityReviewViewModel.CropStageModelItem {
        var cropObj = ProductQualityReviewViewModel.CropStageModelItem(0, "")
        for (crop in finishedProductQualityService.listCrop()) {
            val cropModelNew = ProductQualityReviewViewModel.CropStageModelItem(crop.id, crop.name!!)
            if (cropModelNew.id == id_crop) {
                cropObj = cropModelNew
            }
        }
        return cropObj
    }
}