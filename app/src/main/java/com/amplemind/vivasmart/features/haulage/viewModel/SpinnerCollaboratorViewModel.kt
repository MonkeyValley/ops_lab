package com.amplemind.vivasmart.features.haulage.viewModel

import com.amplemind.vivasmart.core.repository.model.CollaboratorsListModel

class SpinnerCollaboratorViewModel(private val model: CollaboratorsListModel) {

    val collaboratorId = model.id
    val collaboratorName = model.name

    override fun toString(): String {
        return model.name
    }

}