package com.amplemind.vivasmart.features.production_lines

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.DefaultItemAnimator
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.*
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.model.CountBox
import com.amplemind.vivasmart.core.repository.model.HeaderModel
import com.amplemind.vivasmart.features.collaborators.AddCollaboratorsActivity
import com.amplemind.vivasmart.features.count_box.ItemBoxCountViewModel
import com.amplemind.vivasmart.features.production_lines.adapter.BoxCountAdapter
import com.amplemind.vivasmart.features.production_lines.viewModel.BoxCountLinesViewModel
import com.amplemind.vivasmart.features.production_roster.ChangeLotActivity
import com.amplemind.vivasmart.features.scannerbarcode.ReadBarcodeActivity
import kotlinx.android.synthetic.main.content_box_count_lines_activity.*
import javax.inject.Inject

class BoxCountLinesActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: BoxCountLinesViewModel

    @Inject
    lateinit var mediator: Mediator

    companion object {
        val BOX_CANCEL = 500
        val BOX_DONE = 200
        val BOX_ALL_DONE = 201
        val CHANGE_LOT = 1
        val ADD_COLLABORATOR = 2
        val FINISH_COLLABORATOR = 3
        val ADD_COLLABORATORS_BOX = 4
        val BOX_COUNT = 5
        val CHANGE_PRESENTATION = 6
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_box_count_lines_activity)

        setupToolbar()
        setupRecycler()
        setupOnEvents()

        line_name.text = getNameLine()

        viewModel.finishCountBox().subscribe(this::finishSuccessCountBox).addTo(subscriptions)
        viewModel.getListBoxComplete().subscribe(this::sendCountBox).addTo(subscriptions)
        viewModel.getCountBox().subscribe(this::setData).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)

        loadCountBox()
    }

    private fun loadCountBox() {
        if (true) {
            viewModel.loadCountBox().addTo(subscriptions)
        } else {
            loadCountBoxOffline(true)
        }
    }

    private fun loadCountBoxOffline(result: Boolean) {
        if (result) {
            view_main.snackWithoutInternetMessage()
            viewModel.getCountBoxOffline().addTo(subscriptions)
        }
    }

    /**
     *  position in adapter collaborator
     */
    private fun getPosition(): Int {
        return intent.getIntExtra("position", -1)
    }

    private fun getNameLine(): String {
        return intent.getStringExtra("name_line")
    }

    private fun getLotID(): Int {
        return intent.getIntExtra("lot_id", -1)
    }

    /**
     *   if type is 3 add collaborators in package
     */
    private fun getType(): Int {
        return intent.getIntExtra("type", -1)
    }

    /**
     *  add listener clicks events
     */
    private fun setupOnEvents() {
        btn_cancel.addThrottle().subscribe(this::onEventCancel).addTo(subscriptions)
        btn_done_box.addThrottle().subscribe(this::onEventDone).addTo(subscriptions)
    }

    /**
     *  method cancel button
     */
    private fun onEventCancel(any: Any) {
        hideKeyboard(this)
        Handler().postDelayed({
            finish()
        },300)
    }

    /**
     *  method done button
     */
    private fun onEventDone(any: Any) {
        if (viewModel.isCorrectCountBox()) {
            hideKeyboard(this)
            viewModel.getNewCountBox().addTo(subscriptions)
        } else {
            view_main.snack("Ningun conteo puede quedar vacio o ser menor al anterior")
        }
    }

    private fun sendCountBox(data: CountBox) {
        mediator.count_box = data

        when (getType()) {
            ADD_COLLABORATORS_BOX -> {
                addCollaborators()
            }
            CHANGE_LOT -> {
                startActivity(Intent(this, ChangeLotActivity::class.java)
                        .putExtra("lot_id", getLotID())
                        .putExtra("name_line", getNameLine()))
                finish()
            }
            ADD_COLLABORATOR -> {
                if (true) {
                    startActivity(Intent(this, AddCollaboratorsActivity::class.java)
                            .putExtra("onlySelected", true)
                            .putExtra("section", getSection()))
                } else {
                    startActivity(Intent(this, ReadBarcodeActivity::class.java)
                            .putExtra("showActivities", true)
                            .putExtra("section", getSection()))
                }
                finish()
            }
            FINISH_COLLABORATOR -> {
                finishBoxCount(if (getPosition() > -1) BOX_DONE else BOX_ALL_DONE)
            }
            BOX_COUNT -> {
                viewModel.sendCountBox(true, mediator.getMapCountBoxRequest()).addTo(subscriptions)
            }
        }
    }

    private fun finishSuccessCountBox(result: Boolean) {
        finish()
    }

    private fun getSection(): HeaderModel? {
        return intent?.getParcelableExtra("section")
    }

    private fun getRegisterId(): Int {
        return intent?.getIntExtra("registerId", -1) ?: -1
    }

    /**
     *  finish add collaborators
     */
    private fun addCollaborators() {
        startActivity(Intent(this, ReadBarcodeActivity::class.java).putExtra("showActivities", false))
        setResult(Activity.RESULT_OK)
        finish()
    }

    /**
     *  finish collaborator result
     */
    private fun finishBoxCount(result: Int) {
        setResult(result, Intent().putExtra("position", getPosition()).putExtra("registerId", getRegisterId()))
        finish()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_box
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = getString(R.string.count_box)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    private fun setData(data: MutableList<ItemBoxCountViewModel>) {
        (rv_box.adapter as BoxCountAdapter).addElements(data)
    }

    private fun setupRecycler() {
        rv_box.itemAnimator = DefaultItemAnimator()
        rv_box.adapter = BoxCountAdapter()
    }

    private fun isAddCollaboratorBox(): Boolean {
        return getType() == ADD_COLLABORATORS_BOX
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isAddCollaboratorBox()) {
            setResult(Activity.RESULT_OK)
        }
    }

}
