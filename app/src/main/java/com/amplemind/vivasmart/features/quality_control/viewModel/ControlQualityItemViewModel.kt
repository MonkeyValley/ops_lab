package com.amplemind.vivasmart.features.quality_control.viewModel

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.core.repository.model.TableGrooves
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import io.reactivex.Observable

class ControlQualityItemViewModel(
        item: StageModel,
        val flow: QualityControlViewModel.LOT_FLOW = QualityControlViewModel.LOT_FLOW.ROSTER_PRODUCTION
    )
    : BaseAdapter.ViewModel<StageModel>(item) {

    enum class LOT_STATUS(val type: Int) {
        HOUSE_CIRCLE(1),
        HOUSE_CIRCLE_COMPLETE(2),
        HOUSE_RECT(3),
        HOUSE_RECT_COMPLETE(4),
        HOUSE_CIRCLE_PENDING(5),
        HOUSE_RECT_PENDING(6);

        companion object {
            fun from(value: Int): LOT_STATUS = LOT_STATUS.values().first { it.type == value }
        }
    }

    lateinit var stage: StageModel

    var lot = item.lot

    var stageId = item.id

    var cropId = item.cropId
    var lotId = item.lotId
    var cropName = item.crop!!.name
    var cropVariety = item.crop!!.variety
    var businessId = item.lot!!.businessUnitId
    var businessName = item.lot!!.businessUnit?.name ?: ""
    var plantPerGroove = item.plantsPerGroove

    var position : Int? = null

    private var hasActivities = item.stageActivities

    var house: LOT_STATUS
        private set

    val tablesGrooves = createTablesList()

    var counter: Observable<Int>? = null
    var count = ObservableField<String>()
    var showCounter = ObservableInt(View.GONE)

    init {
        count.set("0")
    }

    private fun createTablesList(): ArrayList<TableGrooves> {
        val tables = arrayListOf<TableGrooves>()
        val lot = lot!!
        if (lot.table1 > 0) tables.add(TableGrooves(1,lot.table1))
        if (lot.table2 > 0) tables.add(TableGrooves(2,lot.table2))
        if (lot.table3 > 0) tables.add(TableGrooves(3,lot.table3))
        if (lot.table4 > 0) tables.add(TableGrooves(4,lot.table4))
        if (lot.table5 > 0) tables.add(TableGrooves(5,lot.table5))
        if (lot.table6 > 0) tables.add(TableGrooves(6,lot.table6))
        return tables
    }

    override fun bind(item: StageModel) {

        stage = item
        lot = item.lot ?: throw IllegalStateException("Stage with uuid ${item.uuid} has no associated LotModel")

        stageId = item.id

        cropId = item.cropId
        lotId = item.lotId
        cropName = item.crop?.name ?: "Sin cultivo"
        cropVariety = item.crop?.variety ?: "Sin variedad"
        businessId = item.lot?.businessUnitId ?: 0
        businessName = item.lot?.businessUnit?.name ?: "Sin empresa"
        plantPerGroove = item.plantsPerGroove
        hasActivities = item.activityLogCount > 0
        count.set(item.activityLogCount.toString())

        showCounter.set(if (item.activityLogCount > 0) View.VISIBLE else View.GONE)

        retrieveLotType()
    }

    fun retrieveLotType() {
        when(lot!!.lotTypeId) {
            1 -> {
                house = LOT_STATUS.HOUSE_CIRCLE
                if (isComplete()) {
                    house = LOT_STATUS.HOUSE_CIRCLE_COMPLETE
                } else if(isPending()) {
                    house = LOT_STATUS.HOUSE_CIRCLE_PENDING
                }
            }
            else -> {
                house = LOT_STATUS.HOUSE_RECT
                if (isComplete()) {
                    house = LOT_STATUS.HOUSE_RECT_COMPLETE
                } else if(isPending()) {
                    house = LOT_STATUS.HOUSE_RECT_PENDING
                }
            }
        }
    }

    init {
        when(lot!!.lotTypeId) {
            1 -> {
                house = LOT_STATUS.HOUSE_CIRCLE
                if (isComplete()) {
                    house = LOT_STATUS.HOUSE_CIRCLE_COMPLETE
                } else if(isPending()) {
                    house = LOT_STATUS.HOUSE_CIRCLE_PENDING
                }
            }
            else -> {
                house = LOT_STATUS.HOUSE_RECT
                if (isComplete()) {
                    house = LOT_STATUS.HOUSE_RECT_COMPLETE
                } else if(isPending()) {
                    house = LOT_STATUS.HOUSE_RECT_PENDING
                }
            }
        }
    }


    fun getLotType(): Int {
        return house.type
    }

    fun getLotName(): String? {
        return lot!!.name
    }

    fun isComplete(): Boolean {
        when(flow) {
            QualityControlViewModel.LOT_FLOW.QUALITY_PRODUCTION -> return lot!!.isQaDone == "Done"
            QualityControlViewModel.LOT_FLOW.ROSTER_PRODUCTION -> return lot!!.isClosed
            QualityControlViewModel.LOT_FLOW.QUALITY_CONTROL -> return lot!!.isQualityControlClosed
        }
    }

    fun isPending(): Boolean {
        when(flow) {
            QualityControlViewModel.LOT_FLOW.QUALITY_PRODUCTION -> return lot!!.isQaDone == "Pending"
            QualityControlViewModel.LOT_FLOW.ROSTER_PRODUCTION -> return hasActivities
            QualityControlViewModel.LOT_FLOW.QUALITY_CONTROL -> return false // This doesn't have a pending status, only open and close
        }
    }

}
