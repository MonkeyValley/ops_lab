package com.amplemind.vivasmart.features.sync_forced.repository

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.pollination.repository.responses.Pollinationrevisions
import com.amplemind.vivasmart.features.sync_forced.repository.remote.SyncForcedApi
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.remote.FertirriegoApi
import com.amplemind.vivasmart.vo_core.utils.currentISODate
import com.google.gson.Gson
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SyncForcedRepository @Inject constructor(private val api: SyncForcedApi,
                                               private val mLotFertirriegoApi: FertirriegoApi,
                                               private val preferences: UserAppPreferences) {

    var fromDate: String = currentISODate("yyyy-MM-dd")

    val businessUnitId = Gson().fromJson(preferences.userInfo,
            UserModel::class.java)?.businessUnitId ?: ""

    fun getCollaboratorsForced(lastVersion: Long?): Single<List<CollaboratorModel>> =
            api.getCollaborators(
                    preferences.authorizationToken,
                    businessUnitId = preferences.user.businessUnitId,
                    version = lastVersion)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version: ", lastVersion.toString())
                        Log.e("Data-Collaboratos: ", response.data.toString())
                        response.data
                    }

    fun getActivitiesForced(lastVersion: Long?): Single<List<ActivityModel>> =
            api.getActivities(
                    preferences.authorizationToken,
                    businessUnitId = preferences.user.businessUnitId,
                    version = lastVersion)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version: ", lastVersion.toString())
                        Log.e("Data-Activity: ", response.data.toString())
                        response.data
                    }

    fun getStagesForced(lastVersion: Long?): Single<List<StageModel>> =
            api.getStages(
                    preferences.authorizationToken,
                    businessUnitId = preferences.user.businessUnitId,
                    version = lastVersion)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version-Stages: ", lastVersion.toString())
                        Log.e("Data-Stages: ", response.data.toString())
                        response.data
                    }

    fun getPlaguesForced(lastVersion: Long?): Single<List<PlagueDetail>> =
            api.getPlagues(
                    preferences.authorizationToken,
                    version = lastVersion)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version-Plagues: ", lastVersion.toString())
                        Log.e("Data-Plagues: ", response.data.toString())
                        response.data
                    }

    fun getDataForPollination(lotId: Int): Single<List<Pollinationrevisions>> =
            api.getPollinationLastWeeks(
                    preferences.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        response.data
                    }

    fun getCodesForced(lastVersion: Long?, stageId: Long): Single<List<ActivityCodeModel>> =
            api.getCodes(
                    preferences.authorizationToken,
                    closedFrom = fromDate,
                    stageId = stageId,
                    version = lastVersion)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version: ", lastVersion.toString())
                        Log.e("Data-Codes: ", response.toString())
                        response
                    }

    fun getActivityLogsForCodeForced(lastVersion: Long?, activityCodeId: Long): Single<List<ActivityLogModel>> =
            api.getActivityLogsForCode(
                    preferences.authorizationToken,
                    activityCodeId = activityCodeId,
                    sinceDate = fromDate,
                    version = lastVersion)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version: ", lastVersion.toString())
                        Log.e("Data-ActivityLogs: ", response.toString())
                        response.data
                    }

    fun getGroovesForced(activityCodeId: Long): Single<List<GrooveModel>> =
            api.getGrooves(
                    preferences.authorizationToken,
                    activityCodeId = activityCodeId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("activityCodeId: ", activityCodeId.toString())
                        Log.e("Data-Groove: ", response.toString())
                        response
                    }

    fun getLotForFertirriego(): Single<List<LotFertirriegoModel>> =
            mLotFertirriegoApi.getLotFertirriego(
                    preferences.authorizationToken)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        deleteFertirriego()
                        Log.e("Data-LotFertirriego: ", response.data.toString())
                        getLotFromFertirriego(response.data)
                        response.data
                    }

    fun deleteFertirriego(){
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                realm.delete(LotFertirriegoModel::class.java)
                realm.delete(HydroponicModel::class.java)
                realm.delete(SoilModel::class.java)
                realm.delete(ValvesPerLotFertirriegoModel::class.java)
                realm.delete(ValveFertirriego::class.java)
                realm.delete(ValvesPerWeekFertirriegoModel::class.java)
                realm.delete(TablesPerDay::class.java)
                realm.delete(ValveForTable::class.java)
                realm.delete(PHCEPerWeekFertirriegoModel::class.java)
                realm.delete(TablesPHCEPerDay::class.java)
                realm.delete(PHCEForTable::class.java)
                realm.delete(PulsesPerWeekFertirriegoModel::class.java)
                realm.delete(TablesPulsePerDay::class.java)
                realm.delete(PulseForTable::class.java)
            }
        }
    }

    fun getLotFromFertirriego(data: List<LotFertirriegoModel>) {
        for(item in data){

            for(soil in item.soilList!!){
                getLotFromFertirriego(soil.lotId, "Soil")
                getValvesPerWeek(soil.lotId)
                getPHCEPerWeek(soil.lotId)
            }

            for(hydroponic in item.hydroponicList!!){
                getLotFromFertirriego(hydroponic.lotId, "hydroponic")
                getPHCEPerWeek(hydroponic.lotId)
                getPulsePerWeek(hydroponic.lotId)
            }

        }
    }

    fun getLotFromFertirriego(lotId: Int, type: String): Disposable =
            getValvesForFertirriego(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        insertValves(it, lotId, type)
                    }, {
                        Log.e("ValvesPerFertirriego", "error", it)
                    })

    fun getValvesForFertirriego(lotId: Int): Single<ValvesPerLotFertirriegoModel> =
            mLotFertirriegoApi.getValveLotFertirriego(
                    preferences.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Data-ValvesPerLot: ", response.data.toString())
                        response.data
                    }

    fun insertValves(item : ValvesPerLotFertirriegoModel?, lotId: Int, type: String) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.type = type
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }
    }

    fun getValvesPerWeek(lotId: Int): Disposable =
            getInfoMonitoringPerLot(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        insertValvesMonitoringPerWeek(it, lotId)
                    }, {
                        Log.e("ValvesPerWeekFer", "error", it)
                    })

    fun getInfoMonitoringPerLot(lotId: Int): Single<ValvesPerWeekFertirriegoModel> =
            mLotFertirriegoApi.getValesPerWeekFertirriego(
                    preferences.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Data-ValvesPerWeek: ", response.data.toString())
                        response.data
                    }

    fun insertValvesMonitoringPerWeek(item : ValvesPerWeekFertirriegoModel?, lotId: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }
    }

    fun getPHCEPerWeek(lotId: Int): Disposable =
            getInfoPHCEPerLot(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        insertValvesMonitoringPerWeek(it, lotId)
                    }, {
                        Log.e("PHCEPerWeekFer", "error", it)
                    })

    fun getInfoPHCEPerLot(lotId: Int): Single<PHCEPerWeekFertirriegoModel> =
            mLotFertirriegoApi.getPHCEPerWeekFertirriego(
                    preferences.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Data-PHCEPerWeek: ", response.data.toString())
                        response.data
                    }

    fun insertValvesMonitoringPerWeek(item : PHCEPerWeekFertirriegoModel?, lotId: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }
    }


    fun getPulsePerWeek(lotId: Int): Disposable =
            getInfoMonitoringPulsePerLot(lotId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        insertPulseMonitoringPerWeek(it, lotId)
                    }, {
                        Log.e("ValvesPerWeekFer", "error", it)
                    })

    fun getInfoMonitoringPulsePerLot(lotId: Int): Single<PulsesPerWeekFertirriegoModel> =
            mLotFertirriegoApi.getPulsePerWeekFertirriego(
                    preferences.authorizationToken,
                    lotId = lotId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Data-PulsesPerWeek: ", response.data.toString())
                        response.data
                    }

    fun insertPulseMonitoringPerWeek(item : PulsesPerWeekFertirriegoModel?, lotId: Int) {
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                if (item != null) {
                    item.lotId = lotId
                    realm.insertOrUpdate(item)
                }
            }
        }
    }

    fun getProductionRanges(): Single<List<ProductionRangeVariatiesModel>> =
            api.getProductionRangeVarieties(
                    preferences.authorizationToken,
                    businessId = preferences.user.businessUnitId)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        deleteProductionRange()
                        Log.e("Data-ProductionRanges: ", response.data.toString())
                        response.data
                    }

    private fun deleteProductionRange(){
        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                realm.delete(ProductionRangeVariatiesModel::class.java)
                realm.delete(ProductionRangesOption::class.java)
                realm.delete(ProductionVarDataType::class.java)
                realm.delete(ProductionVarType::class.java)
                realm.delete(SoilType::class.java)
                realm.delete(Crops::class.java)
                realm.delete(Crop::class.java)
                realm.delete(ActivitySubCategory::class.java)
            }
        }
    }

    fun getDailyPay(lastVersion: Long?): Single<List<DailyPayModel>> =
            api.getDailyPay(
                    preferences.authorizationToken,
                    date = getTodayDate(),
                    businessId = preferences.user.businessUnitId,
                    version = lastVersion)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version-DailyPay: ", lastVersion.toString())
                        Log.e("Data-DailyPay: ", response.data.toString())
                        response.data
                    }

    fun getTodayDate(): String {
        val calendar = Calendar.getInstance()
        val month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        val day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        return ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
    }

    fun getStageForProductionRangeForced(lastVersion: Long?): Single<List<StageModel>> =
            api.getStageForProductionRange(
                    preferences.authorizationToken,
                    businessUnitId = preferences.user.businessUnitId,
                    version = lastVersion)
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
                    .map { response ->
                        Log.e("Version-Stages: ", lastVersion.toString())
                        Log.e("Data-Stages: ", response.data.toString())
                        response.data
                    }

}