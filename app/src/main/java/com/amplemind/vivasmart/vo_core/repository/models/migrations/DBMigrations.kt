package com.amplemind.vivasmart.vo_core.repository.models.migrations

import io.realm.DynamicRealm
import io.realm.RealmMigration

class DBMigrations : RealmMigration {

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {

        val execs = MigrationExecutable.getExecutables(oldVersion, newVersion)

        execs.forEach { exec ->
            if (oldVersion < newVersion) {
                exec.run(realm)
            }
            else {
                exec.drop(realm)
            }
        }

    }

}