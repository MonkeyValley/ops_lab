package com.amplemind.vivasmart.features.production_roster.adapters.package_roster

interface ControlAdapterPackage {
    fun onNotifyItemChanged(position: Int)
    fun onRemoveItem(position: Int)
    fun onNotifyDataSetChange()
}
