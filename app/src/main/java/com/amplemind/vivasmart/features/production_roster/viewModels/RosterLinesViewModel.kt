package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.extensions.registerObserver
import com.amplemind.vivasmart.core.extensions.registerSingle
import com.amplemind.vivasmart.core.repository.RosterRepository
import com.amplemind.vivasmart.core.repository.model.RosterLineModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class RosterLinesViewModel @Inject constructor(private val repository: RosterRepository,
                                               private val apiErrors: HandleApiErrors) {

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val searchLocalLines = BehaviorSubject.create<Boolean>()
    private val requestFail = BehaviorSubject.create<String>()
    private val getLines = BehaviorSubject.create<List<ItemRosterLinesViewModel>>()

    private val linesList = mutableListOf<ItemRosterLinesViewModel>()

    fun getLines(): Disposable {
        return repository.getLines()
                .registerObserver(progressStatus)
                .map {
                    repository.saveLine(it.lines)
                    mapModelToViewModel(it.lines)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    linesList.clear()
                    linesList.addAll(it)
                }
                .subscribe({
                    linesList.clear()
                    linesList.addAll(it)
                    getLines.onNext(linesList)
                }, {
                    searchLocalLines.onNext(true)
                })
    }

    private fun mapModelToViewModel(list: List<RosterLineModel>): List<ItemRosterLinesViewModel> {
        val viewModel = mutableListOf<ItemRosterLinesViewModel>()
        list.forEach { line ->
            viewModel.add(ItemRosterLinesViewModel(line))
        }
        return viewModel
    }

    fun onSearchLocalLines(): BehaviorSubject<Boolean> {
        return searchLocalLines
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onGetLines(): BehaviorSubject<List<ItemRosterLinesViewModel>> {
        return getLines
    }

    fun getLocalLines(): Disposable {
        return repository.geLocalLines()
                .registerSingle(progressStatus)
                .map(this::mapModelToViewModel)
                .subscribe({
                    linesList.clear()
                    linesList.addAll(it)
                    getLines.onNext(linesList)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })

    }

}