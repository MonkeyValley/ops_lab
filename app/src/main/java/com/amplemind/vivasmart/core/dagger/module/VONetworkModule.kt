package com.amplemind.vivasmart.core.dagger.module

import com.amplemind.vivasmart.vo_core.repository.remote.*
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class VONetworkModule {

    @Provides
    @Singleton
    internal fun provideCollaboratorsApi(retrofit: Retrofit): CollaboratorsApi {
        return retrofit.create(CollaboratorsApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideStagesApi(retrofit: Retrofit): StagesApi {
        return retrofit.create(StagesApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideActivitiesApi(retrofit: Retrofit): PayrollActivitiesApi {
        return retrofit.create(PayrollActivitiesApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideActivityCodesApi(retrofit: Retrofit): ActivityCodeApi {
        return retrofit.create(ActivityCodeApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideMessageApi(retrofit: Retrofit): MessageApi {
        return retrofit.create(MessageApi::class.java)
    }

    @Provides
    @Singleton
    internal fun providePackagingQualityApi(retrofit: Retrofit): PackagingQualityApi {
        return retrofit.create(PackagingQualityApi::class.java)
    }
}