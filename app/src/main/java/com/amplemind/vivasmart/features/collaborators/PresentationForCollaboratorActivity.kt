package com.amplemind.vivasmart.features.collaborators

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.EditText
import android.widget.SearchView
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.*
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import com.amplemind.vivasmart.core.utils.COLLABORATORS
import com.amplemind.vivasmart.core.utils.HAS_BOXCOUNT
import com.amplemind.vivasmart.core.utils.LOCAL_BROADCAST_USER_PACKING_RECEIVED
import com.amplemind.vivasmart.features.collaborators.adapter.AddActivityForCollaboratorAdapter
import com.amplemind.vivasmart.features.collaborators.viewModel.AddPresentationViewModel
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemAddActivitiesViewModel
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity.Companion.CHANGE_PRESENTATION
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.content_add_presentation_activity.*
import javax.inject.Inject

class PresentationForCollaboratorActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: AddPresentationViewModel

    var item: ItemAddActivitiesViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_add_presentation_activity)

        setupToolbar()

        setupSearch()

        initRecycler()
        addActionsButtons()

        viewModel.onSearchLocalPresentation().subscribe(this::loadLocalPresentation).addTo(subscriptions)
        viewModel.getData().subscribe(this::setData).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onUpdateSuccess().subscribe(this::updateSuccess).addTo(subscriptions)
        viewModel.onAssingCollaboratorSuccess().subscribe(this::successAssing).addTo(subscriptions)

        loadPresentation()

        showCurrentPresentation()
    }

    private fun loadPresentation(){
        if (true){
            viewModel.getPresentations().addTo(subscriptions)
        }else{
            loadLocalPresentation(true)
        }
    }

    private fun loadLocalPresentation(show : Boolean) {
        if (show){
            view_presentation.snackWithoutInternetMessage()
            viewModel.getLocalPresentations().addTo(subscriptions)
        }
    }

    private fun showCurrentPresentation(){
        if (getNameCurrentPresentation() != null){
            current_presentation.text = getNameCurrentPresentation()
        }else{
            ln_presentation.visibility = View.GONE
        }
    }

    private fun successAssing(result: CollaboratorsInLinesResult) {
        val intent = Intent(LOCAL_BROADCAST_USER_PACKING_RECEIVED)

        intent.putExtra(COLLABORATORS, result.data as java.util.ArrayList<out Parcelable>)
        intent.putExtra(HAS_BOXCOUNT, result.has_boxcount)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        setResult(200)
        finish()
    }

    private fun updateSuccess(name: String) {
            setResult(CHANGE_PRESENTATION, Intent().putExtra("registerId", getRegisterId()).putExtra("name_presentation",name))
            finish()
    }

    private fun addActionsButtons() {
        btn_add_new.addThrottle().subscribe {
            /*
            if (getRegisterId() == -1) {
                viewModel.assignCollaborator(isOnlineMode(this),getIdsCollaborator(), item?.presentation, getActivityId(), getNameActivity(),getCollaboratoLeft())?.addTo(subscriptions)
            } else {
                viewModel.updateCollaborator(isOnlineMode(this), getRegisterId(), item?.presentation).addTo(subscriptions)
            }

             */
        }.addTo(subscriptions)

        cv_cancel.addThrottle().subscribe {
            finish()
        }.addTo(subscriptions)
    }

    private fun setData(data: List<ItemAddActivitiesViewModel>) {
        if(data.isEmpty()){
            ll_list_empty_presentation.visibility = View.VISIBLE
            cardView2.visibility = View.GONE
        }
        if(data.size  == 1) {
            onEnableButton(data.first())
        }
        (rv_add_presentation.adapter as AddActivityForCollaboratorAdapter).addElements(data)
    }

    private fun initRecycler() {
        rv_add_presentation.hasFixedSize()
        rv_add_presentation.layoutManager = LinearLayoutManager(this)
        rv_add_presentation.itemAnimator = DefaultItemAnimator()
        rv_add_presentation.adapter = AddActivityForCollaboratorAdapter()
        (rv_add_presentation.adapter as AddActivityForCollaboratorAdapter).onClickItem().subscribe(this::onEnableButton).addTo(subscriptions)
    }

    private fun onEnableButton(item: ItemAddActivitiesViewModel) {
        this.item = item
        btn_add_new.isEnabled = item.status
    }

    private fun setupSearch() {
        //Set SearView EditText padding to match the title padding
        search_view_presentation.findViewById<EditText>(androidx.appcompat.R.id.search_src_text).searchViewPadding()
        search_view_presentation.setOnQueryTextListener(object : SearchView.OnQueryTextListener, androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.searchPresentation(query ?: "")
                hideKeyboard(this@PresentationForCollaboratorActivity)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.searchPresentation(newText ?: "")
                return true
            }
        })
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_add_presentation
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = getName()
        mToolbar.findViewById<CircleImageView>(R.id.ci_profile).setImageDrawable(R.drawable.ic_user)
        supportActionBar!!.title = ""
    }


    private fun getNameCurrentPresentation() = intent.getStringExtra("curren_presentation")

    private fun getNameActivity() = intent.getStringExtra("name_activity") ?: ""

    private fun getName() = intent.getStringExtra("name") ?: ""

    //private fun getIdsCollaborator() = intent.getParcelableArrayListExtra<CollaboratorModel>("list_collaborators")

    private fun getCollaboratoLeft() = intent.getIntExtra("collaborator_left", -1)

    private fun getActivityId() = intent.getIntExtra("activity_id", -1)

    private fun getRegisterId() = intent.getIntExtra("registerId", -1)

}


