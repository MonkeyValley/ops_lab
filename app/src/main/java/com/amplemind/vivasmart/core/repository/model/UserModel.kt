package com.amplemind.vivasmart.core.repository.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class UserModel(@SerializedName("birthdate") val birthdate: String?,
                     @SerializedName("email") val email: String,
                     @SerializedName("id") @ColumnInfo(name = "user_id") @PrimaryKey  val id: Int,
                     @SerializedName("image") var image: String?,
                     @SerializedName("is_active") val is_active: Boolean,
                     @SerializedName("name") val name: String,
                     @SerializedName("business_unit_id") @ColumnInfo(name = "business_unit_id_user") val businessUnitId: String?,
                     @SerializedName("tutorial") val tutorial: Boolean,
                     @SerializedName("role") @Embedded val role: RoleModel?,
                     @SerializedName("role_id") val role_id: Int,
                     @SerializedName("firebase_token") var firebasetoken: String?)