package com.amplemind.vivasmart.features.interfaces

import android.graphics.Bitmap

interface OnSignatureCompleteListener {

    fun completeSign(sign1: Bitmap, sign2: Bitmap? = null)

    fun onDismiss()

}