package com.amplemind.vivasmart.features.production_roster

import android.os.Bundle
import com.amplemind.vivasmart.core.repository.PayrollRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ActivitiesModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PayrollActivitiesViewModelNew @Inject constructor(private val repository: PayrollRepository, private val preferences: UserAppPreferences,
                                                        private val apiErrors: HandleApiErrors) {

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val offLineMode = BehaviorSubject.create<Boolean>()
    private val requestFail = BehaviorSubject.create<String>()

    private val list = BehaviorSubject.create<List<ItemActivitiesPayrollViewModelNew>>()

    private var lotId: Int? = null
    private var stageId: Int? = null

    private val noFoundActivities = BehaviorSubject.create<Boolean>()

    fun getTitle(title : String) : String{
        if (title.length > 21){
            return "${title.removeRange(20, title.length)}..."
        }
        return title
    }

    /**
     *  it is necessary to bring the stage first to be able to find the activities
     */
    fun loadChooseActivities(): Disposable {
        return repository.getActivities(preferences.token, lotId!!, stageId!!)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(Schedulers.io())
                .map {
                    repository.saveLocalActivities(it.data)
                    mapModelToViewModels(it.data, stageId!!)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    list.onNext(it)
                }
                .subscribe({}, {
                    list.onNext(listOf())
                })
    }

    fun getLocalActivities(): Disposable {
        return repository.getLocalActivities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it ->
                    val filtered = mutableListOf<ActivitiesModel>()
                    for (activity in it) {
                        if (activity.crops != null && activity.crops.any { it.cropId == lotId!! }) {
                            filtered.add(activity)
                        }
                    }
                    mapModelToViewModels(filtered, stageId!!)
                }
                .subscribe({
                    offLineMode.onNext(true)
                    if (it.isNotEmpty()) {
                        list.onNext(it)
                    }
                    noFoundActivities.onNext(it.isEmpty())
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun noFoundActivities(): BehaviorSubject<Boolean> {
        return noFoundActivities
    }

    fun getActivities(): BehaviorSubject<List<ItemActivitiesPayrollViewModelNew>> {
        return list
    }

    fun mapModelToViewModels(models: List<ActivitiesModel>, stageId: Int): List<ItemActivitiesPayrollViewModelNew> {
        val list = arrayListOf<ItemActivitiesPayrollViewModelNew>()
        models.forEach {
            list.add(ItemActivitiesPayrollViewModelNew(it, stageId))
        }
        return list
    }

    fun loadArguments(arguments: Bundle) {
        lotId = arguments.getInt("lot_id")
        stageId = arguments.getInt("stage_id")

    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onOffLineMode(): BehaviorSubject<Boolean> {
        return offLineMode
    }
}
