package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName

data class HarvestValidationModel (
        @SerializedName("carry_order_id")   val carryOrderId: Long,
        @SerializedName("issues")           val issues: List<IssueModel>,
        @SerializedName("temperature")      val temperature: Double = 0.0,
        @SerializedName("comment")          val comments: String = "",
        @SerializedName("quality_comment")  val qualityComment: String = ""
) {
    data class IssueModel (
            @SerializedName("issue_id")     val issueId: Long,
            @SerializedName("is_ok")        var isOk: Boolean = false,
            @SerializedName("issue")        var issue: com.amplemind.vivasmart.vo_core.repository.models.realm.IssueModel? = null
    )
}