package com.amplemind.vivasmart.features.production_quality.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemQualityControlListBinding
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlListItemViewModel
import io.reactivex.subjects.PublishSubject

class QualityControlListAdapter constructor(context: Context, val showScore:Boolean=true) : RecyclerView.Adapter<QualityControlListAdapter.ControlListViewHolder>() {

    private var list = listOf<ControlListItemViewModel>()

    private val clickSubject = PublishSubject.create<ControlListItemViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ControlListViewHolder {
        val binding = ItemQualityControlListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ControlListViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ControlListViewHolder, position: Int) {
        holder.bind(list[position])
    }


    fun addElements(data : List<ControlListItemViewModel>) {
        list = data
        notifyDataSetChanged()
    }

    fun onClickEvent(): PublishSubject<ControlListItemViewModel> {
        return clickSubject
    }


    inner class ControlListViewHolder(private val binding : ItemQualityControlListBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item : ControlListItemViewModel

        fun bind(item : ControlListItemViewModel){
            binding.setVariable(BR.viewItem, item)
            binding.setVariable(BR.showScore, showScore)
            binding.executePendingBindings()
            this.item = item
            binding.root.setOnClickListener {
                clickSubject.onNext(this.item)
            }
        }

    }

}

