package com.amplemind.vivasmart.core.repository.response

import com.google.gson.annotations.SerializedName

data class UnitResponse(
        @SerializedName("data") val data: List<UnitResponseResult>,
        @SerializedName("total_count") val total_count: Int
)

data class UnitResponseResult(
        @SerializedName("id") val id: Int,
        @SerializedName("business_unit_id") val business_unit_id: Int,
        @SerializedName("business_unit_names") val business_unit_names: String,
        @SerializedName("name") val name: String? = null
)

