package com.amplemind.vivasmart.domain.reciba

import android.util.Log
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.platform.SyncNotification
import com.amplemind.vivasmart.core.platform.SynchronizeDataBaseService
import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.RecibaDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ProductionQualityOfflineRequest
import com.amplemind.vivasmart.core.repository.model.SyncReportModel
import com.amplemind.vivasmart.core.repository.model.UpdateCarryOrderSync
import com.amplemind.vivasmart.core.repository.request.CreateIssueRequest
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.PicturesHelper
import org.jetbrains.anko.doAsync
import java.io.File
import javax.inject.Inject

class UploadReciba @Inject constructor(private val picturesHelper: PicturesHelper,
                                       private val uploadRepository: ProfileRepository,
                                       private val preferences: UserAppPreferences,
                                       private val database: RecibaDao,
                                       private val repository: PackagingQualityRepository) {

    private var delegate: SyncNotification? = null
    private val type = SynchronizeDataBaseService.SynchronizeRequest.RECIBA
    private var subscriptions: AndroidDisposable? = null

    fun syncReciba(delegate: SyncNotification, subscriptions: AndroidDisposable) {
        this.delegate = delegate
        this.subscriptions = subscriptions

        doAsync({
            Log.e("Error", "syncReciba ${it.message}")
            delegate.onTerminate(mutableListOf(), type, "syncReciba ${it.message}")
        }, {
            uploadSigns(database.getCarryOrderReadyToSync(), 0, true)
        })
    }

    private fun uploadSigns(reports: List<UpdateCarryOrderSync>, position: Int, signCollaborator: Boolean) {
        if (position < reports.size) {
            val sign = if (signCollaborator) reports[position].signCollaborator else reports[position].signSuper
            val signPosition = if (signCollaborator) position else position + 1
            if (!File(sign).exists()) {
                uploadSigns(reports, signPosition, !signCollaborator)
                return
            }
            uploadImage(sign,
                    { path ->
                        if (signCollaborator) {
                            reports[position].signCollaborator = path
                        } else {
                            reports[position].signSuper = path
                        }
                        uploadSigns(reports, signPosition, !signCollaborator)
                    },
                    { error -> uploadSigns(reports, signPosition, !signCollaborator) })
        } else {
            syncRecibaReports(reports)
        }
    }

    private fun uploadImage(image: String, complete: (String) -> Unit, error: (String) -> Unit) {
        val file = File(image)
        picturesHelper.compressImage(file, file, 400f, 400f)
        uploadRepository.uploadImage(file, "user")
                .subscribe({
                    complete(it.relative_url)
                }, {
                    error(it.message ?: "")
                }).addTo(subscriptions!!)
    }

    private fun syncRecibaReports(reports: List<UpdateCarryOrderSync>) {
        repository.syncCarryOrders(reports)
                .subscribe({ reportsSent ->
                    delegate?.onTerminate(reportsSent.data.map {
                        SyncReportModel("Orden ${it.carryOrderId}", 1, if (it.status) 1 else 0, it.carryOrderId)
                    }, type)
                    deleteSyncReports()
                }, {
                    Log.e("Fail upload", "")
                    val response = reports.map {
                        SyncReportModel("Orden ${it.carryOrderId}", 1, 0, it.carryOrderId)
                    }
                    delegate?.onTerminate(response, type)
                }).addTo(subscriptions!!)
    }

    private fun deleteSyncReports() {
        doAsync({
            Log.e("Error", "deleteSyncReports ${it.message}")
            delegate?.onTerminate(mutableListOf(), type, "deleteSyncReports ${it.message}")
        }, { database.deleteAllCarryOrderReadyToSync() })
    }
}