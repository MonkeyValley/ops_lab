package com.amplemind.vivasmart.features.reciba_quality.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.FinishedProductReportModel
import com.amplemind.vivasmart.databinding.ItemReportQualityReviewDetailBinding
import com.amplemind.vivasmart.features.production_quality.ItemReportQualityReviewDetailViewModel
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ReacibaQualityReviewViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.BehaviorSubject
import java.math.RoundingMode

class RecibaQualityIssuesAdapter constructor(val list: MutableList<ItemReportQualityReviewDetailViewModel>, var objFinished: ReacibaQualityReviewViewModel, var newReport: Boolean) : RecyclerView.Adapter<RecibaQualityIssuesAdapter.RecibaQualityIssuesAdapterViewHolder>() {
    private val onClickSubject = BehaviorSubject.create<ItemReportQualityReviewDetailViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecibaQualityIssuesAdapterViewHolder {
        val binding = ItemReportQualityReviewDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RecibaQualityIssuesAdapterViewHolder(binding, objFinished)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecibaQualityIssuesAdapterViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<ItemReportQualityReviewDetailViewModel> {
        return onClickSubject
    }

    fun isEmpty() = list.isEmpty()

    inner class RecibaQualityIssuesAdapterViewHolder(private val binding: ItemReportQualityReviewDetailBinding, private var objFinished: ReacibaQualityReviewViewModel) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ItemReportQualityReviewDetailViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
//                binding.cvUser.setOnClickListener {
//                    onClickSubject.onNext(item)
//                }
//                binding.cvUser.setOnLongClickListener {
//                    onClickSubject.onNext(item)
//                    return@setOnLongClickListener true
//                }
                binding.TVNameIssue.setOnClickListener {
                    onClickSubject.onNext(item)
                }
                binding.damageMinus.setOnClickListener {
                    var count = Integer.parseInt(item.frequency_no)
                    if (count != 0) {
                        count--
                        item.frequency_no = count.toString()
                        item.percent = calPercent(count)
//                        setColor(calPercent(count))
                        notifyDataSetChanged()
                    }
                }
                binding.damagePlus.setOnClickListener {
                    var count = Integer.parseInt(item.frequency_no)
                    count++
                    val sample = with(objFinished) { this.sample }
                    if (count <= sample) {
                        item.frequency_no = count.toString()
                        item.percent = calPercent(count)
//                        setColor(calPercent(count))
                        notifyDataSetChanged()
                    }
                }

                val newRep = with(newReport) { this }
                if(!newRep){
                    binding.damageMinus.visibility = View.GONE
                    binding.damagePlus.visibility = View.GONE
                }
            }
        }

        fun setColor(percent: Double){
            if (percent >= 94) {
                binding.TVPercentIssueDetail.setBackgroundColor(binding.root.resources.getColor(R.color.redAlert))
            } else if (percent > 89 && percent <= 93 ) {
                binding.TVPercentIssueDetail.setBackgroundColor(binding.root.resources.getColor(R.color.backVanishedRed))
            }
            else{
                binding.TVPercentIssueDetail.setBackgroundColor(binding.root.resources.getColor(R.color.backRed))
            }

        }

        private fun calPercent(cant: Int): String {
            val sample = with(objFinished) { this.sample }
            val percent: Double =  ((cant * 100.00) / sample)
            return percent.toBigDecimal().setScale(2, RoundingMode.CEILING).toString() + "%"
        }

    }

}