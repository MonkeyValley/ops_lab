package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class UpdateCarryOrderSync(@SerializedName("box_no") val boxNo: Double,
                                @SerializedName("average_unit") val averageUnit: Int,
                                @SerializedName("average_weight") val averageWeight: Double,
                                @SerializedName("temperature") val temperature: Double,
                                @SerializedName("sign_super_reception") var signSuper: String,
                                @SerializedName("sign_collaborator_reception") var signCollaborator: String,
                                @SerializedName("carry_order_id") val carryOrderId: Int,
                                @SerializedName("status") val status: Boolean = false,
                                @SerializedName("offline_id") @PrimaryKey val id: Int? = null)

data class UpdateCarryOrderSyncRequest(@SerializedName("bulk") val data: List<UpdateCarryOrderSync>)
data class UpdateCarryOrderSyncResponse(@SerializedName("data") val data: List<UpdateCarryOrderSync>)