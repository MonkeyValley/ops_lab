package com.amplemind.vivasmart.features.collaborators.viewModel

import com.amplemind.vivasmart.features.collaborators.adapter.CollaboratorsDatabaseAdapter
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import javax.inject.Inject

data class ItemProgressMore(val message: String)

class ItemCollaboratorsViewModel @Inject constructor(val model: CollaboratorModel) {

    val name = model.name
    val id = model.id
    val url = model.image
    val birthday = model.birthdate
    var position: Int? = null

    val employeeCode = model.employeeCode

    var state = CollaboratorsDatabaseAdapter.STATE.NORMAL

    var model_data = model

    fun changeState() {
        state = if (state == CollaboratorsDatabaseAdapter.STATE.NORMAL) {
            CollaboratorsDatabaseAdapter.STATE.SELECTED
        } else {
            CollaboratorsDatabaseAdapter.STATE.NORMAL
        }
    }

    fun isSelected(): Boolean {
        return state == CollaboratorsDatabaseAdapter.STATE.SELECTED
    }

}