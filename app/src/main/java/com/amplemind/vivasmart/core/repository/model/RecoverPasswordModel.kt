package com.amplemind.vivasmart.core.repository.model

class RecoverPasswordModel() {

    data class RecoverPasswordResponse(val msg: String)

}