package com.amplemind.vivasmart.features.mipe.fragment


import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.fertiriego.FertirriegoLotActivity
import com.amplemind.vivasmart.features.mipe.MipeActivitiesActivity
import com.amplemind.vivasmart.features.mipe.MipeMonitoringLotsActivity
import com.amplemind.vivasmart.features.mipe.viewModel.MenuMipeActivitiesViewModel
import com.amplemind.vivasmart.features.phenology.adapters.ActivitiesPhenologyAdapter
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModel
import kotlinx.android.synthetic.main.content_payroll_activities_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

@SuppressLint("Registered")
class MenuActivitiesMipeFragment : BaseFragment() {

    @Inject
    lateinit var viewModelMenu: MenuMipeActivitiesViewModel

    @Inject
    lateinit var prefer: UserAppPreferences

    private var section: String? = null

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    fun newInstance(title: String): MenuActivitiesMipeFragment {
        val args = Bundle()
        args.putString("Title", title)

        val fragment = MenuActivitiesMipeFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_menu_activities_mipe, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()

        viewModelMenu.setupData(arguments!!)
        viewModelMenu.getToolbarTitle().subscribe {
            section = it
            lbl_track_title.text = "/ "+it+" /"
            (activity as MipeActivitiesActivity).supportActionBar!!.title = it
            viewModelMenu.loadMenuActivities().addTo(subscriptions)
            viewModelMenu.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)

        }.addTo(subscriptions)
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_menu_activities_payroll.adapter as ActivitiesPhenologyAdapter).addElements(activities)
        (rv_menu_activities_payroll.adapter as ActivitiesPhenologyAdapter).onClickItem().subscribe(this::clickActivityPhenology).addTo(subscriptions)
    }

    private fun clickActivityPhenology(item: ItemActivitiesPayrollViewModel) {
        val now = System.currentTimeMillis()
        val bndlanimation = ActivityOptions.makeCustomAnimation(context, R.anim.enter, R.anim.exit).toBundle()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now
        when (item.id) {
            "0" -> {
                startActivity(Intent(context, MipeMonitoringLotsActivity::class.java)
                        .putExtra("Title", "MIPE")
                        .putExtra(MipeMonitoringLotsActivity.PARAM_TYPE, "1")
                        .putExtra(MipeMonitoringLotsActivity.PARAM_TITTLE, "Lotes")
                        ,bndlanimation)
            }
            else -> {  }
        }
    }

    private fun initRecycler() {
        rv_menu_activities_payroll.hasFixedSize()
        rv_menu_activities_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_activities_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_activities_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_activities_payroll.adapter = ActivitiesPhenologyAdapter()
    }

    companion object {
        val TAG = MenuActivitiesMipeFragment::class.java.simpleName
    }

}