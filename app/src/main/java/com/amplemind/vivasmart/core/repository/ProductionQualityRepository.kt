package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.extensions.dummyDetectionTypes
import com.amplemind.vivasmart.core.extensions.dummyGreenhouseData
import com.amplemind.vivasmart.core.repository.local.LotsDao
import com.amplemind.vivasmart.core.repository.local.ProductionQualityDao
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.remote.ProductionQualityApi
import com.amplemind.vivasmart.core.repository.request.*
import com.amplemind.vivasmart.core.repository.response.IncidentsResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.production_quality.ProductionCategoriesItemViewModel
import io.reactivex.Observable
import org.jetbrains.anko.doAsync
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class ProductionQualityRepository @Inject constructor(private val api : ProductionQualityApi,
                                                           private val database: ProductionQualityDao,
                                                           private val databaseLots: LotsDao) {

    @Inject
    lateinit var  authorization: ApiAuthorization

    fun getGreenhouses() : Observable<List<ProductionQualityModel>> {
        return Observable.just(dummyGreenhouseData())
    }

    fun getDetectionTypes() : Observable<List<CategoriesDetailModel>> {
        return Observable.just(dummyDetectionTypes())
    }


    fun getActivities(token: String): Observable<List<ProductionCategoriesModel>> {
        //This is activities are supposed to be the same always, to avoid another request this are returned like this
        val activities = mutableListOf(
                ProductionCategoriesModel(1, "Cosecha", 0),
                ProductionCategoriesModel(2, "Mantenimiento", 0),
                ProductionCategoriesModel(3, "Fertiriego", 0),
                ProductionCategoriesModel(4, "MIPE", 0),
                ProductionCategoriesModel(5, "Labores Culturales", 0),
                ProductionCategoriesModel(6, "Inocuidad", 0)
        )
        return Observable.just(activities)
    }

    fun getCountIssues(token: String, lotId: Int): Observable<ProductionCategoriesResponse> {
        return api.getCountIssues(authorization.getAuthToken(token), lotId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getLocalCountIssues(lotId: Int): Observable<ProductionCategoriesResponse> {
        return Observable.fromCallable {
            ProductionCategoriesResponse(database.getCategories(lotId))
        }
    }

    fun saveCategories(lotId: Int, categories: MutableList<ProductionCategoriesModel>, allCategories: List<ProductionCategoriesItemViewModel>) {
        doAsync {
            allCategories.map { category ->
                if (categories.filter { it.id == category.id }.isEmpty()) {
                    categories.add(ProductionCategoriesModel(category.id, category.name, category.count, lotId))
                }
            }
            categories.map { it.stageId = lotId }
            database.insertCategories(categories)
        }
    }

    fun finishActivities(token: String, signCollaborator: String, signSuper: String, lotId: Int, categoryId: Int): Observable<Any> {
        val request = ProductionQulitySignsRequest(signSuper, signCollaborator)
        return api.finishActivities(authorization.getAuthToken(token), lotId, categoryId, request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun finishLocalActivity(signCollaborator: String, signSuper: String, stageId: Int): Observable<Any> {
        return Observable.fromCallable {
            database.insertProductionQualityOfflineRequest(ProductionQualityOfflineRequest(signSuper, signCollaborator, stageId))
            databaseLots.closeQualityLot(stageId)
        }
    }

    fun getIssues(token: String, id_category: Int, id_lot : Int) : Observable<IssuesByCategoryResponse> {
        return api.getIssues(authentication = authorization.getAuthToken(token), sub_category_id__eq = id_category, lot_id = id_lot)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun saveIssues(categoryId: Int, lotId: Int, list: List<IssuesResultResponse>) {
        doAsync {
            list.map {
                it.categoryId = categoryId
                it.stageId = lotId
            }
            database.insertIssues(list)
        }
    }

    fun getLocalIssues(id_category: Int, id_lot : Int) : Observable<IssuesByCategoryResponse> {
        return Observable.fromCallable {
            IssuesByCategoryResponse(database.getIssues(id_category, id_lot))
        }
    }

    fun createIssue(token: String, description: String, stageId: Int, issueId: Int, tableNo: Int, grooveNum: Int, image: String) : Observable<Any> {
        val request = CreateIssueRequest(description, stageId, issueId, tableNo, grooveNum, image, null)
        return api.createIssue(authorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun createLocalIssue(description: String, stageId: Int, issueId: Int, tableNo: Int, grooveNum: Int, image: String, categoryId: Int, lotId: Int): Observable<Any> {
        val issue = CreateIssueRequest(description, stageId, issueId, tableNo, grooveNum, image, null, categoryId)
        return Observable.fromCallable {
            database.insertIssuePendingToSend(issue)
            database.incrementCountInCategory(categoryId, 1)
            database.incrementCountIssues(issueId, 1)
        }
    }

    fun getIssuesReport(token: String, lotId: Int): Observable<ProductionCategoriesReportResponse> {
        return api.getIssuesReport(authorization.getAuthToken(token), lotId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun saveIssuesReportes(reports: ProductionCategoriesReportResponse) {
        doAsync {
            if (reports.data.table1 != null) {
                database.insertReportIssues(reports.data.table1)
            }
            if (reports.data.table2 != null) {
                database.insertReportIssues(reports.data.table2)
            }
            if (reports.data.table3 != null) {
                database.insertReportIssues(reports.data.table3)
            }
            if (reports.data.table4 != null) {
                database.insertReportIssues(reports.data.table4)
            }
            if (reports.data.table5 != null) {
                database.insertReportIssues(reports.data.table5)
            }
            if (reports.data.table6 != null) {
                database.insertReportIssues(reports.data.table6)
            }
        }
    }

    private fun getLocalIssuesByTable(table: Int, lotId: Int, syncFlow: Boolean): List<ProductionCategoriesTable> {
        val reports = mutableListOf<ProductionCategoriesTable>()
        reports.addAll(database.getReportIssuesByTable(table,lotId))
        reports.addAll(database.getIssuesPendingToSendByTable(table, lotId, syncFlow).map {
            ProductionCategoriesTable(it.description, it.grooveNo, 0, it.dbPrimaryKey ?: 0, it.image, true, it.issueId,
                    "", "", it.stageId, it.categoryId.toString(), it.tableNo, isLocalModel = true, readOnly = syncFlow)
        })
        return reports
    }

    fun getLocalIssuesReport(lotId: Int, syncFlow: Boolean): Observable<ProductionCategoriesReportResponse> {
        return Observable.fromCallable {
            val d = ProductionCategoriesReportModel(
                    getLocalIssuesByTable(1, lotId, syncFlow),
                    getLocalIssuesByTable(2, lotId, syncFlow),
                    getLocalIssuesByTable(3, lotId, syncFlow),
                    getLocalIssuesByTable(4, lotId, syncFlow),
                    getLocalIssuesByTable(5, lotId, syncFlow),
                    getLocalIssuesByTable(6, lotId, syncFlow)
            )
            ProductionCategoriesReportResponse(d)
        }
    }

    fun deleteIssue(token: String, issueId: Int): Observable<Any> {
        return api.deleteIssue(authorization.getAuthToken(token), issueId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun deleteLocalIssue(issueId: Int, isLocalIssue: Boolean, categoryId: Int): Observable<Any> {
        return Observable.fromCallable {
            if (isLocalIssue) {
                val issue = database.getIssuesPendingToSendById(issueId)
                database.deleteIssuePendingToSend(issueId)
                database.incrementCountIssues(issue.issueId, -1)
            } else {
                database.deleteReportIssue(issueId)
            }
            database.incrementCountInCategory(categoryId, -1)
        }
    }

    fun syncIssuesReports(token: String, request: ReportIssuesRequest): Observable<ReportIssuesResponse> {
        return api.syncIssuesReport(authorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getOpenIncidents(token: String, stageId: Int): Observable<IncidentsResponse> {
        return api.getOpenIncidents(authorization.getAuthToken(token), stageId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun closeIncident(token: String, incidentId: Int, request: CloseIssueRequest): Observable<CloseIssueResponse> {
        return api.closeIncident(authorization.getAuthToken(token), incidentId, request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getGeneralQualityReport(token: String, lotId: Int): Observable<List<GeneralQualityResponse>> {
        return api.getGeneralQualityReport(authorization.getAuthToken(token), lotId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

}