package com.amplemind.vivasmart.core.repository.request

import com.amplemind.vivasmart.core.repository.model.CountBoxModel
import com.google.gson.annotations.SerializedName

data class PauseTimerCollaboratorRequest(@SerializedName("bulk") val collaborators: List<PauseTimerCollaborator>)
data class PauseTimerCollaborator(
        @SerializedName("id") val id: Int,
        @SerializedName("temp_time") val time: Long,
        @SerializedName("is_paused") val isPused: Boolean = true)

data class RestarTimerCollaboratorLines(@SerializedName("bulk") val collaborators: List<RestartTimerCollaboratorLines>)
data class RestartTimerCollaboratorLines(
        @SerializedName("id") val id: Int)

data class FinishCollaboratorSkipSignatureRequest(
        @SerializedName("bulk") val collaborators: List<FinishCollaboratorSignature>)

data class FinishCollaboratorSignature(
        @SerializedName("id") val id: Int,
        @SerializedName("temp_time") val time: Long,
        @SerializedName("is_paused") val isPause: Boolean = true,
        @SerializedName("is_done") val is_done: Boolean = true,
        @SerializedName("sign") val sign: String?,
        @SerializedName("is_practice") val isPractice : Boolean,
        @SerializedName("is_training") val is_training : Boolean = false)

data class UpdatePresentationRequest(@SerializedName("bulk") val collaborators: List<UpdatePresentation>)
data class UpdatePresentation(
        @SerializedName("id") val id: Int,
        @SerializedName("packing_id") val packing_id: Int?,
        @SerializedName("packingline_id") val packing_line: Int?
)
