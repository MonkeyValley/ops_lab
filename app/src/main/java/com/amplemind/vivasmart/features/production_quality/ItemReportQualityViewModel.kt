package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.response.CarryOrderPendingResult

class ItemReportQualityViewModel(order : CarryOrderPendingResult) {

    val id_carry = order.id
    val lot = order.lot_name
    val tables = order.table
    val percent = "%${(order.percent_exports ?: 0.0).toInt()}"
    val statusPercent = (order.percent_exports ?: 0.0).toInt() >= 50
    var status = true

    var position : Int? = null
}