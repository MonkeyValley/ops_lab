package com.amplemind.vivasmart.features.production_roster.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.NumberPicker
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_roster.viewModels.AssignUnitHarvestViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import javax.inject.Inject

class AssignUnitHarvestDialogFragment : BaseDialogFragment() {

    companion object {
        val TAG = AssignUnitHarvestDialogFragment::class.java.simpleName

        lateinit var activityLog: ActivityLogModel

        fun newInstance(activityLog: ActivityLogModel, name: String?, busy: Int, units: Int): AssignUnitHarvestDialogFragment {
            val fragment = AssignUnitHarvestDialogFragment()
            this.activityLog = activityLog
            val args = Bundle()
            args.putString("name_unit", name)
            args.putInt("busy", busy)
            args.putInt("units", units)
            fragment.arguments = args
            return fragment
        }

    }

    @Inject
    lateinit var mViewModel: AssignUnitHarvestViewModel

    private lateinit var mBadCount: EditText
    private lateinit var mGoodCount: EditText
    private lateinit var mTotalText: TextView
    private lateinit var mError: TextView

    var listener: Listener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.initialize(activityLog)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_assign_unit_harvest_dialog, null, false)
        builder.setView(view)

        mBadCount = view.findViewById(R.id.assign_bad_units_harvest)
        mGoodCount = view.findViewById(R.id.assign_good_units_harvest)
        mError = view.findViewById(R.id.tv_error)
        mError.text = ""
        OnTextChangeWatcher()

        mTotalText = view.findViewById(R.id.assign_units_harvest_total) as TextView

        mGoodCount.hint = mViewModel.goodBoxes.toString()
        mBadCount.hint = mViewModel.badBoxes.toString()

        setUpCallbacks()
        loadData()
        displayTotal(mViewModel.totalBoxes)

        val dialog = builder
                .setPositiveButton("Aceptar", null)
                .setNegativeButton("Cancelar", null)
                .create()


        dialog.setOnShowListener {
            val positiveButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE)
            val negativeButton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE)

            positiveButton.setOnClickListener {
                listener?.onOkClicked(this@AssignUnitHarvestDialogFragment, mViewModel.newGoodBoxes, mViewModel.newBadBoxes, mViewModel.valuesChanged())
            }

            negativeButton.setOnClickListener {
                listener?.onCancelClicked(this@AssignUnitHarvestDialogFragment)
            }
        }

        return dialog
    }

    private fun OnTextChangeWatcher() {
        mGoodCount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (mGoodCount.text.isNotEmpty()) {
                    if (mGoodCount.text.toString().toInt() > mViewModel.mTotalReceivedBoxes) {
                        displayExceededErrorIfTrue()
                        mGoodCount.setText(mViewModel.mTotalReceivedBoxes.toString())
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (mGoodCount.text.isNotEmpty()) mViewModel.newGoodBoxes = mGoodCount.text.toString().toInt()
                else mViewModel.newGoodBoxes = 0
                displayTotal()
            }
        })

        mBadCount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (mBadCount.text.isNotEmpty()) mViewModel.newBadBoxes = mBadCount.text.toString().toInt()
                else mViewModel.newBadBoxes = 0
                displayTotal()
            }
        })
    }

    override fun loadData() {
        mViewModel.mTotalReceivedBoxes = arguments!!.getInt("busy", 0)
        mViewModel.mTotalAssignedBoxes = arguments!!.getInt("units", 0)
    }

    fun displayTotal(total: Int = mViewModel.totalNewBoxes) {
        mTotalText.text = "%02d".format(total)
    }

    fun displayExceededErrorIfTrue() {
        mError.apply {
            if (mViewModel.isBoxCountExceeded()) {
                text = "Se ha sobrepasado el límite de cajas"
                visibility = View.VISIBLE
            } else {
                visibility = View.GONE
            }
        }
    }

    interface Listener {
        fun onOkClicked(dialog: AssignUnitHarvestDialogFragment, goodBoxes: Int, badBoxes: Int, valuesChanges: Boolean)
        fun onCancelClicked(dialog: AssignUnitHarvestDialogFragment)
    }

}
