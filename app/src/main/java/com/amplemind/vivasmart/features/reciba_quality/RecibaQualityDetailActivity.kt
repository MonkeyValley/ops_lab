package com.amplemind.vivasmart.features.reciba_quality

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.features.reciba_quality.adapter.DetailRecibaQualityAdapter
import com.amplemind.vivasmart.features.reciba_quality.viewModel.ItemDetailRecibaQualityViewModel
import com.amplemind.vivasmart.features.reciba_quality.viewModel.RecibaQualityDetailViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_reciba_quality_detail.*
import kotlinx.android.synthetic.main.activity_report_quality_finished_product.*
import kotlinx.android.synthetic.main.activity_report_quality_finished_product.btn_addReport
import kotlinx.android.synthetic.main.activity_report_quality_finished_product.fab_menu
import kotlinx.android.synthetic.main.activity_report_quality_finished_product.toolbar_report_quality_finished
import kotlinx.android.synthetic.main.categories_toolbar.*
import javax.inject.Inject

class RecibaQualityDetailActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: RecibaQualityDetailViewModel
    var tvTittleTecibaQualityDetail: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reciba_quality_detail)

        tvTittleTecibaQualityDetail = findViewById(R.id.tv_tittle_reciba_quality_detail)

        getArgument()
        setupToolbar()
        setupButton()

        addSubscribers()
        loadDetailList()
    }

    private fun getArgument(){
        val args = intent.extras
        viewModel.carryOrderId = args!!.getString("id", "0")
        viewModel.lotName = args!!.getString("lotName", "")
        viewModel.table = args!!.getString("table", "")
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest().subscribe(this::updateList).addTo(subscriptions)
    }

    private fun updateList(list: List<ItemDetailRecibaQualityViewModel>) {
        Log.d("lista", Gson().toJson(list))
        val adapter = DetailRecibaQualityAdapter(list.toMutableList())
        rv_reciba_review_detail.hasFixedSize()
        rv_reciba_review_detail.layoutManager = LinearLayoutManager(this)
        rv_reciba_review_detail.itemAnimator = DefaultItemAnimator()
        rv_reciba_review_detail.adapter = adapter

        tvTittleTecibaQualityDetail!!.text = viewModel.tittle
    }


    private fun setupToolbar() {
        val mToolbar = toolbar_report_quality_finished
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.reciba_quality)
        toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    fun setupButton(){
        btn_addReport.setOnClickListener {
            if(ifConected()) {
                intent = Intent(this, RecibaQualityReviewActivity::class.java)
                intent.putExtra("id", viewModel.carryOrderId)
                intent.putExtra("lotName", viewModel.lotName)
                intent.putExtra("table", viewModel.table)
                startActivityForResult(intent, 200)
                finish()
            }
            fab_menu.collapse()
        }
    }

    fun ifConected(): Boolean{
        return if(hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

    fun loadDetailList(){
        viewModel.getDetailList()
    }

    override fun onResume() {
        super.onResume()
        loadDetailList()
    }
}
