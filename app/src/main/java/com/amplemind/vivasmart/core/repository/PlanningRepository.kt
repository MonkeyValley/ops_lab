package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.remote.PlanningApi
import com.amplemind.vivasmart.core.repository.response.CropReportResponse
import com.amplemind.vivasmart.core.repository.response.PlanningResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PlanningRepository @Inject constructor(private val api: PlanningApi,
                                             private val apiAuthorization: ApiAuthorization,
                                             private val preferences: UserAppPreferences) {

    /**
     * @param AreaId: Int, date: String, placeId: Map
     * @return PlanningResponse
     *
     * */
    fun getPlanning(areaId: Int, date: String, placeId: Map<String, Int>, categoryId: Int): Observable<PlanningResponse> {
        return api.getPlanning(apiAuthorization.getAuthToken(preferences.token), areaId = areaId, date = date, placeId = placeId)//, activity_category_id = categoryId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    /**
     * @param lotId: Int
     * @return CropReportResponse
     *
     * */
    fun getCropReport(lotId: Int, date: String): Observable<CropReportResponse> {
        return api.getCropReport(apiAuthorization.getAuthToken(preferences.token), lotId, date)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

}