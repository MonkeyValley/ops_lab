package com.amplemind.vivasmart.features.pollination.repository.remote

import com.amplemind.vivasmart.features.pollination.repository.responses.PollinationObjectResponse
import com.amplemind.vivasmart.features.pollination.repository.responses.Pollinationrevisions
import com.amplemind.vivasmart.vo_core.repository.models.realm.PollinationModel
import io.reactivex.Single
import retrofit2.http.*

interface PollinationApi {

    //https://dev.vivasmart.com.mx/v2/pollination/lastweek/11
    @GET("/v2/pollination/lastweek/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getPollinationLastWeeks(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<PollinationObjectResponse<Pollinationrevisions>>

}