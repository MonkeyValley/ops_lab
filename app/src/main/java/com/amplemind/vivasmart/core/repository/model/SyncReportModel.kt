package com.amplemind.vivasmart.core.repository.model

import android.os.Parcel
import android.os.Parcelable

data class SyncReportModel(val name: String,
                           val total: Int,
                           val complete: Int,
                           val offline_id: Int? = -1) : Parcelable {

    val hasErrors = complete < total

    fun formatTotal(): String {
        return "$complete / $total"
    }

    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readInt(),
            parcel.readValue(Int::class.java.classLoader) as? Int)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(total)
        parcel.writeInt(complete)
        parcel.writeValue(offline_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SyncReportModel> {
        override fun createFromParcel(parcel: Parcel): SyncReportModel {
            return SyncReportModel(parcel)
        }

        override fun newArray(size: Int): Array<SyncReportModel?> {
            return arrayOfNulls(size)
        }
    }
}