package com.amplemind.vivasmart.features.pollination

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.features.fertiriego.fragment.MonitoringFragment
import com.amplemind.vivasmart.features.mipe.MipeReportActivity
import com.amplemind.vivasmart.features.pollination.adapter.PollinationLotTab
import com.amplemind.vivasmart.features.pollination.fragment.PollinationFragment
import kotlinx.android.synthetic.main.activity_haulage.tabs_main
import kotlinx.android.synthetic.main.activity_haulage.viewpager_main
import kotlinx.android.synthetic.main.activity_mipe_select_table.*
import kotlinx.android.synthetic.main.activity_pollination.*
import kotlinx.android.synthetic.main.activity_pollination.fab_menu
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.*

class PollinationActivity : BaseActivityWithFragment() {

    companion object {
        val TAG = PollinationActivity::class.simpleName
        val TAG_NAME = "$TAG.TagName"
        val LOT_ID = "$TAG.LotId"
        val LOT_NAME = "$TAG.LotName"
    }

    var lotId: String = "0"
    var week = 0
    var lotName = ""
    var tagName = ""
    lateinit var fragmentAdapter: PollinationLotTab

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pollination)

        arguments()
        setupToolbar()

        val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        week = calendar.get(Calendar.WEEK_OF_YEAR)

        fragmentAdapter = PollinationLotTab(supportFragmentManager, week, lotId.toInt())
        callTabFragment()
        tabs_main.setupWithViewPager(viewpager_main)

        tv_lot_week.text = "Semana " + week

        lbl_track_title.text = "/ Fenología / Polinización / "+  intent?.getStringExtra(LOT_NAME)
        setupButton()
    }

    fun arguments(){
        lotId = intent?.getStringExtra(LOT_ID) ?: "0"
        tagName = intent?.getStringExtra(TAG_NAME) ?: ""
        lotName = intent?.getStringExtra(LOT_NAME) ?: ""
    }

    private fun setupButton(){
        btn_report_pollination.setOnClickListener {
            if(ifConected()) {
                intent = Intent(this, PollinationReportActivity::class.java).apply {
                    putExtra(PollinationReportActivity.LOT_ID, lotId)
                    putExtra(PollinationReportActivity.TAG_NAME, tagName)
                    putExtra(PollinationReportActivity.LOT_NAME, lotName)
                    putExtra(PollinationReportActivity.WEEK, week)
                }
                fab_menu.collapse()
                startActivity(intent)
            }
        }
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_pollination
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = lotName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    fun callTabFragment(){
        viewpager_main.adapter = fragmentAdapter
        viewpager_main.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                val fragment: Fragment = fragmentAdapter.getRegisteredFragment(position)!!
                (fragment as PollinationFragment)
            }
        })
        tabs_main.setupWithViewPager(viewpager_main)
    }

    override fun onBackPressed() {
        finish()
    }

    fun ifConected(): Boolean {
        return if (this.hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }
}