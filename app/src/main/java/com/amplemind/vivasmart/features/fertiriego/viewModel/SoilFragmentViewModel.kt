package com.amplemind.vivasmart.features.fertiriego.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService
import com.amplemind.vivasmart.vo_core.repository.models.realm.SoilModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class SoilFragmentViewModel @Inject constructor(
        private val apiErrors: HandleApiErrors,
        private val service: FertirriegoService) : BaseViewModel() {

    private val requestSuccess_lots = BehaviorSubject.create<List<LotSoilFertirriegoItemViewModel>>()
    private val listLots = mutableListOf<LotSoilFertirriegoItemViewModel>()

    fun getLots(): Disposable {
        return service.getLotsSoil()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModels)
                .subscribe({
                    listLots.clear()
                    listLots.addAll(it)
                    requestSuccess_lots.onNext(listLots)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModels(models: List<SoilModel>): List<LotSoilFertirriegoItemViewModel> {
        val list = arrayListOf<LotSoilFertirriegoItemViewModel>()
        for (item in models) {
            val lotmodel = LotSoilFertirriegoItemViewModel(item)
            list.add(lotmodel)
        }
        return list
    }

    fun onSuccessRequest_lots(): BehaviorSubject<List<LotSoilFertirriegoItemViewModel>> {
        return requestSuccess_lots
    }
}