package com.amplemind.vivasmart.features.production_roster

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.lots.fragment.SelectionOfLotsFragment
import kotlinx.android.synthetic.main.content_payroll_production_menu_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

class MenuActivitiesProductionFragment : BaseFragment() {

    companion object {
        val TAG = MenuActivitiesProductionFragment::class.java.simpleName
    }

    @Inject
    lateinit var viewModel: PayrollActivitiesViewModel

    fun newInstance(): MenuActivitiesProductionFragment {
        return MenuActivitiesProductionFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_payroll_production_menu_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "Producción"

        initRecycler()
        lbl_track_title.text = "/ Nomina / Producción /"
        viewModel.loadMenuActivitiesPayrollProduction().addTo(subscriptions)

        viewModel.getActivities().subscribe(this::setDataAdapter).addTo(subscriptions)
    }

    fun setDataAdapter(activities: List<ItemActivitiesPayrollViewModel>) {
        (rv_menu_production_payroll.adapter as ActivitiesPayrollAdapter).addElements(activities)
        (rv_menu_production_payroll.adapter as ActivitiesPayrollAdapter).onClickItem().subscribe(this::clickActivityPayroll).addTo(subscriptions)
    }

    private fun clickActivityPayroll(item: String) {
        when (item) {
            "Cosecha" -> {
                (activity as AppCompatActivity)
                        .supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                        .add(R.id.container_payroll, SelectionOfLotsFragment.newInstance(item))
                        .addToBackStack(SelectionOfLotsFragment.TAG)
                        .commit()
            }
            "Mantenimiento" -> {
                (activity as AppCompatActivity)
                        .supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                        .add(R.id.container_payroll, SelectionOfLotsFragment.newInstance(item))
                        .addToBackStack(SelectionOfLotsFragment.TAG)
                        .commit()
            }
            "Fertiriego" -> {
                (activity as AppCompatActivity)
                        .supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                        .add(R.id.container_payroll, SelectionOfLotsFragment.newInstance(item))
                        .addToBackStack(SelectionOfLotsFragment.TAG)
                        .commit()
            }
            "MIPE" -> {
                (activity as AppCompatActivity)
                        .supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                        .add(R.id.container_payroll, SelectionOfLotsFragment.newInstance(item))
                        .addToBackStack(SelectionOfLotsFragment.TAG)
                        .commit()
            }
            "Labores Culturales" -> {
                (activity as AppCompatActivity)
                        .supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                        .add(R.id.container_payroll, SelectionOfLotsFragment.newInstance(item))
                        .addToBackStack(SelectionOfLotsFragment.TAG)
                        .commit()
            }
            "Inocuidad" -> {
                (activity as AppCompatActivity)
                        .supportFragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_right, R.anim.exit_right)
                        .add(R.id.container_payroll, SelectionOfLotsFragment.newInstance(item))
                        .addToBackStack(SelectionOfLotsFragment.TAG)
                        .commit()
            }
            else -> {
                Toast.makeText(context, "in construction", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initRecycler() {
        rv_menu_production_payroll.hasFixedSize()
        rv_menu_production_payroll.layoutManager = GridLayoutManager(context, 3)
        rv_menu_production_payroll.addItemDecoration(ItemOffsetDecoration(8.toPx()))
        rv_menu_production_payroll.itemAnimator = DefaultItemAnimator()
        rv_menu_production_payroll.adapter = ActivitiesPayrollAdapter()
    }

    override fun onDestroy() {
        super.onDestroy()
        //TODO: waiting to see if there is a different way of doing
        (activity as ProductionRosterActivitiesActivity).supportActionBar!!.title = "Nómina"
    }


}
