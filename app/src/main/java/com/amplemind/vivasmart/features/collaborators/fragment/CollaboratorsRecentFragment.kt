package com.amplemind.vivasmart.features.collaborators.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.COLLABORATORS
import com.amplemind.vivasmart.core.utils.LOCAL_BROADCAST_USER_RECEIVED
import com.amplemind.vivasmart.features.collaborators.AddActivityForCollaborator
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemCollaboratorsViewModel
import com.amplemind.vivasmart.features.collaborators.adapter.CollaboratorsDatabaseAdapter
import com.amplemind.vivasmart.features.collaborators.callBack.ActionModeCallback
import com.amplemind.vivasmart.features.collaborators.viewModel.CollaboratorsDatabaseViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import kotlinx.android.synthetic.main.content_collaborators_recent_fragment.*
import javax.inject.Inject

class CollaboratorsRecentFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: CollaboratorsDatabaseViewModel

    private var mAdapter: CollaboratorsDatabaseAdapter? = null

    private var actionMode: ActionMode? = null

    private var actionModeCallback: ActionModeCallback? = null

    @Inject
    lateinit var prefer: UserAppPreferences

    lateinit var mActivityCode: ActivityCodeModel


    companion object {

        val TAG = CollaboratorsRecentFragment::class.simpleName
        val PARAM_ACTIVITY_CODE = "$TAG.ActivityCode"

        fun newInstance(activityCode: ActivityCodeModel, showActivities: Boolean): CollaboratorsRecentFragment {
            val fragment = CollaboratorsRecentFragment()
            val args = Bundle()
            args.putBoolean("onlySelected", showActivities)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_collaborators_recent_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()

        addActionMode()

        viewModel.getListCollaboratorsRecent().addTo(subscriptions)

        viewModel.getCollaboratorsRecent().subscribe(this::setAdapter).addTo(subscriptions)
    }

    private fun onlySelected(): Boolean {
        return arguments!!.getBoolean("onlySelected", false)
    }


    /**
     *  cuando es produccion sera multiselect y tendra un flujo diferente
     */
    private fun isMultiselectd(): Boolean {
        return prefer.flow == "Producción"
    }

    private fun addActionMode() {
        actionModeCallback = ActionModeCallback()

        actionModeCallback!!.getActionMode().subscribe(this::removeActionMode).addTo(subscriptions)

        actionModeCallback!!.onAddClick().subscribe(this::onClickAddCollaborator).addTo(subscriptions)

        actionModeCallback!!.onClearSelection().subscribe(this::clearSelection).addTo(subscriptions)

        actionModeCallback!!.onResetAnimation().subscribe(this::resetAnimation).addTo(subscriptions)
    }

    private fun initRecycler() {
        mAdapter = CollaboratorsDatabaseAdapter(!isMultiselectd() and !onlySelected())
        rv_recent.hasFixedSize()
        rv_recent.layoutManager = LinearLayoutManager(context)
        rv_recent.itemAnimator = DefaultItemAnimator()
        rv_recent.adapter = mAdapter

        mAdapter!!.onClick().subscribe(this::onClickItem).addTo(subscriptions)
    }


    fun setAdapter(list: List<ItemCollaboratorsViewModel>) {
        mAdapter!!.addElements(list)
    }

    /**
     *  on click add collaborator in toolbar
     */
    fun onClickAddCollaborator(it: Boolean) {
        if (!isMultiselectd() and !onlySelected()) {
            startActivityForResult(Intent(context, AddActivityForCollaborator::class.java), 200)
        } else {
            //mViewModel.sendCollaboradors(mAdapter!!.getSelectedIDs(), prefer.activityCodeId, prefer.activityCode).addTo(subscriptions)
            viewModel.sendCollaborators(mActivityCode, mAdapter!!.getSelected())
            //mViewModel.finishSendCollaborators().subscribe(this::sendCollaborators).addTo(subscriptions)
        }
    }


    private fun sendCollaborators(collaborators: List<ActivityLogModel>) {
        val intent = Intent(LOCAL_BROADCAST_USER_RECEIVED)
        //intent.putExtra("code", activityLog.)
        intent.putExtra(COLLABORATORS, collaborators as java.util.ArrayList<out Parcelable>)
        //LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
        (context as AppCompatActivity).setResult(200)
        (context as AppCompatActivity).finish()
    }

    fun resetAnimation(it: Boolean) {
        rv_recent.post {
            Runnable {
                mAdapter!!.resetAnimationIndex()
            }.run()
        }
    }

    fun clearSelection(it: Boolean) {
        mAdapter!!.clearSelections()
    }

    fun removeActionMode(remove: Boolean) {
        if (!remove) {
            actionMode = null
        }
    }

    fun onClickItem(item : ItemCollaboratorsViewModel) {
        if (actionMode == null) {
            actionMode = (activity as AppCompatActivity).startSupportActionMode(actionModeCallback!!)
        }
        toggleSelection(item.position!!)
    }

    private fun toggleSelection(position: Int) {
        //TODO: Buscar la mejor manera de usar esta pantalla para diferentes procesos
        if (!isMultiselectd() and !onlySelected()) {
            mAdapter!!.clearSelections()
        }

        //mAdapter!!.toogleSelection(position)
        val count = mAdapter!!.getSelectedItemCount()

        if (count == 0) {
            actionMode!!.finish()
        } else {
            actionMode!!.title = count.toString()
            actionMode!!.invalidate()
        }
    }


}
