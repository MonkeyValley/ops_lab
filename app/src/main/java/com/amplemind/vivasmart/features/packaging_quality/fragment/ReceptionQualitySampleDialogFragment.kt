package com.amplemind.vivasmart.features.packaging_quality.fragment

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.core.repository.model.UnitModel
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ReceptionQualitySampleViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityCarryOrderModel
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.fragment_reception_quality_sample.*
import retrofit2.HttpException
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class ReceptionQualitySampleDialogFragment : BaseDialogFragment() {

    companion object {

        private val TAG = ReceptionQualitySampleDialogFragment::class.java.simpleName

        private val PARAM_CARRY_ORDER_ID = "$TAG.CarryOrderId"
        private val PARAM_LOT_NAME = "$TAG.LotName"
        private val PARAM_LOT_SIZE = "$TAG.LotSize"
        private val PARAM_TABLES = "$TAG.Tables"

        fun newInstance(carryOrder: ReceptionQualityCarryOrderModel) =
                ReceptionQualitySampleDialogFragment().apply {
                    arguments = Bundle().apply {
                        putLong(PARAM_CARRY_ORDER_ID, carryOrder.id)
                        putString(PARAM_LOT_NAME, carryOrder.lotName)
                        putIntArray(PARAM_TABLES, carryOrder.tables.split(",").mapNotNull{ table ->
                            table.toIntOrNull()
                        }.toIntArray())
                    }
                }

        fun newInstance(carryOrder: CarryOrderModel) =
                ReceptionQualitySampleDialogFragment().apply {
                    arguments = Bundle().apply {
                        putLong(PARAM_CARRY_ORDER_ID, carryOrder.id.toLong())
                        putString(PARAM_LOT_NAME, carryOrder.lot?.name)
                        putDouble(PARAM_LOT_SIZE, carryOrder.boxNo)
                        putIntArray(PARAM_TABLES, carryOrder.table?.toIntArray())
                    }
                }

        fun newInstance(carryOrderId: Long) =
                ReceptionQualitySampleDialogFragment().apply {
                    arguments = Bundle().apply {
                        putLong(PARAM_CARRY_ORDER_ID, carryOrderId)
                    }
                }

    }

    @Inject
    lateinit var mViewModel: ReceptionQualitySampleViewModel

    @Inject
    lateinit var mEventBus: EventBus

    private lateinit var mLotName: String
    private lateinit var mTables: IntArray
    private var mLotSize: Double = -1.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.carryOrderId = arguments?.getLong(PARAM_CARRY_ORDER_ID)
                ?: throw IllegalArgumentException("$TAG: Argument $PARAM_CARRY_ORDER_ID is required!")

        arguments?.getString(PARAM_LOT_NAME)?.let { lotName ->
            mLotName = lotName
        }

        arguments?.getDouble(PARAM_LOT_SIZE, -1.0)?.let{ lotSize ->
            mLotSize = lotSize
        }

        arguments?.getIntArray(PARAM_TABLES)?.let { tables ->
            mTables = tables
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reception_quality_sample, container)
    }

    override fun setUpUI() {
        super.setUpUI()
        ViewCompat.setElevation(layout_progress, 20.0f)
    }

    override fun setUpUICallbacks() {

        RxView.clicks(btn_cancel)
                .subscribe{
                    dismiss()
                }
                .addTo(subscriptions)

        RxView.clicks(btn_ok)
                .subscribe{
                    sendOkClickedEvent()
                }
                .addTo(subscriptions)
    }

    override fun loadData() {
        mViewModel.loadUnits().subscribe(this::onUnitsLoaded, this::onUnitsLoadingError).addTo(subscriptions)

        if (mLotSize == -1.0 ||  !::mTables.isInitialized || !::mLotName.isInitialized) {
            mViewModel.loadCarryOrder().subscribe(this::onCarryOrderLoaded, this::onCarryOrderLoadingError).addTo(subscriptions)
        }
        else {
            onCarryOrderLoaded(null)
        }
    }

    private fun onUnitsLoaded(units: List<UnitModel>) {
        showProgressLayout(false)
        val adapter = ArrayAdapter<String>(
                context!!,
                android.R.layout.simple_spinner_dropdown_item,
                units.map{unit ->
                    unit.name
                }
        )

        sp_units.adapter = adapter
    }

    private fun onUnitsLoadingError(error: Throwable) {
        dismiss()

        val msg = when (error) {
            is IOException -> getString(R.string.network_error)
            is HttpException -> getString(R.string.server_error).format(error.response())
            is TimeoutException -> getString(R.string.timeout_error)
            else -> getString(R.string.default_error)
        }

        AlertDialog.Builder(context!!)
                .setTitle(R.string.title_network_error)
                .setMessage(msg)
                .setPositiveButton(R.string.accept, null)
                .show()
    }

    private fun onCarryOrderLoaded(carryOrder: CarryOrderModel?) {
        showCarryOrderInfo(carryOrder)
    }

    private fun onCarryOrderLoadingError(error: Throwable) {
        showCarryOrderInfo(null)
    }

    private fun showCarryOrderInfo(carryOrder: CarryOrderModel?) {

        val errorMessage = context?.getString(R.string.rqsf_error_loading)
        val lotNameFormat = context?.getString(R.string.rqsf_lot_name)

        tv_lot_name.text = when {
            carryOrder?.lot?.name != null -> lotNameFormat?.format(carryOrder.lot.name)
            ::mLotName.isInitialized -> lotNameFormat?.format(mLotName)
            else -> errorMessage
        }

        tv_crop_name.text = carryOrder?.crop?.name ?: if (::mLotName.isInitialized) mLotName else errorMessage

        tv_box_size.text = carryOrder?.boxNo?.toString() ?: if (mLotSize != -1.0) mLotSize.toString() else errorMessage

        val tables = carryOrder?.table?.toIntArray() ?: if (::mTables.isInitialized) mTables else intArrayOf()

        showTables(tables)
    }

    private fun showTables(tables: IntArray) {
        if (tables.isNotEmpty()) {
            val tableViews = listOf(
                    tv_table_1,
                    tv_table_2,
                    tv_table_3,
                    tv_table_4,
                    tv_table_5,
                    tv_table_6
            )

            tables.forEach { table ->
                tableViews[table - 1].visibility = View.VISIBLE
            }
        }
    }

    private fun sendOkClickedEvent() {
        if (validateForm()) {
            mEventBus.send(
                    OnOkClickedEvent(
                            this,
                            mViewModel.carryOrderId,
                            et_sample_size.text.toString().toIntOrNull() ?: 0,
                            mViewModel.units[sp_units.selectedItemPosition]
                    )
            )
            dismiss()
        }
    }

    private fun validateForm(): Boolean {

        var result = true
        et_sample_size.error = null

        if (et_sample_size.text.isNullOrBlank()) {
            et_sample_size.error = context!!.getString(R.string.rqsf_error_sample_size)
            result = false
        }

        if (sp_units.selectedItemPosition == AdapterView.INVALID_POSITION) {
            Snackbar.make(
                    view!!,
                    context!!.getString(R.string.rqsf_error_unit),
                    Snackbar.LENGTH_SHORT
            ).view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.bgAlert))
            result = false
        }

        return result
    }

    private fun showProgressLayout(show: Boolean) {
        layout_progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    data class OnOkClickedEvent (
            val dialog: ReceptionQualitySampleDialogFragment,
            val carryOrderId: Long,
            val sampleSize: Int,
            val unit: UnitModel
    )

}
