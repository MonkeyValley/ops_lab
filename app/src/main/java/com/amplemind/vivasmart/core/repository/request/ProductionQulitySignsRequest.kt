package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

data class ProductionQulitySignsRequest(@SerializedName("sign_super") val signSuper: String,
                                        @SerializedName("sign_super_qa") val signCollaborator: String)