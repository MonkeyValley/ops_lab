package com.amplemind.vivasmart.features.pollination

import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.pollination.adapter.PollinationReportTab
import com.amplemind.vivasmart.features.pollination.fragment.PollinationReportFragment
import kotlinx.android.synthetic.main.activity_pollination_report.*
import kotlinx.android.synthetic.main.custom_toolbar.*

class PollinationReportActivity : BaseActivityWithFragment() {

    companion object {
        val TAG = PollinationReportActivity::class.simpleName
        val TAG_NAME = "$TAG.TagName"
        val LOT_ID = "$TAG.LotId"
        val LOT_NAME = "$TAG.LotName"
        val WEEK = "$TAG.Week"
    }

    var lotId: String = "0"
    var week: Int = 0
    var lotName: String = ""
    var tagName: String = ""
    lateinit var fragmentAdapter: PollinationReportTab


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pollination_report)
        readArguments()
        setupToolbar()

        fragmentAdapter = PollinationReportTab(supportFragmentManager, week, lotId.toInt(), "" + intent?.getStringExtra(LOT_NAME))
        callTabFragment()
        tabs_pollination_report.setupWithViewPager(viewpager_pollination_report)
    }

    fun readArguments(){
        lotId = intent?.getStringExtra(LOT_ID) ?: "0"
        week = intent?.getIntExtra(WEEK, 0) ?: 0
        lotName = intent?.getStringExtra(LOT_NAME)?: ""
        tagName =  intent?.getStringExtra(TAG_NAME)?: ""
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_pollination_report
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = "" + lotName + " - " + tagName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    fun callTabFragment() {
        viewpager_pollination_report.adapter = fragmentAdapter
        viewpager_pollination_report.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                val fragment: Fragment = fragmentAdapter.getRegisteredFragment(position)!!
                (fragment as PollinationReportFragment)
            }
        })
        tabs_pollination_report.setupWithViewPager(viewpager_pollination_report)
    }

    override fun onBackPressed() {
        finish()
    }
}