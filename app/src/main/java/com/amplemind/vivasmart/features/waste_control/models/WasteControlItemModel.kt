package com.amplemind.vivasmart.features.waste_control.models


import com.google.gson.annotations.SerializedName

data class WasteControlItemModel(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("business_unit_id") val business_unit_id: Int? = null,
        @SerializedName("crop_id") val crop_id: Int?,
        @SerializedName("unit_id") val unit_id: Int?,
        @SerializedName("sample")  val sample: Int? = 0,
        @SerializedName("waste") var waste: Int? = 0,
        @SerializedName("national") val national: Int? = 0,
        @SerializedName("natural_waste") val natural_waste: Int? = 0,
        @SerializedName("operation_waste") var operation_waste: Int? = 0,
        @SerializedName("export") var export: Int? = 0,
        @SerializedName("sign")  val sign: String?,
        @SerializedName("crop") val crop: WasteCrop? = null
)

data class WasteControlResponse(
        @SerializedName("data") val data: List<WasteControlItemModel>,
        @SerializedName("total_count") val total_count: Int
)

data class WasteCrop(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("crop_type_id") val crop_type_id: Int
)