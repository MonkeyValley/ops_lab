package com.amplemind.vivasmart.features.production_roster.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import android.widget.RelativeLayout
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageDataModel

private val ARG_MESSAGE_DATA= "message data"
class ExtraHoursAuthRequest : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private var mDataMessage: MessageDataModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mDataMessage = arguments!!.getParcelable(ARG_MESSAGE_DATA)

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_extra_hours_auth_request, container, false)
        var npCollaborators = view.findViewById(R.id.extrahours_np_collaborators) as NumberPicker
        var npHours = view.findViewById(R.id.extrahours_np_hours) as NumberPicker
        val btnAuth = view.findViewById(R.id.extraHoursRequest_btn_auth) as RelativeLayout

        btnAuth.setOnClickListener {
            // Call the update service
        }

        npCollaborators.value = mDataMessage!!.collaboratorNo
        npCollaborators.maxValue = 31
        npCollaborators.minValue = 0
        npHours.value = mDataMessage!!.collaboratorNo
        npHours.maxValue = 8
        npHours.minValue = 0
        npCollaborators.setOnValueChangedListener { _, _, newVal ->  }

        npHours.setOnValueChangedListener { _, _, newVal ->  }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
                ExtraHoursAuthRequest().apply {
                    arguments = Bundle().apply {

                    }
                }

        fun newInstanceAuthorize(data: MessageDataModel) =
                ExtraHoursAuthRequest().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_MESSAGE_DATA, data)
                    }
                }
    }
}
