package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.realm.StagesDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import javax.inject.Inject

class LotChooserRepository @Inject constructor(
        private val stagesDao: StagesDao
): Repository() {

    override fun cleanUp() {
        stagesDao.cleanUp()
    }

    fun findAll() =
            stagesDao.findAllActive()

    fun isPending(stage: StageModel, isPending: Boolean) =
            stagesDao.isPending(stage, isPending)

    fun isClosed(stage: StageModel, isClosed: Boolean, stageTypeId: Int) =
            stagesDao.isClose(stage, isClosed, stageTypeId)

    fun isClose(stageTypeId: Int, lotId: Int) =
            stagesDao.isClose(stageTypeId, lotId)

    fun getLotActivityCouter(lotId: Int, stageTypeId: Int) =
            stagesDao.getLotActivityCouter(lotId, stageTypeId)


}