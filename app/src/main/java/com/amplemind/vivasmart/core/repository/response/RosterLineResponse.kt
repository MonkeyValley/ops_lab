package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.RosterLineModel
import com.google.gson.annotations.SerializedName

data class RosterLineResponse(
        @SerializedName("data") val lines: List<RosterLineModel>
)