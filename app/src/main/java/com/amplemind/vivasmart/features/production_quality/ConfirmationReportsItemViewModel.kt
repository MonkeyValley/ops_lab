package com.amplemind.vivasmart.features.production_quality

import androidx.databinding.ObservableInt
import android.util.Log
import android.view.View
import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ProductionCategoriesTable
import javax.inject.Inject

class ConfirmationReportsItemViewModel @Inject constructor(val pref: UserAppPreferences, val header: String?, val model: ProductionCategoriesTable? = null, position_item: String) {

    var serverUrl: String? = pref.selectedServerUrl

    val url: String
        get() = calculateUrl()

    var headerVisibility = ObservableInt(View.GONE)
    var itemVisibility = ObservableInt(View.VISIBLE)
    var deleteVisibility = ObservableInt(View.VISIBLE)

    val issueId = model?.id ?: 0
    val category:String = model?.subCategory ?: ""
    val description = model?.description ?: ""
    val grooves = "${model?.grooveNo}"
    val isLocalModel = model?.isLocalModel ?: false

    var position = position_item


    init {
        if (header != null) {
            headerVisibility = ObservableInt(View.VISIBLE)
            itemVisibility = ObservableInt(View.GONE)
        } else {
            deleteVisibility = ObservableInt(View.GONE)
        }
    }

    fun isHeader(): Boolean {
        return header != null
    }

    fun getTableName(): String {
        return "Tabla ${model?.tableNo}"
    }

    private fun calculateUrl(): String {
        if (model != null && model.image.startsWith("/storage")) {
            Log.e("Imagen-null: ", model.image)
           return "https://www.infobae.com/new-resizer/McqpA6V_SbCbRWhrICLw2E3C5Cw=/750x0/filters:quality(100)/arc-anglerfish-arc2-prod-infobae.s3.amazonaws.com/public/SPT35Y22RREGNORTIJ5VIED2XI.jpg"
        }

        Log.e("Imagen: ", "${pref.serverImageStorage}${model?.image}")
        return "${pref.serverImageStorage}${model?.image}"
   }

}