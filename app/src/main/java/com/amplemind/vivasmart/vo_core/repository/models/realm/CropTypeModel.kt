package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class CropTypeModel(
        @SerializedName("id")   var id: Int = 0,
        @SerializedName("name") var name: String? = null
) : RealmObject(), Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CropTypeModel> {
        override fun createFromParcel(parcel: Parcel): CropTypeModel {
            return CropTypeModel(parcel)
        }

        override fun newArray(size: Int): Array<CropTypeModel?> {
            return arrayOfNulls(size)
        }
    }
}
