package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

data class FinishCollaboratorRequest(@SerializedName("bulk") val collaborators: List<FinishCollaborator>)
data class FinishCollaborator(
        @SerializedName("grooveNo") val id: Int,
        @SerializedName("is_done") val isDone: Boolean = true,
        @SerializedName("temp_time") val tempTime: Int = 0,
        @SerializedName("is_paused") val isPaused: Boolean = true
)