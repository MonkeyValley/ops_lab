package com.amplemind.vivasmart.features.reciba_quality.model

class UnitItem {
    var id: Int? = null
    var business_unit_id: Int? = null
    var name: String? = null
    var business_unit_names: String?  = null

    constructor(id: Int?,business_unit_id:Int, name: String?, business_unit_names: String) {
        this.id = id
        this.business_unit_id = business_unit_id
        this.name = name
        this.business_unit_names = business_unit_names
    }

    override fun toString(): String {
        return name!!
    }

    fun getId(): Int{
        return id!!
    }
}