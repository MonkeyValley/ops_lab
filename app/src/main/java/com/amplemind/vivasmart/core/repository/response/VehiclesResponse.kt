package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.VehicleModel
import com.google.gson.annotations.SerializedName

data class VehiclesResponse(@SerializedName("data") val data: List<VehicleModel>)