package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.core.utils.SUCCESS
import com.amplemind.vivasmart.features.adapters.SwipableItemTouchHelper
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import com.amplemind.vivasmart.features.production_roster.dialogs.CollaboratorsActionsDialog
import kotlinx.android.synthetic.main.activity_production_categories.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.io.File
import javax.inject.Inject

class ProductionCategoriesActivity : BaseActivity(), SwipableItemTouchHelper.Listener {

    private val TAG = ProductionQualityActivity::class.java.simpleName

    //Vertical span for Categories RecyclerView
    private val CAT_VERT_SPAN = 2

    //Horizontal span for Categories RecyclerView
    private val CAT_HORI_SPAN = 3

    val CATEGORIES_DETAIL_INTENT = 1

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var viewModel: ProductionCategoriesViewModel

    var deletingPos: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_production_categories)

        val readOnly = intent.getBooleanExtra("is_closed", false)

        if (readOnly) {
            findViewById<TextView>(R.id.average).visibility = View.VISIBLE
            findViewById<LinearLayout>(R.id.send).visibility = View.GONE
        }

        setupToolbar()
        initRecycler(readOnly)
        getData()
        setupListenners(readOnly)
    }
/*
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_incidents, menu)
        return true
    }
*/

    override fun onResume() {
        super.onResume()
        viewModel.getOpenIncidents(getStageId()).addTo(subscriptions)
    }

    private fun getData() {
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onRequestSuccess().subscribe(this::onSuccessRequest).addTo(subscriptions)
        viewModel.onSuccessUpdateSign().subscribe(this::onSuccessUpdateImage).addTo(subscriptions)
        viewModel.onSuccessFinishActivity().subscribe(this::onFinishActivity).addTo(subscriptions)
        viewModel.onSuccessGetCountIssues().subscribe(this::onSuccessGetCountIssues).addTo(subscriptions)
        viewModel.onSuccessGetOpenIssues().subscribe(this::onSuccessGetOpenIssues).addTo(subscriptions)
        viewModel.onSuccessCloseIncident().subscribe(this::onSuccessCloseIncident).addTo(subscriptions)
        viewModel.onSuccessGetGeneralReport().subscribe(this::onSuccessGetGeneralReport).addTo(subscriptions)
        viewModel.getActivities().addTo(subscriptions)
    }

    private fun onSuccessUpdateImage(files: MutableList<File>) {
        if (files.isNotEmpty()) {
            viewModel.updateSign(files).addTo(subscriptions)
        } else {
            viewModel.finishActivity(getLotId())?.addTo(subscriptions)
        }
    }

    private fun getLotId(): Int {
        return intent.getIntExtra("lot_id", 0)
    }

    private fun getStageId(): Int {
        return intent.getIntExtra("stage_id", 0)
    }

    private fun onFinishActivity(success: Boolean) {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun onSuccessRequest(categories: List<ProductionCategoriesItemViewModel>) {
        (rv_categories.adapter as ProductionCategoriesAdapter).addElements(categories)
        requestCountIssues()
        requestGeneralQualityReport()
    }

    private fun requestGeneralQualityReport() {
        val items = (rv_categories.adapter as ProductionCategoriesAdapter).list
        viewModel.getGeneralQualityReport(getLotId(), items).addTo(subscriptions)
    }

    private fun requestCountIssues() {
        //if (isOnlineMode(this)) {
            viewModel.getCountIssues(getLotId()).addTo(subscriptions)
        /*
        } else {
            root_activity_production_categories.snackWithoutInternetMessage()
            mViewModel.getLocalCountIssues(getLotId()).addTo(subscriptions)
        }
        */
    }

    private fun getLotName(): String {
        return intent.getStringExtra("lot_name")
    }

    private fun onSuccessGetCountIssues(success: Boolean) {
        (rv_categories.adapter as ProductionCategoriesAdapter).notifyDataSetChanged()
    }

    private fun onSuccessGetOpenIssues(list: List<IncidentItemViewModel>) {
        (rv_incidents.adapter as ProductionIncidentsAdapter).addElements(list)
        if (!list.isEmpty()) {
            findViewById<TextView>(R.id.no_incidents).visibility = View.GONE
        }
    }

    private fun onSuccessCloseIncident(success: Boolean) {
        if (success && deletingPos > -1) {
            (rv_incidents.adapter as ProductionIncidentsAdapter).deleteElement(deletingPos)
            requestCountIssues()
            requestGeneralQualityReport()
            deletingPos = -1
        }
    }

    private fun onSuccessGetGeneralReport(success: Boolean) {
        (rv_categories.adapter as ProductionCategoriesAdapter).notifyDataSetChanged()
        setGeneralQualityAverage()
    }

    private fun initRecycler(readOnly: Boolean) {

        rv_incidents.hasFixedSize()
        rv_incidents.layoutManager = LinearLayoutManager(this)
        rv_incidents.adapter = ProductionIncidentsAdapter(this)
        rv_incidents.itemAnimator = DefaultItemAnimator()

        val itemTouchHelper = SwipableItemTouchHelper(SwipableItemTouchHelper.FINISHABLE, this)
        ItemTouchHelper(itemTouchHelper).attachToRecyclerView(rv_incidents)

        rv_categories.hasFixedSize()
        rv_categories.layoutManager = GridLayoutManager(this, CAT_VERT_SPAN, RecyclerView.HORIZONTAL, false)
        rv_categories.adapter = ProductionCategoriesAdapter(this, CAT_HORI_SPAN, readOnly)
        (rv_categories.adapter as ProductionCategoriesAdapter).onClickItem().subscribe(this::showIssues).addTo(subscriptions)
    }

    private fun showIssues(item: ProductionCategoriesItemViewModel) {
        startActivityForResult(
                Intent(this, CategoriesDetailActivity::class.java)
                        .putExtra("category_id", item.id)
                        .putExtra("name", item.name)
                        .putExtra("name_lot", getLotName())
                        .putExtra("stage_id", intent.getIntExtra("stage_id", 0))
                        .putExtra("lot_id", getLotId())
                        .putParcelableArrayListExtra("tables", intent.getParcelableArrayListExtra("tables")),
                ProductionCategoriesActivity().CATEGORIES_DETAIL_INTENT)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_categories.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getLotName()
        supportActionBar!!.subtitle = "Incidencias pendientes"
        lbl_track_title.text = "/ Calidad / Producción / Control / "+ getLotName()
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(this@ProductionCategoriesActivity)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(this@ProductionCategoriesActivity, bitmap), image, 400f, 400f)
        return image
    }


    private fun refressIssues() {
        //if (isOnlineMode(this)) {
            viewModel.getCountIssues(getLotId()).addTo(subscriptions)
        /*
        } else {
            mViewModel.getLocalCountIssues(getLotId()).addTo(subscriptions)
        }
        */
    }

    private fun sendReport() {
        if (viewModel.hasReports()) {
            val intentActivity = Intent(this, ConfirmationReportsActivity::class.java)
            intentActivity.putExtra("lot_id", getLotId())
            intentActivity.putExtra("stage_id", intent.getIntExtra("stage_id", 0))
            startActivityForResult(intentActivity, 200)
        } else {
            val dialog = SignatureDialogFragment().newInstance(viewModel.getSignatureTitle(), viewModel.getSignatureMessage(), false, mode = 0)
            dialog.show(supportFragmentManager, SignatureDialogFragment.TAG)
            dialog.setOnCompleteListener(object : OnSignatureCompleteListener {
                override fun onDismiss() {}

                override fun completeSign(sign1: Bitmap, sign2: Bitmap?) {
                    val signs = mutableListOf<File>()
                    signs.add(compressImage(sign1))
                    if (sign2 != null) {
                        signs.add(compressImage(sign2))
                    }
                    //if (isOnlineMode(this@ProductionCategoriesActivity)) {
                        viewModel.updateSign(signs).addTo(subscriptions)
                    /*
                    } else {
                        mViewModel.finishLocalActivity(signs[0].absolutePath, signs[1].absolutePath, intent.getIntExtra("stage_id", 0)).addTo(subscriptions)
                    }
                    */
                }
            })
        }
    }

    private fun setupListenners(readOnly: Boolean = false) {
        val btnSend = findViewById<View>(R.id.send)

        if (!readOnly) {
            btn_finish
                    .addThrottle()
                    .subscribe { sendReport() }
                    .addTo(subscriptions)

            btnSend.setOnClickListener {
                sendReport()
            }
        }
    }

    private fun setGeneralQualityAverage() {
        findViewById<TextView>(R.id.average).text = viewModel.getGeneralQualityAverage()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CATEGORIES_DETAIL_INTENT -> {
                if (resultCode == Activity.RESULT_OK) {
                    //viewModel.getOpenIncidents(getStageId()).addTo(subscriptions)
                    requestCountIssues()
                    requestGeneralQualityReport()
                    //refressIssues()
                }
            }
            SUCCESS -> {
                if (resultCode == Activity.RESULT_OK) {
                    onFinishActivity(true)
                } else {
                    requestCountIssues()
                    requestGeneralQualityReport()
                    refressIssues()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {

        val dialogTitle = this.getString(R.string.alert_dialog_close_incident_title)
        val dialogMsg = this.getString(R.string.alert_dialog_close_incident_msg)

        val adapter = (rv_incidents.adapter as ProductionIncidentsAdapter)
        val incidentId = adapter.getItemId(position).toInt()

        CollaboratorsActionsDialog.showAlertAction(
                this, dialogTitle, dialogMsg, {
            deletingPos = position
            viewModel.closeIncident(incidentId)
        }, {
            (rv_incidents.adapter as ProductionIncidentsAdapter).notifyItemChanged(position)
        })
    }

}
