package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.UnitsList
import com.google.gson.annotations.SerializedName

class UnitsDataResponse(@SerializedName("data") val data: List<UnitsResponse>)
class UnitsResponse(@SerializedName("units") val units: List<UnitsList>)