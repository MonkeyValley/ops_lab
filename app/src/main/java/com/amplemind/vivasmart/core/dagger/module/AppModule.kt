package com.amplemind.vivasmart.core.dagger.module

import android.app.Application
import androidx.room.Room
import android.content.Context
import android.content.Intent
import com.amplemind.vivasmart.core.repository.local.AppDatabase
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.core.utils.RetrofitHolder
import com.amplemind.vivasmart.features.login.LoginActivity
import com.amplemind.vivasmart.features.packaging_quality.clean_report.SensorCardReload
import com.amplemind.vivasmart.vo_features.services.SyncNotificationManager
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by
 *              amplemind on 6/29/18.
 */
@Module
class AppModule {


    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    internal fun provideAppDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application.applicationContext,
                AppDatabase::class.java, "viva_smart").build()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpCache(application: Application): Cache {
        val cacheFile = File(application.applicationContext
                .cacheDir.absolutePath, "HttpCache")
        val cacheSize = 10 * 1024 * 1024
        val cache = Cache(cacheFile, cacheSize.toLong())
        return cache
    }

    @Provides
    @Singleton
    internal fun provideSharedPreferences(context: Context, gson: Gson, apiAuth: ApiAuthorization) : UserAppPreferences {
        return UserAppPreferences(context, gson, apiAuth)
    }

    @Provides
    @Singleton
    internal fun provideSensorAccelerometer(context: Context) : SensorCardReload {
        return SensorCardReload(context)
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(cache: Cache, context: Context, room: AppDatabase, hostSelInt: HostSelectionInterceptor, pref: UserAppPreferences): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val networkInterceptor = Interceptor { chain ->
            val request = chain.request().newBuilder().build()
            val response = chain.proceed(request)
            if (response.code() == 401) {
                pref.token = ""
                pref.userInfo = ""
                room.clearAllTables()
                val intent = Intent(context, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
            }
            response
        }

        val okBuilder = OkHttpClient().newBuilder()
        okBuilder.cache(cache)
        okBuilder.readTimeout(30, TimeUnit.SECONDS)
        okBuilder.connectTimeout(30, TimeUnit.SECONDS)
        okBuilder.writeTimeout(30, TimeUnit.SECONDS)
        okBuilder.addInterceptor(interceptor)
        /*
        if(BuildConfig.FLAVOR.equals("local")) {
            okBuilder.addInterceptor(hostSelInt)
        }
        */
        okBuilder.networkInterceptors().add(networkInterceptor)
        okBuilder.addNetworkInterceptor(StethoInterceptor())
        return okBuilder.build()
    }

    @Provides
    @Singleton
    internal fun provideHostInterceptor(): HostSelectionInterceptor {
        return HostSelectionInterceptor()
    }

    @Provides
    @Singleton
    internal fun provideRetrofitHolder(okHttpClient: OkHttpClient, pref: UserAppPreferences): RetrofitHolder {
        return RetrofitHolder(okHttpClient, pref)
    }

    @Provides
    internal fun provideRetrofit(retrofitHolder: RetrofitHolder): Retrofit {
        return retrofitHolder.retrofit
    }

    @Provides
    @Singleton
    internal fun provideRealmHolder(): RealmHolder {
        return RealmHolder()
    }

    @Provides
    @Singleton
    internal fun provideSyncNotificationManager(): SyncNotificationManager {
        return SyncNotificationManager()
    }

}