package com.amplemind.vivasmart.features.production_roster.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment

class EditTimerGeneralDialog: BaseDialogFragment() {

    private var editTimerGeneralListener: EditTimerGeneralListener? = null

    companion object {
        val TAG = EditTimerGeneralDialog::class.java.simpleName

        fun newInstance(title: String, time: Int = 0, quantity: Int = 0,
                        extraTime: Int = 0): EditTimerGeneralDialog {
            val dialog = EditTimerGeneralDialog()
            val args = Bundle()
            args.putString("title", title)
            args.putInt("time", time)
            args.putInt("quantity", quantity)
            args.putInt("extraTime", extraTime)
            dialog.arguments = args
            return dialog
        }
    }

    fun onEditTimerGeneral(listener: EditTimerGeneralListener) {
        editTimerGeneralListener = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val rootView = LayoutInflater.from(context).inflate(R.layout.content_edit_timer_general, null, false)
        builder.setView(rootView)

        val close = rootView.findViewById<ImageView>(R.id.iv_close)
        val time = rootView.findViewById<EditText>(R.id.ed_time)
        val quantity = rootView.findViewById<EditText>(R.id.ed_quantity)
        val extraTime = rootView.findViewById<EditText>(R.id.ed_extra_time)

        close.setOnClickListener { dismiss() }
        rootView.findViewById<TextView>(R.id.tv_title).text = arguments!!.getString("title")
        time.setText("${arguments!!.getInt("time")}")
        quantity.setText("${arguments!!.getInt("quantity")}")
        extraTime.setText("${arguments!!.getInt("extraTime")}")

        builder.setPositiveButton(getString(R.string.accept)) {
            _, _ ->
            editTimerGeneralListener?.onChange(time.text.toString().toIntOrNull(), quantity.text.toString().toIntOrNull(),
                    extraTime.text.toString().toIntOrNull())
            dismiss()
        }

        builder.setNegativeButton(getString(R.string.cancel), {_, _ -> dismiss()})

        return builder.create()
    }

}

interface EditTimerGeneralListener {
    fun onChange(time: Int?, quantity: Int?, extraTime: Int?)
}