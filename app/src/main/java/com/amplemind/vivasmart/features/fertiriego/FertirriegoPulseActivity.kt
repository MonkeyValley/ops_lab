package com.amplemind.vivasmart.features.fertiriego

import android.os.Bundle
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.fertiriego.adapter.FertirriegoPulseTab
import com.amplemind.vivasmart.features.fertiriego.fragment.PulseFragment
import kotlinx.android.synthetic.main.activity_haulage.tabs_main
import kotlinx.android.synthetic.main.activity_haulage.toolbar_haulage
import kotlinx.android.synthetic.main.activity_haulage.viewpager_main
import kotlinx.android.synthetic.main.custom_toolbar.*

class FertirriegoPulseActivity : BaseActivityWithFragment(), CompoundButton.OnCheckedChangeListener {

    companion object {
        val TAG = FertirriegoPulseActivity::class.simpleName
        val TAG_NAME = "$TAG.TagName"
        val LOT_ID = "$TAG.LotId"
        val DATE = "$TAG.Date"
        val PULSE = "$TAG.Pulse"
        val LOT_NAME = "$TAG.LotName"
    }

    var tvPulse: TextView? = null
    var tvDatePulse: TextView? = null
    var swTableValve: SwitchCompat? = null

    var lotId: String = "0"
    var date: String = ""
    var pulse: String = "0"
    var lotName: String = ""

    lateinit var preferences: UserAppPreferences

    lateinit var fragmentAdapter: FertirriegoPulseTab

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fertirriego_pulse)

        preferences = UserAppPreferences(this, null, null)

        arguments()
        setUpUi()

        callTabFragment()
        tabs_main.setupWithViewPager(viewpager_main)
    }

    fun arguments() {
        lotId = intent?.getStringExtra(LOT_ID) ?: "0"
        date = intent?.getStringExtra(DATE) ?: "00-00-0000"
        pulse = intent?.getStringExtra(PULSE) ?: "0"
        lotName = intent?.getStringExtra(LOT_NAME) ?: ""
    }

    fun setUpUi() {
        setupToolbar()

        tvPulse = findViewById(R.id.tv_pulse)
        tvDatePulse = findViewById(R.id.tv_date_pulse)

        tvPulse!!.text = "Pulso " + pulse
        tvDatePulse!!.text = date

        fragmentAdapter = FertirriegoPulseTab(supportFragmentManager, date, lotId.toInt(), pulse.toInt(), lotName)

        swTableValve = findViewById(R.id.sw_table_valve)
        swTableValve!!.isChecked = preferences.table
        swTableValve!!.text = if (preferences.table) "Valvula" else "Tabla"
        swTableValve!!.setOnCheckedChangeListener(this)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_haulage
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = intent?.getStringExtra(TAG_NAME)
                ?: ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    fun callTabFragment() {
        viewpager_main.adapter = fragmentAdapter
        viewpager_main.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                val fragment: Fragment = fragmentAdapter.getRegisteredFragment(position)!!
                (fragment as PulseFragment).showValves(preferences.table)
            }
        })
        tabs_main.setupWithViewPager(viewpager_main)
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
        preferences.table = isChecked
        swTableValve!!.text = if (isChecked) "Valvula" else "Tabla"
        val pos: Int = viewpager_main.currentItem
        val fragment: Fragment = fragmentAdapter.getRegisteredFragment(pos)!!
        (fragment as PulseFragment).showValves(preferences.table)
    }

}
