package com.amplemind.vivasmart.vo_core.repository.remote

import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_core.repository.responses.ObjectResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface CollaboratorsApi {

    @GET("/v2/collaborator")
    @Headers("Content-Type: application/json")
    fun getCollaborators(
            @Header("Authorization")                authentication: String,
            @Query("limit")                         limit: Int = 100000,
            @Query("business_unit_id")              businessUnitId: Long,
            @Query("version__gte")                   version: Long? = null,
            @Query("__logic")                       logic: String = "AND"
    ): Single<ObjectResponse<CollaboratorModel>>


//    @POST("/v2/activity_code/collaborator")
//    @Headers("Content-Type: application/json")
//    fun sendCollaborators(
//            @Header("Authorization") authentication: String,
//            @Body body: CollaboratorsRepository.SendCollaboratorsReponse
//    ): Observable<CollaboratorsReponseActivities>
//
//    @GET("v2/activity/paysheet/packingline/{packingline}")
//    @Headers("Content-Type: application/json")
//    fun getLinesActivities(
//            @Header("Authorization") authentication: String,
//            @Path("packingline") packingline: String,
//            @Query("embed") embed: String = "packinglines,crops",
//            @Query("PackinglineActivity.packingline_id") packingline_id: String,
//            @Query("packingline_activity.packingline_id") packingline_id_embed: String,
//            @Query("business_unit_id") business_unit_id: String,
//            @Query("__logic") logic: String = "AND"
//    ): Observable<ActivityLinesResponse>
//
//    @GET("/v2/{package}/{paysheet}{packingline}{stageId}")
//    @Headers("Content-Type: application/json")
//    fun searchCollaboratorByName(
//            @Header("Authorization") authentication: String,
//            @Path("package") package_path: String = "a_collaborator",
//            @Path("paysheet", encoded = true) paysheet: String = "",
//            @Path("packingline", encoded = true) packingline: String = "stage/",
//            @Path("stageId") stage: Int,
//            @Query("business_unit_id") business_unit_id: String,
//            @Query("limit") limit: Int = 50,
//            @Query("order_by") order: String = "name",
//            @Query("offset") offset: Int = 0,
//            @Query("employee_code__eq") code: String,
//            @Query("__logic") logic: String = "AND"
//    ): Observable<CollaboratorsRepository.WorkingCollaboratorModel>
//
//    @GET("/v2/a_collaborator/stage/{stageId}")
//    @Headers("Content-Type: application/json")
//    fun getOperators(
//            @Header("Authorization") authentication: String,
//            @Path("stageId") stage: Int,
//            @Query("name__icontains") name: String,
//            @Query("limit") limit: Int = 5
//    ): Observable<CollaboratorsRepository.WorkingCollaboratorModel>
//
//
//    @GET("/v2/packing/paysheet/packingline/{packingline}")
//    @Headers("Content-Type: application/json")
//    fun getPresentation(
//            @Header("Authorization") authentication: String,
//            @Path("packingline") packingline: String,
//            @Query("business_unit_id") bu : String
//    ): Observable<PresentationResponse>
//
//
//    @POST("/v2/packingline_paysheet/collaborator/in")
//    @Headers("Content-Type: application/json")
//    fun assignCollaborator(
//            @Header("Authorization") authentication: String,
//            @Body body: CollaboratorsRepository.AssingCollaborators
//    ): Observable<CollaboratorsInLinesResult>
//
//    @PUT("/v2/packingline/collaborator")
//    @Headers("Content-Type: application/json")
//    fun updatePresentation(
//            @Header("Authorization") authentication: String,
//            @Body body: UpdatePresentationRequest
//    ): Observable<Any>
//
//    @POST("/v2/offline/sync/production")
//    @Headers("Content-Type: application/json")
//    fun syncCollaborators(
//            @Header("Authorization") authentication: String,
//            @Body body: SyncRosterProductionRequest
//    ): Observable<SyncRosterProductionResponse>
//
//    @POST("/v2/activity_code/collaborator/offline_delete")
//    @Headers("Content-Type: application/json")
//    fun syncDeleteCollaborators(
//            @Header("Authorization") authentication: String,
//            @Body body: SyncDeleteCollaborators
//    ): Observable<Any>

}