package com.amplemind.vivasmart.features.quality_control

import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import com.amplemind.vivasmart.features.production_quality.SignatureDialogFragment
import com.amplemind.vivasmart.features.production_quality.adapters.QualityControlListAdapter
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlListItemViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.ListControlViewModel
import kotlinx.android.synthetic.main.content_list_control_activity.*
import kotlinx.android.synthetic.main.progress_track_component.*
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/17/18.
 */
@SuppressLint("Registered")
open class ListControlActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ListControlViewModel

    @Inject
    lateinit var picturesHelper: PicturesHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_list_control_activity)
        setupToolbar()
        initRecycler()
        loadData()
        initUI()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCondensedCollaborators(intent.getIntExtra("stage_id", 0))
    }

    private fun compressImage(bitmap: Bitmap): File {
        val image = picturesHelper.createImageFile(this)
        picturesHelper.compressImage(picturesHelper.createBitmapToFile(this, bitmap), image, 400f, 400f)
        return image
    }

    private fun initUI() {
        btn_send.addThrottle().subscribe(this::showSendReport).addTo(subscriptions)
    }

    private fun showSendReport(any: Any) {
        val dialog = SignatureDialogFragment().newInstance("Enviar reporte de calidad", "¿Está seguro de querer enviar este reporte? Por favor firme para continuar", false, mode = 0)
        dialog.show(supportFragmentManager, SignatureDialogFragment.TAG)
        dialog.setOnCompleteListener(object : OnSignatureCompleteListener {
            override fun onDismiss() {}

            override fun completeSign(sign1: Bitmap, sign2: Bitmap?) {
                val signs = mutableListOf<File>()
                signs.add(compressImage(sign1))
                if (sign2 != null) {
                    signs.add(compressImage(sign2))
                }
                viewModel.uploadSign(signs).addTo(subscriptions)
            }
        })
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_list_control
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.quality_control)
        supportActionBar!!.subtitle = intent.getStringExtra("title")
        lbl_track_title.text = "/ Calidad / Producción / Labores / "+intent.getStringExtra("title")+" /"
    }

    private fun initRecycler() {
        rv_list_control.hasFixedSize()
        rv_list_control.layoutManager = LinearLayoutManager(this)
        rv_list_control.adapter = QualityControlListAdapter(this)
        clickItemList()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadData() {
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onRequestSuccess().subscribe(this::onRequestSuccess).addTo(subscriptions)
        viewModel.onSuccessUploadSign().subscribe(this::onSuccessUpdateImage).addTo(subscriptions)
        viewModel.onActivityFinalize().subscribe(this::onActivityFinalize).addTo(subscriptions)
    }

    private fun onSuccessUpdateImage(files: MutableList<File>) {
        if (files.isNotEmpty()) {
            viewModel.uploadSign(files).addTo(subscriptions)
        } else {
            viewModel.finalizeControlActivity(intent.getIntExtra("stage_id", 0))
        }
    }

    private fun onActivityFinalize(success: Boolean) {
        finish()
    }

    private fun onRequestSuccess(list: List<ControlListItemViewModel>) {
        updateQualityAverage(list)
        if (list.isEmpty()) {
            ll_list_empty.visibility = View.VISIBLE
            btn_send.visibility = View.GONE
        }
        (rv_list_control.adapter as QualityControlListAdapter).addElements(list)
    }

    private fun updateQualityAverage(list: List<ControlListItemViewModel>) {
        val average = findViewById<TextView>(R.id.average)
        if (list.isNotEmpty()) {
            average.text = applicationContext.getString(R.string.percentage, viewModel.getQualityAverage())
        }
        else {
            average.text = applicationContext.getString(R.string.percentage, 100)
        }
    }

    private fun clickItemList() {
        (rv_list_control.adapter as QualityControlListAdapter).onClickEvent()
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe {
                    val activityIntent = Intent(this, ActivitiesControlQualityActivity::class.java)
                    activityIntent.putExtra("NAME", it.name)
                    activityIntent.putExtra("NUMBER", it.total)
                    activityIntent.putExtra("stage_id", intent.getIntExtra("stage_id", 0))
                    activityIntent.putExtra("collaborator_id", it.id_collaborator)
                    activityIntent.putExtra("title", supportActionBar!!.title)
                    activityIntent.putExtra("lot_name", supportActionBar!!.subtitle)
                    val bndlanimation = ActivityOptions.makeCustomAnimation(this, R.anim.enter, R.anim.exit).toBundle()
                    startActivity(activityIntent, bndlanimation)
                }.addTo(subscriptions)
    }

}