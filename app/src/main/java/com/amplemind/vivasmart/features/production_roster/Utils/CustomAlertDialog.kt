package com.amplemind.vivasmart.features.production_roster.Utils

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.databinding.adapters.AdapterViewBindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.utils.STAGE_TYPE_ID
import com.amplemind.vivasmart.features.production_roster.adapters.CollaboratorSelectedAdapter
import com.amplemind.vivasmart.features.production_roster.adapters.SelectedItem
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.google.android.material.snackbar.Snackbar
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.alert_layout.*
import kotlinx.android.synthetic.main.alert_layout.view.*
import java.lang.Exception

class CustomAlertDialog: SelectedItem {

    private var activity: Activity
    private lateinit var alertDialog:AlertDialog
    private var step = 0
    private lateinit var viewInf:View
    var arrayNames = ArrayList<String>()
    lateinit var resultStageModel:StageModel
    private lateinit var sendVacancie:SendDataDelegate
    val realm:Realm = Realm.getDefaultInstance()

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: CollaboratorSelectedAdapter
    var arraySelected = ArrayList<ActivityLogModel>()

    constructor(myActivity: Activity){
        activity = myActivity
    }

    fun alertNot(delegate:SendDataDelegate, array_uno:RealmResults<LotModel>, array_collaborators:List<ActivityLogModel>, currentLot:LotModel){
        var builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        var inflater: LayoutInflater = activity.layoutInflater
        viewInf = inflater.inflate(R.layout.alert_layout,null)
        sendVacancie = delegate
        builder.setView( viewInf)
        builder.setCancelable(false)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))
        arraySelected.addAll(array_collaborators)

        viewInf.findViewById<Button>(R.id.btn_cancel).setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                alertDialog.dismiss()
            }
        })


        list = viewInf.findViewById(R.id.list_users)
        layoutManager = LinearLayoutManager(activity.applicationContext)
        adaptador = CollaboratorSelectedAdapter(activity, array_collaborators, this)
        list.isNestedScrollingEnabled = false
        list.layoutManager = layoutManager
        list.adapter = adaptador

        val spinner_activity = viewInf.findViewById<Spinner>(R.id.spinner_activity)
        val spinner_lotes = viewInf.findViewById<Spinner>(R.id.spinner_lote)

        var currentLotcounter = 0
        var flag = 0
        var lotes = ArrayList<String>()

        array_uno.forEach {
            lotes.add(it.name!!)
            if(it.name == currentLot.name){
                currentLotcounter = flag
            }
            flag ++
        }



        if (spinner_lotes != null) {
            val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, lotes)
            spinner_lotes.adapter = adapter
        }

        spinner_lotes.setSelection(currentLotcounter)

        lateinit var resultActivityodel:RealmResults<ActivityModel>
        var lote_selected:LotModel = LotModel()
        var activity_selected:ActivityModel = ActivityModel()

        spinner_lotes.onItemSelectedListener = (object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {

                lote_selected = array_uno.get(position)!!
                var query = realm.where(StageModel::class.java)
                query.equalTo("lotId",array_uno.get(position)!!.id)
                resultStageModel = query.findFirst()!!

                var queryActivities = realm
                        .where(ActivityModel::class.java)
                        .equalTo("activitySubCategoryId", STAGE_TYPE_ID)
                queryActivities.equalTo("crops.cropId", resultStageModel.cropId)
                queryActivities.sort("name", Sort.ASCENDING)
                resultActivityodel = queryActivities.findAll()
                var activities = ArrayList<String>()
                resultActivityodel.forEach {
                    activities.add(it.name)
                }

                Log.e("-------------------> ", activities.toString())

                if (spinner_activity != null) {
                    val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, activities)
                    spinner_activity.adapter = adapter
                }
            }
        })

        spinner_activity.onItemSelectedListener = (object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                activity_selected = resultActivityodel.get(position)!!
            }
        })


        viewInf.findViewById<Button>(R.id.btn_action).setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {

                try {
                    sendVacancie.sendSing(lote_selected, activity_selected, resultStageModel, viewInf.rb_sign.isChecked, arraySelected)
                }catch (e:Exception){
                    resultStageModel = StageModel()
                    sendVacancie.sendSing(lote_selected, activity_selected, resultStageModel, viewInf.rb_sign.isChecked, arraySelected)
                }
            }
        })


        viewInf.findViewById<RadioGroup>(R.id.rdg_end_activity).setOnCheckedChangeListener(object:RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {

                val checkedRadioButton = p0!!.findViewById(R.id.rb_sign) as RadioButton
                val isChecked = checkedRadioButton.isChecked
                val buttonOk = viewInf.findViewById<Button>(R.id.btn_action)

                if (isChecked) {
                    viewInf.findViewById<LinearLayout>(R.id.layout_move_activity).visibility = View.GONE
                    buttonOk.setBackgroundColor(activity.resources.getColor(R.color.colorPrimary))
                    buttonOk.setTextColor(activity.resources.getColor(R.color.white))
                }else{
                    viewInf.findViewById<LinearLayout>(R.id.layout_move_activity).visibility = View.VISIBLE
                    if(arraySelected.size == 0){
                        buttonOk.setBackgroundColor(activity.resources.getColor(R.color.mililightgary))
                        buttonOk.setTextColor(activity.resources.getColor(R.color.colorPrimaryDark))
                    }else{
                        buttonOk.setBackgroundColor(activity.resources.getColor(R.color.colorPrimary))
                        buttonOk.setTextColor(activity.resources.getColor(R.color.white))
                    }
                }
            }
        })
        alertDialog.show()
    }
    override fun clickItem(item: ActivityLogModel, position: Int, isChecked:Boolean) {
        if(isChecked){
            arraySelected.add(item)
        }else{
            arraySelected.remove(item)
        }
        val buttonOk = viewInf.findViewById<Button>(R.id.btn_action)

        if(arraySelected.size == 0){
            buttonOk.setBackgroundColor(activity.resources.getColor(R.color.mililightgary))
            buttonOk.setTextColor(activity.resources.getColor(R.color.colorPrimaryDark))
        }else{
            buttonOk.setBackgroundColor(activity.resources.getColor(R.color.colorPrimary))
            buttonOk.setTextColor(activity.resources.getColor(R.color.white))
        }
    }

}
interface SendDataDelegate{
    fun sendSing(lote:LotModel, actividad:ActivityModel, stage:StageModel, option_selected:Boolean, items_seleted:ArrayList<ActivityLogModel>)
}