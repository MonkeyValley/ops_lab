package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.platform.SyncNotification
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.domain.controlQuality.UploadControlQuality
import com.amplemind.vivasmart.domain.qualityControl.UploadQualityControl
import com.amplemind.vivasmart.domain.qualitycarryorder.UploadQualityCarryOrder
import com.amplemind.vivasmart.domain.reciba.UploadReciba
import com.amplemind.vivasmart.domain.rosterpackage.UploadRosterPackage
import com.amplemind.vivasmart.domain.rosterproduction.UploadRosterProduction
import javax.inject.Inject

class PendingRequestRepository @Inject constructor(private val uploadRosterProduction: UploadRosterProduction,
                                                   private val uploadQualityControl: UploadQualityControl,
                                                   private val uploadControlQuality: UploadControlQuality,
                                                   private val uploadRosterPackage: UploadRosterPackage,
                                                   private val uploadQualityCarryOrder: UploadQualityCarryOrder,
                                                   private val uploadReciba: UploadReciba) {


    fun syncRosterProduction(delegate: SyncNotification, subscribe: AndroidDisposable) {
        uploadRosterProduction.collaboratorsReadyToSync(delegate, subscribe)
    }

    fun syncQualityControl(delegate: SyncNotification, subscribe: AndroidDisposable) {
        uploadQualityControl.syncIssues(delegate, subscribe)
    }

    fun syncLabors(delegate: SyncNotification, subscribe: AndroidDisposable) {
        uploadControlQuality.syncLabors(delegate, subscribe)
    }

    fun syncRosterPackage(delegate: SyncNotification, subscribe: AndroidDisposable) {
        uploadRosterPackage.collaboratorsPackageReadyToSync(delegate, subscribe)
    }

    fun syncQualityCarryOrder(delegate: SyncNotification, subscribe: AndroidDisposable) {
        uploadQualityCarryOrder.syncCarryOrders(delegate, subscribe)
    }

    fun syncReciba(delegate: SyncNotification, subscribe: AndroidDisposable) {
        uploadReciba.syncReciba(delegate, subscribe)
    }

}