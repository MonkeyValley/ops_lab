package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class BusinessUnitModel(
        @SerializedName("id")      var id: Int = 0 ,
        @SerializedName("name")    var name: String? = null) : RealmObject(), Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BusinessUnitModel> {
        override fun createFromParcel(parcel: Parcel): BusinessUnitModel {
            return BusinessUnitModel(parcel)
        }

        override fun newArray(size: Int): Array<BusinessUnitModel?> {
            return arrayOfNulls(size)
        }
    }
}
