package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.PlanningRepository
import com.amplemind.vivasmart.core.repository.response.CropReportResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.subjects.BehaviorSubject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class CropReportViewModel @Inject constructor(private val repository: PlanningRepository,
                                              private val apiErrors: HandleApiErrors,
                                              private val mediator: Mediator) {

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val requestFail = BehaviorSubject.create<String>()
    private val requestSuccess = BehaviorSubject.create<List<ItemCropReportViewModel>>()
    private var response: CropReportResponse? = null

    private fun currentDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return sdf.format(Date())
    }

    /*
     * Get Totals
     * */

    fun getTotalRecollection(): String {
        if (response == null) return ""
        return "${response!!.totalCollectedBoxes}"
    }

    fun getDifference(): String {
        if (response == null) return ""
        return "${response!!.differenceBoxes}"
    }

    fun getTotalSend(): String {
        if (response == null) return ""
        return "${response!!.totalOrderBoxes}"
    }

    /*
     * Get Report
     * */
    /*
    fun getReport(): Disposable {
        return repository.getCropReport(mediator.bundleControlQualityItemViewModel.lotId, currentDate())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    response = it
                    it.data.map { ItemCropReportViewModel(it) }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccess.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }
    */

    /*
     * Return Observers
     * */

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onError(): BehaviorSubject<String> {
        return requestFail
    }

    fun onRequestSuccess(): BehaviorSubject<List<ItemCropReportViewModel>> {
        return requestSuccess
    }
}