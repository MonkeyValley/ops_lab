package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.*
import com.amplemind.vivasmart.core.repository.model.*
import com.amplemind.vivasmart.core.repository.remote.ActivityCodeApi
import com.amplemind.vivasmart.core.repository.request.*
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import io.reactivex.Single
import org.jetbrains.anko.doAsync
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ActivityCodeRepository @Inject constructor(private val api: ActivityCodeApi,
                                                 private val database: ActivitiesDao,
                                                 private val databaseCollaborator: CollaboratorsDao,
                                                 private val databaseLines: CollaboratorsLineDao,
                                                 private val databaseLots: LotsDao,
                                                 private val apiAuthorization: ApiAuthorization,
                                                 private val preferences: UserAppPreferences) {

    data class CreateActivityCodeRequest(@SerializedName("activity_id") val activityCode: Int,
                                         @SerializedName("stage_id") val stageId: Int)

    data class GroovesIds(val id: Int)

    data class GroovesAssing(@SerializedName("groove_no") val groove_no: Int, @SerializedName("table_no") val table_no: Int)

    //region Update Grooves Assing data class
    data class GroovesUpdate(@SerializedName("bulk") val grooves: List<ListGroovesUpdate>)

    data class ListGroovesUpdate(@SerializedName("id") val id: Long, @SerializedName("grooves") val grooves: List<GroovesAssing>)
    //endregion

    data class FinishCollaboratorGrooveRequest(@SerializedName("collaborator_id") val collaboratorId: Int,
                                               @SerializedName("activity_code_id") val activityCode: Int,
                                               @SerializedName("quantity") val quantity: Int,
                                               @SerializedName("groove_no") val grooves: List<GroovesIds>,
                                               @SerializedName("table_no") val tableNo: Int,
                                               @SerializedName("time") val time: Long,
                                               @SerializedName("date") val date: String,
                                               @SerializedName("is_training") val isTraining: Boolean,
                                               @SerializedName("sign") val sign: String?)

    data class AssignGroovesRequest(
            @SerializedName("collaborator_id") val collaboratorId: Int,
            @SerializedName("activity_code_id") val activityCode: Int,
            @SerializedName("quantity") val quantity: Int,
            @SerializedName("grooves") val grooves: List<GroovesAssing>,
            @SerializedName("time") val time: Long,
            @SerializedName("date") val date: String,
            @SerializedName("is_training") val isTraining: Boolean,
            @SerializedName("sign") val sign: String?,
            @SerializedName("is_practice") val isPractice: Boolean = false)

    data class FinishCollaboratorUnitRequest(@SerializedName("collaborator_id") val collaboratorId: Int,
                                             @SerializedName("activity_code_id") val activityCode: Int,
                                             @SerializedName("quantity") val quantity: Int,
                                             @SerializedName("time") val time: Long,
                                             @SerializedName("date") val date: String,
                                             @SerializedName("is_training") val isTraining: Boolean,
                                             @SerializedName("sign") val sign: String?,
                                             @SerializedName("is_practice") val isPractice: Boolean = false)

    data class UpdateResultResponse(
            @SerializedName("remaining") val remaining: Int,
            @SerializedName("total") val total: Int
    )

    data class UpdateCollaboratorGrooveRequest(
            @SerializedName("bulk") val collaborators: List<UpdateCollaboratorGroove>
    )


    data class UpdateCollaboratorGroove(
            @SerializedName("id") val id_activity_log: Int,
            @SerializedName("table_no") val table_no: Int,
            @SerializedName("groove_no") val grooves: List<GroovesIds>
    )

    fun getCodes(token: String, activityId: Int, stageId: Int): Observable<ArrayList<ActivityCodeModel>> {
        return api.getCodes(apiAuthorization.getAuthToken(token), activityId, stageId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun saveLocalCodes(codes: List<ActivityCodeModel>, activityId: Int, stageId: Int) {
        database.deleteActivityCodeForStage(activityId, stageId)
        codes.map { database.insertActivityCode(it) }
    }

    fun saveLocalCode(code: ActivityCodeModel) {
        database.insertActivityCode(code)
    }

    fun getLocalCodes(activityId: Int, stageId: Int): Observable<List<ActivityCodeModel>> {
        return Observable.fromCallable {
            val codes = database.getLocalActivityCodes(activityId, stageId)
            codes.map {
                it.activeCollaborators = databaseCollaborator.collaboratorsRunningInCode(it.id?.toInt()
                        ?: 0)
            }
            codes
        }
    }

    fun savePendingRequestActivityCode(code: ActivityCodeModel): Observable<Long> {
        return Observable.fromCallable {
            database.insertActivityCode(code)
        }
    }

    fun createCode(token: String, activityId: Int, stageId: Int): Observable<ActivityCodeModel> {
        val request = CreateActivityCodeRequest(activityId, stageId)
        return api.createCode(apiAuthorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun updateActivityCodeQuantity(quantity: Int, activityCode: Int,totals: String?, remaining_total: Int?) {
        doAsync {
            val split = (totals ?: "0/0").replace("Estado: ","").split(" ", "/")
            val remaining = split[0].toInt()
            val total = split[1]
            database.updateTotalActivityCode(activityCode, "$remaining/$total", remaining_total
                    ?: 0)
        }
    }

    /*
     * Collaborators Region
     */
    fun getActivitiesByCode(activity_code: Int): Observable<CollaboratorsReponseActivities> {
        return api.getActivitiesByCode(authentication = apiAuthorization.getAuthToken(preferences.token), activityCode = activity_code)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun finishCollaborator(token: String, collaborators: List<FinishCollaborator>): Observable<Any> {
        return api.finishCollaborator(apiAuthorization.getAuthToken(token), FinishCollaboratorRequest(collaborators))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun updateCollaborator(token: String, collaborators: List<UpdateCollaborator>, activityCode: Int?): Observable<Any> {
        return api.updateCollaborator(apiAuthorization.getAuthToken(token), UpdateCollaboratorRequest(collaborators, activityCode))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun updateCollaboratorUnity(collaborators: List<UpdateCollaboratorUnity>, isLineFlow: Boolean): Observable<Any> {
        return if (isLineFlow) {
            api.updateCollaboratorUnitLines(apiAuthorization.getAuthToken(preferences.token), UpdateCollaboratorUnityRequest(collaborators))
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        } else {
            api.updateCollaboratorUnit(apiAuthorization.getAuthToken(preferences.token), UpdateCollaboratorUnityRequest(collaborators))
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        }
    }

    fun updateLocalCollaboratorUnity(time: Long, quantity: Double, activityLogId: Int): Observable<Any> {
        return Observable.fromCallable {
            databaseCollaborator.updateCollaboratorTimeQuantity(quantity, time, activityLogId)
        }
    }


    fun updateLocalCollaboratorUnityOffline(time: Long, quantity: Double, idDetailLocal: Int): Observable<Any> {
        return Observable.fromCallable {
            databaseCollaborator.updateCollaboratorTimeQuantityLines(quantity, time, idDetailLocal)
        }
    }

    fun finishCollaboratorSign(request: List<UpdateCollaboratorSign>, isLineFlow: Boolean): Observable<Any> {
        return if (isLineFlow) {
            api.finishCollaboratorSignLines(apiAuthorization.getAuthToken(preferences.token), UpdateCollaboratorSignRequest(request))
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        } else {
            api.finishCollaboratorSign(apiAuthorization.getAuthToken(preferences.token), UpdateCollaboratorSignRequest(request))
                    .timeout(TIME_OUT, TimeUnit.SECONDS)
                    .retry(RETRY)
        }
    }

    fun finishLocalCollaboratorProduction(id_activities: List<Int>, isTraining: Boolean, sign: String, isProduction: Boolean): Observable<Any> {
        return Observable.fromCallable {
            id_activities.map { id ->
                if (isProduction) {
                    databaseCollaborator.signCollaborator(isTraining, sign, id)
                } else {
                    databaseCollaborator.signOfflineCollaboratorInLines(isTraining, sign, id)
                }
            }
        }
    }

    fun finishLocalProductionCollaboratorSign(activityCodeDetail: ActivityCodeDetail, collaborators: List<TimerUserViewModel>,
                                              sign: String?, isTraining: Boolean, isPractice: Boolean, activityCode: Int): Observable<Boolean> {
        return Observable.fromCallable {
            val sdf = SimpleDateFormat("dd-MMM-yyyy hh:mm:ss", Locale.getDefault())
            val date = sdf.format(Date())
            collaborators.map { collaborator ->
                val detailRoster = DetailtRosterResponse(activityCodeDetail, collaborator.getActivityCodeCollaboratorId(), activityCode, collaborator.getCollaboratorId(),
                        date, null, true, isTraining, isPractice, collaborator.units.toDouble(), sign, collaborator.time.toInt(), 0, 0.0, activityLogIdLocalDB = collaborator.activityLogId!!)

                databaseCollaborator.insertCollaboratorReadyToReview(detailRoster)
                databaseCollaborator.finishCollaborator(collaborator.getCollaboratorIdDB())
            }
            true
        }
    }

    fun saveCollaborators(collaborators: List<CollaboratorsListReponse>, stage_id: Int, activity_code: Int, activity_id: Int) {
        collaborators.map { collaborator ->
            collaborator.stageId = stage_id
            collaborator.activityCode = activity_code
            collaborator.activityId = activity_id
            databaseCollaborator.insertCollaborator(collaborator)
        }
    }

    fun getLocalCollaborators(activityCode: Int): Single<List<CollaboratorsListReponse>> {
        return databaseCollaborator.getCollaborators(activityCode)
    }

    /*
     * End Collaborators Region
     */

    fun addUnitsCollaborator(collaborators: List<AddMoreUnitCollaborator>): Observable<Any> {
        return api.addMoreCollaboratorUnit(apiAuthorization.getAuthToken(preferences.token), AddMoreCollaboratorUnityRequest(collaborators))
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun assingCollaboratorUnit(request: FinishCollaboratorUnitRequest): Observable<GroovesResultReponse> {
        return api.finishCollaboratorUnit(apiAuthorization.getAuthToken(preferences.token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun restartTimer(token: String, request: RestartTimerCollaboratorRequest): Observable<Any> {
        doAsync {
            request.collaborators.map { databaseCollaborator.restartCollaboratorTime(it.id, System.currentTimeMillis()) }
        }
        return api.restartTimerCollaborators(apiAuthorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun pauseTimer(token: String, request: PauseTimerCollaboratorRequest): Observable<Any> {
        doAsync {
            request.collaborators.map { databaseCollaborator.updateCollaboratorPause(it.id, it.time) }
        }

        return api.pauseTimerCollaborators(apiAuthorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun restartLocalCollaborators(collaborators: List<RestartTimerCollaborator>): Observable<Any> {
        return Observable.fromCallable {
            collaborators.map {
                databaseCollaborator.restartCollaboratorTime(it.id, System.currentTimeMillis())
            }
        }

    }

    fun pauseLocalCollaborators(collaborators: List<PauseTimerCollaborator>): Observable<Any> {
        return Observable.fromCallable {
            collaborators.map {
                databaseCollaborator.updateCollaboratorPause(it.id, it.time)
            }
        }
    }

    fun deletCollaborator(token: String, collaboratorId: Int, activityLogId: Long): Observable<Any> {
        doAsync { databaseCollaborator.deleteCollaborator(collaboratorId.toLong()) }
        return api.deleteCollaborator(apiAuthorization.getAuthToken(token), collaboratorId, activityLogId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun deleteLocalCollaborator(collaboratorId: Long, activityLogId: Long, isCreatedOffLine: Boolean): Observable<Any> {
        return Observable.fromCallable {
            if (!isCreatedOffLine) {
                databaseLines.deleteCollaboratorBySync(DeleteCollaboratorsOffline(idRegister = collaboratorId.toInt(), activityLog = activityLogId.toInt()))
            }
            databaseCollaborator.deleteCollaborator(collaboratorId)
        }
    }

    fun changeStatusLot(stage_id: Int, activityId: Int) {
        val stageStatus = databaseCollaborator.collaboratorsIsRunningStage(stage_id)
        databaseLots.updateLotStatus(stageStatus, stage_id)

        val activityStatus = databaseCollaborator.collaboratorsIsRunning(activityId, stage_id)
        database.updateActivityStatus(activityStatus, activityId)
    }


//    fun getLocalCollaborator(collaboratorId: Int, activityCodeId: Int, activityLogId: Long): Observable<CollaboratorsListReponse> {
//        return Observable.fromCallable {
//            val timer = TimerUserViewModel()
//            val collaborator = CollaboratorsListReponse(activityCodeId,
//                    CollaboratorsReponse("", "", collaboratorId, "", "", null),
//                    collaboratorId, collaboratorId, false, false, 0.0, true, false, 0, date = 0)
//            timer.setActivities(collaborator)
//
//            databaseCollaborator.updateCollaboratorPause()
//            collaborator
//        }
//    }

}