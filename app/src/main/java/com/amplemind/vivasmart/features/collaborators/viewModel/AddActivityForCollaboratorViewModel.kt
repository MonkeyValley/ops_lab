package com.amplemind.vivasmart.features.collaborators.viewModel

import com.amplemind.vivasmart.core.extensions.registerObserver
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.CollaboratorsRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.AssignCollaboratorModel
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.response.ActivityLinesResponse
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import com.amplemind.vivasmart.core.utils.FLOW_ACTIVITES
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class AddActivityForCollaboratorViewModel @Inject constructor(private val repository: CollaboratorsRepository,
                                                              private val preferences: UserAppPreferences,
                                                              private val apiErrors: HandleApiErrors,
                                                              var mediator: Mediator) {

    val activities = mutableListOf<ItemAddActivitiesViewModel>()

    private val activitiesSubject = BehaviorSubject.create<List<ItemAddActivitiesViewModel>>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val assingSuccess = BehaviorSubject.create<CollaboratorsInLinesResult>()
    private val requestFail = BehaviorSubject.create<String>()

    private val searchLocalActivities = BehaviorSubject.create<Boolean>()

    fun getActivities(): Disposable? {
        val user = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        if (user.businessUnitId == null) {
            requestFail.onNext("Usuario sin unidad de negocio requerida, favor de verificar")
            return null
        }
        return repository.getLineActivities(preferences.token, mediator.bundleItemLinesPackage.toString())
                .registerObserver(progressStatus)
                .map {
                    repository.saveActivities(it.activities)
                    mapModelToViewModels(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    activities.addAll(it)
                }
                .subscribe({
                    activitiesSubject.onNext(activities)
                }, {
                    searchLocalActivities.onNext(true)
                })
    }

    fun getActivitiesLocal(): Disposable {
        return repository.getLineActivitiesLocal(mediator.bundleItemLinesPackage!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    mapModelToViewModels(ActivityLinesResponse(it))
                }
                .subscribe({
                    activities.clear()
                    activities.addAll(it)
                    activitiesSubject.onNext(activities)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSearchLocalActivities(): BehaviorSubject<Boolean> {
        return searchLocalActivities
    }

    fun searchActivities(query: String) {
        if (query.isEmpty()) {
            activitiesSubject.onNext(activities)
        }
        activitiesSubject.onNext(activities.filter { it.name!!.toLowerCase().contains(query.toLowerCase()) })
    }

    fun getData(): BehaviorSubject<List<ItemAddActivitiesViewModel>> {
        return activitiesSubject
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onAssingCollaboratorSucess(): BehaviorSubject<CollaboratorsInLinesResult> {
        return assingSuccess
    }

    private fun mapModelToViewModels(response: ActivityLinesResponse): List<ItemAddActivitiesViewModel> {
        val list = arrayListOf<ItemAddActivitiesViewModel>()

        for (item in response.activities) {
            list.add(ItemAddActivitiesViewModel(model = item, flow = FLOW_ACTIVITES))
        }
        return list
    }

    fun assignCollaborator(hasInternet: Boolean, idCollaborators: List<CollaboratorModel>, activity_id: Int?, name: String?, collaborator_left: Int?): Disposable {
        val list = mutableListOf<AssignCollaboratorModel>()

        val id_line = mediator.bundleItemLinesPackage!!

        for (user in idCollaborators) {
            list.add(AssignCollaboratorModel(id_line, activity_id!!, user.id, null, true))
        }
        return if (hasInternet) {
            assignCollaboratorOnline(list)
        } else {
            saveCollaboratorLocal(list, idCollaborators, name, collaborator_left, activity_id!!, id_line)
        }
    }

    private fun saveCollaboratorLocal(list: MutableList<AssignCollaboratorModel>, idCollaborators: List<CollaboratorModel>, name: String?, collaborator_left: Int?, activity_id: Int?, id_line: Int): Disposable {
        return repository.saveCollaboratorsAndLineLocal(list, idCollaborators, name, collaborator_left, activity_id, id_line, count_box = mediator.getMapCountBoxRequest())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({ line ->
                    assingSuccess.onNext(line)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun assignCollaboratorOnline(list: MutableList<AssignCollaboratorModel>): Disposable {
        val assing = CollaboratorsRepository.AssingCollaborators(list, mediator.getMapCountBoxRequest())
        return repository.assingCollaborator(token = preferences.token, assing = assing)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    assingSuccess.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

}
