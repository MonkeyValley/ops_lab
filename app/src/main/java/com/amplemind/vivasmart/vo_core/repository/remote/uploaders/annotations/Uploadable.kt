package com.amplemind.vivasmart.vo_core.repository.remote.uploaders.annotations

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Uploadable(
        val path: String,
        val deletePath: String = "",
        val contentType: String
)


