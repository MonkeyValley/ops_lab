package com.amplemind.vivasmart.features.production_roster.dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class DeleteCollaboratorDialog: BaseDialogFragment() {

    private var onDeleteCollaborator: DeleteCollaboratorListener? = null

    companion object {
        val TAG = "DeleteCollaboratorDialog"

        fun newInstance(image: String, name: String): DeleteCollaboratorDialog {
            val dialog = DeleteCollaboratorDialog()
            val args = Bundle()
            args.putString("image", image)
            args.putString("name", name)
            dialog.arguments = args
            return dialog
        }
    }

    fun setOnDeleteCollaboratorListener(listener: DeleteCollaboratorListener) {
        onDeleteCollaborator = listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val root = LayoutInflater.from(context).inflate(R.layout.content_delete_collaborator_dialog, null, false)
        builder.setView(root)

        val name = root.findViewById<TextView>(R.id.tv_user_name)
        val image = root.findViewById<ImageView>(R.id.profileImage)
        name.text = arguments!!.getString("name")
        Glide.with(context!!)
                .load(arguments!!.getString("image"))
                .apply(RequestOptions()
                        .placeholder(R.drawable.ic_user))
                .into(image)

        builder.setPositiveButton("SI", {
            _, i ->
            onDeleteCollaborator?.onDelete(true)
            dismiss()
        })

        builder.setNegativeButton("CANCELAR", {
            _, i ->
            onDeleteCollaborator?.onDelete(false)
            dismiss()
        })

        return builder.create()
    }

    interface DeleteCollaboratorListener {
        fun onDelete(isDeleted: Boolean)
    }

}