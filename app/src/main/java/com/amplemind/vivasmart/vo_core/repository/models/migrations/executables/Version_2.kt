package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm
import io.realm.FieldAttribute
import java.util.*

class Version_2 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        createDownloadObjectModel(realm)
        addCollaboratorsIsActive(realm)
        changeActivityUUID(realm)
        addActivityLogVersion(realm)
    }

    override fun drop(realm: DynamicRealm) {
        dropDownloadObjectModel(realm)
        dropCollaboratorsIsActive(realm)
        dropChangeActivityUUID(realm)
        dropActivityLogVersion(realm)
    }

    private fun createDownloadObjectModel(realm: DynamicRealm) {
        realm.schema.createWithPrimaryKeyField("DownloadObjectModel", "type", String::class.java, FieldAttribute.REQUIRED)
                .addField("isServerFaulty", Boolean::class.java)
                .addField("firstSyncDone", Boolean::class.java, FieldAttribute.INDEXED)
                .addField("requestDate", Date::class.java, FieldAttribute.INDEXED)
                .addField("lastUpdated", Date::class.java, FieldAttribute.INDEXED)
                .addField("timesRequested", Int::class.java, FieldAttribute.INDEXED)
                .addRealmListField("deletes", String::class.java)
    }

    private fun addCollaboratorsIsActive(realm: DynamicRealm) {
        realm.schema.get("CollaboratorModel")
                ?.addField("isActive", Boolean::class.java, FieldAttribute.INDEXED)
    }

    private fun changeActivityUUID(realm: DynamicRealm) {
        realm.schema.get("ActivityModel")
                ?.setRequired("uuid", true)
    }

    private fun addActivityLogVersion(realm: DynamicRealm) {
        realm.schema.get("ActivityLogModel")
                ?.addField("version", Long::class.java)
    }

    private fun dropDownloadObjectModel(realm: DynamicRealm) {
        realm.schema.remove("DownloadObjectModel")
    }

    private fun dropCollaboratorsIsActive(realm: DynamicRealm) {
        realm.schema.get("CollaboratorModel")
                ?.removeField("isActive")
    }

    private fun dropActivityLogVersion(realm: DynamicRealm) {
        realm.schema.get("ActivityLogModel")
                ?.removeField("version")
    }

    private fun dropChangeActivityUUID(realm: DynamicRealm) {
        realm.schema.get("ActivityModel")
                ?.setRequired("uuid", false)
    }

}