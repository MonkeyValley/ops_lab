package com.amplemind.vivasmart.features.production_lines.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemLotsInLinesBinding
import com.amplemind.vivasmart.features.production_lines.fragment.LotsInLinesFragment
import com.amplemind.vivasmart.features.production_lines.viewModel.ItemLotPackageViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.BehaviorSubject

class LotsInLinesAdapter : RecyclerView.Adapter<LotsInLinesAdapter.LotsLinesViewHolder>() {

    private val list = mutableListOf<ItemLotPackageViewModel>()

    private val onClick = BehaviorSubject.create<ItemLotPackageViewModel>()

    private var events : LotsInLinesFragment.OnEvents? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LotsLinesViewHolder {
        val binding = ItemLotsInLinesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LotsLinesViewHolder(binding)
    }

    override fun getItemCount() = list.size

    fun setEvents(events : LotsInLinesFragment.OnEvents){
        this.events = events
    }

    override fun onBindViewHolder(holder: LotsLinesViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(data: List<ItemLotPackageViewModel>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    inner class LotsLinesViewHolder(val binding: ItemLotsInLinesBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemLotPackageViewModel) {
            binding.setVariable(BR.itemModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.root.setOnClickListener {
                    events?.onClickEvent(item)
                }
            }
        }

    }

}
