package com.amplemind.vivasmart.features.production_roster.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.features.production_roster.adapters.ExtraHoursCreateRequestAdapter
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.utils.TYPE_EXTRA_TIME
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.PayrollActivitiesViewModel
import java.util.*
import javax.inject.Inject

private const val ARG_activities_list = "activities"
private const val ARG_activities_stage = "stage"
private const val ARG_message_data = "message_data"

class ExtraHoursCreateRequestFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: PayrollActivitiesViewModel

    @Inject
    lateinit var mediator: Mediator

    @Inject
    lateinit var preferences: UserAppPreferences

    private lateinit var mStage: StageModel
    private var mActivities = ArrayList<ActivityModel>()
    private var mExtraTimeActivities = mutableListOf<ExtraTimeModel>()
    private var mDataMessage: MessageDataModel? = null

    private var listener: OnFragmentInteractionListener? = null
    private lateinit var recycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mDataMessage = arguments!!.getParcelable(ARG_message_data)
            if(mDataMessage == null){
                mDataMessage = MessageDataModel()
                //mActivities = arguments!!.getParcelableArrayList(ARG_activities_list)
                //mStage = arguments!!.getParcelable(ARG_activities_stage)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_extra_hours_create_request, container, false)
        val btnCreate = view.findViewById(R.id.extraHoursRequest_btn_send) as RelativeLayout
        val btnCreateText = view.findViewById(R.id.extraHoursRequest_btn_txt) as TextView
        if(mDataMessage != null){
            // Caso cuando viene desde el centro de mensajes esta pantalla
            val activityM = ActivityModel(id = mDataMessage!!.activityId, icon = mDataMessage!!.activity_icon, name = mDataMessage!!.activity_name)
            mActivities.add(activityM)
            mStage = StageModel(id = mDataMessage!!.stage_id)

            btnCreateText.text = "Autorizar solicitud"
            btnCreate.setOnClickListener {
                Log.d("Authorize request", mExtraTimeActivities.size.toString())
                authorizeExtraHourRequest(view.context, mDataMessage!!)
            }

        }else{
            btnCreateText.text = "Generar solicitud"
            btnCreate.setOnClickListener {
                Log.d("CreateRequest", mExtraTimeActivities.size.toString())
                CreateExtraHoursRequest(view.context)
            }
        }

        recycler = view.findViewById(R.id.extraHoursRequest_rv_activities) as RecyclerView
        initRecycler()
        return view
    }

    private fun initRecycler() {
        recycler.hasFixedSize()
        recycler.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.adapter = ExtraHoursCreateRequestAdapter(mActivities)
        if(mDataMessage != null) {
            (recycler.adapter as ExtraHoursCreateRequestAdapter).onSelectedItem().subscribe(this::selecteditem).addTo(subscriptions)
            (recycler.adapter as ExtraHoursCreateRequestAdapter).setSelectedOptions(mDataMessage!!.collaboratorNo, mDataMessage!!.extraHours)
        } else {
            (recycler.adapter as ExtraHoursCreateRequestAdapter).onSelectedItem().subscribe(this::selectedAuthItem).addTo(subscriptions)
        }
    }

    private fun selecteditem(item: ExtraTimeModel) {
        if (item.extra_hours == 0 || item.collaborators_no == 0) {
            val exist = mExtraTimeActivities.find { it.activity_id == item.activity_id }
            if (exist != null) {
                mExtraTimeActivities.remove(exist)
                Toast.makeText(activity, item.activity_id.toString(), Toast.LENGTH_SHORT).show()
            }
        } else {
            val exist = mExtraTimeActivities.find { it.activity_id == item.activity_id }
            if (exist != null) {
                mExtraTimeActivities.remove(exist)
                mExtraTimeActivities.add(item)
            } else {
                mExtraTimeActivities.add(item)
            }
        }
    }

    private fun selectedAuthItem(item: ExtraTimeModel) {
        mDataMessage?.collaboratorNo = item.collaborators_no
        mDataMessage?.extraHours = item.extra_hours
    }

    private fun CreateExtraHoursRequest(context: Context) {
        var extraTime: MessageModel
        Log.d("CreateRequest", mExtraTimeActivities.size.toString())
        mExtraTimeActivities.forEach {
            val uuid = UUID.randomUUID()
            val data = MessageDataModel(activityId = it.activity_id, collaboratorNo = it.collaborators_no, extraHours = it.extra_hours)
            extraTime = MessageModel(type = TYPE_EXTRA_TIME, message = "EXTRA TIME", data = data, uuid = uuid.toString())
            Log.d("CreateRequest", it.activity_id.toString())
            // mExtraTimeActivities.remove(it)
        }

    }

    private fun authorizeExtraHourRequest(context: Context, messageDataModel: MessageDataModel){

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ExtraHoursCreateRequestFragment().apply {
                    arguments = Bundle().apply {
                        // putString(ARG_PARAM1, param1)
                        // putString(ARG_PARAM2, param2)
                    }
                }

        fun newInstanceActivities(stage: StageModel, activities: List<ActivityModel>) =
                ExtraHoursCreateRequestFragment().apply {
                    arguments = Bundle().apply {
                        //putParcelable(ARG_activities_stage, stage)
                        //putParcelableArrayList(ARG_activities_list, ArrayList(activities))
                    }
                }

        fun newInstanceAuthorize(data: MessageDataModel) =
                ExtraHoursCreateRequestFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_message_data, data)
                    }
                }

    }
}
