package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.production_range.repository.remote.ProductionRangeApi
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangeVariatiesModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ProductionRangesDataModel
import com.google.gson.Gson
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmObject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ProductionRangesDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mProductionRangeApi: ProductionRangeApi,
        private val mPref: UserAppPreferences
) : RealmDownloader<RealmObject?, ProductionRangeVariatiesModel>(realmHolder, ProductionRangeVariatiesModel::class.java) {


    override fun buildServerRequest(parent: RealmObject?, lastVersion: Long): Single<List<ProductionRangeVariatiesModel>> =
            mProductionRangeApi.getProductionRangeVarieties(
                    mPref.authorizationToken, businessId = mPref.user.businessUnitId
            ).map { response ->
                Log.e("Data-ProductionRange: ", response.data.toString())
                response.data
            }
}