package com.amplemind.vivasmart.vo_core.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.IssueModel
import com.google.gson.annotations.SerializedName

data class HarvestValidationCriteriaResponse (
    @SerializedName("data")         val data: List<IssueModel>,
    @SerializedName("total_count")  val totalCount: Int
)
