package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.RosterRepository
import com.amplemind.vivasmart.core.repository.model.LotsPackageResponse
import com.amplemind.vivasmart.core.repository.response.RosterLineResponse
import io.reactivex.Observable
import retrofit2.http.*

interface RosterApi {

    @GET("v2/packingline")
    @Headers("Content-Type: application/json")
    fun getLines(
            @Header("Authorization") authentication: String,
            @Query("packinghouse.business_unit_id") unitId: String,
            @Query("embed") embed: String = "packinghouse,crops",
            @Query("is_active") isActive: Boolean = true,
            @Query("__logic") logic: String = "AND"
    ): Observable<RosterLineResponse>

    @GET("/v2/lot/paysheet/{packing_line}/{bu}")
    @Headers("Content-Type: application/json")
    fun getLotsPackage(
            @Header("Authorization") authentication: String,
            @Path("packing_line") user_id: Int,
            @Path("bu") bu: String
    ): Observable<LotsPackageResponse>

    @POST("/v2/packingline_paysheet/stage/{packing_line}")
    @Headers("Content-Type: application/json")
    fun changeLotInLine(
            @Header("Authorization") authentication: String,
            @Path("packing_line") packing_line : Int,
            @Body body : RosterRepository.ChangeLotRequest
    ): Observable<Any>

    @PUT("/v2/packingline/{line_packing}")
    @Headers("Content-Type: application/json")
    fun assignLot(
            @Header("Authorization") authentication: String,
            @Path("line_packing") linesPackage: Int,
            @Body body : RosterRepository.ActualStageRequest
    ): Observable<Any>


}