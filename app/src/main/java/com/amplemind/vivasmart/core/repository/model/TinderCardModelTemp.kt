package com.amplemind.vivasmart.core.repository.model

data class TinderCardModelTemp(val id : Int, val name : String, val url : String)
