package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.PendingLogsModel
import javax.inject.Inject

class RevisedRosterRepository @Inject constructor(
        private val activityLogsDao: ActivityLogsDao
): Repository() {

    override fun cleanUp() {
        activityLogsDao.cleanUp()
    }

    fun getPendingLogs(stageUuid: String, stageTypeId: Int) =
            activityLogsDao.groupSignedLogs(stageUuid, stageTypeId)

    fun filterLogsByMode(pendingLogs: List<PendingLogsModel>, mode: Int) =
        activityLogsDao.filterLogsByMode(pendingLogs, mode)

}