package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.core.repository.remote.PackagingQualityApi
import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityRevisionModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReceptionQualityRevisionListRepository @Inject constructor (
        private val mPreferences: UserAppPreferences,
        private val mPackagingQualityApi: PackagingQualityApi
) : Repository() {

    override fun cleanUp() {
    }

    fun loadCarryOrder(carryOrderId: Long): Observable<CarryOrderModel> =
            mPackagingQualityApi.getItemCarryOrder(mPreferences.authorizationToken, id = carryOrderId.toInt(), businessUnitId = mPreferences.user.businessUnitId.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map {response ->
                        response.data.first()
                    }

    fun loadRevisions(carryOrderId: Long): Observable<List<ReceptionQualityRevisionModel>> =
            mPackagingQualityApi.getReceptionQualityRevisions(mPreferences.authorizationToken, carryOrderId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .map {response ->
                        response.data
                    }

}