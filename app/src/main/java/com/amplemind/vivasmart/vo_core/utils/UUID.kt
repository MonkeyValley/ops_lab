package com.amplemind.vivasmart.vo_core.utils

import java.util.*

fun getUUID(): String {
    return UUID.randomUUID().toString()
}