package com.amplemind.vivasmart.feature.login

import com.amplemind.vivasmart.AndroidTest
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.LoginModel
import com.amplemind.vivasmart.core.repository.remote.AccountApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.features.login.viewModel.LoginViewModel
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class LoginViewModelTest : AndroidTest() {

    // TODO falta probar el view model como tal

    private var testObserver: TestObserver<LoginModel.LoginResponse> = TestObserver.create()
    private val username: String = "user@test.com"
    private val password: String = "123456"

    private lateinit var repository: AccountRepository
    private lateinit var viewModel: LoginViewModel

    private val accountApi: AccountApi = mock()
    private val apiAuthorization: ApiAuthorization = mock()
    private val userAppPreferences: UserAppPreferences = mock()

    fun getMockRequest() = AccountRepository.LoginRequest(username, password)
    fun getMockResponse() = Observable.just(LoginModel.LoginResponse("Test Token"))


    @Before
    fun setup() {
        repository = AccountRepository(accountApi, apiAuthorization, userAppPreferences)
        viewModel = LoginViewModel(repository)
    }

    @Test
    fun onLoginSuccess() {
        whenever(accountApi.doLogin(getMockRequest())).thenReturn(getMockResponse())

        repository.doLogin(getMockRequest()).subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertValue {
            it.token == "Test Token"
        }
    }

    @Test
    fun onLoginSuccess2() {

        val mObservable: TestObserver<LoginModel.LoginResponse> = TestObserver.create()

        whenever(accountApi.doLogin(getMockRequest())).thenReturn(getMockResponse())

        //mViewModel.doLogin(getMockRequest()).subscribe(mObservable)

//        mObservable.awaitTerminalEvent()
        mObservable.awaitDone(1, TimeUnit.SECONDS)
        mObservable.assertNoErrors()

        val values = mObservable.values()

        mObservable.assertComplete()
    }

}