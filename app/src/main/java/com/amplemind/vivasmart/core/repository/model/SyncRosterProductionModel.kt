package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SyncRosterProductionModel(
                                @SerializedName("employee_code") val employeeCode: String,
                                @SerializedName("activity_id") val activityId: Int,
                                @SerializedName("stage_id") val stageId: Int,
                                @SerializedName("quantity") val quantity: Int,
                                @SerializedName("sign") var sign: String?,
                                @SerializedName("time") val time: Int,
                                @SerializedName("grooves") val grooves: List<GrooveSyncModel>,
                                @SerializedName("is_training") val isTraining: Boolean,
                                @SerializedName("is_practice") val isPractice: Boolean,
                                @SerializedName("activity_code") val activityCodeId: Int,
                                @SerializedName("offline_id") val offlineId: Int,
                                @SerializedName("status") val status: Boolean,
                                @SerializedName("is_done") val isDone: Boolean,
                                @Expose(serialize = false, deserialize = false) var stillRunning: Boolean = false)

data class GrooveSyncModel(
        @SerializedName("groove_no") val grooveNo: Int,
        @SerializedName("table_no") val tableNo: Int)

data class SyncRosterProductionRequest(@SerializedName("bulk") val bulk: List<SyncRosterProductionModel>)
data class SyncRosterProductionResponse(@SerializedName("data") val data: List<SyncRosterProductionModel>)