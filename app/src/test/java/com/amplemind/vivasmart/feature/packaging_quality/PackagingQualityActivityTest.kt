package com.amplemind.vivasmart.feature.packaging_quality

import android.content.Intent
import com.google.android.material.appbar.AppBarLayout
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.amplemind.vivasmart.AndroidTest
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.PackageQualityModelTemp
import com.amplemind.vivasmart.features.packaging_quality.DetailOrdenActivity
import com.amplemind.vivasmart.features.packaging_quality.PackagingQualityActivity
import com.amplemind.vivasmart.features.packaging_quality.QualitySearchCodeActivity
import org.junit.Before
import org.junit.Test
import org.robolectric.Robolectric
import org.robolectric.Shadows
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class PackagingQualityActivityTest : AndroidTest() {

    private var packagingActivity: PackagingQualityActivity? = null

    @Before
    fun setup() {
        packagingActivity = Robolectric.buildActivity(PackagingQualityActivity::class.java)
                .create()
                .get()
    }

    @Test
    fun `Activity should not be null`() {
        assertNotNull(packagingActivity)
    }

    @Test
    fun `toolbar create`(){
        val toolbar = packagingActivity?.findViewById<AppBarLayout>(R.id.toolbar_package)
        assertNotNull(toolbar)
    }

    @Test
    fun `verify status recycler`(){
        //given
        val recycler = packagingActivity?.findViewById<RecyclerView>(R.id.rv_quality)
        //then
        assertEquals(View.VISIBLE, recycler!!.visibility)
        assertNotNull(recycler.adapter)
    }

    @Test
    fun `DetailOrdenActivity must be called`(){
        //given
        val expectedIntent = Intent(packagingActivity, DetailOrdenActivity::class.java)

        //TODO: this is correct?
        val mock = PackageQualityModelTemp("1","name","description")

        //when
        packagingActivity?.clickItemList(mock)
        val actualItem = Shadows.shadowOf(packagingActivity).peekNextStartedActivity()

        //then
        assertTrue(expectedIntent.filterEquals(actualItem))
    }

    @Test
    fun `QualitySearchCodeActivity must be called`(){
        //given
        val expectedIntent = Intent(packagingActivity, QualitySearchCodeActivity::class.java)

        //when
        packagingActivity?.showQualitySearchCode()
        val actuaIntent = Shadows.shadowOf(packagingActivity).peekNextStartedActivity()

        //then
        assertTrue(expectedIntent.filterEquals(actuaIntent))
    }

    @Test
    fun `Finish activity back button`(){
        packagingActivity?.finishThisActivity()

        assertTrue(packagingActivity!!.isFinishing)
    }
}