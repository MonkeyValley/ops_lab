package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.ProductionQualityRepository
import com.amplemind.vivasmart.core.repository.model.CategoriesDetailModel
import com.amplemind.vivasmart.core.repository.model.IssuesResultResponse
import com.amplemind.vivasmart.core.repository.model.ProductionQualityModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.interfaces.ICategoriesDetail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject

class CategoriesDetailViewModel @Inject constructor(private val repository : ProductionQualityRepository, private val apiErrors: HandleApiErrors) {

    private val progressStatus = BehaviorSubject.create<Boolean>()

    private val issues_list = mutableListOf<CategoriesDetailItemViewModel>()

    private val requestFail = BehaviorSubject.create<Pair<String, Int?>>()

    private val issues = BehaviorSubject.create<List<CategoriesDetailItemViewModel>>()

    fun getIssuesByCategory(token: String , id_category : Int, id_lot : Int) : Disposable{
        return repository.getIssues(token, id_category, id_lot)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    repository.saveIssues(id_category, id_lot, it.data)
                    mapModeltoViewModel(it.data)
                }
                .subscribe({
                    issues_list.addAll(it)
                    issues.onNext(issues_list)
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), null))
                })
    }

    fun getLocalIssuesByCategory(id_category : Int, id_lot : Int) : Disposable{
        return repository.getLocalIssues(id_category, id_lot)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { mapModeltoViewModel(it.data) }
                .subscribe({
                    issues_list.addAll(it)
                    issues.onNext(issues_list)
                }, {
                    requestFail.onNext(Pair(apiErrors.handleLoginError(it), null))
                })
    }

    fun onProgressStatus() = progressStatus

    fun getIssuesList() = issues

    fun onRequestFail() = requestFail

    private fun mapModeltoViewModel(data : List<IssuesResultResponse>) : List<CategoriesDetailItemViewModel> {
        val model = mutableListOf<CategoriesDetailItemViewModel>()
        data.forEach {
            model.add(CategoriesDetailItemViewModel(CategoriesDetailModel(it.id ?:0,it.name?:"",it.count?:0)))
        }
        return model
    }
}