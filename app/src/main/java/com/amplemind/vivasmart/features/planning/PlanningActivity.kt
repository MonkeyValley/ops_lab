package com.amplemind.vivasmart.features.planning

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.planning.adapters.PlanningAdapter
import com.amplemind.vivasmart.features.planning.viewmodels.PlanningItemViewModel
import com.amplemind.vivasmart.features.planning.viewmodels.PlanningViewModel
import kotlinx.android.synthetic.main.activity_planning.*
import javax.inject.Inject

class PlanningActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: PlanningViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_planning)
        setupToolbar()
        setupRecycleView()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.onError().subscribe(this::onError).addTo(subscriptions)
        viewModel.onGetPlanning().subscribe(this::onGetPlanning).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        val flow = PlanningViewModel.PlanningFlow.from(intent.getIntExtra("flow", 1))
        val id = intent.getIntExtra("id", 0)
        viewModel.getPlanning(flow, id).addTo(subscriptions)
    }

    private fun onGetPlanning(list: List<PlanningItemViewModel>) {
        if (list.isEmpty()) {
            ln_header.visibility = View.GONE
            view_line.visibility = View.GONE
            ln_no_data.visibility = View.VISIBLE
        }
        (rv_planning.adapter as PlanningAdapter).addElements(list)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_planning
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.planning_title)
        tv_date.text = viewModel.getTitleDate()
    }

    private fun setupRecycleView() {
        rv_planning.layoutManager = LinearLayoutManager(this)
        rv_planning.itemAnimator = DefaultItemAnimator()
        rv_planning.adapter = PlanningAdapter()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
