package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.PayrollRepository
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface PayrollActivitiesApi {

    @GET("/v2/activity")
    @Headers("Content-Type: application/json")
    fun getActivities(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "crops,unit",
            @Query("stage_activities") stage_activities: Int,
            @Query("activity_area_id") activity_category_id: Int,
            @Query("crop_activity.crop_id") crop_id: Int,
            @Query("__logic") logic: String = "AND",
            @Query("is_active") isEnabled: Boolean = true,
            @Query("order_by") order_by: String = "name.asc"
    ) : Observable<PayrollRepository.ActivitiesReponse>

    @GET("/v2/stage")
    @Headers("Content-Type: application/json")
    fun getStages(
            @Header("Authorization") authentication: String,
            @Query("embed") embed : String,
            @Query("lot_id__eq") lot_id__eq : Int,
            @Query("order_by") order_by : String,
            @Query("limit") limit : Int
    ): Observable<PayrollRepository.StagesReponse>

}
