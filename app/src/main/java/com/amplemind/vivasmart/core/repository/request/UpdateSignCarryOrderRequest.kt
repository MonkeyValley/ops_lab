package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

data class UpdateSignCarryOrderRequest(@SerializedName("sign_collaborator_reception") val sign: String,
                                       @SerializedName("sign_super_reception") val supervisorSign: String)