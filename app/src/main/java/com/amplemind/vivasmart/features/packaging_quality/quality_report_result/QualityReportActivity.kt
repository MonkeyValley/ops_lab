package com.amplemind.vivasmart.features.packaging_quality.quality_report_result

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_quality_report_activity.*
import javax.inject.Inject

/**
 * Created by
 *          amplemind on 7/12/18.
 */

@SuppressLint("Registered")
open class QualityReportActivity : BaseActivity() {


    @Inject
    lateinit var viewModel: QualityReportViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_quality_report_activity)
        viewModel.setupCleanReport(intent.getParcelableExtra("cleanReport"))
        setupToolbar()

        initRecycler()

        loadData()
    }

    private fun loadData() {
        tv_crop_name.text = intent.getStringExtra("crop")
        (rv_result_report.adapter as QualityReportAdapter).addElements(viewModel.getIssues())
        tv_total_size.text = viewModel.getSampleSize()
        tv_total_percent.text = viewModel.getTotalPercent()
        tv_exportation.text = viewModel.getExportation()
        tv_exportation_percent.text = viewModel.getExportationPercent()
        tv_permanent_damage.text = viewModel.getPermanentDamage()
        tv_permanent_damage_percent.text = viewModel.getPermanentDamagePercent()
        tv_defect.text = viewModel.getDefects()
        tv_defect_percent.text = viewModel.getDefectsPercent()
    }

    private fun initRecycler() {
        rv_result_report.hasFixedSize()
        rv_result_report.layoutManager = LinearLayoutManager(this)
        rv_result_report.itemAnimator = DefaultItemAnimator()
        rv_result_report.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        rv_result_report.adapter = QualityReportAdapter()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_report
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = getString(R.string.clean_report_title)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_quality_search_code, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.search_package_code -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}