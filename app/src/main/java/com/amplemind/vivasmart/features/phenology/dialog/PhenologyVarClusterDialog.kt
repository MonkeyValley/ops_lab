package com.amplemind.vivasmart.features.phenology.dialog

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyVarietiesPlantsFragment
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyClusterItem
import com.amplemind.vivasmart.features.phenology.viewmodel.VarMeasurePhenologyItemViewModel
import com.google.gson.Gson

class PhenologyVarClusterDialog : BaseDialogFragment() {

    companion object {
        val TAG = PhenologyVarClusterDialog::class.java.simpleName
        lateinit var item: VarMeasurePhenologyItemViewModel

        fun newInstance(objectCluster: PhenologyClusterItem): PhenologyVarClusterDialog {
            val dialog = PhenologyVarClusterDialog()
            val args = Bundle()
            args.putString("objectCluster", Gson().toJson(objectCluster))
            dialog.arguments = args
            return dialog
        }
    }

    lateinit var mView: View
    lateinit var buttonStatus: EditText
    lateinit var floweringStatus: EditText
    lateinit var cuajeStatus: EditText
    lateinit var clusterNo: EditText
    lateinit var fruitNo: EditText

    var buttonInt: Int = 0
    var floweringInt: Int = 0
    var cuajeInt: Int = 0
    var clusterInt: Int = 0
    var fruitInt: Int = 0

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        mView = LayoutInflater.from(context).inflate(R.layout.phenology_varieties_var_cluster_dialog, null, false)
        builder.setView(mView)

        var objectCluster = Gson().fromJson(arguments!!.getString("objectCluster"), PhenologyClusterItem::class.java)
        setViewElements()
        setListeners()
        setObject(objectCluster)
        builder.setPositiveButton ("Aceptar"){ _, _ ->
            validarDatos()
        }
        builder.setNegativeButton(getString(R.string.cancel)) { _, i -> dismiss() }
        builder.setCancelable(false)
        return builder.create()
    }

    fun setViewElements( ){
        buttonStatus = mView.findViewById(R.id.et_button_status)
        floweringStatus = mView.findViewById(R.id.et_flowering_status)
        cuajeStatus = mView.findViewById(R.id.et_cuaje_status)
        clusterNo = mView.findViewById(R.id.et_cluster_no)
        fruitNo = mView.findViewById(R.id.et_fruit_no)
    }

    fun setObject(data : PhenologyClusterItem){
        buttonStatus.setText(valueOrEmpty(data.button_status))
        floweringStatus.setText(valueOrEmpty(data.flowering_status))
        cuajeStatus.setText(valueOrEmpty(data.cuaje_status))
        clusterNo.setText(valueOrEmpty(data.cluster_no))
        fruitNo.setText(valueOrEmpty(data.fruit_no))
    }

    fun valueOrEmpty(value: Int) : String {
        return if (value == 0)
            ""
        else
            value.toString()
    }

    fun setListeners(){
        buttonStatus.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) { if (p0 != null) { if (p0.isNotBlank()) { buttonInt = Integer.parseInt(p0.toString()) } } }
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                })

        floweringStatus.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) { if (p0 != null) { if (p0.isNotBlank()) { floweringInt = Integer.parseInt(p0.toString()) } } }
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                })

        cuajeStatus.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) { if (p0 != null) { if (p0.isNotBlank()) { cuajeInt = Integer.parseInt(p0.toString()) } } }
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                })

        clusterNo.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) { if (p0 != null) { if (p0.isNotBlank()) { clusterInt = Integer.parseInt(p0.toString()) } } }
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                })

        fruitNo.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) { if (p0 != null) { if (p0.isNotBlank()) { fruitInt = Integer.parseInt(p0.toString()) } } }
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                })

    }

    fun validarDatos(){
        val cluster = PhenologyClusterItem(buttonInt, floweringInt, cuajeInt, clusterInt, fruitInt)
        sendResult(Gson().toJson(cluster))
    }

    private fun sendResult(cluster: String) {
        if (targetFragment == null) {
            return
        }
        val intent: Intent? = PhenologyVarietiesPlantsFragment.newIntent(cluster, "", "")
        targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
        dismiss()
    }
}