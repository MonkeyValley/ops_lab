package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.ProductionQualityRepository
import com.amplemind.vivasmart.core.repository.model.ProductionQualityModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class ProductionQualityViewModel @Inject constructor(private val repository : ProductionQualityRepository){

    private val listData = BehaviorSubject.create<List<ProductionQualityItemViewModel>>()

    fun loadDummyData(): Disposable {
        return repository.getGreenhouses()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it -> mapModelToViewModels(it) }
                .doOnNext {
                    listData.onNext(it)
                }.subscribe()
    }

    fun getList(): BehaviorSubject<List<ProductionQualityItemViewModel>> {
        return listData
    }

    private fun mapModelToViewModels(models: List<ProductionQualityModel>): List<ProductionQualityItemViewModel> {
        val list = arrayListOf<ProductionQualityItemViewModel>()
        for (item in models) {
            list.add(ProductionQualityItemViewModel((item)))
        }
        return list
    }

}