package com.amplemind.vivasmart.core.repository.model

import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel

data class ItemRecibaReportModel(
    val lot: LotModel,
    val carries: Int,
    val boxes: Int,
    val avgWeight: Double
)