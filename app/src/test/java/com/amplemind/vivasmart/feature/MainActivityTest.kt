package com.amplemind.vivasmart.feature

import android.content.Intent
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import android.widget.ImageView
import com.amplemind.vivasmart.AndroidTest
import com.amplemind.vivasmart.MenuActivitiesFragment
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.main.MainActivity
import com.amplemind.vivasmart.features.production_roster.PayrollProductionFragment
import com.amplemind.vivasmart.features.profile.ProfileActivity
import org.junit.Before
import org.junit.Test
import org.robolectric.Robolectric
import org.robolectric.Shadows
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class MainActivityTest : AndroidTest() {

    // TODO Test Labels

    private var activity: MainActivity? = null

    private var navigationView: NavigationView? = null
    private var drawerLayout: DrawerLayout? = null

    @Before
    fun setup() {
        activity = Robolectric.buildActivity(MainActivity::class.java)
                .create()
                .resume()
                .get()

        navigationView = activity?.findViewById(R.id.nav_view)
        drawerLayout = activity?.findViewById(R.id.drawer_layout)
    }

    @Test
    fun `should close drawer if visible on back pressed`() {
        // given
        drawerLayout?.openDrawer(GravityCompat.START)

        // When
        activity?.onBackPressed()

        // Then
        assertFalse(drawerLayout?.isDrawerVisible(GravityCompat.START)!!)
    }

    @Test
    fun `MenuActivitiesFragment must be inflated on shortcuts_menu option clicked`() {
        // navigationView?.menu?.performIdentifierAction(R.id.shortcuts_menu, 0)

        val fragment = activity?.supportFragmentManager?.findFragmentById(R.id.container_main)

        assertTrue(fragment is MenuActivitiesFragment)
        assertFalse(drawerLayout?.isDrawerVisible(GravityCompat.START)!!)
    }

    @Test
    fun `PayrollProductionFragment must be inflated on greenhouse_ship_menu option clicked`() {
        navigationView?.menu?.performIdentifierAction(R.id.greenhouse_ship_menu, 0)

        val fragment = activity?.supportFragmentManager?.findFragmentById(R.id.container_main)

        assertTrue(fragment is PayrollProductionFragment)
        assertFalse(drawerLayout?.isDrawerVisible(GravityCompat.START)!!)
    }

    @Test
    fun `ProfileActivity should be called when user picture is pressed`() {
        // given
        val userPicture = navigationView?.getHeaderView(0)?.findViewById<ImageView>(R.id.im_user_picture)
        val expectedIntent = Intent(activity, ProfileActivity::class.java)

        // when
        userPicture?.performClick()
        val actualIntent = Shadows.shadowOf(activity).peekNextStartedActivity()

        // then
        assertTrue(expectedIntent.filterEquals(actualIntent))
        assertFalse(drawerLayout?.isDrawerVisible(GravityCompat.START)!!)
    }

}