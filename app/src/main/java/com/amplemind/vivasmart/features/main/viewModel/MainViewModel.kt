package com.amplemind.vivasmart.features.main.viewModel

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.features.sync_forced.SyncForcedActivity
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_features.sync.SyncActivity
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import javax.inject.Inject

class MainViewModel @Inject constructor(private val preferences: UserAppPreferences,
                                        private val repository: AccountRepository) {

    private val onSucess: BehaviorSubject<UserModel> = BehaviorSubject.create()
    private val onFail: BehaviorSubject<String> = BehaviorSubject.create()

    fun getUserImage(): String {
        return "${preferences.selectedServerUrl}/uploads/${Gson().fromJson(preferences.userInfo, UserModel::class.java).image}"
    }

    fun getUserEmail(): String {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java).email
    }

    fun getUserName(): String {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java).name
    }

    fun getRoleName(): String {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java).role?.roleName ?: ""
    }

    fun getServerUrl(): String {
        return preferences.selectedServerUrl
    }

    fun putFirebasetoken(firebasetoken: AccountRepository.Firebasetoken, id: Int): Disposable {
        Log.d("tokenUpdate5", firebasetoken.firebasetoken)

        return repository.putFirebaseToken(firebasetoken, id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate {}
                .subscribe({
                    Log.d("tokenUpdate2", it.firebasetoken)
                    onSucess.onNext(it)
                }, {
                    Log.d("tokenUpdate1", it.localizedMessage)
                    onFail.onNext(it.localizedMessage)
                })
    }

    fun observeSuccessPutFirebaseToken(): BehaviorSubject<UserModel> {
        return onSucess
    }

    fun observeFailPutToken(): BehaviorSubject<String> {
        return onFail
    }

    fun validateSync() = preferences.syncStatus != "COMPLETE"

    fun initSync(context: Context) {
        if (preferences.sync.equals("sync")) SyncActivity.startForFirstSync(context)
        else SyncForcedActivity.startForForcedDownload(context)
    }

    fun validateUploadObject(): Boolean =
            Realm.getDefaultInstance().use {
                it.where(UploadObjectModel::class.java)
                        .findAll()
                        .size > 0
            }

}
