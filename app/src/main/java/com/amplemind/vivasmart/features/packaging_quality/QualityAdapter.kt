package com.amplemind.vivasmart.features.packaging_quality

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemPackagingQualityBinding
import com.amplemind.vivasmart.features.packaging_quality.viewModel.CarryOrderCondensedViewModel
import io.reactivex.subjects.PublishSubject

class QualityAdapter : RecyclerView.Adapter<QualityAdapter.QualityViewHolder>() {

    private var list_data = mutableListOf<CarryOrderCondensedViewModel>()

    private val clickSubject = PublishSubject.create<CarryOrderCondensedViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QualityViewHolder {
        val binding = ItemPackagingQualityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return QualityViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list_data.size
    }

    override fun onBindViewHolder(holder: QualityViewHolder, position: Int) {
        holder.bind(list_data[position])
    }

    fun addElements(data : List<CarryOrderCondensedViewModel>){
        list_data = data as MutableList<CarryOrderCondensedViewModel>
        notifyDataSetChanged()
    }

    fun isEmpty() = list_data.isEmpty()

    fun getClickItem() : PublishSubject<CarryOrderCondensedViewModel> {
        return clickSubject
    }

    inner class QualityViewHolder(private val binding: ItemPackagingQualityBinding) : RecyclerView.ViewHolder(binding.root){


        fun bind(item : CarryOrderCondensedViewModel){
            binding.setVariable(BR.data,item)
            binding.executePendingBindings()

            binding.root.setOnClickListener{
                clickSubject.onNext(item)
            }

        }

    }

}
