package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class IssueModel(
        @SerializedName("id") @PrimaryKey val id: Int,
        @SerializedName("is_active") val is_active: Boolean,
        @SerializedName("name") val name: String,
        @SerializedName("image") val image: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("activity_id") var activityId: Int = 0,
        @SerializedName("area_id") var areaId: Int = 0,
        @SerializedName("category_id") var category: Int = 0,
        @SerializedName("sub_category_id") var subCategory: Int = 0,
        @SerializedName("is_condition") var isCondition: Boolean = false,
        var cropId: Int = 0
) {
    //Somehow this didn't work as a constructor's argument. We declare it as a regular field
    @Ignore @SerializedName("sub_category") var subCategoryObj: ProductionCategoriesModel? = null
}