package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm
import io.realm.FieldAttribute

class Version_4 : MigrationExecutable {
    override fun run(realm: DynamicRealm) {
        createVarietyModel(realm)
        createStageVarietyModel(realm)
        createStageVarietyList(realm)
    }

    override fun drop(realm: DynamicRealm) {
        dropVarietyModel(realm)
        dropStageVarietyModel(realm)
        dropStageVarietyList(realm)
    }

    private fun createStageVarietyList(realm: DynamicRealm) {
        realm.schema.get("StageModel")
                ?.addRealmListField("varieties", realm.schema.get("StageVarietyModel")!!)
    }

    private fun createStageVarietyModel(realm: DynamicRealm) {
        realm.schema.create("StageVarietyModel")
                .addField("id", Long::class.java, FieldAttribute.INDEXED)
                .addField("isPriority", Boolean::class.java)
                .addField("sampling", Int::class.java)
                .addField("stageId", Long::class.java, FieldAttribute.INDEXED)
                .addField("tableNo", Int::class.java)
                .addField("varietyId", Long::class.java, FieldAttribute.INDEXED)
                .addRealmObjectField("variety", realm.schema.get("VarietyModel")!!)
    }

    private fun createVarietyModel(realm: DynamicRealm) {
        realm.schema.create("VarietyModel")
                .addField("id", Long::class.java, FieldAttribute.INDEXED)
                .addField("cropId", Long::class.java, FieldAttribute.INDEXED)
                .addField("graftName", String::class.java, FieldAttribute.REQUIRED)
                .addField("isCommercial", Boolean::class.java, FieldAttribute.REQUIRED)
                .addField("isGraft", Boolean::class.java, FieldAttribute.REQUIRED)
                .addField("supplier", String::class.java, FieldAttribute.REQUIRED)
                .addField("name", String::class.java, FieldAttribute.REQUIRED)
                .addField("uuid", String::class.java, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
                .addField("version", Long::class.java, FieldAttribute.INDEXED)
                .setRequired("version", false)
    }

    private fun dropStageVarietyModel(realm: DynamicRealm) {
        realm.schema.remove("StageVarietyModel")
    }

    private fun dropVarietyModel(realm: DynamicRealm) {
        realm.schema.remove("VarietyModel")
    }

    private fun dropStageVarietyList(realm: DynamicRealm) {
        realm.schema.get("StageModel")
                ?.removeField("varieties")
    }
}