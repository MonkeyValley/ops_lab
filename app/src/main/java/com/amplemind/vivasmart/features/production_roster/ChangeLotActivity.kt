package com.amplemind.vivasmart.features.production_roster

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.snack
import com.amplemind.vivasmart.core.extensions.snackWithoutInternetMessage
import com.amplemind.vivasmart.features.production_lines.viewModel.ItemLotPackageViewModel
import com.amplemind.vivasmart.features.production_lines.viewModel.LotsInLinesViewModel
import kotlinx.android.synthetic.main.content_change_lot_activity.*
import javax.inject.Inject

class ChangeLotActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: LotsInLinesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_change_lot_activity)

        Log.e("ChangeLotActivity", "......")

        tv_name_line.text = getNameLine()


        setupRecycler()

        viewModel.onLoadLocalData().subscribe(this::loadLotsOffline).addTo(subscriptions)
        viewModel.onSucessChangeLot().subscribe(this::onSuccessChangeLot).addTo(subscriptions)
        viewModel.getLots().subscribe(this::setData).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onErrorSelected().subscribe(this::onErrorSelected).addTo(subscriptions)

        loadLots()

        setupControls()

    }

    private fun loadLots() {
        if (true) {
            viewModel.loadLots().addTo(subscriptions)
        } else {
            loadLotsOffline(true)
        }
    }

    private fun loadLotsOffline(show : Boolean){
        if (show){
            view_main.snackWithoutInternetMessage()
            viewModel.getLocalLots().addTo(subscriptions)
        }
    }

    private fun setupControls() {
        btn_cancel.setOnClickListener {
            finish()
        }
        btn_acept.setOnClickListener {
            viewModel.changeLot(true)?.addTo(subscriptions)
        }
    }

    private fun onSuccessChangeLot(result: Boolean) {
        finish()
    }

    private fun onErrorSelected(error : String){
        view_main.snack(error)
    }

    private fun getNameLine(): String {
        return intent.getStringExtra("name_line") ?: ""
    }

    private fun getLotId(): Int {
        return intent.getIntExtra("lot_id", -1)
    }

    private fun setupRecycler() {
        rv_change_lot.hasFixedSize()
        rv_change_lot.layoutManager = LinearLayoutManager(this)
        rv_change_lot.itemAnimator = DefaultItemAnimator()
        rv_change_lot.adapter = ChangeLotAdapter()
    }


    fun setData(data: List<ItemLotPackageViewModel>) {
        (rv_change_lot.adapter as ChangeLotAdapter).addElements(data)
    }


}
