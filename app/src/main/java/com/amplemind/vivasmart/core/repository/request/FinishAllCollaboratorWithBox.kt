package com.amplemind.vivasmart.core.repository.request

import com.amplemind.vivasmart.core.repository.model.CountBoxModel
import com.google.gson.annotations.SerializedName

data class FinishAllCollaboratorWithBox(
        @SerializedName("packingline_collaborator") val packingline_collaborator : Int = 0,
        @SerializedName("packingline_id") val packingline_id : Int,
        @SerializedName("batch_collaborator") val collaborators: List<FinishCollaboratorSignature>? = null,
        @SerializedName("presentations") val presentation : List<CountBoxModel>?,
        @SerializedName("times") val times : HashMap<Int,Long>?,
        @SerializedName("sign") val sign : String? = null,
        @SerializedName("is_practice") val isPractice : Boolean,
        @SerializedName("is_training") val training : Boolean = false,
        @SerializedName("temp_time") val time: Long? = null
)
