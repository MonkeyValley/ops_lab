package com.amplemind.vivasmart.features.finished_product_quality.fragment


import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.repository.model.FinishedProductReportModel
import com.amplemind.vivasmart.core.repository.model.ImagesModel
import com.amplemind.vivasmart.core.repository.model.IssueModel
import com.amplemind.vivasmart.core.repository.model.IssuesModel
import com.amplemind.vivasmart.features.finished_product_quality.FinishedProductQualityActivity.Companion.sharedPref
import com.amplemind.vivasmart.features.finished_product_quality.adapter.ProductQualityIssuesAdapter
import com.amplemind.vivasmart.features.finished_product_quality.viewModel.ProductQualityReviewDetailViewModel
import com.amplemind.vivasmart.features.gallery.GalleryDialog
import com.amplemind.vivasmart.features.production_quality.ItemReportQualityReviewDetailViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.dialog_image_finished_product.view.*
import kotlinx.android.synthetic.main.fragment_product_quality_review_detail.*
import me.grantland.widget.AutofitTextView
import java.lang.Math.round
import java.math.RoundingMode
import javax.inject.Inject


class ProductQualityReviewDetailFragment : BaseFragment(), HasSupportFragmentInjector {
    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>
    lateinit var progressDialogInner: ProgressDialog

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return childFragmentInjector
    }

    companion object {
        const val LIST_IMG_URL_JSON = "listImgUrlJSON"
        const val STATUS = "status"
        lateinit var totalExportacion: TextView
        lateinit var percentExportacion: TextView
        lateinit var totalDamaged: TextView
        lateinit var percentDamaged: TextView
        lateinit var totalSample: TextView
        lateinit var totalBrix: TextView
        var newReport: Boolean = false
        var idReport: String = ""
        var lotName: String = ""
        var packingName: String = ""

        fun newIntent(listImgUrlJSON: String?, status: Int): Intent? {
            val intent = Intent()
            intent.putExtra(STATUS, status)
            intent.putExtra(LIST_IMG_URL_JSON, listImgUrlJSON)
            return intent
        }

        fun ifConected(): Boolean{
            return if(ProductQualityReviewDetailFragment().hasInternet())
                true
            else {
                Toast.makeText( ProductQualityReviewDetailFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

    @Inject
    lateinit var viewModel: ProductQualityReviewDetailViewModel
    var ObjFinishedProdReport: FinishedProductReportModel? = null
    lateinit var listaIssues: List<ItemReportQualityReviewDetailViewModel>
    lateinit var btnSend: Button
    lateinit var prodNameLayout: LinearLayout
    lateinit var headerFragment: LinearLayout
    lateinit var linearBrix: LinearLayout
    lateinit var idReportLabel: AutofitTextView
    lateinit var lotNameLabel: AutofitTextView
    lateinit var cropNameLabel: AutofitTextView
    lateinit var packingNameLabel: AutofitTextView
    lateinit var tvStatus: AutofitTextView
    lateinit var llStatus: LinearLayout

    fun newInstance(title: String, newReport: Boolean, idReport: String, lotName:String, packingName:String  ): ProductQualityReviewDetailFragment {
        val args = Bundle()
        args.putString("Title", title)
        args.putBoolean("newReport", newReport)
        args.putString("idReport", idReport)
        args.putString("lotName", lotName)
        args.putString("packingName", packingName)

        val fragment = ProductQualityReviewDetailFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_product_quality_review_detail, container, false)
        addSubscribers()
        ObjFinishedProdReport = sharedPref.getSavedObjectFromPreference("finishedProductObj", FinishedProductReportModel::class.java)
        newReport = arguments!!.getBoolean("newReport")
        idReport = arguments!!.getString("idReport", "0")
        lotName = arguments!!.getString("lotName", "0")
        packingName = arguments!!.getString("packingName", "0")

        if (newReport) {
            setDialogLoadingInner()
            setViewElements(view)
            setListeners()
            getInfoIssues()
        } else {
            setDialogLoadingInner()
            setViewElements(view)
            getInfoReport(idReport, lotName, packingName)
        }

        return view
    }

    fun setViewElements(view: View) {
        btnSend = view.findViewById(R.id.btn_send)
        totalExportacion = view.findViewById(R.id.total_exportacion)
        percentExportacion = view.findViewById(R.id.percent_exportacion)
        totalDamaged = view.findViewById(R.id.total_damaged)
        percentDamaged = view.findViewById(R.id.percent_damaged)
        totalSample = view.findViewById(R.id.total_sample)

        prodNameLayout = view.findViewById(R.id.LL_header_detail)
        idReportLabel = view.findViewById(R.id.TV_id)
        lotNameLabel= view.findViewById(R.id.TV_lot)
        cropNameLabel = view.findViewById(R.id.TV_crop)
        packingNameLabel = view.findViewById(R.id.TV_packing)
        llStatus = view.findViewById(R.id.ll_status)
        tvStatus = view.findViewById(R.id.TV_status)
        headerFragment = view.findViewById(R.id.LL_header)

        linearBrix = view.findViewById(R.id.LL_brix)
        totalBrix = view.findViewById(R.id.tv_brix)
    }

    fun setListeners() {
        btnSend.setOnClickListener { v: View -> onClickEvidenceAndSign(v) }
        totalExportacion.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    if (p0.isNotBlank()) {
                        var totalExpo: Int = Integer.parseInt(totalExportacion.text.toString())
                        var totalSampInt: Int = Integer.parseInt(totalSample.text.toString())

                        if (totalExpo <= totalSampInt) {
                            // Calculo de porcentajes y cantidades
                            var porcentajeExpo = ((totalExpo * 100.00) / totalSampInt)
                            var porTransf =  porcentajeExpo.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toString() + "%"
                            percentExportacion.text = porTransf

                            var totalDamageInt = (totalSampInt - totalExpo)
                            var porcentajeDamage = ((totalDamageInt * 100.00) / totalSampInt)
                            var porDamg =  porcentajeDamage.toBigDecimal().setScale(2,  RoundingMode.HALF_EVEN).toString() + "%"

                            totalDamaged.text = totalDamageInt.toString()
                            percentDamaged.text = porDamg
                            btnSend!!.visibility = View.VISIBLE
                        } else {
                            Toast.makeText(context, "La cantidad de exportacdión debe ser menor a la cantidad de la muestra", Toast.LENGTH_SHORT).show()
                            percentExportacion.text = "0.00%"
                            totalDamaged.text = "0"
                            percentDamaged.text = "0.0%"
                            btnSend.visibility = View.GONE
                        }
                    } else {
                        btnSend.visibility = View.GONE
                    }

                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    fun setDialogLoadingInner() {
        progressDialogInner = ProgressDialog(context)
        progressDialogInner.setTitle(getString(R.string.app_name))
        progressDialogInner.setMessage(getString(R.string.server_loading))
        progressDialogInner.setCancelable(false)
    }

    fun showProgressDialogInner(show: Boolean) {
        if (show) progressDialogInner.show() else progressDialogInner.dismiss()
    }

    fun getInfoIssues() {
        if (ObjFinishedProdReport != null) {
            totalSample.text = ObjFinishedProdReport!!.sample.toString()
            getListIssues(ObjFinishedProdReport!!.crop_id)
        }
    }

    fun getInfoReport(idReport: String, lotName:String, packingName:String ) {
        showProgressDialogInner(true)
        headerFragment.visibility = View.GONE
        prodNameLayout.visibility = View.VISIBLE

        idReportLabel.text = idReport
        lotNameLabel.text = lotName
        packingNameLabel.text = packingName

        viewModel.getReportDetail(idReport.toInt())
        totalExportacion.isEnable(false)
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_issues().subscribe(this::updateList_issues).addTo(subscriptions)
        viewModel.onSuccessRequest_report().subscribe(this::returnToMain).addTo(subscriptions)
        viewModel.onSuccessGet_report().subscribe(this::setDetailElements).addTo(subscriptions)

    }

    fun onClickEvidenceAndSign(view: View) {
        val dialog = GalleryDialog().newInstance(totalDamaged.text.toString().toDouble(), 1)
        dialog.setTargetFragment(this, 100)
        dialog.show(fragmentManager!!, GalleryDialog.TAG)
    }

    fun getListIssues(id_crop: Int) {
        if(ifConected()) {
            viewModel.getIssuesList(id_crop)
        }
    }

    private fun updateList_issues(list: List<ItemReportQualityReviewDetailViewModel>) {
        val adapter = ProductQualityIssuesAdapter(list.toMutableList(), ObjFinishedProdReport!!, newReport)
        rv_quality_issues.hasFixedSize()
        rv_quality_issues.layoutManager = LinearLayoutManager(context)
        rv_quality_issues.itemAnimator = DefaultItemAnimator()
        rv_quality_issues.adapter = adapter
        adapter.onClick().subscribe(this::loadModalImages).addTo(subscriptions)
        listaIssues = list
    }

    fun loadModalImages(unit: ItemReportQualityReviewDetailViewModel) {
        Log.d("imagen", unit.image.toString())
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.dialog_image_finished_product, null, false)
        builder.setCancelable(true)
        builder.setView(mView)
        mView.TV_issue_name.text = unit.name
        if (unit.image != null) {
//        https://dev.vivasmart.com.mx/v2/files/
//        type= "image"
//        name = issue/bab2251e-3f89-11e9-a1ec-0242ac130003.jpg

            mView.TV_issue_empty_name.visibility = View.GONE
            mView.IV_issue_image.visibility = View.VISIBLE

            var urlImg = unit.image
            Glide.with(this)
                    .load(urlImg)
                    .apply(RequestOptions().placeholder(R.drawable.ic_star_menu))
                    .into(mView.IV_issue_image)

        } else {
            mView.TV_issue_empty_name.visibility = View.VISIBLE
            mView.IV_issue_image.visibility = View.GONE
        }
        builder.show()
    }

    private fun setDetailElements(item: FinishedProductReportModel) {
        ObjFinishedProdReport = item
        var cropObj = viewModel.getCrop(item.crop_id)
        cropNameLabel.text = "${(cropObj.name)}"

        if(item.status != null) {
            tvStatus.text = when(item.status){
                1 -> "Retenida"
                2 -> "Liberada"
                else -> ""
            }
        } else llStatus.visibility = View.GONE

        if(item.brix != null){
            linearBrix.visibility = View.VISIBLE
            totalBrix.text = item.brix.toString()
        }

        totalExportacion.text = item.export_no.toString()
        val percentExpo: Double = ((item.export_no!! * 100.00) / item.sample)
        var porTransf =  percentExpo.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toString() + "%"
        percentExportacion.text = porTransf

        totalDamaged.text = item.damaged_no.toString()
        val percentDamage: Double = ((item.damaged_no!! * 100.00) / item.sample)
        var porDamg =  percentDamage.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toString() + "%"
        percentDamaged.text = porDamg

        totalSample.text = item.sample.toString()
        prodNameLayout.visibility = View.VISIBLE

        val listaIssues = ArrayList<ItemReportQualityReviewDetailViewModel>()
        val listIssuesFilter = item.issues!!.filter { it.frequency_no > 0 }
        for (itemIssues in listIssuesFilter) {
            val issueModel = IssueModel(itemIssues.issue.id, itemIssues.issue.is_active, itemIssues.issue.name, itemIssues.issue.description, itemIssues.issue.image)
            val percent: Double = round((itemIssues.frequency_no * 100.00 / item.sample)).toDouble()
            val reportQualityDetail = ItemReportQualityReviewDetailViewModel(issueModel, itemIssues.frequency_no.toString(), percent.toString())
            listaIssues.add(reportQualityDetail)
        }

        val adapter = ProductQualityIssuesAdapter(listaIssues, ObjFinishedProdReport!!, newReport)
        rv_quality_issues.hasFixedSize()
        rv_quality_issues.layoutManager = LinearLayoutManager(context)
        rv_quality_issues.itemAnimator = DefaultItemAnimator()
        rv_quality_issues.adapter = adapter

        showProgressDialogInner(false)
    }

    fun prepareData(listUrls: ArrayList<String>, status: Int) {
        var exportNo = Integer.parseInt(totalExportacion.text.toString())
        var damagedNo = Integer.parseInt(totalDamaged.text.toString())
        var listIssues = ArrayList<IssuesModel>()
        var listImages = ArrayList<ImagesModel>()
        val issuMod = IssueModel(0, false, "", "", "")

        // el primer elemento del array es la firma
        var sign: String = listUrls[0]

        for (itemUrl in listUrls) {
            val imageModel = ImagesModel(itemUrl)
            listImages.add(imageModel)
        }

        for (issue in listaIssues) {
            val issueModel = IssuesModel(issue.id, Integer.parseInt(issue.frequency_no), issuMod)
            listIssues.add(issueModel)
        }

        ObjFinishedProdReport?.export_no = exportNo
        ObjFinishedProdReport?.damaged_no = damagedNo
        ObjFinishedProdReport?.sign = sign
        ObjFinishedProdReport?.issues = listIssues
        ObjFinishedProdReport?.images = listImages
        ObjFinishedProdReport?.status = status

        Log.d("ObjFinishedProdReport", Gson().toJson(ObjFinishedProdReport))
        sendDataReport()
    }

    fun sendDataReport() {
        if(ifConected()) {
            viewModel.postFinishedProduct(ObjFinishedProdReport!!)
        }
    }

    fun returnToMain(finishedProduct: FinishedProductReportModel) {
        Log.d("finalString", Gson().toJson(finishedProduct))
        var intent = Intent()
        intent.putExtra("finishedProduct", Gson().toJson(finishedProduct))
        activity!!.setResult(Activity.RESULT_OK, intent)
        activity!!.finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == 100) {
            val status = data!!.getIntExtra(STATUS, 0)
            val jsonList = data!!.getStringExtra(LIST_IMG_URL_JSON)
            val listUrls: ArrayList<String> = Gson().fromJson(jsonList, object : TypeToken<ArrayList<String?>?>() {}.getType())
            prepareData(listUrls, status)
        }
    }
}
