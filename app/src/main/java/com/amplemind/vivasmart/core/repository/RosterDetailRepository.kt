package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.extensions.dummyRosterDetail
import com.amplemind.vivasmart.core.repository.model.RosterDetailModelTemp
import io.reactivex.Observable
import javax.inject.Inject

class RosterDetailRepository @Inject constructor(){

    fun getDetailRoster(): Observable<List<RosterDetailModelTemp>> {
        return Observable.just(dummyRosterDetail())
    }


}