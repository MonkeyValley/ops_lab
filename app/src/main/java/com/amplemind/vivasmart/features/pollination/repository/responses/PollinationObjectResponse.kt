package com.amplemind.vivasmart.features.pollination.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.PollinationModel
import com.google.gson.annotations.SerializedName


data class PollinationObjectResponse<T: Any> (
        @SerializedName("weeks")        val data: List<T>,
        @SerializedName("item_count")   val itemCount: Int
)

data class Pollinationrevisions(
        @SerializedName("revisions")         val revision: List<PollinationModel>
)