package com.amplemind.vivasmart.core.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences


fun Context.isOnlineMode(context: Context): Boolean {
    //var online = true
    //if (!BuildConfig.FLAVOR.equals("local")) {
        val preferences = UserAppPreferences(context, null, null)
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val online = activeNetwork?.isConnectedOrConnecting == true && preferences.isOnline
        preferences.isOnline = online
    //}
    return online
}


fun Context.hasInternet(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
    return activeNetwork != null
}


