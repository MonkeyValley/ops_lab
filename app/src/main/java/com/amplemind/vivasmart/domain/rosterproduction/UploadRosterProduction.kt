package com.amplemind.vivasmart.domain.rosterproduction

import android.util.Log
import android.util.Log.e
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.platform.SyncNotification
import com.amplemind.vivasmart.core.platform.SynchronizeDataBaseService
import com.amplemind.vivasmart.core.repository.CollaboratorsRepository
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.local.ActivitiesDao
import com.amplemind.vivasmart.core.repository.local.CollaboratorsDao
import com.amplemind.vivasmart.core.repository.local.GroovesDao
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.GrooveSyncModel
import com.amplemind.vivasmart.core.repository.model.SyncReportModel
import com.amplemind.vivasmart.core.repository.model.SyncRosterProductionModel
import com.amplemind.vivasmart.core.repository.model.SyncRosterProductionRequest
import com.amplemind.vivasmart.core.repository.request.DeleteCollaborator
import com.amplemind.vivasmart.core.repository.request.SyncDeleteCollaborators
import com.amplemind.vivasmart.core.utils.AndroidDisposable
import com.amplemind.vivasmart.core.utils.PicturesHelper
import org.jetbrains.anko.doAsync
import java.io.File
import javax.inject.Inject

class UploadRosterProduction @Inject constructor(private val database: CollaboratorsDao,
                                                 private val databaseActivities: ActivitiesDao,
                                                 private val databaseGroove: GroovesDao,
                                                 private val uploadRepository: ProfileRepository,
                                                 private val collaboratorRepository: CollaboratorsRepository,
                                                 private val preferences: UserAppPreferences,
                                                 private val picturesHelper: PicturesHelper) {

    private var delegate: SyncNotification? = null
    private val type = SynchronizeDataBaseService.SynchronizeRequest.ROSTER_PRODUCTION
    private var subscriptions: AndroidDisposable? = null

    fun collaboratorsReadyToSync(delegate: SyncNotification, subscriptions: AndroidDisposable) {
        this.delegate = delegate
        this.subscriptions = subscriptions

        val collaborators = mutableListOf<SyncRosterProductionModel>()
        doAsync({
            e("Error", "CollaboratorsReadyToSync : ${it.message}")
            delegate.onTerminate(mutableListOf(), type,"CollaboratorsReadyToSync : ${it.message}")
        }, {
            database.getCollaboratorsReadyToSync().map { log ->
                val collaborator = database.getCollaboratorByIdAndCode(log.collaboratorId!!, log.activityCodeId!!, true)
                if (collaborator != null) {
                    val grooves = databaseGroove.getGrooveByActivityLog(log.activityLogIdLocalDB)
                    val syncGrooves = grooves.map { GrooveSyncModel(it.groove_no, it.table_no) }
                    val code = databaseActivities.getActivityCodeById(log.activityCodeId.toLong())
                    collaborators.add(SyncRosterProductionModel(collaborator.collaborator.employeeCode, log.activity_code!!.activityId!!,
                            log.activity_code.stageId!!, log.quantity!!.toInt(), log.sign, log.time!!, syncGrooves, log.isTraining ?: false,log.isPractice ?: false,
                            code.code, log.id!!, true, collaborator.isDone))
                }
            }
            // Add Collaborators Running
            database.getCollaboratorsList().map { collaborator ->

                val units = databaseGroove.getActivityLogUnits(collaborator.collaborator.id ?: 0,collaborator.activityCodeId)

                val grooves = databaseGroove.getGrooveByActivityAndIdCollaborator(collaborator.collaborator.id ?: 0,collaborator.activityCodeId)
                val syncGrooves = grooves?.map { GrooveSyncModel(it.groove_no, it.table_no) } ?: listOf()

                if (collaborators.none { it.employeeCode == collaborator.collaborator.employeeCode } && collaborator.stageId != 0
                        && collaborator.activityId != 0) {
                    val time = if (collaborator.date > 0) {
                        ((System.currentTimeMillis() - collaborator.date) / 1000).toDouble()
                    } else collaborator.timerCount
                    collaborators.add(SyncRosterProductionModel(collaborator.collaborator.employeeCode, collaborator.activityId,
                            collaborator.stageId, units?.quantity ?: 0, "No Sign", time.toInt(), syncGrooves, false, false,
                            collaborator.activityCode, collaborator.id ?: 0, status = true, isDone = false, stillRunning = true))
                }
            }
            uploadImage(collaborators, 0)
        })
    }

    private fun deleteSyncCollaborators(collaborators: List<SyncRosterProductionModel>) {
        doAsync({
            e("Error", "deleteSyncCollaborators : ${it.message}")
            delegate?.onTerminate(mutableListOf(), type,"deleteSyncCollaborators : ${it.message}")
        }, {
            collaborators.map {
                if (it.stillRunning) {
                    database.deleteCollaborator(it.offlineId.toLong())
                } else {
                    val log = database.getCollaboratorsActivityLog(it.offlineId)
                    val collaborator = database.getCollaboratorByIdAndCode(log.collaboratorId!!, log.activityCodeId!!, true)
                    database.deleteCollaborator(collaborator?.id?.toLong() ?: 0)
                    database.deleteActivityLog(it.offlineId)
                }
            }
        })
    }

    private fun uploadImage(collaborators: List<SyncRosterProductionModel>, position: Int) {
        if (position < collaborators.size) {
            if (collaborators[position].sign == null || !File(collaborators[position].sign).exists()) {
                uploadImage(collaborators, position + 1)
                return
            }
            val file = File(collaborators[position].sign)
            picturesHelper.compressImage(file, file, 400f, 400f)
            uploadRepository.uploadImage(file, "user")
                    .subscribe({
                        collaborators[position].sign = it.relative_url
                        uploadImage(collaborators, position + 1)
                    }, {
                        Log.e("Fail upload", "Error ${it.localizedMessage}")
                        uploadImage(collaborators, position + 1)
                    }).addTo(subscriptions!!)
        } else {
            deleteCollaboratorsOnline(collaborators)
        }
    }

    /**
     *  Send request to remove collaborators that are assigned online
     *  but offline were eliminated
     */
    private fun deleteCollaboratorsOnline(collaborators: List<SyncRosterProductionModel>) {
        val list = mutableListOf<DeleteCollaborator>()
        database.getCollaboratorsByDeleteSync().map { collaborator ->
            if (collaborator.activityLog != null) {
                list.add(DeleteCollaborator(collaborator.activityLog, collaborator.idRegister))
            }
        }
        collaboratorRepository.syncRosterCollaboratorsDeleted(preferences.token, SyncDeleteCollaborators(list))
                .subscribe({
                    list.map { database.deleteByDeleteSync(it.collaboratorId) }
                    syncCollaborators(collaborators)
                }, {
                    //finishWithErrors(collaborators)
                    syncCollaborators(collaborators)
                    Log.e("Fail upload", "Error ${it.message}")
                }).addTo(subscriptions!!)
    }

    private fun syncCollaborators(collaborators: List<SyncRosterProductionModel>) {
        val request = SyncRosterProductionRequest(collaborators)
        val report = mutableListOf<SyncReportModel>()
        collaboratorRepository.syncRosterCollaborators(preferences.token, request)
                .subscribe({ syncCollaborators ->
                    syncCollaborators.data.forEach {
                        val total = if (it.grooves.isEmpty()) 1 else it.grooves.size
                        val complete = if (it.status) total else 0
                        report.add(SyncReportModel(it.employeeCode, total, complete, it.offlineId))
                    }
                    delegate?.onTerminate(report, type)
                    deleteSyncCollaborators(collaborators)
                }, {
                    Log.e("Fail upload", "Error ${it.message}")
                    finishWithErrors(collaborators)
                }).addTo(subscriptions!!)
    }

    private fun finishWithErrors(collaborators: List<SyncRosterProductionModel>) {
        val reports = collaborators.map {
            database.updateActivityLogError(it.offlineId)
            SyncReportModel(it.employeeCode, if (it.grooves.isEmpty()) 1 else it.grooves.size, 0, it.offlineId)
        }
        delegate?.onTerminate(reports, type)
    }

}