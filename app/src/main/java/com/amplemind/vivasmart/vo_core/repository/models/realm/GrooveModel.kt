package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class GrooveModel (


        @SerializedName("id")                   var id: Int = 0,
        @Index
        @SerializedName("activity_log_id")      var activityLogId: Long = 0,
        @Index
        @SerializedName("activity_code_id")     var activityCodeId: Long = 0,
        @Index
        @SerializedName("groove_no")            var grooveNo: Int = 0,
        @Index
        @SerializedName("table_no")             var tableNo: Int = 0,

        @SerializedName("quality_comment")      var qualityComment: String? = "None",
        @SerializedName("quality_control")      var qualityControl: Boolean = false,

        @PrimaryKey
        @SerializedName("uuid")                 override var uuid: String = getUUID(),

        @Index
        @SerializedName("activity_log_uuid")     var activityLogUuid: String = "",

        //Local fields
        @Index var activityCodeUuid: String = "",

        var activityLog: ActivityLogModel? = null,
        var activityCode: ActivityCodeModel? = null

): RealmObject(), Identifiable
