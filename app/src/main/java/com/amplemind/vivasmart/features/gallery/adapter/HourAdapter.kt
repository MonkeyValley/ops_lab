package com.amplemind.vivasmart.features.gallery.adapter

import android.app.TimePickerDialog
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.databinding.ItemHourBinding
import com.amplemind.vivasmart.features.fertiriego.viewModel.HourModel
import io.reactivex.subjects.PublishSubject
import java.util.*

class HourAdapter : (RecyclerView.Adapter<HourAdapter.HourViewHolder>)() {

    var copy = false
    var lotId = 0

    private var list = listOf<HourModel>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    fun addElements(data: List<HourModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourViewHolder {
        val binding = ItemHourBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HourViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HourViewHolder, position: Int) {
        holder.bind(list[position])

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class HourViewHolder(private val binding: ItemHourBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: HourModel

        fun bind(item: HourModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.tvPulseName.text = "Pulso " + item.pulse
            if(item.hour != "")binding.etHour.setText(item.hour)

            binding.etHour.setOnClickListener {
                val c = Calendar.getInstance()
                val hour = c.get(Calendar.HOUR_OF_DAY)
                val minute = c.get(Calendar.MINUTE)

                TimePickerDialog(binding.root.context, TimePickerDialog.OnTimeSetListener(function = { view, hour, minute ->

                    var hour = hour
                    var am_pm = ""
                    when {
                        hour == 0 -> {
                            hour += 12
                            am_pm = "am"
                        }
                        hour == 12 -> am_pm = "pm"
                        hour > 12 -> {
                            hour -= 12
                            am_pm = "pm"
                        }
                        else -> am_pm = "am"
                    }

                    val timeHour = if (hour < 10) "0" + hour else hour
                    val tiemMin = if (minute < 10) "0" + minute else minute
                    var hourSelect = "$timeHour:$tiemMin$am_pm"
                    binding.etHour.setText(hourSelect)
                    item.hour = hourSelect

                }), hour, minute, false).show()
            }

        }
    }
}