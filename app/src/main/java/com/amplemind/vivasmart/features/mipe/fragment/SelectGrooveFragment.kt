package com.amplemind.vivasmart.features.mipe.fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.*
import com.amplemind.vivasmart.features.mipe.MipeSelectPlantActivity
import com.amplemind.vivasmart.features.mipe.adapter.GrooveMipeAdapter
import com.amplemind.vivasmart.features.mipe.viewModel.MipeGrooveViewModel
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SelectGrooveFragment : BaseFragment(), GrooveMipeAdapter.Listener, View.OnClickListener {

    fun newInstance(week: Int, lotId: Int, table: String, lotName: String): SelectGrooveFragment {
        val args = Bundle()
        args.putInt("WEEK", week)
        args.putInt("LOT_ID", lotId)
        args.putString("TABLE", table)
        args.putString("LOT_NAME", lotName)

        val fragment = SelectGrooveFragment()
        fragment.arguments = args
        return fragment
    }

    @Inject
    lateinit var viewModel: MipeGrooveViewModel

    private var mListener: GrooveMipeAdapter.Listener? = null

    private var pairAdapter = GrooveMipeAdapter()
    private var oddAdapter = GrooveMipeAdapter()

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    var btnPair: Button? = null
    var btnOdd: Button? = null
    var rvPair: RecyclerView? = null
    var rvOdd: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_select_groove, container, false)

        argument()
        setUpUi(view)
        setupRecycler()
        setUpSubcribers()

        return view
    }

    fun setUpUi(view: View) {
        btnPair = view!!.findViewById(R.id.btn_pair)
        btnPair!!.setOnClickListener(this)
        btnOdd = view!!.findViewById(R.id.btn_odd)
        btnOdd!!.setOnClickListener(this)
        rvPair = view!!.findViewById(R.id.rv_pair)
        rvOdd = view!!.findViewById(R.id.rv_odd)
    }

    private fun argument() {
        viewModel.lotId = arguments!!.getInt("LOT_ID") ?: 0
        viewModel.week = arguments!!.getInt("WEEK") ?: 0
        viewModel.table = arguments!!.getString("TABLE") ?: ""
        viewModel.lotName = arguments!!.getString("LOT_NAME") ?: ""

        pairAdapter.setData(viewModel.lotId, viewModel.table, viewModel.week)
        oddAdapter.setData(viewModel.lotId, viewModel.table, viewModel.week)
    }

    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.btn_pair -> {
                rvPair!!.visibility = View.VISIBLE
                rvOdd!!.visibility = View.GONE
//                btnPair!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
//                btnOdd!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
                btnPair!!.setBackgroundResource(R.drawable.corner_left_radius)
                btnOdd!!.setBackgroundResource(R.drawable.corner_right_radius_disable)
            }
            R.id.btn_odd -> {
                rvPair!!.visibility = View.GONE
                rvOdd!!.visibility = View.VISIBLE
//                btnPair!!.setBackgroundColor(Color.parseColor("#E3E3E3"))
//                btnOdd!!.setBackgroundColor(Color.parseColor("#9B9B9B"))
                btnPair!!.setBackgroundResource(R.drawable.corner_left_radius_disable)
                btnOdd!!.setBackgroundResource(R.drawable.corner_right_radius)
            }
        }
    }

    fun setUpSubcribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getDataPairGroove()
        viewModel.getDataOddGroove()
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun getData() {
        addElementsPair(viewModel.getDataPairGroove())
        addElementsOdd(viewModel.getDataOddGroove())
    }

    fun addElementsPair(data: ArrayList<Int>) {
        pairAdapter.addElements(data)
    }

    fun addElementsOdd(data: ArrayList<Int>) {
        oddAdapter.addElements(data)
    }

    private fun setupRecycler() {
        rvPair!!.hasFixedSize()
        rvPair!!.layoutManager = GridLayoutManager(activity, getSpanCount3(activity!!.applicationContext))
        rvPair!!.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

        pairAdapter.listener = this
        rvPair!!.adapter = pairAdapter
        clickItemPair()

        rvOdd!!.hasFixedSize()
        rvOdd!!.layoutManager = GridLayoutManager(activity, getSpanCount3(activity!!.applicationContext))
        rvOdd!!.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

        oddAdapter.listener = this
        rvOdd!!.adapter = oddAdapter
        clickItemOdd()
    }

    private fun clickItemPair() {
        pairAdapter.onClickItem()
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe(this::showGrooveDetail).addTo(subscriptions)
    }

    private fun clickItemOdd() {
        oddAdapter.onClickItem()
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe(this::showGrooveDetail).addTo(subscriptions)
    }

    private fun showGrooveDetail(groove: Int) {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now
        val intent = Intent(activity, MipeSelectPlantActivity::class.java).apply {
            putExtra(MipeSelectPlantActivity.LOT_ID, viewModel.lotId)
            putExtra(MipeSelectPlantActivity.GROOVE, groove)
            putExtra(MipeSelectPlantActivity.WEEK, viewModel.week)
            putExtra(MipeSelectPlantActivity.TABLE, viewModel.table)
            putExtra(MipeSelectPlantActivity.TAG_NAME, "" + viewModel.lotName + " - Monitoreo")
        }
        startActivity(intent)
    }

    override fun onGetCounterValue(position: Int): Observable<Int>? {
        return mListener?.onGetCounterValue(position)
    }

}
