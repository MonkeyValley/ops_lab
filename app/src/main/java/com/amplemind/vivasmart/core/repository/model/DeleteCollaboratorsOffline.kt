package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DeleteCollaboratorsOffline (
        @PrimaryKey(autoGenerate = true) val id : Int? = null,
        val idRegister : Int,
        val activityLog: Int? = null
)