package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

class UpdateCarryOrderRequest(@SerializedName("box_no") val boxNo: Double,
                              @SerializedName("variety_box") val boxCounts: List<VarietyBoxCount>,
                              @SerializedName("second_quality_box") val secondQualiltyBox: Double,
                              @SerializedName("average_unit") val averageUnit: Int,
                              @SerializedName("average_weight") val averageWeight: Double,
                              @SerializedName("temperature") val temperature: Double)