package com.amplemind.vivasmart.vo_core.repository.remote.uploaders

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.dao.UploadObjectDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.remote.uploaders.annotations.Uploadable
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

class ActivityLogDailyPayUploader @Inject constructor(mPref: UserAppPreferences, mDao: UploadObjectDao): Uploader(mPref, mDao){

    fun create(obj: ActivityLogModel) =
            buildRequest(fillLogBody(obj, false), POST)

    fun create(objs: List<ActivityLogModel>) =
            buildRequest(objs.map{fillLogBody(it, false)}, POST)

    fun update(obj: ActivityLogModel, withGrooves: Boolean = false) =
            update(listOf(obj), withGrooves)

    fun update(objs: List<ActivityLogModel>, withGrooves: Boolean = false) =
            buildRequest(fillBulkLogBody(objs, withGrooves), PUT)

    fun delete(obj: ActivityLogModel) =
            buildDeleteRequest(ActivityLogDelete(obj.uuid))

    fun delete(objs: List<ActivityLogModel>) =
            objs.map {
                delete(it)
            }


    private fun fillBulkLogBody(objs: List<ActivityLogModel>, withGrooves: Boolean) =
            BulkLogBody(
                    objs.map {
                        fillLogBody(it, withGrooves)
                    }
            )

    private fun fillLogBody(obj: ActivityLogModel, withGrooves: Boolean): LogBody {

        var grooves: List<Groove>? = null

        if (withGrooves) {
            grooves = obj.grooves?.map {
                Groove(tableNo = it.tableNo, grooveNo = it.grooveNo)
            } ?: listOf()
        }

        return LogBody(
                uuid = obj.uuid,
                activityCodeUuid = obj.activityCodeUuid,
                collaboratorUuid = obj.collaboratorUuid,
                collaboratorId = obj.collaboratorId.toLong(),
                isPending = obj.isPending,
                isDone = obj.isDone,
                isPaused = obj.isPaused,
                isPractice = obj.isPractice,
                isTraining = obj.isTraining,
                isControl = obj.isControl,
                grooves = grooves,
                quantity = obj.quantity,
                badQuantity = obj.badQuantity ?: 0,
                startTime = obj.startTime,
                time = obj.time,
                date = obj.date,
                sign = obj.sign,
                signDate = obj.signDate
        )

    }

    @Uploadable(
            path = "activity_code/activity_log/dailypay",
            contentType = "application/json"
    )
    private data class ActivityLogDelete(
            override val identifier: String
    ) : Deletable

    @Uploadable(
            path = "activity_code/activity_log/dailypay",
            contentType = "application/json"
    )
    private data class BulkLogBody (
            @SerializedName("bulk") val bulk: List<LogBody>
    )

    @Uploadable(
            path = "activity_code/activity_log/dailypay",
            contentType = "application/json"
    )
    private data class LogBody (
            @SerializedName("uuid")                 val uuid: String,
            @SerializedName("activity_code_uuid")   val activityCodeUuid: String,
            @SerializedName("collaborator_uuid")    val collaboratorUuid: String,
            @SerializedName("collaborator_id")      val collaboratorId: Long,
            @SerializedName("is_pending")           val isPending: Boolean,
            @SerializedName("is_done")              val isDone: Boolean,
            @SerializedName("is_paused")            val isPaused: Boolean,
            @SerializedName("is_practice")          val isPractice: Boolean,
            @SerializedName("is_training")          val isTraining: Boolean,
            @SerializedName("is_control")           val isControl: Boolean,
            @SerializedName("grooves")              val grooves: List<Groove>?,
            @SerializedName("quantity")             val quantity: Int,
            @SerializedName("bad_quantity")         val badQuantity: Int,
            @SerializedName("start_time")           val startTime: Long,
            @SerializedName("time")                 val time: Long,
            @SerializedName("date")                 val date: String,
            @SerializedName("sign")                 val sign: String,
            @SerializedName("sign_date")            val signDate: String
    )

    private data class Groove (
            @SerializedName("table_no")     val tableNo: Int,
            @SerializedName("groove_no")    val grooveNo: Int
    )

}