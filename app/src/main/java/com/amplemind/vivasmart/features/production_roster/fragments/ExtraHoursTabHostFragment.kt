package com.amplemind.vivasmart.features.production_roster.fragments

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.FragmentTabHost
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel


class ExtraHoursTabHostFragment : BaseFragment(), ExtraHoursAuthorizationFragment.OnFragmentInteractionListener, ExtraHoursRequestsFragment.OnFragmentInteractionListener {

    private var mStage: StageModel? = null

    private var listener: OnFragmentInteractionListener? = null

    private var mTabHost: FragmentTabHost? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            //mStage = it.getParcelable("stage")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view:View =  inflater.inflate(R.layout.fragment_extra_hours_tab_host, container, false)
        arguments?.let {
            //mStage = it.getParcelable("stage")!!
        }
        mTabHost = view.findViewById<View>(android.R.id.tabhost) as FragmentTabHost

        mTabHost!!.setup(context!!, fragmentManager!!,android.R.id.tabcontent)


        mTabHost!!.addTab(mTabHost!!.newTabSpec("solicitudes").setIndicator("solicitudes"), ExtraHoursRequestsFragment::class.java, arguments)

        mTabHost!!.addTab(mTabHost!!.newTabSpec("autorizaciones").setIndicator("autorizaciones"), ExtraHoursAuthorizationFragment::class.java, arguments)
        return view
    }

/*
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
*/
    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(stage: StageModel) =
                ExtraHoursTabHostFragment().apply {
                    arguments = Bundle().apply {
                        //putParcelable("stage", stage)
                    }
                }
    }
}
