package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.PayrollRepository
import com.amplemind.vivasmart.core.repository.model.ControlQualityListModel
import io.reactivex.Observable
import retrofit2.http.*

interface ReportsApi {

    @GET("v2/activity_code/activity_log/condensed/{stageId}/signed")
    @Headers("Content-Type: application/json")
    fun getReviewedCollaborators(
            @Header("Authorization") authentication: String,
            @Path("stageId") stageId: Int
    ): Observable<List<ControlQualityListModel>>

    @GET("v2/activity_code/activity_log/condensed/{stageId}/nosigned")
    @Headers("Content-Type: application/json")
    fun getInProcessCollaborators(
            @Header("Authorization") authentication: String,
            @Path("stageId") stageId: Int
    ): Observable<List<ControlQualityListModel>>

    @GET("v2/packingline_paysheet/condensed/{packing_line}/signed")
    @Headers("Content-Type: application/json")
    fun getReviewedCollaboratorsPackingline(
            @Header("Authorization") authentication: String,
            @Path("packing_line") packing_line: Int
    ): Observable<List<ControlQualityListModel>>

    @GET("v2/packingline_paysheet/condensed/{packing_line}/nosigned")
    @Headers("Content-Type: application/json")
    fun getInProcessCollaboratorsPackingline(
            @Header("Authorization") authentication: String,
            @Path("packing_line") packing_line: Int
    ): Observable<List<ControlQualityListModel>>

    @GET("v2/activity_code/activity_log")
    @Headers("Content-Type: application/json")
    fun getRosterDetailt(
            @Header("Authorization") authentication: String,
            @Query("embed") embed : String = "activity_code,activity_code.activity,activity_code.activity.unit",
            @Query("collaborator_id") id_collaborator: Int,
            @Query("activity_code.stage_id") stage: Int,
            @Query("is_done") is_done: Boolean = false,
            @Query("is_pending") is_pending: Boolean = true,
            @Query("__logic") logic: String = "AND"
    ) : Observable<PayrollRepository.DetailtRosterActivitiesResponse>

    @PUT("/v2/activity_code/activity_log/daydone/{stageId}")
    @Headers("Content-Type: application/json")
    fun finishActivity(
            @Header("Authorization") authentication: String,
            @Path("stageId") stageId: Int
    ): Observable<Any>

    @GET("/v2/packingline_paysheet/packinglinelog")
    @Headers("Content-Type: application/json")
    fun getRosterDetailtLines(
            @Header("Authorization") authentication: String,
            @Query("embed") embed: String = "activity,packingline_collaborator,packing",
            @Query("is_done") is_done: Boolean = false,
            @Query("packingline_collaborator.packingline_id") idLine: Int,
            @Query("collaborator_id") id_collaborator: Int,
            @Query("__logic") logic: String = "AND"
    ): Observable<PayrollRepository.DetailtRosterLinesActivitiesResponse>

    @PUT("/v2/packingline/daydone/{id_packingline}")
    @Headers("Content-Type: application/json")
    fun finishActivityLines(
            @Header("Authorization") authentication: String,
            @Path("id_packingline") id_packingline: Int,
            @Body body: PayrollRepository.FinishDayLine
    ): Observable<Any>

}