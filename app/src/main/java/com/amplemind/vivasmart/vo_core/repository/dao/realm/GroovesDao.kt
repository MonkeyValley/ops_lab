package com.amplemind.vivasmart.vo_core.repository.dao.realm

import android.util.SparseArray
import collections.forEach
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel
import com.amplemind.vivasmart.vo_core.utils.getUUID
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmList
import io.realm.Sort
import javax.inject.Inject

class GroovesDao @Inject constructor(
        realmHolder: RealmHolder
): RealmDao<GrooveModel>(realmHolder, GrooveModel::class.java) {

    fun loadGrooves(activityCodeUuid: String, tableNo: Int): Single<List<GrooveModel>> =
            findAll({ realm ->
                query(realm)
                        .equalTo("activityCodeUuid", activityCodeUuid)
                        .and()
                        .equalTo("tableNo", tableNo)
                        .sort("grooveNo", Sort.ASCENDING)
            }, true)

    fun deleteAllForActivityLogAux(realm: Realm, activityLogUuid: String) {
        query(realm)
                .equalTo("activityLogUuid", activityLogUuid)
                .findAll()
                .deleteAllFromRealm()
    }

    fun createAsRealmListAux(realm: Realm, grooves: SparseArray<List<Int>>, func: (GrooveModel) -> GrooveModel): RealmList<GrooveModel> {
        val grooveObjects = RealmList<GrooveModel>()
        grooves.forEach { tableNumber, grooveNumbers ->
            grooveNumbers.forEach { grooveNumber ->
                val groove = func.invoke(
                    realm.createObject(modelClass, getUUID()).apply {
                            grooveNo = grooveNumber
                            tableNo = tableNumber
                    }
                )
                grooveObjects.add(groove)
            }
        }
        return grooveObjects
    }
}