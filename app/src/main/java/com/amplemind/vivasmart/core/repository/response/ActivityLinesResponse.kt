package com.amplemind.vivasmart.core.repository.response

import androidx.room.*
import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.core.repository.model.Crop
import com.amplemind.vivasmart.core.repository.model.CropUnitTypeConverters
import com.google.gson.annotations.SerializedName

data class ActivityLinesResponse(@SerializedName("data") val activities: List<ActivitiesResponse>)

@Entity
data class ActivitiesResponse(
        @SerializedName("activity_area_id") val activityAreaId: Int,
        @SerializedName("activity_category_id") val activityCategoryId: Int,
        @SerializedName("activity_sub_category_id") val activitySubCategoryId: Int,
        @SerializedName("activity_type_id") val activityTypeId: Int,
        @SerializedName("business_unit_id") val businessUnitId: Int,
        @SerializedName("code") val code: Int,
        @SerializedName("cost") val cost: Double?,
        @SerializedName("cost_per_hour") val costPerHour: Double,
        @SerializedName("practice_cost") val practice_cost: Double,
        @SerializedName("practice_code") val practice_code: Int,
        @TypeConverters(CropUnitTypeConverters::class) @SerializedName("crops") val crops: List<Crop>?,
        @SerializedName("description") val description: String,
        @SerializedName("extra_time_code") val extraTimeCode: Int,
        @SerializedName("extra_time_cost") val extraTimeCost: Double,
        @SerializedName("ft_code") val ftCode: Int,
        @SerializedName("ft_cost") val ftCost: Double,
        @SerializedName("icon") val icon: String,
        @SerializedName("id") @ColumnInfo(name = "id_activities") @PrimaryKey(autoGenerate = true) val id: Int,
        @SerializedName("is_active") val isActive: Boolean,
        @SerializedName("is_editable") val isEditable: List<IsEditable>,
        @SerializedName("is_grooves") val isGrooves: Boolean,
        @SerializedName("is_removable") val isRemovable: Boolean,
        @SerializedName("is_sunday_rate") @ColumnInfo(name = "sunday") val isSundayRate: Boolean?,
        @SerializedName("name") val name: String,
        @SerializedName("performance") val performance: Double,
        @SerializedName("range_from") val rangeFrom: Int,
        @SerializedName("range_to") val rangeTo: Int,
        @SerializedName("sunday_rate") @ColumnInfo(name = "sunday_rate") val sunday_rate_value: Boolean,
        @SerializedName("unit_id") val unitId: Int,
        @SerializedName("unit_limit") val unitLimit: Int,
        @SerializedName("collaborator_left") var collaborator_left: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readInt(),
            parcel.createTypedArrayList(Crop),
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readString()!!,
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            parcel.createTypedArrayList(IsEditable)!!,
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
            parcel.readString()!!,
            parcel.readDouble(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(activityAreaId)
        parcel.writeInt(activityCategoryId)
        parcel.writeInt(activitySubCategoryId)
        parcel.writeInt(activityTypeId)
        parcel.writeInt(businessUnitId)
        parcel.writeInt(code)
        parcel.writeValue(cost)
        parcel.writeDouble(costPerHour)
        parcel.writeDouble(practice_cost)
        parcel.writeInt(practice_code)
        parcel.writeTypedList(crops)
        parcel.writeString(description)
        parcel.writeInt(extraTimeCode)
        parcel.writeDouble(extraTimeCost)
        parcel.writeInt(ftCode)
        parcel.writeDouble(ftCost)
        parcel.writeString(icon)
        parcel.writeInt(id)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeTypedList(isEditable)
        parcel.writeByte(if (isGrooves) 1 else 0)
        parcel.writeByte(if (isRemovable) 1 else 0)
        parcel.writeValue(isSundayRate)
        parcel.writeString(name)
        parcel.writeDouble(performance)
        parcel.writeInt(rangeFrom)
        parcel.writeInt(rangeTo)
        parcel.writeByte(if (sunday_rate_value) 1 else 0)
        parcel.writeInt(unitId)
        parcel.writeInt(unitLimit)
        parcel.writeInt(collaborator_left)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActivitiesResponse> {
        override fun createFromParcel(parcel: Parcel): ActivitiesResponse {
            return ActivitiesResponse(parcel)
        }

        override fun newArray(size: Int): Array<ActivitiesResponse?> {
            return arrayOfNulls(size)
        }
    }
}


data class IsEditable (
        @SerializedName("unit_id") val unitId: Boolean,
        @SerializedName("range_from") val rangeFrom: Boolean,
        @SerializedName("range_to") val rangeTo: Boolean,
        @SerializedName("code") val code: Boolean,
        @SerializedName("extra_time_code") val extraTimeCode: Boolean,
        @SerializedName("ft_code") val ftCode: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (unitId) 1 else 0)
        parcel.writeByte(if (rangeFrom) 1 else 0)
        parcel.writeByte(if (rangeTo) 1 else 0)
        parcel.writeByte(if (code) 1 else 0)
        parcel.writeByte(if (extraTimeCode) 1 else 0)
        parcel.writeByte(if (ftCode) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<IsEditable> {
        override fun createFromParcel(parcel: Parcel): IsEditable {
            return IsEditable(parcel)
        }

        override fun newArray(size: Int): Array<IsEditable?> {
            return arrayOfNulls(size)
        }
    }
}

data class Packingline(
        @SerializedName("activity_id") val activityId: Int,
        @SerializedName("packingline_id") val packinglineId: Int,
        @SerializedName("people_no") val peopleNo: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(activityId)
        parcel.writeInt(packinglineId)
        parcel.writeInt(peopleNo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Packingline> {
        override fun createFromParcel(parcel: Parcel): Packingline {
            return Packingline(parcel)
        }

        override fun newArray(size: Int): Array<Packingline?> {
            return arrayOfNulls(size)
        }
    }
}