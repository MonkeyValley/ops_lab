package com.amplemind.vivasmart.features.production_roster.adapters

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.HeaderModel
import com.amplemind.vivasmart.features.production_roster.fragments.interfaces.ControlTimerManager
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserSectionViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import io.reactivex.subjects.BehaviorSubject


class TimerManagerUsersSectionAdapter(private val context: Context, private val supportFragmentManager: FragmentManager, val list: MutableList<TimerUserSectionViewModel>) :
        ExpandableRecyclerViewAdapter<TimerUserSectionViewHolder, TimerUserViewHolder>(list), ControlTimerManager {

    private val onClickAdd = BehaviorSubject.create<HeaderModel>()
    private val isCleanListSubject = BehaviorSubject.create<Boolean>()
    private val onDeleteLine = BehaviorSubject.create<Int>()

    fun isCleanListSubject(): BehaviorSubject<Boolean> {
        return isCleanListSubject
    }

    fun onDeleteLine() : BehaviorSubject<Int>{
        return onDeleteLine
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        if (holder is TimerUserViewHolder){
            holder.onRecycle()
        }
        super.onViewRecycled(holder)
    }

    override fun onStartGeneralTimer() {
        list.forEach { section ->
            section.items.forEach { viewModel ->
                (viewModel as TimerUserViewModel).startTimer()
            }
        }
        notifyDataSetChanged()
    }

    override fun onPauseGeneralTimer() {
        list.forEach { section ->
            section.items.forEach { viewModel ->
                (viewModel as TimerUserViewModel).stopTimer()
            }
        }
        notifyDataSetChanged()
    }

    override fun onNotifyItemChanged(position: Int) {
        notifyItemChanged(position)
    }

    override fun onNotifyDataSetChange() {
        notifyDataSetChanged()
    }

    override fun onRemoveItem(position: Int) {
        var count = 0
        var totalItems = 0
        list.forEach { section ->
            val visibleItems = if (isGroupExpanded(section)) section.getUsersSize() + 1 else 1
            count += visibleItems
            if (position <= count) {
                section.users.removeAt((position - 1) - totalItems)
                if (section.users.isEmpty()){

                    deleteLineOffline(section.id_lineOffline)

                    list.remove(section)
                    notifyDataSetChanged()
                }else{
                    notifyItemRemoved(position)
                    section.changeCountTitle()
                }
                isCleanListSubject.onNext(list.size <= 0)
                return
            }
            totalItems += visibleItems
        }
    }

    /**
     * in the offline process it is necessary to manually remove the line
     */
    private fun deleteLineOffline(id_lineOffline : Int?){
        if (id_lineOffline != null){
            onDeleteLine.onNext(id_lineOffline)
        }
    }

    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int): TimerUserSectionViewHolder {
        val layoutInflater = LayoutInflater.from(parent!!.context)
        val view = layoutInflater.inflate(R.layout.item_timer_user_header, parent, false)
        return TimerUserSectionViewHolder(view, context)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int): TimerUserViewHolder {
        val layoutInflater = LayoutInflater.from(parent!!.context)
        val view = layoutInflater.inflate(R.layout.item_users_timer, parent, false)
        return TimerUserViewHolder(view, supportFragmentManager)
    }

    override fun onBindChildViewHolder(holder: TimerUserViewHolder?, flatPosition: Int, group: ExpandableGroup<*>?, childIndex: Int) {
        val itemUser = (group as TimerUserSectionViewModel).users[childIndex]
        holder!!.bind(itemUser, childIndex, "isSection", 0.0)
    }

    fun onClickAddCollaborators(): BehaviorSubject<HeaderModel> {
        return onClickAdd
    }

    override fun onBindGroupViewHolder(holder: TimerUserSectionViewHolder?, flatPosition: Int, group: ExpandableGroup<*>?) {
        if (holder != null) {
            holder.setSectionTitle(group!!)
            holder.add.setOnClickListener {
                val name = (group as TimerUserSectionViewModel).name
                onClickAdd.onNext(HeaderModel(name,group.getPackingId(), group.getPackingLine(), group.getActivityId(), group.getCollaboratorLeft()))
            }
        }
    }


}