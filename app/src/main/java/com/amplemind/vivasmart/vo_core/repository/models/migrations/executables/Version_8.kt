package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_8 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        alterGrooveModel(realm)
    }

    override fun drop(realm: DynamicRealm) {
        revertGrooveModel(realm)
    }

    private fun alterGrooveModel(realm: DynamicRealm) {
        realm.schema.get("GrooveModel")
                ?.addIndex("id")
                ?.addIndex("activityLogId")
                ?.addIndex("activityLogUuid")
                ?.addIndex("tableNo")
                ?.addIndex("grooveNo")
    }

    private fun revertGrooveModel(realm: DynamicRealm) {
        realm.schema.get("GrooveModel")
                ?.removeIndex("id")
                ?.removeIndex("activityLogId")
                ?.removeIndex("activityLogUuid")
                ?.removeIndex("tableNo")
                ?.removeIndex("grooveNo")
    }

}