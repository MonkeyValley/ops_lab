package com.amplemind.vivasmart.vo_core.repository

import android.util.SparseArray
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityCodesDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.GroovesDao
import com.amplemind.vivasmart.vo_core.repository.models.realm.GrooveModel
import io.reactivex.Single
import javax.inject.Inject

class AssignGroovesRepository @Inject constructor(
        private val activityLogsDao: ActivityLogsDao,
        private val activityCodesDao: ActivityCodesDao,
        private val groovesDao: GroovesDao
) : Repository() {

    override fun cleanUp() {
        activityLogsDao.cleanUp()
        activityCodesDao.cleanUp()
        groovesDao.cleanUp()
    }

    fun loadActivityLog(activityLogUuid: String) =
            activityLogsDao.findByUuid(activityLogUuid)

    fun loadActivityCode(activityCodeUuid: String) =
            activityCodesDao.findByUuid(activityCodeUuid)

    fun loadGrooves(activityCodeUuid: String, tableNo: Int): Single<List<GrooveModel>> =
            groovesDao.loadGrooves(activityCodeUuid, tableNo)

    fun assignGrooves(activityLogUuid: String, grooves: SparseArray<List<Int>>) =
            activityLogsDao.assignGrooves(activityLogUuid, grooves)


}