package com.amplemind.vivasmart.features.mipe.adapter

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.databinding.ItemDiseaseMipeBinding
import com.amplemind.vivasmart.features.gallery.VisualSupportDialog
import com.amplemind.vivasmart.features.mipe.MipeMonitoringActivity
import com.amplemind.vivasmart.features.mipe.viewModel.ItemPlagueViewModel
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyDetailDialog
import com.amplemind.vivasmart.vo_core.repository.models.realm.MipeMonitoringModel
import io.reactivex.subjects.PublishSubject
import io.realm.Realm

class DiseaseAdapter : (RecyclerView.Adapter<DiseaseAdapter.DiseaseViewHolder>)() {

    var lotId = 0
    var groove = 0
    var week = 0
    var table = ""
    var plant = 0
    var activity: MipeMonitoringActivity? = null

    fun setData(lotId: Int, groove: Int, week: Int, table: String, plant: Int){
        this.lotId = lotId
        this.groove = groove
        this.week = week
        this.table = table
        this.plant = plant
    }

    private var list = listOf<ItemPlagueViewModel>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    fun addElements(data: List<ItemPlagueViewModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiseaseViewHolder {
        val binding = ItemDiseaseMipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DiseaseViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: DiseaseViewHolder, position: Int) {
        holder.bind(list[position])

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun onClickItem() = clickSubject

    fun isEmpty() = list.isEmpty()

    inner class DiseaseViewHolder(private val binding: ItemDiseaseMipeBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ItemPlagueViewModel

        fun bind(item: ItemPlagueViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item

            binding.tvDiseaseName.text = item.name

            binding.tvDiseaseName.setOnClickListener { view ->
                if(activity!!.ifConected()) {
                    val dialog = VisualSupportDialog().newInstance(item.id!!, item.name!!, 1)
                    dialog.show(activity?.supportFragmentManager!!, PhenologyDetailDialog.TAG)
                }
            }

            binding.cbDisease.setOnCheckedChangeListener { _, checkedId ->
                val radio: RadioButton = binding.root.findViewById(checkedId)
                if(radio.text.toString() == ""){
                    item.medition = 0
                    item.lvl = 0
                } else {
                    item.medition = 1
                    item.lvl = radio.text.toString().toInt()
                }
            }

            Realm.getDefaultInstance().use {
                val result = it
                        .where(MipeMonitoringModel::class.java)
                        .beginGroup()
                        .equalTo("lotId", lotId)
                        .and()
                        .equalTo("table", table.toInt())
                        .and()
                        .equalTo("grooveNo", groove)
                        .and()
                        .equalTo("week", week)
                        .endGroup()
                        .findFirst()

                if(result != null){
                    val plantResult = result.plants!!.where().equalTo("plantNo", plant).findFirst()
                    if(plantResult != null){
                        val data = plantResult.findings!!.where().equalTo("plagueId", item.id).findFirst()
                        if(data != null){
                            if(data.finding != null) {
                                when(data.level){
                                    1 -> binding.lvl1.isChecked = true
                                    2 -> binding.lvl2.isChecked = true
                                    3 -> binding.lvl3.isChecked = true
                                    else -> binding.lvl0.isChecked = true
                                }
                            }
                        }
                        binding.cbDisease.isEnable(false)
                        binding.cbDisease.isEnable(false)
                        binding.lvl0.isEnable(false)
                        binding.lvl1.isEnable(false)
                        binding.lvl2.isEnable(false)
                        binding.lvl3.isEnable(false)
                    }
                }
            }
        }

    }

}
