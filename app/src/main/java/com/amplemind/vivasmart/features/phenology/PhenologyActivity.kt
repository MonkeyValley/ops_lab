package com.amplemind.vivasmart.features.phenology

import android.os.Bundle
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyFragment
import com.amplemind.vivasmart.features.phenology.viewmodel.PhenologyActivityViewModel
import kotlinx.android.synthetic.main.activity_phenology.*
import javax.inject.Inject

class PhenologyActivity : BaseActivity() {
    @Inject
    lateinit var viewModel: PhenologyActivityViewModel

    companion object {
        const val TAG = "PhenologyActivity"
        var itemCode = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phenology)
        itemCode = intent.getStringExtra("itemCode")
        setupToolbar(itemCode)
        callFragmentMenuActivities(itemCode)
    }

    private fun callFragmentMenuActivities(itemCode: String) {
        val support = supportFragmentManager
        val commit = support.beginTransaction()
                .replace(R.id.container, PhenologyFragment().newInstance("PhenologyFragment", itemCode))
                .addToBackStack("PhenologyFragment")
                .commit()
    }

    private fun setupToolbar(itemCode:String) {
        val mToolbar = toolbar_phenology
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        if(itemCode == "0"){
            supportActionBar!!.title = "Lotes en variedades de pruebas "
        }else{
            supportActionBar!!.title = "Lotes en variedades comerciales"
        }

    }

    override fun onBackPressed() {
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}