package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemCategoriesDetailBinding
import io.reactivex.subjects.BehaviorSubject

class CategoriesDetailAdapter constructor(val context: Context): RecyclerView.Adapter<CategoriesDetailAdapter.CategoriesDetailViewHolder>() {

    private var list = mutableListOf<CategoriesDetailItemViewModel>()
    private val itemClicked = BehaviorSubject.create<CategoriesDetailItemViewModel>()

    fun addElements(data : List<CategoriesDetailItemViewModel>){
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesDetailViewHolder {
        val binding = ItemCategoriesDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoriesDetailViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CategoriesDetailViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onItemClicked(): BehaviorSubject<CategoriesDetailItemViewModel> {
        return itemClicked
    }

    inner class CategoriesDetailViewHolder(private val binding: ItemCategoriesDetailBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item : CategoriesDetailItemViewModel

        fun bind(item: CategoriesDetailItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item
            binding.addType.setOnClickListener{
                itemClicked.onNext(item)
            }
        }

    }

}