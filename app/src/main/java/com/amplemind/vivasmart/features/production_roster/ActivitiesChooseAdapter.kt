package com.amplemind.vivasmart.features.production_roster

import android.content.Context
import android.os.SystemClock
import androidx.core.view.ViewCompat
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.loadActivityIcon
import com.amplemind.vivasmart.databinding.ItemChooseActivityForLotBinding
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ItemActivitiesPayrollViewModel
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.content_haulage_activity.view.*
import kotlinx.android.synthetic.main.content_revised_fragment.view.*
import kotlinx.android.synthetic.main.item_choose_activity_for_lot.view.*

class ActivitiesChooseAdapter constructor(val context: Context, private val stageUuid: String) : BaseAdapter<ActivityModel, ItemActivitiesPayrollViewModel, ActivitiesChooseAdapter.ActivitiesPayrollViewHolder>() {

    val onActivityClicked: PublishSubject<ActivityModel> = PublishSubject.create()
    var listener: Listener? = null
    var lastClickTime: Long = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivitiesPayrollViewHolder {
        val binding = ItemChooseActivityForLotBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ActivitiesPayrollViewHolder(binding)
    }

    override fun buildViewModel(item: ActivityModel): ItemActivitiesPayrollViewModel =
            ItemActivitiesPayrollViewModel(stageUuid, item)

    override fun getItemKey(item: ActivityModel): String = item.uuid


    inner class ActivitiesPayrollViewHolder(private val binding: ItemChooseActivityForLotBinding) : BaseAdapter.ViewHolder<ItemActivitiesPayrollViewModel>(binding.root) {

        private var mIconLoaderDisposable: Disposable? = null
        private var mClickDisposable: Disposable? = null

        override fun bind(viewModel: ItemActivitiesPayrollViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()
            val ivIcon = binding.root.findViewById<ImageView>(R.id.icon)
            val tvCounter =  binding.root.findViewById<TextView>(R.id.tv_counter)

            mClickDisposable = binding.root.clicks()
                    .subscribe {
                        if (SystemClock.elapsedRealtime() - lastClickTime > 1000){
                            onActivityClicked.onNext(viewModel.item)
                            lastClickTime = SystemClock.elapsedRealtime()
                        }

                    }

            viewModel.counter = listener?.onGetCounterValue(viewModel.item, adapterPosition)

            if (viewModel.iconDrawable == null) {
                mIconLoaderDisposable =
                        context.loadActivityIcon(viewModel.item)
                                .subscribe { icon ->
                                    viewModel.iconDrawable = icon
                                    ivIcon.setImageDrawable(icon)
                                }
            }
            else {
                ivIcon.setImageDrawable(viewModel.iconDrawable)
            }

            ViewCompat.setElevation(binding.root.tv_counter, 10.0f)

        }

        override fun cleanUp() {
            super.cleanUp()
            mIconLoaderDisposable?.dispose()
            mClickDisposable?.dispose()
        }

    }

    interface Listener {
        fun onGetCounterValue(activity: ActivityModel, position: Int): Observable<Int>?
    }

}