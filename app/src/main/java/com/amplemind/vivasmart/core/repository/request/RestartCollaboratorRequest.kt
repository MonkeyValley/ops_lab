package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

class RestartTimerCollaboratorRequest (@SerializedName("bulk") val collaborators: List<RestartTimerCollaborator>)
data class RestartTimerCollaborator(@SerializedName("id") val id: Int)