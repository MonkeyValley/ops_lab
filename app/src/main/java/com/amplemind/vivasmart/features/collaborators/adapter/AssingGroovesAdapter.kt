package com.amplemind.vivasmart.features.collaborators.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.flipView
import com.amplemind.vivasmart.databinding.ItemAssingGrooveBinding
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemAssingGroovesViewModel
import com.amplemind.vivasmart.features.collaborators.ManagerSelectedItems
import com.amplemind.vivasmart.features.collaborators.fragment.OnGrooveListener
import androidx.databinding.library.baseAdapters.BR
import kotlinx.android.synthetic.main.content_groove_activities.view.*
import kotlinx.android.synthetic.main.item_assing_groove.view.*

class AssingGroovesAdapter(private val listener: OnGrooveListener) : RecyclerView.Adapter<AssingGroovesAdapter.GroovesViewHolder>() {

    private var manager = ManagerSelectedItems()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroovesViewHolder {
        val binding = ItemAssingGrooveBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GroovesViewHolder(binding)
    }

    override fun getItemCount() = manager.getItemCount()

    override fun onBindViewHolder(holder: GroovesViewHolder, position: Int) {
        holder.bind(manager.getItemAssingGroovesViewModel(position))
        applyIconAnimation(holder, position)
    }

    private fun applyIconAnimation(holder: GroovesViewHolder, position: Int) {
        val status = getAnimationState(holder.item!!, position)

        when(status){
            ManagerSelectedItems.ANIMATION_STATUS.DELETE_GROOVE -> {
                holder.itemView.image_status.visibility = View.GONE
                resetIconYAxis(holder.itemView.ln_groove)
                holder.itemView.ln_groove.setBackgroundResource(R.drawable.ic_groove_disable)
                holder.itemView.ln_groove.visibility = View.VISIBLE
                holder.itemView.ln_groove.alpha = 1F
            }
            ManagerSelectedItems.ANIMATION_STATUS.SELECTED_GROOVE -> {
                holder.itemView.ln_groove.visibility = View.GONE
                resetIconYAxis(holder.itemView.image_status)
                holder.itemView.image_status.visibility = View.VISIBLE
                holder.itemView.image_status.alpha = 1F
                if (manager.isCurrentPosition(position)) {
                    holder.itemView.image_status.flipView(holder.itemView.ln_groove, true)
                    resetCurrentIndex()
                }
            }
            ManagerSelectedItems.ANIMATION_STATUS.RESET_GROOVE_ASSING -> {
                resetIconYAxis(holder.itemView.image_status)
                holder.itemView.image_status.visibility = View.VISIBLE
                holder.itemView.image_status.alpha = 1F
            }
            ManagerSelectedItems.ANIMATION_STATUS.RESET_GROOVE -> {
                holder.itemView.image_status.visibility = View.GONE
                resetIconYAxis(holder.itemView.ln_groove)
                holder.itemView.ln_groove.visibility = View.VISIBLE
                holder.itemView.ln_groove.alpha = 1F
                if (manager.isReverseAllAnimations(position) || manager.isCurrentPosition(position)) {
                    holder.itemView.image_status.flipView(holder.itemView.ln_groove, false)
                    resetCurrentIndex()
                }
            }
        }

        holder.item!!.state_animation = status
    }

    private fun getAnimationState(item: ItemAssingGroovesViewModel, position: Int): ManagerSelectedItems.ANIMATION_STATUS {
        return if (item.state_animation != null) item.state_animation!! else manager.getAnimationState(item, position)
    }

    private fun resetCurrentIndex() {
        manager.resetCurrentIndex()
    }

    private fun resetIconYAxis(view: View) {
        if (view.rotationY != 0F) {
            view.rotationY = 0F
        }
    }

    fun addElements(data: List<ItemAssingGroovesViewModel>) {
        manager.removeAll()
        manager.addElements(data)
        notifyDataSetChanged()
    }

    inner class GroovesViewHolder(private val binding: ItemAssingGrooveBinding) : RecyclerView.ViewHolder(binding.root) {

        var item: ItemAssingGroovesViewModel? = null

        fun bind(item: ItemAssingGroovesViewModel) {
            binding.setVariable(BR.viewGroove, item)
            binding.executePendingBindings()
            this.item = item
            if (adapterPosition > -1) {
                binding.root.rl_groove.setOnClickListener {
                    if (!item.status || item.isActive()){
                        item.selected = !item.selected
                        item.setPosition(adapterPosition)

                        toogleSelection(adapterPosition)
                        item.state_animation = manager.getAnimationState(item, adapterPosition)
                        item.item.animation_status = (item.state_animation as ManagerSelectedItems.ANIMATION_STATUS).state
                        listener.onClickGroove(item)
                    }
                }
                binding.root.rl_groove.setOnLongClickListener {
                    if (!item.status || item.isActive()) {
                        item.selected = !item.selected
                        item.setPosition(adapterPosition)

                        toogleSelection(adapterPosition)
                        item.state_animation = manager.getAnimationState(item, adapterPosition)

                        item.item.animation_status = (item.state_animation as ManagerSelectedItems.ANIMATION_STATUS).state
                        listener.onClickGroove(item)
                        it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                    }
                    return@setOnLongClickListener true
                }
            }
        }
    }


    /**
     * create the animation for the selected item either to return to the normal state or to be selected
     */
    fun toogleSelection(position: Int) {
        manager.addDeleteItems(position)

        manager.toogleSelection(position)

        notifyItemChanged(position)
    }

    /**
     *  delete all elements selected
     */
    fun clearSelections() {
        manager.clearSelections()
        notifyDataSetChanged()
    }

    /**
     *  return normal state all elements
     */
    fun resetAnimationIndex() {
        manager.resetAnimationIndex()
    }
}
