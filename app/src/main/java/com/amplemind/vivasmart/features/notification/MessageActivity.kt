package com.amplemind.vivasmart.features.notification

import android.os.Bundle
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.features.notification.fragment.InboxFragment
import kotlinx.android.synthetic.main.custom_toolbar.*

class MessageActivity : BaseActivityWithFragment(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)
        initToolbar()
        callFragmentMenuActivities()
    }

    private fun callFragmentMenuActivities() {
        val support = supportFragmentManager
        val commit = support.beginTransaction()
                .replace(R.id.fragment_message, InboxFragment().newInstance())
                .addToBackStack("InboxFragment")
                .commit()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.subtitle = "Bandeja de mensajes"
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        finish()
    }
}