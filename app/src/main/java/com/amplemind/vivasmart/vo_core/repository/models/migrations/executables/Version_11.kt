package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_11 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        createPulseForTable(realm)
        createPulseLotSpike(realm)
    }

    private fun createPulseForTable(realm: DynamicRealm) {
        val pulseForTableSchema = realm.schema.create("PulseForTable")
                .addField("date", String::class.javaObjectType)
                .addField("drain", Int::class.javaObjectType)
                .addField("drainPercent", Double::class.javaObjectType)
                .addField("hour", String::class.javaObjectType)
                .addField("lotId", Int::class.javaObjectType)
                .addField("minutes", Double::class.javaObjectType)
                .addField("pulse", Int::class.javaObjectType)
                .addField("sending", Int::class.javaObjectType)
                .addField("spike", Int::class.javaObjectType)
                .addField("table", Int::class.javaObjectType)
                .addField("uuid", String::class.javaObjectType)
                .setRequired("uuid", true)

        val tablesPulsePerDaySchema = realm.schema.create("TablesPulsePerDay")
                .addField("lotId", Int::class.javaObjectType)
                .addField("date", String::class.javaObjectType)
                .addRealmListField("T1", pulseForTableSchema)
                .addRealmListField("T2", pulseForTableSchema)
                .addRealmListField("T3", pulseForTableSchema)
                .addRealmListField("T4", pulseForTableSchema)
                .addRealmListField("T5", pulseForTableSchema)
                .addRealmListField("T6", pulseForTableSchema)
                .addField("uuid", String::class.javaObjectType)
                .setRequired("uuid", true)

        realm.schema.create("PulsesPerWeekFertirriegoModel")
                .addField("lotId", Int::class.javaObjectType)
                .addRealmListField("date0", tablesPulsePerDaySchema)
                .addRealmListField("date1", tablesPulsePerDaySchema)
                .addRealmListField("date2", tablesPulsePerDaySchema)
                .addRealmListField("date3", tablesPulsePerDaySchema)
                .addRealmListField("date4", tablesPulsePerDaySchema)
                .addRealmListField("date5", tablesPulsePerDaySchema)
                .addRealmListField("date6", tablesPulsePerDaySchema)
                .addField("uuid", String::class.java)
                .setRequired("uuid", true)
    }


    private fun createPulseLotSpike(realm: DynamicRealm) {
        realm.schema.create("PulseLotSpike")
                .addField("lotId", Int::class.javaObjectType)
                .addField("spike", Int::class.javaObjectType)
                .addField("table", Int::class.javaObjectType)
                .addField("uuid", String::class.javaObjectType)
                .setRequired("uuid", true)
    }

    override fun drop(realm: DynamicRealm) {}
}