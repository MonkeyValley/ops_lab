package com.amplemind.vivasmart.features.quality_control.viewModel

import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.core.repository.model.IssueModel

class IssueViewModel constructor(val model: IssueModel?, val isNote: Boolean = false, val image_no_found : Boolean = false) {

    var notesVisibility = ObservableInt(View.GONE)
    var switchVisibility = ObservableInt(View.VISIBLE)
    val name = "${model?.name}:"
    val id = model?.id ?: 0
    var switchEnabled = model?.is_active ?: false
    var comment: String = ""

    init {
        if (isNote) {
            notesVisibility = ObservableInt(View.VISIBLE)
            switchVisibility = ObservableInt(View.GONE)
        }
    }

}