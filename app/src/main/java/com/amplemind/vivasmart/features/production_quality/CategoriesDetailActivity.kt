package com.amplemind.vivasmart.features.production_quality

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.snackWithoutInternetMessage
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import kotlinx.android.synthetic.main.activity_categories_detail.*
import javax.inject.Inject

class CategoriesDetailActivity : BaseActivity() {

    companion object {
        val TAG = ProductionQualityActivity::class.java.simpleName

        val CATEGORIES_REPORT_INTENT = 1
    }

    @Inject
    lateinit var viewModel: CategoriesDetailViewModel

    @Inject
    lateinit var preferences: UserAppPreferences

    private lateinit var progress: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories_detail)

        setupToolbar()

        initProgress()
        initRecycler()

        getData()
    }

    private fun showProgress(show: Boolean) {
        if (show) progress.show() else progress.dismiss()
    }


    private fun getData() {
        viewModel.onProgressStatus().subscribe(this::showProgress).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onRequestFail).addTo(subscriptions)
        viewModel.getIssuesList().subscribe(this::setData).addTo(subscriptions)
        if(true) {
            viewModel.getIssuesByCategory(preferences.token, getCategory(), getLotID()).addTo(subscriptions)
        } else {
            root_activity_categories.snackWithoutInternetMessage()
            viewModel.getLocalIssuesByCategory(getCategory(), getLotID()).addTo(subscriptions)
        }
    }

    private fun onRequestFail(pair: Pair<String, Int?>) {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.app_name))
                .setMessage(pair.first)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }.show()
    }

    private fun setData(data: List<CategoriesDetailItemViewModel>) {
        (rv_categories.adapter as CategoriesDetailAdapter).addElements(data)
    }

    private fun initProgress() {
        progress = ProgressDialog(this)
        progress.setTitle(getString(R.string.app_name))
        progress.setMessage(getString(R.string.server_connection))
        progress.setCancelable(false)
    }

    private fun initRecycler() {
        rv_categories.hasFixedSize()
        rv_categories.layoutManager = LinearLayoutManager(this)
        rv_categories.adapter = CategoriesDetailAdapter(this)
        (rv_categories.adapter as CategoriesDetailAdapter).onItemClicked().subscribe(this::onItemClicked).addTo(subscriptions)
    }

    private fun onItemClicked(item: CategoriesDetailItemViewModel) {
        val activityIntent = Intent(this, CategoriesReportsActivity::class.java)
        activityIntent.putExtra("id", item.getId())
        activityIntent.putExtra("title", "${getLotName()}: ${getNameCategory()}")
        activityIntent.putExtra("stage_id", intent.getIntExtra("stage_id", 0))
        activityIntent.putExtra("category_id", getCategory())
        activityIntent.putExtra("lot_id", getLotID())
        activityIntent.putParcelableArrayListExtra("tables", intent.getParcelableArrayListExtra("tables"))
        startActivityForResult(activityIntent, CategoriesDetailActivity.CATEGORIES_REPORT_INTENT)
    }

    private fun getLotID(): Int {
        return intent.getIntExtra("lot_id", 0)
    }

    private fun getCategory(): Int {
        return intent.getIntExtra("category_id", 0)
    }

    private fun getNameCategory(): String {
        return intent.getStringExtra("name")
    }

    private fun getLotName(): String {
        return intent.getStringExtra("name_lot")
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_categories_detail
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "${getLotName()}: ${getNameCategory()}"
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("incidents", 1) //TODO: add logic
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            CATEGORIES_REPORT_INTENT -> {
                if (resultCode == Activity.RESULT_OK) {
                    rv_categories.adapter!!.notifyDataSetChanged()
                    onBackPressed()
                }
            }
        }
    }
}
