package com.amplemind.vivasmart.vo_core.repository.models.extensions

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.utils.currentDate

val ActivityLogModel?.isActive: Boolean
    get() = this != null && !this.isDone && !this.isPending

fun ActivityLogModel?.getCollaboratorName(context: Context): String =
        this?.collaborator?.name ?: context.getString(R.string.default_collaborator_name)

fun ActivityLogModel?.getUnitName(context: Context): String =
        this?.activityCode?.activity?.getUnitName(context) ?: context.getString(R.string.units_general)

fun ActivityLogModel.getActiveHours(): Int {

    val currentTime = getActiveTime()

    var hours = currentTime / 3600
    val minutes = currentTime % 3600 / 60

    if (minutes >= 45) {
        hours ++
    }

    return hours.toInt()

}

fun ActivityLogModel.getActiveOnlyHours(): Int {

    val currentTime = getActiveOnlyTime()

    var hours = currentTime / 3600
    val minutes = currentTime % 3600 / 60

    if (minutes >= 45) {
        hours ++
    }

    return hours.toInt()

}

fun ActivityLogModel.getActivityCode(): ActivityCodeModel {
    return activityCode ?: throw IllegalStateException("ActivityCode not assigned to ActivityLog")
}

fun ActivityLogModel.getActivity(): ActivityModel {
    return activityCode?.activity ?: throw IllegalStateException("Activity not assigned to ActivityLog's ActivityCode")
}

fun ActivityLogModel.isControlAvailable(): Boolean {
    return activityCode?.activity?.controlCode != null
}

fun ActivityLogModel.isTrainingAvailable(): Boolean {
    return (activityCode?.activity?.costPerHour ?: 0.0) > 0.0
}

fun ActivityLogModel.isPracticeAvailable(): Boolean {
    return activityCode?.activity?.isPractice ?: false
}

fun ActivityLogModel.getActiveTime(checkIfPaused: Boolean = true): Long {
    return if (checkIfPaused && isPaused) {
        time
    }
    else {
        time + (currentDate() - startTime)
    }
}

fun ActivityLogModel.getActiveOnlyTime(): Long = time

fun ActivityLogModel.setTimeInHours(timeInHours: Int) {
    time = timeInHours * 3600L
}

fun ActivityLogModel.setTimeInMinutes(timeInMinutes: Int) {
    time = timeInMinutes * 60L
}

fun ActivityLogModel.getPayment(
        isPractice: Boolean = this.isPractice,
        isTraining: Boolean = this.isTraining,
        isControl: Boolean  = this.isControl,
        withSundayRate: Boolean = false): Double {

    val activity = activityCode!!.activity!!
    val stage = activityCode!!.stage!!

    val activeHours = getActiveHours()

    Log.i("Collaborator", collaborator?.name)
    Log.i("Activity", activity.name)
    Log.i("Active Hours", activeHours.toString())
    Log.i("IsTraining", isTraining.toString())
    Log.i("IsPractice", isPractice.toString())
    Log.i("IsControl", isControl.toString())
    Log.i("Control Cost", activity.controlCost.toString())
    Log.i("CostPerHour", activity.costPerHour.toString())

    return when {
        isTraining   -> activeHours * activity.costPerHour
        isControl    -> activeHours * activity.controlCost
        isPractice   -> activeHours * activity.practiceCost
        else -> when (activity.type) {
            ActivityType.PIECE ->
                if (activity.isHead) {
                    quantity * stage.plantsPerGroove * activity.realCost
                } else {
                    quantity * activity.realCost
                }
            else ->
                activeHours * activity.realCost
        }
    } * if (withSundayRate) 1.5 else 1.0

}
