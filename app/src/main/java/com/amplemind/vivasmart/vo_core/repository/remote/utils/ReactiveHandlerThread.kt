package com.amplemind.vivasmart.vo_core.repository.remote.utils

import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject

open class ReactiveHandlerThread (name: String): HandlerThread(name) {

    enum class Status {
        NOT_READY,
        READY,
        QUITTING,
        QUITTING_SAFELY
    }

    val onStatusChanged = BehaviorSubject.createDefault(Status.NOT_READY)

    val scheduler: Scheduler
        get() = AndroidSchedulers.from(looper)

    private fun sendStatusEvent(status: Status) =
            onStatusChanged.onNext(status)

    override fun quit(): Boolean {
        sendStatusEvent(Status.QUITTING)
        return super.quit()
    }

    override fun quitSafely(): Boolean {
        sendStatusEvent(Status.QUITTING_SAFELY)
        return super.quitSafely()
    }

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        Handler(Looper.getMainLooper()).post {
            sendStatusEvent(Status.READY)
        }
    }

}
