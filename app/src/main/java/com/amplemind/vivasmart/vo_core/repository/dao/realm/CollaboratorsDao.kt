package com.amplemind.vivasmart.vo_core.repository.dao.realm

import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.MessageModel
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.Case
import io.realm.Realm
import io.realm.Sort
import java.util.*
import javax.inject.Inject

class CollaboratorsDao @Inject constructor(realmHolder: RealmHolder): RealmDao<CollaboratorModel>(realmHolder, CollaboratorModel::class.java) {

    fun findAllNotWorking(): Observable<Changes<CollaboratorModel>> =
            findAllLiveAsync { realm ->
                query(realm).equalTo("isWorking", false)
                        .sort("name", Sort.ASCENDING)
            }

    fun findByUuidAndCodeQuery(uuids: List<String>, query: String): Observable<Changes<CollaboratorModel>> {
        return findAllLiveAsync { realm ->

            var employeeList = query(realm)
                    .`in`("uuid", uuids.toTypedArray())
                    .and()
                    .contains("employeeCode", query)
                    .sort("name", Sort.ASCENDING)

            if (employeeList.count() > 0) {
                return@findAllLiveAsync employeeList
            } else {
                employeeList = query(realm)
                        .`in`("uuid", uuids.toTypedArray())
                        .and()
                        .contains("name", query, Case.INSENSITIVE)
                        .sort("name", Sort.ASCENDING)

                return@findAllLiveAsync employeeList
            }

        }
    }

    fun findActivityByUuidAndCodeQuery(uuids: List<String>, query: String): Observable<Changes<CollaboratorModel>> {
        return findAllLiveAsync { realm ->

            var employeeList = query(realm)
                    .beginGroup()
                    .`in`("uuid", uuids.toTypedArray())
                    .and()
                    .isNotNull("activityId")
                    .and()
                    .contains("employeeCode", query)
                    .and()
                    .equalTo("activityDate", getDate(1))
                    .or()
                    .equalTo("activityDate", getDate(2))
                    .endGroup()
                    .sort("activityId", Sort.ASCENDING)

            if (employeeList.count() > 0) {
                return@findAllLiveAsync employeeList
            } else {
                employeeList = query(realm)
                        .beginGroup()
                        .`in`("uuid", uuids.toTypedArray())
                        .and()
                        .isNotNull("activityId")
                        .and()
                        .contains("name", query, Case.INSENSITIVE)
                        .and()
                        .equalTo("activityDate", getDate(1))
                        .or()
                        .equalTo("activityDate", getDate(2))
                        .endGroup()
                        .sort("activityId", Sort.ASCENDING)

                return@findAllLiveAsync employeeList
            }

        }
    }

    fun findWithCodeQuery(query: String): Observable<Changes<CollaboratorModel>> {
        return findAllLiveAsync { realm ->

            var employeeList = query(realm).contains("employeeCode", query)
                    .and()
                    .equalTo("isWorking", false)
                    .sort("name", Sort.ASCENDING)

            if (employeeList.count() > 0) {
                return@findAllLiveAsync employeeList
            } else {
                employeeList = query(realm).contains("name", query, Case.INSENSITIVE)
                        .and()
                        .equalTo("isWorking", false)
                        .sort("name", Sort.ASCENDING)

                return@findAllLiveAsync employeeList
            }

        }
    }

    fun findActivityWithCodeQuery(query: String): Observable<Changes<CollaboratorModel>> {
        return findAllLiveAsync { realm ->

            var employeeList = query(realm)
                    .beginGroup()
                    .contains("employeeCode", query)
                    .and()
                    .equalTo("isWorking", false)
                    .and()
                    .isNotNull("activityId")
                    .and()
                    .equalTo("activityDate", getDate(1))
                    .or()
                    .equalTo("activityDate", getDate(2))
                    .endGroup()
                    .sort("activityId", Sort.ASCENDING)

            if (employeeList.count() > 0) {
                return@findAllLiveAsync employeeList
            } else {
                employeeList = query(realm)
                        .beginGroup()
                        .contains("name", query, Case.INSENSITIVE)
                        .and()
                        .equalTo("isWorking", false)
                        .and()
                        .isNotNull("activityId")
                        .and()
                        .equalTo("activityDate", getDate(1))
                        .or()
                        .equalTo("activityDate", getDate(2))
                        .endGroup()
                        .sort("activityId", Sort.ASCENDING)

                return@findAllLiveAsync employeeList
            }

        }
    }

    fun findByCode(code: String): Maybe<CollaboratorModel> =
            findFirstAsync ({ realm ->
                query(realm).equalTo("employeeCode", code)
                        .sort("name", Sort.ASCENDING)
            })

    fun findByCodeX(code: String): Single<CollaboratorModel> =
            Realm.getDefaultInstance().use {
                Single.create<CollaboratorModel> { emitter ->
                    Realm.getDefaultInstance().use { realm ->
                        val results = realm
                                .where(CollaboratorModel::class.java)
                                .equalTo("employeeCode", code)
                                .sort("name", Sort.ASCENDING)
                                .findFirst()
                        emitter.onSuccess(realm.copyFromRealm(results)!!)
                    }
                }
            }


    fun setIsWorkingAux(realm: Realm, collaborator: CollaboratorModel, isWorking: Boolean) {
        collaborator.isWorking = isWorking
        if (!collaborator.isManaged) {
            realm.insertOrUpdate(collaborator)
        }
    }

    fun getDate(mode: Int) : String {
        val calendar = Calendar.getInstance()
        var month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        var day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()

        return when(mode){
            1 -> ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
            else -> {
                calendar.add(Calendar.DATE, -1)
                month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
                day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
                ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)
            }
        }
    }

}