package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.response.FinishedProductResult
import java.math.RoundingMode

class ItemReportFinishedProductViewModel(finishedProduct : FinishedProductResult) {
    val id = finishedProduct.id.toString()
    val lot_name = finishedProduct.lot_name
    val packing_name = finishedProduct.packing_name
    val percent = "${(finishedProduct.percent ?: 0.00).toBigDecimal().setScale(2, RoundingMode.HALF_EVEN)}%"
    var percentInt = finishedProduct.percent
}
