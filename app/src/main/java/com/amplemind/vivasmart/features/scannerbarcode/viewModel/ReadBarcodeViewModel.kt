package com.amplemind.vivasmart.features.scannerbarcode.viewModel

import android.util.Log.e
import com.amplemind.vivasmart.core.extensions.onlyDigits
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.CollaboratorsRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ReadBarcodeViewModel @Inject constructor(private val repository: CollaboratorsRepository,
                                               private val repository2: com.amplemind.vivasmart.vo_core.repository.CollaboratorsRepository,
                                               private val preferences: UserAppPreferences,
                                               private val apiErrors: HandleApiErrors,
                                               private val mediator: Mediator) {

    private var getCollaborator = BehaviorSubject.create<List<CollaboratorModel>>()
    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val onFail: BehaviorSubject<String> = BehaviorSubject.create()

    private val scannedUsers = mutableListOf<CollaboratorModel>()

    private val collaboratorSearch = BehaviorSubject.create<String>()

    private fun getStageByFlow(lines_flow: Boolean): Int {
        return if (lines_flow) {
            mediator.bundleItemLinesPackage ?: 0
        } else {
            mediator.bundleItemActivitiesPayroll?.stageId ?: 0
        }
    }

    fun getListCollaborators(code: String, lines_flow: Boolean): Disposable {
        TODO("NOT IMPLEMENTED")
        /*
        return repository2.getCollaboratorWithCode(code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    if (it != null) {

                        if (it.isWorking == true) {
                            noUserFound("Este colaborador ya se encuentra laborando")
                        }
                        else {

                            //Check if user already exists in our scanned list
                            val found = scannedUsers.find { scanned -> scanned.employeeCode == it.employeeCode }

                            if (found == null) {
                                scannedUsers.add(it)
                            }

                            getCollaborator.onNext(scannedUsers)

                        }
                    } else {
                        noUserFound("Usuario no encontrado.")
                    }
                }) {
                    if (it is NullPointerException) {
                        noUserFound("Usuario no encontrado")
                    }
                    else {
                        onFail.onNext(it.localizedMessage)
                    }
                }

         */
        /*
        return repository.searchCollaboratorByCode(preferences.token, code, getStageByFlow(lines_flow), lines_flow)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    if (!it.data.isEmpty()) {
                        for (result in it.data) {
                            if (!scannedUsers.contains(result)) {
                                scannedUsers.add(result)
                            }
                        }
                        getCollaborator.onNext(scannedUsers)
                    } else {
                        noUserFound("Usuario no encontrado.")
                    }
                }) {
                    onFail.onNext(apiErrors.handleLoginError(it))
                }
                */
    }

    private fun noUserFound(error: String) {
        onFail.onNext(error)
    }

    fun addLocalEmployee(code: String) {
        if (!code.onlyDigits()) {
            noUserFound("Usuario no encontrado.")
            return
        }

        if (scannedUsers.none { employee -> employee.employeeCode == code }) {
            //val employee = CollaboratorsListModel(code.toInt(), code, null, code, null)
            //scannedUsers.add(employee)
        }
        getCollaborator.onNext(scannedUsers)
    }


    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return onFail
    }

    fun onGetCollaborators(): BehaviorSubject<List<CollaboratorModel>> {
        return getCollaborator
    }

    fun addUserInPackage(): BehaviorSubject<String> {
        return collaboratorSearch
    }

    fun existCollaborator(code: String): Disposable {
        return repository.existCollaboratorInLines(code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result) {
                        noUserFound("Ya esta en una linea")
                    } else {
                        collaboratorSearch.onNext(code)
                    }
                }, {
                    e("Busqueda", "Error ${it.message}")
                })
    }

}