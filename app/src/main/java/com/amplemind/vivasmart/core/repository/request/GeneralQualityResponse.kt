package com.amplemind.vivasmart.core.repository.request

import com.amplemind.vivasmart.core.repository.model.GeneralQualityScoreModel
import com.google.gson.annotations.SerializedName

data class GeneralQualityResponse(
        @SerializedName("general_quality_act") val scores: List<GeneralQualityScoreModel>
)