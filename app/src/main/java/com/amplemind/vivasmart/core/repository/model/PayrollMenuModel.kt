package com.amplemind.vivasmart.core.repository.model


data class PayrollMenuModel(val name : String, val drawable : Int,val active : Boolean = false)
