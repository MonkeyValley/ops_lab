package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName

data class ReceptionQualityRevisionModel(
        @SerializedName("carry_order_id")   val carryOrderId: Long,
        @SerializedName("unit_id")          val unitId: Long,
        @SerializedName("user_id")          val userId: Long,
        @SerializedName("issues")           val issues: List<IssueCountModel>,
        @SerializedName("sample_size")      val sampleSize: Int,
        @SerializedName("export")           val export: Int,
        @SerializedName("permanent_defect") val permanentDefect: Int,
        @SerializedName("defect_condition") val defectCondition: Int,
        @SerializedName("id")               val id: Long = 0,
        @SerializedName("uuid")             val uuid: String = getUUID()
) {
    data class IssueCountModel (
            @SerializedName("id")           val issueId: Long,
            @SerializedName("count")        val count: Int,
            @SerializedName("isPermanent")  val isPermanent: Boolean,
            @SerializedName("name")         val name: String = ""
    )

}