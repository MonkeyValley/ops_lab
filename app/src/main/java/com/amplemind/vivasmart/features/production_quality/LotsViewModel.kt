package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.response.LotsResponseResult

class LotsViewModel(lot : LotsResponseResult) {
    val id = lot.id
    val business_unit_id =  lot.business_unit_id
    val code =  lot.code
    val crop_id =  lot.crop_id
    val cycle_id =  lot.cycle_id
    val lot =  lot.lot
}
