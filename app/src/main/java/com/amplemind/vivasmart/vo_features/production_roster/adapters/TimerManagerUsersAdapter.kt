package com.amplemind.vivasmart.vo_features.production_roster.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.databinding.ItemUsersTimerBinding
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.ActivityLogRecyclerViewModel
import javax.inject.Inject


class TimerManagerUsersAdapter @Inject constructor (private val mEventBus: EventBus)
    : BaseAdapter<ActivityLogModel, ActivityLogRecyclerViewModel, TimerUserViewHolder>() {


    private val mUuidViewModels = HashMap<String, ActivityLogRecyclerViewModel>()

    override fun buildViewModel(item: ActivityLogModel): ActivityLogRecyclerViewModel {
        val viewModel = ActivityLogRecyclerViewModel(item)
        mUuidViewModels[item.uuid] = viewModel
        return viewModel
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimerUserViewHolder {
        val binding = ItemUsersTimerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TimerUserViewHolder(binding, mEventBus)
    }

    override fun onBindViewHolder(viewHolder: TimerUserViewHolder, position: Int) {
        super.onBindViewHolder(viewHolder, position)

        getViewModel(position).updateTime()

    }

    override fun onDeletedViewModels(viewModels: List<ActivityLogRecyclerViewModel>) {
        super.onDeletedViewModels(viewModels)

        for (viewModel in viewModels) {
            mUuidViewModels.remove(viewModel.key)
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemKey(item: ActivityLogModel): String = item.uuid

    fun setRemoving(position: Int, isRemoving: Boolean) {
        mViewModels[position].isRemoving = isRemoving
    }

    fun isRemoving(position: Int): Boolean = mViewModels[position].isRemoving

    fun updateTimer(activityLogUuid: String) {
        mUuidViewModels[activityLogUuid]?.updateTime()
    }

    fun showMissingUnitsError(position: Int) {
        val viewModel = getViewModel(position)
        viewModel.showMissingUnitsError = true
        notifyItemChanged(position)

    }

}