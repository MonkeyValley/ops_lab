package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class ActivityModel(@SerializedName("name") val name: String, @SerializedName("description") val description: String)