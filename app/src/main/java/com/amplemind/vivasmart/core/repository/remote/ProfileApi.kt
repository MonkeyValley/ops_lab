package com.amplemind.vivasmart.core.repository.remote

import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.model.UserModel
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ProfileApi {

    // Upload image to Server
    @Multipart
    @POST("v2/upload")
    fun uploadImage(
            @Header("Authorization") authentication: String,
            @Part file: MultipartBody.Part,
            @Part("type") type: RequestBody
    ): Observable<ProfileRepository.UploadImageResponse>

    @PUT("v2/auth/update_image")
    @Headers("Content-Type: application/json")
    fun updateUserImage(
            @Header("Authorization") authentication: String,
            @Body body: ProfileRepository.UpdateUserImageRequest
    ): Observable<UserModel>

}