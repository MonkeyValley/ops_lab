package com.amplemind.vivasmart.features.root_quality.remote

import com.amplemind.vivasmart.features.root_quality.viewModel.ObjectResponseRootQuality
import com.amplemind.vivasmart.features.root_quality.viewModel.rootQualityModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path

interface rootQualityApi {

    //https://dev.vivasmart.com.mx/v2/fertigation/root_quality/11
    @GET("/v2/fertigation/root_quality/{lot_id}")
    @Headers("Content-Type: application/json")
    fun getRootQualityWeek(
            @Header("Authorization") authentication: String,
            @Path("lot_id")           lotId: Int
    ): Single<ObjectResponseRootQuality<rootQualityModel>>
}