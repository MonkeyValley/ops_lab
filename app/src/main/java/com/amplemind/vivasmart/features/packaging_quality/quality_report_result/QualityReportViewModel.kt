package com.amplemind.vivasmart.features.packaging_quality.quality_report_result

import com.amplemind.vivasmart.core.repository.response.QualityCarryOrderResponse
import javax.inject.Inject

class QualityReportViewModel @Inject constructor() {

    private lateinit var model: QualityCarryOrderResponse

    fun setupCleanReport(report: QualityCarryOrderResponse) {
        model = report
    }

    fun getIssues(): List<ItemQualityReportViewModel> {
        if (model.issues == null){
            return listOf()
        }
        return model.issues!!.map { ItemQualityReportViewModel(it, model.sampleSize) }
    }

    private fun validateProblems(numberProblems: Int): Int {
        return if (numberProblems > model.sampleSize) model.sampleSize else numberProblems
    }

    private fun getPercentFromProblem(numberProblems: Int): String {
        val problems = validateProblems(numberProblems)
        return "${(problems * 100) / model.sampleSize} %"
    }

    fun getTotalPercent(): String {
        return "${((model.export + model.permanentDefect + model.defectCondition) * 100) / model.sampleSize} %"
    }

    fun getSampleSize(): String {
        return model.sampleSize.toString()
    }

    fun getExportation(): String {
        return model.export.toString()
    }

    fun getExportationPercent(): String {
        return getPercentFromProblem(model.export)
    }

    fun getPermanentDamage(): String {
        return model.permanentDefect.toString()
    }

    fun getPermanentDamagePercent(): String {
        return getPercentFromProblem(model.permanentDefect)
    }

    fun getDefects(): String {
        return model.defectCondition.toString()
    }

    fun getDefectsPercent(): String {
        return getPercentFromProblem(model.defectCondition)
    }


}
