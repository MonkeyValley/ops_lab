package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm
import io.realm.FieldAttribute

class Version_2_10_5 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateCreateObjectModelPollinationModel(realm)
    }

    private fun migrateCreateObjectModelPollinationModel(realm: DynamicRealm){
        realm.schema
                .create("PollinationModel")
                .addField("stageId", Int::class.javaObjectType)
                .addField("lvl0", Int::class.javaObjectType)
                .addField("lvl1", Int::class.javaObjectType)
                .addField("lvl2", Int::class.javaObjectType)
                .addField("lvl3", Int::class.javaObjectType)
                .addField("porcentLvl0", Double::class.javaObjectType)
                .addField("porcentLvl1", Double::class.javaObjectType)
                .addField("porcentLvl2", Double::class.javaObjectType)
                .addField("porcentLvl3", Double::class.javaObjectType)
                .addField("pollinationPorcent", Double::class.javaObjectType)
                .addField("table", Int::class.javaObjectType)
                .addField("week", Int::class.javaObjectType)
                .addField("flowers", Int::class.javaObjectType)
                .addField("uuid", String::class.javaObjectType, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
    }

    override fun drop(realm: DynamicRealm) {

    }
}