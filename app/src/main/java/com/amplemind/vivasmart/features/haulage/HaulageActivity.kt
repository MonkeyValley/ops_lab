package com.amplemind.vivasmart.features.haulage


import android.os.Bundle
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.utils.STAGE_UUID
import com.amplemind.vivasmart.features.haulage.adapter.HaulageTabAdapter
import kotlinx.android.synthetic.main.activity_haulage.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.progress_track_component.*

class HaulageActivity : BaseActivityWithFragment() {

    companion object {
        val TAG = HaulageActivity::class.simpleName
        val PARAM_STAGE_UUID = "$TAG.StageUuid"
        val PARAM_LOT_NAME = "$TAG.LotName"
        val PRAM_STAGE_ID = "$TAG.StageId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_haulage)

        setupToolbar()

        STAGE_UUID = intent?.getStringExtra(PARAM_STAGE_UUID) ?: ""

        var stageId = intent?.getStringExtra(PRAM_STAGE_ID) ?: "0"

        lbl_track_title.text="/ Nomina / Producción / Labores Culturales / "+ intent?.getStringExtra(PARAM_LOT_NAME) + " / Orden de acarreo /"
        val fragmentAdapter = HaulageTabAdapter(supportFragmentManager, stageId.toInt())
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_haulage
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text =
                getString(R.string.haulage_title) + "\n" + intent?.getStringExtra(PARAM_LOT_NAME) ?: ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        finish()
    }

}
