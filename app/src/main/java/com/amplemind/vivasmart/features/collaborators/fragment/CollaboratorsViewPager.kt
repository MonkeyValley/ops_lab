package com.amplemind.vivasmart.features.collaborators.fragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.amplemind.vivasmart.core.repository.model.HeaderModel

class CollaboratorsViewPager(supportFragmentManager: FragmentManager, private val showActivities: Boolean, private val section : HeaderModel? = null) : FragmentPagerAdapter(supportFragmentManager) {

    private val titles = mutableListOf<String>()
    private val fragments = mutableListOf<Fragment>()


    init {
        createTitles()
        createFragments()
    }

    private fun createTitles() {
        titles.add("BASE DE DATOS")
    }

    private fun createFragments() {
        //fragments.add(CollaboratorsDataBaseFragment.newInstance(showActivities, section =  section))
    }

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = titles.size

    override fun getPageTitle(position: Int) = titles[position]

}
