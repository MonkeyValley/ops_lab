package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_15 : MigrationExecutable {
    override fun run(realm: DynamicRealm) {
        migrateUploadObjectModel(realm)
    }

    private fun migrateUploadObjectModel(realm: DynamicRealm){
        realm.schema.create("MaxPulseModel")?.apply {
            addField("lotId", Int::class.javaObjectType)
            addField("max", Int::class.javaObjectType)
            addField("date", String::class.javaObjectType)
        }

        realm.schema.get("PulseForTable")?.apply {
            addField("check", String::class.javaObjectType)
        }

        realm.schema.get("VarietyModel")?.apply {
            addField("stageId", Int::class.javaObjectType)
        }
    }

    override fun drop(realm: DynamicRealm) {

    }
}