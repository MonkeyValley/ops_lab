package com.amplemind.vivasmart.features.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.View

abstract class BaseRecyclerViewAdapter<T, VH: BaseRecyclerViewAdapter.BaseViewHolder<T>> : RecyclerView.Adapter<VH>() {

    protected lateinit var mData: List<T>

    override fun getItemCount(): Int {
        return if (::mData.isInitialized) mData.size else 0
    }

    fun setData(data: List<T>) {
        mData = data
        notifyDataSetChanged()
    }

    fun clearData() {
        val count = mData.size
        mData = listOf()
        notifyItemRangeRemoved(0, count)
    }

    override fun onBindViewHolder(viewHolder: VH, position: Int) {
        viewHolder.bind(mData[position])
    }

    abstract class BaseViewHolder<in T>(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: T)
    }

}