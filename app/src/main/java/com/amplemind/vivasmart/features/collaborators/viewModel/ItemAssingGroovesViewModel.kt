package com.amplemind.vivasmart.features.collaborators.viewModel

import android.util.Log
import com.amplemind.vivasmart.core.repository.model.AssingGroovesModel
import com.amplemind.vivasmart.core.utils.SELECT_GROOVE
import com.amplemind.vivasmart.features.collaborators.ManagerSelectedItems
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel

//class ItemAssingGroovesViewModel(val item: AssingGroovesModel, val idCollaborator: Int, val idActivityLog : Long) {
class ItemAssingGroovesViewModel(val item: AssingGroovesModel, val activityLog: ActivityLogModel) {

    val id = item.grooveNo
    val name = item.name
    val status = item.status
    val table = item.table
    var position : Int? = null
    var state = if (isActive()) SELECT_GROOVE else item.state

    var selected = false

    var state_animation = if (item.animation_status == null)  null else ManagerSelectedItems.ANIMATION_STATUS.from(item.animation_status!!)

    fun isActive() : Boolean {
        if (item.activityLog?.collaboratorUuid == activityLog.collaboratorUuid) {
            Log.d("GROOVE", item.activityLog.toString())
        }

        Log.d("GROOVE", "${activityLog.sign == "No Sign" && activityLog.uuid == item.groove.activityLogUuid}")

        return activityLog.sign == "No Sign" && activityLog.uuid == item.groove.activityLogUuid
    }

    fun setPosition(position : Int) {
        this.position = position
    }

}