package com.amplemind.vivasmart.vo_core.repository.models.realm


data class PendingLogsModel (
        val collaborator: CollaboratorModel,
        val stage: StageModel,
        var payment: Double
)