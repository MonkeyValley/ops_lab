package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class GeneralQualityScoreModel (
    @SerializedName("cat_id") val catId: Int,
    @SerializedName("cat_name") val catName: String,
    @SerializedName("score") val score: Double
)