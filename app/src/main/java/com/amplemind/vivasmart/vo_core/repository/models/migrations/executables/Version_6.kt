package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm
import io.realm.FieldAttribute

class Version_6 : MigrationExecutable {
    override fun run(realm: DynamicRealm) {
        alterGrooveModel(realm)
        alterUploadObjectModel(realm)
    }

    override fun drop(realm: DynamicRealm) {
        revertGrooveModel(realm)
        revertUploadObjectModel(realm)
    }

    private fun alterGrooveModel(realm: DynamicRealm) {
        realm.schema.get("GrooveModel")
                ?.setRequired("qualityComment", false)
    }

    private fun revertGrooveModel(realm: DynamicRealm) {
        realm.schema.get("GrooveModel")
                ?.setRequired("qualityComment", true)
    }

    private fun alterUploadObjectModel(realm: DynamicRealm) {
        realm.schema.get("UploadObjectModel")
                ?.removePrimaryKey()
                ?.removeField("marked")
                ?.removeField("divisible")
                ?.removeField("isBulk")
                ?.removeField("id")
                ?.addField("errorServerResponse", String::class.java)
                ?.addField("errorCode", String::class.java)
                ?.addField("uuid", String::class.java, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
    }

    private fun revertUploadObjectModel(realm: DynamicRealm) {
        realm.schema.get("UploadObjectModel")
                ?.removePrimaryKey()
                ?.removeField("errorServerResponse")
                ?.removeField("errorCode")
                ?.removeField("uuid")
                ?.addField("marked", Boolean::class.java)
                ?.addField("divisible", Boolean::class.java)
                ?.addField("isBulk", Boolean::class.java)
                ?.addField("id", String::class.java, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
    }

}