package com.amplemind.vivasmart.features.crop_validation.adapter

import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.disable
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.extensions.setColorEnable
import com.amplemind.vivasmart.databinding.ItemIssuesCropValidationBinding
import com.amplemind.vivasmart.features.crop_validation.viewmodel.ItemCropIssuesViewModel
import androidx.databinding.library.baseAdapters.BR
import com.jakewharton.rxbinding2.widget.checked
import io.reactivex.subjects.BehaviorSubject

class CropIssuesValidationAdapter constructor(val list: MutableList<ItemCropIssuesViewModel>, val has_harvest_quality: Boolean) : RecyclerView.Adapter<CropIssuesValidationAdapter.CropIssuesValidationViewHolder>() {

    private val onClickSubject = BehaviorSubject.create<ItemCropIssuesViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CropIssuesValidationViewHolder {
        val binding = ItemIssuesCropValidationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CropIssuesValidationViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CropIssuesValidationViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<ItemCropIssuesViewModel> {
        return onClickSubject
    }

    fun isEmpty() = list.isEmpty()


    inner class CropIssuesValidationViewHolder(private val binding: ItemIssuesCropValidationBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemCropIssuesViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.TVCriteria.setOnClickListener {
                    onClickSubject.onNext(item)
                }
                binding.SWissue.setOnCheckedChangeListener { _, isChecked ->
                    item.isChecked = isChecked
                }
                if(has_harvest_quality){
                    binding.SWissue.isClickable = false
                }
            }
        }

    }

}