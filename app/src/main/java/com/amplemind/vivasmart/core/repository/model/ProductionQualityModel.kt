package com.amplemind.vivasmart.core.repository.model

data class ProductionQualityModel(val name: String, val complete: Boolean)