package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.production_roster.fragments.LotInfoDialogFragment
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.PayrollActivitiesViewModel
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.content_payroll_choose_activity_fragment.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

class ChooseActivityFragment : BaseFragment(), ActivitiesChooseAdapter.Listener {

    @Inject
    lateinit var viewModel: PayrollActivitiesViewModel

    private lateinit var mAdapter: ActivitiesChooseAdapter

    override var argumentsNeeded: Boolean = true

    private lateinit var mStage: StageModel

    private var mTitle: String = ""

    private var mListener: Listener? = null

    companion object {
        val TAG: String = ChooseActivityFragment::class.java.simpleName

        val PARAM_STAGE_UUID = "$TAG.StageUuid"
        val PARAM_CROP_ID = "$TAG.CropId"
        val PRAM_STAGE_TYPE = "$TAG.StageType"
        lateinit var stageModel: StageModel
        fun newInstance(stageUuid: String, cropId: Long, stage: StageModel,
                        productionStageName: String) =
                ChooseActivityFragment().apply {
            arguments = Bundle().apply {
                putString(PARAM_STAGE_UUID, stageUuid)
                putLong(PARAM_CROP_ID, cropId)
                putString(PRAM_STAGE_TYPE, productionStageName)
                stageModel = stage
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        Log.e(TAG," ----------------")
        val view = inflater.inflate(R.layout.content_payroll_choose_activity_fragment, container, false)
        val txtTitle = view.findViewById(R.id.payrol_txt_info) as TextView

        txtTitle.setOnClickListener {
            showStageInfoDialog(stageModel)
        }

        return view
    }
    fun getStageId(): Int {
        return arguments?.getInt("stage_id", -1) ?: -1
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.cleanUp()
    }

    override fun readArguments(args: Bundle) {
        viewModel.stageUuid = requireNotNull(args.getString(PARAM_STAGE_UUID)) { "PARAM_STAGE_UUID is required" }
        viewModel.cropId = requireNotNull(args.getLong(PARAM_CROP_ID)) { "PARAM_CROP_ID is required" }
        viewModel.productionStageName = requireNotNull(args.getString(PRAM_STAGE_TYPE)) { "" }
        viewModel.setStageType()
    }

    override fun loadData() {
        super.loadData()
        viewModel.loadStage().subscribe(this::onStageLoaded).addTo(subscriptions)
        viewModel.loadActivities().subscribe(this::onActivitiesLoaded).addTo(subscriptions)
    }

    private fun onStageLoaded(stage: StageModel) {
        mTitle = stage.lot?.name ?: getString(R.string.caf_default_title)
        setUpToolbar()
    }

    private fun setUpToolbar() {
        lbl_track_title.text = "/ Nómina / Producción / "+ viewModel.productionStageName  +" / "+mTitle + " /"
//        lbl_track_title.setTextColor(this.resources.getColor(R.color.white))
//        icon_track_title.setColorFilter( this.resources.getColor( R.color.white))
        sendSetToolbarTitleEvent(getString(R.string.caf_subtitle), mTitle)
    }

    override fun setUpUI() {
        super.setUpUI()
        initRecycler()
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()

        RxView.clicks(btn_ready)
                .subscribe {
                    val intent = Intent(context, ReportRosterActivity::class.java)
                            .putExtra(ReportRosterActivity.PARAM_STAGE_UUID, viewModel.stageUuid)
                            .putExtra("nameStage", viewModel.productionStageName)
                            .putExtra(ReportRosterActivity.PRAM_STAGE_TYPE_ID, viewModel.stageTypeId)
                    startActivityForResult(intent, 200)
                }
                .addTo(subscriptions)

        mAdapter.onActivityClicked.subscribe(this::onActivityClicked).addTo(subscriptions)
    }

    private fun noActivities(isEmpty: Boolean) {
        if (isEmpty) {
            no_found_activities.visibility = View.VISIBLE
            btn_ready.visibility = View.GONE
        } else {
            no_found_activities.visibility = View.GONE
            btn_ready.visibility = View.VISIBLE
        }
    }

    private fun onActivitiesLoaded(activities: Changes<ActivityModel>) {
        noActivities(activities.data.isEmpty())
        mAdapter.setItems(activities)
    }

    private fun initRecycler() {
        rv_chosse_activity.hasFixedSize()
        rv_chosse_activity.layoutManager = GridLayoutManager(context, 3)
        rv_chosse_activity.itemAnimator = DefaultItemAnimator()
        rv_chosse_activity.setPadding(0, 8.toPx(), 0, 70.toPx())
        rv_chosse_activity.clipToPadding = false

        mAdapter = ActivitiesChooseAdapter(context!!, viewModel.stageUuid)

        rv_chosse_activity.adapter = mAdapter
    }

    override fun onResume() {
        super.onResume()
        setUpToolbar()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null && data!!.extras!!.getInt("code") == 200) {
            fragmentManager!!.popBackStack()
        }
    }

    override fun onGetCounterValue(activity: ActivityModel, position: Int): Observable<Int>? {
        return mListener?.onGetCounterValue(mStage, activity, position)
    }

    interface Listener {
        fun onGetCounterValue(stage: StageModel, activity: ActivityModel, position: Int): Observable<Int>?
    }

    private fun onActivityClicked(activity: ActivityModel) =
            mEventBus.send(ActivitySelectedEvent(viewModel.stageUuid, activity.uuid))

    private fun showStageInfoDialog(stage: StageModel) {
        val dialog = LotInfoDialogFragment.newInstance(stage)

        dialog.show((context as AppCompatActivity).supportFragmentManager, LotInfoDialogFragment.TAG)
        dialog.setChangeListenner(object : LotInfoDialogFragment.OnChangeUnit {
            override fun cancelEdit() {
                if (dialog.context != null) {
                    hideKeyboard(dialog.context!!)
                }
                dialog.dismiss()
            }

            override fun changeUnit(new_value: Int, previous_value: Int) {
            }
        })
    }


    class ActivitySelectedEvent(
            val stageUuid: String,
            val activityUuid: String
    )



}
