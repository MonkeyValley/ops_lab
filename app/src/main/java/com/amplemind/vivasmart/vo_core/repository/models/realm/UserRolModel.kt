package com.amplemind.vivasmart.vo_core.repository.models.realm

import androidx.room.PrimaryKey
import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.getUUID
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class UserRolModel (
        @PrimaryKey @SerializedName("id") var id: Int? = null,
        @SerializedName("name") var name: String? = null,
        @SerializedName("role") var role: AppUserRolModel? = null,
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class AppUserRolModel(
        @PrimaryKey @SerializedName("id") var roleId: Int? = null,
        @SerializedName("name") var roleName: String? = null,
        @SerializedName("app_modules") var appModules: RealmList<AppModule>? = RealmList(),
        @SerializedName("uuid") override var uuid: String = getUUID()
) : RealmObject(), Identifiable

open class AppModule(
        @PrimaryKey @SerializedName("module_id") var moduleId: Int? = null,
        @SerializedName("module_name") var moduleName: String? = null,
        @SerializedName("has_access") var hasAccess: Boolean? = false,
        @SerializedName("uuid") override var uuid: String = getUUID()
): RealmObject(), Identifiable