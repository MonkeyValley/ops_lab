package com.amplemind.vivasmart.features.packaging_quality.clean_report

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment

/**
 * Created by
 *      amplemind on 7/11/18.
 */
class SendReportDialogFragment : BaseDialogFragment(){

    fun newInstance(): SendReportDialogFragment {
        return SendReportDialogFragment()
    }

    private var eventListener : OnClickListenner? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_send_report_dialog, null, false)
        builder.setView(mView)


        mView.findViewById<TextView>(R.id.return_problem).setOnClickListener {
            eventListener!!.returnCard()
        }

        mView.findViewById<TextView>(R.id.send_data).setOnClickListener {
            eventListener!!.sendCards()
        }


        return builder.create()
    }

    fun setClickListener(listener : OnClickListenner) {
        eventListener = listener
    }


    interface OnClickListenner{
        fun returnCard()
        fun sendCards()
    }

}