package com.amplemind.vivasmart.features.sync_forced.viewModel

import android.util.Log
import android.widget.Toast
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.utils.ACTIVITY_CONTEXT
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.pollination.repository.responses.Pollinationrevisions
import com.amplemind.vivasmart.features.sync_forced.SyncForcedActivity
import com.amplemind.vivasmart.features.sync_forced.repository.SyncForcedRepository
import com.amplemind.vivasmart.features.sync_forced.service.SyncForcedService
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.jetbrains.anko.runOnUiThread
import javax.inject.Inject

class SyncForcedViewModel @Inject constructor(
        private val repository: SyncForcedRepository,
        private val apiErrors: HandleApiErrors,
        private val service: SyncForcedService) : BaseViewModel() {

    lateinit var activity: SyncForcedActivity

    var validateStageDownload = false

    private val requestCollaboratorSuccess = BehaviorSubject.create<List<CollaboratorModel>>()
    private val requestActivitySuccess = BehaviorSubject.create<List<ActivityModel>>()
    private val requestStageSuccess = BehaviorSubject.create<List<StageModel>>()
    private val requestPlagueSuccess = BehaviorSubject.create<List<PlagueDetail>>()
    private val requestFertirriegoSuccess = BehaviorSubject.create<List<LotFertirriegoModel>>()
    private val requestProductionRangesSuccess = BehaviorSubject.create<List<ProductionRangeVariatiesModel>>()

    fun getColaboratorForced(): Disposable =
            repository.getCollaboratorsForced(service.getCollaboratorLastVersion())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Colaboradores...") }
                    .doOnTerminate { activity.setStatus("Guardando Colaboradores...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        requestCollaboratorSuccess.onNext(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("CollaboratosForced", "error", it)
                    })

    fun onSuccessCollaboratorRequest(): BehaviorSubject<List<CollaboratorModel>> = requestCollaboratorSuccess
    fun uploadCollaborator(list: List<CollaboratorModel>): Boolean = service.uploadCollaborator(list)

    fun getActivitiesForced(): Disposable =
            repository.getActivitiesForced(service.getActivitiesLastVersion())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Actividades...") }
                    .doOnTerminate { activity.setStatus("Guardando Actividades...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        requestActivitySuccess.onNext(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("ActivitiesForced", "error", it)
                    })

    fun onSuccessActivituesRequest(): BehaviorSubject<List<ActivityModel>> = requestActivitySuccess
    fun uploadActivity(list: List<ActivityModel>): Boolean = service.uploadActivity(list)

    fun getPlagueForced(): Disposable =
            repository.getPlaguesForced(service.getPlagueLastVersion())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Plagas...") }
                    .doOnTerminate { activity.setStatus("Guardando Plagas...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        requestPlagueSuccess.onNext(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("PlagueForced", "error", it)
                    })

    fun onSuccessPlagueRequest(): BehaviorSubject<List<PlagueDetail>> = requestPlagueSuccess
    fun uploadPlague(list: List<PlagueDetail>) {
        activity.setStatus("Guardando Plagas...")
        list.forEach { plagueDetail ->
            service.uploadPlague(plagueDetail)
        }
        activity.finishSyncForced()
    }

    fun getStageForced(): Disposable =
            repository.getStagesForced(service.getStageLastVersion())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Etapas...") }
                    .doOnTerminate { activity.setStatus("Guardando Etapas...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        requestStageSuccess.onNext(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("StageForced", "error", it)
                    })

    fun onSuccessSatgeRequest(): BehaviorSubject<List<StageModel>> = requestStageSuccess
    fun uploadStage(list: List<StageModel>) {
        service.uploadStage(list)
        if (validateStageDownload) {
            for (item in list) {
                getActivityCodeForced(item.id.toLong())
            }
        }
    }

    fun deletePollination() = service.deletePollination()

    fun getPollinationForced(lotId: Int): Disposable =
            repository.getDataForPollination(lotId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Polinización...") }
                    .doOnTerminate { activity.setStatus("Guardando Polinización...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .map { mapModelToViewModels(it) }
                    .subscribe({
                        service.insertPollinationData(it)
                    }, {
                        Log.e("PollinationModel", "error", it)
                    })

    private fun mapModelToViewModels(response: List<Pollinationrevisions>): List<PollinationModel> {
        val list = arrayListOf<PollinationModel>()

        response.forEach { pollination ->
            pollination.revision.forEach { revision ->
                list.add(revision)
            }
        }

        return list
    }

    private fun getActivityCodeForced(stageId: Long): Disposable =
            repository.getCodesForced(service.getActivityCodeLastVersion(), stageId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Códigos de actividad...") }
                    .doOnTerminate { activity.setStatus("Guardando Códigos de actividad...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({

                        //service.uploadActivityCode(it)

                        for (item in it) {
                            getActivityLogForced(item.id)
                            getGrooveForced(item.id)
                        }

                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("ActivityCodeForced", "error", it)
                    })

    private fun getActivityLogForced(activityCodeId: Long): Disposable =
            repository.getActivityLogsForCodeForced(service.getActivityLogLastVersion(), activityCodeId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Bitácoras de actividades...") }
                    .doOnTerminate { activity.setStatus("Guardando Bitácoras de actividades...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        service.uploadActivityLog(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("ActivityLogForced", "error", it)
                    })

    private fun getGrooveForced(activityCodeId: Long): Disposable =
            repository.getGroovesForced(activityCodeId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Surcos...") }
                    .doOnTerminate { activity.setStatus("Guardando Surcos...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        service.uploadGroove(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("GrooveForced", "error", it)
                    })


    fun getFertirriegoForced(): Disposable =
            repository.getLotForFertirriego()
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Valvulas de suelo...") }
                    .doOnTerminate { activity.setStatus("Guardando Valvulas de suelo...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        requestFertirriegoSuccess.onNext(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("GrooveForced", "error", it)
                    })

    fun onSuccessFertirriegoRequest(): BehaviorSubject<List<LotFertirriegoModel>> = requestFertirriegoSuccess
    fun uploadFertirriego(list: List<LotFertirriegoModel>): Boolean = service.uploadFertirriegoLots(list)

    fun getProductionRangesForced(): Disposable =
            repository.getProductionRanges()
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Rangos de Produccion...") }
                    .doOnTerminate { activity.setStatus("Guardando Rangos de Produccion...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        requestProductionRangesSuccess.onNext(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("GrooveForced", "error", it)
                    })

    fun onSuccessProductionRangesRequest(): BehaviorSubject<List<ProductionRangeVariatiesModel>> = requestProductionRangesSuccess
    fun uploadProductionRanges(list: List<ProductionRangeVariatiesModel>): Boolean = service.uploadProductionRanges(list)

    fun moduleHasAccess(moduleId: Int): Boolean = service.moduleHasAccess(moduleId)


    fun getLots() = service.getLots()


    fun getActivitiesBackground(): Disposable =
            repository.getActivitiesForced(service.getActivitiesLastVersion())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { showToast("Descargando Actividades...") }
                    .doOnTerminate { showToast("Guardando Actividades...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        uploadActivityBackground(it)
                    }, {
                        Log.e("ActivitiesBackground", "error", it)
                    })

    fun uploadActivityBackground(list: List<ActivityModel>): Boolean = service.uploadActivity(list)

    fun showToast(msj: String) {
        ACTIVITY_CONTEXT!!.runOnUiThread {
            Toast.makeText(ACTIVITY_CONTEXT, msj, Toast.LENGTH_SHORT).show()
        }
    }

    fun deleteActivity(uuid: String?) {
        service.deleteActivity(uuid)
        showToast("Actividad eliminada")
    }

    fun getStageBackground(): Disposable =
            repository.getStagesForced(service.getStageLastVersion())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { showToast("Descargando Etapas...") }
                    .doOnTerminate { showToast("Guardando Etapas...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        Log.e("Stage-size", it.size.toString())
                        uploadStageBackground(it)
                    }, {
                        Log.e("StageBackground", "error", it)
                    })

    fun uploadStageBackground(list: List<StageModel>) {
        service.uploadStage(list)
        for (item in list) {
            getActivityCodeBackground(item.id.toLong())
        }
    }

    private fun getActivityCodeBackground(stageId: Long): Disposable =
            repository.getCodesForced(service.getActivityCodeLastVersion(), stageId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { showToast("Descargando Códigos de actividad...") }
                    .doOnTerminate { showToast("Guardando Códigos de actividad...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({

                        //service.uploadActivityCode(it)

                        for (item in it) {
                            getActivityLogBackground(item.id)
                            getGrooveBackground(item.id)
                        }

                    }, {
                        Log.e("ActivityCodeBackground", "error", it)
                    })

    private fun getActivityLogBackground(activityCodeId: Long): Disposable =
            repository.getActivityLogsForCodeForced(service.getActivityLogLastVersion(), activityCodeId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { showToast("Descargando Bitácoras de actividades...") }
                    .doOnTerminate { showToast("Guardando Bitácoras de actividades...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        service.uploadActivityLog(it)
                    }, {
                        Log.e("ActivityLogBackground", "error", it)
                    })

    private fun getGrooveBackground(activityCodeId: Long): Disposable =
            repository.getGroovesForced(activityCodeId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { showToast("Descargando Surcos...") }
                    .doOnTerminate { showToast("Guardando Surcos...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        service.uploadGroove(it)
                    }, {
                        Log.e("GrooveBackground", "error", it)
                    })

    fun getDailyPay() : Disposable =
                repository.getDailyPay(service.getDailyPayLastVersion())
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { showToast("Descargando datos de depósito diario...") }
                        .doOnTerminate { showToast("Guardando datos de depósito diario...") }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            Log.e("DailyPay-size", it.size.toString())
                            service.saveDailyPay(it)
                        }, {
                            Log.e("DailyPay", "error", it)
                        })


    fun getStageForProductionRangeForced(): Disposable =
            repository.getStageForProductionRangeForced(service.getStageLastVersion())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe { activity.setStatus("Descargando Etapas...") }
                    .doOnTerminate { activity.setStatus("Guardando Etapas...") }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        requestStageSuccess.onNext(it)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                        Log.e("StageForced", "error", it)
                    })

}
