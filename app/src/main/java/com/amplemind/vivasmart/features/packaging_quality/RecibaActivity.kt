package com.amplemind.vivasmart.features.packaging_quality

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.packaging_quality.viewModel.CarryOrderCondensedViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.RecibaViewModel
import kotlinx.android.synthetic.main.activity_reciba.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

class RecibaActivity : BaseActivity() {

    private val TAG = PackagingQualityActivity::class.java.simpleName

    @Inject
    lateinit var viewModel : RecibaViewModel

    private val SIGN_RECIBA = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reciba)

        setupToolbar()

        initRecycler()

        viewModel.getList().subscribe(this::setData).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
       // if (isOnlineMode(this)) {
            viewModel.getRecibaList().addTo(subscriptions)
        /*
        } else {
            root_activity_reciba.snackWithoutInternetMessage()
            mViewModel.getLocalRecibaList().addTo(subscriptions)
        }
        */
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_reciba, menu)
        return true
    }

    /**
     *  call new activity detail orden
     */
    private fun clickItemList(item : CarryOrderCondensedViewModel) {
        startActivityForResult(Intent(this, RecibaDetailActivity::class.java)
                .putExtra("id", item.id), SIGN_RECIBA)
    }

    /**
     *  get data and asign click in mItems
     */
    fun setData(data : List<CarryOrderCondensedViewModel>){
        (rv_quality.adapter as QualityAdapter).addElements(data)
        (rv_quality.adapter as QualityAdapter).getClickItem().subscribe(this::clickItemList).addTo(subscriptions)

        showImageNoData(data.isEmpty())
    }

    fun showImageNoData(visible : Boolean){
        no_found_activities.visibility = if(visible) View.VISIBLE else View.GONE
    }

    /**
     *  create adapter for recyclerview
     */
    private fun initRecycler() {
        rv_quality.hasFixedSize()
        rv_quality.layoutManager = LinearLayoutManager(this)
        rv_quality.itemAnimator = DefaultItemAnimator()
        rv_quality.adapter = QualityAdapter()
    }

    /**
     *  change title in toolbar
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_package
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Reciba"
        lbl_track_title.text = "/ Operaciones / Reciba /"
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.menu_report -> {
                startActivity(Intent(this, RecibaReportActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SIGN_RECIBA && resultCode == Activity.RESULT_OK) {
            viewModel.removeItem(data!!.getIntExtra("id", 0))
            (rv_quality.adapter as QualityAdapter).notifyDataSetChanged()
            showImageNoData((rv_quality.adapter as QualityAdapter).isEmpty())
        }
    }
}
