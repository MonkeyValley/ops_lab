package com.amplemind.vivasmart.features.production_roster.viewModels

import com.amplemind.vivasmart.vo_core.repository.models.realm.DailyReportModel
import com.amplemind.vivasmart.vo_core.utils.formatPayment

class ItemDailyReportViewModel(val model: DailyReportModel) {

    val code: String
        get() = model.code

    val name: String
        get() = model.name

    val total: String
        get() = formatPayment(model.total)
}