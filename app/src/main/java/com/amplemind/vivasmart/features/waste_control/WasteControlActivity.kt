package com.amplemind.vivasmart.features.waste_control

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hasInternet
import com.amplemind.vivasmart.features.waste_control.Adapters.WasteControlAdapter
import com.amplemind.vivasmart.features.waste_control.models.WasteControlItemViewModel
import com.amplemind.vivasmart.features.waste_control.viewModel.WasteControlViewModel
import com.github.badoualy.datepicker.DatePickerTimeline
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_report_quality_finished_product.*
import kotlinx.android.synthetic.main.activity_waste_control.*
import kotlinx.android.synthetic.main.activity_waste_control.btn_addReport
import kotlinx.android.synthetic.main.activity_waste_control.fab_menu
import kotlinx.android.synthetic.main.activity_waste_control.timeline
import kotlinx.android.synthetic.main.progress_track_component.*
import java.util.*
import javax.inject.Inject

class WasteControlActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: WasteControlViewModel
    var fecha: String? = null

    companion object {
        const val TAG = "WasteControlActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waste_control)

        setupToolbar()
        addSubscribers()

        setupButton()
        loadDateTime()

        loadReportList()
    }

    fun ifConected(): Boolean {
        return if (hasInternet())
            true
        else {
            Toast.makeText(this, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
            false
        }
    }

    private fun loadReportList() {
        viewModel.getList(fecha!!)
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_waste_control
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.waste_control_title)
        lbl_track_title.text = "/ Calidad / Empaque / Control de desperdicio /"
        lbl_track_title.setTextColor(this.resources.getColor(R.color.white))
        icon_track_title.setColorFilter(this.resources.getColor(R.color.white))
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest().subscribe(this::updateList).addTo(subscriptions)
    }

    fun setupButton() {
        btn_addReport.setOnClickListener {
            if (ifConected()) {
                intent = Intent(this, WasteControlTestActivity::class.java)
                intent.putExtra("crop", "")
                startActivityForResult(intent, 200)
            }
            fab_menu.collapse()
        }
    }

    private fun loadDateTime() {
        val date = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        var cal_begin = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        cal_begin.add(Calendar.DATE, -15)

        Log.e("date", "" + date.get(Calendar.DST_OFFSET))
        Log.e("cal_begin", "" + date.get(Calendar.DST_OFFSET))

        if(date.get(Calendar.DST_OFFSET) != 0){
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) +"-"+ cal_begin.get(Calendar.MONTH)+"-"+cal_begin.get(Calendar.DAY_OF_MONTH)+1)
            timeline.setLastVisibleDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH)+1)
            timeline.setSelectedDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH)+1)
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH)+1)
        } else {
            timeline.setFirstVisibleDate(cal_begin.get(Calendar.YEAR), cal_begin.get(Calendar.MONTH), cal_begin.get(Calendar.DAY_OF_MONTH))
            Log.e("setFirstVisibleDate", "" + cal_begin.get(Calendar.YEAR) +"-"+ cal_begin.get(Calendar.MONTH)+"-"+cal_begin.get(Calendar.DAY_OF_MONTH))
            timeline.setLastVisibleDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setLastVisibleDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH))
            timeline.setSelectedDate( date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH))
            Log.e("setSelectedDate", "" + date.get(Calendar.YEAR) +"-"+ date.get(Calendar.MONTH)+"-"+date.get(Calendar.DAY_OF_MONTH))
        }

        var sti = date.get(Calendar.YEAR).toString()
        fecha = ((date.get(Calendar.YEAR)).toString() + "-" + (date.get(Calendar.MONTH) + 1).toString() + "-" + (date.get(Calendar.DAY_OF_MONTH)).toString())

        val stringYear = sti.subSequence(2, 4)
        timeline.setDateLabelAdapter { calendar,
                                       index ->
            Integer.toString(calendar[Calendar.MONTH] + 1) + "/" + stringYear
        }
        timeline.onDateSelectedListener = DatePickerTimeline.OnDateSelectedListener { year, month, day, index ->
            Log.d("dateSelected", "year:$year month:$month day:$day index: $index")
            fecha = ((year).toString() + "-" + (month + 1).toString() + "-" + (day).toString())
            if (ifConected()) {
                loadReportList()
            }
        }
    }

    private fun updateList(list: List<WasteControlItemViewModel>) {
        Log.d("lista", Gson().toJson(list))
        val adapter = WasteControlAdapter(list.toMutableList())
        rv_wasted_control.hasFixedSize()
        rv_wasted_control.layoutManager = LinearLayoutManager(this)
        rv_wasted_control.itemAnimator = DefaultItemAnimator()
        rv_wasted_control.adapter = adapter

        adapter.onClick().subscribe(this::onClickItem).addTo(subscriptions)
    }

    private fun onClickItem(item: WasteControlItemViewModel) {
        if (ifConected()) {
            intent = Intent(this, WasteControlTestActivity::class.java)
            intent.putExtra("crop", Gson().toJson(item))
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d("requestCode", requestCode.toString())
        Log.d("resultCode", resultCode.toString())

        if (requestCode == 200) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("wasteControl", data!!.getStringExtra("wasteControlItemModel"))
                loadReportList()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
