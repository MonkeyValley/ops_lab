package com.amplemind.vivasmart.features.production_roster


import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_roster.adapters.CropReportAdapter
import com.amplemind.vivasmart.features.production_roster.viewModels.CropReportViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.ItemCropReportViewModel
import kotlinx.android.synthetic.main.activity_crop_report.*
import javax.inject.Inject

class CropReportActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: CropReportViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crop_report)
        setupToolbar()
        setupObservers()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_crop
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Reporte de cosecha"
    }

    private fun setupObservers() {
        viewModel.onError().subscribe(this::onError).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onRequestSuccess().subscribe(this::onRequestSuccess).addTo(subscriptions)
        //viewModel.getReport().addTo(subscriptions)
    }

    private fun onRequestSuccess(items: List<ItemCropReportViewModel>) {
        setupRecycleView(items)
        tv_difference.text = viewModel.getDifference()
        tv_send.text = viewModel.getTotalSend()
        tv_total.text = viewModel.getTotalRecollection()
    }

    private fun setupRecycleView(items: List<ItemCropReportViewModel>) {
        rv_crop_report.hasFixedSize()
        rv_crop_report.layoutManager = LinearLayoutManager(this)
        rv_crop_report.itemAnimator = DefaultItemAnimator()
        rv_crop_report.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        rv_crop_report.adapter = CropReportAdapter(items)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
