package com.amplemind.vivasmart.core.repository.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CollaboratorsListModel(@SerializedName("id") val id : Int,
                                  @SerializedName("name") val name : String,
                                  @SerializedName("image") val image : String?,
                                  @SerializedName("employee_code") val employeeCode: String,
                                  @SerializedName("birthdate") val birthdate: String?): Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readString(),
            parcel.readString()!!,
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(image)
        parcel.writeString(employeeCode)
        parcel.writeString(birthdate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CollaboratorsListModel> {
        override fun createFromParcel(parcel: Parcel): CollaboratorsListModel {
            return CollaboratorsListModel(parcel)
        }

        override fun newArray(size: Int): Array<CollaboratorsListModel?> {
            return arrayOfNulls(size)
        }
    }


}