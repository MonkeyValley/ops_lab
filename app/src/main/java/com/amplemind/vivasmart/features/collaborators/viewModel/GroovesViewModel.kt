package com.amplemind.vivasmart.features.collaborators.viewModel

import com.amplemind.vivasmart.core.repository.ActivityCodeRepository
import com.amplemind.vivasmart.core.repository.GroovesRepository
import com.amplemind.vivasmart.core.repository.model.AssingGroovesModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class GroovesViewModel @Inject constructor(private var repository: GroovesRepository
                                                    , private val apiErrors: HandleApiErrors) {

    private var groovesSubject = BehaviorSubject.create<List<ItemAssingGroovesViewModel>>()


    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()

    private val responseId = BehaviorSubject.create<Pair<Long, Int>>()

    private val updateFinish = BehaviorSubject.create<Boolean>()

    fun getGroovesData(): BehaviorSubject<List<ItemAssingGroovesViewModel>> {
        return groovesSubject
    }

    fun sendGrooves(token: String, collaboratorId: Int, activityCode: Int,
                    groovesSelected: List<ActivityCodeRepository.GroovesIds>, table: Int, currentDate: String): Disposable {

        val request = ActivityCodeRepository.FinishCollaboratorGrooveRequest(
                collaboratorId, activityCode, 0, groovesSelected,
                table, 0, currentDate, false, null)

        return repository.finishCollaboratorGroove(token, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    responseId.onNext(Pair(it.id, it.collaborator_id))
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onResponseID(): BehaviorSubject<Pair<Long, Int>> {
        return responseId
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun createGrooves(data: ArrayList<AssingGroovesModel>, activityLog: ActivityLogModel) {
        val grooves = mutableListOf<ItemAssingGroovesViewModel>()
        data.forEach {
            grooves.add(ItemAssingGroovesViewModel(it, activityLog))
        }
        groovesSubject.onNext(grooves)
    }

    fun onFinishUpdate(): BehaviorSubject<Boolean> {
        return updateFinish
    }

}
