package com.amplemind.vivasmart.features.packaging_quality.fragment


import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.features.packaging_quality.adapters.ReceptionQualityRevisionAdapter
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ItemReceptionQualityRevisionViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ReceptionQualityRevisionListViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityCarryOrderModel
import com.jakewharton.rxbinding3.view.clicks
import kotlinx.android.synthetic.main.fragment_reception_quality_revision_list.*
import javax.inject.Inject

class ReceptionQualityRevisionListFragment : BaseFragment() {

    companion object {

        private val TAG: String = ReceptionQualityRevisionListFragment::class.java.simpleName

        private val PARAM_CARRY_ORDER_ID = "$TAG.CarryOrderId"

        fun newInstance(carryOrder: ReceptionQualityCarryOrderModel) =
                ReceptionQualityRevisionListFragment().apply {
                    arguments = Bundle().apply {
                        putLong(PARAM_CARRY_ORDER_ID, carryOrder.id)
                    }
                }

    }

    @Inject
    lateinit var mViewModel: ReceptionQualityRevisionListViewModel

    private lateinit var mAdapter: ReceptionQualityRevisionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.carryOrderId = arguments?.getLong(PARAM_CARRY_ORDER_ID)
                ?: throw IllegalArgumentException("$TAG: $PARAM_CARRY_ORDER_ID is required!")

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reception_quality_revision_list, container, false)
    }

    override fun setUpUICallbacks() {
        super.setUpUICallbacks()

        btn_create.clicks()
                .subscribe{
                    mEventBus.send(OnCreateRevisionClickedEvent(
                            this,
                            mViewModel.carryOrderId,
                            mViewModel.carryOrder
                    ))
                }
                .addTo(subscriptions)
    }

    override fun setUpUI() {
        super.setUpUI()
        sendSetToolbarTitleEvent(getString(R.string.rqrlf_subtitle))
        setUpRecyclerView()
    }

    override fun loadData() {
        super.loadData()
        mViewModel.loadCarryOrder().subscribe(this::onCarryOrderLoaded).addTo(subscriptions)
        mViewModel.loadRevisions().subscribe(this::onRevisionsLoaded).addTo(subscriptions)
    }

    private fun onCarryOrderLoaded(carryOrder: CarryOrderModel) {
        tv_carry_order_info.text = getString(R.string.rqrlf_carry_order_info_format)
                .format(carryOrder.lot?.name, carryOrder.crop?.name)
    }

    private fun onRevisionsLoaded(revisions: List<ItemReceptionQualityRevisionViewModel>) {
        showProgressLayout(false)
        mAdapter.setItems(revisions)
    }

    private fun setUpRecyclerView() {
        mAdapter = ReceptionQualityRevisionAdapter()

        rv_revisions.setHasFixedSize(true)
        rv_revisions.layoutManager = LinearLayoutManager(context!!)
        rv_revisions.adapter = mAdapter
        rv_revisions.setPadding(0, 0, 0, 60.toPx())
        rv_revisions.clipToPadding = false
    }

    private fun showProgressLayout(show: Boolean) {
        layout_progress.visibility = if (show) View.VISIBLE else View.GONE
    }

    data class OnCreateRevisionClickedEvent(
            val fragment: ReceptionQualityRevisionListFragment,
            val carryOrderId: Long,
            val carryOrder: CarryOrderModel?
    )

}
