package com.amplemind.vivasmart.features.production_roster.fragments

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class ExtraHoursRequestsFragment : BaseFragment() {
    @Inject
    lateinit var mediator: Mediator

    @Inject
    lateinit var preferences: UserAppPreferences
    // TODO: Rename and change types of parameters
    // private var param1: String? = null
    //private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var mStage: StageModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            // param1 = it.getString(ARG_PARAM1)
            // param2 = it.getString(ARG_PARAM2)
            //mStage = it.getParcelable("stage")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_extra_hours_requests, container, false)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(stage: StageModel) =
                ExtraHoursTabHostFragment().apply {
                    arguments = Bundle().apply {
                        //putParcelable("stage", stage)
                    }
                }
    }
}
