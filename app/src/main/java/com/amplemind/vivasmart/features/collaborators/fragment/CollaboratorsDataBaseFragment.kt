package com.amplemind.vivasmart.features.collaborators.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.snack
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.CollaboratorsListModel
import com.amplemind.vivasmart.core.repository.model.HeaderModel
import com.amplemind.vivasmart.core.repository.response.CollaboratorsInLinesResult
import com.amplemind.vivasmart.core.utils.COLLABORATORS
import com.amplemind.vivasmart.core.utils.HAS_BOXCOUNT
import com.amplemind.vivasmart.core.utils.LOCAL_BROADCAST_USER_PACKING_RECEIVED
import com.amplemind.vivasmart.core.utils.LOCAL_BROADCAST_USER_RECEIVED
import com.amplemind.vivasmart.features.collaborators.*
import com.amplemind.vivasmart.features.collaborators.adapter.CollaboratorsDatabaseAdapter
import com.amplemind.vivasmart.features.collaborators.callBack.ActionModeCallback
import com.amplemind.vivasmart.features.collaborators.viewModel.CollaboratorsDatabaseViewModel
import com.amplemind.vivasmart.features.collaborators.viewModel.ItemCollaboratorsViewModel
import com.amplemind.vivasmart.features.production_quality.AssingTypeActivity
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import kotlinx.android.synthetic.main.content_collaborators_database_fragment.*
import javax.inject.Inject


class CollaboratorsDataBaseFragment : BaseFragment() {

    enum class SEARCH_STATE {
        LOCAL,
        ONLINE,
        NONE
    }

    @Inject
    lateinit var viewModel: CollaboratorsDatabaseViewModel

    @Inject
    lateinit var mediator: Mediator

    @Inject
    lateinit var prefer: UserAppPreferences

    private var actionMode: ActionMode? = null

    private var actionModeCallback: ActionModeCallback? = null

    private var mAdapter: CollaboratorsDatabaseAdapter? = null

    private var state_search = SEARCH_STATE.NONE

    private lateinit var mActivityCode: ActivityCodeModel

    companion object {

        val TAG = CollaboratorsDataBaseFragment::class.simpleName

        val PARAM_ACTIVITY_CODE = "$TAG.ActivityCode"

        fun newInstance(activityCode: ActivityCodeModel, showActivities: Boolean, models: List<CollaboratorsListModel>? = null, section: HeaderModel? = null): CollaboratorsDataBaseFragment {
            val fragment = CollaboratorsDataBaseFragment()
            val args = Bundle()
            args.putBoolean("onlySelected", showActivities)
            if (section != null)
                args.putParcelable("section", section)
            if (models != null)
                args.putParcelableArrayList(COLLABORATORS, models as java.util.ArrayList<out Parcelable>)
            fragment.arguments = args

            return fragment
        }

        fun newInstanceWithActivityCode(showActivities: Boolean, models: List<CollaboratorsListModel>? = null, section: HeaderModel? = null, activityCode: ActivityCodeModel): CollaboratorsDataBaseFragment {
            val fragment = CollaboratorsDataBaseFragment()
            val args = Bundle()
            args.putBoolean("onlySelected", showActivities)
            if (section != null)
                args.putParcelable("section", section)
            if (models != null)
                args.putParcelableArrayList(COLLABORATORS, models as java.util.ArrayList<out Parcelable>)
            fragment.arguments = args

            return fragment
        }

    }

    private fun getSection(): HeaderModel? {
        return arguments?.getParcelable("section")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_collaborators_database_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()

        addActionMode()

        viewModel.onProgressStatus().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onProgressItem().subscribe(this::showProgressItem).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onError).addTo(subscriptions)
        viewModel.setStage(getStageByFlow())
        viewModel.getCollaborators().subscribe(this::setCollaborators).addTo(subscriptions)
        viewModel.getCollaboratorsSearchResult().subscribe(this::setCollaboratorsSearch).addTo(subscriptions)
        viewModel.onAssingSucess().subscribe(this::onAssignSuccess).addTo(subscriptions)
        viewModel.finishSendCollaborators().subscribe(this::sendCollaborators).addTo(subscriptions)
        viewModel.isLoading().subscribe(this::isLoadingRecycler).addTo(subscriptions)

        if (arguments!!.containsKey(COLLABORATORS)) {
            actionMode = (activity as AppCompatActivity).startSupportActionMode(actionModeCallback!!)
            //viewModel.setupData(arguments!!.getParcelableArrayList(COLLABORATORS))
            mAdapter?.selectAllElements()
            val count = mAdapter!!.getSelectedItemCount()
            actionMode!!.title = count.toString()
            actionMode!!.invalidate()
        } else {
            viewModel.getListCollaborators(!isMultiselectd()).addTo(subscriptions)
            (activity as AddCollaboratorsActivity).onSearchQueryChange().subscribe(this::searchCollaborator).addTo(subscriptions)
            rv_collaborators_database.addOnScrollListener(rvScrollManager)
        }
    }

    private fun getStageByFlow(): Int {
        return if (isMultiselectd()) {
            mediator.bundleItemActivitiesPayroll?.stageId ?: 0
        } else {
            mediator.bundleItemLinesPackage ?: 0
        }
    }

    private val rvScrollManager = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val lastVisiblePosition = (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
            val total = (recyclerView.adapter as CollaboratorsDatabaseAdapter).itemCount
            when (state_search) {
                SEARCH_STATE.NONE -> {
                    if (lastVisiblePosition == total - 1 && total < viewModel.getTotalCollaborators() && !(recyclerView.tag as Boolean)) {
                        viewModel.getMoreCollaborators(total, total - 1, !isMultiselectd()).addTo(subscriptions)
                    }
                }
                SEARCH_STATE.ONLINE -> {
                    if (lastVisiblePosition == total - 1 && total < viewModel.getTotalCollaboratorsSearch() && !(recyclerView.tag as Boolean)) {
                        viewModel.searchMoreCollaboratorName(total, total - 1, !isMultiselectd()).addTo(subscriptions)
                    }
                }
                else -> {
                    return
                }
            }
        }
    }

    private fun isLoadingRecycler(isLoading: Boolean) {
        rv_collaborators_database.tag = isLoading
    }

    /**
     *  @param query secound variable isTrue = online search isFalse = local search
     */
    private fun searchCollaborator(query: Pair<String, Boolean>) {
        if (query.second) {
            state_search = SEARCH_STATE.ONLINE
            viewModel.searchCollaboratorName(query.first, 0, !isMultiselectd()).addTo(subscriptions)
        } else {
            setStatusSearch(query.first)
            mAdapter!!.addElements(viewModel.searchCollaborators(query.first), true)
        }
    }

    private fun setStatusSearch(query: String) {
        state_search = if (!query.isEmpty()) {
            SEARCH_STATE.LOCAL
        } else {
            SEARCH_STATE.NONE
        }
    }

    private fun showProgressItem(show: Pair<Int, Boolean>) {
        if (show.second) {
            (rv_collaborators_database.adapter as CollaboratorsDatabaseAdapter).addProgressItem(show.first)
        } else {
            (rv_collaborators_database.adapter as CollaboratorsDatabaseAdapter).removeProgressItem(show.first)
        }
    }

    private fun onlySelected(): Boolean {
        return arguments!!.getBoolean("onlySelected", false)
    }

    /**
     *  cuando es produccion sera multiselect y tendra un flujo diferente
     */
    private fun isMultiselectd(): Boolean {
        return prefer.flow == "Producción"
    }

    private fun addActionMode() {
        actionModeCallback = ActionModeCallback()
        actionModeCallback!!.getActionMode().subscribe(this::removeActionMode).addTo(subscriptions)
        actionModeCallback!!.onAddClick().subscribe(this::onClickAddCollaborator).addTo(subscriptions)
        actionModeCallback!!.onClearSelection().subscribe(this::clearSelection).addTo(subscriptions)
        actionModeCallback!!.onResetAnimation().subscribe(this::resetAnimation).addTo(subscriptions)
    }

    /**
     *  on click add collaborator in toolbar
     */
    private fun onClickAddCollaborator(it: Boolean) {
        //if (isMultiselectd()) {
            viewModel.sendCollaborators(mActivityCode, mAdapter!!.getSelected())
                    .subscribe({
                        //mActivityLogUploader.create(context!!, it)
                        sendCollaborators(it!!)
                    }, {
                        showRequestsFailDialog(it.localizedMessage)
                    })
                    .dispose()
            /*
            if (hasInternet())
                mViewModel.sendCollaboradors(mAdapter!!.getSelectedIDs(), prefer.activityCodeId, prefer.activityCode).addTo(subscriptions)
            else
                mViewModel.saveLocalCollaborators(mAdapter!!.getSelectedIDs(), prefer.activityCodeId, prefer.activityCode)
            */
        /*
            return
        }
        addCollaboratorsFlowPackage()
        */
    }

    private fun showRequestsFailDialog(msg: String) {
        AlertDialog.Builder(context!!)
                .setTitle(getString(R.string.app_name))
                .setMessage(msg)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }.show()

    }

    private fun addCollaboratorsFlowPackage() {
        if (getSection() == null) {
            startActivityForResult(Intent(context, AddActivityForCollaborator::class.java)
                    .putExtra("name", mAdapter!!.getCollaboratorName())
                    .putParcelableArrayListExtra("list_collaborators", viewModel.getCollaboratorSelected(mAdapter!!.getSelectedIDs()) as java.util.ArrayList<out Parcelable>),
                    200)
        } else {
            val section = getSection()

            val dialog = AssingTypeActivity.newInstance()
            dialog.show(activity?.supportFragmentManager!!, AssingTypeActivity.TAG)
            dialog.setTypeActivityListener(object : AssingTypeActivity.OnTypeActivity {
                override fun onHours() {
                    if (section != null) {
                        viewModel.assingCollaborators(hasInternet(), viewModel.getCollaboratorSelected(mAdapter!!.getSelectedIDs()), section.activity_id, getSection()?.title
                                ?: "", getSection()?.collaborator_left
                                ?: 0, mediator.bundleItemLinesPackage!!).addTo(subscriptions)
                        dialog.dismiss()
                    }
                }

                override fun onPiece() {
                    if (section != null) {
                        startActivityForResult(Intent(dialog.context, PresentationForCollaboratorActivity::class.java)
                                .putExtra("name", section.title)
                                .putExtra("name_activity", section.title)
                                .putExtra("collaborator_left", section.collaborator_left)
                                .putParcelableArrayListExtra("list_collaborators", viewModel.getCollaboratorSelected(mAdapter!!.getSelectedIDs()) as java.util.ArrayList<out Parcelable>)
                                .putExtra("activity_id", section.activity_id),
                                200)
                        dialog.dismiss()
                    }
                }

                override fun onCancel() {
                    dialog.dismiss()
                }
            })
        }
    }

    private fun onAssignSuccess(result: CollaboratorsInLinesResult) {
        val intent = Intent(LOCAL_BROADCAST_USER_PACKING_RECEIVED)

        intent.putExtra(COLLABORATORS, result.data as java.util.ArrayList<out Parcelable>)
        intent.putExtra(HAS_BOXCOUNT, result.has_boxcount)
        LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
        (context as AppCompatActivity).setResult(200)
        (context as AppCompatActivity).finish()
    }

    private fun sendCollaborators(collaborators: List<ActivityLogModel>) {
        //mViewModel.saveRecentCollaborators(collaborators).addTo(subscriptions)

        val intent = Intent(LOCAL_BROADCAST_USER_RECEIVED)
        intent.putExtra("code", mActivityCode.id)
        intent.putExtra(COLLABORATORS, collaborators as java.util.ArrayList<out Parcelable>)
        // LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
        (context as AppCompatActivity).setResult(200)
        (context as AppCompatActivity).finish()
    }

    private fun resetAnimation(it: Boolean) {
        rv_collaborators_database.post {
            Runnable {
                mAdapter!!.resetAnimationIndex()
            }.run()
        }
    }

    private fun clearSelection(it: Boolean) {
        mAdapter!!.clearSelections()
    }

    private fun setCollaborators(list: List<ItemCollaboratorsViewModel>) {
        mAdapter!!.addElements(list)
    }

    private fun setCollaboratorsSearch(list: List<ItemCollaboratorsViewModel>) {
        mAdapter!!.addElements(list, true)
    }

    private fun removeActionMode(remove: Boolean) {
        if (!remove) {
            actionMode = null
        }
    }

    fun onClickItem(item: ItemCollaboratorsViewModel) {
        enableActionMode(item.position!!)
    }

    private fun enableActionMode(position: Int) {
        if (actionMode == null) {
            actionMode = (activity as AppCompatActivity).startSupportActionMode(actionModeCallback!!)
        }
        toggleSelection(position)
    }

    private fun toggleSelection(position: Int) {
        val count = mAdapter!!.getSelectedItemCount()

        if (count == 0) {
            actionMode!!.finish()
        } else {
            actionMode!!.title = count.toString()
            actionMode!!.invalidate()
        }
    }

    private fun initRecycler() {
        mAdapter = CollaboratorsDatabaseAdapter(!isMultiselectd() and !onlySelected(), getSection()?.collaborator_left)
        val manager = LinearLayoutManager(context)
        rv_collaborators_database.hasFixedSize()
        rv_collaborators_database.layoutManager = manager
        rv_collaborators_database.itemAnimator = DefaultItemAnimator()
        rv_collaborators_database.adapter = mAdapter

        rv_collaborators_database.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val position = manager.findFirstVisibleItemPosition()
                mAdapter?.nextLetter(position)
            }
        })

        mAdapter!!.currentLetter().subscribe { current_letter.text = it }.addTo(subscriptions)

        mAdapter!!.onClick().subscribe(this::onClickItem).addTo(subscriptions)
        mAdapter!!.exceededLimitCollaborators().subscribe(this::isLimitCollaborator).addTo(subscriptions)
    }

    private fun isLimitCollaborator(result: Boolean) {
        main_collaborator.snack("La actividad no permite agregar mas colaboradores")
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200) {
            (context as AppCompatActivity).setResult(200)
            (context as AppCompatActivity).finish()
        }
    }

}
