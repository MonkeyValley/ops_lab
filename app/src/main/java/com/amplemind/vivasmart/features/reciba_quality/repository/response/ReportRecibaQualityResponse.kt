package com.amplemind.vivasmart.features.reciba_quality.repository.response

import com.google.gson.annotations.SerializedName

data class ReportRecibaQualityResponse (
    @SerializedName("id") val id: Int,
    @SerializedName("folio") val folio: String,
    @SerializedName("lot_name") val lot_name: String? = null,
    @SerializedName("table") val table: String? = null,
    @SerializedName("export_percentage") val percent: Double? = null
)