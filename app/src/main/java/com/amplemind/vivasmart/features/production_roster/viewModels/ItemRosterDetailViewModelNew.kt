package com.amplemind.vivasmart.features.production_roster.viewModels

import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.core.repository.model.DetailtRosterResponse
import javax.inject.Inject

class ItemRosterDetailViewModelNew @Inject constructor(val section : String?,val  model : DetailtRosterResponse?) {

    val activity = model?.activity_code!!.activity!!.name
    val lote = model?.activity_code!!.stageId
    var hrs = model?.time
    var cant = model?.quantity
    val cost = model?.activity_code!!.activity!!.cost
    val money = moneyCalc()
    var status = isFinish()
    var position : Int? = null


    var showSection: ObservableInt
    var showItem: ObservableInt

    init {
        if (section == null) {
            showSection = ObservableInt(View.GONE)
            showItem = ObservableInt(View.VISIBLE)
        } else {
            showSection = ObservableInt(View.VISIBLE)
            showItem = ObservableInt(View.GONE)
        }
    }

    fun moneyCalc(): Double {
        return cant!! * cost!!
    }

    fun isFinish(): Boolean {
        return !model?.sign!!.contains("No Sign")
    }

    fun setPosition(pos : Int){
        this.position = pos
    }

    fun changeValue(cant : Int) {
        this.cant = cant.toDouble()
    }//recepion.hotelposadavirreyes

}