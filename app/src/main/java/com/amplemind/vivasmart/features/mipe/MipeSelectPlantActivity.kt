package com.amplemind.vivasmart.features.mipe

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.features.mipe.viewModel.MipeSelectPlantViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.MipeMonitoringModel
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_mipe_select_plant.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import javax.inject.Inject

class MipeSelectPlantActivity : BaseActivity(), View.OnClickListener {

    companion object {
        val TAG = MipeSelectPlantActivity::class.simpleName
        val LOT_ID = "$TAG.LotId"
        val GROOVE = "$TAG.Groove"
        val WEEK = "$TAG.Week"
        val TABLE = "$TAG.Table"
        val TAG_NAME = "$TAG.TagName"
    }

    var lotId = 0
    var groove = 0
    var week = 0
    var table = ""
    var tagName = ""

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    @Inject
    lateinit var viewModel: MipeSelectPlantViewModel

    var tvSelectPlant: TextView? = null
    var btnPlant1: Button? = null
    var btnPlant2: Button? = null
    var btnPlant3: Button? = null
    var btnPlant4: Button? = null
    var btnPlant5: Button? = null
    var btnSave: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mipe_select_plant)

        viewModel.setContext(this)

        arguments()
        setUpUi()
    }

    fun arguments() {
        lotId = intent?.getIntExtra(LOT_ID, 0) ?: 0
        groove = intent?.getIntExtra(GROOVE, 0) ?: 0
        week = intent?.getIntExtra(WEEK, 0) ?: 0
        table = intent?.getStringExtra(TABLE) ?: ""
        tagName = intent?.getStringExtra(TAG_NAME) ?: ""
    }

    fun setUpUi() {
        setupToolbar()

        tvSelectPlant = findViewById(R.id.tv_select_plant)
        tvSelectPlant!!.text = "Tabla " + table + " - " + "Surco " + groove
        btnPlant1 = findViewById(R.id.btn_plant1)
        btnPlant1!!.setOnClickListener(this)
        btnPlant2 = findViewById(R.id.btn_plant2)
        btnPlant2!!.setOnClickListener(this)
        btnPlant3 = findViewById(R.id.btn_plant3)
        btnPlant3!!.setOnClickListener(this)
        btnPlant4 = findViewById(R.id.btn_plant4)
        btnPlant4!!.setOnClickListener(this)
        btnPlant5 = findViewById(R.id.btn_plant5)
        btnPlant5!!.setOnClickListener(this)
        btnSave = findViewById(R.id.btn_save_monitoring_groove)
        btnSave!!.setOnClickListener(this)

        validatePlantState()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_select_plant
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        mToolbar.findViewById<TextView>(R.id.tv_title_tb).text = intent?.getStringExtra(TAG_NAME)
                ?: ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btn_plant1 -> callMonitoringMipe(1)
            R.id.btn_plant2 -> callMonitoringMipe(2)
            R.id.btn_plant3 -> callMonitoringMipe(3)
            R.id.btn_plant4 -> callMonitoringMipe(4)
            R.id.btn_plant5 -> callMonitoringMipe(5)
            R.id.btn_save_monitoring_groove -> saveData()
        }
    }

    fun callMonitoringMipe(plant: Int) {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now
        val intent = Intent(this, MipeMonitoringActivity::class.java).apply {
            putExtra(MipeMonitoringActivity.LOT_ID, lotId)
            putExtra(MipeMonitoringActivity.GROOVE, groove)
            putExtra(MipeMonitoringActivity.WEEK, week)
            putExtra(MipeMonitoringActivity.TABLE, table)
            putExtra(MipeMonitoringActivity.TAG_NAME, tagName)
            putExtra(MipeMonitoringActivity.PLANT, plant)
        }
        startActivity(intent)
    }

    fun validatePlantState() {
        Realm.getDefaultInstance().use {
            val result = it
                    .where(MipeMonitoringModel::class.java)
                    .beginGroup()
                    .equalTo("lotId", lotId)
                    .and()
                    .equalTo("table", table.toInt())
                    .and()
                    .equalTo("grooveNo", groove)
                    .and()
                    .equalTo("week", week)
                    .endGroup()
                    .findFirst()

            if (result != null) {
                if (result.plants!!.where().equalTo("plantNo", "1".toInt()).findFirst() != null) btnPlant1!!.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))
                if (result.plants!!.where().equalTo("plantNo", "2".toInt()).findFirst() != null) btnPlant2!!.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))
                if (result.plants!!.where().equalTo("plantNo", "3".toInt()).findFirst() != null) btnPlant3!!.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))
                if (result.plants!!.where().equalTo("plantNo", "4".toInt()).findFirst() != null) btnPlant4!!.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))
                if (result.plants!!.where().equalTo("plantNo", "5".toInt()).findFirst() != null) btnPlant5!!.setBackgroundColor(ContextCompat.getColor(this, R.color.blue))

                if(result.plants!!.size == 5 && result.finished == false) btnSave!!.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        validatePlantState()
    }

    fun save(){
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now
        val builder = AlertDialog.Builder(this)
        builder.setTitle("MIPE")
        builder.setMessage("¿Desea enviar la información del surco " + groove + " ?")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            saveData()
            dialog.dismiss()
        }
        builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
        builder.show()
    }

    fun saveData(){
        viewModel.saveData(lotId, table, groove, week)
        finish()
    }

}
