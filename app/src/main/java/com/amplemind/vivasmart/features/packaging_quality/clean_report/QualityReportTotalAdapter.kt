package com.amplemind.vivasmart.features.packaging_quality.clean_report

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemCleanReportTotalBinding
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_clean_report_total.view.*

class QualityReportTotalAdapter : (RecyclerView.Adapter<QualityReportTotalAdapter.QualityReportTotalViewHolder>)() {

    private var list = listOf<ItemCleanTotalReportViewModel>()

    private val clickSubject = PublishSubject.create<Pair<Int, String>>()

    fun addElements(data: List<ItemCleanTotalReportViewModel>) {
        list = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QualityReportTotalViewHolder {
        val binding = ItemCleanReportTotalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return QualityReportTotalViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: QualityReportTotalViewHolder, position: Int) {
        holder.bind(list[position])

    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        clickSubject.onComplete()
    }

    fun getOnClick(): PublishSubject<Pair<Int, String>> {
        return clickSubject
    }

    inner class QualityReportTotalViewHolder(private val binding: ItemCleanReportTotalBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: ItemCleanTotalReportViewModel

        fun bind(item: ItemCleanTotalReportViewModel) {
            binding.setVariable(BR.itemTotal, item)
            binding.executePendingBindings()
            this.item = item
        }

    }

}
