package com.amplemind.vivasmart.core.repository.remote


import com.amplemind.vivasmart.core.repository.response.UnitResponse
import com.amplemind.vivasmart.features.waste_control.models.WasteControlItemModel
import com.amplemind.vivasmart.features.waste_control.models.WasteControlResponse
import io.reactivex.Observable
import retrofit2.http.*

interface WasteControlApi {
    @GET("/v2/waste_control")
    @Headers("Content-Type: application/json")
    fun getList(
            @Header("Authorization") authentication: String,
            @Query("from_date") from_date: String,
            @Query("to_date") to_date: String,
            @Query("embed") embed: String,
            @Query("business_unit_id") business_unit_id: String
    ): Observable<WasteControlResponse>

    //https://qa-ops.vivasmart.com.mx/v2/unit_list?business_unit_id=2F
    @GET("/v2/unit")
    @Headers("Content-Type: application/json")
    fun getUnits(
            @Header("Authorization") authentication: String,
            @Query("business_unit_id") business_unit_id: String
    ): Observable<UnitResponse>

    @POST("v2/waste_control")
    @Headers("Content-Type: application/json")
    fun postFinishedWasteControl(
            @Header("Authorization") authentication: String,
            @Body body: WasteControlItemModel
    ): Observable<WasteControlItemModel>

}