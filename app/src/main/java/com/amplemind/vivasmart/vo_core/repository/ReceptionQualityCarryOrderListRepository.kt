package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.remote.PackagingQualityApi
import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityCarryOrderModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReceptionQualityCarryOrderListRepository @Inject constructor(
        private val mPreferences: UserAppPreferences,
        private val mPackagingQualityApi: PackagingQualityApi
) : Repository() {

    override fun cleanUp() {
    }

    fun loadCarryOrders(date: String): Observable<List<ReceptionQualityCarryOrderModel>> =
        mPackagingQualityApi.getReceptionQualityCarryOrders(mPreferences.authorizationToken, date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {response ->
                    response.data
                }

}