package com.amplemind.vivasmart.features.quality_control.viewModel

import androidx.databinding.ObservableInt
import android.view.View
import com.amplemind.vivasmart.features.interfaces.IDetailControlQualityItemViewModel
import java.util.*
import javax.inject.Inject

class DetailControlQualityItemViewModel @Inject constructor(val name: String, private val isNote: Boolean = false) : IDetailControlQualityItemViewModel, Observable() {

    var notesVisibility = ObservableInt(View.GONE)
    var switchVisibility = ObservableInt(View.VISIBLE)

    init {
        if (isNote) {
            notesVisibility = ObservableInt(View.VISIBLE)
            switchVisibility = ObservableInt(View.GONE)
        }
    }

}