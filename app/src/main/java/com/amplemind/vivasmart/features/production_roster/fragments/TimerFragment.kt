package com.amplemind.vivasmart.features.production_roster.fragments


import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.hideKeyboard
import com.amplemind.vivasmart.core.extensions.touchHelper
import com.amplemind.vivasmart.core.navegation.data_mediator.Colleague
import com.amplemind.vivasmart.core.navegation.data_mediator.Mediator
import com.amplemind.vivasmart.core.navegation.data_mediator.MediatorNavigation
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.CollaboratorsListReponse
import com.amplemind.vivasmart.core.utils.*
import com.amplemind.vivasmart.features.collaborators.AssignGroovesActivity
import com.amplemind.vivasmart.features.production_lines.BoxCountLinesActivity
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModelNew
import com.amplemind.vivasmart.features.production_roster.adapters.RecyclerItemTouchHelper
import com.amplemind.vivasmart.features.production_roster.adapters.TimerManagerUsersAdapter
import com.amplemind.vivasmart.features.production_roster.adapters.TimerUserViewHolder
import com.amplemind.vivasmart.features.production_roster.dialogs.CollaboratorsActionsDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.FinalizeUserTimerDialog
import com.amplemind.vivasmart.features.production_roster.dialogs.PauseTimerDialog
import com.amplemind.vivasmart.features.production_roster.fragments.interfaces.ControlTimerManager
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import kotlinx.android.synthetic.main.fragment_timer.*
import javax.inject.Inject


open class TimerFragment : BaseFragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, Colleague {

    @Inject
    lateinit var viewModel: TimerViewModel

    @Inject
    lateinit var mediator: Mediator

    @Inject
    lateinit var mediatorNavigation: MediatorNavigation

    @Inject
    lateinit var picturesHelper: PicturesHelper

    var controlAdapterManager: ControlTimerManager? = null

    private lateinit var progress: ProgressDialog

    @Inject
    lateinit var preferences: UserAppPreferences

    private lateinit var mActivityCode: ActivityCodeModel
    private lateinit var mActivity: ActivityModel
    private lateinit var mStage: StageModel

    companion object {
        val TAG = TimerFragment::class.java.simpleName

        private val PARAM_ACTIVITY_CODE = "$TAG.ActivityCode"
        private val PARAM_ACTIVITY      = "$TAG.Activity"
        private val PARAM_STAGE         = "$TAG.Stage"

        fun newInstance(type: Int, activityCode: ActivityCodeModel, activity: ActivityModel, stage: StageModel): TimerFragment {
            val fragment = TimerFragment()
            val bundle = Bundle()

            bundle.putInt("type", type)
            //bundle.putParcelable(PARAM_ACTIVITY_CODE, activityCode)
            //bundle.putParcelable(PARAM_ACTIVITY, activity)
            //bundle.putParcelable(PARAM_STAGE, stage)

            //bundle.putInt("type", type)
            //bundle.putInt("activity_code", activity_code)
            //bundle.putString("count_total", total)
            //bundle.putInt("remaining", remaining)
            //bundle.putString("name_lot", name_lot)
            //bundle.putInt("activity_code_number", activity_code_number)
            fragment.arguments = bundle
            return fragment
        }
    }

    fun getNameLot(): String {
        return arguments?.getString("name_lot", "") ?: ""
    }

    private val userRecived = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                LOCAL_BROADCAST_USER_RECEIVED -> addCollaborators(intent.getParcelableArrayListExtra(COLLABORATORS),
                        intent.getIntExtra("code", 0))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //mActivityCode   = arguments!!.getParcelable(PARAM_ACTIVITY_CODE)!!
        //mActivity       = arguments!!.getParcelable(PARAM_ACTIVITY)!!
        //mStage          = arguments!!.getParcelable(PARAM_STAGE)!!

        mediatorNavigation.addColleague(this)
        addSubscribers()
        LocalBroadcastManager.getInstance(context!!).registerReceiver(userRecived, IntentFilter(LOCAL_BROADCAST_USER_RECEIVED))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_timer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()

        initProgress()


        viewModel.setCountTotal(mActivityCode.total!!)
        viewModel.setRemaining(0)
        //mViewModel.setCountTotal(arguments?.getString("count_total") ?: "0/0")
        //mViewModel.setRemaining(arguments?.getInt("remaining", 0))

        cost_by_unit.text = viewModel.getPaymentByCode(mediator.bundleItemActivitiesPayroll?.cost
                ?: 0.0)

        tv_name_unit.text = viewModel.getNameUnit(mediator.bundleItemActivitiesPayroll?.name_unit
                ?: "", mediator.bundleItemActivitiesPayroll?.payment
                ?: ItemActivitiesPayrollViewModelNew.PAYMENTTYPE.PIECE)

        tv_count.text = viewModel.getCountTotal()//arguments?.getString("count_total") ?: "0/0"
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(userRecived)
        mediatorNavigation.removeColleague(this)
        viewModel.onDestroy()
        super.onDestroy()
    }

    private fun addSubscribers() {
        viewModel.onSuccessUpload().subscribe(this::onSuccessUpload).addTo(subscriptions)
        viewModel.onSuccessFinishCollaborator().subscribe(this::onSuccessFinishCollaborator).addTo(subscriptions)
        viewModel.onSuccessFinishAllCollaborators().subscribe(this::onSuccessFinishAllCollaborators).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgress).addTo(subscriptions)
        viewModel.onRequestFail().subscribe(this::onRequestFail).addTo(subscriptions)
        viewModel.onDeleteCollaborator().subscribe(this::onDeleteCollaborator).addTo(subscriptions)
        viewModel.onMissingUnits().subscribe { missingUnitsError(null) }.addTo(subscriptions)
        viewModel.isAllPause().subscribe(this::pauseTime).addTo(subscriptions)
        viewModel.isAllRestart().subscribe(this::restartTime).addTo(subscriptions)
    }

    private fun seekBarStateControl(state: TimerViewModel.SeekControlState) {
        when (state) {
            TimerViewModel.SeekControlState.PAUSED -> {
                val dialog = PauseTimerDialog().newInstance()
                dialog.isCancelable = false
                dialog.show(activity!!.supportFragmentManager, PauseTimerDialog.TAG)
                dialog.setListener(object : PauseTimerDialog.OnPauseTimerListenner {
                    override fun pauseTimer(type: Int) {
                        dialog.dismiss()
                        if (getType() == TIME_PRODUCTION) {
                            viewModel.pauseAllCollaborator(preferences.token, hasInternet())
                            seekBar.pauseTimers()
                        }
                        controlAdapterManager!!.onPauseGeneralTimer()

                        tv_left_seek_control.text = getString(R.string.mcontinue)
                    }
                })
            }
            TimerViewModel.SeekControlState.CONTINUE -> {
                if (getType() == TIME_PRODUCTION) {
                    viewModel.restartAllCollaboratorsTimer(preferences.token, hasInternet())
                    seekBar.restartTimers()
                }
                tv_left_seek_control.text = getString(R.string.pause)
                controlAdapterManager!!.onStartGeneralTimer()

            }
            TimerViewModel.SeekControlState.FINISH -> {
                when (getType()) {
                    TIME_PRODUCTION -> {
                        CollaboratorsActionsDialog.showAlertAction(context!!, getString(R.string.finalize), getString(R.string.finalize_users_msg), acceptAction = {
                            if (hasInternet()) {
                                viewModel.finishAllCollaborator(preferences.token, null, getActivityCode())?.addTo(subscriptions)
                            } else {
                                //viewModel.finishAllLocalCollaborator(getActivityCode())?.addTo(subscriptions)
                            }
                        }, cancelAction = {})
                    }
                    else -> {
                        startActivityForResult(Intent(context, BoxCountLinesActivity::class.java).putExtra("name_lot", getNameLot()), COUNT_BOX_REQUEST)
                    }
                }
            }
        }
    }


    private fun pauseTime(allPause: Boolean) {
        if (allPause) {
            tv_left_seek_control.text = getString(R.string.mcontinue)
            seekBar.pauseTimers()
        }
    }

    private fun restartTime(allRestart: Boolean) {
        if (allRestart) {
            tv_left_seek_control.text = getString(R.string.pause)
            seekBar.restartTimers()
        }
    }

    private fun onDeleteCollaborator(position: Int) {
        controlAdapterManager!!.onRemoveItem(position)
        changeState()
        hideTimerControl()
    }

    private fun addCollaborators(collaborators: List<CollaboratorsListReponse>, activityCode: Int) {
        if (getActivityCode() == activityCode) {
            viewModel.addCollaborators(collaborators, activityCode)
        }
    }

    private fun initProgress() {
        progress = ProgressDialog(context)
        progress.setTitle(getString(R.string.app_name))
        progress.setMessage(getString(R.string.server_connection))
        progress.setCancelable(false)

        initRecycler()
    }

    private fun showProgress(show: Boolean) {
        if (show) progress.show() else progress.dismiss()
    }

    private fun onSuccessUpload(results: List<Any>) {
        viewModel.finishCollaborator(preferences.token, results[1] as Int, results[0] as String, null, results[2] as Boolean, results[3] as Boolean).addTo(subscriptions)
    }

    private fun onSuccessFinishCollaborator(position: Int) {
        controlAdapterManager!!.onRemoveItem(position)
        hideTimerControl()
    }

    private fun hideTimerControl() {
        if (viewModel.listSize() == 0) {
            btn_start.visibility = View.VISIBLE
            seekBarContainer.visibility = View.INVISIBLE
        }
    }

    private fun showTimerControl() {
        btn_start.visibility = View.GONE
        seekBarContainer.visibility = View.VISIBLE
    }

    private fun onSuccessFinishAllCollaborators(success: Boolean) {
        if (success) {
            //All collaborators were finalized
            activity!!.setResult(200)
            activity!!.finish()
        } else {
            //Refresh adapter maybe a few collaborators were finalized
            controlAdapterManager!!.onNotifyDataSetChange()
        }
    }

    private fun onRequestFail(pair: Pair<String, Int?>) {
        AlertDialog.Builder(context)
                .setTitle(getString(R.string.app_name))
                .setMessage(pair.first)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }.show()
        if (pair.second != null) {
            controlAdapterManager!!.onNotifyItemChanged(pair.second!!)
        }
    }

    private fun setupListeners() {

        rv_timer.touchHelper(this)

        //seekBar.getSeekStateControl().subscribe(this::seekBarStateControl).addTo(subscriptions)

        btn_start.setOnClickListener {
            if (viewModel.listSize() > 0) {
                controlAdapterManager!!.onStartGeneralTimer()
                if (getType() == TIME_PRODUCTION) {
                    viewModel.restartAllCollaboratorsTimer(preferences.token, hasInternet())
                }
                showTimerControl()
            }
        }
    }

    fun initRecycler() {
        rv_timer.hasFixedSize()
        rv_timer.layoutManager = LinearLayoutManager(context)
        rv_timer.itemAnimator = DefaultItemAnimator()

        controlAdapterManager = getManagerAdapter()
    }

    private fun getManagerAdapter(): ControlTimerManager {
        val adapter = TimerManagerUsersAdapter(activity!!.supportFragmentManager)
        rv_timer.adapter = adapter
        state_tab.visibility = View.VISIBLE

        loadDataTimerUser()

        return adapter
    }

    fun getActivityCode(): Int {
        return arguments!!.getInt("activity_code", 0)
    }

    fun getActivityCodeNumber(): Int {
        return arguments!!.getInt("activity_code_number", 0)
    }

    /**
     *  upload the information of Collaborators registered previously
     */
    private fun loadDataTimerUser() {
        if (hasInternet()) {
            viewModel.loadActivitiesByCode(mActivityCode.id.toInt(), mStage.id, mActivityCode.code, mActivity.id).addTo(subscriptions)
            //mViewModel.loadActivitiesByCode(getActivityCode(), mediator.bundleItemActivitiesPayroll!!.stageId, getActivityCodeNumber(),
            //        mediator.bundleItemActivitiesPayroll!!.activityId ?: 0).addTo(subscriptions)
        } else {
            viewModel.getLocalCollaborators(getActivityCode()).addTo(subscriptions)
        }
        viewModel.getListActivitiesByCode().subscribe {
            if (it.isNotEmpty()) {
                if (viewModel.allCallaboratorsHaveTime()) {
                    showTimerControl()
                }
                (rv_timer.adapter as TimerManagerUsersAdapter).addElements(it, mediator.bundleItemActivitiesPayroll?.name_unit, mediator.bundleItemActivitiesPayroll?.cost)

                changeState()


            }
        }.addTo(subscriptions)
        viewModel.changeStateEmptyCollaborators().subscribe { changeState() }.addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::showProgress).addTo(subscriptions)
        viewModel.onSuccessUnit().subscribe(this::getIdUnit).addTo(subscriptions)
    }

    fun isEmptyFragment(): Boolean {
        if (!::viewModel.isInitialized) return true
        return viewModel.listSize() == 0
    }

    private fun missingUnitsError(position: Int?) {
        val snackbar = Snackbar.make(fragment_timer_root, getString(R.string.missing_units), Snackbar.LENGTH_LONG)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text)
                .setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
        viewModel.setUserErrorMissingUnits(true, position)
        if (position != null) controlAdapterManager!!.onNotifyItemChanged(position) else controlAdapterManager!!.onNotifyDataSetChange()
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is TimerUserViewHolder) {
            when (direction) {
                ItemTouchHelper.LEFT -> {
                    when (getType()) {
                        TIME_PRODUCTION -> {
                            CollaboratorsActionsDialog.showAlertAction(context!!, getString(R.string.delete), getString(R.string.delete_user_msg), acceptAction = {
                                if (hasInternet()) {
                                    viewModel.deleteCollaborator(preferences.token, position).addTo(subscriptions)
                                } else {
                                    viewModel.deleteLocalCollaborator(position).addTo(subscriptions)
                                }
                            }, cancelAction = {
                                controlAdapterManager!!.onNotifyItemChanged(viewHolder.adapterPosition)
                            })
                        }
                        else -> {
                            controlAdapterManager!!.onRemoveItem(viewHolder.adapterPosition)
                        }
                    }
                }
                ItemTouchHelper.RIGHT -> {
                    when (getType()) {
                        TIME_PRODUCTION -> {
                            finishCollaboratorProduction(position, viewHolder)
                        }
                        else -> {
                            finishCollaboratorLines(position)
                        }
                    }
                }
            }
        }
    }

    /**
     *  method finish collaborator in production LINES flow
     */
    private fun finishCollaboratorLines(position: Int) {
        startActivityForResult(Intent(context, BoxCountLinesActivity::class.java).putExtra("position", position).putExtra("name_lot", getNameLot()), COUNT_BOX_REQUEST)
        controlAdapterManager!!.onNotifyItemChanged(position)
    }

    /**
     *  Method finish collaborator in production flow
     */
    private fun finishCollaboratorProduction(position: Int, viewHolder: RecyclerView.ViewHolder) {
        if (viewModel.canFinalizeCollaborator(position)) {
            val practiceCost = viewModel.getUserPracticePayment(mediator.bundleItemActivitiesPayroll?.costPractice,
                    viewModel.getCost(position)
                            ?: 0.0, position, mediator.bundleItemActivitiesPayroll?.payment!!)
            val dialog = FinalizeUserTimerDialog.newInstance(getString(R.string.finish_activity_title), "¿Esta seguro de querer enviar este reporte? Por favor firme para confirmar",
                    viewModel.getUserName(position), viewModel.getUserTime(position),
                    viewModel.getUserPayment(viewModel.getCost(position)
                            ?: 0.0, position, mediator.bundleItemActivitiesPayroll?.payment!!, mediator.bundleItemActivitiesPayroll?.sunday_rate!!, isHead = mediator.bundleItemActivitiesPayroll?.isHead ?: false,
                            plantPerGroove = mediator.bundleControlQualityItemViewModel.plantPerGroove),
                    viewModel.getUserWork(position), getType() == TIME_LINES, viewModel.getUnitName(position),
                    viewModel.getUserTrainingPayment(mediator.bundleItemActivitiesPayroll?.costPerHour,
                            viewModel.getCost(position)
                                    ?: 0.0, position, mediator.bundleItemActivitiesPayroll?.payment!!),
                    practiceCost,
                    mediator.bundleItemActivitiesPayroll?.isTrainingEnabled() ?: false,
                    isPractice = mediator.bundleItemActivitiesPayroll?.isPractice ?: false)
            dialog.isCancelable = false
            dialog.show(activity!!.supportFragmentManager, FinalizeUserTimerDialog.TAG)

            /*
            dialog.setOnCompleteListener(object : OnFinalizeUserListener {
                override fun cancel() {
                    controlAdapterManager!!.onNotifyItemChanged(viewHolder.adapterPosition)
                }

                override fun completeSign(sign: Bitmap, isTrainig: Boolean, isPractice: Boolean) {
                    val image = mPicturesHelper.createImageFile(context!!)
                    if (hasInternet()) {
                        mPicturesHelper.compressImage(mPicturesHelper.createBitmapToFile(context!!, sign), image, 400f, 400f)
                        mViewModel.uploadSign(image, preferences.token, position, isTrainig, isPractice).addTo(subscriptions)
                    } else {
                        mViewModel.finishSignLocalCollaborator(mPicturesHelper.createBitmapToFile(context!!, sign), position, isTrainig, isPractice, getActivityCode()).addTo(subscriptions)
                    }
                }

                override fun skipSignature(isTrainig: Boolean, isPractice: Boolean) {
                    if (hasInternet()) {
                        mViewModel.finishCollaborator(preferences.token, position, null, null, isTrainig, isPractice).addTo(subscriptions)
                    } else {
                        mViewModel.finishSignLocalCollaborator(null, position, isTrainig, isPractice, getActivityCode()).addTo(subscriptions)
                    }
                }
            })
            */
        } else {
            missingUnitsError(position)
        }
    }

    /**
     *  assign grooves or units
     */
    override fun onClick(item: TimerUserViewModel, position: Int) {
        if (mediator.bundleItemActivitiesPayroll?.isGrooves!!) {
            mediator.sendBundleTimerUserViewModel(item)
            startActivityForResult(Intent
            (context, AssignGroovesActivity::class.java)
                    .putExtra("activity_log", item.activityLogId)
                    .putExtra("position", position),
                    GROOVES_UPDATE_REQUEST)
        } else {
            showEditDialog(item, position)
        }
    }



    private fun showEditDialog(item: TimerUserViewModel, position: Int) {
        // val dialog = AssignUnitDialogFragment.newInstance(mediator.bundleItemActivitiesPayroll?.name_unit, mViewModel.getBusy(position), mViewModel.getCountUnit(position))
        /*
        val dialog = AssignUnitHarvestDialogFragment.newInstance(mediator.bundleItemActivitiesPayroll?.name_unit, mViewModel.getBusy(position), mViewModel.getCountUnit(position))

        dialog.show((context as AppCompatActivity).supportFragmentManager, AssignUnitHarvestDialogFragment.TAG)
        dialog.setChangeListenner(object : AssignUnitHarvestDialogFragment.OnChangeUnit {
            override fun cancelEdit() {
                if (dialog.context != null){
                    hideKeyboard(dialog.context!!)
                }
                dialog.dismiss()
            }

            override fun changeUnit(new_value: Int, previous_value: Int) {
                if (hasInternet()) {
                    mViewModel.sendUnit(new_value, previous_value,
                            item.getCollaboratorId(),
                            item.getActivityCode(),
                            item.time,
                            item.getCurrentDate(),
                            false,
                            null,
                            position)
                } else {
                    mViewModel.sendLocalUnit(new_value, previous_value,
                            item.getCollaboratorId(),
                            item.getActivityCode(),
                            item.time,
                            item.getCurrentDate(),
                            false,
                            null,
                            position)
                }
            }
        })
        */
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GROOVES_UPDATE_REQUEST) {
            if (data != null) {
                viewModel.setActivitLogToCollaborator(getActivityCode(), data.getIntExtra("id_collaborator", 0),
                        data.getLongExtra("id", 0), data.getIntExtra("quantity", -1), true)
                controlAdapterManager!!.onNotifyItemChanged(data.getIntExtra("position", 0))

                changeState()
            }
        } else {
            when (resultCode) {
                BoxCountLinesActivity.BOX_DONE -> {
                    controlAdapterManager!!.onRemoveItem(data?.getIntExtra("position", 0) ?: 0)
                }
                BoxCountLinesActivity.BOX_CANCEL -> {
                    controlAdapterManager!!.onNotifyItemChanged(data?.getIntExtra("position", 0)
                            ?: 0)
                }
                BoxCountLinesActivity.BOX_ALL_DONE -> {
                    activity?.finish()
                }
            }
        }
    }

    private fun changeState() {
        tv_count?.text = viewModel.getCountTotal() ?: ""
        arguments?.putInt("remaining", viewModel.getRemainigs())
    }

    private fun getIdUnit(position: Int) {
        changeState()
        controlAdapterManager!!.onNotifyItemChanged(position)
        rv_timer.postDelayed({
            hideKeyboard(context!!)
        }, 500)
    }

    private fun getType(): Int {
        return arguments!!.getInt("type", 0)
    }

    override fun startTimer(item: TimerUserViewModel, position: Int) {
        viewModel.restartCollaboratorTimer(preferences.token, position, hasInternet())
        if (viewModel.allTimersAreRunning()) {
            showTimerControl()
        }
    }

    override fun pauseTimer(item: TimerUserViewModel, position: Int) {
        viewModel.pauseCollaborator(preferences.token, position, hasInternet())
    }

}
