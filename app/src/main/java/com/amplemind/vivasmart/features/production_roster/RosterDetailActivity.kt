package com.amplemind.vivasmart.features.production_roster

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.SwitchCompat
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.GROOVES_UPDATE_REQUEST
import com.amplemind.vivasmart.core.utils.PicturesHelper
import com.amplemind.vivasmart.core.utils.VALIDATE_NO_SIGNATURE
import com.amplemind.vivasmart.features.collaborators.AssignGroovesActivity
import com.amplemind.vivasmart.features.extensions.showErrorDialog
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import com.amplemind.vivasmart.features.production_quality.SignatureDialogFragment
import com.amplemind.vivasmart.vo_core.repository.RosterDetailRepository
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getActiveHours
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getActivity
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.CollaboratorModel
import com.amplemind.vivasmart.vo_features.production_roster.viewmodels.TimerFragmentViewModel
import com.google.gson.Gson
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.content_roster_detail_activity.*
import javax.inject.Inject

class RosterDetailActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener, View.OnTouchListener {

    companion object {

        val TAG: String = RosterDetailActivity::class.java.simpleName

        //Types passed to PARAM_TYPE
        val TYPE_SIGNED = RosterDetailRepository.Type.SIGNED.name
        val TYPE_UNSIGNED = RosterDetailRepository.Type.UNSIGNED.name

        val PARAM_COLLABORATOR_UUID = "$TAG.CollaboratorUuid"
        val PARAM_STAGE_UUID = "$TAG.StageUuid"
        val PARAM_TYPE = "$TAG.Type"

        private const val REQUEST_GROOVES = 100
    }

    @Inject
    lateinit var viewModel: RosterDetailViewModel

    @Inject
    lateinit var picturesHelper: PicturesHelper

    @Inject
    lateinit var mViewModel: TimerFragmentViewModel

    private lateinit var mAdapter: RosterDetailAdapter

    private lateinit var mSwIsTraining: SwitchCompat
    private lateinit var mSwIsControl: SwitchCompat
    private lateinit var mSwIsPractice: SwitchCompat

    private lateinit var mRoot: View

    private var mSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_roster_detail_activity)

        readArguments()
        setUpUI()
        setUpUICallbacks()

        loadData()

        setUpCollaboratorUI()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        switchValidation(buttonView as SwitchCompat)

        viewModel.isTraining = mSwIsTraining.isChecked
        viewModel.isControl = mSwIsControl.isChecked
        viewModel.isPractice = mSwIsPractice.isChecked

        displayPayment()
    }

    private fun switchValidation(switch: SwitchCompat) {
        if (switch.isChecked) {
            showPaymentModeWarningMesssage(switch)

            when (switch) {
                mSwIsControl -> {
                    mSwIsPractice.isChecked = false
                    mSwIsTraining.isChecked = false
                }
                mSwIsTraining -> {
                    mSwIsPractice.isChecked = false
                    mSwIsControl.isChecked = false
                }
                //switchPractice
                else -> {
                    mSwIsTraining.isChecked = false
                    mSwIsControl.isChecked = false
                }
            }
        }
    }

    private fun showPaymentModeWarningMesssage(switch: SwitchCompat) {
        if (mSnackbar?.isShown == true) {
            mSnackbar?.dismiss()
        }

        val msgId = when (switch) {
            mSwIsTraining -> if (switch.isChecked)
                R.string.training_activated
            else
                R.string.training_deactivated
            mSwIsPractice -> if (switch.isChecked)
                R.string.practice_activated
            else
                R.string.practice_deactivated
            //switchControl
            else -> if (switch.isChecked)
                R.string.control_activated
            else
                R.string.control_deactivated
        }

        val message = getString(msgId)
        mSnackbar = Snackbar.make(mRoot, message, Snackbar.LENGTH_LONG)
        val snackText = mSnackbar?.view?.findViewById<TextView>(R.id.snackbar_text)
        mSnackbar?.view?.setBackgroundColor(ContextCompat.getColor(this, R.color.bgAlert))
        snackText?.setTextColor(Color.WHITE)
        snackText?.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(this, R.drawable.ic_warning), null)
        mSnackbar?.view?.layoutParams?.width = ViewGroup.LayoutParams.MATCH_PARENT
        mSnackbar?.show()
    }

    private fun readArguments() {
        viewModel.collaboratorUuid = requireNotNull(intent.getStringExtra(PARAM_COLLABORATOR_UUID))
        viewModel.stageUuid = requireNotNull(intent.getStringExtra(PARAM_STAGE_UUID))
        viewModel.type = requireNotNull(intent.getStringExtra(PARAM_TYPE))
    }

    private fun loadData() {

        if (viewModel.isReadOnly) {
            mSwIsTraining.isChecked = viewModel.isTraining
            mSwIsControl.isChecked = viewModel.isControl
            mSwIsPractice.isChecked = viewModel.isPractice
        }
        viewModel.loadCollaborator().subscribe(this::onCollaboratorLoaded).addTo(subscriptions)
        viewModel.loadActivityLogs().subscribe(this::onActivityLogsLoaded).addTo(subscriptions)
    }

    private fun onCollaboratorLoaded(collaborator: CollaboratorModel) {
        tv_name.text = collaborator.name
    }

    private fun onActivityLogsLoaded(activityLogs: List<ActivityLogModel>) {
        if(activityLogs.size != 0) {
            //Set switch setting only on the first activityLogs load
            viewModel.getPersonalUI(activityLogs)

            // mSwIsTraining.isChecked = false
            if (mAdapter.items.isEmpty()) {
                mSwIsTraining.isChecked = viewModel.isTraining
                mSwIsControl.isChecked = viewModel.isControl
                mSwIsPractice.isChecked = viewModel.isPractice
            }

            mAdapter.items = activityLogs
            displayPayment()
            quantityAndHoursValidation(activityLogs)
        } else {
            showSignDialogNoActivities()
        }
    }

    private fun setUpUICallbacks() {
        mEventBus.observe<RosterDetailAdapter.ActivityLogClickedEvent>()
                .filter { event -> event.adapter == mAdapter && !viewModel.isReadOnly }
                .subscribe(this::onActivityLogClickedEvent).addTo(subscriptions)

        mEventBus.observe<RosterDetailAdapter.ActivityLogLongClickedEvent>()
                .filter { event -> event.adapter == mAdapter && !viewModel.isReadOnly }
                .subscribe(this::onActivityLogLongClickedEvent).addTo(subscriptions)

        RxView.clicks(tv_sing).subscribe { showSignDialog() }.addTo(subscriptions)
    }

    private fun setUpUI() {
        setupToolbar()
        initRecycler()
        mRoot = findViewById(R.id.root)
        mSwIsTraining = findViewById(R.id.sw_is_training)
        mSwIsControl = findViewById(R.id.sw_is_control)
        mSwIsPractice = findViewById(R.id.sw_is_practice)
    }

    private fun setUpCollaboratorUI() {
        if (viewModel.isReadOnly) {
            sw_is_training.isEnabled = false
            sw_is_control.isEnabled = false
            sw_is_practice.isEnabled = false
        }


        mSwIsTraining.setOnTouchListener(this)
        mSwIsControl.setOnTouchListener(this)
        mSwIsPractice.setOnTouchListener(this)

        mSwIsControl.setOnCheckedChangeListener(this)
        mSwIsTraining.setOnCheckedChangeListener(this)
        mSwIsPractice.setOnCheckedChangeListener(this)
    }

    private fun validateSwitch(mode : Int) {
        var msjTraining = ""
        var validateActivity = ""

        if(mode == 1) {
            viewModel.trainingName.distinct().forEach {
                if (it != validateActivity) msjTraining += " $it,"
                validateActivity = it
            }
        }
        var msjControl = ""
        validateActivity = ""

        if(mode == 2) {
            viewModel.controlName.distinct().forEach {
                if (it != validateActivity) msjControl += " $it,"
                validateActivity = it
            }
        }
        var msjPractice = ""
        validateActivity = ""

        if(mode == 3) {
            viewModel.practiceName.distinct().forEach {
                if (it != validateActivity) msjPractice += " $it,"
                validateActivity = it
            }
        }

        if (msjTraining != "" || msjControl != "" || msjPractice != "") {
            var msj = ""
            if (msjTraining != "") msj += "* CAPACITACION:\n$msjTraining\n"
            if (msjPractice != "") msj += "* ENTRENAMIENTO:\n$msjPractice\n"
            if (msjControl != "") msj += "* PERSONAL DE CONTROL:\n$msjControl\n"

            val builder = AlertDialog.Builder(this@RosterDetailActivity)
            builder.setTitle("Detalle de nómina")
            builder.setMessage("Estas actividades no cuentan con los siguientes criterios:\n\n$msj")
            builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                dialog.dismiss()
            }
            builder.show()
        }

    }

    private fun onActivityLogClickedEvent(event: RosterDetailAdapter.ActivityLogClickedEvent) = editElement(event.activityLog)

    private fun onActivityLogLongClickedEvent(event: RosterDetailAdapter.ActivityLogLongClickedEvent) = deleteElement(event.activityLog)


    private fun initRecycler() {
        rv_detailt.setHasFixedSize(true)
        rv_detailt.layoutManager = LinearLayoutManager(this)
        rv_detailt.itemAnimator = DefaultItemAnimator()
        mAdapter = RosterDetailAdapter(mEventBus)
        rv_detailt.adapter = mAdapter
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_report_detail
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.title_roster_detail)
    }

    private fun showSignButton(flagSignature: Boolean) {
        if (!viewModel.isReadOnly) {
            tv_sing.visibility = View.VISIBLE
            tv_sing.setOnClickListener {
                if (!flagSignature)
                    showSignDialogNoSignature()
                else
                    showSignDialog()
            }
        }
    }

    private fun quantityAndHoursValidation(list: List<ActivityLogModel>) {
        var flagSignature = true

        for (activityLogHead in list) {
            var quantityAux = activityLogHead.quantity
            var activeHours = activityLogHead.getActiveHours()

            if (quantityAux == 0 || activeHours == 0)
                flagSignature = false
        }
        Log.d("flagSignature", flagSignature.toString())
        showSignButton(flagSignature)
    }

    private fun displayPayment() {
        /*var isTrainingThis = viewModel.isTraining
        if(mSwIsTraining.isChecked){
            isTrainingThis = true
        }*/
        tv_total_salary.text = mAdapter.calculateTotalPayment(viewModel.isTraining, viewModel.isPractice, viewModel.isControl)
    }

    private fun showSignDialogNoSignature() {
        val mjs = "No puede completar el proceso de firma si no asigna cantidad y horas a las actividades."
        AlertDialog.Builder(this@RosterDetailActivity, R.style.AlertDialogCustom)
                .setTitle("Firmar")
                .setMessage(mjs)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, i ->
                    dialogInterface.dismiss()
                }.show()
    }

    private fun showSignDialogNoActivities() {
        val mjs = "El colaborador no cuenta con mas actividades para firmar, se devolvera a la pantalla anterior."
        AlertDialog.Builder(this@RosterDetailActivity, R.style.AlertDialogCustom)
                .setTitle("ACTIVIDADES")
                .setMessage(mjs)
                .setPositiveButton(getString(R.string.accept)) { dialogInterface, i ->
                    dialogInterface.dismiss()
                    finish()
                }.show()
    }

    private fun showSignDialog() {
        val title = getString(R.string.rda_send_report_title)
        val message = getString(R.string.rda_send_report_desc)
        val dialog = SignatureDialogFragment().newInstance(title, message,
                true, total = tv_total_salary.text.toString(),
                name = tv_name.text.toString(), mode = 1)
        dialog.show(supportFragmentManager, SignatureDialogFragment.TAG)
        dialog.setOnCompleteListener(object : OnSignatureCompleteListener {
            override fun onDismiss() {
                /*
                if (viewModel.hasActivatedSwitch()) {
                    mSwIsTraining.isChecked = false
                    mSwIsPractice.isChecked = false
                    mSwIsControl.isChecked = false
                }
                 */
            }

            override fun completeSign(sign1: Bitmap, sign2: Bitmap?) {
                picturesHelper.createSignatureImageFile(this@RosterDetailActivity, sign1)
                        .flatMap { signature ->
                            Log.e(TAG, "Signature file created at: ${signature.path}")

                            viewModel.finishActivityLogs(
                                    mAdapter.items,
                                    viewModel.isTraining,
                                    viewModel.isPractice,
                                    viewModel.isControl,
                                    signature
                            )
                        }
                        .subscribe({
                            mEventBus.send(UpdateReportEvent(viewModel.stageUuid))
                            finish()
                        }, { error -> onError(error.localizedMessage) })
                        .addTo(subscriptions)
            }
        })
    }

    private fun editElement(activityLog: ActivityLogModel) {
        if (activityLog.activityCode?.activity?.isGrooves == true) editGrooveOrHrs(activityLog)
        else showEditUnitiesDialog(activityLog)
    }

    private fun deleteElement(activityLog: ActivityLogModel) {
        androidx.appcompat.app.AlertDialog.Builder(this)
                .setTitle("ELIMINAR ACTIVIDAD")
                .setMessage("¿Seguro que desea eliminar "
                        + activityLog.getActivity().name +
                        " de este colaborador?")
                .setPositiveButton(R.string.accept) { _, _ ->
                    showProgressDialog(true)
                    mViewModel.removeRosterDetail(activityLog)
                            .subscribe({
                                reloadData()
                                showProgressDialog(false)
                            }, { error ->
                                reloadData()
                                showErrorDialog(error)
                                showProgressDialog(false)
                            }).addTo(subscriptions)
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun reloadData(){
        rv_detailt!!.adapter = null
        loadData()
        rv_detailt!!.adapter = mAdapter
        showProgressDialog(false)
    }

    private fun editGrooveOrHrs(activityLog: ActivityLogModel) {
        val dialog = SelectedTypeEdit().newInstance()
        dialog.show(supportFragmentManager, SelectedTypeEdit.TAG)
        dialog.setListener(object : SelectedTypeEdit.OnSelectedTypeEdit {
            override fun onEditGroove() = editGrooves(activityLog)
            override fun onEditTime() = showEditUnitiesDialog(activityLog, true)
        })
    }

    private fun editGrooves(activityLog: ActivityLogModel) {
        startActivityForResult(Intent(this, AssignGroovesActivity::class.java)
                .putExtra(AssignGroovesActivity.PARAM_ACTIVITY_CODE_UUID, activityLog.activityCodeUuid)
                .putExtra(AssignGroovesActivity.PARAM_ACTIVITY_LOG_UUID, activityLog.uuid),
                GROOVES_UPDATE_REQUEST)
    }

    private fun showEditUnitiesDialog(activityLog: ActivityLogModel, onlyTime: Boolean = false) {
        val dialog = EditQuantityFragment.newInstance(
                activityLog.id.toString(),
                activityLog.collaborator?.name ?: getString(R.string.default_collaborator_name),
                activityLog.quantity,
                activityLog.getActiveHours(),
                onlyTime
        )

        dialog.show(supportFragmentManager, EditQuantityFragment.TAG)
        dialog.setOnEditQuantity(object : OnEditQuantity {
            override fun onCancel() = dialog.dismiss()
            override fun onEditItem(activityLogId: String, quantity: Int, timeInMinutes: Int) {
                viewModel.setQuantityAndTime(activityLog, quantity, timeInMinutes)
                        .subscribe({
                            mSwIsControl.isChecked = false
                            mSwIsPractice.isChecked = false
                            mSwIsTraining.isChecked = false
                            displayPayment()
                            dialog.dismiss()
                            loadData()
                        }, { onError(it.localizedMessage) })
                        .addTo(subscriptions)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mSwIsTraining.isChecked = false
        mSwIsPractice.isChecked = false
        mSwIsControl.isChecked = false
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    data class UpdateReportEvent(val stageUuid: String)

    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
        when (view!!.id) {
            R.id.sw_is_training -> {
                if (MotionEvent.ACTION_UP == event!!.getAction()) {
                    if(viewModel.setTraining && viewModel.setTrainingValidate)
                    mSwIsTraining.isEnabled = true
                   else {
                        mSwIsTraining.isEnabled = false
                        validateSwitch(1)
                    }
                }
            }
            R.id.sw_is_control-> {
                if (MotionEvent.ACTION_UP == event!!.getAction()) {
                    if(viewModel.setControl && viewModel.setControlValidate)
                    mSwIsControl.isEnabled = true
                   else {
                        mSwIsControl.isEnabled = false
                        validateSwitch(2)
                    }
                }
            }
            R.id.sw_is_practice -> {
                if (MotionEvent.ACTION_UP == event!!.getAction()) {
                    if(viewModel.setPractice && viewModel.setPracticeValidate)
                    mSwIsPractice.isEnabled = true
                    else {
                        mSwIsPractice.isEnabled = false
                        validateSwitch(3)
                    }
                }
            }
        }
        return false
    }

}
