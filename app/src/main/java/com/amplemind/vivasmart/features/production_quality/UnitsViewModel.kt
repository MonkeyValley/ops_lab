package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.repository.response.UnitResponseResult

class UnitsViewModel(unit : UnitResponseResult) {
    val id = unit.id
    val business_unit_id =  unit.business_unit_id
    val business_unit_names =  unit.business_unit_names
    val name =  unit.name
}
