package com.amplemind.vivasmart.features.fertiriego.fragment


import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.utils.DecimalDigitsInputFilter
import com.amplemind.vivasmart.features.fertiriego.viewModel.PHCEViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.PHCEForTable
import javax.inject.Inject


class PHCEFragment : BaseFragment(), View.OnClickListener {

    @Inject
    lateinit var viewModel: PHCEViewModel

    lateinit var fragView: View

    var etPH: TextView? = null
    var etCE: TextView? = null
    var btnSave: Button? = null
    var llSend: LinearLayout? = null

    var etPHChup: TextView? = null
    var etCEChup: TextView? = null
    var btnSaveChup: Button? = null
    var llChup: LinearLayout? = null

    var etPHDrain: TextView? = null
    var etCEDrain: TextView? = null
    var btnSaveDrain: Button? = null
    var llDrain: LinearLayout? = null

    var etPHSqueezed: TextView? = null
    var etCESqueezed: TextView? = null
    var btnSaveSqueezed: Button? = null
    var llSqueezed: LinearLayout? = null

    fun newInstance(date: String, lotId: Int, table: String, mode: String): PHCEFragment {
        val args = Bundle()
        args.putString("DATE", date)
        args.putInt("LOT_ID", lotId)
        args.putString("TABLE", table)
        args.putString("MODE", mode)

        val fragment = PHCEFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragView = inflater.inflate(R.layout.fragment_phce, container, false)

        viewModel.setServiceContect(activity!!)
        viewModel.fragment = this

        setUpUiAndArguments(fragView!!)
        addSubscribers()
        getDataFromDate()

        return fragView
    }

    fun setUpUiAndArguments(view: View?) {
        viewModel.table = arguments!!.getString("TABLE") ?: ""
        viewModel.lotId = arguments!!.getInt("LOT_ID") ?: 0

        val validate: Boolean = viewModel.validateDate()

        etPH = view!!.findViewById(R.id.et_ph)
        etPH!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        etCE = view!!.findViewById(R.id.eT_CE)
        etCE!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        btnSave = view!!.findViewById(R.id.btn_check_PHCE)
        btnSave!!.setOnClickListener(this)
        llSend = view!!.findViewById(R.id.ll_send)

        etPHChup = view!!.findViewById(R.id.et_ph_chupa)
        etPHChup!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        etCEChup = view!!.findViewById(R.id.eT_CE_chupa)
        etCEChup!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        btnSaveChup = view!!.findViewById(R.id.btn_check_PHCE_chupa)
        btnSaveChup!!.setOnClickListener(this)
        llChup = view!!.findViewById(R.id.ll_chup)

        etPHDrain = view!!.findViewById(R.id.et_ph_drain)
        etPHDrain!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        etCEDrain = view!!.findViewById(R.id.et_ce_drain)
        etCEDrain!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        btnSaveDrain = view!!.findViewById(R.id.btn_check_PHCE_drain)
        btnSaveDrain!!.setOnClickListener(this)
        llDrain = view!!.findViewById(R.id.ll_drain)

        etPHSqueezed = view!!.findViewById(R.id.et_ph_squeezed)
        etPHSqueezed!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        etCESqueezed = view!!.findViewById(R.id.eT_CE_squeezed)
        etCESqueezed!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        btnSaveSqueezed = view!!.findViewById(R.id.btn_check_PHCE_squeezed)
        btnSaveSqueezed!!.setOnClickListener(this)
        llSqueezed = view!!.findViewById(R.id.ll_squeezed)

        setEnableField(validate)
        setEnableFieldChup(validate)
        setEnableFieldDrain(validate)
        setEnableFieldSqueezed(validate)

        if (!validate) {
            btnSave!!.visibility = View.GONE
            btnSaveChup!!.visibility = View.GONE
            btnSaveDrain!!.visibility = View.GONE
            btnSaveSqueezed!!.visibility = View.GONE
        }

        val mode = arguments!!.getString("MODE") ?: ""

        if(mode == "suelo"){
            llDrain!!.visibility = View.GONE
            llSqueezed!!.visibility = View.GONE
        } else {
            llChup!!.visibility = View.GONE
        }
    }

    private fun setEnableField(enable: Boolean) {
        etPH!!.isEnable(enable)
        etCE!!.isEnable(enable)
    }

    private fun setEnableFieldChup(enable: Boolean) {
        etPHChup!!.isEnable(enable)
        etCEChup!!.isEnable(enable)
    }

    private fun setEnableFieldDrain(enable: Boolean) {
        etPHDrain!!.isEnable(enable)
        etCEDrain!!.isEnable(enable)
    }

    private fun setEnableFieldSqueezed(enable: Boolean) {
        etPHSqueezed!!.isEnable(enable)
        etCESqueezed!!.isEnable(enable)
    }


    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccesPHCEDataPerDay().subscribe(this::setDataForPHCE).addTo(subscriptions)
    }

    private fun setDataForPHCE(list: List<PHCEForTable>) {
        setDataToZero()
        list.forEach {
            if(it.type == "envio") {
                etPH!!.text = if (it.ph != null) it.ph else ""
                etCE!!.text = if (it.ce != null) it.ce else ""
                setEnableField(false)
                btnSave!!.visibility = View.GONE
            } else if(it.type == "chupatubo") {
                etPHChup!!.text = if (it.ph != null) it.ph else ""
                etCEChup!!.text = if (it.ce != null) it.ce else ""
                setEnableFieldChup(false)
                btnSaveChup!!.visibility = View.GONE
            } else if(it.type == "drenado") {
                etPHDrain!!.text = if (it.ph != null) it.ph else ""
                etCEDrain!!.text = if (it.ce != null) it.ce else ""
                setEnableFieldDrain(false)
                btnSaveDrain!!.visibility = View.GONE
            } else if(it.type == "exprimido") {
                etPHSqueezed!!.text = if (it.ph != null) it.ph else ""
                etCESqueezed!!.text = if (it.ce != null) it.ce else ""
                setEnableFieldSqueezed(false)
                btnSaveSqueezed!!.visibility = View.GONE
            }
        }

        if (viewModel.validateDate() && list.isEmpty()) {
            setEnableField(true)
            setEnableFieldChup(true)
            setEnableFieldDrain(true)
            setEnableFieldChup(true)
        }
    }

    fun setDataToZero() {
        etPH!!.text = ""
        etCE!!.text = ""
        etPHChup!!.text = ""
        etCEChup!!.text = ""
        etPHDrain!!.text = ""
        etCEDrain!!.text = ""
        etPHSqueezed!!.text = ""
        etCESqueezed!!.text = ""
    }

    fun getDataFromDate() {
        setEnableField(false)
        setEnableFieldChup(false)
        setEnableFieldDrain(false)
        setEnableFieldSqueezed(false)
        viewModel.getPHCEDataForTable()
        if (viewModel.validateDate()) {
            btnSave!!.visibility = View.VISIBLE
            btnSaveSqueezed!!.visibility = View.VISIBLE
            btnSaveDrain!!.visibility = View.VISIBLE
            btnSaveChup!!.visibility = View.VISIBLE
            setEnableField(true)
            setEnableFieldChup(true)
            setEnableFieldDrain(true)
            setEnableFieldSqueezed(true)
        } else {
            btnSave!!.visibility = View.GONE
            btnSaveSqueezed!!.visibility = View.GONE
            btnSaveDrain!!.visibility = View.GONE
            btnSaveChup!!.visibility = View.GONE
        }
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btn_check_PHCE -> {
                if (validateTextView()) {
                    Log.e("clickMonitoring", "click")
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("pH - CE")
                    builder.setMessage("¿Desea guardar la información de envio de esta valvula?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                        savePHCE(etPH!!.text.toString(), etCE!!.text.toString(),"envio")
                        setEnableField(false)
                        btnSave!!.visibility = View.GONE
                        dialog.dismiss()
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
                    builder.show()
                }
            }
            R.id.btn_check_PHCE_chupa -> {
                if (validateTextViewChup()) {
                    Log.e("clickMonitoring", "click")
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("pH - CE")
                    builder.setMessage("¿Desea guardar la información de chupatubo de esta valvula?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                        savePHCE(etPHChup!!.text.toString(), etCEChup!!.text.toString(),"chupatubo")
                        setEnableFieldChup(false)
                        btnSaveChup!!.visibility = View.GONE
                        dialog.dismiss()
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
                    builder.show()
                }
            }
            R.id.btn_check_PHCE_drain -> {
                if (validateTextViewDrain()) {
                    Log.e("clickMonitoring", "click")
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("pH - CE")
                    builder.setMessage("¿Desea guardar la información de drenado de esta valvula?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                        savePHCE(etPHDrain!!.text.toString(), etCEDrain!!.text.toString(),"drenado")
                        setEnableFieldDrain(false)
                        btnSaveDrain!!.visibility = View.GONE
                        dialog.dismiss()
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
                    builder.show()
                }
            }
            R.id.btn_check_PHCE_squeezed -> {
                if (validateTextViewSqueezed()) {
                    Log.e("clickMonitoring", "click")
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("pH - CE")
                    builder.setMessage("¿Desea guardar la información de exprimido de esta valvula?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                        savePHCE(etPHSqueezed!!.text.toString(), etCESqueezed!!.text.toString(),"exprimido")
                        setEnableFieldSqueezed(false)
                        btnSaveSqueezed!!.visibility = View.GONE
                        dialog.dismiss()
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
                    builder.show()
                }
            }
        }
    }

    private fun validateTextView(): Boolean {
        var validate = false
        when {
            etPH!!.text.isEmpty() -> showSnackBar()
            etCE!!.text.isEmpty() -> showSnackBar()
            else -> validate = true
        }

        return validate
    }

    private fun validateTextViewChup(): Boolean {
        var validate = false
        when {
            etPHChup!!.text.isEmpty() -> showSnackBar()
            etCEChup!!.text.isEmpty() -> showSnackBar()
            else -> validate = true
        }

        return validate
    }

    private fun validateTextViewDrain(): Boolean {
        var validate = false
        when {
            etPHDrain!!.text.isEmpty() -> showSnackBar()
            etCEDrain!!.text.isEmpty() -> showSnackBar()
            else -> validate = true
        }

        return validate
    }

    private fun validateTextViewSqueezed(): Boolean {
        var validate = false
        when {
            etPHSqueezed!!.text.isEmpty() -> showSnackBar()
            etCESqueezed!!.text.isEmpty() -> showSnackBar()
            else -> validate = true
        }

        return validate
    }




    fun showSnackBar() {
        val snackbar = Snackbar.make(fragView, "Todos los campos son requeridos", Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    private fun savePHCE(ph: String, ce: String, type: String) {
        viewModel.saveValveData(ph, ce, type)
        showSaveDialog()
    }

    fun showSaveDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("pH - CE")
        builder.setMessage("Revisión guardada con éxito")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }

}
