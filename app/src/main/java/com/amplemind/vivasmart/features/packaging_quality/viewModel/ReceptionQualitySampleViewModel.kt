package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.core.repository.model.UnitModel
import com.amplemind.vivasmart.vo_core.repository.ReceptionQualitySampleRepository
import io.reactivex.Observable
import javax.inject.Inject

class ReceptionQualitySampleViewModel @Inject constructor(
        private val mRepository: ReceptionQualitySampleRepository
) {

    var carryOrderId: Long = -1

    var carryOrder: CarryOrderModel? = null
    var units = listOf<UnitModel>()

    fun loadUnits(): Observable<List<UnitModel>> =
            mRepository.loadUnits()
                    .doOnNext { units ->
                        this.units = units
                    }

    fun loadCarryOrder(): Observable<CarryOrderModel> =
            mRepository.loadCarryOrder(carryOrderId)
                    .doOnNext { carryOrder ->
                        this.carryOrder = carryOrder
                    }

}
