package com.amplemind.vivasmart.features.phenology.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.databinding.VarietiesTablesPhenologyItemBinding
import com.amplemind.vivasmart.features.phenology.fragments.PhenologyVarietiesFragment
import com.amplemind.vivasmart.features.phenology.models.local.PhenologyVars
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesPhenologyItemViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.VarietiesTablesPhenologyItemViewModel
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.BehaviorSubject

class VarietiesTablesPhenologyAdapter constructor(val variety: VarietiesPhenologyItemViewModel, val fragment: PhenologyVarietiesFragment, val list: MutableList<VarietiesTablesPhenologyItemViewModel>, val phenology_vars: MutableList<PhenologyVars>) : RecyclerView.Adapter<VarietiesTablesPhenologyAdapter.TableVarietiesPhenologyViewHolder>() {
    private val onClickSubject = BehaviorSubject.create<VarietiesTablesPhenologyItemViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TableVarietiesPhenologyViewHolder {
        val binding = VarietiesTablesPhenologyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TableVarietiesPhenologyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TableVarietiesPhenologyViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<VarietiesTablesPhenologyItemViewModel> {
        return onClickSubject
    }

    inner class TableVarietiesPhenologyViewHolder(private val binding: VarietiesTablesPhenologyItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: VarietiesTablesPhenologyItemViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()

            // All plants completed with sign and comment
            if (item.is_complete!!) {
                binding.TVSampling.setBackground(binding.root.resources.getDrawable(R.drawable.ic_button_green))
                binding.TVSampling.setOnClickListener {
                    fragment.showVarietiesRevisionDetail(variety, item, phenology_vars)
                }
            }
            // Revision started but not finished and without sign
            else if (item.revision_id != 0 && !item.is_complete!! && item.revised_plants < item.sampling.toInt()  ) {
                binding.TVSampling.setBackground(binding.root.resources.getDrawable(R.drawable.ic_button_yellow))
                binding.TVSampling.setOnClickListener {
                    fragment.showVarietiesRevisionActivity(variety, item, phenology_vars, false)
                }
            }
            // Revision with all plants without Sign
            else if (!item.is_complete!! && item.revised_plants >= item.sampling.toInt()){
                binding.TVSampling.setBackground(binding.root.resources.getDrawable(R.drawable.ic_button_red))
                binding.TVSampling.setOnClickListener {
                    fragment.showVarietiesRevisionActivity(variety, item, phenology_vars, false)
                }
            }
            // New revision
            else {
                binding.TVSampling.setBackground(binding.root.resources.getDrawable(R.drawable.ic_button_blue))
                binding.TVSampling.setOnClickListener {
                    fragment.showVarietiesRevisionActivity(variety, item, phenology_vars, true)
                }
            }
        }
    }
}
