package com.amplemind.vivasmart.vo_core.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.google.gson.annotations.SerializedName

data class StageResponse(@SerializedName("data") val data: ArrayList<StageModel>)