package com.amplemind.vivasmart.core.repository.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class FinishedProductReportModel(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("crop_id") val crop_id: Int,
        @SerializedName("client_id") val client_id: Int,
        @SerializedName("packing_id")  val packing_id: Int,
        @SerializedName("stage_id") var stage_id: Int,
        @SerializedName("average_weight") var averageWeight: String? = null,

        @SerializedName("sample") val sample: Int,
        @SerializedName("unit_id") val unit_id: Int,
        @SerializedName("export_no") var export_no: Int? = null,
        @SerializedName("damaged_no") var damaged_no: Int? = null,
        @SerializedName("brix")  val brix: Double? = null,
        @SerializedName("sign") var sign: String? = null,
        @SerializedName("issues") var issues: ArrayList<IssuesModel>? = null,
        @SerializedName("images") var images: ArrayList<ImagesModel>? = null,
        @SerializedName("status") var status: Int? = 0
)
