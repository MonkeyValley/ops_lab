package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import java.util.ArrayList

open class CropStageModel (
        @SerializedName("id")                   var id: Int = 0,
        @SerializedName("crop_type_id")         var cropTypeId: Int = 0,
        @SerializedName("graft_name")           var graftName: String? = null,
        @SerializedName("is_active")            var isActive: Boolean = false,
        @SerializedName("is_graft")             var isGraft: Boolean = false,
        @SerializedName("name")                 var name: String? = null,
        @SerializedName("plagues")              var plagues: RealmList<PlaguesModel>? = null,
        @SerializedName("presentation_names")   var presentationNames: String? = null,
        @SerializedName("variety")              var variety: String? = null,
        @SerializedName("crop_type")            var cropType: CropTypeModel? = null)
    : RealmObject(), Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            plaguesArrayToRealmList(parcel.createTypedArrayList(PlaguesModel.CREATOR)),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(CropTypeModel::class.java.classLoader))


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(cropTypeId)
        parcel.writeString(graftName)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeByte(if (isGraft) 1 else 0)
        parcel.writeString(name)
        parcel.writeTypedList(plagues)
        parcel.writeString(presentationNames)
        parcel.writeString(variety)
        parcel.writeParcelable(cropType, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CropStageModel> {

        private fun plaguesArrayToRealmList(array: ArrayList<PlaguesModel>?): RealmList<PlaguesModel> {
            val list = RealmList<PlaguesModel>()
            array?.forEach { list.add(it) }
            return list
        }

        override fun createFromParcel(parcel: Parcel): CropStageModel {
            return CropStageModel(parcel)
        }

        override fun newArray(size: Int): Array<CropStageModel?> {
            return arrayOfNulls(size)
        }
    }



}
