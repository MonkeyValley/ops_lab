package com.amplemind.vivasmart.core.repository.response

import com.google.gson.annotations.SerializedName

data class CropUnitResponse(
        @SerializedName("data") val data: List<CropUnitResponseResult>,
        @SerializedName("total_count") val total_count: Int
)

data class CropUnitResponseResult(
        @SerializedName("id") val id: Int,
        @SerializedName("crop_id") val crop_id: Int,
        @SerializedName("material") val material: String? = null,
        @SerializedName("name") val name: String? = null,
        @SerializedName("code") val code: String? = null,
        @SerializedName("is_premium") val is_premium: Boolean? = false
)
