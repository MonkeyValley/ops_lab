package com.amplemind.vivasmart.features.haulage.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.core.repository.model.TableGrooves
import com.amplemind.vivasmart.databinding.ItemSelectTableBinding
import com.amplemind.vivasmart.features.production_quality.SpinnerTablesViewModel
import androidx.databinding.library.baseAdapters.BR

class HaulageSelectTablesAdapter(val selectedTables: ArrayList<Int>?) : RecyclerView.Adapter<HaulageSelectTablesAdapter.SelectTablesHolder>() {

    private var list = mutableListOf<SpinnerTablesViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectTablesHolder {
        val binding = ItemSelectTableBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SelectTablesHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: SelectTablesHolder, position: Int) {
        val item = list[position]
        if (selectedTables?.any { it == item.getTableId() } == true) {
            item.changeStatus()
        }
        holder.bind(item)
    }

    fun addElements(tables: ArrayList<TableGrooves>?) {
        if (tables != null) {
            this.list = tables.map { SpinnerTablesViewModel(it) }.toMutableList()
            notifyDataSetChanged()
        }
    }

    fun getTablesSelected(): List<Int> {
        val selects = mutableListOf<Int>()
        list.forEach { table ->
            if (table.status) {
                selects.add(table.getTableId())
            }
        }
        return selects
    }

    inner class SelectTablesHolder(val binding: ItemSelectTableBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SpinnerTablesViewModel) {
            binding.setVariable(BR.itemView, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.root.setOnClickListener {
                    selectedTables?.clear()
                    item.changeStatus()
                    notifyDataSetChanged()
                }
            }
        }

    }
}
