package com.amplemind.vivasmart.core.utils

import androidx.databinding.ObservableBoolean
import com.amplemind.vivasmart.core.extensions.isValidPassword
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Validations @Inject constructor() {

    enum class PasswordValidation {
        PASSWORD_MUST_MATCH,
        PASSWORD_ERROR_FORMAT,
        SUCCESS
    }

    var labelError : ObservableBoolean = ObservableBoolean(true)

    fun validatePassword(password: String, repeatPassword: String): PasswordValidation {

        if (!password.isValidPassword()) {
            labelError.set(true)
            return PasswordValidation.PASSWORD_ERROR_FORMAT
        }
        if (!password.equals(repeatPassword)) {
            labelError.set(true)
            return PasswordValidation.PASSWORD_MUST_MATCH
        }
        labelError.set(false)
        return PasswordValidation.SUCCESS

    }

}