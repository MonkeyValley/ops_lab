package com.amplemind.vivasmart.vo_core.repository.responses

import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel
import com.google.gson.annotations.SerializedName

data class ActivitiesResponse (
        @SerializedName("data") val data: List<ActivityModel>,
        @SerializedName("total_count") val totalCount: Int)
