package com.amplemind.vivasmart.features.production_roster

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemChangeLotBinding
import com.amplemind.vivasmart.features.production_lines.viewModel.ItemLotPackageViewModel
import androidx.databinding.library.baseAdapters.BR

class ChangeLotAdapter : RecyclerView.Adapter<ChangeLotAdapter.ChangeLotViewHolder>() {

    private var list = mutableListOf<ItemLotPackageViewModel>()

    private var previous =  - 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChangeLotViewHolder {
        val binding = ItemChangeLotBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ChangeLotViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ChangeLotViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(items: List<ItemLotPackageViewModel>) {
        this.list = items.toMutableList()
        notifyDataSetChanged()
    }

    inner class ChangeLotViewHolder(val binding: ItemChangeLotBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemLotPackageViewModel) {
            binding.setVariable(BR.itemView, item)
            binding.executePendingBindings()
            if (adapterPosition > -1) {
                binding.root.setOnClickListener {
                    if (previous != -1){
                        list[previous].changeStatus()
                    }
                    previous = adapterPosition

                    item.changeStatus()
                    notifyDataSetChanged()
                }
            }
        }

    }

}
