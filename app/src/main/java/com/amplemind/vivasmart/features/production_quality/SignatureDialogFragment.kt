package com.amplemind.vivasmart.features.production_quality

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseDialogFragment
import com.amplemind.vivasmart.core.custom_views.Signature
import com.amplemind.vivasmart.core.custom_views.SignatureListener
import com.amplemind.vivasmart.core.utils.VALIDATE_NO_SIGNATURE
import com.amplemind.vivasmart.features.interfaces.OnSignatureCompleteListener
import com.amplemind.vivasmart.features.packaging_quality.fragment.StartReportDialogFragment

class SignatureDialogFragment : BaseDialogFragment() {

    companion object {
        val TAG = StartReportDialogFragment::class.java.simpleName!!
    }

    private enum class State {
        SIGN_1,
        SIGN_2
    }

    private val PERSON_1 = 0
    private val PERSON_2 = 1

    private var roles : Array<String>? = null
    private var negativeTexts : Array<String>? = null
    private var positiveTexts : Array<String>? = null

    // - Mark Properties
    private var eventListener: OnSignatureCompleteListener? = null
    private lateinit var sign1: Bitmap
    private lateinit var sign2: Bitmap
    private var state = State.SIGN_1

    // - Mark View Variables
    private var signType: TextView? = null
    private var signature: Signature? = null

    fun newInstance(title: String, message: String, one_signature: Boolean, roles: Array<String>? = null, negative: Array<String>? = null, positive: Array<String>? = null
                                    ,total : String = "", name : String = "", mode: Int): SignatureDialogFragment {
        val dialog = SignatureDialogFragment()
        val args = Bundle()
        args.putString("title", title)
        args.putString("message", message)
        args.putString("name", name)
        args.putString("total", total)
        args.putBoolean("isOneSignature",one_signature)
        args.putStringArray("roles", roles)
        args.putStringArray("negative", negative)
        args.putStringArray("positive", positive)
        args.putInt("mode", mode)
        dialog.arguments = args
        return dialog
    }

    private fun getName() : String{
        return arguments!!.getString("name","") ?: ""
    }

    private fun getTotal() : String{
        return arguments!!.getString("total","") ?: ""
    }

    private fun getMode(): Int{
        return arguments!!.getInt("mode",0) ?: 0
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!, R.style.AlertDialogCustom)
        val mView = LayoutInflater.from(context).inflate(R.layout.content_signature_dialog, null, false)
        builder.setView(mView)

        getRoles()
        textButtonsNegative()
        textButtonsPositive()

        val title = mView.findViewById<TextView>(R.id.title_signature)
        val message = mView.findViewById<TextView>(R.id.message_signature)
        val info = mView.findViewById<LinearLayout>(R.id.ln_info)

        if (getName().isNotEmpty()){
            info.visibility = View.VISIBLE
            mView.findViewById<TextView>(R.id.tv_payment).text = getTotal()
            mView.findViewById<TextView>(R.id.tv_user_name).text = getName()
        }

        signature = mView.findViewById(R.id.signature)
        signType = mView.findViewById(R.id.signature_type)



        signature!!.setOnSignatureListener(object: SignatureListener {
            override fun onDraw() {
                (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
                        .setTextColor(ResourcesCompat.getColor(resources, R.color.blue, null))
            }
        })

        mView.findViewById<ImageView>(R.id.btn_reload).setOnClickListener {
            signature!!.clear()
            (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.GRAY)
        }

        signType!!.text = roles!![PERSON_1]

        title.text = getTitle()
        message.text = getMessage()

        builder.setPositiveButton(if (isOneSignature()) "APROBAR" else positiveTexts!![0], null)
        builder.setNegativeButton(negativeTexts!![0]) { _, i -> dismiss() }
        if(getMode() == 1) {
            builder.setNeutralButton(getString(R.string.no_signature), null)
        }

        return builder.create()
    }

    fun isOneSignature(): Boolean {
        return arguments!!.getBoolean("isOneSignature", false)
    }


    fun getTitle(): String {
        return arguments?.getString("title") ?: ""
    }

    private fun getMessage(): String {
        return arguments?.getString("message") ?: ""
    }

    /**
     *  get titles roles and default values
     */
    fun getRoles() {
        roles = if (arguments!!.getStringArray("roles") != null) arguments!!.getStringArray("roles") else  arrayOf(context!!.getString(R.string.signature_rol_1),context!!.getString(R.string.signature_rol_2))
    }

    /**
     *  get text button 1 sign and 2 sign
     *  default 1 Cancelar 2 Cancelar
     */
    fun textButtonsNegative(){
        negativeTexts = if (arguments!!.getStringArray("negative") != null) arguments!!.getStringArray("negative") else  arrayOf(context!!.getString(R.string.cancel),context!!.getString(R.string.cancel))
    }

    /**
     *  get text buttons positive
     *  default 1 Siguiente 2 Listo
     */
    fun textButtonsPositive(){
        positiveTexts = if (arguments!!.getStringArray("positive") != null) arguments!!.getStringArray("positive") else  arrayOf(context!!.getString(R.string.next),context!!.getString(R.string.done))
    }

    override fun onStart() {
        super.onStart()

        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED)
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
            eventListener?.onDismiss()
            dialog!!.dismiss()
        }
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.GRAY)
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (signature!!.hasValidSignature()) {
                when (state) {
                    State.SIGN_1 -> {
                        sign1 = signature!!.save()
                        if (isOneSignature()){
                            eventListener?.completeSign(sign1)
                            dialog!!.dismiss()
                            return@setOnClickListener
                        }

                        signType!!.text = roles!![PERSON_2]

                        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).text = positiveTexts!![1]
                        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.GRAY)
                        state = State.SIGN_2
                    }
                    State.SIGN_2 -> {
                        sign2 = signature!!.save()
                        eventListener?.completeSign(sign1, sign2)
                        dialog!!.dismiss()
                    }
                }
            }
        }
        if(getMode() == 1) {
            (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.GREEN)
            (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener {
                val builder = android.app.AlertDialog.Builder(context)
                builder.setMessage("¿Seguro que desea enviar sin firmar al colaborador?")
                builder.setPositiveButton("Enviar") { msjDialog, _ ->
                    VALIDATE_NO_SIGNATURE = true
                    sign1 = signature!!.save()
                    eventListener?.completeSign(sign1)
                    dialog!!.dismiss()
                    msjDialog.dismiss()
                }
                builder.setNegativeButton(android.R.string.no) { msjDialog, _ -> msjDialog.dismiss() }
                builder.show()
            }
        }
    }

    fun setOnCompleteListener(listener: OnSignatureCompleteListener) {
        eventListener = listener
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.dispose()
    }

}