package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.model.PayrollMenuModel
import com.amplemind.vivasmart.features.menus.getMenuPayroll
import io.reactivex.Observable
import javax.inject.Inject

open class PayrollProductionRepository @Inject constructor() {

    fun getMenus(): Observable<List<PayrollMenuModel>> {
        return Observable.just(getMenuPayroll())
    }

}