package com.amplemind.vivasmart.vo_core.repository.dao

import android.util.Log
import com.amplemind.vivasmart.vo_core.repository.livedata.Changes
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getActiveTime
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getPayment
import com.amplemind.vivasmart.vo_core.repository.models.extensions.isHarvest
import com.amplemind.vivasmart.vo_core.repository.models.extensions.setTimeInHours
import com.amplemind.vivasmart.vo_core.repository.models.realm.*
import com.amplemind.vivasmart.vo_core.repository.responses.ActivityLogsResponse
import com.amplemind.vivasmart.vo_core.utils.currentDate
import com.amplemind.vivasmart.vo_core.utils.currentISODate
import io.reactivex.Observable
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmQuery
import io.realm.Sort
import java.io.File
import javax.inject.Inject

class ActivityLogsDao @Inject constructor(
        private val stagesDao: StagesDao,
        private val activityCodesDao: ActivityCodesDao,
        private val collaboratorsDao: CollaboratorsDao,
        private val groovesDao: GroovesDao
) : RealmDao<ActivityLogModel>(ActivityLogModel::class.java) {

    override fun retrieveServerData(response: Any?): List<ActivityLogModel>? {
        return when (response) {
            is ActivityLogsResponse -> {
                val activityLogs = response.data

                activityLogs.forEach { activityLog: ActivityLogModel ->
                    activityLog.grooves?.forEach { groove ->
                        groove.activityLog = activityLog
                        groove.activityLogUuid = activityLog.uuid
                        groove.activityLogId = activityLog.id
                    }

                   val activityCode =  activityCodesDao.findById(activityLog.activityCodeId)

                    if (activityCode != null) {
                        Log.i(TAG, "Activity Code with uuid ${activityLog.activityCodeUuid} found!")
                    }

                    val collab = collaboratorsDao.findById(activityLog.collaboratorId.toLong())

                    if (collab != null) {
                        activityLog.collaborator = collab
                        activityLog.collaboratorUuid = collab.uuid
                        activityLog.activityCode = activityCode
                    }

                }

                return activityLogs
            }
            else -> null
        }
    }

    fun loadUndone(activityCode: ActivityCodeModel): Observable<Changes<ActivityLogModel>> {

        return mRealm.where(mModelClazz)
                .sort("collaborator.name", Sort.ASCENDING)
                .equalTo("activityCodeUuid", activityCode.uuid)
                .and()
                .equalTo("isDone", false)
                .and()
                .equalTo("isPending", false)
                .and()
                .equalTo("sign", "No Sign")
                .findAll()
                .asChangesetObservable()
                .map{

                    val data = mRealm.copyFromRealm(it.collection)
                    val changes = liveChangeSetConverter(it)
                    changes.data = data

                    return@map changes
                }

    }

    fun loadUnsignedPendingBase(stage: StageModel, realm: Realm = mRealm): RealmQuery<ActivityLogModel> {
        return realm.where(ActivityLogModel::class.java)
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("isDone", false)
                .and()
                .equalTo("isPending", true)
                .and()
                .equalTo("sign", "No Sign")
    }

    fun loadUnsignedPendingLogsInfo(stage: StageModel): Observable<Changes<PendingLogsModel>> {
        return loadUnsignedPendingBase(stage)
                .sort("collaborator.name")
                .distinct("collaboratorUuid")
                .findAll()
                .asChangesetObservable()
                .map{

                    val changes = liveChangeSetConverter(it)

                    val models = mutableListOf<PendingLogsModel>()

                    val realm = Realm.getDefaultInstance()
                    it.collection.forEach { activityLog ->

                        //Deprecated field, defaults to 0
                        val unsignedCount = 0
                        /*
                        val unsignedCount = loadUnsignedPendingBase(stage, realm)
                                .and()
                                .equalTo("collaboratorUuid", activityLog.collaboratorUuid)
                                .and()
                                .equalTo("sign", "No Sign")
                                .count()
                                */

                        val paymentLogs = loadUnsignedPendingBase(stage, realm)
                                .and()
                                .equalTo("collaboratorUuid", activityLog.collaboratorUuid)
                                .findAll()

                        val payment = paymentLogs.sumByDouble { paymentLog -> paymentLog.getPayment() }


                        //Deprecated field, defaults to 0
                        val totalCount = 0
                        //val totalCount = activityLogs.size

                        models.add(PendingLogsModel(
                                activityLog.collaborator
                                        ?: CollaboratorModel(),
                                stage,
                                payment
                        ))
                    }
                    realm.close()

                    return@map Changes(
                            changes.inserted,
                            changes.updated,
                            changes.deleted,
                            models
                    )

                }
    }

    fun loadUnsignedPending(collaborator: CollaboratorModel, stage: StageModel): Observable<Changes<ActivityLogModel>> {
        return mRealm.where(ActivityLogModel::class.java)
                .equalTo("collaboratorUuid", collaborator.uuid)
                .and()
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("isPending", true)
                .and()
                .equalTo("isDone", false)
                .and()
                .equalTo("sign", "No Sign")
                .findAll()
                .asChangesetObservable()
                .map {
                    return@map liveChangeSetConverter(it, true)
                }
    }

    fun loadSignedPendingBase(stage: StageModel, realm: Realm = mRealm): RealmQuery<ActivityLogModel> {
        return realm.where(ActivityLogModel::class.java)
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("isDone", false)
                .and()
                .equalTo("isPending", true)
                .and()
                .notEqualTo("sign", "No Sign")
    }

    fun loadSignedPendingLogsInfo(stage: StageModel): Observable<Changes<PendingLogsModel>> {
        return loadSignedPendingBase(stage)
                .sort("collaborator.name")
                .distinct("collaboratorUuid")
                .findAll()
                .asChangesetObservable()
                .map{

                    Log.i(TAG, "LOADED ${it.collection.size}")
                    val changes = liveChangeSetConverter(it)

                    val models = mutableListOf<PendingLogsModel>()

                    val realm = Realm.getDefaultInstance()
                    it.collection.forEach { activityLog ->

                        val unsignedCount = loadUnsignedPendingBase(stage, realm)
                                .and()
                                .equalTo("collaboratorUuid", activityLog.collaboratorUuid)
                                .and()
                                .equalTo("sign", "No Sign")
                                .count()

                        val totalCount = loadSignedPendingBase(stage, realm)
                                .and()
                                .equalTo("collaboratorUuid", activityLog.collaboratorUuid)
                                .count()

                        val paymentLogs = loadSignedPendingBase(stage, realm)
                                .and()
                                .equalTo("collaboratorUuid", activityLog.collaboratorUuid)
                                .findAll()

                        val payment = paymentLogs.sumByDouble { paymentLog -> paymentLog.getPayment() }

                        models.add(PendingLogsModel(
                                activityLog.collaborator
                                        ?: CollaboratorModel(),
                                stage,
                                payment
                        ))

                    }
                    realm.close()

                    return@map Changes(
                            changes.inserted,
                            changes.updated,
                            changes.deleted,
                            models
                    )

                }
    }

    fun loadSignedPending(collaborator: CollaboratorModel, stage: StageModel): Observable<Changes<ActivityLogModel>> {
        return mRealm.where(ActivityLogModel::class.java)
                .equalTo("collaboratorUuid", collaborator.uuid)
                .and()
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("isPending", true)
                .and()
                .equalTo("isDone", false)
                .and()
                .notEqualTo("sign", "No Sign")
                .findAll()
                .asChangesetObservable()
                .map {changes ->
                    return@map liveChangeSetConverter(changes, true)
                }
    }

    fun create(activityCode: ActivityCodeModel, collaborator: CollaboratorModel): ActivityLogModel? {
        return create(activityCode, listOf(collaborator))?.first()
    }

    fun create(activityCode: ActivityCodeModel, collaborators: List<CollaboratorModel>): List<ActivityLogModel>? {
        val logs = mutableListOf<ActivityLogModel>()

        val collabsCopy = collaboratorsDao.copy(collaborators)

        val realm = Realm.getDefaultInstance()

        collabsCopy.forEach{ collab ->

            val count = realm.where(ActivityLogModel::class.java)
                    .beginGroup()
                        .equalTo("collaboratorId", collab.id)
                        .or()
                        .equalTo("collaboratorUuid", collab.uuid)
                    .endGroup()
                    .and()
                    .equalTo("isPending", false)
                    .and()
                    .equalTo("isDone", false)
                    .and()
                    .equalTo("sign", "No Sign")
                    .count()

            if (count == 0L) {

                collab.isWorking = true

                logs.add(
                        ActivityLogModel(
                                activityCodeId = activityCode.id,
                                activityCodeUuid = activityCode.uuid,
                                collaboratorId = collab.id,
                                collaboratorUuid = collab.uuid,
                                collaborator = collab,
                                activityCode = activityCode
                        )
                )

            }

        }


        val results = upsert(logs)

        if (results?.isNotEmpty() == true) {

            val stage = realm.where(StageModel::class.java)
                    .equalTo("id", activityCode.stageId)
                    .findFirst()

            stagesDao.setLotAsActive(stage!!, true)
        }

        realm.close()

        return results
    }

    fun setUnits(activityLog: ActivityLogModel, quantity: Int): ActivityLogModel? {
        val obj = copy(activityLog)
        obj?.quantity = quantity
        Log.i(TAG, obj?.toString())
        return upsert(obj!!) ?: activityLog
    }

    fun setUnits(activityLog: ActivityLogModel, goodQuantity: Int, badQuantity: Int): ActivityLogModel? {
        val obj = copy(activityLog)
        obj?.quantity = goodQuantity
        obj?.badQuantity = badQuantity
        return upsert(obj!!)
    }

    fun assingGrooves(activityLog: ActivityLogModel, grooves: List<GrooveModel>): ActivityLogModel? {
        val logObj = copy(activityLog)
        val groovesObj = groovesDao.copy(grooves)
        logObj?.grooves = RealmList()
        logObj?.grooves?.addAll(groovesObj)
        logObj?.quantity = grooves.size
        return upsert(logObj!!) ?: activityLog
    }

    fun setQuantityAndTime(activityLog: ActivityLogModel, quantity: Int, timeInHours: Int): ActivityLogModel? {
        val obj = copy(activityLog)

        if (obj != null) {
            obj.quantity = quantity
            obj.setTimeInHours(timeInHours)
            upsert(obj)
        }

        return obj
    }

    fun startTimer(activityLog: ActivityLogModel): ActivityLogModel? {
        return startTimer(listOf(activityLog))?.first()
    }

    fun startTimer(activityLogs: List<ActivityLogModel>): List<ActivityLogModel>? {
        val objs = copy(activityLogs)
        objs.forEach {
            it.startTime = currentDate()
            it.isPaused = false
            Log.i("Update time", "Timer started at: ${it.startTime}")
        }
        return upsert(objs)
    }

    fun pauseTimer(activityLog: ActivityLogModel): ActivityLogModel? {
        return pauseTimer(listOf(activityLog))?.first()
    }

    fun pauseTimer(activityLogs: List<ActivityLogModel>): List<ActivityLogModel>? {
        val objs = copy(activityLogs)
        objs.forEach{
            if (!it.isPaused) {
                it.time = it.getActiveTime()
                it.isPaused = true
                Log.i("Update time", "Time updated to ${it.time}")
                Log.i("Update time", "Timer was started at: ${it.startTime}")
            }
        }
        return upsert(objs)
    }

    fun remove(activityLog: ActivityLogModel): ActivityLogModel? {
        val realm = Realm.getDefaultInstance()

        val removed = delete(activityLog.uuid)

        if (removed != null) {
            collaboratorsDao.markAsWorking(activityLog.collaborator!!, false)
        }

        if (!hasActiveLogs(activityLog.activityCode?.stage!!)) {
            stagesDao.setLotAsActive(activityLog.activityCode!!.stage!!, false)
        }

        realm.close()

        return removed
    }

    fun finish(activityLog: ActivityLogModel, sign: File? = null): ActivityLogModel? {
        return finish(listOf(activityLog), sign)?.first()
    }

    fun finish(activityLogs: List<ActivityLogModel>, sign: File? = null): List<ActivityLogModel>? {
        val objs = mutableListOf<ActivityLogModel>()

        activityLogs.forEach {
            if (it.quantity > 0) {

                val obj = copy(it)
                if (obj != null) {

                    obj.sign = sign?.name ?: "No Sign"
                    obj.time = obj.getActiveTime()
                    obj.isPaused = true
                    obj.isPending = true

                    val result = upsert(obj)

                    if (result != null) {
                        collaboratorsDao.markAsWorking(result.collaborator!!, false)
                    }

                    objs.add(obj)
                }
            }
        }

        return objs
    }

    fun updatePaymentMode(activityLogs: List<ActivityLogModel>, isTraining: Boolean? = null, isControl: Boolean? = null, isPractice: Boolean? = null): List<ActivityLogModel>? {

        val objs = copy(activityLogs)

        objs.forEach { activityLog ->
            activityLog.isTraining = isTraining ?: activityLog.isTraining
            activityLog.isControl = isControl ?: activityLog.isControl
            activityLog.isPractice = isPractice ?: activityLog.isPractice
        }

        return upsert(objs)

    }

    fun sign(activityLogs: List<ActivityLogModel>, signature: File?, isTraining: Boolean = false, isPractice: Boolean = false, isControl: Boolean = false): List<ActivityLogModel> {
        val objs = mutableListOf<ActivityLogModel>()

        activityLogs.forEach {
            if (it.isPending) {
                val obj = copy(it)
                if (obj != null) {
                    obj.sign = signature?.name ?: "No Sign"
                    obj.isTraining = isTraining
                    obj.isPractice = isPractice
                    obj.isControl = isControl
                    objs.add(obj)
                }
            }
        }

        upsert(objs)

        return objs
    }


    fun setAsDone(activityLogs: List<ActivityLogModel>): List<ActivityLogModel> {

        val objs = mutableListOf<ActivityLogModel>()

        activityLogs.forEach {
            if (it.isPending) {
                val obj = copy(it)
                if (obj != null) {
                    obj.isPaused = true
                    obj.isPending = false
                    obj.isDone = true
                    objs.add(obj)
                }
            }
        }

        upsert(objs)

        if (objs.isNotEmpty()) {
            stagesDao.setLotStatus(objs.first().activityCode!!.stage!!, true)
        }

        return objs

    }

    fun setAsDone(stage: StageModel): List<ActivityLogModel> {

        val realm = Realm.getDefaultInstance()

        val results = realm.where(ActivityLogModel::class.java)
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("isPending", true)
                .findAll()

        val objs = setAsDone(results)

        realm.close()

        return objs
    }

    fun hasActiveLogs(stage: StageModel): Boolean {

        val realm = Realm.getDefaultInstance()

        val count = realm.where(ActivityLogModel::class.java)
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("sign", "No Sign")
                .and()
                .equalTo("isPending", false)
                .and()
                .equalTo("isDone", false)
                .count()

        realm.close()

        return count > 0

    }

    fun countActiveLogsForStage(stage: StageModel): Observable<Int> {

        return mRealm.where(ActivityLogModel::class.java)
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("sign", "No Sign")
                .and()
                .equalTo("isPending", false)
                .and()
                .equalTo("isDone", false)
                .findAll()
                .asChangesetObservable()
                .map{changes ->
                    return@map changes.collection.size
                }

    }

    fun countActiveLogsForActivity(stage: StageModel, activity: ActivityModel): Observable<Int> {

        return mRealm.where(ActivityLogModel::class.java)
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("activityCode.activityId", activity.id)
                .and()
                .equalTo("sign", "No Sign")
                .and()
                .equalTo("isPending", false)
                .and()
                .equalTo("isDone", false)
                .findAll()
                .asChangesetObservable()
                .map{changes ->
                    return@map changes.collection.size
                }

    }

    fun retrieveDailyReportForDate(stage: StageModel, date: String): List<DailyReportModel>? {
        val realm = Realm.getDefaultInstance()

        val activityLogs = realm.where(ActivityLogModel::class.java)
                .sort("collaborator.name")
                .distinct("collaboratorUuid")
                .equalTo("activityCode.stageUuid", stage.uuid)
                .and()
                .beginsWith("date", date)
                .and()
                .equalTo("isDone", true)
                .findAll()

        val results = activityLogs?.map { activityLog ->
            val collabLogs = realm.where(ActivityLogModel::class.java)
                    .equalTo("activityCode.stageUuid", stage.uuid)
                    .and()
                    .equalTo("collaboratorUuid", activityLog.collaboratorUuid)
                    .and()
                    .beginsWith("date", date)
                    .and()
                    .equalTo("isDone", true)
                    .findAll()

            val total = collabLogs.sumByDouble { collabLog ->
                collabLog.getPayment()
            }

            val code = activityLog.collaborator?.employeeCode ?: "S/C"
            val name = activityLog.collaborator?.name ?: "S/N"

            return@map DailyReportModel(code, name, total)

        }

        realm.close()

        return results
    }

    fun retrieveTodaysHarvestTotalBoxCount(activityLog: ActivityLogModel): Int {
        val realm = Realm.getDefaultInstance()

        val results = realm.where(ActivityLogModel::class.java)
                .equalTo("activityCode.stageId", activityLog.activityCode!!.stageId)
                .and()
                .equalTo("date", currentISODate("yyyy-MM-dd"))
                .and()
                .equalTo("isDone", false)
                .and()
                .equalTo("isPending", false)
                .and()
                .equalTo("sign", "No Sign")
                .findAll()

        val total = results.sumBy{
            if (it.uuid != activityLog.uuid && it.activityCode?.activity?.isHarvest == true) {
                it.quantity + (it.badQuantity ?: 0)
            }
            else 0
        }

        realm.close()

        return total
    }

    fun thereAreUnsigned(stage: StageModel): Boolean {

        val realm = Realm.getDefaultInstance()

        val count = realm.where(ActivityLogModel::class.java)
                .equalTo("activityCode.stageId", stage.id)
                .and()
                .equalTo("isPending", true)
                .and()
                .equalTo("sign", "No Sign")
                .count()

        realm.close()

        return count > 0L
    }

}