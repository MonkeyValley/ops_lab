package com.amplemind.vivasmart.features.mipe.viewModel

import android.content.Context
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.mipe.service.MipeService
import javax.inject.Inject

class MipeSelectPlantViewModel @Inject constructor(private val apiErrors: HandleApiErrors,
                                                   private val service: MipeService) : BaseViewModel() {

    fun setContext(context: Context){
        service.context = context
        service.preferences  = UserAppPreferences(context, null, null)
    }

    fun saveData(lotId: Int, table: String, groove: Int, week: Int){
        service.savePlantGroove(lotId, table, groove, week)
    }

}