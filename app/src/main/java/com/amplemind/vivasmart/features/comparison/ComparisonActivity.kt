package com.amplemind.vivasmart.features.comparison

import android.app.Activity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.comparison.adapter.ComparisonAdapter
import com.amplemind.vivasmart.features.comparison.viewModel.ComparisonViewModel
import com.amplemind.vivasmart.features.comparison.viewModel.ItemComparisonViewModel
import kotlinx.android.synthetic.main.content_comparison_activity.*
import javax.inject.Inject

class ComparisonActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ComparisonViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_comparison_activity)
        viewModel.setIntent(intent)

        setupToolbar()

        setupRecycler()

        loadComparison()

        addListeners()

        btn_sign.addThrottle().subscribe(this::sendSigns).addTo(subscriptions)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                return false
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addListeners() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getDataComparison().subscribe(this::setData).addTo(subscriptions)
    }

    private fun getFlow(): ComparisonViewModel.ComparisonFlow {
        return ComparisonViewModel.ComparisonFlow.from(intent.getIntExtra("flow", 1))
    }

    private fun loadComparison() {
        viewModel.loadComparison(getFlow()).addTo(subscriptions)
    }

    private fun setData(comparison: List<ItemComparisonViewModel>) {
        if (comparison.isEmpty()) {
            ln_header_comparison.visibility = View.GONE
            view_line.visibility = View.GONE
            ln_no_data.visibility = View.VISIBLE
        }
        (rv_comparation.adapter as ComparisonAdapter).addElements(comparison.toMutableList())
    }

    private fun setupRecycler() {
        rv_comparation.hasFixedSize()
        rv_comparation.layoutManager = LinearLayoutManager(this)
        rv_comparation.itemAnimator = DefaultItemAnimator()
        rv_comparation.adapter = ComparisonAdapter()
    }

    private fun setupToolbar() {
        val mToolbar = toolbar_comparasion
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.comparison_title)
        tv_current_item_new.text = viewModel.getTitleDate()
    }

    private fun sendSigns(view: Any) {
        setResult(Activity.RESULT_OK)
        finish()
    }


}