package com.amplemind.vivasmart.features.profile.viewModel

import com.amplemind.vivasmart.BuildConfig
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject

class ProfileViewModel @Inject constructor(private val repository: ProfileRepository,
                                           private val account: AccountRepository,
                                           private val apiErrors: HandleApiErrors,
                                           private val preferences: UserAppPreferences) {

    private val progressStatus: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val onSucessUpload: BehaviorSubject<ProfileRepository.UploadImageResponse> = BehaviorSubject.create()
    private val onSucessUpdateImage: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val successLogout: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private val onFailUpload: BehaviorSubject<String> = BehaviorSubject.create()

    var userImage: File? = null

    fun updateUserImage(token: String, path: String?): Disposable {
        return repository.updateUserImage(token, ProfileRepository.UpdateUserImageRequest(path))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    //TODO: Do update user info with the model
                    onSucessUpdateImage.onNext(true)
                }, {
                    progressStatus.onNext(false)
                    onFailUpload.onNext(apiErrors.handleLoginError(it))
                })
    }


    fun deleteUserImage(): Disposable {
        deleteUserImagePreferences()
        return updateUserImage(preferences.token, null)
    }

    fun changeProfileImage(file: File, token: String): Disposable {
        return repository.uploadImage(file, "user")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    val user = Gson().fromJson(preferences.userInfo, UserModel::class.java)
                    user.image = it.relative_url
                    preferences.userInfo = Gson().toJson(user)
                    onSucessUpload.onNext(it)
                }, {
                    progressStatus.onNext(false)
                    onFailUpload.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun logOut(): Disposable {
        return repository.logOut()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    progressStatus.onNext(false)
                    successLogout.onNext(true)
                }, {
                    progressStatus.onNext(false)
                    onFailUpload.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getUserImage(): String {
        return "${BuildConfig.SERVER_URL}uploads/${getUserRelativeUrl()}"
    }

    private fun getUserRelativeUrl(): String? {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java).image
    }

    fun deleteUserImagePreferences(){
        val user = Gson().fromJson(preferences.userInfo, UserModel::class.java)
        user.image = null
        preferences.userInfo = Gson().toJson(user)
    }

    fun getUserEmail(): String {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java).email
    }

    fun getUserName(): String {
        return Gson().fromJson(preferences.userInfo, UserModel::class.java).name
    }

    fun observeProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun observeSuccessUploadImage(): BehaviorSubject<ProfileRepository.UploadImageResponse> {
        return onSucessUpload
    }

    fun observeFailUploadImage(): BehaviorSubject<String> {
        return onFailUpload
    }

    fun observeOnSucessUpdateImage(): BehaviorSubject<Boolean> {
        return onSucessUpdateImage
    }

    fun onSuccessLogOut(): BehaviorSubject<Boolean> {
        return successLogout
    }

}