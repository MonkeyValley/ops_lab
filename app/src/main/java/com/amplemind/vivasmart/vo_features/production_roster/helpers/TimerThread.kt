package com.amplemind.vivasmart.vo_features.production_roster.helpers

import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.Log
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivityLogsDao
import com.amplemind.vivasmart.vo_core.repository.models.extensions.getActiveTime
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityLogModel
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

class TimerThread @Inject constructor(
        private val mActivityLogsDao: ActivityLogsDao,
        private val mEventBus: EventBus
) : HandlerThread(TAG) {

    companion object {
        val TAG: String = TimerThread::class.java.simpleName
    }

    private lateinit var mHandler: Handler
    private val mMainHandler = Handler(Looper.getMainLooper())

    private lateinit var mStageUuid: String
    private lateinit var mActivityUuid: String

    private var mNextUpdate: Long = Long.MAX_VALUE
    private var mNextUpdateLogs = mutableListOf<ActivityLogModel>()

    private var mIsRunning = AtomicBoolean(false)
    private var mIsLooperPrepared = AtomicBoolean(false)

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mHandler = Handler(looper)

        mIsLooperPrepared.set(true)

        refreshTimer()

    }

    fun loadActivityLogs(stageUuid: String, activityUuid: String) {
        if (!mIsRunning.get()) {
            mStageUuid = stageUuid
            mActivityUuid = activityUuid

            start()
        }
    }

    fun cleanUp() {
        if (mIsLooperPrepared.get()) {
            mHandler.removeCallbacksAndMessages(null)
            mHandler.post {
                mActivityLogsDao.cleanUp()
            }
            quitSafely()
        }
    }

    fun refreshTimer() {
        if (mIsLooperPrepared.get()) {
            mIsRunning.set(true)
            mHandler.removeCallbacksAndMessages(null)
            updateTimers(0)
        }
    }

    fun pauseTimer() {
        mHandler.removeCallbacksAndMessages(null)
    }

    private fun sendUpdateEvents() {
        mNextUpdateLogs.forEach {activityLog ->
            if (activityLog.isValid) {
                val event = ActivityLogUpdateTimeEvent(activityLog.activityCodeUuid, activityLog.uuid)
                mMainHandler.post {
                    mEventBus.send(event)
                }
            }
        }
        mNextUpdateLogs.clear()
    }

    private fun addUpdateEvent(activityLog: ActivityLogModel) {
        mNextUpdateLogs.add(activityLog)
    }

    private fun updateTimers(delayInSeconds: Long) {
        mMainHandler.post {
            if (mIsRunning.get()) {

                mHandler.postDelayed({

                    sendUpdateEvents()

                    var smallestRemainingSeconds = Long.MAX_VALUE

                    mActivityLogsDao.findActiveLogs(mStageUuid, mActivityUuid) { activityLog ->
                        if (!activityLog.isPaused) {
                            val currentTime = activityLog.getActiveTime()

                            val remainingSeconds = 60 - (currentTime % 60)

                            if (remainingSeconds < smallestRemainingSeconds) {
                                smallestRemainingSeconds = remainingSeconds
                                mNextUpdateLogs.clear()
                                addUpdateEvent(activityLog)
                            } else if (remainingSeconds == smallestRemainingSeconds) {
                                addUpdateEvent(activityLog)
                            }
                        }
                    }

                    if (smallestRemainingSeconds != Long.MAX_VALUE) {
                        mNextUpdate = smallestRemainingSeconds
                        Log.i(TAG, "Next timers update in $mNextUpdate seconds...")
                        updateTimers(mNextUpdate)
                    }

                }, delayInSeconds * 1000)
            }
        }
    }

    data class ActivityLogUpdateTimeEvent(
            val activityCodeUuid: String,
            val activityLogUuid: String
    )
}