package com.amplemind.vivasmart.core.custom_views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.R.attr.path
import android.util.Log
import android.view.MotionEvent



class Signature : View {

    private val strokeWidth: Float = 5f
    private val halfStrokeWidth = strokeWidth / 2
    private val paint = Paint()
    private val path = Path()

    private var lastTouchX: Float = 0f
    private var lastTouchY: Float = 0f
    private var dirtyRect = RectF()
    private var signatureListener: SignatureListener? = null
    private var isValidSignature = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        paint.isAntiAlias = true
        paint.color = Color.BLACK
        paint.style = Paint.Style.STROKE
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeWidth = strokeWidth
    }

    fun save() : Bitmap {
        val bitmap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
//        val bgDrawable = this.background
//        bgDrawable.draw(canvas)
        this.draw(canvas)
        clear()
        return bitmap
    }

    fun clear() {
        isValidSignature = false
       path.reset()
        invalidate()
    }

    fun hasValidSignature(): Boolean {
        return isValidSignature
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.drawPath(path, paint)
    }

    private fun drawLine(eventX: Float, eventY: Float, event: MotionEvent) {
        isValidSignature = true
        resetDirtyRect(eventX, eventY)
        val historySize = event.historySize
        for (i in 0 until historySize) {
            val historicalX = event.getHistoricalX(i)
            val historicalY = event.getHistoricalY(i)
            expandDirtyRect(historicalX, historicalY)
            path.lineTo(historicalX, historicalY)
        }
        path.lineTo(eventX, eventY)
        if (signatureListener != null) {
            signatureListener!!.onDraw()
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val eventX = event.x
        val eventY = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                path.moveTo(eventX, eventY)
                lastTouchX = eventX
                lastTouchY = eventY
                return true
            }

            MotionEvent.ACTION_MOVE -> {
                drawLine(eventX, eventY, event)
            }

            MotionEvent.ACTION_UP -> {
                if (lastTouchX != eventX && lastTouchY != eventY) {
                    drawLine(eventX, eventY, event)
                }
                resetDirtyRect(eventX, eventY)
                val historySize = event.historySize
                for (i in 0 until historySize) {
                    val historicalX = event.getHistoricalX(i)
                    val historicalY = event.getHistoricalY(i)
                    expandDirtyRect(historicalX, historicalY)
                    path.lineTo(historicalX, historicalY)
                }
                path.lineTo(eventX, eventY)
            }
            else -> {
                return false
            }
        }

        invalidate((dirtyRect.left - halfStrokeWidth).toInt(),
                (dirtyRect.top - halfStrokeWidth).toInt(),
                (dirtyRect.right + halfStrokeWidth).toInt(),
                (dirtyRect.bottom + halfStrokeWidth).toInt())

        lastTouchX = eventX
        lastTouchY = eventY

        return true
    }

    private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
        if (historicalX < dirtyRect.left) {
            dirtyRect.left = historicalX
        } else if (historicalX > dirtyRect.right) {
            dirtyRect.right = historicalX
        }

        if (historicalY < dirtyRect.top) {
            dirtyRect.top = historicalY
        } else if (historicalY > dirtyRect.bottom) {
            dirtyRect.bottom = historicalY
        }
    }

    private fun resetDirtyRect(eventX: Float, eventY: Float) {
        dirtyRect.left = Math.min(lastTouchX, eventX)
        dirtyRect.right = Math.max(lastTouchX, eventX)
        dirtyRect.top = Math.min(lastTouchY, eventY)
        dirtyRect.bottom = Math.max(lastTouchY, eventY)
    }

    fun setOnSignatureListener(listener: SignatureListener) {
        signatureListener = listener
    }

}

interface SignatureListener {
    fun onDraw()
}