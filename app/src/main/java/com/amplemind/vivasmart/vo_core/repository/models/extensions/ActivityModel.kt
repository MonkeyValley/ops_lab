package com.amplemind.vivasmart.vo_core.repository.models.extensions

import android.content.Context
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityModel


enum class ActivityType(val value: Int) {
    NONE (0),
    PIECE(1),
    HOURS(2)
}


val ActivityModel?.type: ActivityType
        get() = ActivityType.values().find { type ->
            type.value == (this?.activityTypeId ?: 0)
        } ?: ActivityType.NONE

val ActivityModel?.realCost: Double
    get() = this?.cost ?: this?.unit?.cost ?: 0.0

val ActivityModel?.isHarvest: Boolean
    get() = this?.activitySubCategoryId == 1

fun ActivityModel?.getUnitName(context: Context): String =
        if (this?.isGrooves == true)
            context.getString(R.string.units_groove)
        else
            this?.unit?.name ?: context.getString(R.string.units_general)

