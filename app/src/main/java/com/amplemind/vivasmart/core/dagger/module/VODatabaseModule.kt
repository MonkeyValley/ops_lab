package com.amplemind.vivasmart.core.dagger.module

import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.dao.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class VODatabaseModule {

    @Provides
    @Singleton
    internal fun provideCollaboratorsDao(): CollaboratorsDao {
        return CollaboratorsDao()
    }

    @Provides
    @Singleton
    internal fun provideStagesDao(): StagesDao {
        return StagesDao()
    }


    @Provides
    @Singleton
    internal fun provideActivitiesDao(): ActivitiesDao {
        return ActivitiesDao()
    }

    @Provides
    @Singleton
    internal fun provideActivityCodeDao(activitiesDao: ActivitiesDao, stagesDao: StagesDao): ActivityCodesDao {
        return ActivityCodesDao(activitiesDao, stagesDao)
    }


    @Provides
    @Singleton
    internal fun provideActivityLogsDao(stagesDao: StagesDao, activityCodesDao: ActivityCodesDao, groovesDao: GroovesDao, collaboratorsDao: CollaboratorsDao): ActivityLogsDao {
        return ActivityLogsDao(stagesDao, activityCodesDao, collaboratorsDao, groovesDao)
    }

    @Provides
    @Singleton
    internal fun provideGroovesDao(): GroovesDao {
        return GroovesDao()
    }

    @Provides
    @Singleton
    internal fun provideMessageDao(): MessageDao {
        return MessageDao()
    }

    @Provides
    @Singleton
    internal fun provideUploadObjectDao(realmHolder: RealmHolder): UploadObjectDao {
        return UploadObjectDao(realmHolder)
    }
}
