package com.amplemind.vivasmart.vo_core.utils

import android.os.Parcel
import android.os.Parcelable
import io.reactivex.Flowable
import io.realm.RealmList
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

fun formatPayment(payment: Double): String {
    return "$ %.4f".format(payment)
}

fun formatTime(timeInSeconds: Long): String {
    val hours = timeInSeconds / 3600
    val minutes = timeInSeconds % 3600 / 60

    return "%02d:%02d".format(hours.toInt(), minutes.toInt())
}

fun formatISODate(year: Int, month: Int, day: Int, isZeroBasedMonth: Boolean = true): String {
    return "%d-%02d-%02d".format(year, if (isZeroBasedMonth) month + 1 else month, day)
}

fun formatHumanDate(year: Int, month: Int, day: Int, isZeroBasedMonth: Boolean = true): String {

    val months = arrayOf(
            "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    )

    return "%02d / %s / %d".format(day, months[if (isZeroBasedMonth) month else month-1], year)
}

fun currentISODate(format: String = "yyyy-MM-dd HH:mm:ss"): String =
        SimpleDateFormat(format, Locale.US)
            .format(Calendar.getInstance().time)

fun dateFromToday(days: Int, format: String = "yyyy-MM-dd HH:mm:ss"): String =
        SimpleDateFormat(format, Locale.US).format(Calendar.getInstance().apply {
            add(Calendar.DAY_OF_YEAR, days)
        }.time)


fun currentDate(): Long {
    return currentDateInMillis() / 1000
}

fun currentDateInMillis(): Long {
    return System.currentTimeMillis()
}

fun buildCountdownTimer(seconds: Long, initialDelay: Long = 0): Flowable<Long> =
        Flowable.intervalRange(0, seconds + 1, initialDelay, 1, TimeUnit.SECONDS)
                .map { value ->
                    seconds - value
                }

fun <T>readRealmList(parcel: Parcel, creator: Parcelable.Creator<T>): RealmList<T> {
    val list = RealmList<T>()
    parcel.readTypedList(list, creator)
    return list
}

