package com.amplemind.vivasmart.features.production_roster

import android.os.SystemClock
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemActivitiePayrollMenuBinding
import androidx.databinding.library.baseAdapters.BR
import io.reactivex.subjects.PublishSubject

class MenuListPayrollAdapter : RecyclerView.Adapter<MenuListPayrollAdapter.MenuListPayrollViewHolder>() {

    private var list = mutableListOf<ItemMenuPayrollViewModel>()
    private var lastClickTime: Long = 0
    private val clickSubject = PublishSubject.create<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuListPayrollViewHolder {
        val binding = ItemActivitiePayrollMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MenuListPayrollViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MenuListPayrollViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun addElements(data: List<ItemMenuPayrollViewModel>) {
        list.addAll(data)
        notifyDataSetChanged()
    }

    fun onClickItem(): PublishSubject<String> {
        return clickSubject
    }


    inner class MenuListPayrollViewHolder(private val binding: ItemActivitiePayrollMenuBinding) : RecyclerView.ViewHolder(binding.root) {


        private lateinit var item: ItemMenuPayrollViewModel

        fun bind(item: ItemMenuPayrollViewModel) {

            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            this.item = item
            if (adapterPosition > -1) {
                binding.root.isEnabled = item.active
                binding.root.setOnClickListener {
                    if ( SystemClock.elapsedRealtime() - lastClickTime < 2000){
                        return@setOnClickListener
                    }

                    clickSubject.onNext(item.name)
                    lastClickTime = SystemClock.elapsedRealtime()

                }
            }
        }
    }
}