package com.amplemind.vivasmart.features.production_roster.adapters

import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.features.production_roster.fragments.interfaces.ControlTimerManager
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel


class TimerManagerUsersAdapter(private val supportFragmentManager: FragmentManager) : RecyclerView.Adapter<TimerUserViewHolder>(), ControlTimerManager {

    private var list = mutableListOf<TimerUserViewModel>()
    private var name_unit: String? = null
    private var cost: Double? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimerUserViewHolder {
        return TimerUserViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_users_timer, parent, false), supportFragmentManager)
    }

    override fun onViewRecycled(holder: TimerUserViewHolder) {
        holder.onRecycle()
        super.onViewRecycled(holder)
    }

    override fun onStartGeneralTimer() {
        list.forEach { viewModel ->
            viewModel.startTimer()
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TimerUserViewHolder, position: Int) {
        holder.bind(list[position], position, name_unit, cost)
    }

    fun addElements(activities: List<TimerUserViewModel>, name_unit: String?, cost: Double?) {
        list = activities as MutableList<TimerUserViewModel>
        this.name_unit = name_unit
        this.cost = cost
        notifyDataSetChanged()
    }


    override fun onNotifyItemChanged(position: Int) {
        notifyItemChanged(position)
    }

    override fun onNotifyDataSetChange() {
        notifyDataSetChanged()
    }

    override fun onRemoveItem(position: Int) {
        if (list.size > position) {
            list.removeAt(position)
        }
        //notifyItemRemoved(position)
        notifyDataSetChanged()
    }

    override fun onPauseGeneralTimer() {
        list.forEach { viewModel ->
            viewModel.stopTimer()
        }
        notifyDataSetChanged()
    }

}