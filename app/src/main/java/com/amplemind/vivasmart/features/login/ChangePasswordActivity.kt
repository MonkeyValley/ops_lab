package com.amplemind.vivasmart.features.login


import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addThrottle
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isValidPassword
import com.amplemind.vivasmart.core.extensions.onChange
import com.amplemind.vivasmart.core.repository.AccountRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.Validations
import com.amplemind.vivasmart.features.login.viewModel.ChangePasswordViewModel
import kotlinx.android.synthetic.main.activity_change_password.*
import javax.inject.Inject

class ChangePasswordActivity : BaseActivity() {

    @Inject
    lateinit var viewModel : ChangePasswordViewModel

    @Inject
    lateinit var preferences : UserAppPreferences

    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        initializeUI()
        setProgressDialog()

        viewModel.onValidCurrentPassword().subscribe(this::onValidCurrentPassword).addTo(subscriptions)
        viewModel.onValidNewPassword().subscribe(this::onValidNewPassword).addTo(subscriptions)
        viewModel.onProgressStatus().subscribe(this::onProgressStatus).addTo(subscriptions)
        viewModel.onSuccessChangePassword().subscribe(this::onSuccessChangePassword).addTo(subscriptions)
        viewModel.onFailChangePassword().subscribe(this::onFailChangePassword).addTo(subscriptions)
    }

    private fun setProgressDialog() {
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage(getString(R.string.server_connection))
        progressDialog.setCancelable(false)
    }

    private fun onProgressStatus(show: Boolean) {
        if (show) progressDialog.show() else progressDialog.dismiss()
    }

    private fun onSuccessChangePassword(response: AccountRepository.ChangePasswordResponse) {
        preferences.token = response.token
        finish()
    }

    private fun onFailChangePassword(error: String) {
        val dialog = AlertDialog.Builder(this)
                .setTitle(getString(R.string.app_name))
                .setMessage(error)
                .setPositiveButton("Ok", DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                })
        dialog.show()
    }

    private fun onValidCurrentPassword(isValid: Boolean) {
        new_password.isEnabled = isValid
        confirm_password.isEnabled = isValid
        if (isValid) {
            input_current_password.isErrorEnabled = false
            current_password.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(this, R.drawable.ic_done), null)
        } else {
            current_password.setCompoundDrawables(null, null, null, null)
            input_current_password.error = getString(R.string.invalid_password)
        }
    }

    private fun onValidNewPassword(validation: Validations.PasswordValidation) {
        when(validation) {
            Validations.PasswordValidation.SUCCESS -> {
                input_confirm_password.isErrorEnabled = false
                viewModel.changePassword(AccountRepository.ChangePasswordRequest(
                        current_password.text.toString(), new_password.text.toString()))
            }
            else -> {
                input_confirm_password.error = getString(R.string.password_format_error)
            }
        }
    }

    private fun initializeUI() {
        val toolbar = toolbar_change_password.findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = getString(R.string.change_password)

        btn_validate_pass.isEnabled = false
        confirm_button.addThrottle().subscribe {
            viewModel.validatePassword(new_password.text.toString(), confirm_password.text.toString())
        }.addTo(subscriptions)

        btn_validate_pass.addThrottle().subscribe {
            viewModel.validateUserPassword(current_password.text.toString()).addTo(subscriptions)
        }.addTo(subscriptions)

        current_password.onChange { text ->
            val isValidPassword = text.isValidPassword()
            btn_validate_pass.background = if(isValidPassword) ContextCompat.getDrawable(this, R.drawable.ic_button_blue)
                else ContextCompat.getDrawable(this, R.drawable.ic_button_gray)
            btn_validate_pass.isEnabled = isValidPassword
        }
        confirm_password.onChange { text ->
            val enabledButton = !text.isEmpty() && text == new_password.text.toString()
            confirm_button.isEnabled = enabledButton
            confirm_button.background = if(enabledButton) ContextCompat.getDrawable(this, R.drawable.btn_custom_corner)
                else ContextCompat.getDrawable(this, R.drawable.btn_custom_corner_gray)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> false
    }

}
