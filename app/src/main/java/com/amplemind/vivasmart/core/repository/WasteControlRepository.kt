package com.amplemind.vivasmart.core.repository


import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.WasteControlApi
import com.amplemind.vivasmart.core.repository.response.UnitResponse
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.amplemind.vivasmart.features.waste_control.models.WasteControlItemModel
import com.amplemind.vivasmart.features.waste_control.models.WasteControlResponse
import com.google.gson.Gson
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class WasteControlRepository @Inject constructor(private val api: WasteControlApi, private val authorization: ApiAuthorization, private val preferences: UserAppPreferences) {

    val businessUnitId = Gson().fromJson(preferences.userInfo, UserModel::class.java).businessUnitId
            ?: ""

    fun getList(day: String): Observable<WasteControlResponse> {
        return api.getList(authentication = authorization.getAuthToken(preferences.token),
                business_unit_id = businessUnitId, from_date = day, to_date = day, embed ="crop")
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun getUnits(): Observable<UnitResponse> {
        return api.getUnits(authentication = authorization.getAuthToken(preferences.token),
                business_unit_id = businessUnitId)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun postFinishedWasteControl(objWasteControlItemModel: WasteControlItemModel): Observable<WasteControlItemModel> {
        return api.postFinishedWasteControl(authorization.getAuthToken(preferences.token), objWasteControlItemModel)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

}
