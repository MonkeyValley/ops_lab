package com.amplemind.vivasmart.core.repository.request

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class IssueRequest(@SerializedName("id") val id: Int,
                        @SerializedName("count") var count: Int,
                        @SerializedName("name") val name : String? = "",
                        @SerializedName("isPermanent") val isPermanent : Boolean): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(count)
        parcel.writeString(name)
        parcel.writeByte(if (isPermanent) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<IssueRequest> {
        override fun createFromParcel(parcel: Parcel): IssueRequest {
            return IssueRequest(parcel)
        }

        override fun newArray(size: Int): Array<IssueRequest?> {
            return arrayOfNulls(size)
        }
    }
}