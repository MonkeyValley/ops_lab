package com.amplemind.vivasmart.core.repository.request

import com.google.gson.annotations.SerializedName

data class UpdateCollaborator(@SerializedName("id") val id: Long, @SerializedName("time") val time: Long,
                              @SerializedName("sign") val sign: String?, @SerializedName("is_training") val isTraining: Boolean,
                              @SerializedName("is_practice") val isPractice: Boolean,
                              @SerializedName("is_pending") val isPending: Boolean = true)

data class UpdateCollaboratorRequest(@SerializedName("bulk") val collaborators: List<UpdateCollaborator>,
                                     @SerializedName("acode") val activityCode: Int?)


data class AddMoreUnitCollaborator(@SerializedName("id") val id: Long,@SerializedName("quantity") val quantity: Int)
data class AddMoreCollaboratorUnityRequest(@SerializedName("bulk") val collaborators: List<AddMoreUnitCollaborator>)

data class UpdateCollaboratorUnity(@SerializedName("id") val id: Int, @SerializedName("time") val time: Long,@SerializedName("quantity") val quantity: Double)
data class UpdateCollaboratorUnityRequest(@SerializedName("bulk") val collaborators: List<UpdateCollaboratorUnity>)

data class UpdateCollaboratorSign(@SerializedName("id") val id: Int, @SerializedName("sign") val sign: String?, @SerializedName("is_training") val isTraining: Boolean)

data class UpdateCollaboratorSignRequest(@SerializedName("bulk") val collaborators: List<UpdateCollaboratorSign>)
