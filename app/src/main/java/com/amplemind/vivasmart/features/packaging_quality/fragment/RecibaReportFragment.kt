package com.amplemind.vivasmart.features.packaging_quality.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.utils.CROP_ID_RECIBA
import com.amplemind.vivasmart.features.adapters.MarginItemDecoration
import com.amplemind.vivasmart.features.packaging_quality.adapters.RecibaReportAdapter
import com.amplemind.vivasmart.features.packaging_quality.viewModel.ItemRecibaReportViewModel
import com.amplemind.vivasmart.features.packaging_quality.viewModel.RecibaReportViewModel
import com.amplemind.vivasmart.features.production_roster.fragments.HarvestBoxCountByVarietyDialog
import com.amplemind.vivasmart.vo_core.utils.currentDateInMillis
import kotlinx.android.synthetic.main.fragment_reciba_report.*
import kotlinx.android.synthetic.main.progress_track_component.*
import javax.inject.Inject

class RecibaReportFragment : BaseFragment() {

    companion object {

        val TAG = RecibaReportFragment::class.java.simpleName

        fun newInstance(): RecibaReportFragment = RecibaReportFragment()

    }

    @Inject
    lateinit var mViewModel: RecibaReportViewModel

    private lateinit var mAdapter: RecibaReportAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        CROP_ID_RECIBA = 0
        return inflater.inflate(R.layout.fragment_reciba_report, container, false)
    }

    override fun setUpUI() {
        super.setUpUI()

        lbl_track_title.text = "/ Operaciones / Reciba / Reporte de reciba /"
        setUpRecyclerView()

        btn_date_from.apply {

            text = mViewModel.humanFromDate

            setOnClickListener{
                val dialog = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
                    mAdapter.clearData()
                    mViewModel.setFromDate(selectedYear, selectedMonth, selectedDay)
                    displayHumanFromDate()
                    loadData()
                }, mViewModel.fromYear, mViewModel.fromMonth, mViewModel.fromDay)

                dialog.apply {
                    datePicker.maxDate = currentDateInMillis()
                    setTitle("Selecciona una fecha")
                    show()
                }

            }
        }

        btn_date_to.apply {
            text = mViewModel.humanToDate

            setOnClickListener{
                val dialog = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDay ->
                    mAdapter.clearData()
                    mViewModel.setToDate(selectedYear, selectedMonth, selectedDay)
                    displayHumanToDate()
                    loadData()
                }, mViewModel.toYear, mViewModel.toMonth, mViewModel.toDay)

                dialog.apply {
                    datePicker.maxDate = currentDateInMillis()
                    setTitle("Selecciona una fecha")
                    show()
                }

            }
        }

    }

    override fun loadData() {
        showNoRecordsLayout(false)
        mViewModel.loadCarryOrders()
    }

    private fun setUpRecyclerView() {

        mAdapter = RecibaReportAdapter()

        mAdapter.setOnClickListener { _, itemRecibaReportViewModel, _ ->
            HarvestBoxCountByVarietyDialog.newInstance(
                    itemRecibaReportViewModel.lotId,
                    itemRecibaReportViewModel.lotName,
                    itemRecibaReportViewModel.boxes,
                    itemRecibaReportViewModel.varieties,
                    itemRecibaReportViewModel.boxCounts,
                    itemRecibaReportViewModel.secondQualityBoxCount,
                    mViewModel.toDate,
                    mViewModel.fromDate,
                    true)
                    .show(activity!!.supportFragmentManager, HarvestBoxCountByVarietyDialog.TAG)
        }

        rv_reports.apply{
            layoutManager = LinearLayoutManager(context!!)
            adapter = mAdapter
            addItemDecoration(MarginItemDecoration(8))
            addOnScrollListener(rvScrollManager)
        }
    }

    private val rvScrollManager = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            mAdapter.scroll = true
        }
    }

    private fun displayHumanFromDate() {
        btn_date_from.text = mViewModel.humanFromDate
    }

    private fun displayHumanToDate() {
        btn_date_to.text = mViewModel.humanToDate
    }

    override fun setUpCallbacks() {
        mViewModel.onShowProgress(this::showProgressBar).addTo(subscriptions)
        mViewModel.onReportLoaded(this::onReportLoaded).addTo(subscriptions)
        mViewModel.onReportLoadingError(this::onReportLoadingError).addTo(subscriptions)
    }

    private fun showProgressBar(show: Boolean) {
        progress_bar.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun showNoRecordsLayout(show: Boolean) {
        layout_no_records.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun onReportLoaded(data: List<ItemRecibaReportViewModel>) {
        showNoRecordsLayout(data.isEmpty())
        mAdapter.data.clear()
        mAdapter.data.addAll(data.sortedBy { it.cropId})
        mAdapter.setData( mAdapter.data )
    }

    private fun onReportLoadingError( error: Throwable ) {
        onError(error.localizedMessage)
    }
}
