package com.amplemind.vivasmart.features.waste_control.models

import java.math.RoundingMode

class WasteControlItemViewModel(wasteCrop : WasteControlItemModel) {
    val id = wasteCrop.id
    val id_record = wasteCrop.id.toString()
    var crop_id = wasteCrop.crop_id
    val crop_name =  wasteCrop.crop?.name
    val export = wasteCrop.export
    val export_percent = ""
    val national = wasteCrop.national
    val national_percent = ""
    val sample = wasteCrop.sample
    val waste = wasteCrop.waste
    var unit_id = wasteCrop.unit_id
    var perc = (wasteCrop.waste?.toBigDecimal()?.setScale(2, RoundingMode.HALF_EVEN))

    val percentWasteDiv = ((wasteCrop.waste?.times(100.00))?.div(wasteCrop.sample!!))
    val percentString = percentWasteDiv?.toBigDecimal()?.setScale(2, RoundingMode.CEILING).toString()
    val waste_percentView = "$percentString%"
    var waste_percent = percentString

    var natural_waste = wasteCrop.natural_waste
    var operation_waste = wasteCrop.operation_waste
}