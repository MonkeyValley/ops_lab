package com.amplemind.vivasmart.core.repository.response

import com.google.gson.annotations.SerializedName

data class CollaboratorFinishCostResponse (
        @SerializedName("practice_cost") val practice_cost: Double?,
        @SerializedName("total") val total: Double?,
        @SerializedName("training_cost") val training_cost: Double?,
        @SerializedName("unit_total") val unit_total: Double?,
        @SerializedName("hours") val hours: Double?,
        var registerID : Int = -1,
        var position : Int = -1,
        var isCountBox : Boolean = false
)