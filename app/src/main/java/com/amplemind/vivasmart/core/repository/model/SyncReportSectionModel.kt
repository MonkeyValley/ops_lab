package com.amplemind.vivasmart.core.repository.model

import android.os.Parcel
import android.os.Parcelable

data class SyncReportSectionModel(val reports: List<SyncReportModel>,
                                  val type: Int): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(SyncReportModel)!!,
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(reports)
        parcel.writeInt(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SyncReportSectionModel> {
        override fun createFromParcel(parcel: Parcel): SyncReportSectionModel {
            return SyncReportSectionModel(parcel)
        }

        override fun newArray(size: Int): Array<SyncReportSectionModel?> {
            return arrayOfNulls(size)
        }
    }

}