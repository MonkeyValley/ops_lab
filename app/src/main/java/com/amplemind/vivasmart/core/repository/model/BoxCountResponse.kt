package com.amplemind.vivasmart.core.repository.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class BoxCountResponse(
        @SerializedName("data") val packings: List<BoxCountResultResponse>,
        @SerializedName("total_count") val total_count: Int
)

@Entity
data class BoxCountResultResponse(
        @SerializedName("id") @PrimaryKey(autoGenerate = true) val id: Int? = null,
        @SerializedName("last_count") val last_count: Int,
        @SerializedName("packing_id") val packing_id: Int,
        @SerializedName("packingline_id") val packingline_id: Int,
        @Embedded @SerializedName("packing") val packing: PresentationResultResponse
)