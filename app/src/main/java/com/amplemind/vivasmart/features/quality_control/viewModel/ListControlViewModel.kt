package com.amplemind.vivasmart.features.quality_control.viewModel

import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.repository.QualityControlRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ControlQualityListModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject

class ListControlViewModel @Inject constructor(private val repository : QualityControlRepository,
                                               private val uploadRepository: ProfileRepository,
                                               private val preferences: UserAppPreferences,
                                               private val apiErrors: HandleApiErrors) {

    private val requestFail = BehaviorSubject.create<String>()
    private val requestSuccess = BehaviorSubject.create<List<ControlListItemViewModel>>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val activityFinalize = BehaviorSubject.create<Boolean>()
    private val successUpload = BehaviorSubject.create<MutableList<File>>()
    private val signs = mutableListOf<String>()
    private var qualityAverage = 0

    fun uploadSign(files: MutableList<File>): Disposable {
        return uploadRepository.uploadImage(files.first(), "user")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .subscribe({
                    if (files.isNotEmpty()) {
                        files.removeAt(0)
                    }
                    signs.add(it.relative_url)
                    successUpload.onNext(files)
                }, {
                    progressStatus.onNext(false)
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getCondensedCollaborators(stageId: Int): Disposable {
        return repository.getControlList(preferences.token, stageId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    //repository.saveControlQualityList(it, stageId)
                    calculateQualityAverage(it)
                    mapModelToViewModels(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccess.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLocalCondensedCollaborators(stageId: Int): Disposable {
        return repository.getLocalControlList(stageId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { it -> mapModelToViewModels(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccess.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun finalizeControlActivity(stageId: Int): Disposable {
        return repository.finalizeControlActivity(preferences.token, stageId, signs[0], signs[1])
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    activityFinalize.onNext(true)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getQualityAverage(): Int {
        return qualityAverage
    }

    private fun calculateQualityAverage(items: List<ControlQualityListModel>) {
        if (items.isNotEmpty()) {
            qualityAverage = 0
            for (item in items) {
                qualityAverage += item.score
            }
            qualityAverage /= items.size
        }
        else {
            qualityAverage = 100
        }
    }

    private fun mapModelToViewModels(models: List<ControlQualityListModel>): List<ControlListItemViewModel> {
        val list = arrayListOf<ControlListItemViewModel>()
        for (item in models) {
            list.add(ControlListItemViewModel((item)))
        }
        return list
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestSuccess(): BehaviorSubject<List<ControlListItemViewModel>> {
        return requestSuccess
    }

    fun onSuccessUploadSign(): BehaviorSubject<MutableList<File>> {
        return successUpload
    }

    fun  onActivityFinalize(): BehaviorSubject<Boolean> {
        return activityFinalize
    }

}
