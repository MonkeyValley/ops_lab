package com.amplemind.vivasmart.features.production_roster.viewModels

class ItemEditQuantityRosterViewModel constructor(val line: String, var quantity: String,
                                                  var time: String, val ppp: String) {

    var position: Int? = null
}