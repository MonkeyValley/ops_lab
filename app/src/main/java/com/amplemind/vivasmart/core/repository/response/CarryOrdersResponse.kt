package com.amplemind.vivasmart.core.repository.response

import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.google.gson.annotations.SerializedName

data class CarryOrdersResponse(@SerializedName("data") val data: List<CarryOrderModel>)