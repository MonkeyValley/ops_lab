package com.amplemind.vivasmart.core.repository.remote.models

import com.google.gson.annotations.SerializedName

data class HarvestQualitySettingModel (
        @SerializedName("carry_order_id")   val carryOrderId: Long,
        @SerializedName("issue_id")         val issueId: Long,
        @SerializedName("is_ok")            val isOk: Boolean = true
)