package com.amplemind.vivasmart.vo_core.repository.remote.downloaders.realm

import android.util.Log
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.RealmHolder
import com.amplemind.vivasmart.vo_core.repository.dao.realm.ActivitiesDao
import com.amplemind.vivasmart.vo_core.repository.dao.realm.StagesDao
import com.amplemind.vivasmart.vo_core.repository.models.extensions.grooveTotal
import com.amplemind.vivasmart.vo_core.repository.models.extensions.parseOccupiedSpaces
import com.amplemind.vivasmart.vo_core.repository.models.realm.ActivityCodeModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_core.repository.remote.ActivityCodeApi
import com.amplemind.vivasmart.vo_core.utils.currentISODate
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ActivityCodeDownloader @Inject constructor(
        realmHolder: RealmHolder,
        private val mActivitiesDao: ActivitiesDao,
        private val mStagesDao: StagesDao,
        private val mActivityCodesApi: ActivityCodeApi,
        private val mPref: UserAppPreferences
): RealmDownloader<StageModel, ActivityCodeModel>(realmHolder, ActivityCodeModel::class.java) {

    var fromDate: String = currentISODate("yyyy-MM-dd")

    override fun buildServerRequest(parent: StageModel, lastVersion: Long): Single<List<ActivityCodeModel>> {
       val activityCodeList = mActivityCodesApi.getCodes(
                mPref.authorizationToken,
                closedFrom = fromDate,
                stageId = parent.id.toLong(),
                version = lastVersion
        )
        Log.e("Version: ", lastVersion.toString())
        Log.e("Data-ActivityCode: ", activityCodeList.toString())

        return activityCodeList
    }

    override fun beforeSaving(parent: StageModel, child: ActivityCodeModel): Single<ActivityCodeModel> =
            mActivitiesDao.findById(child.activityId.toLong())
                    .flatMapSingle {activity ->
                        Single.fromCallable {
                            child.apply {
                                totalSpaces = activity.unitLimit
                                stageUuid = parent.uuid
                                activityUuid = activity.uuid
                                stage = parent
                                this.activity = activity
                            }
                        }
                    }
                    .onErrorReturnItem(child)
                    .doOnSuccess { activityCode ->
                        activityCode.apply {
                            if (activityCode.activity?.isGrooves == true) {
                                activityCode.totalSpaces = parent.lot.grooveTotal()
                            }
                            occupiedSpaces = parseOccupiedSpaces
                            isOpen = occupiedSpaces < totalSpaces
                        }
                    }

}