package com.amplemind.vivasmart.features.production_roster

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivity
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.features.production_quality.QualityOrderActivity
import kotlinx.android.synthetic.main.content_quality_order_result.*
import javax.inject.Inject

class QualifiedCarryOrderActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: QualifiedCarryOrderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_quality_order_result)

        setupToolbar()
        addSubscribers()
        loadQualifiedCarryOrders()
    }

    private fun loadQualifiedCarryOrders() {
        //viewModel.getLoadQualifiedCarry().addTo(subscriptions)
    }

    private fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getQualifiedOrders().subscribe(this::loadQUalifieData).addTo(subscriptions)
    }

    private fun loadQUalifieData(data: List<ItemQualifiedViewModel>) {

        no_found_activities.visibility = if (data.isEmpty()) View.VISIBLE else View.GONE

        val adapter = ReportQualifiedCarryAdapter(data)
        rv_order_result.hasFixedSize()
        rv_order_result.layoutManager = LinearLayoutManager(this)
        rv_order_result.itemAnimator = DefaultItemAnimator()
        rv_order_result.adapter = adapter

        adapter.onClick().subscribe(this::onClickDetail).addTo(subscriptions)
    }

    private fun onClickDetail(item: ItemQualifiedViewModel) {
        val intent = Intent(this, QualityOrderActivity::class.java)
        intent.putExtra("id_carry", item.id_carry)
        intent.putExtra("status", item.evaluation)
        intent.putExtra("name_lot", viewModel.getLotName())
        intent.putExtra("flow", QualityOrderActivity.QualityOrderFlow.READ.type)
        startActivity(intent)
    }

    /**
     *  change title in toolbar
     */
    private fun setupToolbar() {
        val mToolbar = toolbar_report_order_result
        setSupportActionBar(mToolbar.findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.clean_report_title)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
