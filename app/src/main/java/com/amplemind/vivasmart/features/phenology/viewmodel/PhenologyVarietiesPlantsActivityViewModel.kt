package com.amplemind.vivasmart.features.phenology.viewmodel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.PhenologyRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PhenologyVarietiesPlantsActivityViewModel @Inject constructor(private val repository: PhenologyRepository, private val apiErrors: HandleApiErrors) : BaseViewModel() {
    var currentPosition: Int = 0
    private val requestSuccess: BehaviorSubject<Boolean> = BehaviorSubject.create()

    /*
     * Region get behavior subjects
     */

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestSuccess(): BehaviorSubject<Boolean> {
        return requestSuccess
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

}