package com.amplemind.vivasmart.features.quality_control.viewModel

import android.content.Intent
import com.amplemind.vivasmart.core.repository.QualityControlRepository
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.ControlQualityActivitiesListModel
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ActivitiesControlViewModel
@Inject constructor(
        private val repository: QualityControlRepository,
        private val preferences: UserAppPreferences,
        private val apiErrors: HandleApiErrors
) {

    private val TAG = "---> ${this.javaClass.simpleName}"

    private val nameSubject = BehaviorSubject.create<String>()
    private val numberSubject = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val requestFail = BehaviorSubject.create<String>()
    private val activities = BehaviorSubject.create<List<ControlActivitiesItemViewModel>>()
    private val list = arrayListOf<ControlActivitiesItemViewModel>()

    /*
     * Called when Online
     * */
    fun getControlListActivities(collaboratorId: Int, stageId: Int): Disposable {
        return repository.getControlListActivities(preferences.token, collaboratorId, stageId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map {
                    mapModelToViewModels(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    activities.onNext(it)
                    numberSubject.onNext(it.size.toString())
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    /*
     * Called when Offline
     * */
    fun getLocalControlListActivities(collaboratorId: Int): Disposable {
        return repository.getLocalControlListActivities(collaboratorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { mapModelToViewModels(it) }
                .subscribe({
                    activities.onNext(it)
                    numberSubject.onNext(it.size.toString())
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getActivities(): BehaviorSubject<List<ControlActivitiesItemViewModel>> {
        return activities
    }

    private fun mapModelToViewModels(models: List<ControlQualityActivitiesListModel>): List<ControlActivitiesItemViewModel> {
        list.clear()
        for (item in models) {
            list.add(ControlActivitiesItemViewModel((item)))
        }
        return list
    }

    fun getSignatureTitle(): String {
        return "Verificar control de calidad"
    }

    fun getSignatureMessage(): String {
        return "¿Estas seguro de querer enviar este reporte? Por favor firme para confirmar:"
    }

    fun getDataFromIntent(intent: Intent) {
        nameSubject.onNext(intent.getStringExtra("NAME"))
    }

    fun getName(): BehaviorSubject<String> {
        return nameSubject
    }

    fun getNumber(): BehaviorSubject<String> {
        return numberSubject
    }

    fun getActivityName(position: Int): String {
        if (position >= list.size) return ""
        return list[position].activityName
    }

    fun canEditGroove(position: Int): Boolean {
        if (position >= list.size) return false
        return list[position].status < 3
    }

    fun getActivityId(position: Int): Int {
        if (position >= list.size) return 0
        return list[position].activityId
    }

    fun getActivityLog(position: Int): Int {
        if (position >= list.size) return 0
        return list[position].activityLog
    }

    fun getActivityArea(position: Int): Int {
        if (position >= list.size) return 0
        return list[position].activityArea
    }

    fun getActivityCategory(position: Int): Int {
        if (position >= list.size) return 0
        return list[position].activityCategoryId
    }

    fun getActivitySubCategory(position: Int): Int {
        if (position >= list.size) return 0
        return list[position].activitySubCategory
    }

    fun updateCount(position: Int, grooveId: Int) {
        if (position >= list.size) return
        list[position].grooves.map { groove ->
            if (grooveId == groove.id) {
                groove.qualityControl = true
            }
        }
        list[position].status += 1
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

}
