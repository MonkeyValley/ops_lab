package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.repository.ReportQualityRepository
import com.amplemind.vivasmart.core.repository.response.CarryOrderPendingResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReportQualityViewModel @Inject constructor(
        private val repository: PackagingQualityRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val carry_list = mutableListOf<ItemReportQualityViewModel>()

    private val requestSuccess = BehaviorSubject.create<List<ItemReportQualityViewModel>>()

    fun getCarryOrder(): Disposable {
        return repository.loadCarryOrderPending()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel)
                .subscribe({
                    carry_list.addAll(it)

                    requestSuccess.onNext(carry_list)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun mapModelToViewModel(result : CarryOrderPendingResponse) : List<ItemReportQualityViewModel>{
        val viewModels = mutableListOf<ItemReportQualityViewModel>()

        result.data.forEach { order ->
            viewModels.add(ItemReportQualityViewModel(order))
        }
        return viewModels
    }

    fun onSuccessRequest(): BehaviorSubject<List<ItemReportQualityViewModel>> {
        return requestSuccess
    }

}