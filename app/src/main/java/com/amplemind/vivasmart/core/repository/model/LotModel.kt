package com.amplemind.vivasmart.core.repository.model

import androidx.room.*
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

@Entity
data class Stage(
        @SerializedName("id") @ColumnInfo(name = "stage_id") @PrimaryKey(autoGenerate = false) val id: Int,
        @Embedded @SerializedName("lot") val lot : LotModel,
        @SerializedName("code") val code: Int,
        @SerializedName("crop_id") val crop_id: Int,
        @SerializedName("cycle_id") val cycle_id: Int,
        @SerializedName("density") val density: Int,
        @SerializedName("is_active") val is_active: Boolean,
        @SerializedName("is_closed") val is_closed: Boolean,
        @SerializedName("last_close") val last_close: String?,
        @SerializedName("lot_id") @ColumnInfo(name = "lot_id") val lot_id : Int,
        @Embedded @SerializedName("crop") val crop : CropStage,
        @SerializedName("plant_date") val plant_date : String,
        @SerializedName("plants_per_groove") val plants_per_groove : Int,
        @SerializedName("stage_activities") var stage_activities : Boolean
)

@Entity
data class LotModel (
                     @SerializedName("id") @ColumnInfo(name = "lot_id_primary") @PrimaryKey(autoGenerate = false) val id: Int,
                     @SerializedName("business_unit_id") val busnisessUnitId: Int,
                     @SerializedName("company_id") val companyId: Int,
                     @SerializedName("lot_type_id") val lotType: Int,
                     @SerializedName("name") @ColumnInfo(name = "lot_name") val name: String,
                     @SerializedName("is_closed") val isCloded: Boolean,
                     @SerializedName("is_quality_control_closed") val isQualityControlClosed: Boolean,
                     @SerializedName("is_qa_done") val isQaClose: String,
                     @SerializedName("business_unit") @Embedded val business_unit: BusinessUnit?,
                     @SerializedName("table_1") val table1: Int,
                     @SerializedName("table_2") val table2: Int,
                     @SerializedName("table_3") val table3: Int,
                     @SerializedName("table_4") val table4: Int,
                     @SerializedName("table_5") val table5: Int,
                     @SerializedName("table_6") val table6: Int)

@Entity
data class BusinessUnit(@ColumnInfo(name = "business_unit_id_model") @SerializedName("id") @PrimaryKey(autoGenerate = false) val id: Int,
                        @SerializedName("name") @ColumnInfo(name = "business_unit_name") val name: String)


data class TableGrooves(val table : Int, val grooves_no : Int): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(table)
        parcel.writeInt(grooves_no)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TableGrooves> {
        override fun createFromParcel(parcel: Parcel): TableGrooves {
            return TableGrooves(parcel)
        }

        override fun newArray(size: Int): Array<TableGrooves?> {
            return arrayOfNulls(size)
        }
    }

}