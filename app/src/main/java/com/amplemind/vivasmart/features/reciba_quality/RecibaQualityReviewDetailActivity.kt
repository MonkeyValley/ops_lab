package com.amplemind.vivasmart.features.reciba_quality

import android.os.Bundle
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseActivityWithFragment
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.SharedPreferencesUtils
import com.amplemind.vivasmart.features.reciba_quality.fragment.RecibaQualityReviewDetailFragment
import kotlinx.android.synthetic.main.categories_toolbar.*
import javax.inject.Inject

class RecibaQualityReviewDetailActivity : BaseActivityWithFragment() {

    @Inject
    lateinit var preferences: UserAppPreferences

    companion object {
        lateinit var sharedPref: SharedPreferencesUtils
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reciba_quality_review_detaill)
        sharedPref = SharedPreferencesUtils(this)
        val newReport =  intent.getBooleanExtra("newReport", true)
        var idReport = ""
        var lotName = ""
        var packingName = ""
        var  cropId = intent.getIntExtra("cropId", 0)

        if(intent.getStringExtra("idReport") != null) {
            idReport = intent.getStringExtra("idReport")
            lotName = intent.getStringExtra("lotName")
            packingName = intent.getStringExtra("packingName")
        }

        initToolbar()
        callFragmentMenuActivities(newReport, idReport, lotName, packingName, cropId)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.reciba_quality)
        toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun onBackPressed() {
        finish()
    }

    protected fun getFragmentCount(): Int {
        return supportFragmentManager.backStackEntryCount
    }

    private fun callFragmentMenuActivities(newReport : Boolean, idReport: String, lotName: String, packingName : String, cropId : Int ) {
            val support = supportFragmentManager
            val commit = support?.beginTransaction()
                    ?.replace(R.id.container_payroll, RecibaQualityReviewDetailFragment().newInstance("RecibaQualityReviewDetailFragment", true, "", "", "", cropId))
                    ?.addToBackStack("RecibaQualityReviewDetailFragment")
                    ?.commit()
    }

}
