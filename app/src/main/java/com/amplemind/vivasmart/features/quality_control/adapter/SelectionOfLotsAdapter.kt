package com.amplemind.vivasmart.features.quality_control.adapter

import androidx.core.view.ViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.amplemind.vivasmart.databinding.ItemControlQualityBinding
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlQualityItemViewModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.StageModel
import com.amplemind.vivasmart.vo_features.adapters.recyclerview.BaseAdapter
import androidx.databinding.library.baseAdapters.BR
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.extensions.setHouse
import com.amplemind.vivasmart.features.lots.viewModel.SelectionLotsViewModel
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_control_quality.view.*

class SelectionOfLotsAdapter : BaseAdapter<StageModel, ControlQualityItemViewModel, SelectionOfLotsAdapter.ControlQualityViewHolder>() {

    val onStageClicked: PublishSubject<StageModel> = PublishSubject.create()
    lateinit var mViewModel: SelectionLotsViewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ControlQualityViewHolder {
        val binding = ItemControlQualityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ControlQualityViewHolder(binding)
    }

    override fun buildViewModel(item: StageModel): ControlQualityItemViewModel = ControlQualityItemViewModel(item)

    override fun getItemKey(item: StageModel): String = item.uuid

    inner class ControlQualityViewHolder(private val binding: ItemControlQualityBinding) : BaseAdapter.ViewHolder<ControlQualityItemViewModel>(binding.root) {

        private var mClickDisposable: Disposable? = null
        override fun bind(viewModel: ControlQualityItemViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()

            ViewCompat.setElevation(binding.root.tv_counter, 15.0f)

            var count = mViewModel.getLotActivityCouter(viewModel.lotId)

            var lotType = when (viewModel.getLotType()) {
                1 -> "CIRCLE"
                2 -> "CIRCLE"
                5 -> "CIRCLE"
                else -> "RECT"
            }

            if (count != 0) {
                binding.tvCounter.visibility = View.VISIBLE
                binding.tvCounter.text = count.toString()

                //PENDING
                if (lotType == "CIRCLE") binding.imageView2.setHouse(5)
                else binding.imageView2.setHouse(6)

                binding.textView13.setTextColor(ContextCompat.getColor(this.binding.root.context,R.color.disableLot))
            } else {
                binding.tvCounter.visibility = View.GONE

                if(!mViewModel.isClosed(viewModel.lotId)) {
                    //HOUSE
                    if (lotType == "CIRCLE") binding.imageView2.setHouse(1)
                    else binding.imageView2.setHouse(3)

                    binding.textView13.setTextColor(ContextCompat.getColor(this.binding.root.context,R.color.disableLot))
                } else {
                    //COMPLETE
                    if (lotType == "CIRCLE") binding.imageView2.setHouse(2)
                    else binding.imageView2.setHouse(4)

                    binding.textView13.setTextColor(ContextCompat.getColor(this.binding.root.context,R.color.blue))
                }
            }


            mClickDisposable = binding.root.clicks()
                    .subscribe {
                        onStageClicked.onNext(viewModel.stage)
                    }
        }
        override fun cleanUp() {
            super.cleanUp()
            mClickDisposable?.dispose()
        }

    }

}
