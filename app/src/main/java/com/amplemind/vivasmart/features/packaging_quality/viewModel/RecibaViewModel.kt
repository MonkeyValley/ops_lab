package com.amplemind.vivasmart.features.packaging_quality.viewModel

import com.amplemind.vivasmart.core.repository.PackagingQualityRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class RecibaViewModel @Inject constructor(private val repository : PackagingQualityRepository,
                                          private val apiErrors: HandleApiErrors) {

    private var listData = BehaviorSubject.create<List<CarryOrderCondensedViewModel>>()
    private val requestFail = BehaviorSubject.create<String>()
    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val listReciba = mutableListOf<CarryOrderCondensedViewModel>()

    fun getRecibaList(): Disposable {
        return repository.getRecibaList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { models ->
                    models.data.map { CarryOrderCondensedViewModel(it) }
                }
                .subscribe({
                    listReciba.clear()
                    listReciba.addAll(it)
                    listData.onNext(listReciba)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun getLocalRecibaList(): Disposable {
        return repository.getLocalRecibaCondensedList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .map { models -> models.data.map { CarryOrderCondensedViewModel(it) } }
                .subscribe({
                    listReciba.clear()
                    listReciba.addAll(it)
                    listData.onNext(listReciba)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun removeItem(recibaId: Int) {
        val filtered = listReciba.filter { it.id == recibaId }
        if (filtered.isNotEmpty()) {
            listReciba.remove(filtered.first())
        }
    }

    fun getList(): BehaviorSubject<List<CarryOrderCondensedViewModel>> {
        return listData
    }

    fun onRequestFail(): BehaviorSubject<String> {
        return requestFail
    }

    fun onProgressStatus(): BehaviorSubject<Boolean> {
        return progressStatus
    }

}