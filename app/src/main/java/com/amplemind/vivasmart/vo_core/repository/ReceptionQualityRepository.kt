package com.amplemind.vivasmart.vo_core.repository

import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.CarryOrderModel
import com.amplemind.vivasmart.core.repository.remote.PackagingQualityApi
import com.amplemind.vivasmart.core.utils.EventBus
import com.amplemind.vivasmart.vo_core.repository.models.realm.IssueModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.ReceptionQualityRevisionModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReceptionQualityRepository @Inject constructor (
        private val mPreferences: UserAppPreferences,
        private val mEventBus: EventBus,
        private val mPackagingQualityApi: PackagingQualityApi
): Repository() {

    override fun cleanUp() {
    }

    fun loadCarryOrder(carryOrderId: Int): Observable<CarryOrderModel> =
            mPackagingQualityApi.getItemCarryOrder(mPreferences.authorizationToken, id = carryOrderId, businessUnitId = mPreferences.user.businessUnitId.toString())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .map { response ->
                        response.data.first()
                    }


    fun loadIssues(cropId: Long): Observable<List<IssueModel>> =
            mPackagingQualityApi.getReceptionQualityIssues(mPreferences.authorizationToken, cropId = cropId, businessUnitId = mPreferences.user.businessUnitId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .map { response ->
                        response.data
                    }

    fun createReceptionQualityRevision(reception: ReceptionQualityRevisionModel) =
            mPackagingQualityApi.createReceptionQualityRevision(mPreferences.authorizationToken, reception)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())

}