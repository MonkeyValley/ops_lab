package com.amplemind.vivasmart.core.repository.model

data class SendCollaboratorsModel(val activity_code_id : Int, val collaborator_id : Int)