package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import com.amplemind.vivasmart.vo_core.repository.models.realm.PlagueDetail
import com.amplemind.vivasmart.vo_core.repository.models.realm.PlaguesModel
import io.realm.DynamicRealm

class Version_12 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectModel(realm)
    }

    private fun migrateUploadObjectModel(realm: DynamicRealm) {

        val plagueDetailSchema = realm.schema.create("PlagueDetail")
                .addField("date", String::class.javaObjectType)
                .addField("id", Int::class.javaObjectType)
                .addField("image", String::class.javaObjectType)
                .addField("isDesease", Boolean::class.javaObjectType)
                .addField("isQualitative", Boolean::class.javaObjectType)
                .addField("lv1", Int::class.javaObjectType)
                .addField("lv2", Int::class.javaObjectType)
                .addField("lv3", Int::class.javaObjectType)
                .addField("medition", String::class.javaObjectType)
                .addField("name", String::class.javaObjectType)
                .addField("propagation", String::class.javaObjectType)

        val plaguesModelSchema = realm.schema.create("PlaguesModel")
                .addField("cropId", Int::class.javaObjectType)
                .addField("plagueId", Int::class.javaObjectType)
                .addRealmObjectField("plagueDetail", plagueDetailSchema)

        realm.schema.get("CropStageModel")?.apply {
            addRealmListField("plagues", plaguesModelSchema)
        }

    }

    override fun drop(realm: DynamicRealm) {

    }
}