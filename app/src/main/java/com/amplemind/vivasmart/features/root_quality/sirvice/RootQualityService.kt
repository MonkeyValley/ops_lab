package com.amplemind.vivasmart.features.root_quality.sirvice

import android.content.Context
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel
import com.amplemind.vivasmart.vo_core.repository.models.realm.UploadObjectModel
import com.amplemind.vivasmart.vo_core.utils.getUUID
import io.realm.Realm
import org.json.JSONObject
import java.util.*
import javax.inject.Inject

class RootQualityService  @Inject constructor() {

    var context: Context? = null

    lateinit var preferences: UserAppPreferences

    fun saveRootQuality(lotId: Int, table: Int, image: String, comment: String, quality: Int)  {
        val prefer = context!!.getSharedPreferences(
                context!!.getString(R.string.app_name), Context.MODE_PRIVATE)

        val cal = Calendar.getInstance(TimeZone.getTimeZone("GMT-6"))
        val date  = ( ( cal.get(Calendar.YEAR)).toString() + "-" +(cal.get(Calendar.MONTH) + 1).toString() + "-" + (cal.get(Calendar.DAY_OF_MONTH)).toString() )

        val rootObject = JSONObject()
        rootObject.put("lot_id", lotId)
        rootObject.put("table",  table)
        rootObject.put("image", image)
        rootObject.put("comment", comment)
        rootObject.put("quality", quality)
        rootObject.put("date", date)

        Realm.getDefaultInstance().use {
            it.executeTransaction { realm ->
                val url = preferences.selectedServerUrl
                val uploadObject = realm.createObject(UploadObjectModel::class.java, getUUID())
                uploadObject.verb = "POST"
                uploadObject.url = url + "v2/fertigation/root_quality"
                uploadObject.contentType = "application/json"
                uploadObject.body = rootObject.toString()
                uploadObject.token = prefer.getString("token", "") ?: ""
                uploadObject.order = 99
                realm.insertOrUpdate(uploadObject)
            }
        }
    }

    fun getTableCount(lotId: Int): Int {
        Realm.getDefaultInstance().use {
            val result = it
                    .where(LotModel::class.java)
                    .equalTo("id", lotId)
                    .findFirst()

            var count = 0
            if (result != null) {
                if (result.table1 != 0) count++
                if (result.table2 != 0) count++
                if (result.table3 != 0) count++
                if (result.table4 != 0) count++
                if (result.table5 != 0) count++
                if (result.table6 != 0) count++
            }
            return count
        }
    }


}