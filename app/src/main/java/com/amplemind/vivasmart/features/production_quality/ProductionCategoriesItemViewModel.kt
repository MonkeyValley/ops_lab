package com.amplemind.vivasmart.features.production_quality

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.ProductionCategoriesModel
import java.util.*
import javax.inject.Inject

class ProductionCategoriesItemViewModel @Inject constructor(model: ProductionCategoriesModel) : Observable() {

    val id = model.id
    val name: String
    val image: Int
    var count: Int
    var score: Int = 100
    var isActive = false

    init {
        name = model.name
        image = when(model.id) {
            1 -> R.drawable.ic_corte
            2 -> R.drawable.ic_mantenimiento
            3 -> R.drawable.ic_riego
            4 -> R.drawable.ic_mipe
            5 -> R.drawable.ic_labores
            else -> R.drawable.ic_inocuidad
        }
        count = model.total
        isActive = count > 0
    }

    fun getTotalIssues(): String {
        return "$count"
    }

    fun updateIncidences(total: Int) {
        count = total
        isActive = total > 0
    }


}