package com.amplemind.vivasmart.features.crop_validation.viewmodel

import android.annotation.SuppressLint
import com.amplemind.vivasmart.core.repository.ProfileRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import javax.inject.Inject

class SignPhotoCropValidationDialogViewModel @Inject constructor(
        private val uploadRepository: ProfileRepository,
        private val apiErrors: HandleApiErrors) {

    private val progressStatus = BehaviorSubject.create<Boolean>()
    private val successUploadImageUrl: BehaviorSubject<MutableList<String>> = BehaviorSubject.create()

    private val requestFail = BehaviorSubject.create<String>()
    val imageList = mutableListOf<String>()

    @SuppressLint("CheckResult")
    fun uploadImage(files: MutableList<File>, type: String) {
        for (file in files) {
            uploadRepository.uploadImage(file, type)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { progressStatus.onNext(true) }
                    .doOnTerminate { progressStatus.onNext(false) }
                    .doOnTerminate {
                        progressStatus.onNext(false)
                        onSuccessUpdateImageUrl().onNext(imageList)
                    }
                    .subscribe({
                        if (files.isNotEmpty()) {
                            files.removeAt(0)
                        }
                        imageList.add(it.relative_url)
                    }, {
                        requestFail.onNext(apiErrors.handleLoginError(it))
                    })
        }
    }

    fun onSuccessUpdateImageUrl(): BehaviorSubject<MutableList<String>> {
        return successUploadImageUrl
    }

}