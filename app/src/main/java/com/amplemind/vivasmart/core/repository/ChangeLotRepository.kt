package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.extensions.dummyCHangeLots
import com.amplemind.vivasmart.core.repository.model.ChangeLotModel
import io.reactivex.Observable
import javax.inject.Inject

class ChangeLotRepository @Inject constructor(){

    fun getLots() : Observable<List<ChangeLotModel>>{
        return Observable.just(dummyCHangeLots())
    }


}
