package com.amplemind.vivasmart.core.repository.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.amplemind.vivasmart.core.repository.model.ActivitiesModel
import com.amplemind.vivasmart.core.repository.model.ActivityCodeModel
import io.reactivex.Single

@Dao
interface ActivitiesDao {

    /*
     * Activities Database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertActivities(recent: ActivitiesModel)

    @Query("SELECT * FROM ActivitiesModel ORDER BY name ASC")
    fun getLocalActivities() : List<ActivitiesModel>

    @Query("SELECT * FROM ActivitiesModel WHERE activity_id = :activityId")
    fun getActivityById(activityId: Int) : ActivitiesModel

    @Query("UPDATE ActivitiesModel SET stage_activities = :status WHERE activity_id = :activityId")
    fun updateActivityStatus(status : Boolean, activityId : Int)

    /*
     * Activities Codes Database
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertActivityCode(recent: ActivityCodeModel): Long

    @Query("DELETE FROM ActivityCodeModel WHERE id = :id")
    fun deleteActivityCode(id: Long)

    @Query("DELETE FROM ActivityCodeModel WHERE activityId = :activityId AND stageId = :stageId")
    fun deleteActivityCodeForStage(activityId: Int, stageId: Int)

    @Query("SELECT * FROM ActivityCodeModel WHERE activityId = :activityId AND stageId = :stageId ORDER BY code ASC")
    fun getLocalActivityCodes(activityId: Int, stageId: Int) : List<ActivityCodeModel>

    @Query("SELECT * FROM ActivityCodeModel WHERE id = :id")
    fun getActivityCodeById(id: Long) : ActivityCodeModel

    @Query("SELECT * FROM ActivityCodeModel WHERE id = :id")
    fun getActivityCodeById(id: Int) : ActivityCodeModel

    @Query("UPDATE ActivityCodeModel SET total = :total, remaining = :remaining WHERE id = :id")
    fun updateTotalActivityCode(id: Int, total: String, remaining: Int)

}