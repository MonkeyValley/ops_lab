package com.amplemind.vivasmart.features.phenology.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.extensions.toDp
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.features.phenology.PhenologyVarietiesPlantsActivity
import com.amplemind.vivasmart.features.phenology.PhenologyVarietiesPlantsActivity.Companion.indexPrincipal
import com.amplemind.vivasmart.features.phenology.PhenologyVarietiesPlantsActivity.Companion.stage_id
import com.amplemind.vivasmart.features.phenology.adapters.VarMeasurePhenologyAdapter
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyVarClusterDialog
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyVarMeasureDialog
import com.amplemind.vivasmart.features.phenology.dialog.SignCommentDialog
import com.amplemind.vivasmart.features.phenology.models.local.*
import com.amplemind.vivasmart.features.phenology.viewmodel.PhenologyVarietiesPlantFragmentViewModel
import com.amplemind.vivasmart.features.phenology.viewmodel.VarMeasurePhenologyItemViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_varieties_plants_phenology.*
import javax.inject.Inject


@SuppressLint("Registered")
class PhenologyVarietiesPlantsFragment : BaseFragment(), HasSupportFragmentInjector {
    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = childFragmentInjector

    @Inject
    lateinit var viewModel: PhenologyVarietiesPlantFragmentViewModel

    lateinit var fragView: View
    var vars = ArrayList<PhenologyVars>()
    lateinit var tvCluster: TextView
    lateinit var spinnerValve: Spinner
    lateinit var saveButton: Button

    var index: Int = 0
    var lastPlant = false
    var objCluster = PhenologyClusterItem()
    var sign: String = ""
    var comment: String = ""
    var has_cluster: Boolean = false
    var fullPlants: Boolean = false
    lateinit var varDetailItem: VarMeasurePhenologyItemViewModel
    val listaVar = ArrayList<VarMeasurePhenologyItemViewModel>()
    val mAdapter = VarMeasurePhenologyAdapter(this)

    companion object {
        const val clusterResult = "cluster"
        const val signResult = "sign"
        const val commentResult = "comment"
        var sumBalance = 0
        var sumVegetative = 0
        var sumGenerative = 0

        fun newIntent(cluster: String?, sign: String, comment: String): Intent? {
            val intent = Intent()
            intent.putExtra(clusterResult, cluster)
            intent.putExtra(signResult, sign)
            intent.putExtra(commentResult, comment)
            return intent
        }

        fun ifConected(): Boolean {
            return if (PhenologyVarietiesPlantsFragment().hasInternet())
                true
            else {
                Toast.makeText(PhenologyVarietiesPlantsFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }

        fun newInstance(index: Int, vars: String, phenology: String, has_cluster: Boolean, fullPlants: Boolean): PhenologyVarietiesPlantsFragment {
            val args = Bundle()
            args.putInt("index", index)
            args.putString("vars", vars)
            args.putString("phenology", phenology)
            args.putBoolean("has_cluster", has_cluster)
            args.putBoolean("fullPlants", fullPlants)

            val fragment = PhenologyVarietiesPlantsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragView = inflater.inflate(R.layout.fragment_varieties_plants_phenology, container, false)
        tvCluster = fragView.findViewById(R.id.tv_racimo)
        spinnerValve = fragView.findViewById(R.id.spinner_valve)
        saveButton = fragView.findViewById(R.id.btn_save_test)
        return fragView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        index = arguments?.getInt("index")!!
        vars = Gson().fromJson(arguments?.getString("vars"), object : TypeToken<ArrayList<PhenologyVars>?>() {}.type)
        has_cluster = arguments?.getBoolean("has_cluster")!!
        fullPlants = arguments?.getBoolean("fullPlants")!!

        if (fullPlants) {
            signatureAndComment()
            RL_fullview.visibility = View.GONE
        } else {
            initSpinner(16)
            setClustter()
            initRecycler()
            transfElements(vars)

            tvCluster.setOnClickListener { showCluster() }
            saveButton.setOnClickListener {
                if(saveButton.text == "Editar revisión"){
                    loadData(true)
                } else {
                    if (validateData(true)) {
                        showAlertBuilder()
                    }
                }
            }
            if (index == PhenologyVarietiesPlantsActivity.sampling) {
                lastPlant = true
                saveButton.text = "Enviar revisión"
            }
        }

        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccessRequest_postRevision().subscribe(this::getPhenologyVersionResponse).addTo(subscriptions)
        viewModel.onSuccessRequest_postPlant().subscribe(this::getPhenologyPlantResponse).addTo(subscriptions)
        viewModel.onSuccessRequest_postSignAndComment().subscribe(this::getPhenologySignAndCommentResponse).addTo(subscriptions)
        viewModel.onSuccessRequest_getVariableLastRecord().subscribe(this::showVarMeasureDialog).addTo(subscriptions)
    }

    fun evaluateDate() {
        when (index) {
            1 -> {
                prepareDataForRevision()
            }
            else -> {
                prepareDataForPlant()
            }
        }
    }

    fun showAlertBuilder() {
        var validate = true
        if (indexPrincipal == (PhenologyVarietiesPlantsActivity.checkList.size - 1)) {
            for (i in 0 until PhenologyVarietiesPlantsActivity.checkList.size) {
                if (PhenologyVarietiesPlantsActivity.checkList.size == 1) {
                    break
                } else {
                    if (i == PhenologyVarietiesPlantsActivity.checkList.size - 1) {
                        break
                    } else {
                        if (!PhenologyVarietiesPlantsActivity.checkList[i]) {
                            validate = false
                            break
                        }
                    }
                }
            }
        }

        if (validate) {
            //showSaveDialog()
            evaluateDate()
        } else onSNACK(fragView, "Debe de terminar el proceso de las demas plantas", true)
    }

    private fun showSaveDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Fenologia")
        builder.setMessage("¿Desea guardar la información de la planta o las plantas?")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            sendDataRevision(PhenologyVarietiesPlantsActivity.RevisionPost)
            dialog.dismiss()
        }
        builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
        builder.show()
    }

    fun prepareDataForRevision() {
        PhenologyVarietiesPlantsActivity.RevisionPost = Gson().fromJson(arguments?.getString("phenology"), PhenologyPost::class.java)
        val listPlants = ArrayList<PhenologyPlantItem>()
        val listVars = ArrayList<PhenologyVarItem>()
        val listCluster = ArrayList<PhenologyClusterItem>()
        val plant = PhenologyPlantItem()
        for (item in listaVar) {
            var idVar = item.phenology_var_id
            val med = item.medition
            val phenologyState = getState(item)
            val objVarItem = PhenologyVarItem(idVar, med, phenologyState)
            listVars.add(objVarItem)
        }
        if (has_cluster) {
            listCluster.add(objCluster)
        }
        plant.valve = spinnerValve.selectedItemId.toInt()
        plant.vars = listVars
        plant.cluster = listCluster

        listPlants.add(plant)
        PhenologyVarietiesPlantsActivity.RevisionPost.plants = listPlants
        PhenologyVarietiesPlantsActivity.RevisionPost.phenological_state = getPhenological_state(sumGenerative, sumVegetative, sumBalance)

        Log.e("phenologyRevisionPost", Gson().toJson(PhenologyVarietiesPlantsActivity.RevisionPost))
        PhenologyVarietiesPlantsActivity.setupTabsMinusOne()
        //isNew = false
        //sendDataRevision(phenologyRevisionPost)
    }

    fun prepareDataForPlant() {
        val plantItem = PhenologyPlantItem()
        val listVars = ArrayList<PhenologyVarItem>()
        val listCluster = ArrayList<PhenologyClusterItem>()

        for (item in listaVar) {
            val idVar = item.phenology_var_id
            val med = item.medition
            val phenologyState = getState(item)
            val objVarItem = PhenologyVarItem(idVar, med, phenologyState)
            listVars.add(objVarItem)
        }
        if (has_cluster) {
            listCluster.add(objCluster)
        }
        plantItem.valve = spinnerValve.selectedItemId.toInt()
        plantItem.vars = listVars
        plantItem.cluster = listCluster

        Log.d("phenologyPlantPost", Gson().toJson(plantItem))
        sendDataPlant(plantItem)
    }

    private fun signatureAndComment() {
        showsignatureAndCommentDialog()
    }

    private fun showsignatureAndCommentDialog() {
        val dialog = SignCommentDialog().newInstance()
        dialog.setTargetFragment(this, 200)
        dialog.show(fragmentManager!!, SignCommentDialog.TAG)
    }

    fun sendSignatureAndComment(signature: String, comment: String) {
        if (ifConected()) {
            val signAndComment = PhenologySignComment(signature, comment)
            viewModel.postPlants(PhenologyVarietiesPlantsActivity.revision_id)
            viewModel.postPhenologySignAndComment(signAndComment, PhenologyVarietiesPlantsActivity.revision_id.toString())
        }
    }

    fun getPhenologyVersionResponse(respose: PhenologyPostRevisionResponse) {
        Log.d("getPhenologyVersionRes", Gson().toJson(respose))
        //if (isNew) {
        PhenologyVarietiesPlantsActivity.revision_id = respose.id

        PhenologyVarietiesPlantsActivity.listPhenologyPlant.forEach {
            it.revisionId = PhenologyVarietiesPlantsActivity.revision_id
        }

        if (lastPlant) sendPlants()
        else PhenologyVarietiesPlantsActivity.setupTabsMinusOne()
        //isNew = false
        //}
        //PhenologyVarietiesPlantsActivity.setupTabsMinusOne()
    }

    fun getPhenologyPlantResponse(respose: PhenologyPostPlantResponse) {
        Log.d("getPhenologyVersionRes", Gson().toJson(respose))
    }

    fun getPhenologySignAndCommentResponse(respose: PhenologyPostRevisionResponse) {
        Log.d("getPhenologySignAndCom", Gson().toJson(respose))
        lastPlant = false

        val intent = activity?.intent
        intent!!.putExtra("resultRevision", Gson().toJson(respose))
        this.onActivityResult(250, Activity.RESULT_OK, intent)
        activity!!.finish()
    }

    fun getPhenological_state(gen: Int, veg: Int, bal: Int): String {
        return if (gen != 0 && veg != 0 && bal != 0) {
            if (gen >= veg && gen >= bal) {
                "generative"
            } else if (veg >= gen && veg >= bal) {
                "vegetative"
            } else if (bal >= gen && bal >= veg) {
                "balance"
            } else {
                "NA"
            }
        } else {
            "NA"
        }
    }

    fun getState(item: VarMeasurePhenologyItemViewModel): String {
        val ret: String

        if (item.medition!! >= item.balance_from && item.medition!! <= item.balance_to) {
            ret = "balance"
            sumBalance++
        } else if (item.medition!! >= item.generative_from && item.medition!! <= item.generative_to) {
            ret = "generative"
            sumGenerative++
        } else if (item.medition!! >= item.vegetative_from && item.medition!! <= item.vegetative_to) {
            ret = "vegetative"
            sumVegetative++
        } else {
            ret = "NA"
        }
        return ret
    }

    fun sendDataRevision(data: PhenologyPost) {
        if (ifConected()) {
            viewModel.postPhenologyRevision(data)
        }
    }

    fun sendDataPlant(plant: PhenologyPlantItem) {
        if (ifConected()) {
            Log.e("revision_id", PhenologyVarietiesPlantsActivity.revision_id.toString())
            Log.e("plant", PhenologyVarietiesPlantsActivity.pagerPlants.currentItem.toString())

            val plantObj = PhenologyPlant("Planta " + (index), PhenologyVarietiesPlantsActivity.revision_id, plant)
            removeItem()
            PhenologyVarietiesPlantsActivity.listPhenologyPlant.add(plantObj)

            if (lastPlant) {
                if (PhenologyVarietiesPlantsActivity.revision_id == 0) {
                    showSaveDialog()
                } else sendPlants()
            } else PhenologyVarietiesPlantsActivity.setupTabsMinusOne()
        }
    }

    fun removeItem() {
        (0 until PhenologyVarietiesPlantsActivity.listPhenologyPlant.size).forEach { i ->
            if (PhenologyVarietiesPlantsActivity.listPhenologyPlant[i].plantName == "Planta " + (index)) {
                PhenologyVarietiesPlantsActivity.listPhenologyPlant.removeAt(i)
                return
            }
        }
    }

    fun sendPlants() {
        PhenologyVarietiesPlantsActivity.listPhenologyPlant.forEach {
            viewModel.addPhenologyPlant(it.plantName!!, it.revisionId!!, it.plant!!)
        }
        showProgressDialog(false)
        signatureAndComment()
    }

    fun initSpinner(size: Int) {
        val list = ArrayList<String>()
        list.add("SELECCIONAR")
        for (i in 1 until size) {
            list.add(i.toString())
        }
        spinnerValve.adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item_textview, list)
    }

    fun setClustter() {
        if (has_cluster) {
            LL_racimo.visibility = View.VISIBLE
        }
    }

    fun validateData(activate: Boolean): Boolean {
        var validate = true
        var validateVar = true
        var validateCluster = true

        for (item in listaVar) {
            val med = item.medition ?: -10.0
            if (med < 0.0) {
                validateVar = false
            }
        }

        if (has_cluster) {
            when {
                objCluster.button_status == -1 -> {
                    validateCluster = false
                }
                objCluster.flowering_status == -1 -> {
                    validateCluster = false
                }
                objCluster.cuaje_status == -1 -> {
                    validateCluster = false
                }
                objCluster.cluster_no == -1 -> {
                    validateCluster = false
                }
                objCluster.fruit_no == -1 -> {
                    validateCluster = false
                }
            }
            Log.d("objCluster", Gson().toJson(objCluster))
        }

        if (!validateVar) {
            onSNACK(fragView, "Todas las variables son requeridas y no deben estar vacias", activate)
        } else if (!validateCluster) {
            onSNACK(fragView, "Los datos de racimo son requeridos", activate)
        } else if (spinnerValve.selectedItemId.toInt() == 0) {
            validateVar = false
            validateCluster = false
            onSNACK(fragView, "Se debe de seleccionar la válvula que corresponde a la revisión", activate)
        }

        return (validateVar && validateCluster)
    }

    fun onSNACK(view: View, message: String, show: Boolean) {
        if (show) {
            val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT).setAction("Action", null).setActionTextColor(Color.BLUE)
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(Color.RED)
            val textView = snackbarView.findViewById(R.id.snackbar_text) as TextView
            textView.setTextColor(Color.WHITE)
            textView.textSize = 18f
            textView.gravity = Gravity.CENTER
            snackbar.show()
        }
    }

    fun transfElements(data: ArrayList<PhenologyVars>) {
        listaVar.clear()
        for (item in data) {
            val obj = VarMeasurePhenologyItemViewModel(item)
            listaVar.add(obj)
        }
        addElements(listaVar, true)
    }

    fun loadData(editable: Boolean) {
        if(!editable) {
            if (validateData(editable)) {
                reloadData(editable)
                saveButton.text = "Editar revisión"
                tvCluster.isEnable(editable)
                spinnerValve.isEnable(editable)
            }
        } else {
            reloadData(editable)
            saveButton.text = "Guardar"
            spinnerValve.isEnable(editable)
            tvCluster.isEnable(editable)
        }
    }

    private fun reloadData(editable: Boolean){
        showProgressDialog(true)
        mAdapter.editable = editable
        mAdapter.addElements(listaVar)
        showProgressDialog(false)
    }

    fun addElements(data: ArrayList<VarMeasurePhenologyItemViewModel>, editable: Boolean) {
        mAdapter.editable = editable
        mAdapter.addElements(data)
        rv_phenology_var_measure.adapter = mAdapter
    }

    fun initRecycler() {
        rv_phenology_var_measure.hasFixedSize()
        rv_phenology_var_measure.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_phenology_var_measure.itemAnimator = DefaultItemAnimator()
        mAdapter.setHasStableIds(true)
        rv_phenology_var_measure.addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        rv_phenology_var_measure.adapter = mAdapter
    }

    fun showVarMeasureDetail(item: VarMeasurePhenologyItemViewModel) {
        if (ifConected()) {
            varDetailItem = item
            viewModel.getVariableLastRecord(
                    stage_id,
                    PhenologyVarietiesPlantsActivity.RevisionPost.table_no,
                    PhenologyVarietiesPlantsActivity.RevisionPost.groove_from,
                    PhenologyVarietiesPlantsActivity.RevisionPost.groove_to,
                    PhenologyVarietiesPlantsActivity.RevisionPost.variety_id,
                    index)
        }
    }

    fun showVarMeasureDialog(item: VariableLastRecordResponse) {
        varDetailItem.date = item.date

        val dialog = PhenologyVarMeasureDialog.newInstance(Gson().toJson(varDetailItem))
        dialog.show(fragmentManager!!, PhenologyVarMeasureDialog.TAG)
    }

    fun showCluster() {
        val dialog = PhenologyVarClusterDialog.newInstance(objCluster)
        dialog.setTargetFragment(this, 100)
        dialog.show(fragmentManager!!, PhenologyVarClusterDialog.TAG)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("requestCode", requestCode.toString())
        Log.d("resultCode", resultCode.toString())

        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == 100) {
            val json = data!!.getStringExtra(clusterResult)
            objCluster = Gson().fromJson(json, PhenologyClusterItem::class.java)
            if (objCluster.fruit_no == -1) tvCluster.text = ""
            else tvCluster.text = objCluster.fruit_no.toString()
        }

        if (requestCode == 200) {
            sign = data!!.getStringExtra(signResult)
            comment = data!!.getStringExtra(commentResult)
            sendSignatureAndComment(sign, comment)
        }
    }

    override fun onDetach() {
        super.onDetach()
        indexPrincipal = 0
    }

}
