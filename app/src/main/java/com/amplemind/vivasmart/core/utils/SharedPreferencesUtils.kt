package com.amplemind.vivasmart.core.utils


import android.content.Context
import com.amplemind.vivasmart.R
import com.google.gson.Gson
import com.google.gson.GsonBuilder


class SharedPreferencesUtils(var context: Context) {

    var gson = GsonBuilder().serializeNulls().create()
    private var NAME: String? = null

    fun SharedPreferencesUtils(contextActivity: Context) {
        context = contextActivity
        NAME = contextActivity.resources.getString(R.string.app_name)
    }

    fun saveObjectToSharedPreference(serializedObjectKey: String?, `object`: Any?) {
        val sharedPreferences = context!!.getSharedPreferences(NAME, Context.MODE_PRIVATE)
        val sharedPreferencesEditor = sharedPreferences.edit()
        val gson = Gson()
        val serializedObject = gson.toJson(`object`)
        sharedPreferencesEditor.putString(serializedObjectKey, serializedObject)
        sharedPreferencesEditor.apply()
    }

    fun <GenericClass> getSavedObjectFromPreference(preferenceKey: String?, classType: Class<GenericClass>?): GenericClass? {
        val sharedPreferences = context!!.getSharedPreferences(NAME, 0)
        if (sharedPreferences.contains(preferenceKey)) {
            val gson = Gson()
            return gson.fromJson(sharedPreferences.getString(preferenceKey, ""), classType)
        }
        return null
    }

    fun deleteKeyFromPreference(preferenceFileName: String?, preferenceKey: String?): Boolean {
        val sharedPreferences = context!!.getSharedPreferences(preferenceFileName, 0)
        if (sharedPreferences.contains(preferenceKey)) {
            sharedPreferences.edit().remove(preferenceKey).commit()
            return true
        }
        return false
    }

    fun deleteFileFromPreference(preferenceFileName: String?): Boolean {
        val sharedPreferences = context!!.getSharedPreferences(preferenceFileName, 0)
        sharedPreferences.edit().clear().commit()
        return true
    }
}
