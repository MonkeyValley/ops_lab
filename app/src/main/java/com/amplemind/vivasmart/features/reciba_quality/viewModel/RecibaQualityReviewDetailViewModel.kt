package com.amplemind.vivasmart.features.reciba_quality.viewModel

import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.FinishedProductRepository
import com.amplemind.vivasmart.core.repository.response.IssuesResponse
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.production_quality.ItemReportQualityReviewDetailViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class RecibaQualityReviewDetailViewModel @Inject constructor(
        private val repository: FinishedProductRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val listaIssues = mutableListOf<ItemReportQualityReviewDetailViewModel>()
    private val requestSuccess_issues = BehaviorSubject.create<List<ItemReportQualityReviewDetailViewModel>>()
    private val requestSuccess_report = BehaviorSubject.create<ReacibaQualityReviewViewModel>()

    fun getIssuesList(id_crop: Int): Disposable {
        return repository.getIssues(id_crop)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::mapModelToViewModel_issuesUnits)
                .subscribe({
                    listaIssues.clear()
                    listaIssues.addAll(it)
                    requestSuccess_issues.onNext(listaIssues)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    private fun mapModelToViewModel_issuesUnits(result: IssuesResponse): List<ItemReportQualityReviewDetailViewModel> {
        val list = arrayListOf<ItemReportQualityReviewDetailViewModel>()

        for (item in result.data) {
            list.add(ItemReportQualityReviewDetailViewModel(item, "0", "0"))
        }
        list.sortBy { it.name }
        return list
    }

    fun onSuccessRequest_issues(): BehaviorSubject<List<ItemReportQualityReviewDetailViewModel>> {
        return requestSuccess_issues
    }

    fun onSuccessRequest_report(): BehaviorSubject<ReacibaQualityReviewViewModel> {
        return requestSuccess_report
    }

    fun postFinishedRecibaQualityReview(recibaQualityReviewOjb: ReacibaQualityReviewViewModel): Disposable {
        return repository.postRecibaQualityReview(recibaQualityReviewOjb)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestSuccess_report.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

}