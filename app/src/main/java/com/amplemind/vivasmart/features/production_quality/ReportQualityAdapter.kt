package com.amplemind.vivasmart.features.production_quality

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.BR
import com.amplemind.vivasmart.databinding.ItemReportQualityBinding
import io.reactivex.subjects.BehaviorSubject

class ReportQualityAdapter constructor(val list: MutableList<ItemReportQualityViewModel>): RecyclerView.Adapter<ReportQualityAdapter.ProductionQualityViewHolder>() {

    private val onClickSubject = BehaviorSubject.create<ItemReportQualityViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductionQualityViewHolder {
        val binding = ItemReportQualityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductionQualityViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ProductionQualityViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun onClick(): BehaviorSubject<ItemReportQualityViewModel> {
        return onClickSubject
    }

    fun isEmpty() = list.isEmpty()

    fun removeReport(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class ProductionQualityViewHolder(private val binding: ItemReportQualityBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemReportQualityViewModel) {
            binding.setVariable(BR.viewModel, item)
            binding.executePendingBindings()
            if (adapterPosition > -1){
                binding.cvUser.setOnClickListener {
                    item.status = true
                    item.position = adapterPosition
                    onClickSubject.onNext(item)
                }
                binding.cvUser.setOnLongClickListener {
                    item.status = false
                    onClickSubject.onNext(item)
                    return@setOnLongClickListener true
                }
            }
        }

    }

}