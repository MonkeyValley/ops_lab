package com.amplemind.vivasmart.features.fertiriego.viewModel

import android.content.Context
import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_DATE
import com.amplemind.vivasmart.features.fertiriego.fragment.PulseFragment
import com.amplemind.vivasmart.features.fertiriego.service.FertirriegoService
import com.amplemind.vivasmart.vo_core.repository.models.realm.PulseForTable
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveFertirriego
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveForTable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject

class PulseViewModel @Inject constructor(
        private val service: FertirriegoService) : BaseViewModel() {

    var lotId = 0
    var date = ""
    var pulse = 0
    var table = ""
    var valveId: Int = 0

    lateinit var fragment: PulseFragment

    private val requestSuccessPulseDataPerDay = BehaviorSubject.create<PulseForTable>()

    fun setServiceContect(context: Context) {
        service.context = context
        service.preferences  = UserAppPreferences(context, null, null)
    }

    fun validateDate(): Boolean {
        val calendar = Calendar.getInstance()

        var month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        var day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val todayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        calendar.add(Calendar.DATE, -1)
        month = if ((calendar.get(Calendar.MONTH) + 1) < 10) "0" + (calendar.get(Calendar.MONTH) + 1).toString() else (calendar.get(Calendar.MONTH) + 1).toString()
        day = if ((calendar.get(Calendar.DAY_OF_MONTH)) < 10) "0" + (calendar.get(Calendar.DAY_OF_MONTH)).toString() else (calendar.get(Calendar.DAY_OF_MONTH)).toString()
        val yesterdayDate = ((calendar.get(Calendar.YEAR)).toString() + "-" + month + "-" + day)

        return date == todayDate || date == yesterdayDate
    }

    fun calculateDrainPorcent(drain: String, send: String, spike: String): String =
            when {
                drain != "" && send != "" && spike != "" -> {
                    val result = ((drain.toDouble() / (send.toDouble() * spike.toDouble()))) * 100
                    String.format("%.0f", result)
                }
                else -> {
                    ""
                }
            }

    fun calculateMinutes( send: Double): String{
        var result: Double = 0.0
        result = send / 50.0
        return String.format("%.2f", result)
    }
    fun getPulseDataForTable(): Disposable {
        return service.getPulseTableDataPerDay(lotId, date, table, pulse, valveId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccessPulseDataPerDay.onNext(it)
                }, {
                    Log.e("error-getPulse", it.toString())
                    fragment.setDataToZero()
                })
    }

    fun onSuccesPulseDataPerDay(): BehaviorSubject<PulseForTable> = requestSuccessPulseDataPerDay

    fun getSpike(): String {
        return service.getLotSpike(lotId, table.toInt())
    }

    fun savePulseData(spike: String, sending: String, drain: String, minutes: String, drainPercent: String,
                      hour: String, pulsePosition: Int, ceDrain: String, phDrain: String,
                      pulseRegistred: Int, check: Boolean) {
        service.savePulseData(lotId, pulsePosition, spike.toInt(), sending.toInt(), drain.toInt(),
                minutes.toDouble(), drainPercent.toDouble(), hour, date, table, ceDrain, phDrain, pulseRegistred, check, valveId)
    }

    fun saveSpikeLot(spike: String) {
        service.saveSpikeLot(lotId, spike.toInt(), table.toInt())
    }

    fun PulseRemaning(): Int {
        return service.countPulse(lotId, table, date, valveId)
    }

    private val requestSuccessValves = BehaviorSubject.create<List<ValveFertirriego>>()

    fun getValves(): Disposable {
        return service.getValves(lotId, "hydroponic", "T" + table)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { progressStatus.onNext(true ) }
                .doOnTerminate { progressStatus.onNext(false) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestSuccessValves.onNext(it)
                }, {
                    Log.e("valve", it.message)
                })
    }
    fun onSuccessRequestValves(): BehaviorSubject<List<ValveFertirriego>> = requestSuccessValves

}

data class HourModel(
        var pulse: Int = 0,
        var hour: String = ""
)