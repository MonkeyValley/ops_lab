package com.amplemind.vivasmart.features.quality_control.viewModel

import androidx.databinding.ObservableBoolean
import com.amplemind.vivasmart.core.repository.model.ControlQualityActivitiesListModel
import com.amplemind.vivasmart.core.repository.model.ControlQualityGroovesModel
import io.reactivex.Observable

class ControlActivitiesItemViewModel(private val item: ControlQualityActivitiesListModel) {

    var expandable = ObservableBoolean(false)

    var status = item.total
    val activityName = item.activityName
    val activityId = item.activityId
    val activityArea = item.activityAreaId
    val activityCategoryId = item.activityCategoryId
    val activitySubCategory = item.activitySubCategoryId
    val date = item.signDate
    val grooves = item.grooves
    val activityLog = item.activityLogId
    val score = item.score

    fun setExpansable(state: Boolean) {
        expandable.set(state)
    }

    fun showGrooves() : Observable<List<ControlQualityGroovesModel>> {
        return Observable.just(item.grooves)
    }

}
