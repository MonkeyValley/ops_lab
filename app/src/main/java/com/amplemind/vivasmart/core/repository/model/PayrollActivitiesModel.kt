package com.amplemind.vivasmart.core.repository.model

data class PayrollActivitiesModel(val id : String, val name : String, val drawable : Int, val active : Boolean = false)
