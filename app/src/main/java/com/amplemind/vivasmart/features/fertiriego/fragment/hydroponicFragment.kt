package com.amplemind.vivasmart.features.fertiriego.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.custom_views.ItemOffsetDecoration
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.getSpanCount
import com.amplemind.vivasmart.core.extensions.toDp
import com.amplemind.vivasmart.core.extensions.toPx
import com.amplemind.vivasmart.core.utils.FERTIRRIEGO_TPYE
import com.amplemind.vivasmart.features.fertiriego.FertirriegoHydroponicMonitoringActivity
import com.amplemind.vivasmart.features.fertiriego.FertirriegoHydroponicReportActivity
import com.amplemind.vivasmart.features.fertiriego.FertirriegoPHCELotActivity
import com.amplemind.vivasmart.features.fertiriego.adapter.LotsInHydroponicFertirriegoAdapter
import com.amplemind.vivasmart.features.fertiriego.viewModel.HydroponicFragmentViewModel
import com.amplemind.vivasmart.features.fertiriego.viewModel.LotHydroponicFertirriegoItemViewModel
import dagger.android.DispatchingAndroidInjector
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_substrate.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class hydroponicFragment : BaseFragment(), LotsInHydroponicFertirriegoAdapter.Listener {

    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    private var mListener: LotsInHydroponicFertirriegoAdapter.Listener? = null

    @Inject
    lateinit var viewModel: HydroponicFragmentViewModel

    private var mAdapter = LotsInHydroponicFertirriegoAdapter()

    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    companion object {
        fun ifConected(): Boolean {
            return if (hydroponicFragment().hasInternet())
                true
            else {
                Toast.makeText(hydroponicFragment().context, "Se requiere conexión a internet", Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

    fun newInstance(title: String, itemCode: String): SoilFragment {
        val args = Bundle()
        args.putString("Title", title)
        args.putString("itemCode", itemCode)

        val fragment = SoilFragment()
        fragment.arguments = args
        return fragment
    }

    var itemCode: String? = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_substrate, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecycler()

        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.getLots()
        viewModel.onSuccessRequest_lots().subscribe(this::addElements).addTo(subscriptions)
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun getData() {
        viewModel.getLots()
    }

    fun addElements(data: List<LotHydroponicFertirriegoItemViewModel>) {
        mAdapter.addElements(data)
    }

    private fun clickItemLot() {
        mAdapter.onClickItem()
                .throttleFirst(2, TimeUnit.SECONDS)
                .subscribe(this::showLotDetail).addTo(subscriptions)
    }

    private fun showLotDetail(item: LotHydroponicFertirriegoItemViewModel) {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return
        }
        mLastClickTime = now
        if(ifConected()) {
            if(FERTIRRIEGO_TPYE == "Monitoreo" || FERTIRRIEGO_TPYE == "Lotes en monitoreo") {
                val intent= Intent(context, FertirriegoHydroponicMonitoringActivity::class.java).apply {
                     putExtra(FertirriegoHydroponicMonitoringActivity.LOT_ID, item.lot_id.toString())
                     putExtra(FertirriegoHydroponicMonitoringActivity.TAG_NAME, item.lot_name)
                    putExtra("lot_name", "Monitoreo de hidroponia")
                    putExtra(FertirriegoHydroponicReportActivity.MODE, "sustrato")
                 }
                 startActivity(intent)
            } else if (FERTIRRIEGO_TPYE == "Reporte de fertirriego" || FERTIRRIEGO_TPYE == "Reporte") {
                val intent  = Intent(context, FertirriegoHydroponicReportActivity::class.java).apply {
                    putExtra(FertirriegoHydroponicReportActivity.LOT_ID, item.lot_id.toString())
                    putExtra(FertirriegoHydroponicReportActivity.TAG_NAME, "Reporte de hidroponia")
                    putExtra(FertirriegoHydroponicReportActivity.LOT_NAME, item.lot_name)
                    putExtra(FertirriegoHydroponicReportActivity.MODE, "sustrato")
                }
                startActivity(intent)
            } else {
                val intent  = Intent(context, FertirriegoPHCELotActivity::class.java).apply {
                    putExtra(FertirriegoPHCELotActivity.LOT_ID, item.lot_id.toString())
                    putExtra(FertirriegoPHCELotActivity.TAG_NAME, "CE-pH")
                    putExtra(FertirriegoPHCELotActivity.LOT_NAME, item.lot_name)
                    putExtra(FertirriegoPHCELotActivity.MODE, "sustrato")
                }
                startActivity(intent)
            }
        }
    }

    private fun setupRecycler() {
        rv_selection_lots_hydroponic.hasFixedSize()
        rv_selection_lots_hydroponic.layoutManager = GridLayoutManager(context, getSpanCount(context!!))
        rv_selection_lots_hydroponic.addItemDecoration(ItemOffsetDecoration(4.toDp().toPx()))

        mAdapter.listener = this
        rv_selection_lots_hydroponic.adapter = mAdapter
        clickItemLot()
    }

    override fun onGetCounterValue(position: Int): Observable<Int>? {
        return mListener?.onGetCounterValue(position)
    }
}
