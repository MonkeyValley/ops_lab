package com.amplemind.vivasmart.features.phenology.viewmodel

import android.util.Log
import com.amplemind.vivasmart.core.base.BaseViewModel
import com.amplemind.vivasmart.core.repository.PhenologyRepository
import com.amplemind.vivasmart.core.utils.HandleApiErrors
import com.amplemind.vivasmart.features.phenology.models.local.*
import com.amplemind.vivasmart.features.phenology.models.realm.PhenologyModel
import com.amplemind.vivasmart.features.phenology.models.realm.PhenologyPlantsModel
import com.amplemind.vivasmart.features.phenology.models.realm.convetPhenologyClusterModel
import com.amplemind.vivasmart.features.phenology.models.realm.convetPhenologyVarModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import javax.inject.Inject

class PhenologyVarietiesPlantFragmentViewModel @Inject constructor(
        private val repository: PhenologyRepository,
        private val apiErrors: HandleApiErrors) : BaseViewModel() {

    private val requestSuccess_postRevision = BehaviorSubject.create<PhenologyPostRevisionResponse>()
    private val requestSuccess_postPlant = BehaviorSubject.create<PhenologyPostPlantResponse>()
    private val requestSuccess_postSignAndComment = BehaviorSubject.create<PhenologyPostRevisionResponse>()
    private val requestSuccess_getVariableLastRecord = BehaviorSubject.create<VariableLastRecordResponse>()

    fun postPhenologyRevision(objectModel: PhenologyPost): Disposable {
        return repository.postPhenologyRevision(objectModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestSuccess_postRevision.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSuccessRequest_postRevision(): BehaviorSubject<PhenologyPostRevisionResponse> {
        return requestSuccess_postRevision
    }

    fun postPhenologyPlant(objectModel: PhenologyPlantItem, id_revision: String): Disposable {
        return repository.postPhenologyPlant(objectModel, id_revision)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onSuccessRequest_postPlant().onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSuccessRequest_postPlant(): BehaviorSubject<PhenologyPostPlantResponse> {
        return requestSuccess_postPlant
    }

    fun postPhenologySignAndComment(objectModel: PhenologySignComment, id_revision: String): Disposable {
        return repository.postPhenologySignAndComment(objectModel, id_revision)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestSuccess_postSignAndComment.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSuccessRequest_postSignAndComment(): BehaviorSubject<PhenologyPostRevisionResponse> {
        return requestSuccess_postSignAndComment
    }

    fun getVariableLastRecord(stage_id: Int, table_no: Int, groove_from: Int, groove_to: Int, var_id: Int, plant_number: Int): Disposable {
        return repository.getVariableLastRecord(stage_id, table_no, groove_from, groove_to, var_id, plant_number)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { progressStatus.onNext(true) }
                .doOnTerminate { progressStatus.onNext(false) }
                .subscribe({
                    requestSuccess_getVariableLastRecord.onNext(it)
                }, {
                    requestFail.onNext(apiErrors.handleLoginError(it))
                })
    }

    fun onSuccessRequest_getVariableLastRecord(): BehaviorSubject<VariableLastRecordResponse> {
        return requestSuccess_getVariableLastRecord
    }

    fun addPhenologyPlant(plantName: String, revisionId: Int, plant: PhenologyPlantItem) {
        Realm.getDefaultInstance().use { realm ->

            var result = realm.where(PhenologyModel::class.java)
                    .equalTo("revisionId", revisionId)
                    .findFirst()

            if (result != null) {
                var plantResult = result.plantList!!.realm
                        .where(PhenologyPlantsModel::class.java)
                        .equalTo("plantName", plantName)
                        .and()
                        .equalTo("revisionId", revisionId)
                        .findFirst()

                realm.executeTransaction {
                    if (plantResult != null) {
                        plantResult.deleteFromRealm()
                    }

                    result.plantList!!.add(PhenologyPlantsModel(
                            revisionId,
                            plantName,
                            plant.valve,
                            convetPhenologyVarModel(plant.vars!!),
                            convetPhenologyClusterModel(plant.cluster!!)))

                    it.insertOrUpdate(result)
                }

            } else {
                realm.executeTransaction {
                    val newPhenology = it.createObject(PhenologyModel::class.java, revisionId)
                    newPhenology.plantList!!.add(PhenologyPlantsModel(
                            revisionId,
                            plantName,
                            plant.valve,
                            convetPhenologyVarModel(plant.vars!!),
                            convetPhenologyClusterModel(plant.cluster!!)))
                    it.insertOrUpdate(newPhenology)
                }
            }

        }
    }

    fun postPlants(revisionId: Int): Boolean {
        Realm.getDefaultInstance().use { realm ->

            val result = realm.where(PhenologyModel::class.java)
                    .equalTo("revisionId", revisionId)
                    .findFirst()

            if(result != null) {
                Log.e("DestroyPlant", "Destroy")
                for (plant in result!!.plantList!!) {

                    val listVars = ArrayList<PhenologyVarItem>()
                    for (item in plant.vars!!) {
                        listVars.add(PhenologyVarItem(item.phenology_var_id, item.medition, item.phenological_state))
                    }

                    val listCluster = ArrayList<PhenologyClusterItem>()
                    for (item in plant.cluster!!) {
                        listCluster.add(PhenologyClusterItem(item.button_status, item.flowering_status,
                                item.cuaje_status, item.cluster_no, item.fruit_no))
                    }

                    postPhenologyPlant(PhenologyPlantItem(plant.valve, listVars, listCluster), result.revisionId.toString())
                }

                realm.executeTransaction {
                    result.deleteFromRealm()
                }

            }
            return true
        }
    }


}