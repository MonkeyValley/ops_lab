package com.amplemind.vivasmart.features.haulage.viewModel

import android.os.Build
import androidx.annotation.RequiresApi

class ItemHaulageReportViewModel (
        private var date: String,
        private val folio: String,
        private val boxNo: Float,
        private val validBoxNo: Float,
        val reception: Boolean
) {

    val createDate: String
        @RequiresApi(Build.VERSION_CODES.O)
        get() = date

    val folioNumber: String
        get() = folio

    val box: String
        get() = "%.1f".format(boxNo)

    val validBoxes: String
        get() = "%.1f".format(validBoxNo)

}

