package com.amplemind.vivasmart.vo_core.repository.models.realm

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class MipeMonitoringModel(
        @SerializedName("lot_id") var lotId: Int? = 0,
        @SerializedName("table_no") var table: Int? = 0,
        @SerializedName("groove_no") var grooveNo: Int? = 0,
        @SerializedName("date") var date: String? = "",
        @SerializedName("week") var week: Int? = 0,
        @SerializedName("finished") var finished: Boolean? = false,
        @SerializedName("plants") var plants: RealmList<PlantsMonitoringModel>? = null
) : RealmObject()

open class PlantsMonitoringModel(
        @SerializedName("plant_no") var plantNo: Int? = 0,
        @SerializedName("quadrant") var quadrant: Int? = 0,
        @SerializedName( "findings") var findings: RealmList<FindingsModel>? = null
): RealmObject()

open class FindingsModel(
        @SerializedName("plague_id") var plagueId: Int? = 0,
        @SerializedName("findings") var finding: Int? = 0,
        @SerializedName("level") var level: Int? = 0
) : RealmObject()