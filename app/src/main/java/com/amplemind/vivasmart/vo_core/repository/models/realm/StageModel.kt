package com.amplemind.vivasmart.vo_core.repository.models.realm

import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.vo_core.repository.models.Identifiable
import com.amplemind.vivasmart.vo_core.utils.readRealmList
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import java.util.*

open class StageModel (
        @SerializedName("id")               var id: Int = 0,
        @SerializedName("lot")              var lot : LotModel? = null,
        @SerializedName("code")             var code: Int = 0,
        @SerializedName("crop_id")          var cropId: Int = 0,
        @SerializedName("cycle_id")         var cycleId: Int = 0,
        @SerializedName("density")          var density: Double = 0.0,
        @SerializedName("is_active")        var isActive: Boolean = false,
        @SerializedName("is_closed")        var isClosed: Boolean = false,
        @SerializedName("last_close")       var lastClose: String? = null,
        @SerializedName("lot_id")           var lotId : Int = 0,
        @SerializedName("crop")             var crop : CropStageModel? = null,
        @SerializedName("plant_date")       var plantDate : String? = null,
        @SerializedName("plants_per_groove")var plantsPerGroove : Int = 0,
        @SerializedName("stage_activities") var stageActivities : Boolean = false,
        @SerializedName("varieties")        var varieties: RealmList<StageVarietyModel>? = null,
        @PrimaryKey @SerializedName("uuid") override var uuid: String = UUID.randomUUID().toString(),
        @Index @SerializedName("version")   var version: Long = 0,

        //Local fields
        var activityLogCount: Int = 0

) : RealmObject(), Identifiable, Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readParcelable(LotModel::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readParcelable(CropStageModel::class.java.classLoader),
            parcel.readString(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            readRealmList(parcel, StageVarietyModel.CREATOR),
            parcel.readString()!!,
            parcel.readLong())


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeParcelable(lot, flags)
        parcel.writeInt(code)
        parcel.writeInt(cropId)
        parcel.writeInt(cycleId)
        parcel.writeDouble(density)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeByte(if (isClosed) 1 else 0)
        parcel.writeString(lastClose)
        parcel.writeInt(lotId)
        parcel.writeParcelable(crop, flags)
        parcel.writeString(plantDate)
        parcel.writeInt(plantsPerGroove)
        parcel.writeByte(if (stageActivities) 1 else 0)
        parcel.writeTypedList(varieties)
        parcel.writeString(uuid)
        parcel.writeLong(version)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StageModel> {
        override fun createFromParcel(parcel: Parcel): StageModel {
            return StageModel(parcel)
        }

        override fun newArray(size: Int): Array<StageModel?> {
            return arrayOfNulls(size)
        }
    }
}

