package com.amplemind.vivasmart.features.interfaces

import com.amplemind.vivasmart.features.production_quality.CategoriesDetailItemViewModel
import io.reactivex.Observable

interface ICategoriesDetail {

    fun getDetectionTypes() : Observable<MutableList<CategoriesDetailItemViewModel>>
    fun updateDetection(index: Int)
    fun getTotalIncidents() : Int

}