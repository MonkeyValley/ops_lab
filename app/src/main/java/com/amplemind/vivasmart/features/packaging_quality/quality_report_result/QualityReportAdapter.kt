package com.amplemind.vivasmart.features.packaging_quality.quality_report_result

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amplemind.vivasmart.databinding.ItemQualityReportBinding
import androidx.databinding.library.baseAdapters.BR

class QualityReportAdapter : RecyclerView.Adapter<QualityReportAdapter.QualityReportViewHolder>() {

    private var list = mutableListOf<ItemQualityReportViewModel>()

    fun addElements(data : List<ItemQualityReportViewModel>){
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QualityReportViewHolder {
        val binding = ItemQualityReportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return QualityReportViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: QualityReportViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class QualityReportViewHolder(private val binding : ItemQualityReportBinding) : RecyclerView.ViewHolder(binding.root){

        private lateinit var item : ItemQualityReportViewModel

        fun bind(item : ItemQualityReportViewModel){
            binding.setVariable(BR.itemModel, item)
            binding.executePendingBindings()
            this.item = item
        }

    }

}
