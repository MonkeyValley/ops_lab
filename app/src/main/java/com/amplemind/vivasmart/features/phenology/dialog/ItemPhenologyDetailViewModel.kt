package com.amplemind.vivasmart.features.phenology.dialog

import com.amplemind.vivasmart.features.phenology.models.local.ValvesModel
import com.amplemind.vivasmart.features.phenology.models.local.VariablesModel


class ItemPhenologyDetailViewModel (var valves: List<ValvesModel>, var variables: List<VariablesModel>)