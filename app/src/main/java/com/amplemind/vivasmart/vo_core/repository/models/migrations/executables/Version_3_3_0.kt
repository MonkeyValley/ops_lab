package com.amplemind.vivasmart.vo_core.repository.models.migrations.executables

import com.amplemind.vivasmart.vo_core.repository.models.migrations.MigrationExecutable
import io.realm.DynamicRealm

class Version_3_3_0 : MigrationExecutable {

    override fun run(realm: DynamicRealm) {
        migrateUploadObjectStageTypeModelrealm(realm)
    }

    private fun migrateUploadObjectStageTypeModelrealm(realm: DynamicRealm){
        realm.schema.get("StageTypeModel")?.apply {
            addField("lotId", Int::class.javaObjectType)
        }
    }


    override fun drop(realm: DynamicRealm) {

    }
}