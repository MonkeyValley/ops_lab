package com.amplemind.vivasmart.core.extensions

import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.repository.model.*
import java.util.*

/**
 * Created by
 *          amplemind on 7/6/18.
 */


fun dummyDetectionTypes(): List<CategoriesDetailModel> {
    val data = mutableListOf<CategoriesDetailModel>()
    for (i in 1..15) {
        data.add(CategoriesDetailModel(i, "Basura", 0))
    }
    return data
}

fun dummyGreenhouseData(): List<ProductionQualityModel> {
    val data = mutableListOf<ProductionQualityModel>()
    for (i in 1..15) {
        data.add(ProductionQualityModel(name = "INV ${i}", complete = i % 2 == 0))
    }
    return data
}

fun dummyGreenhouseDataControl(): List<ControlQualityModel> {
    val data = mutableListOf<ControlQualityModel>()
    for (i in 1..15) {
        data.add(ControlQualityModel(name = "INV ${i}", status = Random().nextInt(4)))
    }
    return data
}


fun dummyListCards(): List<TinderCardModelTemp> {
    val data = mutableListOf<TinderCardModelTemp>()
    data.add(TinderCardModelTemp(1, "Pepino", "http://naturnoa.com/174-atmn_xlarge/cucumber-marketmore-organic-seeds.jpg"))
    data.add(TinderCardModelTemp(2, "Tomate", "https://infoagro.com/mexico/wp-content/uploads/2017/03/Tomate.jpg"))
    data.add(TinderCardModelTemp(3, "Cebolla", "https://www.garibaldifruits.com/Nueva/wp-content/uploads/2014/10/Cebolla-morada-e1412711686675.jpg"))
    data.add(TinderCardModelTemp(4, "Durazno", "http://mercadoabastocordoba.com/wp-content/uploads/2015/11/Durazno-600x300.jpg"))
    data.add(TinderCardModelTemp(5, "Manzana", "https://www.cubanos.guru/wp-content/uploads/2016/11/manzanas.jpg"))
    data.add(TinderCardModelTemp(6, "Naranja", "http://www.plastico.com/documenta/imagenes/126549/Estudiante-mexicana-crea-biomaterial-a-base-de-desechos-de-naranja-G.jpg"))
    data.add(TinderCardModelTemp(7, "Melon", "http://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2017/07/semillasmelon.jpg"))
    data.add(TinderCardModelTemp(8, "Sandia", "https://www.lasemilleria.com/img/med/semillas_de_sandia.jpg"))
    return data
}


fun dummyReportResultData(): List<QualityReportModelTemp> {
    val data = mutableListOf<QualityReportModelTemp>()
    data.add(QualityReportModelTemp("Cicatriz", 20, 20))
    data.add(QualityReportModelTemp("Curvo", 10, 10))
    data.add(QualityReportModelTemp("Deforme", 5, 5))
    data.add(QualityReportModelTemp("Mal cierre", 23, 23))
    data.add(QualityReportModelTemp("Golpe", 11, 11))
    data.add(QualityReportModelTemp("Vencido", 54, 54))
    data.add(QualityReportModelTemp("Amarillo", 65, 65))
    data.add(QualityReportModelTemp("Cicatriz", 32, 32))
    data.add(QualityReportModelTemp("Curvo", 21, 21))
    data.add(QualityReportModelTemp("Deforme", 53, 53))
    data.add(QualityReportModelTemp("Mal cierre", 78, 78))
    data.add(QualityReportModelTemp("Golpe", 76, 76))
    data.add(QualityReportModelTemp("Vencido", 43, 43))
    data.add(QualityReportModelTemp("Amarillo", 32, 32))
    return data
}



fun dummyQuantityOrder() : QuantityOrderModelTemp {
    val issues = mutableListOf<IssuesModelTemp>()

    val photos = mutableListOf<String>()
    photos.add("https://www.contramuro.com/wp-content/uploads/2017/11/academia-nacional-de-ciencias-de-EEUU-Los-cultivos-transgenicos-son-seguros.jpg")
    photos.add("https://agromagazine.es/wp-content/uploads/2017/07/Cultivos-invernados-webpg.jpg")
    photos.add("https://d17w6g4z9zersu.cloudfront.net/listings/Grl3oRE1uoD_fa5efdf4532448a9d5b2d6e8c497c32d.jpg")
    photos.add("https://www.cosemarozono.com/wp-content/uploads/2017/05/tratamiento-agua-con-ozono-cultivos-hidroponicos.jpg")

    for (item in 0..10){
        issues.add(IssuesModelTemp("Cicatriz $item",item,20, photos = photos))
    }

    return QuantityOrderModelTemp(issues)
}


fun dummyActivitiesPayrollLines(): List<PayrollMenuModel> {
    val data = mutableListOf<PayrollMenuModel>()
    data.add(PayrollMenuModel("T. Bola", R.drawable.ic_line_activity))
    data.add(PayrollMenuModel("Pepino E.", R.drawable.ic_line_activity))
    data.add(PayrollMenuModel("T. Grape", R.drawable.ic_line_activity))
    data.add(PayrollMenuModel("Sweet pepper", R.drawable.ic_line_activity))
    return data
}


fun dummyActivitiesPayrollChoose(): List<PayrollActivitiesModel> {
    val data = mutableListOf<PayrollActivitiesModel>()
    data.add(PayrollActivitiesModel("1321JK", "Arrancar planta", R.drawable.ic_arrancar_planta))
    data.add(PayrollActivitiesModel("1028HGHJ", "Arreglo de racimo", R.drawable.ic_arreglo_racimo))
    data.add(PayrollActivitiesModel("10GSKdqwL", "Bajar planta", R.drawable.ic_bajar_planta))
    data.add(PayrollActivitiesModel("10GSKasL", "Alinear bolsa", R.drawable.ic_alinear_bolsa))
    data.add(PayrollActivitiesModel("10GSKawsL", "Barrer canaleta", R.drawable.ic_barrer_canaleta))
    data.add(PayrollActivitiesModel("10GSwdKL", "Capacitaciòn", R.drawable.ic_capacitacion))
    data.add(PayrollActivitiesModel("10GSsaKL", "Capar brote", R.drawable.ic_capar_broto))
    //TODO: Change gridlayput 2 or 1
    if (Random().nextBoolean()) {
        data.add(PayrollActivitiesModel("10GSKdwL", "Colocar anillos", R.drawable.ic_colocar_anillos))
    }
    return data
}



fun dummyAddActivities(): List<AddActivitiesListModel> {
    val data = mutableListOf<AddActivitiesListModel>()
    data.add(AddActivitiesListModel("Empacar"))
    data.add(AddActivitiesListModel("Rezagar"))
    data.add(AddActivitiesListModel("Tirar basura"))
    data.add(AddActivitiesListModel("Regar"))
    data.add(AddActivitiesListModel("Cambiar banda"))
    data.add(AddActivitiesListModel("Barrer"))
    data.add(AddActivitiesListModel("Limpiar"))
    return data
}

fun dummyRosterDetail(): List<RosterDetailModelTemp> {
    val data = mutableListOf<RosterDetailModelTemp>()
    data.add(RosterDetailModelTemp("Corte pepino", "INV 01 M", "8", 300, 250, true))
    data.add(RosterDetailModelTemp("Desbrote y Guia", "INV 01 M", "8", 300, 350, true))
    data.add(RosterDetailModelTemp("Corte pepino", "INV 01 M", "8", 310, 450, true))
    data.add(RosterDetailModelTemp("Desbrote y Guia", "INV 01 M", "8", 330, 950, true))
    data.add(RosterDetailModelTemp("Corte pepino", "INV 01 M", "8", 100, 650, true))
    data.add(RosterDetailModelTemp("Desbrote y Guia", "INV 01 M", "8", 200, 950, false))
    data.add(RosterDetailModelTemp("Corte pepino", "INV 01 M", "8", 400, 850, false))
    data.add(RosterDetailModelTemp("Desbrote y Guia", "INV 01 M", "8", 200, 650, false))
    data.add(RosterDetailModelTemp("Corte pepino", "INV 01 M", "8", 330, 550, false))
    data.add(RosterDetailModelTemp("Desbrote y Guia", "INV 01 M", "8", 310, 210, false))
    return data
}

fun dummyCHangeLots(): List<ChangeLotModel> {
    val data = mutableListOf<ChangeLotModel>()

    data.add(ChangeLotModel("0123 INV 01 S", false))
    data.add(ChangeLotModel("0123 INV 02 S", false))
    data.add(ChangeLotModel("0123 INV 03 S", false))
    data.add(ChangeLotModel("0123 INV 04 S", false))
    data.add(ChangeLotModel("0123 INV 05 S", false))
    data.add(ChangeLotModel("0123 INV 06 S", false))
    data.add(ChangeLotModel("0123 INV 07 S", false))

    return data
}


