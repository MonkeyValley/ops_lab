package com.amplemind.vivasmart.vo_core.repository.models

interface Identifiable {
    val uuid: String
}