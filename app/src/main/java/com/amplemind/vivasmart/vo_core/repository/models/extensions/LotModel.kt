package com.amplemind.vivasmart.vo_core.repository.models.extensions

import android.util.SparseIntArray
import com.amplemind.vivasmart.vo_core.repository.models.realm.LotModel

fun LotModel?.grooveTotal(): Int =
        (this?.table1 ?: 0) +
        (this?.table2 ?: 0) +
        (this?.table3 ?: 0) +
        (this?.table4 ?: 0) +
        (this?.table5 ?: 0) +
        (this?.table6 ?: 0)

fun LotModel?.tableTotal(): Int {
    var counter = 0

    if ((this?.table1 ?: 0) > 0) counter ++
    if ((this?.table2 ?: 0) > 0) counter ++
    if ((this?.table3 ?: 0) > 0) counter ++
    if ((this?.table4 ?: 0) > 0) counter ++
    if ((this?.table5 ?: 0) > 0) counter ++
    if ((this?.table6 ?: 0) > 0) counter ++

    return counter
}

fun LotModel?.tablesAndGrooves(): SparseIntArray {

    val tablesAndGrooves = SparseIntArray()

    if ((this?.table1 ?: 0) > 0) tablesAndGrooves.append(1, this?.table1 ?: 0)
    if ((this?.table2 ?: 0) > 0) tablesAndGrooves.append(2, this?.table2 ?: 0)
    if ((this?.table3 ?: 0) > 0) tablesAndGrooves.append(3, this?.table3 ?: 0)
    if ((this?.table4 ?: 0) > 0) tablesAndGrooves.append(4, this?.table4 ?: 0)
    if ((this?.table5 ?: 0) > 0) tablesAndGrooves.append(5, this?.table5 ?: 0)
    if ((this?.table6 ?: 0) > 0) tablesAndGrooves.append(6, this?.table6 ?: 0)

    return tablesAndGrooves
}