package com.amplemind.vivasmart.core.navegation.data_mediator

import android.os.Parcel
import android.os.Parcelable
import com.amplemind.vivasmart.core.repository.model.CountBox
import com.amplemind.vivasmart.core.repository.model.CountBoxRequest
import com.amplemind.vivasmart.core.repository.model.TimesCollaborator
import com.amplemind.vivasmart.features.production_roster.ItemActivitiesPayrollViewModelNew
import com.amplemind.vivasmart.features.production_roster.viewModels.TimerUserViewModel
import com.amplemind.vivasmart.features.quality_control.viewModel.ControlQualityItemViewModel
import io.reactivex.subjects.ReplaySubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Mediator @Inject constructor() : IMediator, Parcelable {

    var bundleItemActivitiesPayroll: ItemActivitiesPayrollViewModelNew? = null
    var bundleItemLinesPackage: Int? = null
    lateinit var bundleControlQualityItemViewModel: ControlQualityItemViewModel

    var count_box: CountBox? = null

    var timesCollaborator: List<TimesCollaborator>? = null

    val bundleTimerUserViewModel = ReplaySubject.createWithSize<TimerUserViewModel>(1)

    constructor(parcel: Parcel) : this() {
        bundleItemLinesPackage = parcel.readValue(Int::class.java.classLoader) as? Int
        count_box = parcel.readParcelable(CountBox::class.java.classLoader)
    }

    override fun observerBundleTimerUserViewModel(): ReplaySubject<TimerUserViewModel> {
        return bundleTimerUserViewModel
    }

    override fun sendBundleTimerUserViewModel(item: TimerUserViewModel) {
        bundleTimerUserViewModel.onNext(item)
    }

    fun mapTimesToHashMap() : HashMap<Int, Long>{
        val times = HashMap<Int, Long>()
        timesCollaborator?.forEach { timeUser ->
            times[timeUser.id_collaborator] = timeUser.time
        }
        return times
    }

    fun getMapCountBoxRequest(): CountBoxRequest {
        var count = count_box
        if (count_box == null){
            count = CountBox(mutableListOf(),timesCollaborator)
        }

        return CountBoxRequest(count?.presentation, mapTimesToHashMap())
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(bundleItemLinesPackage)
        parcel.writeParcelable(count_box, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Mediator> {
        override fun createFromParcel(parcel: Parcel): Mediator {
            return Mediator(parcel)
        }

        override fun newArray(size: Int): Array<Mediator?> {
            return arrayOfNulls(size)
        }
    }

}