package com.amplemind.vivasmart.features.fertiriego.fragment

import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.amplemind.vivasmart.R
import com.amplemind.vivasmart.core.base.BaseFragment
import com.amplemind.vivasmart.core.extensions.addTo
import com.amplemind.vivasmart.core.extensions.isEnable
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.utils.DecimalDigitsInputFilter
import com.amplemind.vivasmart.features.fertiriego.viewModel.HourModel
import com.amplemind.vivasmart.features.fertiriego.viewModel.MonitoringViewModel
import com.amplemind.vivasmart.features.fertiriego.viewModel.PulseViewModel
import com.amplemind.vivasmart.features.gallery.HourDialog
import com.amplemind.vivasmart.features.phenology.dialog.PhenologyDetailDialog
import com.amplemind.vivasmart.vo_core.repository.models.realm.PulseForTable
import com.amplemind.vivasmart.vo_core.repository.models.realm.ValveFertirriego
import java.util.*
import javax.inject.Inject


class PulseFragment : BaseFragment(), View.OnClickListener, View.OnTouchListener, AdapterView.OnItemSelectedListener {

    @Inject
    lateinit var viewModel: PulseViewModel

    lateinit var fragView: View

    fun newInstance(date: String, lotId: Int, table: String, pulse: Int, lotName: String): PulseFragment {
        val args = Bundle()
        args.putString("DATE", date)
        args.putInt("LOT_ID", lotId)
        args.putString("TABLE", table)
        args.putInt("PULSE", pulse)
        args.putString("LOT_NAME", lotName)

        val fragment = PulseFragment()
        fragment.arguments = args
        return fragment
    }

    companion object {
        var LIST_PULSE_HOUR = ArrayList<HourModel>()

        fun newIntent(listPulseHour: ArrayList<HourModel>): Intent? {
            LIST_PULSE_HOUR = listPulseHour
            return Intent()
        }
    }

    var etPiqueta: TextView? = null
    var etSend: TextView? = null
    var etDrain: TextView? = null
    var etMinutes: TextView? = null
    var etPorcentDrain: TextView? = null
    var etPulseHour: TextView? = null
    var etCEDrain: TextView? = null
    var etPHDrain: TextView? = null
    var btnSendPulse: Button? = null
    var spinnerValve: Spinner? = null
    var trSppinerValves: TableRow? = null

    var lotName: String = ""
    var firtTime = true
    var pulseMsj = ""

    var valveId = 0
    var listPulse = ArrayList<HourModel>()
    val valveList = ArrayList<MonitoringViewModel.ValveItem>()

    lateinit var preferences: UserAppPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragView = inflater.inflate(R.layout.fragment_pulse, container, false)

        viewModel.setServiceContect(activity!!)
        viewModel.fragment = this

        preferences = UserAppPreferences(activity!!, null, null)

        argument()
        setUpUi(fragView!!)
        addSubscribers()

        getDataFromDate()
        valitateSpike()
        validateRemaning()
        viewModel.getValves()
        showValves(preferences.table)
        return fragView
    }

    fun showValves(show: Boolean) {
        trSppinerValves!!.visibility = if (show) View.VISIBLE else View.GONE
        viewModel.valveId = if (!show) 0 else valveId
        getDataFromDate()
        if (show && viewModel.valveId == 0) {
            setDataToZero()
            btnSendPulse!!.visibility = View.GONE
        } else btnSendPulse!!.visibility = View.VISIBLE
        firtTime = true
        validateRemaning()
    }

    private fun validateRemaning() {
        if (viewModel.PulseRemaning() < viewModel.pulse - 1) {
            listPulse.clear()
            for (pulse in viewModel.PulseRemaning() + 1..viewModel.pulse) {
                var hourObj = HourModel(pulse, "")
                listPulse!!.add(hourObj)
            }
        }
    }

    private fun setUpUi(view: View?) {
        etPiqueta = view!!.findViewById(R.id.et_piqueta)
        etSend = view!!.findViewById(R.id.et_send)
        etSend!!.setOnTouchListener(this)
        etDrain = view!!.findViewById(R.id.et_drain)
        etMinutes = view!!.findViewById(R.id.et_minutes_pulse)
        etPorcentDrain = view!!.findViewById(R.id.et_porcent_drain)
        etPulseHour = view!!.findViewById(R.id.et_pulse_hour)
        etPulseHour!!.setOnClickListener(this)
        etCEDrain = view!!.findViewById(R.id.et_ce_drain)
        etCEDrain!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        etPHDrain = view!!.findViewById(R.id.et_ph_drain)
        etPHDrain!!.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(2, 2))
        btnSendPulse = view!!.findViewById(R.id.btn_send_pulse)
        btnSendPulse!!.setOnClickListener(this)
        spinnerValve = view!!.findViewById(R.id.spinner_valve)
        spinnerValve!!.onItemSelectedListener = this
        trSppinerValves = view!!.findViewById(R.id.tr_sppiner_valves)

        val validate: Boolean = viewModel.validateDate()

        setEnableField(validate)

        if (!validate) btnSendPulse!!.visibility = View.GONE

        etSend!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (etSend!!.text.isNotEmpty()) {
                    etMinutes!!.text = viewModel.calculateMinutes(etSend!!.text.toString().toDouble())

                    etPorcentDrain!!.text = viewModel.calculateDrainPorcent(
                            etDrain!!.text.toString(),
                            etSend!!.text.toString(),
                            etPiqueta!!.text.toString()
                    )

                }
            }
        })

        etDrain!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (etDrain!!.text.isNotEmpty()) {

                    etPorcentDrain!!.text = viewModel.calculateDrainPorcent(
                            etDrain!!.text.toString(),
                            etSend!!.text.toString(),
                            etPiqueta!!.text.toString()
                    )

                }
            }
        })

        etPiqueta!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (etPiqueta!!.text.isNotEmpty()) {

                    etPorcentDrain!!.text = viewModel.calculateDrainPorcent(
                            etDrain!!.text.toString(),
                            etSend!!.text.toString(),
                            etPiqueta!!.text.toString()
                    )

                }
            }
        })
    }

    private fun validatePulseRemaning() {
        if (viewModel.PulseRemaning() < viewModel.pulse - 1) {

            pulseMsj = ""

            for (pulse in viewModel.PulseRemaning() + 1..viewModel.pulse) {
                pulseMsj += if (pulse == viewModel.pulse) " y " + pulse + ""
                else " " + pulse + ","
            }

            val builder = AlertDialog.Builder(context)
            builder.setTitle("Hidroponia")
            builder.setMessage("Ingrese la información de los siguientes pulsos pendientes: " + pulseMsj + ".")
            builder.setCancelable(false)
            builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                dialog.dismiss()
            }
            builder.setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
                activity!!.finish()
            }
            builder.show()
        }
    }

    private fun setEnableField(enable: Boolean) {
        etPiqueta!!.isEnable(enable)
        etSend!!.isEnable(enable)
        etDrain!!.isEnable(enable)
        etCEDrain!!.isEnable(enable)
        etPHDrain!!.isEnable(enable)
        etPulseHour!!.isEnable(enable)
    }

    fun argument() {
        viewModel.lotId = arguments!!.getInt("LOT_ID") ?: 0
        viewModel.date = arguments!!.getString("DATE") ?: "00-00-0000"
        viewModel.pulse = arguments!!.getInt("PULSE") ?: 0
        viewModel.table = arguments!!.getString("TABLE") ?: ""
        lotName = arguments!!.getString("LOT_NAME") ?: ""
    }

    fun addSubscribers() {
        viewModel.showError().subscribe(this::onError).addTo(subscriptions)
        viewModel.showProgress().subscribe(this::showProgressDialog).addTo(subscriptions)
        viewModel.onSuccesPulseDataPerDay().subscribe(this::setDataForPulse).addTo(subscriptions)
        viewModel.onSuccessRequestValves().subscribe(this::updateValveList).addTo(subscriptions)
    }

    fun valitateSpike() {
        if (viewModel.validateDate()) etPiqueta!!.text = viewModel.getSpike()
    }

    private fun setDataForPulse(obj: PulseForTable) {
        setDataToZero()
        if (obj != null) {
            if (preferences.table && viewModel.valveId != 0) {
                etPiqueta!!.text = if (obj.spike != 0) obj.spike.toString() else ""
                etSend!!.text = if (obj.sending != 0) obj.sending.toString() else ""
                etDrain!!.text = if (obj.drain != 0) obj.drain.toString() else ""
                etMinutes!!.text = if (obj.minutes != 0.0) obj.minutes.toString() else ""
                etPorcentDrain!!.text = if (obj.drainPercent != 0.0) obj.drainPercent.toString() else ""
                etPulseHour!!.text = if (obj.hour != "") obj.hour else ""
                etCEDrain!!.text = if (obj.ceDrain != null) obj.ceDrain.toString() else ""
                etPHDrain!!.text = if (obj.phDrain != null) obj.phDrain.toString() else ""
                setEnableField(false)
                btnSendPulse!!.visibility = View.GONE
            } else if (!preferences.table && viewModel.valveId == 0) {
                etPiqueta!!.text = if (obj.spike != 0) obj.spike.toString() else ""
                etSend!!.text = if (obj.sending != 0) obj.sending.toString() else ""
                etDrain!!.text = if (obj.drain != 0) obj.drain.toString() else ""
                etMinutes!!.text = if (obj.minutes != 0.0) obj.minutes.toString() else ""
                etPorcentDrain!!.text = if (obj.drainPercent != 0.0) obj.drainPercent.toString() else ""
                etPulseHour!!.text = if (obj.hour != "") obj.hour else ""
                etCEDrain!!.text = if (obj.ceDrain != null) obj.ceDrain.toString() else ""
                etPHDrain!!.text = if (obj.phDrain != null) obj.phDrain.toString() else ""
                setEnableField(false)
                btnSendPulse!!.visibility = View.GONE
            }
        }

        if (viewModel.validateDate() && obj == null) {
            setEnableField(true)
        }
    }

    fun setDataToZero() {
        etSend!!.text = ""
        etDrain!!.text = ""
        etMinutes!!.text = ""
        etPorcentDrain!!.text = ""
        etCEDrain!!.text = ""
        etPHDrain!!.text = ""
        etPulseHour!!.text = ""
    }

    fun getDataFromDate() {
        setEnableField(false)
        viewModel.getPulseDataForTable()
        if (viewModel.validateDate()) {
            if(!preferences.table && viewModel.valveId == 0) {
                btnSendPulse!!.visibility = View.VISIBLE
                setEnableField(true)
            } else if(preferences.table && viewModel.valveId != 0){
                btnSendPulse!!.visibility = View.VISIBLE
                setEnableField(true)
            }
        } else btnSendPulse!!.visibility = View.GONE
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btn_send_pulse -> {
                if (validateTextView()) {
                    Log.e("clickMonitoring", "click")
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("Hidroponia")
                    if (pulseMsj != "")
                        builder.setMessage("¿Confirma el envio e informacion del " + lotName + " - Tabla " + viewModel.table + " - Pulso " + pulseMsj + "?")
                    else
                        builder.setMessage("¿Confirma el envio e informacion del " + lotName + " - Tabla " + viewModel.table + " - Pulso " + viewModel.pulse + "?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
                        savePulse()
                        dialog.dismiss()
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, _ -> dialog.dismiss() }
                    builder.show()
                }
            }

            R.id.et_pulse_hour -> {
                if (viewModel.PulseRemaning() < viewModel.pulse - 1) {
                    val dialog = HourDialog().newInstance(listPulse!!)
                    dialog.setTargetFragment(this, 100)
                    dialog.show(fragmentManager!!, PhenologyDetailDialog.TAG)
                } else {
                    val c = Calendar.getInstance()
                    val hour = c.get(Calendar.HOUR_OF_DAY)
                    val minute = c.get(Calendar.MINUTE)

                    TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener(function = { view, hour, minute ->

                        var hour = hour
                        var am_pm = ""
                        when {
                            hour == 0 -> {
                                hour += 12
                                am_pm = "am"
                            }
                            hour == 12 -> am_pm = "pm"
                            hour > 12 -> {
                                hour -= 12
                                am_pm = "pm"
                            }
                            else -> am_pm = "am"
                        }

                        val timeHour = if (hour < 10) "0" + hour else hour
                        val tiemMin = if (minute < 10) "0" + minute else minute
                        etPulseHour!!.text = "$timeHour:$tiemMin$am_pm"

                    }), hour, minute, false).show()
                }
            }
        }
    }

    private fun validateTextView(): Boolean {
        var validate = false
        when {
            etPiqueta!!.text.isEmpty() -> showSnackBar()
            etSend!!.text.isEmpty() -> showSnackBar()
            etDrain!!.text.isEmpty() -> showSnackBar()
            etMinutes!!.text.isEmpty() -> showSnackBar()
            etPorcentDrain!!.text.isEmpty() -> showSnackBar()
            etPulseHour!!.text.isEmpty() -> showSnackBar()
            preferences.table && viewModel.valveId == 0 -> showSnackBar()
            else -> validate = true
        }

        return validate
    }

    private fun showSnackBar() {
        val snackbar = Snackbar.make(fragView, "Todos los campos son requeridos", Snackbar.LENGTH_SHORT)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.redAlert))
        snackbar.view.findViewById<TextView>(R.id.snackbar_text).setTextColor(Color.WHITE)
        snackbar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackbar.show()
    }

    private fun savePulse() {

        val pulsesPending = viewModel.pulse - viewModel.PulseRemaning().toDouble()
        if (pulsesPending == 1.0) {
            viewModel.savePulseData(etPiqueta!!.text.toString(), etSend!!.text.toString(),
                    etDrain!!.text.toString(), etMinutes!!.text.toString(),
                    etPorcentDrain!!.text.toString(), etPulseHour!!.text.toString(), viewModel.pulse,
                    etCEDrain!!.text.toString(), etPHDrain!!.text.toString(), viewModel.pulse, false)
        } else {
            val send = String.format("%.0f", etSend!!.text.toString().toDouble() / pulsesPending)
            val drain = String.format("%.0f", etDrain!!.text.toString().toDouble() / pulsesPending)
            val minutesPulse = viewModel.calculateMinutes(send.toDouble())
            val porcentDrain = viewModel.calculateDrainPorcent(drain, send, etPiqueta!!.text.toString())

            listPulse!!.forEach {
                viewModel.savePulseData(etPiqueta!!.text.toString(), send, drain, minutesPulse,
                        porcentDrain, it.hour, it.pulse, etCEDrain!!.text.toString(),
                        etPHDrain!!.text.toString(), viewModel.pulse, true)
            }

            etSend!!.text = send
            etDrain!!.text = drain
            etMinutes!!.text = minutesPulse
            etPorcentDrain!!.text = porcentDrain
        }

        viewModel.saveSpikeLot(etPiqueta!!.text.toString())
        setEnableField(false)
        showSaveDialog()
    }

    private fun showSaveDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Hidroponia")
        builder.setMessage("Revisión guardada con éxito")
        builder.setPositiveButton(android.R.string.yes) { dialog, _ ->
            btnSendPulse!!.visibility = View.GONE
            dialog.dismiss()
        }
        builder.show()
    }

    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
        when (view!!.id) {
            R.id.et_send -> {
                if (MotionEvent.ACTION_UP == event!!.getAction()) {
                    if (firtTime) {
                        validatePulseRemaning()
                        firtTime = false
                    }
                }
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == 100) {
            listPulse = LIST_PULSE_HOUR
            etPulseHour!!.text = listPulse.last().hour
        }
    }

    private fun updateValveList(list: List<ValveFertirriego>) {
        valveList.clear()
        valveList.add(MonitoringViewModel.ValveItem(0, "SELECCIONAR VALVULA"))
        for (item in list) {
            valveList.add(MonitoringViewModel.ValveItem(item.valveId, item.valveName))
        }
        spinnerValve?.adapter = ArrayAdapter<MonitoringViewModel.ValveItem>(activity?.applicationContext!!, R.layout.spinner_item_textview, valveList)
    }

    override fun onItemSelected(adapter: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (adapter!!.id) {
            R.id.spinner_valve -> {
                valveId = valveList[position].valveId!!
                viewModel.valveId = valveList[position].valveId!!
                firtTime = true
                if (valveList[position].valveId!! == 0) {
                    setDataToZero()
                    btnSendPulse!!.visibility = View.GONE
                } else {
                    btnSendPulse!!.visibility = View.VISIBLE
                    getDataFromDate()
                }
                validateRemaning()
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {}

}
