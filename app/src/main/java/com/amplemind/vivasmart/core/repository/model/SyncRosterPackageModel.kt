package com.amplemind.vivasmart.core.repository.model

import com.google.gson.annotations.SerializedName

data class SyncRosterPackageModel(
        @SerializedName("employee_code") val employeeCode: String,
        @SerializedName("packingline_id") val packingline_id : Int,
        @SerializedName("activity_id") val activity_id : Int,
        @SerializedName("is_time") val is_time : Boolean,
        @SerializedName("packing_id") val packing_id : Int?,
        @SerializedName("sign") var sign : String?,
        @SerializedName("temp_time") val temp_time : Int,
        @SerializedName("is_training") var is_training : Boolean,
        @SerializedName("is_practice") val is_practice : Boolean,
        @SerializedName("is_paused") val is_paused : Boolean?,
        @SerializedName("is_done") val isDone : Boolean,
        @SerializedName("status") val status: Boolean,
        @SerializedName("offline_id") val offline_id: Int,
        @SerializedName("presentations") val presentation : List<SyncRosterPresentation>?
)

data class SyncRosterPresentation(
        @SerializedName("id")val id_packing : Int,
        @SerializedName("total") val units : Double,
        @SerializedName("actual_stage") val lot_id : Int,
        @SerializedName("temp_time") val temp_time : Int
)

data class SyncRosterDeleteCollaborators(
        @SerializedName("collaborators") val collaborators : MutableList<Int>
)

data class SyncRosterPackageRequest(@SerializedName("bulk") val bulk: List<SyncRosterPackageModel>)
data class SyncRosterPackageResponse(@SerializedName("data") val data: List<SyncRosterPackageModel>)