package com.amplemind.vivasmart.core.repository

import com.amplemind.vivasmart.core.repository.local.AppDatabase
import com.amplemind.vivasmart.core.repository.local.UserAppPreferences
import com.amplemind.vivasmart.core.repository.model.UserModel
import com.amplemind.vivasmart.core.repository.remote.ProfileApi
import com.amplemind.vivasmart.core.utils.ApiAuthorization
import com.amplemind.vivasmart.core.utils.RETRY
import com.amplemind.vivasmart.core.utils.TIME_OUT
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import okhttp3.MultipartBody


open class ProfileRepository @Inject constructor(private val api: ProfileApi,
                                                 private val room: AppDatabase,
                                                 private val preferences: UserAppPreferences) {

    @Inject
    lateinit var authorization: ApiAuthorization

    data class UploadImageResponse(val relative_url: String)
    data class UpdateUserImageRequest(@SerializedName("image") val image: String?)

    fun uploadImage(image: File, type: String): Observable<UploadImageResponse> {
        val img = RequestBody.create(MediaType.parse("image/jpeg"), image)
        val filePart = MultipartBody.Part.createFormData("img", image.name, img)
        val name = RequestBody.create(MediaType.parse("text/plain"), type)
        return api.uploadImage(authorization.getAuthToken(preferences.token), filePart, name)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun updateUserImage(token: String, request: UpdateUserImageRequest): Observable<UserModel> {
        return api.updateUserImage(authorization.getAuthToken(token), request)
                .timeout(TIME_OUT, TimeUnit.SECONDS)
                .retry(RETRY)
    }

    fun logOut(): Observable<Any> {
        return Observable.fromCallable {
            preferences.agreedTutorial = false
            preferences.token = ""
            preferences.userInfo = ""
            room.clearAllTables()
        }
    }

}